package com.wt.util;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Observer;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.mockito.Mockito;

import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.ctcl.xneutrino.broadcast.MarketWatchResponse;

public class BroadcastManagerStub extends BroadCastManager {
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    
    private List<Observer> observers = new ArrayList<>();
    private volatile boolean isFirstTime = false;
    private volatile boolean isAskingPriceForFirstTime = false;
    private Set<Integer> subscribed = new HashSet<>();
    
    public BroadcastManagerStub(String ip, int port) throws IOException {
      super(ip, port);
      executor.scheduleAtFixedRate(new Updater(), 0, 1, TimeUnit.SECONDS);
    }

    public void addObserver(Observer observer) {
      observers.add(observer);
    }

    public void connect() {
    }

    public boolean isConnected() {
      if(!isFirstTime) {
        isFirstTime = true;
        return false;
      }
      
      return true;
    }

    public void unSubscribe(int token) {
      subscribed.remove(token);
    }
    
    public void subscribe(int token) {
      subscribed.add(token);
    }

    
    class Updater implements Runnable {

      @Override
      public void run() {
        for(int token:subscribed) {
          MarketWatchResponse response = Mockito.mock(MarketWatchResponse.class);
          
          doReturn(token).when(response).getToken();
          doReturn(0).when(response).getSegment();
          if (token == 126)
            doReturn(700000.F).when(response).getLTP();
          else if (token == 127) {
            if (!isAskingPriceForFirstTime) {
              doReturn(1040.F)
              .when(response).getLTP();
              isAskingPriceForFirstTime = true;
            } else {
              doNothing()
              .when(response).getLTP();
            }
          }
          else if (token == 22)
            doReturn(101.F).when(response).getLTP();
          else if (token == 122)
            doNothing().when(response).getLTP();
          else if (token == 123)
            doReturn(990.02F).when(response).getLTP();
          else if (token == 124)
            doReturn(1990.03F).when(response).getLTP();
          else if (token == 125)
            doReturn(90.03F).when(response).getLTP();
          else if (token == 1333)
            doReturn(1919.00F).when(response).getLTP();
          else if (token == 3045)
            doReturn(249.00F).when(response).getLTP();
          else if (token == 7852)
            doReturn(304.5F).when(response).getLTP();
          else if (token == 208)
            doReturn(250.0F).when(response).getLTP();
          else if (token == 123 || token == 124 || token == 125 || token == 134 || token == 133)
            doNothing().when(response).getLTP();
          else if (token == 130)
            doNothing().when(response).getLTP();
          else 
            doReturn(1040.F).when(response).getLTP();
          
          for (Observer observer : observers) {
            observer.update(null, response);
          }
        }
      }
      
    }
  }
