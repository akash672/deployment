package com.wt.universe.eod;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static org.approvaltests.Approvals.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.prices.BSEFileNameExtractor;
import com.wt.prices.BSEPriceSource;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

@RunWith(MockitoJUnitRunner.class)
public class BSEPriceSourceShould {

  @SuppressWarnings("unchecked")
  @Test
  public void fetch_prices_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    BSEPriceSource bsePrices = new BSEPriceSource(new BSEFileNameExtractor("file:///" + current + "/src/test/resources/"));
    ActorSystem system = ActorSystem.create("BSETest");
    Props props = Props.create(TestableEquityActor.class);
    ActorRef mock = system.actorOf(props);
    bsePrices.notifyPriceArrival(system.actorSelection(mock.path()),"06/11/2013");
    List<InstrumentPrice> askAndWaitForResult = (List<InstrumentPrice>) askAndWaitForResult(mock, new GetAll());
    final StringBuilder allPrices = new StringBuilder();
    for (InstrumentPrice priceEvent : askAndWaitForResult) {
      allPrices.append(priceEvent + "\n");
    }
    verify(allPrices);
    system.terminate();
  }

  static class GetAll {
  }

  static class TestableEquityActor extends AbstractActor {
    private List<InstrumentPrice> priceEvents = new ArrayList<>();

    @Override
    public Receive createReceive() {
      return receiveBuilder().match(InstrumentPrice.class, cmd -> {
        priceEvents.add(cmd);
        sender().tell(new Done(cmd.getWdId(), " price event received."), self());
      }).match(GetAll.class, cmd -> {
        sender().tell(priceEvents, self());
      }).build();
    }

  }

}
