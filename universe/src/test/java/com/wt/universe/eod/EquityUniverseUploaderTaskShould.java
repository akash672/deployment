package com.wt.universe.eod;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.domain.EquityInstrument;
import com.wt.domain.repo.EquityInstrumentRepository;
import com.wt.domain.write.commands.UpdateEQInstruments;
import com.wt.domain.write.events.Done;
import com.wt.instruments.EquityUniverseSource;
import com.wt.rds.domain.EquityInstrumentRDSRepo;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import commons.Retry;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class EquityUniverseUploaderTaskShould {
  private @Mock EquityInstrumentRepository repo;
  private @Mock EquityInstrumentRDSRepo rdsRepo;
  private @Mock EquityUniverseSource source;
  private @Mock WDActorSelections actors;
  
  private ActorSystem system;
  private EquityInstrument sbi = new EquityInstrument("ISIN1-NSE-EQ","1", "SBI","","EQ");
  private EquityInstrument icici = new EquityInstrument("ISIN2-NSE-EQ","2", "ICICI","","EQ");
  private EquityInstrument pnb = new EquityInstrument("ISIN3-NSE-EQ","3", "PNB", "" ,"EQ");

  private Set<EquityInstrument> yesterday = newHashSet(sbi, icici);
  private Set<EquityInstrument> today = newHashSet(sbi, pnb);
  
  private Observable<EquityInstrument> observable = Observable.from(today);
  private EquityUniverseUploaderTask equityUniverseUploaderTask;
  private ActorRef universeRootActor;
  
  @Before
  public void setup() throws Exception {
    system = ActorSystem.create(); 
    doReturn(observable).when(source).equityInstruments();
    doReturn(yesterday).when(repo).findAll();
    universeRootActor = system.actorOf(Props.create(TestEquityActor.class));
    doReturn(system.actorSelection(universeRootActor.path())).when(actors).universeRootActor();

    equityUniverseUploaderTask = new EquityUniverseUploaderTask(newArrayList(source), actors, repo, rdsRepo);
  }
  
  @After
  public void terminate() throws Exception {
    system.terminate(); 
  }

  @Rule
  public Retry retry = new Retry(5);

  @Test public void 
  sends_all_instruments_to_equity_actor() throws Exception {
    equityUniverseUploaderTask.run();
    
    UpdateEQInstruments updateInstruments = (UpdateEQInstruments) askAndWaitForResult(universeRootActor, new GetUpdateInstruments());
    
    assertThat(updateInstruments).isEqualTo(new UpdateEQInstruments(newHashSet(today)));
    
  }
  

  @Test public void 
  saves_listed_instruments() throws Exception {
    equityUniverseUploaderTask.run();
    
    verify(repo, times(1)).save(pnb);
  }
  
  static class TestEquityActor extends UntypedAbstractActor {
    private UpdateEQInstruments updateInstruments; 
    @Override
    public void onReceive(Object msg) throws Exception {
      if(msg instanceof UpdateEQInstruments) {
        this.updateInstruments = (UpdateEQInstruments) msg;
        sender().tell(new Done("", ""), self());
      }
      if(msg instanceof GetUpdateInstruments) {
        sender().tell(updateInstruments, self());
      }
    }
    
  }
  
  static class GetUpdateInstruments {
  }

  @Test
  public void test_isTodayBOD_function() throws ParseException {
    String date = ZonedDateTime.now(TimeZone.getTimeZone("Asia/Kolkata").toZoneId())
        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    Assert.assertTrue(DateUtils.isTodayBOD(date));
  }
}
