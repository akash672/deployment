package com.wt.universe.eod;

import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UniverseSchedulerShould {
  @Mock
  private UniverseEODTask task1;
  @Mock
  private UniverseEODTask task2;
  private UniverseSchedulers universeSchedulers;

  @Before
  public void setup() throws Exception {
    doNothing().when(task1).runIfNotRanAlready();
    doNothing().when(task2).runIfNotRanAlready();

    universeSchedulers = new UniverseSchedulers(newArrayList(task1, task2), true);
  }

  @Test public void 
  run_all_eod_tasks() throws Exception {
    universeSchedulers.scheduleJobs();

    verifyAllTasksRanOnce();

  }
  
  @Test public void 
  not_run_if_already_running() throws Exception {
    ExecutorService executor = Executors.newFixedThreadPool(2);
    makeTheTaskTakeLong();

    runTheScheduler(executor);
    
    runAgain(executor);
    waitForLongTaskToFinish();
    
    verifyAllTasksRanOnce();
  }

  @Test public void 
  not_throw_exception_if_a_tasks_throws_error_and_will_not_run_subsequent_tasks() throws Exception {
    doThrow(new Exception()).when(task1).runIfNotRanAlready();
    
    universeSchedulers.scheduleJobs();
        
    verify(task1, times(1)).runIfNotRanAlready();
    verify(task2, never()).runIfNotRanAlready();
  }
  
  private void makeTheTaskTakeLong() throws Exception {
    doAnswer(invocation -> {
      Thread.sleep(500);
      return null;
    }).when(task1).runIfNotRanAlready();
  }

  private void runTheScheduler(ExecutorService executor) {
    executor.execute(() -> {
      try {
        universeSchedulers.scheduleJobs();
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
  }

  private void runAgain(ExecutorService executor) {
    runTheScheduler(executor);
  }
  
  private void waitForLongTaskToFinish() throws InterruptedException {
    Thread.sleep(1000);
  }

  private void verifyAllTasksRanOnce() throws Exception {
    verify(task1, times(1)).runIfNotRanAlready();
    verify(task2, times(1)).runIfNotRanAlready();
  }
 
}
