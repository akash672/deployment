package com.wt.universe.eod;

import static org.approvaltests.Approvals.verify;

import org.junit.Test;

import com.wt.domain.EquityInstrument;
import com.wt.instruments.BSEUniverseSource;

import rx.Observable;

public class BSEUniverseSourceShould {

  @Test public void 
  fetch_all_instruments_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    BSEUniverseSource bseUniverse = new BSEUniverseSource("file:///" + current + "/src/test/resources/scrip.zip");
    Observable<EquityInstrument> equityInstruments = bseUniverse.equityInstruments();

    final StringBuilder allEQInstruments = new StringBuilder();
    equityInstruments.forEach(
        equityInstrument -> allEQInstruments.append(equityInstrument + "\n"));

    verify(allEQInstruments);
  }
}
