package com.wt.universe.eod;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static org.approvaltests.Approvals.verify;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.wt.domain.EquityInstrument;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.prices.NSEFileNameExtractor;
import com.wt.prices.NSEPriceSource;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

@RunWith(MockitoJUnitRunner.class)
public class NSEPriceSourceShould {
  @Mock
  private WDActorSelections selections;
  private ActorSystem system = ActorSystem.create("NSETest");
  private Props props = Props.create(TestableEquityActor.class);
  private ActorRef mock = system.actorOf(props);

  @Before
  public void setup() throws Exception {
    doReturn(system.actorSelection(mock.path())).when(selections).universeRootActor();
  }

  @SuppressWarnings("unchecked")
  @Test
  public void fetch_prices_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    NSEPriceSource nsePrices = new NSEPriceSource(
        new NSEFileNameExtractor("file:///" + current + "/src/test/resources/"), selections);

    nsePrices.notifyPriceArrival(system.actorSelection(mock.path()), "08/08/2016");
    List<InstrumentPrice> askAndWaitForResult = (List<InstrumentPrice>) askAndWaitForResult(mock, new GetAll());
    final StringBuilder allPrices = new StringBuilder();
    for (InstrumentPrice priceEvent : askAndWaitForResult) {
      allPrices.append(priceEvent + "\n");
    }
    verify(allPrices);
    system.terminate();
  }

  static class GetAll {
  }

  static class TestableEquityActor extends UntypedAbstractActor {
    private List<InstrumentPrice> priceEvents = new ArrayList<>();
    private Map<String, String> symbolToIsin = ImmutableMap.of(
        "20MICRONS",  "INE144J01027-NSE-EQ", 
        "3IINFOTECH", "INE748C01020-NSE-EQ",
        "3MINDIA",    "INE470A01017-NSE-EQ", 
        "8KMILES",    "INE650K01013-NSE-EQ");
    
    private Map<String, String> symbolToToken = ImmutableMap.of(
        "20MICRONS",  "1", 
        "3IINFOTECH", "2",
        "3MINDIA",    "3", 
        "8KMILES",    "4");
    
    @Override
    public void onReceive(Object cmd) {
      if (cmd instanceof InstrumentPrice) {
        priceEvents.add((InstrumentPrice) cmd);
        sender().tell(new Done(((InstrumentPrice) cmd).getWdId(), " price event received."), self());
      } else if (cmd instanceof GetAll) {
        sender().tell(priceEvents, self());
      } else if (cmd instanceof GetEquityInstrumentBySymbol) {
        sender().tell(new EquityInstrument(symbolToIsin.get(((GetEquityInstrumentBySymbol) cmd).getSymbol()), symbolToToken.get(((GetEquityInstrumentBySymbol) cmd).getSymbol()) ,((GetEquityInstrumentBySymbol) cmd).getSymbol(),"", "EquityInstrument"), self());
      }
    }
  }

}
