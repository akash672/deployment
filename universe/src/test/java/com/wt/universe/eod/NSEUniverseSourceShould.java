package com.wt.universe.eod;

import static org.approvaltests.Approvals.verify;

import org.junit.Test;

import com.wt.domain.EquityInstrument;
import com.wt.instruments.NSEUniverseSource;

import rx.Observable;

public class NSEUniverseSourceShould {
  @Test public void 
  fetch_all_instruments_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    NSEUniverseSource nseUniverse = new NSEUniverseSource("file:///" + current + "/src/test/resources/security.gz");
    Observable<EquityInstrument> equityInstruments = nseUniverse.equityInstruments();

    final StringBuilder allEQInstruments = new StringBuilder();
    equityInstruments.forEach(
        equityInstrument -> allEQInstruments.append(equityInstrument + "\n"));

    verify(allEQInstruments);
  }
}
