package com.wt.universe.bod;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.domain.MutualFundInstrument;
import com.wt.domain.repo.MutualFundInstrumentRepository;
import com.wt.domain.write.commands.UpdateMFInstruments;
import com.wt.domain.write.events.Done;
import com.wt.instruments.MFUniverseSource;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class MFUniverseUploaderTaskShould {
  private @Mock MutualFundInstrumentRepository repo;
  private @Mock MFUniverseSource source;
  private @Mock WDActorSelections actors;

  private ActorSystem system;
  private MutualFundInstrument INF846K01WP8 = new MutualFundInstrument("INF846K01WP8-MF", "INF846K01WP8", "AXCGD1-DP", "5000.0", "1.0","EQUITY","0","CAMS");
  private MutualFundInstrument INF846K01WN3 = new MutualFundInstrument("INF846K01WN3-MF", "INF846K01WN3", "AXCNDP-DP", "5000.0", "1.0","EQUITY","0", "CAMS");
  private MutualFundInstrument INF846K01WL7 = new MutualFundInstrument("INF846K01WL7-MF", "INF846K01WL7", "AXCNDP-DR", "5000.0", "1.0","EQUITY","0", "CAMS");

  private Set<MutualFundInstrument> yesterday = newHashSet(INF846K01WP8, INF846K01WN3);
  private Set<MutualFundInstrument> today = newHashSet(INF846K01WP8, INF846K01WL7);

  private Observable<MutualFundInstrument> observable = Observable.from(today);
  private MFUniverseUploaderTask mfUniverseUploaderTask;
  private ActorRef universeRootActor;

  @Before
  public void setup() throws Exception {
    system = ActorSystem.create();
    doReturn(observable).when(source).instruments();
    doReturn(yesterday).when(repo).findAll();
    universeRootActor = system.actorOf(Props.create(TestEquityActor.class));
    doReturn(system.actorSelection(universeRootActor.path())).when(actors).universeRootActor();

    mfUniverseUploaderTask = new MFUniverseUploaderTask(newArrayList(source), actors, repo);
  }

  @After
  public void terminate() throws Exception {
    system.terminate();
  }

  @Test
  public void sends_all_instruments_to_universe_root_actor() throws Exception {
    mfUniverseUploaderTask.run();
    UpdateMFInstruments updateMFInstruments = (UpdateMFInstruments) askAndWaitForResult(universeRootActor,
        new GetUpdateInstruments());
    assertThat(updateMFInstruments).isEqualTo(new UpdateMFInstruments(newHashSet(today)));
  }

  @Test
  public void saves_listed_instruments() throws Exception {
    mfUniverseUploaderTask.run();

    verify(repo, times(1)).save(INF846K01WL7);
  }

  static class TestEquityActor extends UntypedAbstractActor {
    private UpdateMFInstruments updateInstruments;

    @Override
    public void onReceive(Object msg) throws Exception {
      if (msg instanceof UpdateMFInstruments) {
        this.updateInstruments = (UpdateMFInstruments) msg;
        sender().tell(new Done("", ""), self());
      }
      if (msg instanceof GetUpdateInstruments) {
        sender().tell(updateInstruments, self());
      }
    }

  }

  static class GetUpdateInstruments {
  }
}