package com.wt.universe.bod;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static org.approvaltests.Approvals.verify;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.write.commands.GetMutualFundInstrumentByToken;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.prices.MFFileNameExtractor;
import com.wt.prices.MFPriceSource;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

@RunWith(MockitoJUnitRunner.class)
public class MFPriceSourceShould {
  @Mock
  private WDActorSelections selections;
  private ActorSystem system = ActorSystem.create("MFTest");
  private Props props = Props.create(TestableMFActor.class);
  private ActorRef mock = system.actorOf(props);

  @Before
  public void setup() throws Exception {
    doReturn(system.actorSelection(mock.path())).when(selections).universeRootActor();
  }

  @SuppressWarnings("unchecked")
  @Test
  public void fetch_prices_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    MFPriceSource mfPrices = new MFPriceSource(new MFFileNameExtractor("file:///" + current + "/src/test/resources/"), selections);
    mfPrices.notifyPriceArrival(system.actorSelection(mock.path()), "12/07/2017");
    List<InstrumentPrice> askAndWaitForResult = (List<InstrumentPrice>) askAndWaitForResult(mock, new GetAll());
    final StringBuilder allPrices = new StringBuilder();
    for (InstrumentPrice priceEvent : askAndWaitForResult) {
      allPrices.append(priceEvent + "\n");
    }
    verify(allPrices);
    system.terminate();
  }

  static class GetAll {
  }

  static class TestableMFActor extends UntypedAbstractActor {
    private List<InstrumentPrice> priceEvents = new ArrayList<>();
    private String minInvestment = "1000.0";
    private String minIncrement = "1.0";
    private String schemeType = "Direct";
    private String exitLoad = "1.0";
    private String rta = "CAMS"; 
    private String schemeCode = "5046-DP";
    private Map<String, String> tokenToSymbol = ImmutableMap.of(
        "INF846K01WP8",  "INF846K01WP8-MF", 
        "INF209KA1LH3",  "INF209KA1LH3-MF",
        "INF209K01ZB2", "INF209K01ZB2-MF",
        "INF084M01DK3", "INF084M01DK3-MF",
        "INF084M01DL1", "INF084M01DL1-MF");

    @Override
    public void onReceive(Object cmd) {
      if (cmd instanceof InstrumentPrice) {
        priceEvents.add((InstrumentPrice) cmd);
        sender().tell(new Done(((InstrumentPrice) cmd).getWdId(), " price event received."), self());
      } else if (cmd instanceof GetAll) {
        sender().tell(priceEvents, self());
      } else if (cmd instanceof GetMutualFundInstrumentByToken) {
        sender().tell(new MutualFundInstrument(tokenToSymbol.get(((GetMutualFundInstrumentByToken) cmd).getToken()), "Instrument", schemeCode, minInvestment, minIncrement, schemeType, exitLoad, rta), self());
      }
    }
  }

}
