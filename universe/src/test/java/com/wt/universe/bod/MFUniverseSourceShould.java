package com.wt.universe.bod;

import static org.approvaltests.Approvals.verify;

import org.junit.Test;

import com.wt.domain.MutualFundInstrument;
import com.wt.instruments.MFUniverseSource;

import rx.Observable;

public class MFUniverseSourceShould {
  @Test public void 
  fetch_all_instruments_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    MFUniverseSource mfUniverse = new MFUniverseSource("file:///" + current + "/src/test/resources/", "SCHMSTRDET.txt");
    Observable<MutualFundInstrument> instruments = mfUniverse.instruments();

    final StringBuilder allInstruments = new StringBuilder();
    instruments.forEach(
        instrument -> allInstruments.append(instrument + "\n"));

    verify(allInstruments);
  }
}
