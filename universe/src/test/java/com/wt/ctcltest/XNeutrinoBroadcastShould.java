package com.wt.ctcltest;

import static akka.stream.ActorMaterializer.create;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.ctcl.xneutrino.broadcast.MarketWatchResponse;
import com.wt.broadcast.xneutrino.XNeutrinoBroadcast;
import com.wt.config.utils.WDProperties;
import com.wt.domain.EquityInstrument;
import com.wt.domain.Instrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.GetPrice;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.akka.CancellableStream;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import akka.testkit.javadsl.TestKit;
import commons.Retry;

@RunWith(MockitoJUnitRunner.class)
public class XNeutrinoBroadcastShould {

  PriceWithPaf price1 = new PriceWithPaf(new InstrumentPrice(103.0f, 0, "22"), Pafs.withCumulativePaf(1.));
  PriceWithPaf price2 = new PriceWithPaf(new InstrumentPrice(101.0f, 0, "22"), Pafs.withCumulativePaf(1.));
  PriceWithPaf price3 = new PriceWithPaf(new InstrumentPrice(100.0f, 0, "22"), Pafs.withCumulativePaf(1.));

  private static final Instrument INSTRUMENT = new EquityInstrument("ISIN-NSE-EQ", "22", "ACC", "Desc", "EQ");
  @Mock
  private BroadCastManager broadCastManager;
  @Mock
  private WDActorSelections actorSelections;
  @Autowired
  private WDProperties wdProperties;

  @Rule
  public Retry retry = new Retry(5);
  
  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  private XNeutrinoBroadcast xneutrino;
  public final ActorSystem system = ActorSystem.create("TestSystem");
  private Props props = Props.create(TestableEquityActor.class, () -> new TestableEquityActor());
  private ActorRef mock = system.actorOf(props);

  @org.junit.Before
  public void setup() {
    environmentVariables.set("WT_BROADCAST_OPEN", "true");
    xneutrino = new XNeutrinoBroadcast(broadCastManager, actorSelections, wdProperties);
    doReturn(true).when(broadCastManager).isConnected();
    doReturn(system.actorSelection(mock.path())).when(actorSelections).universeRootActor();
  }

  static class TestableEquityActor extends AbstractActor {
    private PriceWithPaf price;

    @Override
    public Receive createReceive() {
      return receiveBuilder().match(PriceWithPaf.class, cmd -> {
        price = cmd;
      }).match(GetPrice.class, cmd -> {
        sender().tell(price, self());
      }).build();
    }

  }

  @Test
  public void subscribe_to_broadcast_manager_for_snapshot() throws IOException, InterruptedException {
    new TestKit(system) {
      {
        mock.tell(price1, ActorRef.noSender());

        xneutrino.snapshot(INSTRUMENT, this.getRef());

        verify(broadCastManager, times(1)).subscribe(22);

        xneutrinoUpdates(100.0f, 22);

        expectMsgEquals(price3);
      }
    };
  }

  @Test
  public void unsubscribe_to_broadcast_manager_after_subscribed_to_snapshot() throws IOException, InterruptedException {
    new TestKit(system) {
      {
        mock.tell(price1, ActorRef.noSender());

        xneutrino.snapshot(INSTRUMENT, this.getRef());

        verify(broadCastManager, times(1)).subscribe(22);

        xneutrinoUpdates(100.0f, 22);

        expectMsgEquals(price3);

        xneutrinoUpdates(101.0f, 22);
        expectNoMessage();
      }
    };
  }

  @Test
  public void sends_the_last_updated_tick() throws IOException, InterruptedException {
    ActorMaterializer mat = create(system);

    mock.tell(price1, ActorRef.noSender());

    CancellableStream<PriceWithPaf, NotUsed> source = xneutrino.subscribeStream(INSTRUMENT, mat);
    verify(broadCastManager, atLeastOnce()).subscribe(22);

    xneutrinoUpdates(103.0f, 22);
    xneutrinoUpdates(101.0f, 22);
    xneutrinoUpdates(100.0f, 22);

    List<PriceWithPaf> prices = new ArrayList<PriceWithPaf>();

    Source<PriceWithPaf, NotUsed> last = source.getSource();

    last.runForeach(update -> prices.add(update), mat);
    assertThat(prices).contains(price3);
  }

  private void xneutrinoUpdates(float price, int token) {
    MarketWatchResponse marketWatchResponse = mock(MarketWatchResponse.class);
    doReturn(price).when(marketWatchResponse).getLTP();
    doReturn(token).when(marketWatchResponse).getToken();
    xneutrino.update(null, marketWatchResponse);
  }

}
