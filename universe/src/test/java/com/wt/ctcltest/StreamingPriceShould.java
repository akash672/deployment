package com.wt.ctcltest;

import static akka.stream.ActorMaterializer.create;
import static com.wt.domain.PriceWithPaf.noPrice;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.CtclTestAppConfiguration;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.EquityInstrument;
import com.wt.domain.PriceWithPaf;
import com.wt.utils.akka.CancellableStream;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.testkit.TestProbe;
import commons.Retry;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CtclTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class StreamingPriceShould {
  @Autowired
  private ActorSystem system;

  @Autowired
  private CtclBroadcast ctclBroadcast;
  
  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
  
  @Rule
  public Retry retry = new Retry(5);

  @Test
  public void be_intercepted_by_the_source_actor() throws IOException, InterruptedException {
    
    environmentVariables.set("WT_BROADCAST_OPEN", "true");
    CancellableStream<PriceWithPaf, NotUsed> priceStream = ctclBroadcast
        .subscribeToLiveStream(new EquityInstrument("ISIN-NSE", "22", "ACC", "Desc", "EQ"), create(system));
    
    Source<PriceWithPaf, NotUsed> sourceUnderTest = priceStream.getSource();
    
    TestProbe probe = new TestProbe(system);
    
    sourceUnderTest
    .to(Sink.actorRef(probe.ref(), noPrice("22")))
    .run(create(system));
    
    PriceWithPaf priceWithPaf = probe.expectMsgClass(PriceWithPaf.class);
    
    assertThat(priceWithPaf.getPrice().getPrice()).isEqualTo(101.);
    
  }
}
