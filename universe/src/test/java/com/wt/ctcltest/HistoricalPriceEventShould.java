package com.wt.ctcltest;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.CtclTestAppConfiguration;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.GetHistoricalPriceEventsByInstrument;
import com.wt.domain.write.commands.HistoricalPriceEventsList;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.DateUtils;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.testkit.javadsl.TestKit;
import commons.Retry;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CtclTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class HistoricalPriceEventShould {

  private static final String USER_ADVISE_COMPOSITE_KEY = "userAdviseCompositeKey";
  private static final long TWENTY_DAYS_FROM_NOW = DateUtils.currentTimeInMillis() - (20 * 24 * 60 * 60 * 1000);
  private static final long MILLISECONDS_IN_A_DAY = (24 * 60 * 60 * 1000);
  private static final long TEN_DAYS_FROM_NOW = DateUtils.currentTimeInMillis() - (10 * 24 * 60 * 60 * 1000);
  private static final double CURRENT_MARKET_PRICE = 1040d;
  private static final String TICKER = "22-NSE-EQ";
  private List<PriceWithPaf> prices = new ArrayList<PriceWithPaf>();
  public @Autowired ActorSystem system;
  private ActorRef universeRootActor;
  
  @Rule
  public Retry retry = new Retry(5);

  @Before
  public void createAndFeedEquityActor() {
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
  }

  private void sendEODPriceToEquityActor(double currentMarketPrice, long deductedDays, String token) {
    PriceWithPaf price = new PriceWithPaf(
        new InstrumentPrice(currentMarketPrice, TEN_DAYS_FROM_NOW - deductedDays, token), Pafs.withCumulativePaf(1.));
    prices.add(price);
    universeRootActor.tell(price, ActorRef.noSender());
  }

  @Test
  public void testHistoricalPerformance() throws InterruptedException {
    new TestKit(system) {
      {
        for (int i = 0; i < 10; i++)
          sendEODPriceToEquityActor(CURRENT_MARKET_PRICE + i, (10-i)*MILLISECONDS_IN_A_DAY, TICKER);
        GetHistoricalPriceEventsByInstrument getHistoricalPriceEventsByInstrument = new GetHistoricalPriceEventsByInstrument(
            TICKER, TWENTY_DAYS_FROM_NOW, USER_ADVISE_COMPOSITE_KEY);
        universeRootActor.tell(getHistoricalPriceEventsByInstrument, getRef());
        HistoricalPriceEventsList historicalPriceEventsList = expectMsgClass(HistoricalPriceEventsList.class);
        assertThat(historicalPriceEventsList.getListOfPriceEvent()).isEqualTo(prices);
      }
    };
  }
  @After
  public void shutDown() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    try {
      deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
