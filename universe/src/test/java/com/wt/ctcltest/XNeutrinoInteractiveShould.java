package com.wt.ctcltest;

import static com.wt.domain.OrderSide.BUY;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.broadcast.xneutrino.CancellablePriceStream;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.domain.CtclEquityOrder;
import com.wt.domain.CtclOrder;
import com.wt.domain.DestinationAddress;
import com.wt.domain.EquityInstrument;
import com.wt.domain.Instrument;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.OrderKey;
import com.wt.domain.OrderSegment;
import com.wt.domain.OrderSide;
import com.wt.domain.OrderType;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.OrderPlacedWithExchange;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.utils.DateUtils;
import com.wt.utils.NumberUtils;
import com.wt.utils.akka.CancellableStream;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import akka.testkit.javadsl.TestKit;
import lombok.NoArgsConstructor;

@RunWith(MockitoJUnitRunner.class)
public class XNeutrinoInteractiveShould {
  private static final String TOKEN = "22";
  private static final String SYMBOL = "ACC";
  private static final String ISIN = "ISIN-NSE-EQ";
  private static final String SEGMENT = "EQ";
  public final ActorSystem system = ActorSystem.create("TestSystem");
  @Mock
  private CtclBroadcast ctclBroadcast;
  @Mock
  private CtclInteractive ctclInteractive;
  @Mock
  private WDActorSelections wdActorSelections;
  private Props props = Props.create(TestableOrderActor.class, () -> new TestableOrderActor());
  private ActorRef mock = system.actorOf(props);
  private ActorRef testActor;

  @NoArgsConstructor
  class TestableOrderActor extends AbstractActor {
    @Override
    public Receive createReceive() {
      return receiveBuilder().match(SendOrder.class, cmd -> {
        ctclInteractive.sendMarketCashOrder(createOrder());
      }).build();
    }

    private CtclOrder createOrder() {
      CtclOrder orderData = new CtclEquityOrder();
      OrderKey orderKey = new OrderKey();
      OrderExecutionMetaData orderInstrument = new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(TOKEN.split("-")[0], OrderSide.BUY, 22, OrderType.MARKET, OrderSegment.Equity)
      .withSymbol("").build();
      orderKey
          .setUsernameWithOrderId("email@gmail.com" + CompositeKeySeparator + "wealthdeskUUIDString-localOrderId");
      orderKey.setOrderTimestamp(DateUtils.currentTimeInMillis());
      orderData.setLocalOrderId("20");
      orderData.setOrderExecutionMetaData(orderInstrument);
      orderData.setSide(OrderSide.BUY);
      orderData.setIntegerQuantity(22);
      orderData.setClientCode("clientCode");
      return orderData;
    }
  }

  static class SendOrder {
    
  }

  @Test
  public void test_qty_splits() {
    assertEquals(112, NumberUtils.getFirstHalf(225));
    assertEquals(113, NumberUtils.getSecondHalf(225));
    assertEquals(10, NumberUtils.getFirstHalf(21));
    assertEquals(11, NumberUtils.getSecondHalf(21));
    assertEquals(25, NumberUtils.getFirstHalf(50));
    assertEquals(25, NumberUtils.getSecondHalf(50));
  }

  @Test
  public void test_sending_buy_order_to_mock_ctcl() throws Exception {
    new TestKit(system) {
      {
        testActor = getRef();
        
        doReturn(new CancellablePriceStream<PriceWithPaf, NotUsed>(sourceOfPrices(), testActor, new EquityInstrument(ISIN, TOKEN, SYMBOL,"",SEGMENT)))
        .when(ctclBroadcast)
        .subscribeToLiveStream(Mockito.any(Instrument.class), Mockito.any(ActorMaterializer.class));
        
        doAnswer(invocation -> {
          CtclEquityOrder order = (CtclEquityOrder) invocation.getArguments()[0];
          
          CancellableStream<PriceWithPaf,NotUsed> liveStream = ctclBroadcast.subscribeToLiveStream(new EquityInstrument(ISIN, TOKEN, SYMBOL,"",SEGMENT), ActorMaterializer.create(system));
          
          liveStream
          .getSource()
          .runForeach(priceWithPaf -> liveStream.getActor().tell(priceWithPaf, ActorRef.noSender()), ActorMaterializer.create(system))
          .toCompletableFuture()
          .get();
          
          testActor.tell(new OrderReceivedWithBroker(Mockito.any(DestinationAddress.class),
              "wealthdeskUUIDString-localOrderId", "wealthdeskUUIDString-brokerOrderId"), ActorRef.noSender());
          testActor.tell(
              new OrderPlacedWithExchange(Mockito.any(DestinationAddress.class), "wealthdeskUUIDString-brokerOrderId",
                  "wealthdeskUUIDString-exchOrderId", "wealthdeskUUIDString-ctclOrderId", BUY, order.getIntegerQuantity()),
              ActorRef.noSender());
          testActor.tell(
              new TradeExecuted(Mockito.any(DestinationAddress.class), "wealthdeskUUIDString-exchOrderId",
                  "wealthdeskUUIDString-tradeOrderId-1", BUY,
                  new PriceWithPaf(new InstrumentPrice(102d, 0, TOKEN), Pafs.withCumulativePaf(1.)), 11),
              ActorRef.noSender());
          testActor.tell(
              new TradeExecuted(Mockito.any(DestinationAddress.class), "wealthdeskUUIDString-exchOrderId",
                  "wealthdeskUUIDString-tradeOrderId-2", BUY,
                  new PriceWithPaf(new InstrumentPrice(102.25d, 0, TOKEN), Pafs.withCumulativePaf(1.)), 11),
              ActorRef.noSender());
          return null;
        })
        .when(ctclInteractive)
        .sendMarketCashOrder(Mockito.any(CtclOrder.class));
        
        mock.tell(new SendOrder(), ActorRef.noSender());
        
        expectMsgClass(PriceWithPaf.class);
        expectMsgClass(PriceWithPaf.class);
        expectMsgClass(PriceWithPaf.class);
        expectMsgClass(OrderReceivedWithBroker.class);
        expectMsgClass(OrderPlacedWithExchange.class);
        expectMsgClass(TradeExecuted.class);
        expectMsgClass(TradeExecuted.class);
      }
    };
  }
    

  private Source<PriceWithPaf, NotUsed> sourceOfPrices() {
    PriceWithPaf price1 = new PriceWithPaf(new InstrumentPrice(103.0f, 0, TOKEN), Pafs.withCumulativePaf(1.));
    PriceWithPaf price2 = new PriceWithPaf(new InstrumentPrice(101.0f, 0, TOKEN), Pafs.withCumulativePaf(1.));
    PriceWithPaf price3 = new PriceWithPaf(new InstrumentPrice(100.0f, 0, TOKEN), Pafs.withCumulativePaf(1.));
    List<PriceWithPaf> prices = Arrays.asList(price1, price2, price3);
    return Source.from(prices);
  }
  
}
