package com.wt.corporateevents;

import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.ArrayList;
import java.util.List;

import org.approvaltests.Approvals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.domain.EquityInstrument;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.EquityCorporateEvent;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.http.javadsl.model.StatusCodes;

@RunWith(MockitoJUnitRunner.class)
public class BseCorporateEventShould {

  @SuppressWarnings("unchecked")
  @Test
  public void fetch_corporateevents_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    BseCorporateEventSource bseCorporateEventCource = new BseCorporateEventSource("file:///" + current + "/src/test/resources/Corporate_Actions.csv");
    ActorSystem system = ActorSystem.create("BSETest");
    Props props = Props.create(TestableEquityActor.class);
    ActorRef mock = system.actorOf(props);
    bseCorporateEventCource.notifyCorporateEventArrival(system.actorSelection(mock.path()));
    List<EquityCorporateEvent> askAndWaitForResult = (List<EquityCorporateEvent>) askAndWaitForResult(mock, new GetAll());
    final StringBuilder allCorporateEvents = new StringBuilder();
    for (EquityCorporateEvent corporateEvent : askAndWaitForResult) {
      allCorporateEvents.append(corporateEvent + "\n");
    }
    Approvals.verify(allCorporateEvents);
    system.terminate();
  }

  static class GetAll {
  }
  
  static class TestableEquityActor extends AbstractActor {
    private List<EquityCorporateEvent> corporateEvents = new ArrayList<>();
    String  invalidSymbol = "BSLFTPIYRQ";

    @Override
    public Receive createReceive() {
      return receiveBuilder().match(EquityCorporateEvent.class, cmd -> {
        corporateEvents.add(cmd);
        sender().tell(new Done(cmd.getWdId(), " corporate events received."), self());
      }).match(GetEquityInstrumentBySymbol.class, cmd -> {
        if(cmd.getSymbol().equals(invalidSymbol))
          sender().tell(failed(invalidSymbol, "", StatusCodes.BAD_REQUEST), self());
        else{
          sender().tell(new EquityInstrument("ISIN-BSE-EQ","",cmd.getSymbol(), "Instrument","EQ"), self());
        }}).match(GetAll.class, cmd -> {
        sender().tell(corporateEvents, self());
      }).build();
    }
  }

}
