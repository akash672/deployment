package com.wt.corporateevents;

import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.approvaltests.Approvals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.domain.CorporateEventType;
import com.wt.domain.EquityInstrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.events.BonusEvent;
import com.wt.domain.write.events.DividendEvent;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.EquityCorporateEvent;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.StockSplitEvent;
import com.wt.prices.NSEFileNameExtractor;
import com.wt.prices.NSEPriceSource;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.http.javadsl.model.StatusCodes;

@RunWith(MockitoJUnitRunner.class)
public class NseCorporateEventAdjustedPriceShould {
  @Mock private WDActorSelections selections;

  private ActorSystem system = ActorSystem.create("NSETest");
  private Props props = Props.create(TestableEquityActor.class);
  private ActorRef mock = system.actorOf(props);

  @Before
  public void setup() throws Exception {
    doReturn(system.actorSelection(mock.path())).when(selections).universeRootActor();
  }
  
  @SuppressWarnings({ "unused", "unchecked" })
  @Test
  public void fetch_price_adjusted_corporateevents_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    NseCorporateEventSource nseCorporateEventSource = new NseCorporateEventSource("file:///" + current + "/src/test/resources/CA_ALL_FORTHCOMING.csv");
    NSEPriceSource nsePrices = new NSEPriceSource(new NSEFileNameExtractor("file:///" + current + "/src/test/resources/"), selections);
    mock.tell(new PopulateIsinMap(), ActorRef.noSender());
    nseCorporateEventSource.notifyCorporateEventArrival(system.actorSelection(mock.path()));
	List<EquityCorporateEvent> askAndWaitForCorporateEventResult = (List<EquityCorporateEvent>) askAndWaitForResult(mock,new GetAllCorporateEvents());
    nsePrices.notifyPriceArrival(system.actorSelection(mock.path()), "08/08/2016");
	List<InstrumentPrice> askAndWaitForPriceResult = (List<InstrumentPrice>) askAndWaitForResult(mock, new GetAllPriceEvents());

    final StringBuilder allPriceEvents = new StringBuilder();
    for (InstrumentPrice priceEvent : askAndWaitForPriceResult) {
      allPriceEvents.append(priceEvent + "\n");
    }
    Approvals.verify(allPriceEvents);
    system.terminate();
  }

  static class GetAllCorporateEvents {
  }

  static class GetAllPriceEvents {
  }
  
  static class PopulateIsinMap {
  }

  static class TestableEquityActor extends AbstractActor {
    private PriceWithPaf last;
    private HashMap<String, EquityCorporateEvent> corporateEvents = new HashMap<>();
    private HashMap<String, Double> cumulativePafs = new HashMap<>();
    private List<InstrumentPrice> priceEvents = new ArrayList<>();
    private HashMap<String, String> symbolToIsinLookup = new HashMap<>();

    @Override
    public Receive createReceive() {
      return receiveBuilder().match(EquityCorporateEvent.class, cmd -> {
        if (validateCorporateEvent(cmd)) {
          corporateEvents.put(cmd.getWdId() + DateUtils.prettyDate(cmd.getExDate()) + cmd.getEventType().toString(), cmd);
          sender().tell(new Done(cmd.getWdId(), "corporate event received."), self());
        }
        else {
          sender().tell(failed(cmd.getWdId(), " invalid corporate event.", StatusCodes.BAD_REQUEST), self());
        }
      }).match(GetAllCorporateEvents.class, cmd -> {
        sender().tell(new ArrayList<EquityCorporateEvent>(corporateEvents.values()), self());
      }).match(GetEquityInstrumentBySymbol.class, cmd -> {
        if(symbolToIsinLookup.containsKey(cmd.getSymbol()))
          sender().tell(new EquityInstrument(symbolToIsinLookup.get(cmd.getSymbol()),"",cmd.getSymbol(), "Instrument",""), self());
        else
          sender().tell(new EquityInstrument("ISIN-NSE-EQ","",cmd.getSymbol(), "Instrument",""), self());
      }).match(GetAllPriceEvents.class, cmd -> {
        sender().tell(priceEvents, self());
      }).match(InstrumentPrice.class, cmd -> {
        Pafs adjustPaf = adjustPaf(cmd);
        cumulativePafs.put(DateUtils.prettyDate(cmd.getTime()), adjustPaf.getCumulativePaf());
        priceEvents.add(cmd);
        sender().tell(new Done(cmd.getWdId(), "price event received."), self());
      }).match(PopulateIsinMap.class, cmd -> {
        symbolToIsinLookup.put("20MICRONS", "INE144J01027-NSE-EQ");
        symbolToIsinLookup.put("3IINFOTECH", "INE748C01020-NSE-EQ");
        symbolToIsinLookup.put("3MINDIA", "INE470A01017-NSE-EQ");
        symbolToIsinLookup.put("8KMILES", "INE650K01013-NSE-EQ");
      }).build();
    }

    private boolean validateCorporateEvent(EquityCorporateEvent cmd) {
      if (corporateEvents.get(cmd.getWdId() + DateUtils.prettyDate(cmd.getExDate()) + cmd.getEventType().toString()) != null)
        return false;

      return true;
      
    }

    private Pafs adjustPaf(InstrumentPrice event) {
      double dividendValue = (last != null) ? last.getDividend() : 0.;
      double nonDividendPaf = (last != null) ? last.getNonDividendPaf() : 1.;
      double historicalPaf = (last != null) ? last.getHistoricalPaf() : 0.;
      double dividendPaf = 0.;
      double cumulativePaf = 0.;
      // Handle Bonus

      if (corporateEvents.get(event.getWdId() + DateUtils.prettyDate(event.getTime()) + CorporateEventType.BONUS.toString()) != null)
      {
        BonusEvent bonus = (BonusEvent) corporateEvents.get(event.getWdId() + DateUtils.prettyDate(event.getTime()) + CorporateEventType.BONUS.toString());
        nonDividendPaf *= (bonus.adjustPaf(last));
      }

      // Handle Stock Splits
      if (corporateEvents.get(event.getWdId() + DateUtils.prettyDate(event.getTime()) + CorporateEventType.SPLIT.toString()) != null)
      {
        StockSplitEvent stockSplit = (StockSplitEvent) corporateEvents.get(event.getWdId() + DateUtils.prettyDate(event.getTime()) + CorporateEventType.SPLIT.toString());
        nonDividendPaf *= (stockSplit.adjustPaf(last));
      }

      // Handle Dividends
      if (corporateEvents.get(event.getWdId() + DateUtils.prettyDate(event.getTime()) + CorporateEventType.DIVIDEND.toString()) != null)
      {
        DividendEvent dividend = (DividendEvent) corporateEvents.get(event.getWdId() + DateUtils.prettyDate(event.getTime()) + CorporateEventType.DIVIDEND.toString());
        dividendValue += dividend.getDividend();
      }
      if (dividendValue != 0.)
      {
        dividendPaf = ((event.getPrice() + dividendValue) / (event.getPrice()));
      }

      cumulativePaf = (historicalPaf != 0.) ? (historicalPaf * nonDividendPaf) : nonDividendPaf;
      cumulativePaf = (cumulativePaf != 1.) ? cumulativePaf : 0.;
      cumulativePaf += (dividendPaf);

      //Renormalizing the pafs
      if ((dividendPaf != 0.) && (nonDividendPaf != 1.) && (historicalPaf != 0.) && (historicalPaf != 1.)) {
        cumulativePaf -= 1.;
      }
      else if ((dividendPaf != 0.) && (nonDividendPaf != 1.)) {
        cumulativePaf -= 1.;
      }
      else if ((dividendPaf != 0.) && (historicalPaf != 0.) && (historicalPaf != 1.))
      {
        cumulativePaf -= 1.;
      }

      cumulativePaf = (cumulativePaf != 0.) ? cumulativePaf : 1.0;
      return new Pafs(cumulativePaf, nonDividendPaf, dividendValue, historicalPaf);
    }
  }
}
