package com.wt.corporateevents;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.ArrayList;
import java.util.List;

import org.approvaltests.Approvals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.domain.EquityInstrument;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.EquityCorporateEvent;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

@RunWith(MockitoJUnitRunner.class)
public class NseCorporateEventShould {

  @SuppressWarnings("unchecked")
  @Test
  public void fetch_corporateevents_from_url() throws Exception {
    String current = new java.io.File(".").getCanonicalPath();
    NseCorporateEventSource nseCorporateEventCource = new NseCorporateEventSource("file:///" + current + "/src/test/resources/CA_ALL_FORTHCOMING.csv");
    ActorSystem system = ActorSystem.create("NSETest");
    Props props = Props.create(TestableEquityActor.class);
    ActorRef mock = system.actorOf(props);
    nseCorporateEventCource.notifyCorporateEventArrival(system.actorSelection(mock.path()));
    List<EquityCorporateEvent> askAndWaitForResult = (List<EquityCorporateEvent>) askAndWaitForResult(mock, new GetAll());
    final StringBuilder allCorporateEvents = new StringBuilder();
    for (EquityCorporateEvent corporateEvent : askAndWaitForResult) {
      allCorporateEvents.append(corporateEvent + "\n");
    }
    Approvals.verify(allCorporateEvents);
    system.terminate();
  }

  static class GetAll {
  }

  static class TestableEquityActor extends AbstractActor {
    private List<EquityCorporateEvent> corporateEvents = new ArrayList<>();

    @Override
    public Receive createReceive() {
      return receiveBuilder().match(EquityCorporateEvent.class, cmd -> {
        corporateEvents.add(cmd);
        sender().tell(new Done(cmd.getWdId(), " corporate events received."), self());
      }).match(GetEquityInstrumentBySymbol.class, cmd -> {
        sender().tell(new EquityInstrument("ISIN-NSE-EQ","",cmd.getSymbol(), "Instrument",""), self());
      }).match(GetAll.class, cmd -> {
        sender().tell(corporateEvents, self());
      }).build();
    }
  }

}
