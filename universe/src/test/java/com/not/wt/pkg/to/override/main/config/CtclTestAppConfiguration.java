package com.not.wt.pkg.to.override.main.config;

import static com.google.common.collect.Lists.newArrayList;
import static com.typesafe.config.ConfigFactory.load;
import static com.wt.domain.BrokerName.BPWEALTH;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.mockito.Mockito;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import com.amazonaws.services.dynamodbv2.local.shared.access.AmazonDynamoDBLocal;
import com.amazonaws.services.sns.AmazonSNS;
import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.typesafe.config.Config;
import com.wt.benchmarks.BenchmarkSource;
import com.wt.benchmarks.NseBenchmarkSource;
import com.wt.broadcast.xneutrino.XNeutrinoBroadcast;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.CTCLMock;
import com.wt.ctcl.VirtualMutualFundInteractive;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerType;
import com.wt.instruments.BSEUniverseSource;
import com.wt.instruments.EquityUniverseSource;
import com.wt.instruments.MFUniverseSource;
import com.wt.instruments.NSEUniverseSource;
import com.wt.util.BroadcastManagerStub;
import com.wt.utils.JournalProvider;
import com.wt.utils.JournalProviderTest;
import com.wt.utils.akka.SpringExtension;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorSystem;

@Configuration
@ComponentScan("com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:test-environment.properties")
public class CtclTestAppConfiguration {
  @Autowired
  private ApplicationContext applicationContext;
  @Autowired
  private WDActorSelections actorSelections;
  @Autowired
  private WDProperties wdProperties;
  
  @Lazy
  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() {
    Config conf = load("test-application.conf");
    ActorSystem system = ActorSystem.create("Universe", conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }

  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("test-environment.properties"));
    return properties;
  }
  
  @Bean 
  public BroadCastManager broadCastManager() throws NumberFormatException, IOException {
    return new BroadcastManagerStub(properties().getProperty("xneutrino.broadcast.ip"),
        Integer.parseInt(properties().getProperty("xneutrino.broadcast.port")));
  }

  @Bean
  public CtclBroadcast ctclBroadcast() throws Exception {
      return new XNeutrinoBroadcast(broadCastManager(), actorSelections, wdProperties);
  }
  
  @Bean(name = "InteractiveManager")
  public InteractiveManager interactiveManager() throws IOException {
    return mock(InteractiveManager.class);
  }
  
  @Bean(name = "CTCLMock")
  public CtclInteractive mockCTCL() throws IOException {
    return new CTCLMock(actorSystem(), actorSelections);
  }
  
  @Bean
  public List<EquityUniverseSource> securitiesUniverseSources() {
    return newArrayList(nse());
  }

  @Bean
  public List<MFUniverseSource> mfUniverseSources() {
    return newArrayList(mf());
  }

  @Bean
  public EquityUniverseSource nse() {
    return new NSEUniverseSource("file://src/test/resources/security.txt");
  }

  @Bean
  public EquityUniverseSource bse() {
    return new BSEUniverseSource("file://src/test/resources/scrip.zip");
  }

  @Bean
  public MFUniverseSource mf() {
    return new MFUniverseSource("file://src/test/resources/","NAVAll.txt");
  }
  
  @Bean(name="JournalProvider")
  public JournalProvider journalProvider()
  {
    JournalProviderTest journalProvidermock = new JournalProviderTest(actorSystem());
    return journalProvidermock;
  }

  @Bean
  public List<BenchmarkSource> benchmarkSources() {
    return newArrayList(nseBenchmarks());
  }

  @Bean
  public BenchmarkSource nseBenchmarks() {
    return new NseBenchmarkSource("https://www.nseindia.com/content/indices/");
  }

  public AmazonDynamoDBLocal amazonDynamoDBLocal() {
    return DynamoDBEmbedded.create();
  }

  @Bean
  public AmazonDynamoDB amazonDynamoDB() {
    return amazonDynamoDBLocal().amazonDynamoDB();
  }
  
  @Bean(name = "dataSource")
  public DataSource dataSource() throws IOException {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    
    dataSource.setDriverClassName(wdProperties.rdsDriverClass());
    dataSource.setUsername(wdProperties.rdsUsername());
    dataSource.setPassword(wdProperties.rdsPassword());
    dataSource.setUrl(wdProperties.rdsUrl());
    return dataSource;
  }
  
  @Bean
  public Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation() throws IOException {
    Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation = new HashMap<BrokerType, MutualFundInteractive>();
    brokerTypeWithMFCTCLImplementation.put(new BrokerType(MUTUALFUNDSUBSCRIPTION, VIRTUAL, MOCK), new VirtualMutualFundInteractive(actorSelections));
    brokerTypeWithMFCTCLImplementation.put(new BrokerType(MUTUALFUNDSUBSCRIPTION, REAL, BPWEALTH), new VirtualMutualFundInteractive(actorSelections));
    return brokerTypeWithMFCTCLImplementation;
  }

  @Bean(name = "SNSClient")
  public AmazonSNS snsClientCreation() {
    return Mockito.mock(AmazonSNS.class);
  }
}
