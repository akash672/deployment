package com.wt.rds.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EquityInstrumentRDSRepo extends JpaRepository<EquityInstrumentRDS, String>{

}

