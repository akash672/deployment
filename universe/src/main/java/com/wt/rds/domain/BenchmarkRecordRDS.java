
package com.wt.rds.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
public class BenchmarkRecordRDS {

  @EmbeddedId
  private BenchmarkRecordRDSKey benchmarkRecordRDSKey;

  @Column(name = "open")
  private double open;

  @Column(name = "high")
  private double high;

  @Column(name = "low")
  private double low;

  @Column(name = "close")
  private double close;

  @Column(name = "changes")
  private double changes;

  public BenchmarkRecordRDS() {
  }

  public BenchmarkRecordRDS(BenchmarkRecordRDSKey benchmarkRecordRDSKey) {
    super();
    this.benchmarkRecordRDSKey = benchmarkRecordRDSKey;
  }

}