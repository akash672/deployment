
package com.wt.rds.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BenchmarkRecordRDSBatch {

  private List<BenchmarkRecordRDS> benchmarkRecordRDSBatch;

  public BenchmarkRecordRDSBatch() {
  }

  public BenchmarkRecordRDSBatch(List<BenchmarkRecordRDS> benchmarkRecordRDSBatch) {
    super();
    this.benchmarkRecordRDSBatch = benchmarkRecordRDSBatch;
  }

}