package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BenchmarkRecordRDSKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "fund")
  private String fund;

  @Column(name = "currentdate")
  private long currentdate;

  public BenchmarkRecordRDSKey(String fund, long currentdate) {
    super();
    this.fund = fund;
    this.currentdate = currentdate;
  }

  public BenchmarkRecordRDSKey() {
    super();
  }

}
