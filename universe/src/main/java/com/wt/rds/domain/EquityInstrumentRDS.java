package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "equityInstrument")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EquityInstrumentRDS implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Id
  private String wdId;
  
  @Column(name="symbol")
  private String symbol; 
}
