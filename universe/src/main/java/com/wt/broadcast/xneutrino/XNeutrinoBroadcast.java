
package com.wt.broadcast.xneutrino;

import static akka.actor.ActorRef.noSender;
import static com.wt.domain.PriceWithPaf.noPrice;
import static com.wt.utils.DateUtils.isBroadcastWindowOpen;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.reactivestreams.Publisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.ctcl.xneutrino.broadcast.MarketWatchResponse;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.Instrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Segment;
import com.wt.domain.write.RealTimeInstrumentPrice;
import com.wt.domain.write.commands.GetPrice;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.CancellableStream;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.japi.Pair;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Source;
import akka.stream.scaladsl.Sink;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Log4j
public class XNeutrinoBroadcast implements CtclBroadcast, Observer {

  private BroadCastManager broadcastManager;
  private Multimap<String, SubscriberWithLastPrice> subscribers = HashMultimap.create();
  private Multimap<String, SubscriberWithLastPrice> streamSubcribers = HashMultimap.create();
  private Map<Integer, Boolean> tokenToSubscriptionStatus = new ConcurrentHashMap<Integer, Boolean>();
  private WDActorSelections wdActorSelections;
  private long lastPriceTickTime;
  private long timeBenchmark = NANOSECONDS.convert(30, TimeUnit.SECONDS);
  private WDProperties wdProperties;

  public XNeutrinoBroadcast(BroadCastManager broadCastManager,
      WDActorSelections actorSelections, WDProperties wdProperties) {
    super();
    this.broadcastManager = broadCastManager;
    this.wdActorSelections = actorSelections;
    this.wdProperties = wdProperties;
  }

  private synchronized void startBroadcast() {
    try {
      lastPriceTickTime = System.nanoTime();
      log.info("Connecting to broadcast ");
      broadcastManager.connect();
      broadcastManager.addObserver(this);
      if(tokenToSubscriptionStatus.size() != 0){
        for (Entry<Integer, Boolean> entry : tokenToSubscriptionStatus.entrySet())
        {
            broadcastManager.subscribe(entry.getKey());
        }
      }
    } catch (Exception e) {
      log.info("Could not connect to broadcast socket - Error Details: " + e);
    }
  }

  private synchronized boolean connectIfNotConnected() {
    if (!broadcastManager.isConnected())
      startBroadcast();
    return broadcastManager.isConnected();
  }

  @Override
  public synchronized void update(Observable o, Object arg) {
    try {
      MarketWatchResponse response = (MarketWatchResponse) arg;
      String tokenString = getTokenString(response);
      lastPriceTickTime = System.nanoTime();
      notifySnapshotSubscribers(response, valueOf(response.getToken()), tokenString);

      InstrumentPrice price = new InstrumentPrice(round((double) response.getLTP()), DateUtils.currentTimeInMillis(),
          valueOf(response.getToken()));
      wdActorSelections.universeRootActor().tell(new RealTimeInstrumentPrice(price), ActorRef.noSender());

      for (SubscriberWithLastPrice subscriber : streamSubcribers.get(valueOf(response.getToken()))) {
        subscriber.getActor().tell(new PriceWithPaf(price, subscriber.getPrice().getPafs()), noSender());
      }

    } catch (Exception e) {
      log.error(this.getClass() + " Error while broadcast:", e);
    }
  }

  private synchronized void notifySnapshotSubscribers(MarketWatchResponse response, String token, String wdId)
      throws IOException {
    subscribers.get(token).stream().forEach(actor -> {
      PriceWithPaf price = new PriceWithPaf(new InstrumentPrice(response.getLTP(), 0, token),
          Pafs.withCumulativePaf(actor.getPrice().getCumulativePaf()));
      actor.getActor().tell(price, null);
    });
    subscribers.get(token).clear();
 //   removeSnapshotSubscriptions(token);
  }

//  private void removeSnapshotSubscriptions(String token) throws IOException {
//    if (subscribers.isEmpty() && streamSubcribers.isEmpty()) {
//      broadcastManager.unSubscribe(Integer.parseInt(token));
//    }
//  }

  @Override
  public synchronized CancellableStream<PriceWithPaf, NotUsed> subscribeToLiveStream(final Instrument instrument,
      Materializer mat) throws IOException {
    Pair<ActorRef, Publisher<Object>> sinkActorWithPublisher = subscribeToPrice(instrument, mat);

    streamSubcribers.put(instrument.getToken(),
        new SubscriberWithLastPrice(sinkActorWithPublisher.first(), noPrice(instrument.getWdId())));

    return new CancellablePriceStream<PriceWithPaf, NotUsed>(
        Source.fromPublisher(sinkActorWithPublisher.second()).map(f -> (PriceWithPaf) f),
        sinkActorWithPublisher.first(), instrument);
  }

  @Override
  public synchronized CancellableStream<PriceWithPaf, NotUsed> subscribeStream(final Instrument instrument,
      Materializer mat) throws IOException {
    Pair<ActorRef, Publisher<Object>> sinkActorWithPublisher = subscribeToPrice(instrument, mat);

    PriceWithPaf currentPrice = getEODPrice(instrument.getWdId());

    streamSubcribers.put(instrument.getToken(),
        new SubscriberWithLastPrice(sinkActorWithPublisher.first(), currentPrice));

    sinkActorWithPublisher.first().tell(currentPrice, noSender());

    return new CancellablePriceStream<PriceWithPaf, NotUsed>(
        Source.fromPublisher(sinkActorWithPublisher.second()).map(f -> (PriceWithPaf) f),
        sinkActorWithPublisher.first(), instrument);
  }

  @Override
  public void addSubscription(String token, String exchange) throws IOException {
    int intToken = Integer.parseInt(token);
    if (!tokenToSubscriptionStatus.containsKey(token))
      tokenToSubscriptionStatus.put(intToken, false);
    if (!tokenToSubscriptionStatus.get(intToken)) {
      try {
        if (isBroadcastWindowOpen() && connectIfNotConnected()){
          broadcastManager.subscribe(intToken);
          log.debug("subscribed to token " + intToken);
          tokenToSubscriptionStatus.put(intToken, true);
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void removeSubscription(String token, String exchange) throws NumberFormatException, IOException {
    int intToken = Integer.parseInt(token);
    if (tokenToSubscriptionStatus.containsKey(token)) {
      log.debug("Unsubscribing from token " + token);
      broadcastManager.unSubscribe(intToken);
      tokenToSubscriptionStatus.remove(token);
    }
  }

  private Pair<ActorRef, Publisher<Object>> subscribeToPrice(final Instrument instrument, Materializer mat)
      throws IOException {

    String token = instrument.getToken();
    String exchange = instrument.getWdId().split("-")[1];

    Pair<ActorRef, Publisher<Object>> sinkActorWithPublisher = Source.actorRef(1, OverflowStrategy.dropHead())
        .toMat(Sink.asPublisher(true), Keep.both()).run(mat);


    addSubscription(token, exchange);
    return sinkActorWithPublisher;
  }

  @Override
  public synchronized void unSubscribeStream(final Instrument instrument, ActorRef actor) throws IOException {
    PriceWithPaf currentPrice = getEODPrice(instrument.getWdId());
    streamSubcribers.remove(instrument.getToken(), new SubscriberWithLastPrice(actor, currentPrice));
    removeSubscriptionFrom(instrument);
  }

  @Override
  public synchronized void unSubscribeLiveStream(final Instrument instrument, ActorRef actor) throws IOException {
    streamSubcribers.remove(instrument.getToken(), new SubscriberWithLastPrice(actor, noPrice(instrument.getWdId())));
    removeSubscriptionFrom(instrument);
  }

  private void removeSubscriptionFrom(final Instrument instrument) throws IOException {
    
  }
  
  @Override
  public synchronized void snapshot(Instrument instrument, ActorRef sender) {
    boolean isConnected = connectIfNotConnected();

    if (sender != null) {
      try {
        PriceWithPaf currentPrice = getEODPrice(instrument.getWdId());
        if (!isConnected) {
          sender.tell(currentPrice, noSender());
          return;
        }
        subscribers.put(instrument.getToken(), new SubscriberWithLastPrice(sender, currentPrice));
        broadcastManager.subscribe(parseInt(instrument.getToken()));
      } catch (IOException e) {
        log.error("Unable to Subscribe token " + instrument + " - Error Details: " + e.getMessage());
      }
    }
  }

  public static String getTokenString(MarketWatchResponse response) {
    return response.getToken() + "-" + Segment.getStringValue(response.getSegment());
  }

  private synchronized PriceWithPaf getEODPrice(String token) {
    try {
      return (PriceWithPaf) askAndWaitForResult(wdActorSelections.universeRootActor(), new GetPrice(token));
    } catch (Exception e) {
      log.error("Error while getting PAF for token " + token + ". Using " + PriceWithPaf.noPrice(token), e);
      return PriceWithPaf.noPrice(token);
    }
  }

  @Async
  @Scheduled(cron = "${ctcl.schedule}", zone = "Asia/Kolkata")
  public void lastPriceCheck() throws Exception {
    if (DateUtils.isTradingHoliday()) 
      return;
    if ( !wdProperties.scheduleTicks()) {
      return;
    }
    if(System.nanoTime() > (lastPriceTickTime + timeBenchmark))
      startBroadcast();
  }

  @AllArgsConstructor
  @Getter
  @EqualsAndHashCode(of = "actor")
  @ToString
  public
  static class SubscriberWithLastPrice {
    private ActorRef actor;
    private PriceWithPaf price;
  }

}
