package com.wt.broadcast.xneutrino;

import com.wt.domain.Instrument;
import com.wt.utils.akka.CancellableStream;

import akka.actor.ActorRef;
import akka.stream.javadsl.Source;
import lombok.Getter;

@Getter
public class CancellablePriceStream<Out, Mat> extends CancellableStream<Out, Mat>  {
  private final Instrument token;

  public CancellablePriceStream(Source<Out, Mat> source, ActorRef actor, Instrument token) {
    super(source, actor);
    this.token = token;
  }
  
  public void cancel() {
    super.cancel();
    }
  }

