package com.wt.broadcast.mosl;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.DateUtils.isBroadcastWindowOpen;
import static com.wt.utils.DoublesUtil.round;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.wt.config.utils.WDProperties;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.Instrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.RealTimeInstrumentPrice;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.CancellableStream;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.stream.Materializer;
import lombok.extern.log4j.Log4j;

@Log4j
public class MoslBroadcast implements CtclBroadcast {
  private Map<String, Boolean> tokenToSubscriptionStatus = new ConcurrentHashMap<String, Boolean>();
  private WDActorSelections wdActorSelections;
  private long lastTickTimeFromWebSocket;
  private long timeWithoutPriceTick = NANOSECONDS.convert(1, TimeUnit.MINUTES);
  private WDProperties properties;
  private SimpleSocket socket;
  private WebSocketClient client;
  private ClientUpgradeRequest request;

  public MoslBroadcast( WDActorSelections actorSelections, WDProperties properties) {
    super();
    this.wdActorSelections = actorSelections;
    this.properties = properties;
    this.socket = new SimpleSocket(this);
    this.request = new ClientUpgradeRequest();
  }

  public synchronized void startBroadcast() {
    try {
      log.info("Connecting to broadcast ");
      connect();
      if (tokenToSubscriptionStatus.size() != 0) {
        for (Entry<String, Boolean> entry : tokenToSubscriptionStatus.entrySet()) {
          String[] parts = entry.getKey().split(CompositeKeySeparator);
          int intToken = Integer.parseInt(parts[0]);
          String exchange = parts[1];
          subscribe(intToken, exchange);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.info("Could not connect to broadcast socket - Error Details: " + e);
    }
  }

  private void connect() {
    SslContextFactory sslContextFactory = new SslContextFactory();
    sslContextFactory.setTrustAll(true);
    client = new WebSocketClient(sslContextFactory);
    client.setMaxIdleTimeout(1800000);
    client.setConnectTimeout(1800000);
    try {
      client.start();
      URI echoUri = new URI(properties.moslBroadcastURL());
      client.connect(socket, echoUri, request);
      waitForSession();
    } catch (Throwable t) {
      log.error(t);
      t.printStackTrace();
    }

  }
  
  private void waitForSession() {
    for(int i=0; i<5; i++){
      if(socket.getSession() != null)
        break;
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    
  }

  public void subscribe(int intToken, String exchange) {
    char charExchange = exchange.charAt(0);
    try {
      socket.sendrequest(intToken, charExchange);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void addSubscription(String token, String exchange) throws IOException {
    String tokenWithExchange = token + CompositeKeySeparator + exchange;
    int intToken = Integer.parseInt(token);
    if (!tokenToSubscriptionStatus.containsKey(tokenWithExchange))
      tokenToSubscriptionStatus.put(tokenWithExchange, false);
    if (!tokenToSubscriptionStatus.get(tokenWithExchange)) {
      try {
        if (isBroadcastWindowOpen() && connectIfNotConnected()) {
          subscribe(intToken, exchange);
          log.debug("subscribed to tokenWithExchange " + tokenWithExchange);
          tokenToSubscriptionStatus.put(tokenWithExchange, true);
        }
      }catch(NullPointerException e) {
        log.debug("null pointer while subscribing due to connection is closed");
        startBroadcast();
      }
      catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }

  private boolean connectIfNotConnected() {
    if (socket.getSession() != null) {
      if (!socket.getSession().isOpen())
        startBroadcast();
      return socket.getSession().isOpen();
    } else {
      startBroadcast();
      return false;
    }
  }

  public void update(RealTimePriceWithToken realpricewithWdId) {
    lastTickTimeFromWebSocket = System.nanoTime();
    InstrumentPrice price = new InstrumentPrice(round((double) realpricewithWdId.getLTP()),
        DateUtils.currentTimeInMillis(), realpricewithWdId.getToken());
    wdActorSelections.universeRootActor().tell(new RealTimeInstrumentPrice(price), ActorRef.noSender());
  }

  @Override
  public void removeSubscription(String token, String exchange) throws IOException {
    String tokenWithExchange = token + CompositeKeySeparator + exchange;
    if (tokenToSubscriptionStatus.containsKey(tokenWithExchange)) {
      log.debug("Unsubscribing from token " + tokenWithExchange);
      tokenToSubscriptionStatus.remove(tokenWithExchange);
    }

  }

  @Async
  @Scheduled(cron = "${ctcl.schedule}", zone = "Asia/Kolkata")
  public void lastPriceCheck() throws Exception {
    if (DateUtils.isTradingHoliday())
      return;
    if (!properties.scheduleTicks()) {
      return;
    }
    if(DateUtils.currentTimeInMillis() > (DateUtils.marketCloseTime())) {
      if(socket.getSession() != null) {
        log.info("connection closed");
        socket.getSession().close();
      }
      return;
    }

    if(DateUtils.currentTimeInMillis() >= (DateUtils.marketOpenTime()) && System.nanoTime() > (lastTickTimeFromWebSocket + timeWithoutPriceTick)) {
      log.error("Prices are not updating, so closing the connection and reconnecting");
      if( socket.getSession() != null) {
        socket.getSession().close();
        startBroadcast();
      } else{
        startBroadcast();
      }
      return;
    }
    if(DateUtils.currentTimeInMillis() >= DateUtils.todayBOD()) {
      if( socket.getSession() == null) {
        startBroadcast();
      }
    }
  }

  private void sendHeartbeat() {
    socket.sendHeartbeatRequest();
  }

  public void receivedHeartbeat() {
    log.info("Received Heartbeat");
    lastTickTimeFromWebSocket = System.nanoTime();
    sendHeartbeat();
  }

  @Override
  public CancellableStream<PriceWithPaf, NotUsed> subscribeStream(Instrument token, Materializer mat)
      throws IOException {
    return null;
  }

  @Override
  public CancellableStream<PriceWithPaf, NotUsed> subscribeToLiveStream(Instrument token, Materializer mat)
      throws IOException {
    return null;
  }

  @Override
  public void unSubscribeStream(Instrument token, ActorRef actor) throws IOException {

  }

  @Override
  public void unSubscribeLiveStream(Instrument token, ActorRef actor) throws IOException {
  }

  @Override
  public void snapshot(Instrument token, ActorRef sender) {

  }

}
