package com.wt.broadcast.mosl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import com.wt.utils.ByteConverter;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
@Getter
@WebSocket
public class SimpleSocket {
  private final CountDownLatch closeLatch;
  private Session session;
  private MoslBroadcast moslBroadcast;

  public SimpleSocket(MoslBroadcast moslBroadcast) {
    this.closeLatch = new CountDownLatch(1);
    this.moslBroadcast = moslBroadcast;
  }

  public boolean awaitClose(int duration, TimeUnit unit) throws InterruptedException {
    return this.closeLatch.await(duration, unit);
  }

  @OnWebSocketClose
  public void onClose(int statusCode, String reason) {
    log.info("Connection closed " +reason);
    this.session = null;
    this.closeLatch.countDown(); 
  }

  @OnWebSocketMessage
  public void onMessage(byte[] bytes, int x, int y) {
    int length = bytes.length / 30;
    for (int i = 0; i < length; i++) {
      byte[] ray = new byte[30];
      System.arraycopy(bytes, 30 * i, ray, 0, 30);
      try {
        char exchange = (char) ray[0];
        int scripCode = ByteConverter.getInt32(ray, 1);
        int time = ByteConverter.getInt32(ray, 5);
        char numericType = (char) ray[9];
        float rate = ByteConverter.getFloat32(ray, 10);
        String empty = ByteConverter.getString(ray, 14, 16);
         if (numericType == 'A') {
           String wdId = String.valueOf(scripCode);
           moslBroadcast.update(new RealTimePriceWithToken(wdId, rate, time));
         }
       if(numericType == '1'){
         moslBroadcast.receivedHeartbeat();
       }
       
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @OnWebSocketConnect
  public void onConnect(Session session) {
    this.session = session;
    try {
      session.getRemote().sendBytesByFuture(getData());
      log.info("session created" + session);
    } catch (Throwable t) {
      log.error("error logging in" + t);
      t.printStackTrace();
    }
  }

  @OnWebSocketError
  public void onError(Throwable msg) {
    log.error("Error in connection " + msg);
  }
  
  private ByteBuffer getData() throws UnsupportedEncodingException {
    byte[] struct = new byte[114];
    String id = "T0102315";
    String version = "VER 2.0";
    String pad_id = pad(id, 15, " ", 2);
    String name_pad = pad(id, 30, " ", 2);
    String ver_pad = pad(version, 10, " ", 2);
    struct[0] = (byte) 'Q';
    ByteConverter.setShort((short) 111, struct, 1);
    struct[3] = (byte) id.length();
    ByteConverter.setString(pad_id, 15, struct, 4);
    struct[19] = (byte) id.length();
    ByteConverter.setString(name_pad, 30, struct, 20);
    struct[50] = (byte) 1;
    struct[51] = (byte) 1;
    struct[52] = (byte) 1;
    struct[53] = (byte) version.length();
    ByteConverter.setString(ver_pad, 10, struct, 54);
    struct[64] = (byte) 0;
    struct[65] = (byte) 0;
    struct[66] = (byte) 0;
    struct[67] = (byte) 0;
    struct[68] = (byte) 1;
    for (int i = 0; i < 45; i++) {
      struct[69 + i] = (byte) 0;
    }

    return ByteBuffer.wrap(struct);
  }

  public String pad(String str, int len, String pad, int dir) {
    if (len + 1 >= str.length()) {
      int padLen;
      int right = (int) Math.ceil(padLen = (len - str.length()) / 2);
      int left = padLen - right;
      str = paddedString(left + 1, pad) + str + paddedString(right + 1, pad);
    }
    return str;
  }

  private String paddedString(int len, String pad) {
    String paddedString = "";
    for (int i = 0; i < len; i++)
      paddedString += pad;
    return paddedString;
  }

  public void sendrequest(int token, char exchange) throws IOException {
    String data = exchange + "," + "C" + "," + token + "," + "1"; 
     sendToMoslServer(data);
  }

  private void sendToMoslServer(String data) throws IOException {
    byte[] struct = new byte[10];
    if (data != null) {
      String[] arrayData = data.split(",");
      struct[0] = (byte) 'D';
      short a = 7;
      ByteConverter.setShort(a, struct, 1);
      struct[3] = (byte) (arrayData[0].charAt(0));
      struct[4] = (byte) (arrayData[1].charAt(0));
      int token = Integer.parseInt(arrayData[2]);
      ByteConverter.setInt(token, struct, 5);
      struct[9] = Byte.parseByte(arrayData[3]);
    }
    if (session != null)
      session.getRemote().sendBytesByFuture(ByteBuffer.wrap(struct));

  }
  
  public void sendHeartbeatRequest(){
    byte[] struct = new byte[3];
    struct[0] = (byte)'1';
    ByteConverter.setShort((short)0, struct, 1);
    if (session != null)
      session.getRemote().sendBytesByFuture(ByteBuffer.wrap(struct));
  }
}