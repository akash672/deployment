package com.wt.broadcast.mosl;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class RealTimePriceWithToken implements Serializable{
  
  private static final long serialVersionUID = 1L;
  private String token;
  private float lTP;
  private long time;

}
