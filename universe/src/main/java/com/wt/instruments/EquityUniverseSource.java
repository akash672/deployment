package com.wt.instruments;

import com.wt.domain.EquityInstrument;

import rx.Observable;

public abstract class EquityUniverseSource {
  public static final String CSV_SPLIT_BY = "\\|";

  public abstract Observable<EquityInstrument> equityInstruments();
}
