package com.wt.instruments;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import com.wt.domain.MutualFundInstrument;

import lombok.ToString;
import lombok.extern.log4j.Log4j;
import rx.Observable;
import rx.Subscriber;

@Log4j
@ToString
public class MFUniverseSource {

  String fileName = "";
  private URL mfSecurities;
  private String TXT_SPLIT_BY = "\\|";
  private String DIRECT = "direct";
  private int retryCounter;

  public MFUniverseSource(String filePathUrl, String fileName) {
    try {
      this.mfSecurities = new URL(filePathUrl + fileName);
      this.fileName = fileName;
    } catch (MalformedURLException e) {
      log.error(e);
      throw new RuntimeException(e);
    }
  }

  public Observable<MutualFundInstrument> instruments() {
    retryCounter = 1;
    return Observable.create(subscribeFunction());
  }

  private Observable.OnSubscribe<MutualFundInstrument> subscribeFunction() {
    return (subscriber) -> {
      File docDestFile = getMutualFundFile(fileName, mfSecurities);
      log.info("File Downloaded from : " + mfSecurities);
      try (BufferedReader br = new BufferedReader(new FileReader(docDestFile))) {
        log.info("Reading file: " + fileName);
        Stream<String> lines = br.lines();
        lines.forEach(line -> {
          String[] columns = line.split(TXT_SPLIT_BY);
          if (isValidMfSeries(columns)) {
              MutualFundInstrument instrument;
              instrument = new MutualFundInstrument(columns[4], "MF", columns[8],  columns[1], columns[11], columns[14], columns[6], columns[38],columns[25]);
              notify(subscriber, instrument);
            }
          
        });
      } catch (IOException e) {
        if (!subscriber.isUnsubscribed()) {
          subscriber.onError(e);
        }
      } finally {
        docDestFile.delete();
      }
      subscriber.onCompleted();
    };
  }

  private File getMutualFundFile(String fileName, URL priceLink) {
    Path targetPath = new File(fileName).toPath();
    log.info("Downloading file from : " + priceLink);
    try (InputStream in = priceLink.openStream()) {
      Files.copy(in, targetPath, StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
      if (retryCounter <= 10) {
        retryCounter++;
        getMutualFundFile(fileName, priceLink);
      } else
        log.error(e);
    }
    File docDestFile = targetPath.toFile();
    return docDestFile;
  }

 
  private boolean isValidMfSeries(String[] columns) {
    if (columns.length >1 && columns[16].equals("Y") && columns[9].equals("Y") && columns[7].equals("DIRECT") && columns[10].equals("DP") 
        && !columns[1].contains("L1")){
      return true;
    }  
    else
      return false;
  }
  
  /*
  column 9 - Purchase Allowed ; column 16 - Redemption Allowed ; column 7 - Scheme Type Direct/Normal 
  column 10 - Transaction mode DP/Physical 
  */
  
  private void notify(Subscriber<? super MutualFundInstrument> subscriber, MutualFundInstrument instrument) {
    if (!subscriber.isUnsubscribed()) {
      subscriber.onNext(instrument);
    }
  }

}