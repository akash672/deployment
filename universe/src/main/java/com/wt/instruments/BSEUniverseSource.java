package com.wt.instruments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.wt.domain.EquityInstrument;

import lombok.ToString;
import lombok.extern.log4j.Log4j;
import rx.Observable;
import rx.Subscriber;
@Log4j
@ToString
public class BSEUniverseSource extends EquityUniverseSource {

  private URL bseSecurities;

  public BSEUniverseSource(String url) {
    try {
      this.bseSecurities = new URL(url);
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Observable<EquityInstrument> equityInstruments() {
    return Observable.create(subscriberFunction());
  }

  private Observable.OnSubscribe<EquityInstrument> subscriberFunction() {
    return (subscriber) -> {
      try (ZipInputStream in = new ZipInputStream(bseSecurities.openStream())) {
        ZipEntry entry;
        
        while ((entry = in.getNextEntry()) != null) {
          if (isUniverseFile(entry))
            extractInstruments(subscriber, in);

          in.closeEntry();
        }
      } catch (Exception e) {
        log.error(e);
        if (!subscriber.isUnsubscribed()) {
          subscriber.onError(e);
        }
      }
      subscriber.onCompleted();
    };
  }

  private boolean isUniverseFile(ZipEntry entry) {
    return entry.getName().matches("SCRIP/SCRIP_[0-9]+.TXT");
  }

  private void extractInstruments(Subscriber<? super EquityInstrument> subscriber, ZipInputStream in) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    String line;
    while ((line = reader.readLine()) != null) {
      String[] column = line.split(CSV_SPLIT_BY);
      if (isEquity(column)) {
        EquityInstrument equityInstrument = new EquityInstrument(column[17]+"-BSE-EQ", column[0], column[2],  column[3],"EQ");
        notify(subscriber, equityInstrument);
      }

    }
  }

  private boolean isEquity(String[] column) {
    return "90".equals(column[18]);
  }

  private void notify(Subscriber<? super EquityInstrument> subscriber, EquityInstrument equityInstrument) {
    if (!subscriber.isUnsubscribed()) {
      subscriber.onNext(equityInstrument);
    }
  }

}