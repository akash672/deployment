package com.wt.instruments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import com.wt.domain.EquityInstrument;

import lombok.ToString;
import rx.Observable;
import rx.Subscriber;

@ToString
public class NSEUniverseSource extends EquityUniverseSource {

  private URL nseSecurities;

  public NSEUniverseSource(String url) {
    try {
      this.nseSecurities = new URL(url);
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Observable<EquityInstrument> equityInstruments() {

    return Observable.create(subscribeFunction());
  }

  private Observable.OnSubscribe<EquityInstrument> subscribeFunction() {
    return (subscriber) -> {

      try (GZIPInputStream in = new GZIPInputStream(nseSecurities.openStream())) {

        extractInstruments(subscriber, in);

        in.close();

      } catch (IOException e) {
        if (!subscriber.isUnsubscribed()) {
          subscriber.onError(e);
        }
      }
      subscriber.onCompleted();
    };
  }

  private void extractInstruments(Subscriber<? super EquityInstrument> subscriber, GZIPInputStream in) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    String line;
    while ((line = reader.readLine()) != null) {
      String[] columns = line.split(CSV_SPLIT_BY);
      if (isActiveEquity(columns)) {
        EquityInstrument equityInstrument = new EquityInstrument(columns[53]+"-NSE-EQ", columns[0], columns[1], columns[21], columns[2]);
        notify(subscriber, equityInstrument);
      }

    }
  }

  private boolean isActiveEquity(String[] columns) {
    return ("EQ".equals(columns[2]) || "BE".equals(columns[2]) && "1".equals(columns[8]));
  }

  private void notify(Subscriber<? super EquityInstrument> subscriber, EquityInstrument equityInstrument) {
    if (!subscriber.isUnsubscribed()) {
      subscriber.onNext(equityInstrument);
    }
  }

}
