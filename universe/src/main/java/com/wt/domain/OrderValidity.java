package com.wt.domain;

public enum OrderValidity {
  GTC, GFD, GTD, IOC
}
