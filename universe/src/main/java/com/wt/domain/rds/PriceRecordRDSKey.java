package com.wt.domain.rds;

import java.io.Serializable;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceRecordRDSKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "ticker")
  private String ticker;

  @Column(name = "price_time")
  private long price_time;

  public PriceRecordRDSKey(String ticker, long price_time) {
    super();
    this.ticker = ticker;
    this.price_time = price_time;
  }

  public PriceRecordRDSKey() {
    super();
  }

}
