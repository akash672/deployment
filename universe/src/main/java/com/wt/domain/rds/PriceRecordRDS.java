
package com.wt.domain.rds;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
public class PriceRecordRDS {

  @EmbeddedId
  private PriceRecordRDSKey priceRecordRDSKey;
  
  @Column(name="time")
  private long time;
  
  @Column(name="unadjusted_price")
  private double unadjusted_price;

  @Column(name="adjusted_price")
  private double adjusted_price;
  
  @Column(name="cumulative_paf")
  private double cumulative_paf;

  @Column(name="dividend")
  private double dividend;

  @Column(name="historical_paf")
  private double historical_paf;
  
  @Column(name="nondividend_paf")
  private double nondividend_paf;

  public PriceRecordRDS() {
  }

  public PriceRecordRDS(PriceRecordRDSKey priceRecordRDSKey) {
    super();
    this.priceRecordRDSKey = priceRecordRDSKey;
  }

}