
package com.wt.domain.rds;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceRecordRDSBatch implements Serializable {

  private static final long serialVersionUID = 1L;
  private List<PriceRecordRDS> priceRecordRDSBatch;

  public PriceRecordRDSBatch() {
  }

  public PriceRecordRDSBatch(List<PriceRecordRDS> priceRecordRDSBatch) {
    super();
    this.priceRecordRDSBatch = priceRecordRDSBatch;
  }

}