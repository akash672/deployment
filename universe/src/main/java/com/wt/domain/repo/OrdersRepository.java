package com.wt.domain.repo;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wt.domain.Order;
import com.wt.domain.OrderKey;

@EnableScan
public interface OrdersRepository extends CrudRepository<Order, OrderKey> {
  public List<Order> findByTickerAndOrderTimestampGreaterThan(@Param("ticker") String ticker,
      @Param("orderTimestamp") long orderTimestamp);

}
