package com.wt.domain.repo;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.Benchmark;
import com.wt.domain.BenchmarkPerformanceKey;

@EnableScan
public interface BenchmarkRepository extends CrudRepository<Benchmark, BenchmarkPerformanceKey> {

  public List<Benchmark> findByFund(String fund);

}