
package com.wt.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "Benchmark")
@Entity
@NoArgsConstructor
@DynamoDBTable(tableName = "Benchmark")
@ToString
public class Benchmark implements Serializable {

  private static final long serialVersionUID = 1L;

  @org.springframework.data.annotation.Id
  @DynamoDBIgnore
  private BenchmarkPerformanceKey fundPerformanceKey = new BenchmarkPerformanceKey();

  @Getter
  @Setter
  @DynamoDBAttribute
  private double open;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double high;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double low;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double closing;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double change;

  public Benchmark(String fund, long currentTime, double open, double high,
      double low, double closing, double change) {
    super();
    fundPerformanceKey.setFund(fund);
    fundPerformanceKey.setDate(currentTime);
    this.open = open;
    this.high = high;
    this.low = low;
    this.closing = closing;
    this.change = change;
  }

  @DynamoDBHashKey(attributeName = "fund")
  public String getFund() {
    return fundPerformanceKey.getFund();
  }

  public void setFund(String fund) {
    fundPerformanceKey.setFund(fund);
  }

  @DynamoDBRangeKey(attributeName = "date")
  public long getDate() {
    return fundPerformanceKey.getDate();
  }

  public void setDate(long date) {
    fundPerformanceKey.setDate(date);
  }

}