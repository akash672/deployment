package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class OrderKey implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @DynamoDBHashKey(attributeName = "usernameWithOrderId")
  private String usernameWithOrderId;
  
  @DynamoDBRangeKey(attributeName = "orderTimestamp")
  private long orderTimestamp;
  
  public String getCognitoIdentityId()
  {
    return usernameWithOrderId.split(CompositeKeySeparator)[0];
  }
  
  public InvestingMode getInvestingMode()
  {
    return InvestingMode.valueOf(usernameWithOrderId.split(CompositeKeySeparator)[1]);
  }
  
  public String getOrderId()
  {
    return usernameWithOrderId.split(CompositeKeySeparator)[3];
  }
  
}
