package com.wt.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;

public class MutualFundInstrumentMarshaller implements DynamoDBTypeConverter<String, MutualFundInstrument> {

  private Gson gson = new Gson();

  @Override
  public String convert(MutualFundInstrument object) {
    return gson.toJson(object);
  }

  @Override
  public MutualFundInstrument unconvert(String object) {
    return gson.fromJson(object, MutualFundInstrument.class);
  }

}
