package com.wt.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;

public class InstrumentMarshaller implements DynamoDBTypeConverter<String, Instrument> {

  private Gson gson = new Gson();

  @Override
  public String convert(Instrument object) {
    return gson.toJson(object);
  }

  @Override
  public Instrument unconvert(String object) {
    return gson.fromJson(object, Instrument.class);
  }

}
