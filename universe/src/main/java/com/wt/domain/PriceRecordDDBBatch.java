
package com.wt.domain;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PriceRecordDDBBatch implements Serializable {

  private static final long serialVersionUID = 1L;
  private List<PriceWithPaf> priceRecordDDBBatch;
}