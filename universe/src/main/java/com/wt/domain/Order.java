package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.NumberUtils.averagingWeighted;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import com.wt.domain.write.events.AdviseOrderCreated;
import com.wt.domain.write.events.EventWithTimeStamps;
import com.wt.domain.write.events.FlushIssueAdviseTrades;
import com.wt.domain.write.events.OrderPlacedWithExchange;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.TradeExecuted;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@DynamoDBTable(tableName = "Order")
public class Order implements Serializable, Comparable<Order> {
  private static final long serialVersionUID = 1L;
  @Id
  @DynamoDBIgnore
  private OrderKey orderKey = new OrderKey();
  @DynamoDBAttribute
  private String orderId;
  @DynamoDBAttribute
  private String brokerOrderId;
  @DynamoDBIgnore
  private String ctclId;
  @DynamoDBAttribute
  private String exchangeOrderId;
  @DynamoDBAttribute
  private String ticker;
  @DynamoDBAttribute
  private String symbol;
  @DynamoDBAttribute
  @DynamoDBTypeConvertedEnum
  private OrderSide side;
  @DynamoDBAttribute
  private String exchange;
  @DynamoDBAttribute
  private double quantity;
  @DynamoDBAttribute
  private String clientCode;
  @DynamoDBAttribute
  private String fundName;
  @DynamoDBAttribute
  private List<Trade> trades  = new CopyOnWriteArrayList<>();
  @DynamoDBAttribute
  private double totalTradedQuantity;
  @DynamoDBAttribute
  private double averageTradedPrice;
  @DynamoDBAttribute
  private long lastTradedTime;
  @DynamoDBIgnore
  private Purpose purpose;
  @DynamoDBIgnore
  private double price ;

  public void update(EventWithTimeStamps event) {
    if (event instanceof AdviseOrderCreated) {
      AdviseOrderCreated orderDetails = (AdviseOrderCreated) event;
      orderKey.setUsernameWithOrderId(
          orderDetails.getUsername() + CompositeKeySeparator + orderDetails.getInvestingMode().name()
              + CompositeKeySeparator + orderDetails.getLocalOrderId());
      orderKey.setOrderTimestamp(orderDetails.getOrderTime());
      this.orderId = orderDetails.getLocalOrderId();
      this.side = orderDetails.getSide();
      this.quantity = orderDetails.getNumberOfShares();
      this.ticker = orderDetails.getWdId();
      this.symbol = orderDetails.getSymbol();
      this.exchange = orderDetails.getWdId().split("-")[1];
      this.clientCode = orderDetails.getClientCode();
      this.purpose = orderDetails.getPurpose();
      this.price = orderDetails.getPrice();
      this.fundName = orderDetails.getFundName();
    } else if (event instanceof OrderReceivedWithBroker) {
      OrderReceivedWithBroker orderReceivedWithBroker = (OrderReceivedWithBroker) event;
      this.brokerOrderId = orderReceivedWithBroker.getBrokerOrderId(); 
    } else if (event instanceof OrderPlacedWithExchange) {
      OrderPlacedWithExchange orderPlacedWithExchange = (OrderPlacedWithExchange) event;
      this.ctclId = orderPlacedWithExchange.getCtclId();
      this.exchangeOrderId = orderPlacedWithExchange.getExchangeOrderId();
    } else if (event instanceof TradeExecuted) {
      TradeExecuted tradeExecuted = (TradeExecuted) event;
      this.exchangeOrderId = tradeExecuted.getExchangeOrderId();
      if(isMutualFundOrder())
    	  this.quantity = tradeExecuted.getQuantity();
      Trade trade = new Trade(tradeExecuted.getPrice().getPrice().getPrice(), tradeExecuted.getOrderSide(), tradeExecuted.getQuantity(),
          tradeExecuted.getExchangeOrderId(), tradeExecuted.getTradeOrderId());
      if(trades.contains(trade)) {
        trades.remove(trade);
      }
      trades.add(trade);
      totalTradedQuantity = trades.stream().mapToDouble(tradeObj -> tradeObj.getQuantity()).sum();
      if(trade.getIntegerQuantity() !=0 && !isMutualFundOrder())
        averageTradedPrice = calculateEquityAverageTradedPrice();
      else if(trade.getQuantity() !=0 && isMutualFundOrder())
        averageTradedPrice = calculateMFAverageTradedPrice();
      lastTradedTime = tradeExecuted.getLastTradedTime();
    } else if (event instanceof FlushIssueAdviseTrades) {
      this.trades = new CopyOnWriteArrayList<>();
      this.totalTradedQuantity = 0;
      this.averageTradedPrice = 0;
      this.quantity = 0;
    }
  }

  @DynamoDBHashKey(attributeName = "usernameWithOrderId")
  public String getUsernameWithOrderId() {
    return orderKey.getUsernameWithOrderId();
  }

  public void setUsernameWithOrderId(String usernameWithOrderId) {
    orderKey.setUsernameWithOrderId(usernameWithOrderId);
  }
  
  @DynamoDBIgnore
  public int getIntegertotalTradedQuantity() {
    return (int)getTotalTradedQuantity();
  }

  @DynamoDBIgnore
  public int getIntegerQuantity() {
    return (int)getQuantity();
  }
  public void setIdentityIdWithOrderId(String usernameWithOrderId) {
    orderKey.setUsernameWithOrderId(usernameWithOrderId);
  }

  @DynamoDBRangeKey(attributeName = "orderTimestamp")
  public long getOrderTimestamp() {
    return orderKey.getOrderTimestamp();
  }

  public void setOrderTimestamp(long orderTimestamp) {
    orderKey.setOrderTimestamp(orderTimestamp);
  }
  
  public double calculateMFAverageTradedPrice() {
    return trades
        .stream()
        .collect(averagingWeighted(Trade::getPrice, Trade::getQuantity));
  }

  public double calculateEquityAverageTradedPrice() {
    return trades.size() > 0 ? trades
        .stream()
        .collect(averagingWeighted(Trade::getPrice, Trade::getIntegerQuantity)) : 0;
  }

  private boolean isMutualFundOrder() {
    return ticker.endsWith("-MF");
  }

  @Override
  public int compareTo(Order order) {
     return Long.compare(this.getOrderTimestamp(), order.getOrderTimestamp());
  }
}
