package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class BenchmarkPerformanceKey implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter
  @Setter
  @DynamoDBHashKey(attributeName = "fund")
  private String fund;
  private @Getter @Setter @DynamoDBRangeKey(attributeName = "date") long date;
}
