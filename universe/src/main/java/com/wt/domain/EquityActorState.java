package com.wt.domain;

import static com.wt.utils.CommonConstants.HYPHEN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.wt.domain.write.events.BonusEventUpdatedReceived;
import com.wt.domain.write.events.DividendUpdateReceived;
import com.wt.domain.write.events.EquityCorporateEvent;
import com.wt.domain.write.events.EquityInstrumentDescriptionUpdated;
import com.wt.domain.write.events.EquityInstrumentSegmentUpdated;
import com.wt.domain.write.events.EquityInstrumentSymbolUpdated;
import com.wt.domain.write.events.EquityInstrumentTokenUpdated;
import com.wt.domain.write.events.EquityInstrumentsUpdated;
import com.wt.domain.write.events.PriorEntriesFromCorpEventMapRemoved;
import com.wt.domain.write.events.StockSplitUpdateReceived;
import com.wt.utils.CommonConstants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class EquityActorState implements Serializable {
  private static final long serialVersionUID = 1L;

  @Getter private final Map<String, EquityInstrument> mapSymbolToInstrument;
  @Getter private final Map<String, EquityInstrument> mapWdIdToInstrument;
  @Getter
  @Setter
  private  Map<String, String> mapTokenWithExchangeToWdId;
  @Getter
  @Setter
  private Map<Long, List<EquityCorporateEvent>> mapExDateToCorporateEvents;

  public EquityActorState() {
    this(new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new ConcurrentHashMap<>());
  }

  public EquityActorState(Map<String, EquityInstrument> mapSymbolToInstrument,
      Map<String, EquityInstrument> mapWdIdToInstrument, Map<Long, List<EquityCorporateEvent>> mapDateToCorporateEvents,
      Map<String, String> mapTokenWithExchangeToWdId) {
    this.mapSymbolToInstrument = mapSymbolToInstrument;
    this.mapWdIdToInstrument = mapWdIdToInstrument;
    this.mapExDateToCorporateEvents = mapDateToCorporateEvents;
    this.mapTokenWithExchangeToWdId = mapTokenWithExchangeToWdId;

  }

  public void update(Object evt) {
    if (evt instanceof EquityInstrumentsUpdated) {
      EquityInstrumentsUpdated event = (EquityInstrumentsUpdated) evt;
      event.getAllDeleted().stream().forEach(equityInstrument -> mapSymbolToInstrument
          .remove(equityInstrument.getSymbol().concat(CommonConstants.HYPHEN).concat(equityInstrument.getExchange())));
      event.getAllDeleted().stream()
          .forEach(equityInstrument -> mapWdIdToInstrument.remove(equityInstrument.getWdId()));
      event.getAllDeleted().stream().forEach(equityInstrument -> mapTokenWithExchangeToWdId
          .remove(equityInstrument.getToken().concat(CommonConstants.HYPHEN).concat(equityInstrument.getExchange())));
      event.getAllAdded().stream().forEach(equityInstrument -> mapSymbolToInstrument
          .put(equityInstrument.getSymbol().concat(CommonConstants.HYPHEN).concat(equityInstrument.getExchange()), equityInstrument));
      event.getAllAdded().stream().forEach(equityInstrument -> mapTokenWithExchangeToWdId
          .put(equityInstrument.getToken().concat(CommonConstants.HYPHEN).concat(equityInstrument.getExchange()), equityInstrument.getWdId()));
      event.getAllAdded().stream()
          .forEach(equityInstrument -> mapWdIdToInstrument.put(equityInstrument.getWdId(), equityInstrument));
    } else if (evt instanceof EquityInstrumentDescriptionUpdated) {
      EquityInstrumentDescriptionUpdated event = (EquityInstrumentDescriptionUpdated) evt;
      EquityInstrument equityInstrument;
      if (event.getExchange() != null) {
        equityInstrument = mapSymbolToInstrument.get(event.getSymbol().concat(CommonConstants.HYPHEN).concat(event.getExchange()));
      } else {
        equityInstrument = mapSymbolToInstrument.get(event.getSymbol().concat(CommonConstants.HYPHEN).concat("NSE"));
        if (equityInstrument == null)
          equityInstrument = mapSymbolToInstrument.get(event.getSymbol().concat(CommonConstants.HYPHEN).concat("BSE"));
      }
      equityInstrument.update(event);

      mapWdIdToInstrument.put(equityInstrument.getWdId(), equityInstrument);
    } else if (evt instanceof EquityInstrumentSymbolUpdated) {
      EquityInstrumentSymbolUpdated event = (EquityInstrumentSymbolUpdated) evt;
      EquityInstrument equityInstrument = new EquityInstrument(event.getWdId(), event.getToken(), event.getNewSymbol(),
          event.getDescription(), event.getSegment());
      mapSymbolToInstrument.remove(event.getOldSymbol().concat(CommonConstants.HYPHEN).concat(event.getExchange()));
      mapSymbolToInstrument.put(event.getNewSymbol().concat(CommonConstants.HYPHEN).concat(event.getExchange()), equityInstrument);
      mapWdIdToInstrument.put(event.getWdId(), equityInstrument);
    } else if (evt instanceof EquityInstrumentTokenUpdated) {
      EquityInstrumentTokenUpdated event = (EquityInstrumentTokenUpdated) evt;
      EquityInstrument equityInstrument = mapWdIdToInstrument.get(event.getWdId());

      equityInstrument.update(event);
      
      String symbolWithExchange = event.getSymbol().concat(CommonConstants.HYPHEN).concat(event.getExchange());
      mapSymbolToInstrument.put(symbolWithExchange, equityInstrument);
      mapTokenWithExchangeToWdId.remove(event.getOldToken().concat(HYPHEN).concat(event.getExchange()));
      mapTokenWithExchangeToWdId.put(event.getNewToken().concat(CommonConstants.HYPHEN).concat(event.getExchange()), event.getWdId());
    } else if (evt instanceof EquityInstrumentSegmentUpdated) {
      EquityInstrumentSegmentUpdated event = (EquityInstrumentSegmentUpdated) evt;
      EquityInstrument equityInstrument = mapWdIdToInstrument.get(event.getWdId());

      equityInstrument.update(event);
      
      String symbolWithExchange = event.getSymbol().concat(CommonConstants.HYPHEN).concat(event.getExchange());
      mapSymbolToInstrument.put(symbolWithExchange, equityInstrument);
    } else if (evt instanceof BonusEventUpdatedReceived) {
      BonusEventUpdatedReceived event = (BonusEventUpdatedReceived) evt;
      addToExDateToCorporateEventsMap(event.getCorporateEvent().getExDate(), event.getCorporateEvent());
    } else if (evt instanceof StockSplitUpdateReceived) {
      StockSplitUpdateReceived event = (StockSplitUpdateReceived) evt;
      addToExDateToCorporateEventsMap(event.getCorporateEvent().getExDate(), event.getCorporateEvent());
    } else if (evt instanceof DividendUpdateReceived) {
      DividendUpdateReceived event = (DividendUpdateReceived) evt;
      addToExDateToCorporateEventsMap(event.getCorporateEvent().getExDate(), event.getCorporateEvent());
    } else if (evt instanceof PriorEntriesFromCorpEventMapRemoved) {
      PriorEntriesFromCorpEventMapRemoved event = (PriorEntriesFromCorpEventMapRemoved) evt;
      mapExDateToCorporateEvents.entrySet().removeIf(entry -> entry.getKey() < event.getCurrentBODDate());
    }
  }

  private void addToExDateToCorporateEventsMap(long exDate, EquityCorporateEvent corporateEvent) {
    if (mapExDateToCorporateEvents.containsKey(exDate)) {
      List<EquityCorporateEvent> corpEventsForTheDay = mapExDateToCorporateEvents
          .get(exDate);
      corpEventsForTheDay.add(corporateEvent);
    } else {
      List<EquityCorporateEvent> corpEventsForTheDay = new ArrayList<EquityCorporateEvent>();
      corpEventsForTheDay.add(corporateEvent);
      mapExDateToCorporateEvents.put(exDate, corpEventsForTheDay);
    }
  }

  public EquityInstrument getEquityInstrumentBySymbol(String symbol) {
    return mapSymbolToInstrument.get(symbol);
  }

  public EquityInstrument getEquityInstrumentByWdId(String wdId) {
    return mapWdIdToInstrument.get(wdId);
  }

  public Set<String> getAllWdIds() {
    return mapWdIdToInstrument.keySet();
  }

  public Collection<EquityInstrument> getAllInstruments() {
    return mapWdIdToInstrument.values();
  }

  public EquityActorState copy() {
    return new EquityActorState(new ConcurrentHashMap<>(mapSymbolToInstrument), new ConcurrentHashMap<>(mapWdIdToInstrument), new ConcurrentHashMap<>(mapExDateToCorporateEvents), new ConcurrentHashMap<>(mapTokenWithExchangeToWdId));
  }
}
