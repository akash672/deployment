package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString(of = "wdId")
@DynamoDBTable(tableName = "MutualFund_Instrument")
@EqualsAndHashCode(callSuper=true, of="wdId")
public class MutualFundInstrument extends Instrument implements Serializable {
  private static final long serialVersionUID = 1L;

  @DynamoDBHashKey
  @Getter 
  @Setter
  private String wdId;
  @DynamoDBAttribute
  @Getter
  @Setter
  private String schemeName;
  @Getter
  @Setter
  private String amcCode;
  @DynamoDBIgnore
  @Setter
  private String token;
  @Getter
  @Setter
  @DynamoDBIgnore
  private String schemeCode;
  @Getter
  @Setter
  @DynamoDBIgnore
  private String segment;
  @Getter
  @Setter
  @DynamoDBIgnore
  private String symbol;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String schemeType;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String minInvestment;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String minMultiplier;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String exitLoad;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String rta;
 

  public MutualFundInstrument(String wdId, String schemeName, String schemeCode, String minInvestment, String minMultiplier,
                              String schemeType, String exitLoad, String rta) {
    this.wdId = wdId;
    this.schemeName = schemeName;
    this.schemeCode = schemeCode;
    this.minInvestment = minInvestment;
    this.minMultiplier = minMultiplier;
    this.schemeType = schemeType;
    this.exitLoad = exitLoad;
    String[] parts = wdId.split("-");
    token = parts[0];
    segment = parts[1];
    this.amcCode = token.substring(3, 6);
    symbol = schemeName;
  }
  
  public MutualFundInstrument(String token, String segment, String schemeName, String schemeCode,String minInvestment, String minMultiplier,
                             String schemeType, String exitLoad, String rta) {
    this(token + "-" + segment, schemeName, schemeCode,minInvestment, minMultiplier, schemeType, exitLoad, rta);
  }
  
  public MutualFundInstrument(String wdId) {
    this.wdId = wdId;
    String[] parts = wdId.split("-");
    token = parts[0];
    segment = parts[1];
  }
  
  @DynamoDBIgnore
  public String getToken() {
    return token;
  }
  
}
