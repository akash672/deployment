package com.wt.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;

public class OrderSideMarshaller implements DynamoDBTypeConverter<String, OrderSide> {

  private Gson gson = new Gson();

  @Override
  public String convert(OrderSide object) {
    return gson.toJson(object);
  }

  @Override
  public OrderSide unconvert(String object) {
    return gson.fromJson(object, OrderSide.class);
  }

}
