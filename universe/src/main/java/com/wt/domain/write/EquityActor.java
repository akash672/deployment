package com.wt.domain.write;

import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.newHashSet;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.DateUtils.prettyDate;
import static com.wt.utils.DateUtils.todayBOD;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets.SetView;
import com.wt.domain.CorporateEventType;
import com.wt.domain.EquityActorState;
import com.wt.domain.EquityInstrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.CheckForCorporateEvent;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.commands.GetHistoricalPriceEventsByInstrument;
import com.wt.domain.write.commands.GetInstrumentByWdId;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.GetPrice;
import com.wt.domain.write.commands.GetPricePaf;
import com.wt.domain.write.commands.GetSymbolToInstrumentMap;
import com.wt.domain.write.commands.GetTokenToWdIdMap;
import com.wt.domain.write.commands.GetWdIdToInstrumentMap;
import com.wt.domain.write.commands.MapTokenToWdIdSent;
import com.wt.domain.write.commands.RemovePriorEntriesFromCorpEventMap;
import com.wt.domain.write.commands.SendEODPriceWithPafToDB;
import com.wt.domain.write.commands.StopLivePriceStream;
import com.wt.domain.write.commands.SubscribeAndGetLivePriceStream;
import com.wt.domain.write.commands.SubscribeToToken;
import com.wt.domain.write.commands.SymbolToInstrumentMapSent;
import com.wt.domain.write.commands.UnSubscribeToToken;
import com.wt.domain.write.commands.UpdateDividendEvent;
import com.wt.domain.write.commands.UpdateEQInstruments;
import com.wt.domain.write.commands.UpdateEquityInstrumentDescription;
import com.wt.domain.write.commands.UpdateEquityInstrumentSegment;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateEquityInstrumentToken;
import com.wt.domain.write.commands.UpdateSplitTypeEvent;
import com.wt.domain.write.commands.UpdateTheTokenToWdIdMap;
import com.wt.domain.write.commands.WdIdToInstrumentMapSent;
import com.wt.domain.write.events.BonusEvent;
import com.wt.domain.write.events.BonusEventUpdatedReceived;
import com.wt.domain.write.events.DividendEvent;
import com.wt.domain.write.events.DividendUpdateReceived;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.EquityCorporateEvent;
import com.wt.domain.write.events.EquityInstrumentDescriptionUpdated;
import com.wt.domain.write.events.EquityInstrumentSegmentUpdated;
import com.wt.domain.write.events.EquityInstrumentSymbolUpdated;
import com.wt.domain.write.events.EquityInstrumentTokenUpdated;
import com.wt.domain.write.events.EquityInstrumentsUpdated;
import com.wt.domain.write.events.InstrumentByWdIdSent;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.PriorEntriesFromCorpEventMapRemoved;
import com.wt.domain.write.events.StockSplitEvent;
import com.wt.domain.write.events.StockSplitUpdateReceived;
import com.wt.universe.util.UniverseSNSToDDBHandler;
import com.wt.universe.util.UniverseSNSToRDSHandler;
import com.wt.utils.CommonConstants;
import com.wt.utils.akka.WDActorSelections;

import akka.http.javadsl.model.StatusCodes;
import akka.persistence.RecoveryCompleted;
import akka.persistence.SaveSnapshotFailure;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotMetadata;
import akka.persistence.SnapshotOffer;

@Service("EquityActor")
@Scope("prototype")
public class EquityActor extends AssetActor implements ParentActor<EquityChildActor> {
 private EquityActorState equityActorState =  new EquityActorState();
 private Map<String, String> mapTokenToWdId = new HashMap<String, String>();

  private @Autowired WDActorSelections wdActorSelections;
  private HashMap<String, PriceWithPaf> tickerToPriceWithPafMap = new HashMap<String, PriceWithPaf>();
  private @Autowired UniverseSNSToRDSHandler snsToRDSHandler;
  private @Autowired UniverseSNSToDDBHandler snsToDDBHandler;
  
  @Override
  public String getName() {
    return "EQ";
  }
  
  @Override
  public String getChildBeanName() {
    return "EquityChildActor";
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(InstrumentPrice.class, cmd -> {
      if(cmd.getWdId().endsWith("-EQ"))
        forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(GetInstrumentByWdId.class, cmd -> {
      if(cmd.getWdId().endsWith("-EQ"))
        getEquityInstrumentByWdId(cmd);
    }).match(EquityCorporateEvent.class, cmd -> {
      updateEquityCorporateEvent(cmd);
    }).match(UpdateEQInstruments.class, cmd -> {
      updateEQInstruments(cmd);
    }).match(UpdateEquityInstrumentDescription.class, cmd -> {
      updateEquityInstrumentDescription(cmd);
    }).match(UpdateEquityInstrumentSymbol.class, cmd -> {
      updateEquityInstrumentSymbol(cmd);
    }).match(UpdateEquityInstrumentSegment.class, cmd -> {
      updateEquityInstrumentSegment(cmd);
    }).match(UpdateEquityInstrumentToken.class, cmd -> {
      updateEquityInstrumentToken(cmd);
    }).match(GetEquityInstrumentBySymbol.class, cmd -> {
      getInstrumentBySymbol(cmd);
    }).match(GetPricePaf.class, cmd -> {
      forwardToChildren(context(), cmd, cmd.getToken());
    }).match(PriceWithPaf.class, cmd -> {
      if(cmd.getPrice().getWdId().endsWith("-EQ"))
        forwardToChildren(context(), cmd, cmd.getPrice().getWdId());
    }).match(SendEODPriceWithPafToDB.class, cmd -> {
        handleEODPrices(cmd);
    }).match(SubscribeToToken.class, cmd -> {
      if(cmd.getTicker().endsWith("-EQ")){
        String token = equityActorState.getEquityInstrumentByWdId(cmd.getTicker()).getToken();
        mapTokenToWdId.put(token, cmd.getTicker());
        cmd.setToken(token);
        forwardToChildren(context(), cmd, cmd.getTicker());
      }
    }).match(UnSubscribeToToken.class, cmd -> {
      if (cmd.getTicker().endsWith("-EQ")) {
        String token = equityActorState.getEquityInstrumentByWdId(cmd.getTicker()).getToken();
        mapTokenToWdId.remove(token);
        cmd.setToken(token);
        forwardToChildren(context(), cmd, cmd.getTicker());
      }
    }).match(RealTimeInstrumentPrice.class, cmd -> {
      String wdId = mapTokenToWdId.get(cmd.getInstrumentPrice().getWdId());
      cmd.getInstrumentPrice().setWdId(wdId);
      if(wdId!=null)
        forwardToChildren(context(), cmd, wdId);
    }).match(GetPrice.class, cmd -> {
      if(cmd.getWdId().endsWith("-EQ"))
        forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(GetHistoricalPriceEventsByInstrument.class, cmd -> {
      if(cmd.getWdId().endsWith("-EQ"))
        forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(GetLivePriceStream.class, cmd -> {
      if (cmd.getWdId().endsWith("-EQ")) {
        String isin = cmd.getWdId().split("-")[0];
        if (!isin.matches("[0-9]+"))
          forwardToChildren(context(), cmd, cmd.getWdId());
        else {
          String[] parts = cmd.getWdId().split("-");
          String tokenWithExchange = parts[0] + "-" + parts[1];
          String wdId = equityActorState.getMapTokenWithExchangeToWdId().get(tokenWithExchange);
          if (wdId != null)
            forwardToChildren(context(), cmd, wdId);
        }
      }
    }).match(SubscribeAndGetLivePriceStream.class, cmd -> {
      if(cmd.getWdId().endsWith("-EQ")  && equityActorState.getMapWdIdToInstrument().containsKey(cmd.getWdId())){
        String token = equityActorState.getEquityInstrumentByWdId(cmd.getWdId()).getToken();
        mapTokenToWdId.put(token, cmd.getWdId());
        cmd.setToken(token);
        forwardToChildren(context(), cmd, cmd.getWdId());
      }
    }).match(StopLivePriceStream.class, cmd -> {
      if(cmd.getWdId().endsWith("-EQ"))
      forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(CheckForCorporateEvent.class, cmd -> {
      checkforCorporateEvent(cmd);
    }).match(EODCompleted.class, cmd -> {
      log.debug("Saving snapshot of EquityActor.");
      saveSnapshot(equityActorState.copy());
      sendPricesToRDSHandler();
      sendPricesToDDBHandler();
      forwardToChildren(context(), cmd, getChildBeanName());
    }).match(SaveSnapshotFailure.class, cmd -> {
      handleSnapshotFailure(cmd);
    }).match(SaveSnapshotSuccess.class, cmd -> {
      handleSnapshotSuccess(cmd);
    }).match(GetWdIdToInstrumentMap.class, cmd -> {
       sender().tell(new WdIdToInstrumentMapSent(equityActorState.getMapWdIdToInstrument()), self());
    }).match(GetSymbolToInstrumentMap.class, cmd -> {
      sender().tell(new SymbolToInstrumentMapSent(equityActorState.getMapSymbolToInstrument()), self());
    }).match(RemovePriorEntriesFromCorpEventMap.class, cmd -> {
      removePriorEntriesFromCorpEventMap(cmd);
    }).match(UpdateTheTokenToWdIdMap.class, cmd -> {
      Map<String, String> mapTokenToWdId =  new HashMap<String,String>();
      equityActorState.getMapWdIdToInstrument().entrySet().stream().
        forEach(entry -> {
          String tokenWithSymbol = entry.getValue().getToken().concat(CommonConstants.HYPHEN).concat(entry.getValue().getExchange());
          mapTokenToWdId.put(tokenWithSymbol, entry.getValue().getWdId());
        });
      equityActorState.setMapTokenWithExchangeToWdId(mapTokenToWdId);
      saveSnapshot(equityActorState.copy());
    }).match(GetTokenToWdIdMap.class, cmd -> {
      sender().tell(new MapTokenToWdIdSent(equityActorState.getMapTokenWithExchangeToWdId()), self());
   }).build();
  }

  private void updateEquityCorporateEvent(EquityCorporateEvent cmd) {
    if (commandNotAlreadyPersisted(cmd)) {
      
      if (cmd instanceof BonusEvent) {
        BonusEvent bonusEvent = (BonusEvent) cmd;
        persist(new BonusEventUpdatedReceived(bonusEvent), event -> {
          equityActorState.update(event);
          forwardToChildren(context(), cmd, bonusEvent.getWdId());
        });
      }
      if (cmd instanceof StockSplitEvent) {
        StockSplitEvent stockSplitEvent = (StockSplitEvent) cmd;
        persist(new StockSplitUpdateReceived(stockSplitEvent), event -> {
          equityActorState.update(event);
          forwardToChildren(context(), cmd, stockSplitEvent.getWdId());
        });
      }
      if (cmd instanceof DividendEvent) {
        DividendEvent dividendEvent = (DividendEvent) cmd;
        persist(new DividendUpdateReceived(dividendEvent), event -> {
          equityActorState.update(event);
          forwardToChildren(context(), cmd, dividendEvent.getWdId());
        });
      }
    } else {
        sender().tell(new Done(cmd.getWdId(), " corporate event recevied."), self());
    }
  }

  private boolean commandNotAlreadyPersisted(EquityCorporateEvent cmd) {
    List<EquityCorporateEvent> corpEventsForTheDay;
    if(!equityActorState.getMapExDateToCorporateEvents().containsKey(cmd.getExDate()))
      return true;
    else
      corpEventsForTheDay = equityActorState.getMapExDateToCorporateEvents().get(cmd.getExDate());
      if(corpEventsForTheDay.contains(cmd))
        return false;
      else
        return true;
  }

  private void handleSnapshotSuccess(SaveSnapshotSuccess snapshotSuccess) {
    SnapshotMetadata metadata = snapshotSuccess.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has sucessfully been persisted with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private void handleSnapshotFailure(SaveSnapshotFailure snapshotFailure) {
    SnapshotMetadata metadata = snapshotFailure.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has failed to persist with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private void updateEquityInstrumentSymbol(UpdateEquityInstrumentSymbol cmd) {
    persist(new EquityInstrumentSymbolUpdated(cmd.getToken(), cmd.getOldSymbol(), cmd.getNewSymbol(), cmd.getWdId(),
        cmd.getDescription(), cmd.getExchange(), cmd.getSegment()), event -> {
          equityActorState.update(event);
          sender().tell(new Done(String.valueOf(cmd.getToken()),
              "Token: " + cmd.getToken() + " updated with Symbol: " + cmd.getNewSymbol()), self());
        });
  }

  private void updateEquityInstrumentDescription(UpdateEquityInstrumentDescription cmd) {

    persist(new EquityInstrumentDescriptionUpdated(cmd.getToken(), cmd.getSymbol(), cmd.getDescription(),
        cmd.getExchange(), cmd.getWdId()), event -> {
          equityActorState.update(event);
          sender().tell(new Done(String.valueOf(cmd.getToken()),
              "Token: " + cmd.getToken() + " Description: " + cmd.getDescription()), self());
        });

  }
  

  private void updateEquityInstrumentToken(UpdateEquityInstrumentToken cmd) {
    persist(new EquityInstrumentTokenUpdated(cmd.getWdId(), cmd.getOldToken(), cmd.getNewToken(), cmd.getSymbol(), 
        cmd.getExchange()), event -> {
          equityActorState.update(event);
          sender().tell(new Done(String.valueOf(cmd.getWdId()),
              " OldToken: " + cmd.getOldToken() + " updated to " + cmd.getNewToken() + " for WdId " + cmd.getWdId()), self());
        });
    
  }
  
  private void updateEquityInstrumentSegment(UpdateEquityInstrumentSegment cmd) {
    persist(new EquityInstrumentSegmentUpdated(cmd.getWdId(), cmd.getSymbol(), cmd.getExchnage(), cmd.getNewSegment()) 
        , event -> {
          equityActorState.update(event);
          sender().tell(new Done(String.valueOf(cmd.getWdId()),
              " Trading Segment updated to : " + cmd.getNewSegment()+ " for wdId " + cmd.getWdId() ), self());
        });
  }

  private void updateEQInstruments(UpdateEQInstruments cmd) {
    Set<EquityInstrument> yesterday = newHashSet(equityActorState.getAllInstruments());
    Set<EquityInstrument> today = newHashSet(((UpdateEQInstruments) cmd).getToday());
    SetView<EquityInstrument> allDeleted = difference(yesterday, yesterday);
    SetView<EquityInstrument> allAdded = difference(today, yesterday);

    persist(new EquityInstrumentsUpdated(allAdded, allDeleted), event -> {
      equityActorState.update(event);
      sender().tell(new Done("All EQ instruments", "All EQ instruments"), self());

    });
  }

  private void getInstrumentBySymbol(GetEquityInstrumentBySymbol cmd) {
    EquityInstrument equityInstrument = equityActorState.getEquityInstrumentBySymbol(cmd.getSymbol().concat("-").concat(cmd.getExchange()));
    if (equityInstrument == null) {
      log.warn("Error. No EQ instrument " + cmd.getSymbol() + " in exhcange: " + cmd.getExchange());
      sender().tell(failed(cmd.getSymbol(),
          "EQ Instrument not found for " + cmd.getSymbol() + " in exhcange: " + cmd.getExchange(), StatusCodes.NOT_FOUND), self());
    } else {
      sender().tell(equityInstrument, self());
    }
  }
  
  private void getEquityInstrumentByWdId(GetInstrumentByWdId cmd){
    EquityInstrument equityInstrument = equityActorState.getEquityInstrumentByWdId(cmd.getWdId());
    if (equityInstrument == null) {
      log.error("Error. No EQ instrument " + cmd.getWdId());
      sender().tell(failed(cmd.getWdId(), "EQ Instrument not found for " + cmd.getWdId(), StatusCodes.NOT_FOUND), self());
    } else {
      InstrumentByWdIdSent instrumentByWdIdSent = new InstrumentByWdIdSent(equityInstrument);
      sender().tell(instrumentByWdIdSent, self());
    }
  }
  
  private void checkforCorporateEvent(CheckForCorporateEvent cmd) throws ParseException {
    long currentdate = todayBOD();
    if (equityActorState.getMapExDateToCorporateEvents().containsKey(currentdate)) {
      List<EquityCorporateEvent> corporateEvents = equityActorState.getMapExDateToCorporateEvents().get(currentdate);
      for (EquityCorporateEvent corporateEvent : corporateEvents) {
        if ((currentdate == corporateEvent.getExDate()) && ((corporateEvent.getEventType() == CorporateEventType.BONUS
            || corporateEvent.getEventType() == CorporateEventType.SPLIT))) {
          wdActorSelections.userRootActor()
              .tell(new UpdateSplitTypeEvent(corporateEvent.getWdId(), corporateEvent.adjustPaf(new PriceWithPaf()),
                  corporateEvent.getEventType(), currentdate), self());
        } else if ((currentdate == corporateEvent.getExDate())
            && ((corporateEvent.getEventType() == CorporateEventType.DIVIDEND))) {

          wdActorSelections.userRootActor()
              .tell(new UpdateDividendEvent(corporateEvent.getWdId(),
                  corporateEvent.adjustPaf(
                      new PriceWithPaf().withPrice(1.0, corporateEvent.getWdId(), Pafs.withCumulativePaf(1.))) - 1.0,
                  corporateEvent.getEventType(), currentdate), self());

        }

      }
     self().tell(new RemovePriorEntriesFromCorpEventMap(currentdate), self());
    }
    
  }
  
  private void removePriorEntriesFromCorpEventMap(RemovePriorEntriesFromCorpEventMap cmd) {

    persist(new PriorEntriesFromCorpEventMapRemoved(cmd.getCurrentBODDate()), event -> {
      equityActorState.update(event);
    });

  }


  private void handleEODPrices(SendEODPriceWithPafToDB cmd) {
    tickerToPriceWithPafMap.put(cmd.getPriceWithPaf().getPrice().getWdId(), cmd.getPriceWithPaf());
  }

  private void sendPricesToRDSHandler() {
    snsToRDSHandler.handlePriceData(tickerToPriceWithPafMap);
    log.debug("Sending Price Snaphot of EOD to SNS to RDS handler of size: " + tickerToPriceWithPafMap.size());
  }
  
  private void sendPricesToDDBHandler() {
    snsToDDBHandler.handlePriceData(tickerToPriceWithPafMap);
    log.debug("Sending Price Snaphot of EOD to SNS to DDB handler of size: " + tickerToPriceWithPafMap.size());
    tickerToPriceWithPafMap.clear();
  }

  @Override
  public Receive createReceiveRecover() { 
    return receiveBuilder().match(EquityInstrumentsUpdated.class, event -> {
      equityActorState.update(event);
    }).match(EquityInstrumentDescriptionUpdated.class, event -> {
      equityActorState.update(event);
    }).match(EquityInstrumentSymbolUpdated.class, event -> {
      equityActorState.update(event);
    }).match(RecoveryCompleted.class, event -> {
      log.debug("Recovery of EquityActor is complete");
    }).match(SnapshotOffer.class, snapshotOffer -> {
      log.debug("Received EquityActor snapshot");
      equityActorState = (EquityActorState) snapshotOffer.snapshot();
      if(equityActorState.getMapExDateToCorporateEvents() == null)
        equityActorState.setMapExDateToCorporateEvents(new ConcurrentHashMap<>());
      if(equityActorState.getMapTokenWithExchangeToWdId() == null)
        equityActorState.setMapTokenWithExchangeToWdId(new ConcurrentHashMap<>()); 
    }).match(BonusEventUpdatedReceived.class, event -> {
      equityActorState.update(event);
    }).match(StockSplitUpdateReceived.class, event -> {
      equityActorState.update(event);
    }).match(PriorEntriesFromCorpEventMapRemoved.class, event -> {
      equityActorState.update(event);
    }).match(DividendUpdateReceived.class, event -> {
      equityActorState.update(event);
    }).build();
  }

  @Override
  public String persistenceId() {
    return "EquityActor";
  }

}