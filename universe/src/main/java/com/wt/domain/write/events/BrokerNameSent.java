package com.wt.domain.write.events;

import static com.wt.domain.BrokerName.NOBROKER;

import java.io.Serializable;

import com.wt.domain.BrokerName;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class BrokerNameSent implements Serializable {

  private static final long serialVersionUID = 1L;
  @Getter
  private BrokerName brokerName;
  
  public static BrokerNameSent noBrokerInformationAvailable() {
    return new BrokerNameSent(NOBROKER);
  }
}
