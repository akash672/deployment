package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class UpdateMutualFundInstrumentSchemeName extends MutualFundCommand implements Serializable{
  private static final long serialVersionUID = 1L;
  @Getter
  private String token;
  @Getter
  private String schemeName;
}
