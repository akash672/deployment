package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Getter
public class PriorEntriesFromCorpEventMapRemoved implements Serializable {
   
  private static final long serialVersionUID = 1L;
  private long currentBODDate;
  
  

}
