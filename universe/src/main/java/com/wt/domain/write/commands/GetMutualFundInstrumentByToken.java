package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class GetMutualFundInstrumentByToken extends MutualFundCommand implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter
  private String token;
}