package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.List;

import com.wt.domain.PriceWithPaf;

import lombok.ToString;

@ToString
public class HistoricalPriceEventsList implements Serializable {
  
  private static final long serialVersionUID = 1L;
  
  private List<PriceWithPaf> priceEvents;
  private String key;
  
  public HistoricalPriceEventsList(String key,List<PriceWithPaf> priceEvents)
  {
    this.key=key;
    this.priceEvents=priceEvents;
  }
  public String getKey()
  {
    return key;
  }
  public List<PriceWithPaf> getListOfPriceEvent()
  {
    return priceEvents;
  }
  

}
