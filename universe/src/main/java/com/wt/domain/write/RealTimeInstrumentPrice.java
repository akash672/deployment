package com.wt.domain.write;

import java.io.Serializable;

import com.wt.domain.write.events.AssetClassEvent;
import com.wt.domain.write.events.InstrumentPrice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class RealTimeInstrumentPrice extends AssetClassEvent implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private InstrumentPrice instrumentPrice;
}
