package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class BonusEventUpdatedReceived implements Serializable {
  
  private static final long serialVersionUID = 1L;
  private BonusEvent corporateEvent;;
  
}
