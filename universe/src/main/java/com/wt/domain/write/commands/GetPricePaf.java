package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GetPricePaf extends AssetClassCommand implements Serializable {
  private static final long serialVersionUID = 1L;
  private long timestamp;
  private String token;


  public GetPricePaf(String token, long timestamp) {
    super();
    this.timestamp = timestamp;
    this.token = token;
  }

}
