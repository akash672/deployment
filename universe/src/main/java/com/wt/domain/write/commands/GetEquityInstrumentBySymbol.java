package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class GetEquityInstrumentBySymbol extends EquityCommand implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter
  private String symbol;
  @Getter
  private String exchange;
}
