package com.wt.domain.write;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.Runtime.getRuntime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wt.cloud.monitoring.ServiceStatusReporter;
import com.wt.universe.util.CtclAppConfiguration;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class UniverseService {
  private static AnnotationConfigApplicationContext ctx;

  private ActorSystem universeSystem;
  @SuppressWarnings("unused")
  private ActorRef universeActor;
  private ServiceStatusReporter serviceStatusReporter;
  @Value("${service.monitoring.flag}")
  private String serviceMonitoringFlag;

  @Autowired
  public UniverseService(ActorSystem universeSystem, ServiceStatusReporter serviceStatusReporter) {
    super();
    this.universeSystem = universeSystem;
    this.serviceStatusReporter = serviceStatusReporter;
  }

  public static void main(String[] args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(CtclAppConfiguration.class);
    UniverseService service = ctx.getBean(UniverseService.class);
    service.start();
  }

  private void start() throws Exception {
    log.info("=======================================");
    log.info("| Starting Universe Service           |");
    log.info("=======================================");
    createActors();

    getRuntime().addShutdownHook(new Thread(() -> {
      ctx.close();
    })); 
  }

  public void createActors() {
    universeActor = universeSystem.actorOf(SpringExtProvider.get(universeSystem).props("UniverseRootActor"), "universe-root-actor");
  }
  
  @Scheduled(cron = "${service.heartbeat.schedule}", zone = "Asia/Kolkata")
  public void sendServiceStatusHeartBeats() {
    if (serviceMonitoringFlag.equalsIgnoreCase("true"))
    serviceStatusReporter.sendStatusUpdate(this.getClass().getSimpleName());
  }

}