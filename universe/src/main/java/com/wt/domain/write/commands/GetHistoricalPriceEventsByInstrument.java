package com.wt.domain.write.commands;

import java.io.Serializable;

public class GetHistoricalPriceEventsByInstrument extends AssetClassCommand implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String wdId;
  private long startDate;
  private String responseIdentifier;
  
  public GetHistoricalPriceEventsByInstrument(String wdId,long startDate,String responseIdentifier)
  {
    this.wdId = wdId;
    this.startDate=startDate;
    this.responseIdentifier=responseIdentifier;
  }
  public String getWdId()
  {
    return wdId;
  }
  public long getStartDate()
  {
    return startDate;
  }
  public String getResponseIdentifier()
  {
    return responseIdentifier;
  }
}

