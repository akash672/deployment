package com.wt.domain.write.events;

import java.io.Serializable;

import com.wt.domain.Instrument;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class InstrumentByWdIdSent implements Serializable {

  private static final long serialVersionUID = 1L;
  private Instrument instrument;
  
  public static InstrumentByWdIdSent noInstrument() {
    return new InstrumentByWdIdSent(new Instrument());
  }
}
