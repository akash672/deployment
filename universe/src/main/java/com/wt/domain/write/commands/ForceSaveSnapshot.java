package com.wt.domain.write.commands;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;

public class ForceSaveSnapshot implements Serializable{
  private static final long serialVersionUID = 1L;

  @JsonCreator
  public ForceSaveSnapshot() {}
}