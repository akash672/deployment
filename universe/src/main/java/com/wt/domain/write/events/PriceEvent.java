package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class PriceEvent extends EventWithTimeStamps implements Serializable{
  private static final long serialVersionUID = 1L;
  private double price;
  @Setter private double cumulativePaf;
  @Setter private double nonDividendPaf;
  @Setter private double dividend;
  @Setter private double historicalPaf;
  private long timestamp;
  private String wdId;

  public PriceEvent(double price, long timestamp, String token)
  {
    this.price = price;
    this.timestamp = timestamp;
    this.wdId = token;
    this.cumulativePaf = 1.0;
    this.nonDividendPaf = 1.0;
    this.dividend = 0.0;
  }
}
