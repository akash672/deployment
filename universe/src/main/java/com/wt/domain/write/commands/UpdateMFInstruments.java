package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.Set;

import com.wt.domain.MutualFundInstrument;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class UpdateMFInstruments extends MutualFundCommand implements Serializable{

  private static final long serialVersionUID = 1L;
  @Getter private Set<MutualFundInstrument> today;
}
