package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class UpdateEquityInstrumentDescription extends EquityCommand implements Serializable{
  private static final long serialVersionUID = 1L;
  @Getter
  private String token;
  @Getter
  private String symbol;
  @Getter
  private String description;
  @Getter
  private String exchange;
  @Getter
  private String wdId;
}
