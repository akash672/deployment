package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GetInstrumentByWdId extends AssetClassCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  String wdId;
}
