package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
public class CheckForCorporateEvent extends EquityCommand implements Serializable {

  private static final long serialVersionUID = 1L;

}
