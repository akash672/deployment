package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.PriceWithPaf;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SendEODPriceWithPafToDB extends AssetClassCommand implements Serializable {
  private static final long serialVersionUID = 1L;
  private PriceWithPaf priceWithPaf;

  public SendEODPriceWithPafToDB(PriceWithPaf priceWithPaf) {
    super();
    this.priceWithPaf = priceWithPaf;
  }

}
