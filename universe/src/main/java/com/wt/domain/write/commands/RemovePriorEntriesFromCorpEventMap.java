package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Getter
public class RemovePriorEntriesFromCorpEventMap implements Serializable {

  private static final long serialVersionUID = 1L;
  private long currentBODDate;

}
