package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;
import com.wt.domain.Purpose;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviseOrderCreated extends AuthenticatedEvent {
  private String localOrderId;
  private long orderTime;
  private String wdId;
  private String symbol;
  private String clientCode;
  private String fundName = "NA";
  private OrderSide side;
  private double numberOfShares;
  private InvestingMode investingMode;
  private Purpose purpose;
  private double price;


  public static class AdviseOrderCreatedBuilder extends AuthenticatedEvent {
    private String localOrderId;
    private long orderTime;
    private String wdId;
    private String symbol;
    private String clientCode;
    private String fundName;
    private OrderSide side;
    private double numberOfShares;
    private InvestingMode investingMode;
    private Purpose purpose;
    private double price;
    
    public AdviseOrderCreatedBuilder(String username, String localOrderId, long orderTime, String wdId, String clientCode, OrderSide side,
        InvestingMode investingMode,String fundName) {
      super(username);
      this.localOrderId = localOrderId;
      this.orderTime = orderTime;
      this.wdId = wdId;
      this.clientCode = clientCode;
      this.side = side;
      this.investingMode = investingMode;
      this.fundName = fundName;
    }
    
    public AdviseOrderCreatedBuilder withNumberOfShares(double numberOfShares) {
      this.numberOfShares = numberOfShares;
      return this;
    }
    
    public AdviseOrderCreatedBuilder withSymbol(String symbol) {
      this.symbol = symbol;
      return this;
    }
    
    public AdviseOrderCreatedBuilder withFundName(String fundName) {
      this.fundName = fundName;
      return this;
    }
    
    public AdviseOrderCreatedBuilder withPurpose(Purpose purpose) {
      this.purpose = purpose;
      return this;
    }
    
    public AdviseOrderCreatedBuilder withPrice(double price) {
      this.price = price;
      return this;
    }
    
    public AdviseOrderCreated build() {
      return new AdviseOrderCreated(this);
    }
  }
  
  private AdviseOrderCreated(AdviseOrderCreatedBuilder builder) {
    super(builder.getUsername());
    this.localOrderId = builder.localOrderId;
    this.orderTime = builder.orderTime;
    this.wdId = builder.wdId;
    this.symbol = builder.symbol;
    this.clientCode = builder.clientCode;
    this.side = builder.side;
    this.investingMode = builder.investingMode;
    this.numberOfShares = builder.numberOfShares;
    this.investingMode = builder.investingMode;
    this.purpose = builder.purpose;
    this.price = builder.price;    
    this.fundName = builder.fundName;    
  }
  
}
