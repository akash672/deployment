package com.wt.domain.write.events;

import com.wt.domain.CorporateEventType;
import com.wt.domain.PriceWithPaf;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true,of={"from","to"})
public class StockSplitEvent extends EquityCorporateEvent {
  private static final long serialVersionUID = 1L;
  private double from;
  private double to;

  public StockSplitEvent(String token, long exDate, double from, double to) {
    super(token, exDate, CorporateEventType.SPLIT);
    this.from = from;
    this.to= to;
  }

  @Override
  public double adjustPaf(PriceWithPaf event) {
    return (this.from != 0.) ? this.from / this.to : 1.;
  }

  
}
