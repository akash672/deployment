package com.wt.domain.write;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.write.commands.AssetClassCommand;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.EquityCommand;
import com.wt.domain.write.commands.EquityDomainUpdationCommands;
import com.wt.domain.write.commands.MutualFundCommand;
import com.wt.domain.write.events.AssetClassEvent;
import com.wt.domain.write.events.EquityEvent;
import com.wt.domain.write.events.MutualFundEvent;

import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;

@Service("UniverseRootActor")
@Scope("prototype")
public class UniverseRootActor extends UntypedAbstractActor implements ParentActor<AssetActor> {

  private ActorRef equityActor;
  private ActorRef mutualFundActor;

  @Override
  public String getName() {
    return "UniverseRootActor";
  }

  @Override
  public String getChildBeanName() {
    return "";
  }

  public UniverseRootActor() {
    super();
    equityActor = context().actorOf(SpringExtProvider.get(context().system()).props("EquityActor"));
    mutualFundActor = context().actorOf(SpringExtProvider.get(context().system()).props("MutualFundActor"));
  }

  @Override
  public void onReceive(Object command) throws Throwable {
    if (command instanceof AssetClassEvent) {
      equityActor.forward(command, context());
      mutualFundActor.forward(command, context());
    } else if (command instanceof AssetClassCommand) {
      equityActor.forward(command, context());
      mutualFundActor.forward(command, context());
    } else if (command instanceof EquityEvent) {
      equityActor.forward(command, context());
    } else if (command instanceof EquityCommand) {
      equityActor.forward(command, context());
    } else if (command instanceof MutualFundEvent) {
      mutualFundActor.forward(command, context());
    } else if (command instanceof MutualFundCommand) {
      mutualFundActor.forward(command, context());
    } else if (command instanceof EODCompleted) {
      equityActor.forward(command, context());
      mutualFundActor.forward(command, context());
    } else if (command instanceof EquityDomainUpdationCommands) {
      equityActor.forward(command, context());
    }
  }

}
