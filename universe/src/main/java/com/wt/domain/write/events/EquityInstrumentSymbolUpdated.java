package com.wt.domain.write.events;

import static java.lang.String.valueOf;

import java.io.Serializable;

import lombok.Getter;

public class EquityInstrumentSymbolUpdated implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter
  private String token;
  @Getter
  private String oldSymbol;
  @Getter
  private String newSymbol;
  @Getter
  private String wdId;
  @Getter
  private String description;
  @Getter
  private String exchange;
  @Getter
  private String segment;

  public EquityInstrumentSymbolUpdated(String token, String oldSymbol, String newSymbol, String wdId, String description, String exchange, String segment) {
    super();
    this.token = token;
    this.oldSymbol = oldSymbol;
    this.newSymbol = newSymbol;
    this.wdId = wdId;
    this.description = description;
    this.exchange = exchange;
    this.segment = segment;
  }
  
  public EquityInstrumentSymbolUpdated(int token, String oldSymbol, String newSymbol, String wdId, String description, String exchange, String segment) {
    super();
    this.token = valueOf(token);
    this.oldSymbol = oldSymbol;
    this.newSymbol = newSymbol;
    this.wdId = wdId;
    this.description = description;
    this.exchange = exchange;
    this.segment = segment;
  }

}
