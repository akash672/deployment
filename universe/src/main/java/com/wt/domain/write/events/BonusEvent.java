package com.wt.domain.write.events;

import com.wt.domain.CorporateEventType;
import com.wt.domain.PriceWithPaf;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true,of={"numerator","denominator"})
public class BonusEvent extends EquityCorporateEvent {
  private static final long serialVersionUID = 1L;
  private double numerator;
  private double denominator;

  public BonusEvent(String token, long exDate, double numerator, double denominator) {
    super(token, exDate, CorporateEventType.BONUS);
    this.numerator = numerator;
    this.denominator = denominator;
  }

  @Override
  public double adjustPaf(PriceWithPaf event) {
    return (this.denominator != 0.) ? (this.numerator + this.denominator) / this.denominator : 1.;
  }
}
