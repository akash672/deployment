package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.InvestingMode;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GetBrokerName implements Serializable {

  private static final long serialVersionUID = 1L;
  private String username;
  private InvestingMode investingMode;

}
