package com.wt.domain.write;

import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.DateUtils.isExecutionWindowOpen;
import static com.wt.utils.DateUtils.lastDayEOD;
import static com.wt.utils.DateUtils.nextBusinessDay;
import static com.wt.utils.DateUtils.nextTradingWeekday;
import static com.wt.utils.DateUtils.prettyDate;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.CorporateEventType;
import com.wt.domain.EquityChildActorState;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.GetHistoricalPriceEventsByInstrument;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.GetPrice;
import com.wt.domain.write.commands.GetPricePaf;
import com.wt.domain.write.commands.HistoricalPriceEventsList;
import com.wt.domain.write.commands.SendEODPriceWithPafToDB;
import com.wt.domain.write.commands.StopLivePriceStream;
import com.wt.domain.write.commands.SubscribeAndGetLivePriceStream;
import com.wt.domain.write.commands.SubscribeToToken;
import com.wt.domain.write.commands.UnSubscribeToToken;
import com.wt.domain.write.events.BonusEvent;
import com.wt.domain.write.events.DividendEvent;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.EquityCorporateEvent;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.StockSplitEvent;
import com.wt.utils.JournalProvider;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.http.javadsl.model.StatusCodes;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.RecoveryCompleted;
import akka.persistence.SaveSnapshotFailure;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotMetadata;
import akka.persistence.SnapshotOffer;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Service("EquityChildActor")
@Scope("prototype")
@Log4j
public class EquityChildActor extends AbstractPersistentActor {
  private String id;
  
  private EquityChildActorState equityChildActorState;
  
  private @Autowired JournalProvider journal;
  private @Autowired ActorMaterializer materializer;
  private CtclBroadcast ctclBroadcast;
  private Set<ActorRef> receivers = new HashSet<ActorRef>();
  @Autowired
  public void setCtclBroadcast(CtclBroadcast ctclBroadcast) {
    this.ctclBroadcast = ctclBroadcast;
  }

  public EquityChildActor(String id) {
    super();
    this.id = id;
    equityChildActorState = new EquityChildActorState(id);
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(InstrumentPrice.class, cmd -> {
      price(cmd);
    }).match(SubscribeToToken.class, cmd -> {
      String exchange = cmd.getTicker().split("-")[1];
      tryToAddSubscriptionFor(cmd.getToken(), exchange);
    }).match(UnSubscribeToToken.class, cmd -> {
      String exchange = cmd.getTicker().split("-")[1];
      ctclBroadcast.removeSubscription(cmd.getToken(), exchange);
    }).match(RealTimeInstrumentPrice.class, cmd -> {
      if (isExecutionWindowOpen())
        equityChildActorState.update(cmd);
      PriceWithPaf last = equityChildActorState.last();
      PriceWithPaf currentTimePriceWithPaf = sendLastPriceWithCurrentTimeStamp(last);
      sendLastPriceToSubscribers(currentTimePriceWithPaf);
    }).match(PriceWithPaf.class, cmd -> {
      priceWithPaf(cmd);
    }).match(EquityCorporateEvent.class, cmd -> {
      corporateEvent(cmd);
    }).match(GetPricePaf.class, cmd -> {
      getPriceWithPaf(cmd);
    }).match(GetPrice.class, cmd -> {
      PriceWithPaf last = equityChildActorState.last();
      sender().tell(last == null ? PriceWithPaf.noPrice(cmd.getWdId()) : sendLastPriceWithCurrentTimeStamp(last), self());
    }).match(GetHistoricalPriceEventsByInstrument.class, cmd -> {
      List<PriceWithPaf> priceEvents = listForPriceEvent(cmd.getWdId(),cmd.getStartDate());
      HistoricalPriceEventsList priceEventList=new HistoricalPriceEventsList(cmd.getResponseIdentifier(),priceEvents);
      sender().tell(priceEventList, self());
    }).match(GetLivePriceStream.class, cmd -> {
      if (equityChildActorState.last() != null){
        sender().tell(sendLastPriceWithCurrentTimeStamp(equityChildActorState.last()), self());
      }receivers.add(sender());
    }).match(SubscribeAndGetLivePriceStream.class, cmd -> {
      SubscribeToToken subscribeToToken = new SubscribeToToken(cmd.getWdId());
      subscribeToToken.setToken(cmd.getToken());
      self().tell(subscribeToToken, self());
      self().forward(new GetLivePriceStream(cmd.getWdId()), context());
    }).match(StopLivePriceStream.class, cmd -> {
      receivers.remove(sender());
    }).match(EODCompleted.class, cmd -> {
      log.debug("Saving snapshot of EquityChildActor.");
      saveSnapshot(equityChildActorState.copy());
    }).match(SaveSnapshotFailure.class, cmd -> {
      handleSnapshotFailure(cmd);
    }).match(SaveSnapshotSuccess.class, cmd -> {
      handleSnapshotSuccess(cmd);
    }).build();
  }

  private void handleSnapshotSuccess(SaveSnapshotSuccess snapshotSuccess) {
    SnapshotMetadata metadata = snapshotSuccess.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has sucessfully been persisted with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private void handleSnapshotFailure(SaveSnapshotFailure snapshotFailure) {
    SnapshotMetadata metadata = snapshotFailure.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has failed to persist with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private PriceWithPaf sendLastPriceWithCurrentTimeStamp(PriceWithPaf lastPriceWithPaf) {
    PriceWithPaf currentTimePriceWithPaf = new PriceWithPaf(new InstrumentPrice(lastPriceWithPaf.getPrice().getPrice(),
        currentTimeInMillis(), lastPriceWithPaf.getPrice().getWdId()), lastPriceWithPaf.getPafs());
	return currentTimePriceWithPaf;
  }

  private void tryToAddSubscriptionFor(String ticker, String exchange) {
    try {
      addSubscription(ticker, exchange);
    } catch (Exception e) {
      log.error("Cannot subscribe to " + ticker + " due to " + e.getMessage());
    }
  }

  private void addSubscription(String token, String exchange) {
    log.debug("requested broadcast for wdId " + token + " with exchange " + exchange);
    try {
      ctclBroadcast.addSubscription(token, exchange);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void sendLastPriceToSubscribers(PriceWithPaf priceWithPaf) {
    receivers
    .stream()
    .forEach(receiver -> receiver.tell(priceWithPaf, self()));
  }

  private void getPriceWithPaf(GetPricePaf c) throws ParseException {
    if(equityChildActorState.cumulativePafs().isEmpty()){
      Double defaultPaf = 1.0;
      sender().tell(defaultPaf, self());
      return;
    }
    Double paf = equityChildActorState.getCumulativePaf(prettyDate(c.getTimestamp()));
    long dayBefore = lastDayEOD(c.getTimestamp());
    long lastCumulativePafDate = 0L;
    for (String date : equityChildActorState.cumulativeDates())
    {
      if(eodDateFromExcelDate(date) <= dayBefore){
        lastCumulativePafDate = eodDateFromExcelDate(date);
        break;
      }
    }
    if(lastCumulativePafDate == 0){
      Double defaultPaf = 1.0;
      sender().tell(defaultPaf, self());
      return;
    }
    while (paf == null) {
      paf = equityChildActorState.getCumulativePaf(prettyDate(dayBefore));
      dayBefore = lastDayEOD(dayBefore);
    }
    sender().tell(paf, self());
  }

  protected void price(InstrumentPrice cmd) {
    Pafs adjustPaf = adjustPaf(cmd.getTime(), cmd.getPrice());
    PriceWithPaf priceWithPafs = new PriceWithPaf(cmd, adjustPaf);
    priceWithPaf(priceWithPafs);
  }

  protected void corporateEvent(EquityCorporateEvent cmd) {
    if (validateCorporateEvent(cmd)) {
      persist(cmd, (EquityCorporateEvent evt) -> {
        equityChildActorState.update(evt);
        sender().tell(new Done(evt.getWdId(), " corporate event recevied."), self());
      });
    } else {
      sender().tell(failed(cmd.getWdId(), " Not a valid corporate event.", StatusCodes.BAD_REQUEST), self());
    }
  }

  protected void priceWithPaf(PriceWithPaf cmd) {
    context().parent().tell(new SendEODPriceWithPafToDB(cmd), self());
    persist(cmd, (PriceWithPaf evt) -> {
      equityChildActorState.update(evt);
      sender().tell(new Done(evt.getPrice().getWdId(), " price event recevied."), self());
    });
  }

  private boolean validateCorporateEvent(EquityCorporateEvent cmd) {
    return (equityChildActorState.getCorporateEvent(
        prettyDate(nextTradingWeekday(cmd.getExDate())) + cmd.getEventType().toString()) == null);
  }

  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(PriceWithPaf.class, e -> {
      equityChildActorState.update(e);
    }).match(RecoveryCompleted.class, event -> {
      log.debug("Recovery of EquityChildActor is complete " + persistenceId());
    }).match(EquityCorporateEvent.class, e -> {
      equityChildActorState.update(e);
    }).match(SnapshotOffer.class, snapshotOffer -> {
      log.debug("Received EquityChildActor snapshot " +  persistenceId());
      equityChildActorState = (EquityChildActorState) snapshotOffer.snapshot();
    }).build();
  }

  @Override
  public String persistenceId() {
    return id;
  }

  private Pafs adjustPaf(long time, double price) {
    double dividendValue = (equityChildActorState.last() != null) ? equityChildActorState.last().getDividend() : 0.;
    double nonDividendPaf = (equityChildActorState.last() != null) ? equityChildActorState.last().getNonDividendPaf() : 1.;
    double historicalPaf = (equityChildActorState.last() != null) ? equityChildActorState.last().getHistoricalPaf() : 0.;
    double dividendPaf = 0.;
    double cumulativePaf = 0.;
    long lastPriceProcessedTime = (equityChildActorState.last() != null) ? equityChildActorState.last().getPrice().getTime(): 0L;

    nonDividendPaf = adjustPafForBonus(lastPriceProcessedTime, time, nonDividendPaf);
    nonDividendPaf = adjustPafForSplit(lastPriceProcessedTime, time, nonDividendPaf);
    dividendValue = adjustDividendValue(lastPriceProcessedTime, time, dividendValue);

    if (dividendValue != 0.) {
      dividendPaf = ((price + dividendValue) / (price));
    }

    cumulativePaf = (historicalPaf != 0.) ? (historicalPaf * nonDividendPaf) : nonDividendPaf;
    cumulativePaf = (cumulativePaf != 1.) ? cumulativePaf : 0.;
    cumulativePaf += (dividendPaf);

    // Renormalizing the pafs
    if ((dividendPaf != 0.) && (nonDividendPaf != 1.) && (historicalPaf != 0.) && (historicalPaf != 1.)) {
      cumulativePaf -= 1.;
    } else if ((dividendPaf != 0.) && (nonDividendPaf != 1.)) {
      cumulativePaf -= 1.;
    } else if ((dividendPaf != 0.) && (historicalPaf != 0.) && (historicalPaf != 1.)) {
      cumulativePaf -= 1.;
    }

    cumulativePaf = (cumulativePaf != 0.) ? cumulativePaf : 1.0;
    return new Pafs(cumulativePaf, nonDividendPaf, dividendValue, historicalPaf);
  }

  private double adjustDividendValue(long lastProcessedDividendTime, long latestPricetime, double dividendValue) {
    // Handle Dividends
    lastProcessedDividendTime = 
        (lastProcessedDividendTime != 0L) ? nextBusinessDay(lastProcessedDividendTime) : latestPricetime;
    while(lastProcessedDividendTime <= latestPricetime) {      
      if (equityChildActorState.getCorporateEvent(prettyDate(lastProcessedDividendTime) + CorporateEventType.DIVIDEND.toString()) != null) {
        DividendEvent dividend = (DividendEvent) equityChildActorState.getCorporateEvent(
            prettyDate(lastProcessedDividendTime) + CorporateEventType.DIVIDEND.toString());
        dividendValue += dividend.getDividend();
      }
      lastProcessedDividendTime = nextBusinessDay(lastProcessedDividendTime);
    }
    return dividendValue;
  }

  private double adjustPafForSplit(long lastProcessedSplitTime, long latestPricetime, double nonDividendPaf) {
    // Handle Stock Splits
    lastProcessedSplitTime = 
        (lastProcessedSplitTime != 0L) ? nextBusinessDay(lastProcessedSplitTime) : latestPricetime;
    while(lastProcessedSplitTime <= latestPricetime){      
      if (equityChildActorState.getCorporateEvent(prettyDate(lastProcessedSplitTime) + CorporateEventType.SPLIT.toString()) != null) {
        StockSplitEvent stockSplit = (StockSplitEvent) equityChildActorState.getCorporateEvent(
            prettyDate(lastProcessedSplitTime) + CorporateEventType.SPLIT.toString());
        nonDividendPaf *= (stockSplit.adjustPaf(equityChildActorState.last()));
      }
      lastProcessedSplitTime = nextBusinessDay(lastProcessedSplitTime);
    }

    return nonDividendPaf;
  }

  private double adjustPafForBonus(long lastProcessedBonusTime, long latestPricetime, double nonDividendPaf) {
    // Handle Bonus
    lastProcessedBonusTime = 
        (lastProcessedBonusTime != 0L) ? nextBusinessDay(lastProcessedBonusTime) : latestPricetime;
    while(lastProcessedBonusTime <= latestPricetime) {      
      if (equityChildActorState.getCorporateEvent(prettyDate(lastProcessedBonusTime) + CorporateEventType.BONUS.toString()) != null) {
        BonusEvent bonus = (BonusEvent) equityChildActorState.getCorporateEvent(prettyDate(lastProcessedBonusTime) + CorporateEventType.BONUS.toString());
        nonDividendPaf *= (bonus.adjustPaf(equityChildActorState.last()));
      }
      lastProcessedBonusTime = nextBusinessDay(lastProcessedBonusTime);
    }

    return nonDividendPaf;
  }

  public  List<PriceWithPaf> listForPriceEvent(String wdId,long startDate) throws InterruptedException, ExecutionException {
    Set<Long> uniqueDates = new HashSet<Long>();
    Source<PriceWithPaf, NotUsed> map = journal.currentEventsSourceForPersistenceId(wdId)
        .filter((event) -> event.event() instanceof PriceWithPaf)
        .map((event) -> (PriceWithPaf) event.event())
        .filter((event) -> event.getPrice().getTime() >= startDate)
        .filter((event) -> uniqueDates.add(Long.valueOf(event.getPrice().getTime())));
    List<PriceWithPaf> historicalPriceEvents = map.runWith(Sink.seq(), materializer).toCompletableFuture()
        .get();
    return historicalPriceEvents;
  }
}
