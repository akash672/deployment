package com.wt.domain.write.events;

import java.io.Serializable;

import com.wt.domain.MutualFundInstrument;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MutualFundInstrumentByWdIdSent implements Serializable {

  private static final long serialVersionUID = 1L;
  private MutualFundInstrument instrument;
  
  public static MutualFundInstrumentByWdIdSent noMFInstrument() {
    return new MutualFundInstrumentByWdIdSent(new MutualFundInstrument());
  }
}
