package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class UpdateEquityInstrumentSymbol extends EquityCommand implements Serializable{
  private static final long serialVersionUID = 1L;
  @Getter
  private String token;
  @Getter
  private String oldSymbol;
  @Getter
  private String newSymbol;
  @Getter
  private String wdId;
  @Getter
  private String description;
  @Getter
  private String exchange;
  @Getter
  private String segment;
}
