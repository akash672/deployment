package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.HashMap;

import com.wt.domain.Benchmark;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class BenchmarkDataToRDS extends AssetClassCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  HashMap<String, Benchmark> indexToBenchmarkData;

}
