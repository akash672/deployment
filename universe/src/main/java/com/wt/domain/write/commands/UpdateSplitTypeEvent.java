package com.wt.domain.write.commands;

import com.wt.domain.CorporateEventType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString(callSuper=true)
@Getter
@EqualsAndHashCode(callSuper=true)
public class UpdateSplitTypeEvent extends CorporateEventUpdate{
  
  private static final long serialVersionUID = 1L;
  private double adjustPaf;

  public UpdateSplitTypeEvent(String wdId, double adjustPaf, CorporateEventType eventType, long eventDate) {
    super(wdId, eventType, eventDate);
    this.adjustPaf = adjustPaf;
  }

}
