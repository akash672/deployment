package com.wt.domain.write.events;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.wt.domain.MutualFundInstrument;

import lombok.Getter;
public class MutualFundInstrumentsUpdated implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private Set<MutualFundInstrument> allAdded;
  @Getter private Set<MutualFundInstrument> allDeleted;
  
  public MutualFundInstrumentsUpdated(Set<MutualFundInstrument> allAdded, Set<MutualFundInstrument> allDeleted) {
    super();
    this.allAdded = new HashSet<>(allAdded);
    this.allDeleted = new HashSet<>(allDeleted);
  }
  
  
}
