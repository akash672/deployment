package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;

public class MutualFundInstrumentSchemeNameUpdated implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private String token;
  @Getter private String schemeName;
  
  public MutualFundInstrumentSchemeNameUpdated(String token, String schemeName) {
    super();
    this.token = token;
    this.schemeName = schemeName;
  }

}
