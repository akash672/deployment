package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class UpdateEquityInstrumentToken extends EquityCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  @Getter
  private String wdId;
  @Getter
  private String oldToken;
  @Getter
  private String newToken;
  @Getter
  private String symbol;
  @Getter
  private String exchange;
}
