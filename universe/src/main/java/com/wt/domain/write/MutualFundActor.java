package com.wt.domain.write;

import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.newHashSet;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.DateUtils.prettyDate;

import java.util.HashMap;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets.SetView;
import com.wt.domain.MutualFundActorState;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.BODCompleted;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.GetHistoricalPriceEventsByInstrument;
import com.wt.domain.write.commands.GetInstrumentByWdId;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.GetMutualFundInstrumentByToken;
import com.wt.domain.write.commands.GetPrice;
import com.wt.domain.write.commands.GetPricePaf;
import com.wt.domain.write.commands.SendEODPriceWithPafToDB;
import com.wt.domain.write.commands.SubscribeAndGetLivePriceStream;
import com.wt.domain.write.commands.UpdateMFInstruments;
import com.wt.domain.write.commands.UpdateMutualFundInstrumentSchemeName;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentByWdIdSent;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.MutualFundInstrumentSchemeNameUpdated;
import com.wt.domain.write.events.MutualFundInstrumentsUpdated;
import com.wt.universe.util.UniverseSNSToRDSHandler;

import akka.http.javadsl.model.StatusCodes;
import akka.persistence.RecoveryCompleted;
import akka.persistence.SaveSnapshotFailure;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotMetadata;
import akka.persistence.SnapshotOffer;

@Service("MutualFundActor")
@Scope("prototype")
public class MutualFundActor extends AssetActor implements ParentActor<MutualFundChildActor> {
  private MutualFundActorState mfActorState = new MutualFundActorState();
  private @Autowired UniverseSNSToRDSHandler snsToRDSHandler;
  private HashMap<String, PriceWithPaf> tickerToPriceWithPafMap = new HashMap<String, PriceWithPaf>();
  @Override
  public String getName() {
    return "MF";
  }

  @Override
  public String getChildBeanName() {
    return "MutualFundChildActor";
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(InstrumentPrice.class, cmd -> {
      if (cmd.getWdId().endsWith("-MF"))
        forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(GetInstrumentByWdId.class, cmd -> {
      if (cmd.getWdId().endsWith("-MF"))
        getMutualFundInstrumentByWdId(cmd);
    }).match(UpdateMFInstruments.class, cmd -> {
      updateMFInstruments(cmd);
    }).match(UpdateMutualFundInstrumentSchemeName.class, cmd -> {
      updateMutualFundInstrumentSchemeName(cmd);
    }).match(GetMutualFundInstrumentByToken.class, cmd -> {
      getMutualFundInstrumentByToken(cmd);
    }).match(GetHistoricalPriceEventsByInstrument.class, cmd -> {
      if(cmd.getWdId().endsWith("-MF"))
        forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(PriceWithPaf.class, cmd -> {
      if(cmd.getPrice().getWdId().endsWith("-MF"))
        forwardToChildren(context(), cmd, cmd.getPrice().getWdId());
    }).match(SendEODPriceWithPafToDB.class, cmd -> {
      handleBODPrices(cmd);
    }).match(GetPricePaf.class, cmd -> {
      if(cmd.getToken().endsWith("-MF"))
        forwardToChildren(context(), cmd, cmd.getToken());
    }).match(GetLivePriceStream.class, cmd -> {
      if(cmd.getWdId().endsWith("-MF"))
      forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(SubscribeAndGetLivePriceStream.class, cmd -> {
      if(cmd.getWdId().endsWith("-MF"))
      forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(GetPrice.class, cmd -> {
      if(cmd.getWdId().endsWith("-MF"))
        forwardToChildren(context(), cmd, cmd.getWdId());
    }).match(EODCompleted.class, cmd -> {
      log.debug("Saving snapshot of MutualFundActor.");
      saveSnapshot(mfActorState.copy());
      forwardToChildren(context(), cmd, getChildBeanName());
    }).match(BODCompleted.class, cmd -> {
      log.debug("Sending prices to RDS.");
      sendPricesToRDSHandler();
    }).match(SaveSnapshotFailure.class, cmd -> {
      handleSnapshotFailure(cmd);
    }).match(SaveSnapshotSuccess.class, cmd -> {
      handleSnapshotSuccess(cmd);
    }).build();
  }

  private void handleBODPrices(SendEODPriceWithPafToDB cmd) {
    tickerToPriceWithPafMap.put(cmd.getPriceWithPaf().getPrice().getWdId(), cmd.getPriceWithPaf());
  }

  private void sendPricesToRDSHandler() {
    snsToRDSHandler.handlePriceData(tickerToPriceWithPafMap);
    log.debug("Sending Price Snaphot of BOD to SNS to RDS handler of size: " + tickerToPriceWithPafMap.size());
    tickerToPriceWithPafMap.clear();
  }

  private void handleSnapshotSuccess(SaveSnapshotSuccess snapshotSuccess) {
    SnapshotMetadata metadata = snapshotSuccess.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has sucessfully been persisted with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private void handleSnapshotFailure(SaveSnapshotFailure snapshotFailure) {
    SnapshotMetadata metadata = snapshotFailure.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has failed to persist with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }
  private void updateMutualFundInstrumentSchemeName(UpdateMutualFundInstrumentSchemeName cmd) {

    persist(new MutualFundInstrumentSchemeNameUpdated(cmd.getToken(), cmd.getSchemeName()), event -> {
      mfActorState.update(event);
      sender().tell(new Done((cmd.getToken()),
          "TOKEN: " + cmd.getToken() + " SchemeName: " + cmd.getSchemeName()), self());
    });

  }

  private void updateMFInstruments(UpdateMFInstruments cmd) {
    Set<MutualFundInstrument> yesterday = newHashSet(mfActorState.getAllMutualFundInstruments());
    Set<MutualFundInstrument> today = newHashSet(((UpdateMFInstruments) cmd).getToday());
    SetView<MutualFundInstrument> allDeleted = difference(yesterday, yesterday);
    SetView<MutualFundInstrument> allAdded = difference(today, yesterday);

    persist(new MutualFundInstrumentsUpdated(allAdded, allDeleted), event->{
      mfActorState.update(event);
      sender().tell(new Done("All MF instruments", "All MF instruments"), self());

    });
  }

  private void getMutualFundInstrumentByToken(GetMutualFundInstrumentByToken cmd) {
    MutualFundInstrument instrument = mfActorState.getMutualFundInstrument(cmd.getToken());
    if(instrument == null) {
      log.warn("Error. No MF instrument " + cmd.getToken());
      sender().tell(failed(cmd.getToken(), "MF Instrument not found for " + cmd.getToken(), StatusCodes.NOT_FOUND), self());
    }
    else {
      sender().tell(instrument, self());
    }
  }

  private void getMutualFundInstrumentByWdId(GetInstrumentByWdId cmd){
    MutualFundInstrument mutualFundInstrument = mfActorState.getMutualFundInstrument(getTokenFromWdId(cmd.getWdId()));
    sender().tell(new InstrumentByWdIdSent(mutualFundInstrument), self());
  }

  private String getTokenFromWdId(String wdId) {
    return wdId.substring(0, wdId.indexOf("-"));
  }

  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(MutualFundInstrumentsUpdated.class, event -> {
      mfActorState.update(event);
    }).match(RecoveryCompleted.class, event -> {
      log.debug("Recovery of MutualFundActor is complete");
    }).match(MutualFundInstrumentSchemeNameUpdated.class, event -> {
      mfActorState.update(event);
    }).match(SnapshotOffer.class, snapshotOffer -> {
      log.debug("Received MutualFundActor snapshot");
      mfActorState = (MutualFundActorState) snapshotOffer.snapshot();
    }).build();

  }

  @Override
  public String persistenceId() {
    return "MutualFundActor";
  }

}