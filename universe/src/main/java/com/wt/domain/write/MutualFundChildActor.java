package com.wt.domain.write;

import static com.wt.utils.DateUtils.prettyDate;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.MutualFundChildActorState;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.GetHistoricalPriceEventsByInstrument;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.GetPrice;
import com.wt.domain.write.commands.GetPricePaf;
import com.wt.domain.write.commands.HistoricalPriceEventsList;
import com.wt.domain.write.commands.SendEODPriceWithPafToDB;
import com.wt.domain.write.commands.SubscribeAndGetLivePriceStream;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.JournalProvider;

import akka.NotUsed;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.RecoveryCompleted;
import akka.persistence.SaveSnapshotFailure;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotMetadata;
import akka.persistence.SnapshotOffer;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Service("MutualFundChildActor")
@Scope("prototype")
@Log4j
public class MutualFundChildActor extends AbstractPersistentActor {
  private MutualFundChildActorState mutualFundChildActorState;
  private String id;
  private @Autowired JournalProvider journal;
  private @Autowired ActorMaterializer materializer;
  
  public MutualFundChildActor(String id) {
    super();
    this.id = id;
    mutualFundChildActorState = new MutualFundChildActorState(id);
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(InstrumentPrice.class, cmd -> {
      price(cmd);
    }).match(GetPrice.class, cmd -> {
      sender().tell(mutualFundChildActorState.last() == null ? PriceWithPaf.noPrice(cmd.getWdId()) : mutualFundChildActorState.last(), self());
    }).match(PriceWithPaf.class, cmd -> {
      sendPrice(cmd);
    }).match(GetPricePaf.class, cmd -> {
      getPriceWithPaf(cmd);
    }).match(GetLivePriceStream.class, cmd -> {
      sender().tell(mutualFundChildActorState.last(), self());
    }).match(SubscribeAndGetLivePriceStream.class, cmd -> {
      self().forward(new GetLivePriceStream(cmd.getWdId()), context());
    }).match(GetHistoricalPriceEventsByInstrument.class, cmd -> {
      List<PriceWithPaf> priceEvents = listForPriceEvent(cmd.getWdId(),cmd.getStartDate());
      HistoricalPriceEventsList priceEventList=new HistoricalPriceEventsList(cmd.getResponseIdentifier(), priceEvents);
      sender().tell(priceEventList, self());
    }).match(EODCompleted.class, cmd -> {
      log.debug("Saving snapshot of MutualFundChildActor.");
      saveSnapshot(mutualFundChildActorState.copy());
    }).match(SaveSnapshotFailure.class, cmd -> {
      handleSnapshotFailure(cmd);
    }).match(SaveSnapshotSuccess.class, cmd -> {
      handleSnapshotSuccess(cmd);
    }).build();
  }

  private void handleSnapshotSuccess(SaveSnapshotSuccess snapshotSuccess) {
    SnapshotMetadata metadata = snapshotSuccess.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has sucessfully been persisted with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private void handleSnapshotFailure(SaveSnapshotFailure snapshotFailure) {
    SnapshotMetadata metadata = snapshotFailure.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has failed to persist with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }
  
  private void getPriceWithPaf(GetPricePaf cmd) {
    Double defaultPaf = 1.0;
    sender().tell(defaultPaf, self());
  }

  protected void price(InstrumentPrice cmd) {
    Pafs adjustPaf = new Pafs(1.0, 0.0, 0.0, 0.0);
    PriceWithPaf price = new PriceWithPaf(cmd, adjustPaf);
    sendPrice(price);
  }

  protected void sendPrice(PriceWithPaf cmd) {
    context().parent().tell(new SendEODPriceWithPafToDB(cmd), self());
    persist(cmd, (PriceWithPaf evt) -> {
      mutualFundChildActorState.update(evt); 
      sender().tell(new Done(evt.getPrice().getWdId(), " MF price event recevied."), self());
    });
  }

  public  List<PriceWithPaf> listForPriceEvent(String wdId,long startDate) throws InterruptedException, ExecutionException {
    Source<PriceWithPaf, NotUsed> map = journal.currentEventsSourceForPersistenceId(wdId)
        .filter((event) -> event.event() instanceof PriceWithPaf)
        .map((event) -> (PriceWithPaf) event.event())
        .filter((event) -> event.getPrice().getTime() >= startDate);
    List<PriceWithPaf> historicalPriceEvents = map.runWith(Sink.seq(), materializer).toCompletableFuture()
        .get();
    return historicalPriceEvents;
  }
  
  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(PriceWithPaf.class, e -> {
      mutualFundChildActorState.update(e);
    }).match(RecoveryCompleted.class, event -> {
      log.debug("Recovery of MutualFundChildActor is complete");
    }).match(SnapshotOffer.class, snapshotOffer -> {
      log.debug("Received MutualFundChildActor snapshot");
      mutualFundChildActorState = (MutualFundChildActorState) snapshotOffer.snapshot();
    }).build();

  }
  
  @Override
  public String persistenceId() {
    return id;
  }

}
