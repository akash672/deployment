package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FlushIssueAdviseTrades extends EventWithTimeStamps implements Serializable {
  private static final long serialVersionUID = 1L;
  
  public FlushIssueAdviseTrades() {
    super();
  }
}
