package com.wt.domain.write.events;

import java.io.Serializable;

import com.wt.domain.CorporateEventType;
import com.wt.domain.PriceWithPaf;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper=false,of={"wdId","exDate"})
public abstract class EquityCorporateEvent extends EquityEvent implements Serializable {
  private static final long serialVersionUID = 1L;
  private String wdId;
  private long exDate;
  private CorporateEventType eventType;
  public EquityCorporateEvent(String token, long exDate, CorporateEventType eventType) {
    this.wdId = token;
    this.exDate = exDate;
    this.eventType = eventType;
  }

  public abstract double adjustPaf(PriceWithPaf lastEvent);
}
