package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.Set;

import com.wt.domain.EquityInstrument;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class UpdateEQInstruments extends EquityCommand implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private Set<EquityInstrument> today;
}
