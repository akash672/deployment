package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.CorporateEventType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class CorporateEventUpdate implements Serializable {

  private static final long serialVersionUID = 1L;
  private String wdId;
  private CorporateEventType eventType;
  private long eventDate;

}
