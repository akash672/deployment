package com.wt.domain.write.commands;

import com.wt.domain.CorporateEventType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString(callSuper=true)
@Getter
@EqualsAndHashCode(callSuper=true)
public class UpdateDividendEvent extends CorporateEventUpdate{
  
  private static final long serialVersionUID = 1L;
  private double dividendAmount;

  public UpdateDividendEvent(String wdId, double dividendAmount, CorporateEventType eventType, long eventDate) {
    super(wdId,eventType, eventDate);
    this.dividendAmount = dividendAmount;
  }

}
