package com.wt.domain.write.events;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.wt.domain.EquityInstrument;

import lombok.Getter;
public class EquityInstrumentsUpdated implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private Set<EquityInstrument> allAdded;
  @Getter private Set<EquityInstrument> allDeleted;
  
  public EquityInstrumentsUpdated(Set<EquityInstrument> allAdded, Set<EquityInstrument> allDeleted) {
    super();
    this.allAdded = new HashSet<>(allAdded);
    this.allDeleted = new HashSet<>(allDeleted);
  }
  
  
}
