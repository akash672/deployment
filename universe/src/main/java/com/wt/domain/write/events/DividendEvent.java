package com.wt.domain.write.events;

import com.wt.domain.CorporateEventType;
import com.wt.domain.PriceWithPaf;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true,of="dividend")
public class DividendEvent extends EquityCorporateEvent {
  private static final long serialVersionUID = 1L;
  private double dividend;

  public DividendEvent(String token, long exDate, double dividend) {
    super(token, exDate, CorporateEventType.DIVIDEND);
    this.dividend = dividend;
  }

  @Override
  public double adjustPaf(PriceWithPaf priceEvent) {
    return ((priceEvent != null) && (this.dividend != 0.)) ? 
        (priceEvent.getPrice().getPrice() + this.dividend)/ (priceEvent.getPrice().getPrice()) : 1.;
  }
}
