package com.wt.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;

public class PriceMarshaller implements DynamoDBTypeConverter<String, PriceWithPaf> {

  private Gson gson = new Gson();

  @Override
  public String convert(PriceWithPaf object) {
    return gson.toJson(object);
  }

  @Override
  public PriceWithPaf unconvert(String object) {
    return gson.fromJson(object, PriceWithPaf.class);
  }

}
