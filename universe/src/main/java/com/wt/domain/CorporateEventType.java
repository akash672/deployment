package com.wt.domain;

public enum CorporateEventType {
  DIVIDEND,
  BONUS,
  SPLIT,
}
