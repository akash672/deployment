package com.wt.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.wt.domain.write.events.MutualFundInstrumentSchemeNameUpdated;
import com.wt.domain.write.events.MutualFundInstrumentsUpdated;

import lombok.ToString;

@ToString
public class MutualFundActorState implements Serializable {
  private static final long serialVersionUID = 1L;

  private final Map<String, MutualFundInstrument> mapTokenToMutualFundInstrument;

  public MutualFundActorState() {
    this(new ConcurrentHashMap<>());
  }

  public MutualFundActorState(Map<String, MutualFundInstrument> mapTokenToMutualFundInstrument) {
    this.mapTokenToMutualFundInstrument = mapTokenToMutualFundInstrument;
  }

  public MutualFundActorState copy() {
    return new MutualFundActorState(new ConcurrentHashMap<String, MutualFundInstrument>(mapTokenToMutualFundInstrument));
  }

  public MutualFundInstrument getMutualFundInstrument(String token) {
    return mapTokenToMutualFundInstrument.get(token);
  }

  public int numOfMutualFundInstruments() {
    return mapTokenToMutualFundInstrument.size();
  }

  public void update(Object evt) {
    if (evt instanceof MutualFundInstrumentsUpdated) {
      MutualFundInstrumentsUpdated event = (MutualFundInstrumentsUpdated) evt;
      event.getAllAdded().stream().forEach(instrument->mapTokenToMutualFundInstrument.put(instrument.getToken(), instrument));
    } else if (evt instanceof MutualFundInstrumentSchemeNameUpdated) {
      MutualFundInstrumentSchemeNameUpdated event = (MutualFundInstrumentSchemeNameUpdated) evt;
      MutualFundInstrument instrument = mapTokenToMutualFundInstrument.get(event.getToken());
      instrument.setSchemeName(event.getSchemeName());
      mapTokenToMutualFundInstrument.put(instrument.getWdId(), instrument);
    }
  }

  public Collection<MutualFundInstrument> getAllMutualFundInstruments() {
    return mapTokenToMutualFundInstrument.values();
  }
}
