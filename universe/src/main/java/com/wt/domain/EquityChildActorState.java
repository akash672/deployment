package com.wt.domain;

import static com.wt.utils.DateUtils.nextTradingWeekday;
import static com.wt.utils.DateUtils.prettyDate;
import static com.wt.utils.DoublesUtil.isZero;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.wt.domain.write.RealTimeInstrumentPrice;
import com.wt.domain.write.events.EquityCorporateEvent;
import com.wt.domain.write.events.Pafs;

import lombok.ToString;

@ToString
public class EquityChildActorState implements Serializable {
  private static final long serialVersionUID = 1L;

  private PriceWithPaf last;
  private final Map<String, EquityCorporateEvent> corporateEvents;
  private final Map<String, Double> cumulativePafs;

  public EquityChildActorState(String token) {
    this(PriceWithPaf.noPrice(token), new ConcurrentHashMap<>(), new ConcurrentHashMap<>());
  }

  public EquityChildActorState(PriceWithPaf last, Map<String, EquityCorporateEvent> corporateEvents,
      Map<String, Double> cumulativePafs) {
    this.last = last;
    this.corporateEvents = corporateEvents;
    this.cumulativePafs = cumulativePafs;
  }

  public EquityChildActorState copy() {
    PriceWithPaf priceWithPaf = last != null ? last.clone() : null;
    return new EquityChildActorState(priceWithPaf , new ConcurrentHashMap<>(corporateEvents), new ConcurrentHashMap<>(cumulativePafs));
  }

  public PriceWithPaf last() {
    return last;
  }
  
  public EquityCorporateEvent getCorporateEvent(String token) {
    return corporateEvents.get(token);
  }

  public Double getCumulativePaf(String token) {
    return cumulativePafs.get(token);
  }

  public void update(Object evt) {
    if (evt instanceof PriceWithPaf) {
      PriceWithPaf event = (PriceWithPaf) evt;
      if (last == null || last.getPrice().getTime() < event.getPrice().getTime()) {
        last = event;
      }
      cumulativePafs.put(prettyDate(event.getPrice().getTime()), event.getCumulativePaf());
    } else if (evt instanceof EquityCorporateEvent) {
      EquityCorporateEvent event = (EquityCorporateEvent) evt;
      corporateEvents.put(prettyDate(nextTradingWeekday(event.getExDate())) + event.getEventType().toString(), event);
    } else if (evt instanceof RealTimeInstrumentPrice) {
      RealTimeInstrumentPrice event = (RealTimeInstrumentPrice) evt;
      Pafs lastUpdatedCumulativePaf = Pafs.withCumulativePaf(1.);

      if (last != null)
        lastUpdatedCumulativePaf = last.getPafs();
      if (event.getInstrumentPrice() != null && !isZero(event.getInstrumentPrice().getPrice()))
        last = new PriceWithPaf(event.getInstrumentPrice(), lastUpdatedCumulativePaf);
    }
  }

  public Collection<EquityCorporateEvent> corporateEvents() {
    return corporateEvents.values();
  }

  public Collection<Double> cumulativePafs() {
    return cumulativePafs.values();
  }

  public Set<String> cumulativeDates() {
    return cumulativePafs.keySet();
  }

}
