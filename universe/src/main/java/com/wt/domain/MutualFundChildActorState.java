package com.wt.domain;

import java.io.Serializable;

import lombok.ToString;

@ToString
public class MutualFundChildActorState implements Serializable {
  private static final long serialVersionUID = 1L;

  private PriceWithPaf last;

  public MutualFundChildActorState(String token) {
    this(PriceWithPaf.noPrice(token));
  }

  public MutualFundChildActorState(PriceWithPaf last) {
    this.last = last;
  }

  public MutualFundChildActorState copy() {
    PriceWithPaf paf = last == null ? null : last.clone();
    return new MutualFundChildActorState(paf);
  }

  public PriceWithPaf last() {
    return last;
  }

  public void update(Object evt) {
    if (evt instanceof PriceWithPaf) {
      PriceWithPaf event = (PriceWithPaf) evt;
      if (last == null || last.getPrice().getTime() < event.getPrice().getTime()) {
        last = event;
      }
    }
  }
}
