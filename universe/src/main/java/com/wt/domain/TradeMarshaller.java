package com.wt.domain;

import java.util.Set;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class TradeMarshaller implements DynamoDBTypeConverter<String, Set<Trade>> {
  @Override
  public String convert(Set<Trade> object) {
    return new Gson().toJson(object);
  }

  @Override
  public Set<Trade> unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Set<Trade>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }

}