package com.wt.corporateevents;

import akka.actor.ActorSelection;

public interface CorporateEventSource {
  public abstract void notifyCorporateEventArrival(ActorSelection instrumentManagerActor) throws Exception;
}
