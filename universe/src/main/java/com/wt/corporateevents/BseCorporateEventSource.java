package com.wt.corporateevents;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import com.wt.config.utils.WDProperties;
import com.wt.domain.Instrument;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.events.BonusEvent;
import com.wt.domain.write.events.DividendEvent;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.StockSplitEvent;
import com.wt.utils.DateUtils;
import com.wt.utils.aws.SESEmailUtil;
import com.wt.utils.email.templates.wealthdesk.CorporateEventNotificationEmail;

import akka.actor.ActorSelection;

@lombok.extern.log4j.Log4j
public class BseCorporateEventSource implements CorporateEventSource {
  @Autowired private SESEmailUtil emailerService;
  @Autowired private WDProperties properties;
  private static String CSV_SPLIT_BY = ",";
  private static String ZERO = "0";

  private URL bseCorporateEvents;

  public BseCorporateEventSource(String baseUrl) {
    try {
      bseCorporateEvents = new URL(baseUrl);
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void notifyCorporateEventArrival(ActorSelection instrumentManagerActor) throws Exception {
    parseAndNotify(instrumentManagerActor);
  }

  private void parseAndNotify(ActorSelection instrumentManagerActor) throws Exception {
    Pattern dividendPattern = Pattern.compile("Dividend");
    Pattern splitPattern = Pattern.compile("Split");
    Pattern bonusPattern = Pattern.compile("Bonus");
    Pattern mergerDemergerPattern = Pattern.compile("Spin|Amalgamation|Scheme");

    Pattern valuePattern = Pattern.compile("(\\d+(?:\\.\\d+)?)");
    try (BufferedReader in = new BufferedReader(new InputStreamReader(bseCorporateEvents.openStream()))) {
      String line = "";
      boolean header = true;

      while ((line = in.readLine()) != null) {
        try {
          line = line.replace("\"", "");
          String[] recordLine = line.split(CSV_SPLIT_BY);
          if (header) {
            header = false;
            continue;
          }


          Matcher dividendMatcher = dividendPattern.matcher(recordLine[4]);
          Matcher splitMatcher = splitPattern.matcher(recordLine[4]);
          Matcher bonusMatcher = bonusPattern.matcher(recordLine[4]);
          Matcher mergerDemergerMatcher = mergerDemergerPattern.matcher(recordLine[4]);
          if ((dividendMatcher.find())) {
            handleDividends(instrumentManagerActor, recordLine, dividendPattern, valuePattern);
          }

          if (bonusMatcher.find()) {
            handleBonus(instrumentManagerActor, recordLine, bonusPattern, valuePattern);
          }

          if (splitMatcher.find()) {
            handleSplits(instrumentManagerActor, recordLine, splitPattern, valuePattern);
          }
          if (mergerDemergerMatcher.find()) {
            handleMergerDemerger(instrumentManagerActor, recordLine, mergerDemergerPattern, valuePattern);
          }
        } catch (Exception e) {
          log.error("Error while storing CE ", e);
        }
      }
    } catch (Exception e) {
      log.error("Error ", e);
      throw e;
    }
  }

  private void handleMergerDemerger(ActorSelection instrumentManagerActor, String[] record,Pattern mergerDemergerPattern,
      Pattern valuePattern) throws IOException {
    Matcher mergerDemergerMatcher = mergerDemergerPattern.matcher(record[4]);
    if(mergerDemergerMatcher.find())
    {
      String symbol = record[1];
      CorporateEventNotificationEmail template = new CorporateEventNotificationEmail(properties.DEV_TEAM_EMAILIDS(), properties.environment(), symbol);
      if(properties.isSendingUserEmailUpdatesAllowed())
        emailerService.sendEmail(template);
  }
    }
    

  public void handleSplits(ActorSelection instrumentManagerActor, String[] record, Pattern splitPattern,
      Pattern valuePattern) throws Exception {
    Matcher splitMatcher = splitPattern.matcher(record[4]);
    if (splitMatcher.find()) {
      String faceValueRemaining = record[4].substring(splitMatcher.end());
      Matcher valueMatcher = valuePattern.matcher(faceValueRemaining);

      String from = "";
      if (valueMatcher.find()) {
        from = faceValueRemaining.substring(valueMatcher.start(), valueMatcher.end());
      }

      String to = "";
      if (valueMatcher.find()) {
        to = faceValueRemaining.substring(valueMatcher.start(), valueMatcher.end());
      }
      
      String wdId = getWdId(record, instrumentManagerActor);
      if(wdId != null){
        StockSplitEvent splitEvent = new StockSplitEvent(getWdId(record, instrumentManagerActor),
            DateUtils.bodDateFromExcelDate(record[3], "dd-MM-yyyy", "-"), Double.parseDouble(from),
            Double.parseDouble(to));
        try {
          askAndWaitForResult(instrumentManagerActor, splitEvent);
        } catch (Exception e) {
          log.error(e);
        }
      }
    }
  }

  public void handleBonus(ActorSelection instrumentManagerActor, String[] record, Pattern bonusPattern,
      Pattern valuePattern) throws Exception {
    Matcher bonusMatcher = bonusPattern.matcher(record[4]);
    if (bonusMatcher.find()) {
      String bonusRemaining = record[4].substring(bonusMatcher.end());
      Matcher valueMatcher = valuePattern.matcher(bonusRemaining);
      String numerator = "";
      if (valueMatcher.find()) {
        numerator = bonusRemaining.substring(valueMatcher.start(), valueMatcher.end());
      }
      String denominator = "";
      if (valueMatcher.find()) {
        denominator = bonusRemaining.substring(valueMatcher.start(), valueMatcher.end());
      }
      String wdId = getWdId(record, instrumentManagerActor);
      if(wdId != null){
        BonusEvent bonusEvent = new BonusEvent(getWdId(record, instrumentManagerActor),
            DateUtils.bodDateFromExcelDate(record[3], "dd-MM-yyyy", "-"), Double.parseDouble(numerator),
            Double.parseDouble(denominator));
  
        try {
          askAndWaitForResult(instrumentManagerActor, bonusEvent);
        } catch (Exception e) {
          log.error(e);
        }
      }
    }
  }

  public void handleDividends(ActorSelection instrumentManagerActor, String[] record, Pattern dividendPattern,
      Pattern valuePattern) throws Exception {
    Matcher dividendMatcher = dividendPattern.matcher(record[4]);

    Double cumulativeDividendValue = 0.;
    while (dividendMatcher.find()) {
      String dividendRemaining = record[4].substring(dividendMatcher.end());

      Matcher valueMatcher = valuePattern.matcher(dividendRemaining);
      String dividendValue = (valueMatcher.find()) ? valueMatcher.group(1) : ZERO;
      if (dividendValue.equals(ZERO)) {
        continue;
      }

      cumulativeDividendValue += (Double.parseDouble(dividendValue));
    }
    if (cumulativeDividendValue != 0.) {
      String wdId = getWdId(record, instrumentManagerActor);
      if(wdId != null){
        DividendEvent dividendEvent = new DividendEvent(wdId,
            DateUtils.bodDateFromExcelDate(record[3], "dd-MM-yyyy", "-"), cumulativeDividendValue);
        try {
          askAndWaitForResult(instrumentManagerActor, dividendEvent);
        } catch (Exception e) {
          log.error(e);
        }
      }
    }
  }

  private String getWdId(String[] record, ActorSelection instrumentManagerActor) throws Exception {
    Object maybeInstrument = askAndWaitForResult(instrumentManagerActor, new GetEquityInstrumentBySymbol(record[1], "BSE"));
    if(maybeInstrument instanceof Failed){
      return null;
    }
    Instrument instrument = (Instrument) maybeInstrument;
    return instrument.getWdId();
  }

}
