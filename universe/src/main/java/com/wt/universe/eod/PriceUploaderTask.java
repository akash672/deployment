package com.wt.universe.eod;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.ForceSaveSnapshot;
import com.wt.prices.InstrumentPriceSource;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import lombok.extern.log4j.Log4j;

@Service("PriceUploaderTask")
@Order(value=4)
@Log4j
public class PriceUploaderTask implements UniverseEODTask {
  private List<InstrumentPriceSource> priceSources;
  private WDActorSelections actorSelections;
  private WDProperties wdProperties;

  @Autowired
  public PriceUploaderTask(@Qualifier("securitiesPriceSources") List<InstrumentPriceSource> priceSources, @Qualifier("WDActors") WDActorSelections actorSelections) {
    super();
    this.priceSources = priceSources;
    this.actorSelections = actorSelections;
  }

  @Autowired
  public void setWDProperties(WDProperties properties) {
    this.wdProperties = properties;
  }

  public void run() throws Exception {
    log.info("Running PriceUploaderTask");
    populatePricesFromAllSources();
    log.info("Ran PriceUploaderTask");
    log.info("Sending EOD Completion message to the WD Actor System");
    actorSelections.advisersRootActor().tell(new EODCompleted(), null);
    actorSelections.userRootActor().tell(new EODCompleted(), null);
    actorSelections.universeRootActor().tell(new EODCompleted(), null);
    actorSelections.equityOrderRootActor().tell(new EODCompleted(), null);
    if (wdProperties.forceSnapshotAtEOD()) {
      actorSelections.userRootActor().tell(new ForceSaveSnapshot(), null);
    }
  }

  private void populatePricesFromAllSources() throws Exception {
    for (InstrumentPriceSource instrumentsPriceSource : priceSources)
      instrumentsPriceSource.notifyPriceArrival(actorSelections.universeRootActor(),
          DateUtils.eodDateToday());
  }
}