package com.wt.universe.eod;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.wt.corporateevents.CorporateEventSource;
import com.wt.utils.akka.WDActorSelections;

import lombok.extern.log4j.Log4j;

@Service("CorporateEventUploaderTask")
@Order(value = 3)
@Log4j
public class CorporateEventUploaderTask implements UniverseEODTask {
  private List<CorporateEventSource> corporateEventSources;
  private WDActorSelections actorSelections;

  @Autowired
  public CorporateEventUploaderTask(List<CorporateEventSource> corporateEventSources,
      @Qualifier("WDActors") WDActorSelections actorSelections) {
    super();
    this.corporateEventSources = corporateEventSources;
    this.actorSelections = actorSelections;
  }

  public void run() throws Exception {
    log.info("Running Corporate Event uploader task");
    populateCorporateEventsFromAllSources();
    log.info("Ran Corporate Event uploader task");
  }

  private void populateCorporateEventsFromAllSources() throws Exception {
    for (CorporateEventSource corporateEventSource : corporateEventSources)
      corporateEventSource.notifyCorporateEventArrival(actorSelections.universeRootActor());
  }
}