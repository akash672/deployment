package com.wt.universe.eod;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j;

@Log4j
@Service("UniverseSchedulers")
public class UniverseSchedulers {
  private volatile boolean isRunning = false;
  private List<UniverseEODTask> eodTasks;
  private boolean scheduleEODJobs; 

  @Autowired
  public UniverseSchedulers(List<UniverseEODTask> eodTasks, @Value("${scheduleEODJobs}") boolean flag) {
    super();
    log.info("Created EOD UniverseSchedulers with " + eodTasks);

    this.eodTasks = eodTasks;
    this.scheduleEODJobs = flag;
  }

  @Async
  @Scheduled(cron = "${eod.schedule}", zone = "Asia/Kolkata")
  public void scheduleJobs() throws Exception {
    if (!scheduleEODJobs) {
      return;
    }

    log.info("Running scheduled EOD tasks " + eodTasks);
    if(isRunning) {
      log.info("Already running scheduled EOD tasks");
      return;
    }
    isRunning = true;
    try {
      for (UniverseEODTask eodTask : eodTasks) {
        log.info("Running EOD : " + eodTask);
        eodTask.runIfNotRanAlready();
      }
    } catch (Exception e) {
      log.error("Error while executing EOD task ", e);
    }
    isRunning = false;
    log.info("Ran EOD scheduled tasks");
  }

}