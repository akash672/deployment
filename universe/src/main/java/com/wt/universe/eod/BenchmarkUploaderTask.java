package com.wt.universe.eod;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.wt.benchmarks.BenchmarkSource;
import com.wt.domain.Benchmark;
import com.wt.universe.util.UniverseSNSToRDSHandler;

import lombok.extern.log4j.Log4j;
import rx.Observable;

@Service("BenchmarkUploaderTask")
@Order(value = 2)
@Log4j
public class BenchmarkUploaderTask implements UniverseEODTask {
  private List<BenchmarkSource> benchmarkSources;
  private @Autowired UniverseSNSToRDSHandler snsToRDSHandler;

  @Autowired
  public BenchmarkUploaderTask(List<BenchmarkSource> benchmarkSources) {
    this.benchmarkSources = benchmarkSources;
  }

  public void run() throws IOException, Exception {
    log.info("Running BenchmarkUploaderTask");
    populateBenchmarkFromAllSources();
    log.info("Ran BenchmarkUploaderTask");
  }

  private void populateBenchmarkFromAllSources() throws Exception {
    for (BenchmarkSource benchmarkSource : benchmarkSources) {
      Observable<Benchmark> benchmarks = benchmarkSource.benchmarks();
      benchmarks.subscribe();
      benchmarks.doOnError(e -> log.error(e));
    }
    sendBenchmarkDataToSNSToRdsHandler();
  }

  private void sendBenchmarkDataToSNSToRdsHandler() {
    for (BenchmarkSource benchmarkSource : benchmarkSources) {
      HashMap<String, Benchmark> benchmarkDataMap = benchmarkSource.getEODBenchmarkData();
      snsToRDSHandler.handleBenchmarkData(benchmarkDataMap);
      log.debug("Sending Benchmark Snaphot of EOD to SNS to RDS handler of size: " + benchmarkDataMap.size());
      benchmarkSource.clearEODMaps();
    }
  }
}