package com.wt.universe.eod;

import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.newHashSet;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets.SetView;
import com.wt.domain.EquityInstrument;
import com.wt.domain.repo.EquityInstrumentRepository;
import com.wt.domain.write.commands.UpdateEQInstruments;
import com.wt.domain.write.commands.UpdateEquityInstrumentDescription;
import com.wt.domain.write.commands.UpdateEquityInstrumentSegment;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateEquityInstrumentToken;
import com.wt.instruments.EquityUniverseSource;
import com.wt.rds.domain.EquityInstrumentRDS;
import com.wt.rds.domain.EquityInstrumentRDSRepo;
import com.wt.utils.akka.WDActorSelections;

import lombok.extern.log4j.Log4j;
import rx.Observable;

@Service("EquityUniverseUploaderTask")
@Order(value=1)
@Log4j
public class EquityUniverseUploaderTask implements UniverseEODTask {
  private List<EquityUniverseSource> equityUniverseSource;
  private WDActorSelections actorSelections;
  private EquityInstrumentRepository instrumentRepository;
  private EquityInstrumentRDSRepo instrumentRdsRepo;
  @Autowired
  public EquityUniverseUploaderTask(List<EquityUniverseSource> equityUniverseSource,
      WDActorSelections actorSelections, EquityInstrumentRepository equityRepo, EquityInstrumentRDSRepo instrumentRdsRepo) {
    this.equityUniverseSource = equityUniverseSource;
    this.actorSelections = actorSelections;
    this.instrumentRepository = equityRepo;
    this.instrumentRdsRepo = instrumentRdsRepo;
  }

  public void run() throws Exception {
    log.info("Running EOD EquityUniverseUploaderTask");
    Set<EquityInstrument> today = newHashSet(today());
    
    askAndWaitForResult(actorSelections.universeRootActor(), new UpdateEQInstruments(today));
    
    Set<EquityInstrument> yesterday = newHashSet(getEQInstruments());
    
    SetView<EquityInstrument> allAdded = difference(today, yesterday);
    Set<EquityInstrument> allUpdated = new HashSet<EquityInstrument>();
    allAdded.stream().forEach(equityInstrument -> allUpdated.add(equityInstrument));
    for (EquityInstrument todayEQInstrument : today) {
      for (EquityInstrument yestEQInstrument : yesterday) {
        if (todayEQInstrument.getWdId().equals(yestEQInstrument.getWdId())) {
          if (!todayEQInstrument.getSymbol().equals(yestEQInstrument.getSymbol())) {
            askAndWaitForResult(actorSelections.universeRootActor(), new UpdateEquityInstrumentSymbol(
                todayEQInstrument.getToken(), yestEQInstrument.getSymbol(), todayEQInstrument.getSymbol(), todayEQInstrument.getWdId(),
                todayEQInstrument.getDescription(), todayEQInstrument.getExchange(), todayEQInstrument.getSegment()));
            allUpdated.add(todayEQInstrument);
            break;
          }
          if (!todayEQInstrument.getDescription().equals(yestEQInstrument.getDescription())) {
            askAndWaitForResult(actorSelections.universeRootActor(),
                new UpdateEquityInstrumentDescription(todayEQInstrument.getToken(), todayEQInstrument.getSymbol(),
                    todayEQInstrument.getDescription(), todayEQInstrument.getExchange(), todayEQInstrument.getWdId()));
           allUpdated.add(todayEQInstrument);
            break;
          }
          if (!todayEQInstrument.getToken().equals(yestEQInstrument.getToken())) {
            askAndWaitForResult(actorSelections.universeRootActor(), new UpdateEquityInstrumentToken(
                yestEQInstrument.getWdId(), yestEQInstrument.getToken(), todayEQInstrument.getToken(), todayEQInstrument.getSymbol(), todayEQInstrument.getExchange()));
            allUpdated.add(todayEQInstrument);
            break;
          }
          if (!todayEQInstrument.getSegment().equals(yestEQInstrument.getSegment())) {
            askAndWaitForResult(actorSelections.universeRootActor(),
                new UpdateEquityInstrumentSegment(todayEQInstrument.getWdId(), todayEQInstrument.getSymbol(), todayEQInstrument.getExchange(), 
                    todayEQInstrument.getSegment()));
            allUpdated.add(todayEQInstrument);
            break;
          }
        }
      }
    }
    log.info("Equity instruments udpated today + "+allUpdated.size());
    addOneByOneAsRepoDoesNotSupportAddAll(allUpdated);
    log.info("Ran EOD EquityUniverseUploaderTask");
  }

  private Set<EquityInstrument> getEQInstruments() {
    Set<EquityInstrument> set = new HashSet<EquityInstrument>();
    Iterator<EquityInstrument> iterator = instrumentRepository.findAll().iterator();
    while (iterator.hasNext()) {
      EquityInstrument instrument = (EquityInstrument) iterator.next();
      if (instrument.getWdId().endsWith("-EQ"))
          set.add(instrument);
    }
    return set;
  }
  
  private void addOneByOneAsRepoDoesNotSupportAddAll(Set<EquityInstrument> allAdded) {
    List<EquityInstrumentRDS> instrumentsForRDS = new ArrayList<EquityInstrumentRDS>();
    for (EquityInstrument instrument : allAdded) {
      instrumentRepository.save(instrument);
      instrumentsForRDS.add(new EquityInstrumentRDS(instrument.getWdId(), instrument.getSymbol()));
    }
    instrumentRdsRepo.saveAll(instrumentsForRDS);
  }

  private List<EquityInstrument> today() {
    List<EquityInstrument> allInstruments = new ArrayList<EquityInstrument>();
    for (EquityUniverseSource securitiesUniverseSource : equityUniverseSource) {
      Observable<EquityInstrument> instruments = securitiesUniverseSource.equityInstruments();
      instruments.doOnError(e -> log.error(e));
      instruments.forEach(instrument -> allInstruments.add(instrument));
    }
    return allInstruments;
  }
}