package com.wt.universe.util;

import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.newHashSet;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets.SetView;
import com.wt.domain.EquityInstrument;
import com.wt.domain.repo.EquityInstrumentRepository;
import com.wt.domain.write.commands.UpdateEQInstruments;
import com.wt.instruments.EquityUniverseSource;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import rx.Observable;

@Service("EquityUniverseUploader")
public class EquityUniverseUploader {

  private List<EquityUniverseSource> equityUniverseSource;
  private EquityInstrumentRepository equityInstrumentRepository;
  @Autowired
  public EquityUniverseUploader(@Qualifier("equityUniverseSource") List<EquityUniverseSource> equityUniverseSource,
      WDActorSelections actorSelections, EquityInstrumentRepository equityRepo) {
    this.equityUniverseSource = equityUniverseSource;
    this.equityInstrumentRepository = equityRepo;
  }

  public static void main(String args[]) throws Exception {
    //
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CtclAppConfiguration.class)) {
      final ActorSystem system = (ActorSystem) ctx.getBean("actorSystem");
      @SuppressWarnings("unused")
      final ActorRef universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
      final WDActorSelections actorSelection = (WDActorSelections) ctx.getBean(WDActorSelections.class);
      EquityUniverseUploader equityUniverseUploader = (EquityUniverseUploader) ctx.getBean("EquityUniverseUploader");
      equityUniverseUploader.setupUniverse(actorSelection);
    }
  }

  public void setupUniverse(WDActorSelections actorSelections) throws Exception {
    Set<EquityInstrument> today = newHashSet(today());
    
    askAndWaitForResult(actorSelections.universeRootActor(), new UpdateEQInstruments(today));
    
    Set<EquityInstrument> yesterday = newHashSet(getEQInstruments());
    
    SetView<EquityInstrument> allAdded = difference(today, yesterday);
    addOneByOneAsRepoDoesNotSupportAddAll(allAdded);
  }

  private Set<EquityInstrument> getEQInstruments() {
    Set<EquityInstrument> set = new HashSet<EquityInstrument>();
    Iterator<EquityInstrument> iterator = equityInstrumentRepository.findAll().iterator();
    while (iterator.hasNext()) {
      EquityInstrument equityInstrument = (EquityInstrument) iterator.next();
      if (equityInstrument.getWdId().endsWith("-EQ"))
          set.add(equityInstrument);
    }
    return set;
  }
  
  private void addOneByOneAsRepoDoesNotSupportAddAll(Set<EquityInstrument> allAdded) {
    for (EquityInstrument equityInstrument : allAdded) {
      equityInstrumentRepository.save(equityInstrument);
    }
  }

  private List<EquityInstrument> today() {
    List<EquityInstrument> allEQInstruments = new ArrayList<EquityInstrument>();
    for (EquityUniverseSource securitiesUniverseSource : equityUniverseSource) {
      Observable<EquityInstrument> equityInstruments = securitiesUniverseSource.equityInstruments();
      equityInstruments.forEach(equityInstrument -> allEQInstruments.add(equityInstrument));
    }
    return allEQInstruments;
  }
}
