package com.wt.universe.util;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.wt.utils.DateUtils;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import lombok.extern.log4j.Log4j;

@Log4j
public class BhaavCopyUploader {
  public static void main(String[] args) throws Exception {
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CtclAppConfiguration.class)) {
      final ActorSystem system = (ActorSystem) ctx.getBean("actorSystem");
      final ActorRef universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
      PriceExtractor extract = new PriceExtractor();
      long startTime = DateUtils.currentTimeInMillis();
      log.info("Reading the file ....");
      DateFormat indiaFormat = new SimpleDateFormat("dd-MM-yyyy");
      indiaFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
      Date startDate = indiaFormat.parse(args[1]);
      Date endDate = indiaFormat.parse(args[2]);
      extract.readFilesFromFolder(args[0], startDate, endDate, args[3], universeRootActor);
      long endTime = DateUtils.currentTimeInMillis();
      long timeElapsed = endTime - startTime;
      log.debug("Total time taken is: " + timeElapsed);
    }
  }
}
