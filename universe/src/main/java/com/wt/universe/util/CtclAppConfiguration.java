package com.wt.universe.util;

import static com.google.common.collect.Lists.newArrayList;
import static com.typesafe.config.ConfigFactory.load;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.typesafe.config.Config;
import com.wt.benchmarks.BenchmarkSource;
import com.wt.benchmarks.NseBenchmarkSource;
import com.wt.broadcast.mosl.MoslBroadcast;
import com.wt.broadcast.xneutrino.XNeutrinoBroadcast;
import com.wt.corporateevents.BseCorporateEventSource;
import com.wt.corporateevents.CorporateEventSource;
import com.wt.corporateevents.NseCorporateEventSource;
import com.wt.ctcl.CTCLMock;
import com.wt.ctcl.CorporateEventCTCLMock;
import com.wt.ctcl.VirtualMutualFundInteractive;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.repo.MutualFundInstrumentRepository;
import com.wt.instruments.BSEUniverseSource;
import com.wt.instruments.EquityUniverseSource;
import com.wt.instruments.MFUniverseSource;
import com.wt.instruments.NSEUniverseSource;
import com.wt.prices.BSEFileNameExtractor;
import com.wt.prices.BSEPriceSource;
import com.wt.prices.InstrumentPriceSource;
import com.wt.prices.MFFileNameExtractor;
import com.wt.prices.MFPriceSource;
import com.wt.prices.NSEFileNameExtractor;
import com.wt.prices.NSEPriceSource;
import com.wt.universe.bod.CorporateEventBODTask;
import com.wt.universe.bod.MFPriceUploaderTask;
import com.wt.universe.bod.MFUniverseUploaderTask;
import com.wt.universe.bod.UniverseBODTask;
import com.wt.utils.CommonAppConfiguration;
import com.wt.utils.JournalProvider;
import com.wt.utils.JournalProviderProduction;
import com.wt.utils.akka.SpringExtension;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan(basePackages = "com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@EnableJpaRepositories(basePackages = "com.wt.rds.domain" , entityManagerFactoryRef = "entityManager" , transactionManagerRef = "transactionManager")
@PropertySource("classpath:environment.properties")
public class CtclAppConfiguration extends CommonAppConfiguration {
  private ApplicationContext applicationContext;
  @Autowired
  public CtclAppConfiguration(ApplicationContext applicationContext) {
    super();
    this.applicationContext = applicationContext;
  }
  
  @Autowired
  protected MutualFundInstrumentRepository mutualFundInstrumentRepository;
  
  @Bean(name="securitiesPriceSources")
  @Primary
  public List<InstrumentPriceSource> securitiesPriceSources() {
    return newArrayList(nsePriceSource(), bsePriceSource());
  }

  @Bean(name="mutualFundPriceSources")
  public List<MFPriceSource> mutualFundPriceSources() {
    return newArrayList(mfPriceSource());
  }
  
  @Bean
  public InstrumentPriceSource nsePriceSource() {
    return new NSEPriceSource(new NSEFileNameExtractor("https://www.nseindia.com/content/historical/EQUITIES/"), wdActorSelections);
  }

  @Bean
  public MFPriceSource mfPriceSource() {
    return new MFPriceSource(new MFFileNameExtractor("https://www.amfiindia.com/spages/"), wdActorSelections);
  }

  @Bean
  public InstrumentPriceSource bsePriceSource() {
    return new BSEPriceSource(new BSEFileNameExtractor("https://www.bseindia.com/download/BhavCopy/Equity/"));
  }

  @Bean(name="equityUniverseSource")
  public List<EquityUniverseSource> equityUniverseSource() {
    return newArrayList(nse(), bse());
  }

  @Bean(name="mfUniverseSources")
  public List<MFUniverseSource> mfUniverseSources() {
    return newArrayList(mf());
  }

  @Bean
  public EquityUniverseSource nse() {
    return new NSEUniverseSource("ftp://ftpguest:FTPGUEST@ftp.connect2nse.com/COMMON/NTNEAT/security.gz");
  }

  @Bean
  public EquityUniverseSource bse() {
    return new BSEUniverseSource("https://www.bseindia.com/downloads/Help/file/scrip.zip");
  }

  @Bean
  public MFUniverseSource mf() {
    return new MFUniverseSource("https://s3.ap-south-1.amazonaws.com/bse-star-scheme-master/", "SCHMSTRDET.txt");
  }
  
  @Bean
  public List<BenchmarkSource> benchmarkSources() {
    return newArrayList(nseBenchmarks());
  }

  @Bean
  public List<CorporateEventSource> corporateEventSources() {
    return newArrayList(nseCorporateEventSource(), bseCorporateEventSource());
  }

  @Bean
  public CorporateEventSource nseCorporateEventSource() {
    return new NseCorporateEventSource("https://www.nseindia.com/corporates/datafiles/CA_ALL_FORTHCOMING.csv");
  }
  
  @Bean
  public CorporateEventSource bseCorporateEventSource() {
    return new BseCorporateEventSource("https://s3.amazonaws.com/bse-corporate-events/Corporate_Actions.csv");
  }

  @Bean
  public BenchmarkSource nseBenchmarks() {
    return new NseBenchmarkSource("https://www.nseindia.com/content/indices/");
  }
  
  
  @Bean
  public List<CtclInteractive> listOfCTCLs() throws Exception {
    return newArrayList(mockCTCL(), corporateEventCtclMock());
  }
  
  @Bean
  public Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation() throws Exception {
    Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation = new HashMap<BrokerType, CtclInteractive>();
    listOfCTCLs()
    .stream()
    .forEach(ctcl -> brokerTypeWithCTCLImplementation.put(ctcl.getBrokerType(), ctcl));
    return brokerTypeWithCTCLImplementation;
  }

  @Bean( name = "VirtualMutualFundInteractive")
  public MutualFundInteractive virtualMutualFundInteractive(){
    return new VirtualMutualFundInteractive(wdActorSelections);
  }
  
  @Bean( name = "CorporateEventBODTask")
  public CorporateEventBODTask corporateEventBODTask(){
    return new CorporateEventBODTask(wdActorSelections);
  }
  
  @Bean( name = "MFUniverseUploaderTask")
  public MFUniverseUploaderTask mfUniverseUploaderTask(){
    return new MFUniverseUploaderTask(mfUniverseSources(),wdActorSelections,mutualFundInstrumentRepository);
  }
  
  @Bean( name = "MFPriceUploaderTask")
  public MFPriceUploaderTask mfPriceUploaderTask(){
    return new MFPriceUploaderTask(mutualFundPriceSources(),wdActorSelections);
  }
  
  @Bean( name = "BODTasksToExecute")
  public List<UniverseBODTask> bodTaksToExecute() throws Exception{
    List<UniverseBODTask> bodTaksToExecute = new ArrayList<>();
    listOfBODTaks()
    .stream()
    .forEach(bodTaks -> {
      try {
        if(wdProperties.BOD_Taks().contains(bodTaks.getClass().getSimpleName()))
          bodTaksToExecute.add(bodTaks);
      } catch (Exception e) {
        e.printStackTrace();
        bodTaksToExecute.add(bodTaks);
      }
    });
    return bodTaksToExecute;
  }
  
  @Bean
  public List<UniverseBODTask> listOfBODTaks() throws Exception {
    return newArrayList(corporateEventBODTask(),mfUniverseUploaderTask(),mfPriceUploaderTask());
  }
  
  @Bean
  public List<MutualFundInteractive> listOfMFCTCLs() throws Exception {
    return newArrayList(virtualMutualFundInteractive());
  }
  
  @Bean
  public Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation() throws Exception {
    Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation = new HashMap<BrokerType, MutualFundInteractive>();
    listOfMFCTCLs()
    .stream()
    .forEach(ctcl -> brokerTypeWithMFCTCLImplementation.put(ctcl.getBrokerType(), ctcl));
    return brokerTypeWithMFCTCLImplementation;
  }
  
  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() throws IOException {
    setSystemProperties();
    String actorName = "Universe";
    Config conf = load("Universe-application.conf");
    if (System.getProperty("appConfig") != null) {
      conf = load(System.getProperty("appConfig"));
      actorName = System.getProperty("appConfig").split("-")[0];
    }
    ActorSystem system = ActorSystem.create(actorName, conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }
  
  @Bean(name="JournalProvider")
  public JournalProvider journalProvider() throws IOException
  {
    JournalProviderProduction journalProviderProduction = new JournalProviderProduction(actorSystem());
    return journalProviderProduction;
  }
  
  @Lazy
  @Bean
  public  BroadCastManager broadCastManager() {
    return new BroadCastManager(wdProperties.XNEUTRINO_BROADCAST_IP(), wdProperties.XNEUTRINO_BROADCAST_PORT());
  }
  
  @Bean
  public CtclBroadcast ctclBroadcast() throws Exception {
    if(wdProperties.ACTIVE_BROKERS().get(0).equals("BPWEALTH"))
      return new XNeutrinoBroadcast(broadCastManager(), wdActorSelections, wdProperties);
    else
      return new MoslBroadcast(wdActorSelections, wdProperties);
  }

  @Bean(name = "CTCLMock")
  public CtclInteractive mockCTCL() throws IOException {
    return new CTCLMock(actorSystem(), wdActorSelections);
  }
  
  @Bean(name = "CorporateEventCTCLMock")
  public CtclInteractive corporateEventCtclMock(){
    return new CorporateEventCTCLMock(wdActorSelections);
  }
     
  @Bean(destroyMethod = "shutdown")
  public ActorMaterializer actorMaterializer() throws IOException {
    return ActorMaterializer.create(actorSystem());
  }

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean(name = "SNSClient")
  public AmazonSNS snsClientCreation() {
    return AmazonSNSClient.builder().withRegion(getAmazonAWSRegion()).withCredentials(amazonAWSCredentialsProvider())
        .build();
  }

  @Bean(name = "SNSClientForSMS")
  public AmazonSNS snsClientSMSRegionCreation() {
    return AmazonSNSClient.builder().withRegion(getAmazonSMSAWSRegion()).withCredentials(amazonAWSCredentialsProvider())
        .build();
  }
  
  @Bean(name = "dataSource")
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    
    dataSource.setDriverClassName(wdProperties.rdsDriverClass());
    dataSource.setUsername(wdProperties.rdsUsername());
    dataSource.setPassword(wdProperties.rdsPassword());
    dataSource.setUrl(wdProperties.rdsUrl().concat("/").concat(wdProperties.environment()).concat("_WDTimeSeries"));

    return dataSource;
  }
  
  
  @Bean(name = "entityManager")
  public LocalContainerEntityManagerFactoryBean entityManager() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource());
    em.setPackagesToScan(new String[] { "com.wt.rds.domain" });

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    HashMap<String, Object> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", "update");
    properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
    em.setJpaPropertyMap(properties);

    return em;
  }
  
  @Bean(name = "transactionManager")
  public PlatformTransactionManager transactionManager() {

    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManager().getObject());
    return transactionManager;
  }
  
}
