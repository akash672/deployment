package com.wt.universe.util;

import static com.wt.utils.DoublesUtil.exchangeParseDouble;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.wt.domain.Benchmark;
import com.wt.utils.CommonAppConfiguration;
import com.wt.utils.DateUtils;

import lombok.extern.log4j.Log4j;

@Service("BenchmarkHistoricalUploader")
@Log4j
public class BenchmarkHistoricalUploader {
  private static AnnotationConfigApplicationContext ctx;
  private UniverseSNSToRDSHandler snsToRDSHandler;

  @Autowired
  public BenchmarkHistoricalUploader(UniverseSNSToRDSHandler snsToRDSHandler) {
    super();
    this.snsToRDSHandler = snsToRDSHandler;
  }

  public static void main(String args[])
      throws FileNotFoundException, IOException, NumberFormatException, ParseException {
    ctx = new AnnotationConfigApplicationContext(CommonAppConfiguration.class);
    BenchmarkHistoricalUploader benchmarkUploaderToRDS = ctx.getBean(BenchmarkHistoricalUploader.class);
    File dir = new File(args[0]);
    log.info("Name of directory having historical benchmark data: " + dir);
    File[] directoryListing = dir.listFiles();
    if (directoryListing != null) {
      for (File child : directoryListing) {
        log.info("Running historical benchmark data for file: " + child.getName());
        benchmarkUploaderToRDS.pushBenchmarkToSNSToRDSHandler(dir + "/" + child.getName());
      }
    } else {
      log.error("Given directory is empty. Change directory name");
    }
  }

  public void pushBenchmarkToSNSToRDSHandler(String csvFile)
      throws FileNotFoundException, IOException, NumberFormatException, ParseException {
    String line = "";
    String csvSplitBy = ",";
    boolean firstRow = false;
    HashMap<String, Benchmark> fileBenchmarkData = new HashMap<String, Benchmark>();
    int i = 0;
    try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
      while ((line = br.readLine()) != null) {
        String[] recordLine = line.split(csvSplitBy);
        if (!firstRow) { // This is avoid the error - as first line is not a
                         // valid line
          firstRow = true;
          continue;
        }
        Benchmark benchmark = new Benchmark(recordLine[0],
            DateUtils.eodDateFromExcelDate(recordLine[1], "dd-MM-yyyy", "/|\\-"), exchangeParseDouble(recordLine[2]),
            exchangeParseDouble(recordLine[3]), exchangeParseDouble(recordLine[4]), exchangeParseDouble(recordLine[5]),
            exchangeParseDouble(recordLine[7]));
        fileBenchmarkData.put(benchmark.getFund(), benchmark);
        snsToRDSHandler.handleHistoricalBenchmarkData(fileBenchmarkData);
        fileBenchmarkData.clear();
        i++;
      }
      log.debug("Sent Benchmark Snaphot from file: " + csvFile + " to SNS to RDS handler of size: " + i);
    }
  }

}