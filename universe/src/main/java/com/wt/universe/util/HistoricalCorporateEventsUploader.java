package com.wt.universe.util;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.wt.corporateevents.BseCorporateEventSource;
import com.wt.corporateevents.CorporateEventSource;
import com.wt.corporateevents.NseCorporateEventSource;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import lombok.extern.log4j.Log4j;

@Log4j
public class HistoricalCorporateEventsUploader {

  public static void main(String[] args) throws Exception {
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CtclAppConfiguration.class)) {
      final ActorSystem system = (ActorSystem) ctx.getBean("actorSystem");
      @SuppressWarnings("unused")
      final ActorRef universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
      final WDActorSelections actorSelection = (WDActorSelections) ctx.getBean(WDActorSelections.class);
      CorporateEventSource corporateEventSource;
      if(args[1].equals("NSE"))
        corporateEventSource = new NseCorporateEventSource(args[0]);
      else
        corporateEventSource = new BseCorporateEventSource(args[0]);
      long startTime = DateUtils.currentTimeInMillis();
      log.info("Reading the file ....");
      corporateEventSource.notifyCorporateEventArrival(actorSelection.universeRootActor());
      long endTime = DateUtils.currentTimeInMillis();
      long timeElapsed = endTime - startTime;
      log.debug("Total time taken is: " + timeElapsed);
    }
  }
}
