package com.wt.universe.util;

import static com.google.common.io.Files.readLines;
import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.google.common.base.Charsets;
import com.wt.domain.Instrument;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.commands.GetInstrumentByWdId;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.InstrumentByWdIdSent;
import com.wt.domain.write.events.InstrumentPrice;

import akka.actor.ActorRef;
import lombok.extern.log4j.Log4j;

@Log4j
public class PriceExtractor {

  public PriceExtractor() {
    super();
  }

  public void readFilesFromFolder(String parentFolderName, Date startDate, Date endDate, String exchange, ActorRef instrumentManagerActor) throws Exception {
    File priceFilesFolder = new File(parentFolderName);
    File[] priceFiles = priceFilesFolder.listFiles();
    Map<String, Double> pafMap = new HashMap<>();
    Calendar start = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
    start.setTime(startDate);
    Calendar end = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
    end.setTime(endDate);
    for (File file : priceFiles) {
      log.info("Processing Folder: " + file.getName());
      File folderName = new File(file.getAbsolutePath());
      if (folderName.isDirectory()) {
        File[] fileNames = folderName.listFiles();
        Map<String, File> fileMap = new HashMap<>(); 
        for (File csv : fileNames) {
          if (isPafFile(csv)) {
            log.info("Processing PAF File: " + csv.getName());
            loadPafFile(csv, pafMap, instrumentManagerActor);
          }
          else {
            fileMap.put(csv.getName().toLowerCase(), csv);
          }
        }
        for (Date sDate = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), sDate = start.getTime())
        {
          String filename = getExchangeFileName(sDate, exchange);
          if (fileMap.get(filename) != null) {
            File csv = fileMap.get(filename.toLowerCase()); 
            if (csv.getName().toLowerCase().contains("paf"))
              continue;

            if (csv.getName().toLowerCase().contains("csv")) {
              log.info("Processing Bhav copy " + csv.getName());
              readFile(csv, instrumentManagerActor, pafMap);
            }
          }
          else {
            log.error("Date not processed for historical price population " + sDate);
          }
        }
      }
    }
  }

  private String getExchangeFileName(Date sDate, String exchange) {
    SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
    monthFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
    cal.setTime(sDate);
    String day = "" + cal.get(Calendar.DAY_OF_MONTH);
    String month = "" + (cal.get(Calendar.MONTH) + 1);
    if (day.length() == 1) {
      day = "0" + day;
    }
    if (month.length() == 1) {
      month = "0" + month;
    }
    
    if (exchange.toLowerCase().equals("nse")) {
      return "cm" + day + monthFormat.format(sDate).toLowerCase() + cal.get(Calendar.YEAR) + "bhav.csv";
    }
    else {      
      return "eq_isincode_" + day + month + String.valueOf(cal.get(Calendar.YEAR)).substring(2) + ".csv";
    }
  }

  private void loadPafFile(File csvFile, Map<String, Double> pafMap, ActorRef instrumentManagerActor) throws Exception {
    List<String> allLines = readLines(csvFile, Charsets.UTF_8);

    for (String line : allLines) {
      String[] fields = line.split(",");
      String wdId = wdId(fields, instrumentManagerActor, "NSE");
      if (wdId == null)
        continue;
      pafMap.put(wdId + eodDateFromExcelDate(fields[1], "dd-MMM-yyyy", "-"), Double.parseDouble(fields[2]));
    }
  }

  @SuppressWarnings("unused")
  public void readFile(File csvFile, ActorRef instrumentManagerActor, Map<String, Double> pafMap) throws Exception {
    String bseDate = "";
    if (csvFile.getName().contains("EQ")) {
      bseDate = getDateFromBSEFile(csvFile);
    }
    List<String> allLines = readLines(csvFile, Charsets.UTF_8);
    boolean isFirst = true;
    for (String line : allLines) {
      if (isFirst) {
        isFirst = false;
        continue;
      }
      String[] fields = line.split(",");
      if (isNSEEquityColumn(fields)) {
        String wdId = wdId(fields, instrumentManagerActor, "NSE");
        if (wdId == null)
            continue;

        InstrumentPrice price = new InstrumentPrice(Double.parseDouble(fields[5]), eodDateFromExcelDate(fields[10], "dd-MMM-yyyy", "-"), wdId);
        Done result = (Done) askAndWaitForResult(instrumentManagerActor, price);
      } else if (isBSEEquityColumn(fields)) {
        String wdId = wdId(fields, instrumentManagerActor, "BSE");
        if (wdId == null)
          continue;
        InstrumentPrice pxEvt = new InstrumentPrice(Double.parseDouble(fields[7]), eodDateFromExcelDate(bseDate, "dd-MMM-yyyy", "-"), wdId);
        Done result = (Done) askAndWaitForResult(instrumentManagerActor, pxEvt);
      }
    }
    log.info("All Done");
  }

  private boolean isNSEEquityColumn(String[] fields) {
    return (fields[1].equals("EQ") || fields[1].equals("BE"));
  }
  
  private boolean isBSEEquityColumn(String[] fields) {
    return fields[3].equals("Q");
  }

  private boolean isPafFile(File csv) {
    return csv.getName().toLowerCase().contains("paf");
  }

  private String getDateFromBSEFile(File csvFile) throws ParseException {
    log.info("Processing BSE File " + csvFile.getName());
    String dateInFileFormat = csvFile.getName().replaceAll("\\D+", "");
    SimpleDateFormat requiredFormat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat receivedFormat = new SimpleDateFormat("ddMMyy");
    Date receivedDate = receivedFormat.parse(dateInFileFormat);
    return requiredFormat.format(receivedDate);
  }

  private String wdId(String[] record, ActorRef instrumentManagerActor, String exchange) throws Exception {
    Object maybeEQInstrument;
    Object instrumentByWdIdSent;
    if (exchange.equals("NSE"))
      maybeEQInstrument = askAndWaitForResult(instrumentManagerActor,
          new GetEquityInstrumentBySymbol(record[0], exchange));
    else {
      instrumentByWdIdSent = askAndWaitForResult(instrumentManagerActor,
          new GetInstrumentByWdId(record[14] + "-" + exchange + "-EQ"));
      if (instrumentByWdIdSent.getClass().equals(Failed.class)) {
        return null;
      } else
        maybeEQInstrument = ((InstrumentByWdIdSent) instrumentByWdIdSent).getInstrument();
    }
    if (maybeEQInstrument.getClass().equals(Failed.class)) {
      return null;
    }

    Instrument equityInstrument = (Instrument) maybeEQInstrument;
    return equityInstrument.getWdId();
  }
}