package com.wt.universe.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.google.gson.Gson;
import com.wt.config.utils.WDProperties;
import com.wt.domain.PriceRecordDDBBatch;
import com.wt.domain.PriceWithPaf;

import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class UniverseSNSToDDBHandler {

  @Autowired
  private WDProperties wdProperties;
  @Autowired
  @Qualifier("SNSClient")
  private AmazonSNS ddbSNSClient;

  public void handlePriceData(HashMap<String, PriceWithPaf> tickerToPriceWithPafMap) {
    List<Map<String, PriceWithPaf>> listOfMaps = splitMap(tickerToPriceWithPafMap,
        wdProperties.maxPricesToSNSBatchSizeToDDB());
    for (Map<String, PriceWithPaf> map : listOfMaps) {
      Map<String, String> attributes = setPriceTableAttributes(map);
      sendToDDB(attributes);
    }
  }

  private void sendToDDB(Map<String, String> additionalAttributes) {
    PublishRequest publishRequest;
    publishRequest = publishRequest(additionalAttributes, wdProperties.SNS_TOPIC_ARN_TRIGGERING_FOR_WRITE_TO_DDB());
    log.info(additionalAttributes.get("message") + ", in table: " + additionalAttributes.get("tablename"));
    ddbSNSClient.publish(publishRequest);
  }

  private PublishRequest publishRequest(Map<String, String> additionalAttributes, String endPoint) {
    PublishRequest publishRequest = new PublishRequest();
    publishRequest.setTargetArn(endPoint);
    publishRequest.setMessageAttributes(getMessageAttributesMap(additionalAttributes));
    publishRequest.setMessage(additionalAttributes.get("message"));
    return publishRequest;
  }

  private Map<String, MessageAttributeValue> getMessageAttributesMap(Map<String, String> additionalAttributes) {
    Map<String, MessageAttributeValue> map = additionalAttributes.entrySet().stream()
        .collect(Collectors.toMap(p -> p.getKey(), p -> {
          MessageAttributeValue attributeValue = new MessageAttributeValue();
          attributeValue.setDataType("String");
          attributeValue.setStringValue(p.getValue().isEmpty() ? "NA" : p.getValue());
          return attributeValue;
        }));
    return map;
  }

  private Map<String, String> setPriceTableAttributes(Map<String, PriceWithPaf> pwpMap) {
    Map<String, String> additionalAttributes = new HashMap<String, String>();
    PriceRecordDDBBatch priceRecordBatchDomain = createPriceRecordDDBBatchDomain(pwpMap);
    String recordString = (new Gson().toJson(priceRecordBatchDomain).toString());
    additionalAttributes.put("recordString", recordString);
    additionalAttributes.put("tablename", wdProperties.environment() + "_PriceWithPaf");
    additionalAttributes.put("message", "Sending PriceBatch of size : " + pwpMap.size());
    return additionalAttributes;
  }

  private PriceRecordDDBBatch createPriceRecordDDBBatchDomain(Map<String, PriceWithPaf> pwpMap) {
    return new PriceRecordDDBBatch(pwpMap.values().stream().collect(Collectors.toList()));
  }

  private List<Map<String, PriceWithPaf>> splitMap(Map<String, PriceWithPaf> tickerToPriceWithPafMap, int splitSize) {

    float mapSize = tickerToPriceWithPafMap.size();
    float splitFactorF = splitSize;
    float actualNoOfBatches = (mapSize / splitFactorF);
    double noOfBatches = Math.ceil(actualNoOfBatches);

    List<Map<String, PriceWithPaf>> listOfMaps = new ArrayList<>();
    List<List<String>> listOfListOfKeys = new ArrayList<>();
    int startIndex = 0;
    int endIndex = (int) ((splitSize > mapSize) ? mapSize : splitSize);

    Set<String> keys = tickerToPriceWithPafMap.keySet();
    List<String> keysAsList = new ArrayList<>();
    keysAsList.addAll(keys);

    for (int i = 0; i < noOfBatches; i++) {
      listOfListOfKeys.add(keysAsList.subList(startIndex, endIndex));
      startIndex = endIndex;
      endIndex = (int) (((endIndex + splitSize) > mapSize) ? mapSize : (endIndex + splitSize));
    }

    for (List<String> keyList : listOfListOfKeys) {
      Map<String, PriceWithPaf> subMap = new HashMap<>();
      for (String key : keyList) {
        subMap.put(key, tickerToPriceWithPafMap.get(key));
      }
      listOfMaps.add(subMap);
    }

    return listOfMaps;
  }

}