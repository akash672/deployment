package com.wt.universe.bod;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j;

@Log4j
@Service("UniverseBODSchedulers")
public class UniverseBODSchedulers {
  private volatile boolean isRunning = false;
  private List<UniverseBODTask> bodTasks;
  private boolean scheduleBODJobs; 

  @Autowired
  public UniverseBODSchedulers(@Qualifier("BODTasksToExecute")List<UniverseBODTask> bodTasks, @Value("${scheduleBODJobs}") boolean flag) {
    super();
    log.info("Created BOD UniverseSchedulers with " + bodTasks);

    this.bodTasks = bodTasks;
    this.scheduleBODJobs = flag;
  }

  @Async
  @Scheduled(cron = "${bod.schedule}", zone = "Asia/Kolkata")
  public void scheduleJobs() throws Exception {
    if (!scheduleBODJobs) {
      return;
    }

    log.info("Running scheduled tasks " + bodTasks);
    if(isRunning) {
      log.info("Already running BOD scheduled tasks");
      return;
    }
    isRunning = true;
    try {
      for (UniverseBODTask bodTask : bodTasks) {
        log.info("Running : " + bodTask);
        bodTask.runIfNotRanAlready();
      }
    } catch (Exception e) {
      log.error("Error while executing task ", e);
    }
    isRunning = false;
    log.info("Ran BOD scheduled tasks");
  }

}