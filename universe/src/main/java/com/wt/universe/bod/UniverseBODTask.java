package com.wt.universe.bod;

import static com.google.common.io.Files.readFirstLine;
import static com.wt.utils.DateUtils.isTodayBOD;
import static com.wt.utils.DateUtils.todayPrettyTime;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.ParseException;

import com.wt.utils.DateUtils;

public interface UniverseBODTask {

  public default void runIfNotRanAlready() throws Exception {
    if (hasNotRunToday()) {
      run();
      try (PrintWriter writer = new PrintWriter(file(), "UTF-8")) {
        writer.println(todayPrettyTime());
        writer.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public default boolean hasNotRunToday() throws IOException, ParseException {
    String firstLine = readFirstLine(file(), Charset.forName("UTF-8"));
    if(DateUtils.isTradingHoliday()) 
      return false;
    if (firstLine == null)
      return true;
    return !isTodayBOD(firstLine);
  }

  public default File file() throws IOException {
    File file = new File(getClass().getSimpleName());
    if(!file.exists()) file.createNewFile();
    return file;
  }

  public void run() throws Exception;
}
