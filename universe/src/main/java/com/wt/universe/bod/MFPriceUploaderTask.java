package com.wt.universe.bod;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.wt.domain.write.commands.BODCompleted;
import com.wt.prices.MFPriceSource;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import lombok.extern.log4j.Log4j;

@Service("MFPriceUploaderTask")
@Log4j
public class MFPriceUploaderTask implements UniverseBODTask {
  private List<MFPriceSource> priceSources;
  private WDActorSelections actorSelections;

  @Autowired
  public MFPriceUploaderTask(@Qualifier("mutualFundPriceSources") List<MFPriceSource> priceSources, @Qualifier("WDActors") WDActorSelections actorSelections) {
    super();
    this.priceSources = priceSources;
    this.actorSelections = actorSelections;
  }

  public void run() throws Exception {
    log.info("Running BOD MF PriceUploaderTask");
    populatePricesFromAllSources();
    log.info("Ran BOD MF PriceUploaderTask");
    log.info("Sending BOD Completion message to the WD Actor System");
    actorSelections.advisersRootActor().tell(new BODCompleted(), ActorRef.noSender());
    actorSelections.userRootActor().tell(new BODCompleted(), ActorRef.noSender());
    actorSelections.realtimeAdviserFundPerformanceRootActor().tell(new BODCompleted(), ActorRef.noSender());
    actorSelections.realtimeMarketPriceRootActor().tell(new BODCompleted(), ActorRef.noSender());
  }

  private void populatePricesFromAllSources() throws Exception {
    for (MFPriceSource instrumentsPriceSource : priceSources)
      instrumentsPriceSource.notifyPriceArrival(actorSelections.universeRootActor(),
          DateUtils.eodDateToday());
  }
}