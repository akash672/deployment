package com.wt.universe.bod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.wt.domain.write.commands.CheckForCorporateEvent;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import lombok.extern.log4j.Log4j;

@Service("CorporateEventBODTask")
@Log4j
public class CorporateEventBODTask implements UniverseBODTask{
  
  private WDActorSelections actorSelections;
  
  @Autowired
  public CorporateEventBODTask(@Qualifier("WDActors") WDActorSelections actorSelections) {
    super();
    this.actorSelections = actorSelections;
  }
  @Override
  public void run() throws Exception {
    log.info("Running BOD Corporate Event Check");
    actorSelections.universeRootActor().tell(new CheckForCorporateEvent(), ActorRef.noSender());
    
  }

}
