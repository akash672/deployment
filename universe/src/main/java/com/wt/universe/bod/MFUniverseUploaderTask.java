package com.wt.universe.bod;

import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.newHashSet;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets.SetView;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.repo.MutualFundInstrumentRepository;
import com.wt.domain.write.commands.UpdateMFInstruments;
import com.wt.domain.write.commands.UpdateMutualFundInstrumentSchemeName;
import com.wt.instruments.MFUniverseSource;
import com.wt.utils.akka.WDActorSelections;

import lombok.extern.log4j.Log4j;
import rx.Observable;

@Service("MFUniverseUploaderTask")
@Log4j
public class MFUniverseUploaderTask implements UniverseBODTask {
  private List<MFUniverseSource> mfUniverseSources;
  private WDActorSelections actorSelections;
  private MutualFundInstrumentRepository mutualFundInstrumentRepository;
  @Autowired
  public MFUniverseUploaderTask(@Qualifier("mfUniverseSources") List<MFUniverseSource> mfUniverseSources,
      WDActorSelections actorSelections, MutualFundInstrumentRepository repo) {
    this.mfUniverseSources = mfUniverseSources;
    this.actorSelections = actorSelections;
    this.mutualFundInstrumentRepository = repo;
  }

  public void run() throws Exception {
    log.info("Running BOD MF UniverseUploaderTask");
    Set<MutualFundInstrument> today = newHashSet(today());

    askAndWaitForResult(actorSelections.universeRootActor(), new UpdateMFInstruments(today));

    Set<MutualFundInstrument> yesterday = newHashSet(getMFInstruments());

    SetView<MutualFundInstrument> allAdded = difference(today, yesterday);
    addOneByOneAsRepoDoesNotSupportAddAll(allAdded);
    log.info("Mutual Funds added today: " + allAdded.size());

    for (MutualFundInstrument todayInstrument : today) {
      for (MutualFundInstrument yestInstrument : yesterday) {
        if (todayInstrument.getWdId().equals(yestInstrument.getWdId())
            && !todayInstrument.getSchemeName().equals(yestInstrument.getSchemeName()))
          askAndWaitForResult(actorSelections.universeRootActor(),
              new UpdateMutualFundInstrumentSchemeName(todayInstrument.getToken(), todayInstrument.getSchemeName()));
      }
    }
    log.info("Ran BOD MF UniverseUploaderTask");
  }

  private Set<MutualFundInstrument> getMFInstruments() {
    Set<MutualFundInstrument> set = new HashSet<MutualFundInstrument>();
    Iterator<MutualFundInstrument> iterator = mutualFundInstrumentRepository.findAll().iterator();
    while (iterator.hasNext()) {
      MutualFundInstrument instrument = (MutualFundInstrument) iterator.next();
      if (instrument.getWdId().endsWith("-MF"))
        set.add(instrument);
    }
    return set;
  }

  private void addOneByOneAsRepoDoesNotSupportAddAll(Set<MutualFundInstrument> allAdded) {
    for (MutualFundInstrument instrument : allAdded) {
      log.debug("Adding MF " + instrument);
      mutualFundInstrumentRepository.save(instrument);
    }
  }

  private List<MutualFundInstrument> today() {
    List<MutualFundInstrument> allInstruments = new ArrayList<MutualFundInstrument>();
    for (MFUniverseSource securitiesUniverseSource : mfUniverseSources) {
      Observable<MutualFundInstrument> instruments = securitiesUniverseSource.instruments();
      instruments.doOnError(e -> log.error(e));
      instruments.forEach(instrument -> allInstruments.add(instrument));
    }
    return allInstruments;
  }
}