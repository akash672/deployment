package com.wt.prices;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.wt.domain.write.events.InstrumentPrice;


public class BSEPriceSource extends InstrumentPriceSource {
  public BSEPriceSource(BSEFileNameExtractor bsePriceFileNameExtractor) {
    super(bsePriceFileNameExtractor);
  }

  @Override
  public InstrumentPrice populatePriceEvent(String[] recordLine, String priceDate) throws ParseException, Exception {
    return new InstrumentPrice(Double.parseDouble(recordLine[7]),
        eodDateFromExcelDate(priceDate, "dd-MMM-yyyy", "-"), recordLine[14] + "-BSE-" + "EQ");
  
  }

  @Override
  protected String getPriceEventDate(String fileName) throws ParseException {
    String dateInFileFormat = fileName.replaceAll("\\D+", "");
    SimpleDateFormat requiredFormat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat receivedFormat = new SimpleDateFormat("ddMMyy");
    Date receivedDate = receivedFormat.parse(dateInFileFormat);
    return requiredFormat.format(receivedDate);
  }

  @Override
  protected boolean isEquityRow(String[] recordLine) {
    return "Q".equals(recordLine[3]);
  }

}