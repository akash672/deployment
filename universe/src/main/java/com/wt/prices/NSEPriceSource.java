package com.wt.prices;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.text.ParseException;

import com.wt.domain.EquityInstrument;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.utils.akka.WDActorSelections;

public class NSEPriceSource extends InstrumentPriceSource {
  private WDActorSelections wdActorSelections;

  public NSEPriceSource(NSEFileNameExtractor nsePriceFileNameExtractor, WDActorSelections wdActorSelections) {
    super(nsePriceFileNameExtractor);
    this.wdActorSelections = wdActorSelections;
  }

  @Override
  public InstrumentPrice populatePriceEvent(String[] recordLine, String priceDate) throws Exception {
    EquityInstrument equityInstrument = (EquityInstrument) askAndWaitForResult(wdActorSelections.universeRootActor(),
        new GetEquityInstrumentBySymbol(recordLine[0], "NSE"));
    return new InstrumentPrice(Double.parseDouble(recordLine[6]), eodDateFromExcelDate(recordLine[10], "dd-MMM-yyyy", "-"),
        equityInstrument.getWdId());
  }

  @Override
  protected String getPriceEventDate(String fileName) throws ParseException {
    return null;
  }

  @Override
  protected boolean isEquityRow(String[] recordLine) {
    return ("EQ".equals(recordLine[1]) || "BE".equals(recordLine[1]));
  }
  
}