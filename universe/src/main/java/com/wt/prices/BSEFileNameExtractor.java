package com.wt.prices;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.wt.utils.DateUtils;

public class BSEFileNameExtractor implements FileNameCreator {

  private String urlPrefix;

  public BSEFileNameExtractor(String urlPrefix) {
    this.urlPrefix = urlPrefix;
  }

  @Override
  public String getFileName(String todaysDate) {
    String parts[] = DateUtils.splitDate(todaysDate, SPLITTER);
    return "EQ_ISINCODE_" + DateUtils.correctDate(parts[0]) + DateUtils.correctMonth(parts[1])
        + DateUtils.correctYear(parts[2]).substring(2, 4) + ".zip";
  }

  public String getDateFromFile(String name) throws ParseException {
    String dateInFileFormat = name.replaceAll("\\D+", "");
    SimpleDateFormat requiredFormat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat receivedFormat = new SimpleDateFormat("ddMMyy");
    Date receivedDate = receivedFormat.parse(dateInFileFormat);
    return requiredFormat.format(receivedDate);
  }

  @Override
  public URL constructPriceURL(String fileName) throws MalformedURLException {
    return new URL(urlPrefix + fileName);
  }

}
