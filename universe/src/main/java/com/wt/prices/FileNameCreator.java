package com.wt.prices;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;

public interface FileNameCreator {
  public static final String SPLITTER = "/";
  public String getFileName(String todaysDate) throws ParseException;
  public URL constructPriceURL(String priceBaseLink) throws MalformedURLException;
  
}
