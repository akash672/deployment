package com.wt.prices;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.util.zip.ZipInputStream;

import com.wt.domain.write.events.InstrumentPrice;

import akka.actor.ActorSelection;
import lombok.extern.log4j.Log4j;

@Log4j
public abstract class InstrumentPriceSource {
  public static final String CSV_SPLIT_BY = "\\,";

  private FileNameCreator fileNameExtractor;

  public InstrumentPriceSource(FileNameCreator fileNameExtractor) {
    this.fileNameExtractor = fileNameExtractor;
  }

  public void notifyPriceArrival(ActorSelection instrumentManagerActor, String todaysDate) throws Exception {
    String fileName = fileNameExtractor.getFileName(todaysDate);
    URL priceLink = fileNameExtractor.constructPriceURL(fileName);
    log.info("Price Source URL: " + priceLink.toString());
    parseAndNotify(instrumentManagerActor, fileName, priceLink);
  }

  private void parseAndNotify(ActorSelection instrumentManagerActor, String fileName, URL priceLink) throws Exception {
    try (ZipInputStream in = new ZipInputStream(priceLink.openStream())) {
      while ((in.getNextEntry()) != null) {

        String priceDate = getPriceEventDate(fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        boolean isHeader = false;
        while ((line = reader.readLine()) != null) {
          try {

            String[] columns = line.split(CSV_SPLIT_BY);
            if (!isHeader) {
              isHeader = true;
              continue;
            }
            if (isEquityRow(columns)) {
              InstrumentPrice pxEvt = populatePriceEvent(columns, priceDate);
              askAndWaitForResult(instrumentManagerActor, pxEvt);
            }
          } catch (Exception e) {
            log.error("Exception while getting price ", e);
          }
        }

      }
      in.closeEntry();
    } catch (Exception e) {
      log.error(e);
      throw e;
    }
  }

  protected abstract String getPriceEventDate(String fileName) throws ParseException;

  protected abstract InstrumentPrice populatePriceEvent(String[] recordLine, String priceDate)
      throws ParseException, Exception;

  protected abstract boolean isEquityRow(String[] recordLine);

}