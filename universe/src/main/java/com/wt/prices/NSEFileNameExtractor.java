package com.wt.prices;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;

import com.wt.utils.DateUtils;

public class NSEFileNameExtractor implements FileNameCreator {
  private String urlPrefix;
  public String nseDate;
  public String nseMonth; 
  public String nseYear; 

  public NSEFileNameExtractor(String urlPrefix) {
    this.urlPrefix = urlPrefix;
  }

  public String getDay(LocalDate todaysDate) {
    String day = Integer.toString(todaysDate.getDayOfMonth());
    if (day.length() != 2) {
      day = "0" + day;
    }
    return day;
  }

  @Override
  public String getFileName(String todaysDate) throws ParseException {
    String[] parts = DateUtils.splitDate(todaysDate, SPLITTER);
    getNSEFormatDate(parts);
    String nseFileName = "cm" + nseDate + nseMonth + nseYear + "bhav.csv";
    return nseFileName;
  }

  private void getNSEFormatDate(String[] parts) throws ParseException {
    this.nseDate = DateUtils.correctDate(parts[0]);
    this.nseMonth = DateUtils.formatMonth(parts[1]).toUpperCase();
    this.nseYear = DateUtils.correctYear(parts[2]);
  }

  @Override
  public URL constructPriceURL(String fileName) throws MalformedURLException {
    return new URL(urlPrefix + nseYear + "/" + nseMonth + "/" + fileName + ".zip");
  }
}
