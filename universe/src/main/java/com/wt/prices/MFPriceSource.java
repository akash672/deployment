package com.wt.prices;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.util.stream.Stream;

import com.wt.domain.MutualFundInstrument;
import com.wt.domain.write.commands.GetMutualFundInstrumentByToken;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorSelection;
import lombok.extern.log4j.Log4j;

@Log4j
public class MFPriceSource {

  private WDActorSelections wdActorSelections;
  private FileNameCreator fileNameExtractor;

  private String TXT_SPLIT_BY = ";";
  private String HYPHEN = "-";
  private String NA = "NA";
  private String VALID_MF_IDENTIFIER_INF = "INF";
  private String DIRECT = "direct";
  private int retryCounter;

  public MFPriceSource(MFFileNameExtractor mfPriceFileNameExtractor, WDActorSelections wdActorSelections) {
    this.wdActorSelections = wdActorSelections;
    this.fileNameExtractor = mfPriceFileNameExtractor;
  }

  public void notifyPriceArrival(ActorSelection instrumentManagerActor, String todaysDate) throws Exception {
    retryCounter = 1;
    String fileName = fileNameExtractor.getFileName(todaysDate);
    URL priceLink = fileNameExtractor.constructPriceURL(fileName);
    parseAndNotify(instrumentManagerActor, fileName, priceLink);
  }

  private void parseAndNotify(ActorSelection instrumentManagerActor, String fileName, URL priceLink) {
    File docDestFile = getMutualFundFile(fileName, priceLink);
    log.info("File Downloaded from : " + priceLink);
    try (BufferedReader br = new BufferedReader(new FileReader(docDestFile))) {
      log.info("Reading file: " + fileName);
      Stream<String> lines = br.lines();
      lines.forEach(line -> {
        try {
          if (line.contains(TXT_SPLIT_BY)) {
            String[] columns = line.split(TXT_SPLIT_BY);
            if (columns[0].equals("Scheme Code")) {
              return;
            }
            if (isValidMfSeries(columns)) {
              if (isValidInvestment(columns[1])) {
                InstrumentPrice pxEvtNormal = populatePriceEvent(columns);
                if (pxEvtNormal != null)
                  askAndWaitForResult(instrumentManagerActor, pxEvtNormal);
              }
              if (isValidInvestment(columns[2])) {
                columns[1] = columns[2];
                InstrumentPrice pxEvtReinvest = populatePriceEvent(columns);
                pxEvtReinvest = populatePriceEvent(columns);
                if (pxEvtReinvest != null)
                  askAndWaitForResult(instrumentManagerActor, pxEvtReinvest);
              }
            }
          }
        } catch (Exception e) {
          log.error("Exception while getting MF price ", e);
        } finally {
          docDestFile.delete();
        }
      });
    } catch (Exception e) {
      log.error(e);
    }
  }

  private File getMutualFundFile(String fileName, URL priceLink) {
    Path targetPath = new File(fileName).toPath();
    log.info("Downloading file from : " + priceLink);
    try (InputStream in = priceLink.openStream()) {
      Files.copy(in, targetPath, StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
      if (retryCounter <= 10) {
        retryCounter++;
        getMutualFundFile(fileName, priceLink);
      } else
        log.error(e);
    }
    File docDestFile = targetPath.toFile();
    return docDestFile;
  }

  protected InstrumentPrice populatePriceEvent(String[] recordLine) throws ParseException, Exception {
    double price = 0;
    try {
      price = Double.parseDouble(recordLine[4]);
    } catch (NumberFormatException e) {
      log.error("No price received for " + String.join(" ", recordLine));
    }

    if (recordLine[1].length() > 3) {
      Object result = askAndWaitForResult(
          wdActorSelections.universeRootActor(), new GetMutualFundInstrumentByToken(recordLine[1]));
      if (result instanceof Failed) {
        //log.error("Error while getting instrument by wdId " + result);
        return null;
      }
      MutualFundInstrument instrument = (MutualFundInstrument) result;
      return new InstrumentPrice(price, eodDateFromExcelDate(recordLine[5], "dd-MMM-yyyy", "-"), instrument.getWdId());
    } else {
      return null;
    }
  }

  private boolean isValidInvestment(String columnValue) {
    return !(columnValue.equals(HYPHEN) || columnValue.equals(NA)) && columnValue.length() > 3;
  }

  private boolean isValidMfSeries(String[] columns) {
    if (!(columns[1].equals(HYPHEN) && columns[2].equals(HYPHEN)))
      if ((getColumnValue(columns, 1).equalsIgnoreCase(VALID_MF_IDENTIFIER_INF)
          || getColumnValue(columns, 2).equalsIgnoreCase(VALID_MF_IDENTIFIER_INF)) && isDirect(columns[3]))
        return true;
      else
        return false;
    else
      return false;
  }

  private boolean isDirect(String columnValue) {
    if (columnValue.toLowerCase().contains(DIRECT))
      return true;
    return false;
  }

  private String getColumnValue(String[] columns, int columnNo) {
    if (columns[columnNo].length() >= 3)
      return columns[columnNo].substring(0, 3);
    return columns[columnNo];
  }

}