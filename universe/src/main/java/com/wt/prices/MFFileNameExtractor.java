package com.wt.prices;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;

public class MFFileNameExtractor implements FileNameCreator {
  private String urlPrefix;
  public String mfDate;
  public String mfMonth; 
  public String mfYear; 

  public MFFileNameExtractor(String urlPrefix) {
    this.urlPrefix = urlPrefix;
  }

  @Override
  public String getFileName(String todaysDate) throws ParseException {
    return "NAVAll.txt";
  }

  @Override
  public URL constructPriceURL(String fileName) throws MalformedURLException {
    return new URL(urlPrefix+fileName);
  }
}
