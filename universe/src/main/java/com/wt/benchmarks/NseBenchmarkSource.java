package com.wt.benchmarks;

import static com.wt.utils.DoublesUtil.exchangeParseDouble;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.wt.domain.Benchmark;
import com.wt.utils.DateUtils;

import lombok.extern.log4j.Log4j;
import rx.Observable;

@Log4j
public class NseBenchmarkSource implements BenchmarkSource {
  private static String CSV_SPLIT_BY = ",";

  private String nseBenchmarkBaseUrl;
  private HashMap<String, Benchmark> eodBenchmarkData;

  public NseBenchmarkSource(String baseUrl) {
    this.nseBenchmarkBaseUrl = baseUrl;
  }

  @Override
  public Observable<Benchmark> benchmarks() {
    URL nseBenchmarks;
    String todayAsString = new SimpleDateFormat("ddMMyyyy").format(getTodaysDate());
    String filename = "ind_close_all_" + todayAsString + ".csv";
    eodBenchmarkData = new HashMap<String, Benchmark>();
    try {
      nseBenchmarks = new URL(nseBenchmarkBaseUrl + filename);
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
    Observable.OnSubscribe<Benchmark> subscribeFunction = (subscriber) -> {
      String line = "";
      boolean header = true;

      try (BufferedReader in = new BufferedReader(new InputStreamReader(nseBenchmarks.openStream()))) {

        while ((line = in.readLine()) != null) {
          String[] recordLine = line.split(CSV_SPLIT_BY);
          if (header) {
            header = false;
            continue;
          }

          Benchmark benchmark = new Benchmark(recordLine[0],
              DateUtils.eodDateFromExcelDate(recordLine[1], "dd-MM-yyyy", "-"), exchangeParseDouble(recordLine[2]),
              exchangeParseDouble(recordLine[3]), exchangeParseDouble(recordLine[4]),
              exchangeParseDouble(recordLine[5]), exchangeParseDouble(recordLine[7]));
          eodBenchmarkData.put(benchmark.getFund(), benchmark);
          if (!subscriber.isUnsubscribed()) {
            subscriber.onNext(benchmark);
          }
        }
      } catch (Exception e) {
        if (!subscriber.isUnsubscribed()) {
          subscriber.onError(e);
        }
      }
      subscriber.onCompleted();
    };

    return Observable.create(subscribeFunction);
  }

  private Date getTodaysDate() {
    try {
      String dateValue = DateUtils.eodDateToday();
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      Date date = df.parse(dateValue);
      return date;
    } catch (ParseException e) {
      log.info(DateUtils.eodDateToday() + "Date was not parsed, returning todays date");
      return new Date();
    }
  }

  @Override
  public HashMap<String, Benchmark> getEODBenchmarkData() {
    return this.eodBenchmarkData;
  }

  @Override
  public void clearEODMaps() {
    this.eodBenchmarkData.clear();
  }

}
