package com.wt.benchmarks;

import java.util.HashMap;

import com.wt.domain.Benchmark;

import rx.Observable;

public interface BenchmarkSource {
  Observable<Benchmark> benchmarks();
  HashMap<String, Benchmark> getEODBenchmarkData();
  void clearEODMaps();
}
