package com.wt.ctcl.facade;

import java.io.IOException;

import com.wt.domain.Instrument;
import com.wt.domain.PriceWithPaf;
import com.wt.utils.akka.CancellableStream;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.stream.Materializer;

public interface CtclBroadcast {
  
  CancellableStream<PriceWithPaf, NotUsed> subscribeStream(Instrument token, Materializer mat) throws IOException;

  CancellableStream<PriceWithPaf, NotUsed> subscribeToLiveStream(Instrument token, Materializer mat) throws IOException;
  
  void unSubscribeStream(Instrument token, ActorRef actor) throws IOException;
  
  void unSubscribeLiveStream(Instrument token, ActorRef actor) throws IOException;
  
  void snapshot(Instrument token, ActorRef sender);

  void addSubscription(String token, String exchange) throws IOException;

  void removeSubscription(String token, String exchange) throws IOException;

  void lastPriceCheck() throws Exception;

}
