package com.wt.rights;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import org.concordion.api.MultiValueResult;
import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.mobileconnectors.apigateway.ApiClientException;
import com.amazonaws.mobileconnectors.apigateway.ApiClientFactory;
import com.wt.client.model.ChangePasswordParameters;
import com.wt.client.model.Credentials;
import com.wt.client.model.ResetPassword;
import com.wt.client.model.SubscriptionParameters;
import com.wt.client.model.User;
import com.wt.client.model.WealthCode;

import lombok.extern.log4j.Log4j;

@RunWith(ConcordionRunner.class)
@Log4j
public class UserClassification {
  private static final String WEALTHCODE = "1234";
  private static final String DUMMY_DEVICETOKEN = "ABCD1234";
  private static final String DUMMY_ADVISER = "A1";
  private static final String DUMMY_NEW_PASSWORD = "newPassword";
  private static final String DUMMY_FUND = "F1";
  private static final String DUMMY_EQUITY = "BOB";
  private static final String DUMMY_PHONENUMBER = "9876543210";
  private static final String DUMMY_OTP = WEALTHCODE;
  private static final String DUMMY_EMAIL = "test@testing.com";
  private static final String DUMMY_PASSWORD = "newPassword";
  private static final String DUMMY_DATE = "21/08/1990";
  private static final BigDecimal LUMPSUMAMOUNT = new BigDecimal(20000);
  private static final BigDecimal SIPAMOUNT = new BigDecimal(2000);

  public MultiValueResult canPerformAction(String action, String method)
      throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, InterruptedException, ExecutionException, IOException {
    try {
      String guestUserResponse = guestUserResponse(action, method);
      String ntUserResponse = NTUserResponse(action, method);
      String ntu_NAResponse = NTU_NAResponse(action, method);

      return new MultiValueResult().with("guestUserResponse", guestUserResponse).with("ntuResponse", ntUserResponse)
          .with("fullUserResponse", ntu_NAResponse);
    } catch (Exception e) {
      log.error(e);
      return null;
    }
  }

  private String guestUserResponse(String action, String methodName) {
    ApiClientFactory factory = factory();
    return sendRequest(action, factory);
  }

  private ApiClientFactory factory() {
    ApiClientFactory factory = new ApiClientFactory();
    factory.apiKey("KiWsv89Ac14Zl6bnniJJP9bKdBGOPEvl9QlJoZR2");
    return factory;
  }

  private String NTUserResponse(String action, String method)
      throws InterruptedException, ExecutionException, ClassNotFoundException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {
    ApiClientFactory factory = factory();

    factory.credentialsProvider(credentials(factory, "EMAIL_NTU@gmail.com"));
    return sendRequest(action, factory);
  }

  private String NTU_NAResponse(String action, String method)
      throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, InterruptedException, ExecutionException, IOException {
    ApiClientFactory factory = factory();

    factory.credentialsProvider(credentials(factory, "EMAIL_NTU_NA@gmail.com"));
    return sendRequest(action, factory);
  }

  private String sendRequest(String action, ApiClientFactory factory) {
    try {
      final Object instance = instance(factory);

      if (action.equals("funds")) {
        Method method = instance.getClass().getMethod("fundsGet");
        method.invoke(instance);

      } else if (action.equals("advisesAdviserFundGet")) {
        Method method = instance.getClass().getMethod("advisesAdviserFundGet", String.class, String.class);
        method.invoke(instance, DUMMY_ADVISER, DUMMY_FUND);
      } else if (action.equals("userAdvicesGet")) {
        Method method = instance.getClass().getMethod("userAdvicesGet", String.class, String.class, String.class);
        method.invoke(instance, DUMMY_EMAIL, DUMMY_ADVISER, DUMMY_FUND);
      } else if (action.equals("compositionAdviserFundGet")) {
        Method method = instance.getClass().getMethod("compositionAdviserFundGet", String.class, String.class);
        method.invoke(instance, DUMMY_ADVISER, DUMMY_ADVISER);
      } else if (action.equals("orderbookGet")) {
        Method method = instance.getClass().getMethod("orderbookGet");
        method.invoke(instance);
      } else if (action.equals("getBenchmarkPerformance")) {
        Method method = instance.getClass().getMethod("benchmarkPerformanceBenchmarkGet", String.class);
        method.invoke(instance, DUMMY_EQUITY);
      } else if (action.equals("getFundPerformance")) {
        Method method = instance.getClass().getMethod("performanceFundGet", String.class);
        method.invoke(instance, DUMMY_EQUITY);
      } else if (action.equals("postPhoneNumber")) {
        Method method = instance.getClass().getMethod("phonePhonenumberPost", String.class);
        method.invoke(instance, DUMMY_PHONENUMBER);
      } else if (action.equals("postOtp")) {
        Method method = instance.getClass().getMethod("otpOtpPost", String.class);
        method.invoke(instance, DUMMY_OTP);
      } else if (action.equals("getFirstName")) {
        Method method = instance.getClass().getMethod("firstNameGet", String.class);
        method.invoke(instance, DUMMY_EMAIL);
      } else if (action.equals("wealthcode")) {
        Method method = instance.getClass().getMethod("wealthcodePost", WealthCode.class);
        WealthCode wealthCode = new WealthCode();
        wealthCode.setWealthCode(WEALTHCODE);
        method.invoke(instance, wealthCode);
      } else if (action.equals("subscriptionPost")) {
        Method method = instance.getClass().getMethod("subscriptionAdviserFundAccountPost", String.class, String.class,
            String.class, SubscriptionParameters.class);
        method.invoke(instance, "Demat", DUMMY_FUND, DUMMY_ADVISER, setSubcriptionParameters());
      } else if (action.equals("subscriptionPut")) {
        Method method = instance.getClass().getMethod("subscriptionAdviserFundAccountPut", String.class, String.class,
            String.class, SubscriptionParameters.class);
        method.invoke(instance, "Demat", DUMMY_FUND, DUMMY_ADVISER, setSubcriptionParameters());
      } else if (action.equals("subscriptionDelete")) {
        Method method = instance.getClass().getMethod("subscriptionAdviserFundAccountDelete", String.class,
            String.class, String.class);
        method.invoke(instance, "Demat", DUMMY_FUND, DUMMY_ADVISER);
      } else if (action.equals("subscriptionGet")) {
        Method method = instance.getClass().getMethod("subscriptionAdviserFundAccountGet", String.class, String.class,
            String.class);
        method.invoke(instance, "Demat", DUMMY_FUND, DUMMY_ADVISER);
      } else if (action.equals("Get Wallet Money")) {
        Method method = instance.getClass().getMethod("walletMoneyGet");
        method.invoke(instance);
      } else if (action.equals("resetPasswordPost")) {
        Method method = instance.getClass().getMethod("resetPasswordPost", ResetPassword.class);
        method.invoke(instance, getResetPasswordBody());
      } else if (action.equals("forgotPasswordGet")) {
        Method method = instance.getClass().getMethod("forgotPasswordGet", String.class);
        method.invoke(instance, DUMMY_EMAIL);
      } else if (action.equals("Get Wallet Money")) {
        Method method = instance.getClass().getMethod("walletMoneyGet");
        method.invoke(instance);
      } else if (action.equals("Get Role")) {
        Method method = instance.getClass().getMethod("userRoleGet");
        method.invoke(instance);
      } 
//      else if (action.equals("changePasswordPost")) {
//        Method method = instance.getClass().getMethod("changePasswordPost", ChangePasswordParameters.class);
//        method.invoke(instance, changePasswordBody());
//      } 

      return "Authorized";
    } catch (InvocationTargetException e) {
      Throwable targetException = e.getTargetException();
      if (targetException instanceof ApiClientException) {
        ApiClientException apiException = (ApiClientException) targetException;
        if (apiException.getStatusCode() == 403) {
          return "Not Authorized";
        } else if (apiException.getStatusCode() == 400) {
          return "Authorized";
        }
      }
      return targetException.getMessage();
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | ClassNotFoundException
        | IOException e) {
      return "ERROR" + e.getMessage();
    }
  }

  private ResetPassword getResetPasswordBody() {
    ResetPassword body = new ResetPassword();
    body.setOtp(DUMMY_OTP);
    body.setPassword(DUMMY_PASSWORD);
    body.setUserEmail(DUMMY_EMAIL);
    return body;
  }

  private Object instance(ApiClientFactory factory) throws ClassNotFoundException, IOException {
    log.info("environment : " + environment());
    Class<Object> clientClass = (Class<Object>) Class.forName("com.wt.client.InvestorAPI" + environment() + "Client");

    return (Object) factory.build(clientClass);
  }

  private AWSCredentialsProvider credentials(ApiClientFactory factory, String user)
      throws InterruptedException, ExecutionException, ClassNotFoundException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {

    Credentials jsonResponse = login(factory, user);
    String accessKey = jsonResponse.getCredentials().getAccessKey();
    String secretKey = jsonResponse.getCredentials().getSecretKey();
    String sessionToken = jsonResponse.getCredentials().getSessionToken();

    AWSCredentialsProvider provider = new AWSCredentialsProvider() {
      @Override
      public void refresh() {
      }

      @Override
      public AWSCredentials getCredentials() {
        return new BasicSessionCredentials(accessKey, secretKey, sessionToken);
      }
    };
    return provider;
  }

  public void register(String userName, String dateStr) throws InterruptedException, ExecutionException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException, UnsupportedEncodingException {

    Calendar date = plusMinusDate(dateStr);

    try {
      register(factory(), userName, date.getTimeInMillis());
    } catch (Exception e) {
      log.error(e);
    }

  }

  private Credentials register(ApiClientFactory factory, String userName, long timestamp)
      throws InterruptedException, ExecutionException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, IOException {
    User user = user(userName);
    user.setTimestamp(new BigDecimal(timestamp));
    user.setFirstName(userName);
    user.setLastName(userName);
    user.setDeviceToken(DUMMY_DEVICETOKEN);
    String request = "registerCustomPost";

    return invokeCredentialsCall(factory, user, request);
  }

  private Credentials login(ApiClientFactory factory, String userName)
      throws InterruptedException, ExecutionException, ClassNotFoundException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {
    User user = user(userName);

    String request = "loginCustomPost";

    return invokeCredentialsCall(factory, user, request);
  }

  private User user(String userName) {
    User user = new User();
    user.setDeviceToken(DUMMY_DEVICETOKEN);
    user.setUserEmail(userName);
    user.setPassword(userName);
    return user;
  }

  private Credentials invokeCredentialsCall(ApiClientFactory factory, User user, String request)
      throws ClassNotFoundException, IOException, NoSuchMethodException, IllegalAccessException,
      InvocationTargetException {
    try {
      final Object instance = instance(factory);
      Method method = instance.getClass().getMethod(request, User.class);
      Credentials invoke = (Credentials) method.invoke(instance, user);
      return invoke;
    } catch (Exception e) {
      log.info("e " + e.getMessage());
      return null;
    }
  }

  private String environment() throws IOException {
    return System.getProperty("environment");
  }

  private Calendar plusMinusDate(String dateStr) {
    Calendar date = Calendar.getInstance();
    if (dateStr.equals("current date")) {

    } else {
      String minusDaysStr = dateStr.replaceAll("[^\\d]", "");
      int minusDays = Integer.parseInt(minusDaysStr);
      date.add(Calendar.DAY_OF_MONTH, -1 * minusDays);
    }
    return date;
  }

  private SubscriptionParameters setSubcriptionParameters() {
    SubscriptionParameters subscriptionParameters = new SubscriptionParameters();
    subscriptionParameters.setSipDate(DUMMY_DATE);
    subscriptionParameters.setLumpSumAmount(LUMPSUMAMOUNT);
    subscriptionParameters.setSipAmount(SIPAMOUNT);
    subscriptionParameters.setWealthCode(WEALTHCODE);
    return subscriptionParameters;
  }

  public ChangePasswordParameters changePasswordBody() {
    ChangePasswordParameters body = new ChangePasswordParameters();
    body.setPassword(DUMMY_NEW_PASSWORD);
    return body;
  }

}
