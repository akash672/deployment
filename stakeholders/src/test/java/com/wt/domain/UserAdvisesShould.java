package com.wt.domain;

import static com.wt.domain.BrokerName.NOBROKER;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class UserAdvisesShould {
  private Set<UserAdvise> userAdvises = new CopyOnWriteArraySet<UserAdvise>();

  @Test
  public void be_unique_when_mirroring() {
    userAdvises.add(createUserAdviseWith("2312sasdasd", 10L, "description", "username", "clientCode", NOBROKER, "token",
        "symbol", 10., 20000.));
    userAdvises.add(createUserAdviseWith("2312sasdasd", 10L, "description", "username", "clientCode", NOBROKER, "token",
        "symbol", 20., 20000.));
    assertThat(userAdvises.size()).isEqualTo(1);
  }

  private UserAdvise createUserAdviseWith(String adviseId, long issueTime, String description, String username,
      String clientCode, BrokerName brokerName, String token, String symbol, double issueAdviceAbsoluteAllocation,
      double issueAdviseAllocationValue) {
    UserAdvise userAdvise = new UserEquityAdvise();
    userAdvise.setAdviseId(adviseId);
    userAdvise.setIssueTime(issueTime);
    userAdvise.setDescription(description);
    userAdvise.setClientCode(clientCode);
    userAdvise.setBrokerName(brokerName);
    userAdvise.setToken(token);
    userAdvise.setSymbol(symbol);
    userAdvise.setIssueAdviceAbsoluteAllocation(issueAdviceAbsoluteAllocation);
    userAdvise.setIssueAdviseAllocationValue(issueAdviseAllocationValue);
    return userAdvise;
  }
}
