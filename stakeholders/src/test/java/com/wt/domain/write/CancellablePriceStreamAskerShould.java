package com.wt.domain.write;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static org.mockito.Mockito.doReturn;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.reactivestreams.Publisher;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.wt.broadcast.xneutrino.CancellablePriceStream;
import com.wt.domain.EquityInstrument;
import com.wt.domain.Instrument;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.GetCancellableLivePriceStream;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.akka.CancellableStream;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.AsPublisher;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.testkit.javadsl.TestKit;

@RunWith(MockitoJUnitRunner.class)
public class CancellablePriceStreamAskerShould {
  private static PriceWithPaf price1 = new PriceWithPaf(new InstrumentPrice(103.0f, 0, "22-NSE-EQ"), Pafs.withCumulativePaf(1.));

  @Mock
  private static WDActorSelections actorSelections;
  private static final ActorSystem system = ActorSystem.create("TestSystem");
  private Props props = Props.create(TestableEquityActor.class, () -> new TestableEquityActor());
  private ActorRef testableEquityActor = system.actorOf(props);

  @org.junit.Before
  public void setup() {
    doReturn(system.actorSelection(testableEquityActor.path())).when(actorSelections).universeRootActor();
  }

  static class TestableEquityActor extends AbstractActor {
    private Multimap<String, ActorRef> streamSubscribers = HashMultimap.create();
    
    @Override
    public Receive createReceive() {
      return receiveBuilder().match(RealTimeInstrumentPrice.class, cmd -> {
        updateLastPriceWithoutPersisting(cmd);
        streamSubscribers.values().stream().forEach(streamSubscriber -> streamSubscriber.tell(price1, self()));
      }).match(GetCancellableLivePriceStream.class, cmd -> {
        sendCancellablePriceStreamToSubscribers(sender(), cmd.getWdId());
      }).build();
    }
    
    private void updateLastPriceWithoutPersisting(RealTimeInstrumentPrice cmd) {
      Pafs lastUpdatedCumulativePaf = Pafs.withCumulativePaf(1.);
      if (price1 != null)
        lastUpdatedCumulativePaf = price1.getPafs();
      if (cmd.getInstrumentPrice() != null)
        price1 = new PriceWithPaf(cmd.getInstrumentPrice(), lastUpdatedCumulativePaf);
    }
    
    private void sendCancellablePriceStreamToSubscribers(ActorRef senderActor, String wdId) {
      Pair<ActorRef, Publisher<Object>> sinkActorForPriceStreamWithPublisher = Source.actorRef(1, OverflowStrategy.dropHead())
          .toMat(Sink.asPublisher(AsPublisher.WITH_FANOUT), Keep.both()).run(ActorMaterializer.create(system));
      streamSubscribers.put(wdId, sinkActorForPriceStreamWithPublisher.first());
      Instrument instrument = wdId.contains("-MF") ? new MutualFundInstrument(wdId) : new EquityInstrument(wdId);
      senderActor.tell(new CancellablePriceStream<PriceWithPaf, NotUsed>(
      Source
      .fromPublisher(sinkActorForPriceStreamWithPublisher.second()).map(f -> (PriceWithPaf) f)
      .via(new SendLastReceived<PriceWithPaf>()), sinkActorForPriceStreamWithPublisher.first(),
      instrument), self());
    }
  }
  
  @Test public void 
  receive_prices_as_soon_as_they_arrive_on_equity_actor() throws Exception {
    new TestKit(system) {
      {
        @SuppressWarnings("unchecked")
        CancellableStream<PriceWithPaf, NotUsed> stockPricesStream = (CancellableStream<PriceWithPaf, NotUsed>) askAndWaitForResult(
            actorSelections.universeRootActor(), new GetCancellableLivePriceStream(price1.getPrice().getWdId()));
        
        stockPricesStream
        .getSource()
        .runWith(Sink.actorRef(getRef(), "Done"), ActorMaterializer.create(system));
        
        testableEquityActor.tell(new RealTimeInstrumentPrice(price1.getPrice()), ActorRef.noSender());
        
        expectMsgEquals(price1);
      }
    };

  }

}
