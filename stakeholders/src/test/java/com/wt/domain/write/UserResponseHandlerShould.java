package com.wt.domain.write;

import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;
import static com.wt.domain.AdviseType.Close;
import static com.wt.domain.AdviseType.Issue;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.UserResponseType.Approve;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DoublesUtil.round;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.config.utils.WDProperties;
import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.User;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.write.commands.AcceptUserApprovalResponse;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.GetPriceBasketFromTracker;
import com.wt.domain.write.commands.GetPriceFromTracker;
import com.wt.domain.write.commands.GetUserPendingRebalancingList;
import com.wt.domain.write.commands.PriceSnapshotsFromPriceSource;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;

@RunWith(MockitoJUnitRunner.class)
public class UserResponseHandlerShould {

  @Mock
  private static WDActorSelections actorSelections;
  @Mock
  private WDProperties wdProperties;
  private static final ActorSystem system = ActorSystem.create("TestSystem");
  private static final String secondAdviseId = "1b";
  private static final String FUND_NAME = "FUND";
  private static final String SUBSCRIPTION_USER_NAME = "Email";
  private static final String EXCHANGE = "exchange";
  private static final String ADVISER_IDENTITY = "adviser_identity";
  private static final String TICKER = "123-NSE-EQ";
  private static final String SYMBOL = "SYMBOL";
  private static final double AVAILABLE_MARGIN = 30000.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2 = 10;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2 = 25000;
  private static final long ENTRYTIME_2 = 2;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1 = 20;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1 = 10000;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_3 = 5000;
  private static final long ENTRYTIME = 1;
  private static final String firstAdviseId = "1a";
  protected static final long EXITTIME = 0;
  protected static final double EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1 = 5.;
  protected static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 = 100;
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;
  private Props props = Props.create(TestableEquityActor.class, () -> new TestableEquityActor());
  private Props props1 = Props.create(TestableActiveInstrumentActor.class, () -> new TestableActiveInstrumentActor());
  private ActorRef testableEquityActor = system.actorOf(props);
  private ActorRef testableActiveInstrumentActor = system.actorOf(props1);
  private User user = new User();
  
  @Before
  public void setup() throws NumberFormatException, IOException {
    doReturn(system.actorSelection(testableEquityActor.path())).when(actorSelections).universeRootActor();
    doReturn(system.actorSelection(testableEquityActor.path())).when(actorSelections).userRootActor();
    doReturn(system.actorSelection(testableActiveInstrumentActor.path())).when(actorSelections).activeInstrumentTracker();
    user.setEmail(SUBSCRIPTION_USER_NAME);
  }
  
  @Test public void
  not_honor_approvals_if_available_margin_is_less_than_required_margin() throws JsonProcessingException {
    new TestKit(system) {
      {
          actorSelections.userRootActor().tell(new GetUserPendingRebalancingList(SUBSCRIPTION_USER_NAME, VIRTUAL.getInvestingMode()), getRef());
          
          List<UserResponseParameters> allUserResponseParameter = Stream.of(
              new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
                  SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_IDENTITY, TICKER, SYMBOL, VIRTUAL, Approve, 
                  EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2),
              new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
                  SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_IDENTITY, TICKER, SYMBOL, VIRTUAL, Approve, 
                  EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1, 0, ENTRYTIME))
              .collect(Collectors.toList());
          ObjectMapper objectMapper = new ObjectMapper();
          String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
          actorSelections.userRootActor().tell(new AcceptUserApprovalResponse(SUBSCRIPTION_USER_NAME, userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST, BrokerAuthInformation.withNoBrokerAuth()), getRef());
          expectMsgEquals(failed(SUBSCRIPTION_USER_NAME, "Could not process approvals as " +  
          round((EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2 + EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1)) + 
          " is more than " + AVAILABLE_MARGIN + "", 
              PRECONDITION_FAILED));
        }
      };
  }
  
  @Test public void
  honor_approvals_if_required_margin_is_more_than_available_margin() throws JsonProcessingException {
    new TestKit(system) {
      {
        actorSelections.userRootActor().tell(new GetUserPendingRebalancingList(SUBSCRIPTION_USER_NAME, VIRTUAL.getInvestingMode()), getRef());
        
        List<UserResponseParameters> allUserResponseParameter = Stream.of(
            new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
                SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_IDENTITY, TICKER, SYMBOL, VIRTUAL, Approve, 
                EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2),
            new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
                SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_IDENTITY, TICKER, SYMBOL, VIRTUAL, Approve, 
                EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_3, 0, ENTRYTIME))
            .collect(Collectors.toList());
        
        ObjectMapper objectMapper = new ObjectMapper();
        String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
        actorSelections.userRootActor().tell(new AcceptUserApprovalResponse(SUBSCRIPTION_USER_NAME, userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST, BrokerAuthInformation.withNoBrokerAuth()), getRef());
        expectMsgEquals(new Done(SUBSCRIPTION_USER_NAME, "Successfully processed approvals!"));
        }
      };
  }
  
  @Ignore
  @Test public void
  honor_approvals_if_required_margin_after_adjusting_for_close_advice_is_less_than_available_margin() throws JsonProcessingException {
    new TestKit(system) {
      {
          actorSelections.userRootActor().tell(new GetUserPendingRebalancingList(SUBSCRIPTION_USER_NAME, VIRTUAL.getInvestingMode()), getRef());
          
          List<UserResponseParameters> allUserResponseParameter = Stream.of(
              new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
                  SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_IDENTITY, TICKER, SYMBOL, VIRTUAL, Approve, 
                  EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2),
              new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
                  SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_IDENTITY, TICKER, SYMBOL, VIRTUAL, Approve, 
                  EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1, 0, (int) ((EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 * EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1) / 100.), EXITTIME))
              .collect(Collectors.toList());
          
          ObjectMapper objectMapper = new ObjectMapper();
          String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
          actorSelections.userRootActor().tell(new AcceptUserApprovalResponse(SUBSCRIPTION_USER_NAME, userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST, BrokerAuthInformation.withNoBrokerAuth()), getRef());
          expectMsgEquals(new Done(SUBSCRIPTION_USER_NAME, "Successfully processed approvals!"));
        }
      };
  }
  
  class TestableEquityActor extends AbstractActor {
    
    @Override
    public Receive createReceive() {
      return receiveBuilder().match(GetPriceFromTracker.class, cmd -> {
        sender().tell(new PriceWithPaf(new InstrumentPrice(1040., currentTimeInMillis(), TICKER), Pafs.withCumulativePaf(1.)), self());
      }).match(MarginReceived.class, cmd -> {
        user.update(cmd);
      }).match(GetUserPendingRebalancingList.class, cmd -> {
          user.setVirtualWalletAmount(AVAILABLE_MARGIN);
      }).match(AcceptUserApprovalResponse.class, cmd -> {
        handleApprovals(cmd, VIRTUAL);
      }).build();
    }
    
    private void handleApprovals(AcceptUserApprovalResponse acceptUserApprovalResponse, InvestingMode investingMode) {
      if (user.handleApprovals(acceptUserApprovalResponse, VIRTUAL, actorSelections)) {
        sender().tell(new Done(SUBSCRIPTION_USER_NAME, "Successfully processed approvals!"), self());
      } else {
        sender().tell(Failed.failed(user.getEmail(), "Could not process approvals as " + 
            user.requiredMarginForApproval(acceptUserApprovalResponse, investingMode, actorSelections) + " is more than " + 
            (investingMode.equals(VIRTUAL) ? user.getVirtualWalletAmount() : user.getRealWalletAmount()), PRECONDITION_FAILED), self());
      }
    }
  }
  
  class TestableActiveInstrumentActor extends AbstractActor {
    
    @Override
    public Receive createReceive() {
      return receiveBuilder().match(GetPriceBasketFromTracker.class, cmd -> {
        HashMap<String, PriceWithPaf> localTokenToPrice = new HashMap<String, PriceWithPaf>();
        localTokenToPrice.put(TICKER, new PriceWithPaf(new InstrumentPrice(1040., currentTimeInMillis(), TICKER), Pafs.withCumulativePaf(1.)));
        sender().tell(new PriceSnapshotsFromPriceSource(localTokenToPrice), self());
      }).build();
    }
  } 
}
