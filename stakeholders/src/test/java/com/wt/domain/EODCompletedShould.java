package com.wt.domain;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static org.apache.commons.io.FileUtils.deleteDirectory;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.events.Done;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.testkit.javadsl.TestKit;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@Ignore
public class EODCompletedShould {

  private @Autowired ActorSystem system;
  private ActorRef userRootActor;
  
  @Before public void 
  setUp() {
    userRootActor = system.actorOf(SpringExtProvider.get(system).props("UserRootActor"), "user-aggregator");
  }
  
  @Test public void
  start_calculating_user_performance_at_EOD() {
    new TestKit(system) {
      {
        TestKit probe = new TestKit(system);
        userRootActor.tell(new EODCompleted(), getRef());
        probe.expectMsgEquals(new Done("EOD", "Received EOD message."));
      }
    };
    
  }
  
  @After public void 
  deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    try {
      deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  
}
