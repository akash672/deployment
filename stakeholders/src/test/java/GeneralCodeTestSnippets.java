import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GeneralCodeTestSnippets {

  @Test
  public void getUsernameFromEmailTest() {
    String userEmail = "wdtest@yopmail.com";
    assertEquals("wdtest", userEmail.substring(0, userEmail.lastIndexOf("@")));

    userEmail = "wdtester@yoil.com";
    assertEquals("wdtester", userEmail.split("@")[0]);
  }
  
  @Test
  public void test_last_name_extractor() {
   assertEquals("Tech Services", extractLastName("Wealth Tech Services"));
   assertEquals("Lastname", extractLastName("Firstname Lastname"));
  }
  
  private String extractLastName(String name) {
    String[] split = name.split(" ");
    String lastName = "";
    for (int i = 1; i < split.length; i++) {
      lastName += split[i] + " ";
    }
    return lastName.trim();
  }

}
