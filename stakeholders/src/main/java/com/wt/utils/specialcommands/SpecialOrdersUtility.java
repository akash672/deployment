package com.wt.utils.specialcommands;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.write.events.SpecialCircumstancesBuy;
import com.wt.domain.write.events.SpecialCircumstancesSell;
import com.wt.utils.akka.WDActorSelections;

@Component
public class SpecialOrdersUtility {
  
  private static AnnotationConfigApplicationContext ctx;
  private WDActorSelections wdActorSelections;
  
  @Autowired
  public SpecialOrdersUtility( WDActorSelections actorSelections){
    this.wdActorSelections = actorSelections;
  }
  
    public static void main(String... args) throws Exception {
      ctx = new AnnotationConfigApplicationContext(SpecialCommandsUtilityConfig.class);
      SpecialOrdersUtility specialUtility = ctx.getBean(SpecialOrdersUtility.class);
      specialUtility.upload(args[0]);
     }

    private void upload(String fileName) throws Exception {
        String line = "";
        String cvsSplitBy = ",";
        boolean hasHeader = true;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            
            while ((line = br.readLine()) != null) {
                if(hasHeader){
                    hasHeader = false;
                    continue;
                }
               
                String[] contents = line.split(cvsSplitBy);
                if(contents[0].equals("BUY")){
                    SpecialCircumstancesBuy buyCommand = new  SpecialCircumstancesBuy(contents[1],contents[2], Double.parseDouble(contents[3]), Integer.parseInt(contents[4]),contents[5], contents[6],
                            contents[7], contents[8], contents[9], BrokerName.valueOf( contents[10]), InvestingMode.valueOf(contents[11]));
                    wdActorSelections.userRootActor().tell(buyCommand, null);
                }else if(contents[0].equals("SELL")){
                    SpecialCircumstancesSell sellCommand = new SpecialCircumstancesSell(contents[1],contents[2], Double.parseDouble(contents[3]), Integer.parseInt(contents[4]),contents[5], contents[6],
                            contents[7], contents[8], contents[9], BrokerName.valueOf( contents[10]), InvestingMode.valueOf(contents[11]));
                    wdActorSelections.userRootActor().tell(sellCommand, null);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
