package com.wt.utils.specialcommands;

import static com.typesafe.config.ConfigFactory.load;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.typesafe.config.Config;
import com.wt.utils.CommonAppConfiguration;
import com.wt.utils.akka.SpringExtension;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;

@EnableAsync
@Configuration
@EnableScheduling
@ComponentScan(basePackages = {"com.wt.utils.specialcommands","com.wt.utils.akka"})
@PropertySource("classpath:environment.properties")
public class SpecialCommandsUtilityConfig extends CommonAppConfiguration {
  private ApplicationContext applicationContext;
  
  @Autowired
  public SpecialCommandsUtilityConfig(ApplicationContext applicationContext) {
    super();
    this.applicationContext = applicationContext;
  }
  
  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() throws IOException {
    setSystemProperties();
    String config = "SpecialCommands-application.conf";
    String actorName = "SpecialCommands";
    if (System.getProperty("appConfig") != null) {
      config = System.getProperty("appConfig");
      actorName = System.getProperty("appConfig").split("-")[0];
    }
    Config conf = load(config);
    ActorSystem system = ActorSystem.create(actorName, conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }
  
  @Bean(destroyMethod = "shutdown")
  public ActorMaterializer actorMaterializer() throws IOException {
    return ActorMaterializer.create(actorSystem());
  }
  
  @Autowired
  @Lazy
  protected WDActorSelections wdActorSelections;
  
 }

