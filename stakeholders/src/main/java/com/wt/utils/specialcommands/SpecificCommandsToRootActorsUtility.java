package com.wt.utils.specialcommands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.wt.domain.write.commands.UpdateTheTokenToWdIdMap;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;

@Component
public class SpecificCommandsToRootActorsUtility {
  private static AnnotationConfigApplicationContext ctx;
  private WDActorSelections wdActorSelections;
  
  @Autowired
  public SpecificCommandsToRootActorsUtility( WDActorSelections actorSelections){
    this.wdActorSelections = actorSelections;
  }
  
  public static void main(String... args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(SpecialCommandsUtilityConfig.class);
    SpecificCommandsToRootActorsUtility specialUtility = ctx.getBean(SpecificCommandsToRootActorsUtility.class);
    specialUtility.sendCommand();
   }

  private void sendCommand() {
    
    wdActorSelections.universeRootActor().tell(new UpdateTheTokenToWdIdMap(),  ActorRef.noSender());
    
  }
}
