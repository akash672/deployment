package com.wt.utils;

import lombok.Getter;

public enum CommandPurpose {
  CalculateAdviseIdToShares("CalculateAdviseIdToShares");

  @Getter
  private final String commandPurpose;

  private CommandPurpose(String commandPurpose) {
    this.commandPurpose = commandPurpose;
  }

}
