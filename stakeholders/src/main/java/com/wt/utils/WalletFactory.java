package com.wt.utils;

import static com.wt.domain.InvestingMode.REAL;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.wt.domain.InvestingMode;
import com.wt.domain.Wallet;

@Component
public class WalletFactory {

  private Wallet realWallet;

  @Autowired
  public WalletFactory(@Qualifier("RealWallet") Wallet realWallet) {
    super();
    this.realWallet = realWallet;
  }

  public Wallet getWallet(String brokerName, InvestingMode investingMode) throws IOException {
    if (investingMode.equals(REAL))
      return realWallet;
    return realWallet;
  }

}
