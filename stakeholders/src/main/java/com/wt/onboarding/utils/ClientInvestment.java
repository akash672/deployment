package com.wt.onboarding.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class ClientInvestment {
  private String adviseId;
  private String symbol;
  private String exchange;
  private double quantity;
  private double price;
}
