package com.wt.onboarding.utils;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class AdviserClientDetails {
  private String adviserName;
  private String fundName;
  private String brokerName;
  private String firstName;
  private String lastName;
  private String clientEmailId;
  private String clientUsername;
  private String phoneNumber;
  private String panCardNumber;
  private String clientCode;
  private double lumpSumAmount;
  private double cash;
  private List<ClientInvestment> clientSpecifications = new ArrayList<ClientInvestment>();

}