package com.wt.onboarding.utils;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ClientCodeWithSymbol {
  private String clientCode;
  private String symbol;
}
