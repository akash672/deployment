package com.wt.onboarding.utils;

import java.util.ArrayList;
import java.util.List;

import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
public class AdviserClientDetailsBuilder {
  
  private List<ClientInvestment> clientSpecifications = new ArrayList<ClientInvestment>();
  private String adviserName;
  private String fundName;
  private String brokerName;
  private String firstName;
  private String lastName;
  private String clientEmailId;
  private String clientUsername;
  private String panCardNumber;
  private String phoneNumber;
  private String clientCode;
  private double lumpSumAmount;
  private double cash;
  
  public AdviserClientDetailsBuilder withAdviserName(String adviserName) {
    this.adviserName = adviserName;
    return this;
  }
  
  public AdviserClientDetailsBuilder withFundName(String fundName) {
    this.fundName = fundName;
    return this;
  }
  
  public AdviserClientDetailsBuilder withBrokerName(String brokerName) {
    this.brokerName = brokerName;
    return this;
  }
  
  public AdviserClientDetailsBuilder withFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }
  
  public AdviserClientDetailsBuilder withLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }
  
  public AdviserClientDetailsBuilder withClientEmailId(String clientEmailId) {
    this.clientEmailId = clientEmailId;
    return this;
  }
  
  public AdviserClientDetailsBuilder withClientUsername(String clientUsername) {
    this.clientUsername = clientUsername;
    return this;
  }
  
  public AdviserClientDetailsBuilder withPanNumber(String panCardNumber) {
    this.panCardNumber = panCardNumber;
    return this;
  }
  
  public AdviserClientDetailsBuilder withPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }
  
  public AdviserClientDetailsBuilder withClientCode(String clientCode) {
    this.clientCode = clientCode;
    return this;
  }
  
  public AdviserClientDetailsBuilder withLumpSumAmount(double lumpSumAmount) {
    this.lumpSumAmount = lumpSumAmount;
    return this;
  }
  
  public AdviserClientDetailsBuilder withCash(double cash) {
    this.cash = cash;
    return this;
  }
  
  public AdviserClientDetailsBuilder withInvestments(ClientInvestment clientInvestment) {
    clientSpecifications.add(clientInvestment);
    return this;
  }
  
  public AdviserClientDetails build() {
    return new AdviserClientDetails(adviserName, fundName, brokerName, firstName, lastName,
        clientEmailId, clientUsername, phoneNumber, panCardNumber, clientCode, lumpSumAmount, cash, clientSpecifications);
  }
  
}
