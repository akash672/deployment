package com.wt.onboarding.utils;

import static com.wt.domain.FundConstituent.withActiveAdvise;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.PriceWithPaf.withPrice;
import static com.wt.domain.write.events.Pafs.withCumulativePaf;
import static com.wt.utils.DateUtils.todayPrettyTime;
import static com.wt.utils.NumberUtils.containsNumbers;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.Double.parseDouble;
import static java.util.stream.StreamSupport.stream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.wt.domain.EquityInstrument;
import com.wt.domain.FundConstituent;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.commands.GetFundAdvises;
import com.wt.domain.write.commands.GetIdentityIdByAdviserUsername;
import com.wt.domain.write.commands.GetIdentityIdByUsername;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.RegisterExistingUser;
import com.wt.domain.write.commands.SubscribeToFund;
import com.wt.domain.write.commands.UpdateWealthCode;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviseListResponse;
import com.wt.domain.write.events.FundConstituentListResponse;
import com.wt.utils.CoreAppConfiguration;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
@Service("CustomerOnboardingService")
public class CustomerOnboardingService {

  private static AnnotationConfigApplicationContext ctx;
  private static final String DEFAULT_USER_WEALTHCODE = "1234";
  private static final String DEFAULT_SIP_DATE = todayPrettyTime();
  private CSVParser adviserClientFileReader;
  @Getter
  private AdviserClientDetails adviserClientDetails;
  @Getter
  private Map<ClientCodeWithSymbol, PriceWithPaf> clientCodeWithSymbolToPrice;
  private Map<String, FundConstituentListResponse> userEmailToFundComposition;
  private WDActorSelections actorSelection;

  @Autowired
  public CustomerOnboardingService(WDActorSelections actorSelection) {
    super();
    this.clientCodeWithSymbolToPrice = new ConcurrentHashMap<ClientCodeWithSymbol, PriceWithPaf>();
    this.userEmailToFundComposition = new ConcurrentHashMap<String, FundConstituentListResponse>();
    this.actorSelection = actorSelection;
  }

  public static void main(String[] args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(CoreAppConfiguration.class);
    CustomerOnboardingService customerOnboardingService = ctx.getBean(CustomerOnboardingService.class);
    createProcessorActors();
    Files.newDirectoryStream(Paths.get(args[0]), path -> path.toFile().isFile()).forEach(fileName -> {
      try {
        customerOnboardingService.processAdviserClientFile(fileName.toFile());
        customerOnboardingService.computeFundCompositions();
        if (customerOnboardingService.validateFundAndUserAdvises(fileName.toFile().getName())) {
          customerOnboardingService.populateClientCodeWithSymbolToPriceMap();
          customerOnboardingService.startOnBoardingProcess();
        }
        Thread.sleep(50000);
        fileName.toFile().delete();
      } catch (Exception e) {
        log.error("Unable to read " + fileName.toFile().getName() + " due to " + e.getMessage() + " Exception type: "
            + e.getStackTrace());
      }
    });
  }

  private static void createProcessorActors() {
    final ActorSystem system = (ActorSystem) ctx.getBean("actorSystem");
    system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"), "adviser-aggregator");
    system.actorOf(SpringExtProvider.get(system).props("UserRootActor"), "user-aggregator");
  }

  public void processAdviserClientFile(File adviserClientFile) throws FileNotFoundException, IOException {
    try {
      adviserClientFileReader = new CSVParser(new FileReader(adviserClientFile), CSVFormat.DEFAULT);
      AdviserClientDetailsBuilder adviserClientDetailsBuilder = new AdviserClientDetailsBuilder();

      stream(adviserClientFileReader.spliterator(), false).forEach(new Consumer<CSVRecord>() {
        @Override
        public void accept(CSVRecord record) {
          if (!containsNumbers(record.get(0))) {
            if (record.get(0).equals("UserEmail"))
              RecordType.fromRecordLabel("Username").handleRecord(record.get(1).trim().split("@")[0], adviserClientDetailsBuilder);
            RecordType.fromRecordLabel(record.get(0)).handleRecord(record.get(1).trim(), adviserClientDetailsBuilder);
          } else
            adviserClientDetailsBuilder.withInvestments(new ClientInvestment(record.get(0), record.get(1),
                record.get(2), parseDouble(record.get(3)), parseDouble(record.get(4))));
        }
      });
      adviserClientDetails = adviserClientDetailsBuilder.build();
    } catch (Exception e) {
    }
  }

  public void computeFundCompositions() {
    List<FundConstituent> fundConstituents = new ArrayList<FundConstituent>();
    adviserClientDetails.getClientSpecifications().stream().forEach(clientInvestment -> {
      String token = getInstrumentFromUniverseRootActor(clientInvestment.getSymbol(), clientInvestment.getExchange(),
          actorSelection).getWdId();
      double allocation = ((clientInvestment.getQuantity() * clientInvestment.getPrice())
          / adviserClientDetails.getLumpSumAmount()) * 100.;
      FundConstituent fundConstituent = withActiveAdvise(clientInvestment.getAdviseId(),
          adviserClientDetails.getFundName(), adviserClientDetails.getAdviserName(), token,
          clientInvestment.getSymbol(), allocation,
          withPrice(clientInvestment.getPrice(), token, withCumulativePaf(1.)));
      fundConstituents.add(fundConstituent);
    });
    userEmailToFundComposition.put(adviserClientDetails.getClientEmailId(), new FundConstituentListResponse(
        adviserClientDetails.getFundName(), adviserClientDetails.getFundName(), fundConstituents));
  }

  public void populateClientCodeWithSymbolToPriceMap() {
    adviserClientDetails.getClientSpecifications().stream()
        .forEach(clientSpecification -> clientCodeWithSymbolToPrice.put(
            new ClientCodeWithSymbol(adviserClientDetails.getClientCode(), clientSpecification.getSymbol()),
            withPrice(clientSpecification.getPrice(),
                getInstrumentFromUniverseRootActor(clientSpecification.getSymbol(), clientSpecification.getExchange(),
                    actorSelection).getWdId(),
                withCumulativePaf(1.))));
  }

  public void startOnBoardingProcess() throws Exception {
    registerUser();
    Thread.sleep(5*1000);
    completeUserKYC();
    updateWealthCode();
    susbcribeUserToFund();
  }

  private void registerUser() throws ParseException {
    actorSelection.userRootActor()
        .tell(new RegisterExistingUser(adviserClientDetails.getClientUsername(), adviserClientDetails.getFirstName(),
            adviserClientDetails.getLastName(), adviserClientDetails.getClientEmailId()),
            ActorRef.noSender());
  }

  private void completeUserKYC() {
    actorSelection.userRootActor()
        .tell(new KycApproveCommand(adviserClientDetails.getClientCode(), adviserClientDetails.getPanCardNumber(),
            adviserClientDetails.getClientUsername(), adviserClientDetails.getBrokerName(),
            adviserClientDetails.getPhoneNumber(), adviserClientDetails.getClientEmailId()), ActorRef.noSender());
  }

  private void updateWealthCode() throws Exception {
    String username = (String) askAndWaitForResult(actorSelection.userRootActor(),
        new GetIdentityIdByUsername(adviserClientDetails.getClientUsername()));

    actorSelection.userRootActor().tell(new UpdateWealthCode(DEFAULT_USER_WEALTHCODE, username), ActorRef.noSender());
  }

  private void susbcribeUserToFund() throws Exception {
    String username = (String) askAndWaitForResult(actorSelection.userRootActor(),
        new GetIdentityIdByUsername(adviserClientDetails.getClientUsername()));

    actorSelection.userRootActor()
        .tell(new SubscribeToFund(adviserClientDetails.getAdviserName(), adviserClientDetails.getFundName(), username,
            adviserClientDetails.getLumpSumAmount(), adviserClientDetails.getCash(), DEFAULT_SIP_DATE, REAL,
            Purpose.ONBOARDING), ActorRef.noSender());
  }

  private EquityInstrument getInstrumentFromUniverseRootActor(String symbol, String exchange,
      WDActorSelections actorSelection) {
    log.info("GET INSTRUMENT FOR: "+symbol+", "+exchange);
    Object result;
    try {
      result = askAndWaitForResult(actorSelection.universeRootActor(),
          new GetEquityInstrumentBySymbol(symbol, exchange));
      if (result instanceof Failed) {
        log.error("Error while getting equity instrument " + result);
        return null;
      }
      log.info("GOT INSTRUMENT FOR: "+result.toString());
      return (EquityInstrument) result;
    } catch (Exception e) {
      log.error("Error while getting equity instrument " + e.getMessage());
    }
    return null;
  }

  public boolean validateFundAndUserAdvises(String fileName) throws Exception {
    String username = (String) askAndWaitForResult(actorSelection.advisersRootActor(),
        new GetIdentityIdByAdviserUsername(adviserClientDetails.getAdviserName()));
    FundAdviseListResponse fundAdviseList = (FundAdviseListResponse) askAndWaitForResult(
        actorSelection.advisersRootActor(), new GetFundAdvises(username, adviserClientDetails.getFundName()));
    Stream<String> fundAdvises = fundAdviseList.getFundAdviseList().stream().map(advise -> advise.getId());
    Stream<String> userAdvises = adviserClientDetails.getClientSpecifications().stream()
        .map(clientInvestment -> clientInvestment.getAdviseId());
    Set<String> combinedFundAndUserAdvises = Stream.concat(fundAdvises, userAdvises).collect(Collectors.toSet());
    if (combinedFundAndUserAdvises.size() == fundAdviseList.getFundAdviseList().size())
      return true;
    else
      log.error("The client file " + fileName + " does not have advises similar to the fund being subscribed to.");
    return false;
  }

  public FundConstituentListResponse getFundCompositionForUser(String userEmail) {
    return userEmailToFundComposition.get(userEmail);
  }

  public PriceWithPaf getPriceForClientCodeAndSymbol(String clientCode, String symbol) {
    return clientCodeWithSymbolToPrice.get(new ClientCodeWithSymbol(clientCode, symbol));
  }

}
