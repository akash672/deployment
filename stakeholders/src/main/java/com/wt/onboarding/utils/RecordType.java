package com.wt.onboarding.utils;

import static com.google.common.collect.Maps.uniqueIndex;
import static java.lang.Double.parseDouble;
import static java.util.Arrays.asList;

import java.util.Map;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum RecordType {
  FIRSTNAME("FirstName") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetailsBuilder) {
      adviserClientDetailsBuilder.withFirstName(recordValue);
    }
  },
  LASTNAME("LastName") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withLastName(recordValue);
    }
  },
  USEREMAIL("UserEmail") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withClientEmailId(recordValue);
    }
  },
  USERNAME("Username") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withClientUsername(recordValue);
    }
  },
  PHONENUMBER("PhoneNumber") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withPhoneNumber(recordValue);
    }
  },
  CLIENTCODE("ClientCode") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withClientCode(recordValue);
    }
  },
  PANNUMBER("PanNumber") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withPanNumber(recordValue);
    }
  },
  BROKERNAME("BrokerName") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withBrokerName(recordValue);
    }
  },
  FUNDNAME("FundName") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withFundName(recordValue);
    }
  },
  ADVISERNAME("AdviserName") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withAdviserName(recordValue);
    }
  },
  LUMPSUMAMOUNT("LumpSumAmount") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withLumpSumAmount(parseDouble(recordValue));
    }
  },
  CASH("Cash") {
    @Override
    public void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder) {
      adviserClientDetialsBuilder.withCash(parseDouble(recordValue));
    }
  };

  private static final Map<String, RecordType> recordNameWithRecordType = uniqueIndex(asList(RecordType.values()),
      RecordType::getRecordType);
  
  @Getter private final String recordType;
 
  @NotNull
  public static RecordType fromRecordLabel(String recordLabel) {
    return recordNameWithRecordType.get(recordLabel);
  }
  
  public abstract void handleRecord(String recordValue, AdviserClientDetailsBuilder adviserClientDetialsBuilder);
  
}
