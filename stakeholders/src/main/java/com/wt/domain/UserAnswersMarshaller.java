package com.wt.domain;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshaller;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@Deprecated
public class UserAnswersMarshaller implements DynamoDBMarshaller<Map<String, Map<String, Object>>> {
  @Override
  public String marshall(Map<String, Map<String, Object>> objects) {
    return new Gson().toJson(objects);
  }

  @Override
  public Map<String, Map<String, Object>> unmarshall(Class<Map<String, Map<String, Object>>> clazz, String object) {
    return new Gson().fromJson(object, new TypeToken<Map<String, Map<String, Object>>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }
}
