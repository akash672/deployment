package com.wt.domain;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class OrderListMarshaller implements DynamoDBTypeConverter<String, Object> {
  @Override
  public String convert(Object obj) {
    if (obj instanceof Order) {
      Order order = (Order) obj;
      return new Gson().toJson(order);
    } else if (obj instanceof Collection<?>) {
      @SuppressWarnings("unchecked")
      Set<Order> setOfOrders = (Set<Order>) obj;
      return new Gson().toJson(setOfOrders);
    }
    return new Gson().toJson(obj);
  }

  @Override
  public Set<Order> unconvert(String object) {
    Set<Order> fromDb;
    try {
      fromDb = new Gson().fromJson(object, new TypeToken<Set<Order>>() {

        private static final long serialVersionUID = 1L;
      }.getType());
      fromDb = new TreeSet<>(fromDb);
    } catch (JsonSyntaxException e) {
      fromDb = new Gson().fromJson(object, new TypeToken<Order>() {

        private static final long serialVersionUID = 1L;
      }.getType());
    }
    return fromDb;
  }

}
