package com.wt.domain;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.persistence.Entity;

import com.wt.domain.write.events.UserAdviseAbsolutePerformanceCalculated;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceWithDate;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.utils.akka.CancellableStream;

import akka.NotUsed;
import akka.stream.Materializer;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@Entity
@EqualsAndHashCode
public abstract class UserAdvise {
  protected String isA="UserAdvise"; 


  public abstract String getAdviseId();

  public abstract void setAdviseId(String adviseId);

  public abstract String getUsernameWithFundWithAdviser();

  public abstract void setUsernameWithFundWithAdviser(String userWithFundWithAdviser);

  public abstract void update(UserAdviseEvent childUserAdviseEvent);

  public abstract UserAdviseCompositeKey getUserAdviseCompositeKey();

  public abstract void setUserAdviseCompositeKey(UserAdviseCompositeKey userAdviseCompositeKey);

  public abstract long getIssueTime();

  public abstract void setIssueTime(long issueTime);

  public abstract String getDescription();
  
  public abstract String getBasketOrderCompositeId();

  public abstract void setDescription(String description);

  public abstract String getUsername();

  @Deprecated
  public abstract void setUsername(String username);

  public abstract String getClientCode();

  public abstract void setClientCode(String clientCode);

  public abstract BrokerName getBrokerName();

  public abstract void setBrokerName(BrokerName brokerName);

  public abstract String getToken();

  public abstract void setToken(String token);

  public abstract String getSymbol();

  public abstract void setSymbol(String symbol);

  public abstract double getIssueAdviceAbsoluteAllocation();

  public abstract void setIssueAdviceAbsoluteAllocation(double issueAdviceAbsoluteAllocation);

  public abstract double getIssueAdviseAllocationValue();

  public abstract void setIssueAdviseAllocationValue(double issueAdviseAllocationValue);

  public abstract double getCloseAdviseAbsoluteAllocation();

  public abstract void setCloseAdviseAbsoluteAllocation(double closeAdviseAbsoluteAllocation);

  public abstract Set<Order> getIssueAdviseOrder();

  public abstract Optional<Order> getLatestIssuedAdviseOrder();
  
  public abstract Optional<Order> getLatestSellOrder();

  public abstract Set<Order> getCloseAdviseOrders();

  public abstract void setCloseAdviseOrders(Set<Order> closeAdviseOrders);

  public abstract Set<Order> getCorporateEventOrders();

  public abstract void setCorporateEventOrders(Set<Order> corporateEventOrders);
  
  public abstract Optional<Order> getLatestCorpActionOrder();

  public abstract int integerQuantityYetToBeExited();

  public abstract double doubleQuantityYetToBeExited();

  public abstract Map<Long, Double> getDateToCumulativePafMap();

  public abstract void setDateToCumulativePafMap(Map<Long, Double> dateToCumulativePafMap);

  public abstract Map<String, Double> getCorporateEventDetailstoPaf();

  public abstract void setCorporateEventDetailstoPaf(Map<String, Double> corporateEventDetailstoPaf);

  public abstract PriceWithPaf getAverageEntryPrice();

  public abstract void setAverageEntryPrice(PriceWithPaf averageEntryPrice);

  public abstract PriceWithPaf getAverageExitPrice();

  public abstract void setAverageExitPrice(PriceWithPaf averageExitPrice);

  public abstract double getCumulativePaf();

  public abstract void setCumulativePaf(double cumulativePaf);
  
  public abstract double getCumulativeDividendsReceived();

  public double exitedEquityValue(long date) {
    double exitedValue = 0D;
    try {
      exitedValue = getCloseAdviseOrders().stream()
          .filter(order -> order.getOrderKey().getOrderTimestamp() < date)
          .flatMap(order -> order.getTrades().stream())
          .mapToDouble(trade -> trade.getPrice() * trade.getIntegerQuantity()).sum();
    } catch (Exception e) {
      exitedValue = 0D;
    }
    return exitedValue;
  }

  public double exitedMFValue(long date) {
    double exitedValue = 0D;
    try {
      exitedValue = getCloseAdviseOrders().stream()
          .filter(order -> order.getOrderKey().getOrderTimestamp() < date)
          .flatMap(order -> order.getTrades().stream())
          .mapToDouble(trade -> trade.getPrice() * trade.getQuantity()).sum();
    } catch (Exception e) {
      exitedValue = 0D;
    }
    return exitedValue;
  }

  public abstract CancellableStream<UserAdviseAbsolutePerformanceCalculated, NotUsed> realtimePerformanceUsing(
      CancellableStream<PriceWithPaf, NotUsed> stockPricesStream);

  public abstract List<UserAdviseAbsolutePerformanceWithDate> historicalPerformance(List<PriceWithPaf> listOfPriceEvent,
      Materializer materializer) throws InterruptedException, ExecutionException;

  public boolean hasIssueAdviceTradeBeenProcessed(String tradeId) {
    return getIssueAdviseOrder().stream().flatMap(order -> order.getTrades().stream()).anyMatch(trade -> trade.getOrderTradeId().equals(tradeId)); 
  }

  public boolean hasCloseAdviceTradeBeenProcessed(String tradeId) {
    return getCloseAdviseOrders().stream().flatMap(order -> order.getTrades().stream())
        .anyMatch(trade -> trade.getOrderTradeId().equals(tradeId));
  }

}
