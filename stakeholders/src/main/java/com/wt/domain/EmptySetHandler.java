package com.wt.domain;

import java.util.Set;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class EmptySetHandler implements DynamoDBTypeConverter<String, Set<String>> {

  @Override
  public String convert(Set<String> object) {
    if (object.isEmpty())
      object.add("NA");
    return new Gson().toJson(object);
  }

  @Override
  public Set<String> unconvert(String object) {
    Set<String> retrievedSet = new Gson().fromJson(object, new TypeToken<Set<String>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
    retrievedSet.remove("NA");
    return retrievedSet;
  }

}
