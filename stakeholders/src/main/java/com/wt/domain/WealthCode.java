package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class WealthCode {
  private String wealthCode;

  @JsonCreator
  public WealthCode(@JsonProperty("wealthCode") String wealthCode) {
    super();
    this.wealthCode = wealthCode;
  }
  
  
}
