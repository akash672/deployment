
package com.wt.domain;

import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.OrderSide.SELL;
import static com.wt.domain.Purpose.PARTIALWITHDRAWAL;
import static com.wt.domain.UserAdviseState.AdditionalLumpSumToMF;
import static com.wt.domain.UserAdviseState.UserMFAdviseIssued;
import static com.wt.domain.UserAdviseState.UserMFOpenAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.UserMFOpenAdviseOrderPlacedWithDealer;
import static com.wt.domain.write.events.IssueType.MutualFund;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.DoublesUtil.isANonZeroValidDouble;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.DoublesUtil.roundToFourDigits;
import static com.wt.utils.DoublesUtil.validDouble;
import static com.wt.utils.NumberUtils.averagingWeighted;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Entity;

import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.wt.domain.write.events.AdviseOrderCreated;
import com.wt.domain.write.events.AdviseOrderReceivedWithBroker;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.ExitAdviseReceived;
import com.wt.domain.write.events.FlushIssueAdviseTrades;
import com.wt.domain.write.events.GenerateClientOrderId;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceCalculated;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceWithDate;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserFundAdditionalSumAdvice;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.CancellableStream;

import akka.NotUsed;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@Getter
@EqualsAndHashCode(callSuper=true, of="userAdviseCompositeKey")
@DynamoDBTable(tableName = "UserMFAdvise")
public class UserMFAdvise extends UserAdvise implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @Setter
  @DynamoDBIgnore
  protected UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey();
  @DynamoDBIgnore
  private String amcCode;
  @DynamoDBIgnore
  private String folioNumber;
  @Setter
  @DynamoDBAttribute
  protected long issueTime;
  @Setter
  @DynamoDBAttribute
  protected String description;
  @Setter
  @DynamoDBIgnore
  protected String clientCode;
  @Setter
  @DynamoDBIgnore
  protected BrokerName brokerName;
  @Setter
  @DynamoDBAttribute
  protected String token;
  @Setter
  @DynamoDBAttribute
  protected String symbol;
  @Setter
  @DynamoDBAttribute
  protected double issueAdviceAbsoluteAllocation;
  @Setter
  @DynamoDBAttribute
  protected double issueAdviseAllocationValue;
  @Setter
  @DynamoDBAttribute
  protected double closeAdviseAbsoluteAllocation;
  @Setter
  @DynamoDBAttribute
  protected double totalQuantityWithdrawn;
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  protected Set<Order> issueAdviseOrder = new TreeSet<Order>();
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  protected Set<Order> closeAdviseOrders = new TreeSet<Order>();
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  protected Set<Order> corporateEventOrders = new TreeSet<Order>();
  @Setter
  @DynamoDBAttribute
  protected PriceWithPaf averageEntryPrice;
  @Setter
  @DynamoDBAttribute
  protected PriceWithPaf averageExitPrice;
  @Setter
  @DynamoDBIgnore
  protected String basketOrderCompositeId;
  @Setter
  @DynamoDBAttribute
  protected double cumulativeDividendsReceived ;
  
  @DynamoDBRangeKey(attributeName = "adviseId")
  public String getAdviseId() {
    return userAdviseCompositeKey.getAdviseId();
  }

  public UserMFAdvise() {
    isA = "UserMFAdvise";
  }

  public void setAdviseId(String adviseId) {
    userAdviseCompositeKey.setAdviseId(adviseId);
  }

  @DynamoDBHashKey(attributeName = "usernameWithFundWithAdviser")
  public String getUsernameWithFundWithAdviser() {
    return userAdviseCompositeKey.getUsernameWithFundWithAdviser();
  }

  public void setUsernameWithFundWithAdviser(String userWithFundWithAdviser) {
    userAdviseCompositeKey.setUsernameWithFundWithAdviser(userWithFundWithAdviser);
  }
  
  public void update(UserAdviseEvent event) {
    if (event instanceof UserFundAdviceIssuedWithUserEmail) {
      UserFundAdviceIssuedWithUserEmail userFundAdviseIssued = (UserFundAdviceIssuedWithUserEmail) event;
      this.userAdviseCompositeKey = new UserAdviseCompositeKey( userFundAdviseIssued.getAdviseId(),
                                                                userFundAdviseIssued.getUsername(), 
                                                                userFundAdviseIssued.getFundName(),
                                                                userFundAdviseIssued.getAdviserUsername(), 
                                                                userFundAdviseIssued.getInvestingMode());
      this.issueTime = userFundAdviseIssued.getTimestamp();
      this.description = userFundAdviseIssued.getDescription();
      this.token = userFundAdviseIssued.getToken();
      this.symbol = userFundAdviseIssued.getSymbol();
      this.clientCode = userFundAdviseIssued.getClientCode();
      this.brokerName = userFundAdviseIssued.getBrokerName();
      this.issueAdviceAbsoluteAllocation = userFundAdviseIssued.getAbsoluteAllocation();
      this.issueAdviseAllocationValue = round(userFundAdviseIssued.getMonetaryAllocationValue(), 4);
      this.averageEntryPrice = new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.));
      this.averageExitPrice = new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.));
      if(userFundAdviseIssued.getBasketOrderCompositeId() != null)
        this.basketOrderCompositeId = userFundAdviseIssued.getBasketOrderCompositeId();
    } else if (event instanceof UserFundAdditionalSumAdvice) {

      UserFundAdditionalSumAdvice userFundAdviseIssued = (UserFundAdditionalSumAdvice) event;
      this.issueTime = userFundAdviseIssued.getTimestamp();
      this.issueAdviceAbsoluteAllocation = userFundAdviseIssued.getAbsoluteAllocation();
      this.issueAdviseAllocationValue = round(userFundAdviseIssued.getMonetaryAllocationValue());
      this.averageEntryPrice = new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.));
      this.averageExitPrice = new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.));
      if(userFundAdviseIssued.getBasketOrderId() != null)
        this.basketOrderCompositeId = userFundAdviseIssued.getBasketOrderId();
    } else if (event instanceof GenerateClientOrderId) {
      GenerateClientOrderId generateClientOrderIdEvent = (GenerateClientOrderId) event;
      amcCode = generateClientOrderIdEvent.getAmcCode();
      AdviseOrderCreated adviseOrderCreated = createAdviseOrder(generateClientOrderIdEvent.getAdviseId(),
          generateClientOrderIdEvent.getLocalOrderId(), issueTime, token, generateClientOrderIdEvent.getUsername(), 0, BUY,
          generateClientOrderIdEvent.getInvestingMode(), generateClientOrderIdEvent.getFundName());
      Order newAdviseOrder = new Order();
      newAdviseOrder.update(adviseOrderCreated);
      issueAdviseOrder.add(newAdviseOrder);
    }
     else if (event instanceof AdviseOrderReceivedWithBroker) {
      AdviseOrderReceivedWithBroker adviseOrderReceivedWithBroker = (AdviseOrderReceivedWithBroker) event;
      OrderReceivedWithBroker orderReceivedWithBroker = new OrderReceivedWithBroker(new DestinationAddress(null),
          adviseOrderReceivedWithBroker.getLocalOrderId(), adviseOrderReceivedWithBroker.getBrokerOrderId());
      if (!adviseOrderReceivedWithBroker.getUserAdviseState().equals(UserMFOpenAdviseOrderExecuted)) {
        if (isIssuedAdviceOrderReceivedWithBroker(adviseOrderReceivedWithBroker)) {
          Order issuedAdviseOrder = getLatestIssuedAdviseOrder().get();
          issuedAdviseOrder.update(orderReceivedWithBroker);
        }
      else {
          Order closeAdviseOrder = getLatestSellOrder().get();
          closeAdviseOrder.update(orderReceivedWithBroker);
        }
       
      }
    } else if (event instanceof AdviseOrderTradeExecuted) {
      AdviseOrderTradeExecuted adviseOrderTradeExecuted = (AdviseOrderTradeExecuted) event;
      folioNumber = adviseOrderTradeExecuted.getFolioNumber();
      TradeExecuted tradeExecuted = new TradeExecuted.TradeExecutedBuilder(new DestinationAddress(null),
          adviseOrderTradeExecuted.getExchangeOrderId(), adviseOrderTradeExecuted.getTradeId(),
          adviseOrderTradeExecuted.getOrderSide(), adviseOrderTradeExecuted.getPrice(),
          adviseOrderTradeExecuted.getQuantity()).withLastTradedTime(adviseOrderTradeExecuted.getTimestamp()).build();
      if (isIssuedAdviceOrderTradeExecuted(adviseOrderTradeExecuted)) {
        Order issuedAdviseOrder = getLatestIssuedAdviseOrder().get();
        issuedAdviseOrder.update(tradeExecuted);
      }
      else {
        Order closeAdviseOrder = getLatestSellOrder().get();
        closeAdviseOrder.update(tradeExecuted);
      }
      calculateAveragePrice(adviseOrderTradeExecuted);
    } else if (event instanceof ExitAdviseReceived) {
      ExitAdviseReceived exitAdviseReceived = (ExitAdviseReceived) event;
      if(exitAdviseReceived.getBasketOrderCompositeId() != null)
        this.basketOrderCompositeId = exitAdviseReceived.getBasketOrderCompositeId();
      closeAdviseAbsoluteAllocation = exitAdviseReceived.getAbsoluteAllocation();
      double numberOfShares = 0;
      if (!isZero(closeAdviseAbsoluteAllocation - 100.))
        numberOfShares =  (doubleQuantityYetToBeExited() * closeAdviseAbsoluteAllocation / 100.);
      else
        numberOfShares = doubleQuantityYetToBeExited();
      AdviseOrderCreated adviseOrderCreated = createAdviseOrder(exitAdviseReceived.getAdviseId(),
          exitAdviseReceived.getLocalOrderId(), exitAdviseReceived.getExitDate(), token, exitAdviseReceived.getUsername(),
          numberOfShares, SELL, exitAdviseReceived.getInvestingMode(), exitAdviseReceived.getFundName());
      Order closeAdviseOrder = new Order();
      closeAdviseOrder.update(adviseOrderCreated);
      closeAdviseOrders.add(closeAdviseOrder);
      if (exitAdviseReceived.getPurpose().equals(PARTIALWITHDRAWAL))
        totalQuantityWithdrawn += closeAdviseOrder.getQuantity();
    } else if (event instanceof CurrentUserAdviseFlushedDueToExit) {
      issueAdviseOrder.stream().forEach(issueAdviseOrder -> issueAdviseOrder.update(new FlushIssueAdviseTrades()));
      closeAdviseOrders.stream().forEach(closeAdviseOrder -> closeAdviseOrder.update(new FlushIssueAdviseTrades()));
      issueAdviseOrder = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      closeAdviseOrders = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      issueAdviceAbsoluteAllocation = 0.;
      issueAdviseAllocationValue = 0.;
      closeAdviseAbsoluteAllocation = 0.;
      averageEntryPrice = PriceWithPaf.noPrice("");
      averageExitPrice = PriceWithPaf.noPrice("");
    } 
  }

  @DynamoDBIgnore
  public Optional<Order> getLatestIssuedAdviseOrder() {
    Optional<Order> latestIssueAdviseOrder = Optional.of(((TreeSet<Order>) issueAdviseOrder).last());
    return latestIssueAdviseOrder;
  }

  @DynamoDBIgnore
  public Optional<Order> getLatestSellOrder() {
    Optional<Order> latestIssueAdviseOrder = Optional.of(((TreeSet<Order>) closeAdviseOrders).last());
    return latestIssueAdviseOrder;
  }

  private boolean isIssuedAdviceOrderReceivedWithBroker(AdviseOrderReceivedWithBroker adviseOrderReceivedWithBroker) {
    return (adviseOrderReceivedWithBroker.getUserAdviseState().equals(UserMFAdviseIssued) || 
        adviseOrderReceivedWithBroker.getUserAdviseState().equals(AdditionalLumpSumToMF)) ;
  }
  
  private boolean isIssuedAdviceOrderTradeExecuted(AdviseOrderTradeExecuted adviseOrderTradeExecuted) {
    return (adviseOrderTradeExecuted.getUserAdviseState().equals(UserMFOpenAdviseOrderPlacedWithDealer));
  }

  private AdviseOrderCreated createAdviseOrder(String adviseId, String localOrderId, long timestamp, String token,
      String emailId, double numberOfShares, OrderSide orderSide, InvestingMode investingMode, String fundName) {
    return new AdviseOrderCreated.AdviseOrderCreatedBuilder(getUsername(), localOrderId, timestamp, token, emailId,
        orderSide, investingMode, fundName)
        .withSymbol(symbol)
        .withNumberOfShares(numberOfShares)
        .build();
  }

  public double calculateNumberOfShares(double amountToInvest, double price) {
    double fractionalShares = amountToInvest/price;
    return (fractionalShares);
  }

  private void calculateAveragePrice(AdviseOrderTradeExecuted adviseOrderTradeExecuted) {
    long tradeTime = adviseOrderTradeExecuted.getTimestamp();
    if (isIssuedAdviceOrderTradeExecuted(adviseOrderTradeExecuted))
      averageEntryPrice = averageEntryPrice
          .withNewPriceTime(roundToFourDigits(getAveragePrice(adviseOrderTradeExecuted)), tradeTime);
    else
      averageExitPrice = averageExitPrice.withNewPriceTime(roundToFourDigits(getAveragePrice(adviseOrderTradeExecuted)),
          tradeTime);
  }

  private double getAveragePrice(AdviseOrderTradeExecuted tradeExecuted) {
    if (isIssuedAdviceOrderTradeExecuted(tradeExecuted))
      return issueAdviseOrder.stream().flatMap(o -> o.getTrades().stream())
          .collect(averagingWeighted(Trade::getPrice, Trade::getQuantity));
    else
      return closeAdviseOrders.stream().flatMap(o -> o.getTrades().stream())
          .collect(averagingWeighted(Trade::getPrice, Trade::getQuantity));
  }
  
  public double tradedEntryQuantity() {
    Stream<Trade> trades = issueAdviseOrder.stream().flatMap(o -> o.getTrades().stream());
    Stream<Double> quantities = trades.map(trade -> trade.getQuantity());
    Double sum = quantities.collect(Collectors.summingDouble(o -> o));
    return sum;
  }

  public double averageEntryPrice() {
    return getLatestIssuedAdviseOrder().get().calculateMFAverageTradedPrice();
  }

  public double currentCloseAdviseTradedQuantity(TradeExecuted tradeExecuted) {
    return currentCloseAdvise(tradeExecuted.getExchangeOrderId()).getTotalTradedQuantity();
  }

  public double currentCloseAdviseOrderQuantity(TradeExecuted tradeExecuted) {
    return currentCloseAdvise(tradeExecuted.getExchangeOrderId()).getQuantity();
  }
  
  public double currentCloseAdviseAverageExitPrice(TradeExecuted tradeExecuted) {
    return currentCloseAdvise(tradeExecuted.getExchangeOrderId()).calculateMFAverageTradedPrice();
  }
  
  private Order currentCloseAdvise(String orderId) {
    return closeAdviseOrders.stream().filter(order -> order.getBrokerOrderId().equals(orderId)).findFirst().get();
  }

  public CancellableStream<UserAdviseAbsolutePerformanceCalculated, NotUsed> realtimePerformanceUsing(
      CancellableStream<PriceWithPaf, NotUsed> subscription) {
    Source<PriceWithPaf, NotUsed> priceEvents = subscription.getSource();
    Source<UserAdviseAbsolutePerformanceCalculated, NotUsed> realtime = priceEvents
        .map(priceEvent -> performanceRealtime(priceEvent));

    return new AdviseStream<UserAdviseAbsolutePerformanceCalculated, NotUsed>(realtime, subscription.getActor(),
        subscription);
  }

  public List<UserAdviseAbsolutePerformanceWithDate> historicalPerformance(List<PriceWithPaf> listOfPriceEvent,
      Materializer materializer) throws InterruptedException, ExecutionException {

    Source<UserAdviseAbsolutePerformanceWithDate, NotUsed> map = Source.from(listOfPriceEvent)
        .map(priceEvent -> performanceForHistorical(priceEvent, priceEvent.getPrice().getTime()));
    List<UserAdviseAbsolutePerformanceWithDate> userAdvisePerformanceList = map.runWith(Sink.seq(), materializer).toCompletableFuture()
        .get();
    return userAdvisePerformanceList;
  }

  private double exitedQuantity() {
    Double sum = soldQuantityTillDate();
    return (sum == null) ? 0. : sum - totalQuantityWithdrawn;
  }

  public Double soldQuantityTillDate() {
    Stream<Trade> trades = closeAdviseOrders.stream().flatMap(o -> o.getTrades().stream());
    Stream<Double> quantities = trades.map(trade -> trade.getQuantity());
    Double sum = quantities.collect(Collectors.summingDouble(o -> o));
    return sum;
  }

  public UserAdviseAbsolutePerformanceCalculated performanceRealtime(PriceWithPaf price) {
    double realized = exitedQuantity()
        * (averageExitPrice.getPrice().getPrice() - averageEntryPrice.getPrice().getPrice());
    double unrealized = doubleQuantityYetToBeExited()
        * (price.getPrice().getPrice() - averageEntryPrice.getPrice().getPrice());
    double cashFromExit = exitedQuantity() * averageExitPrice.getPrice().getPrice();
    double currentValue = doubleQuantityYetToBeExited() * price.getPrice().getPrice();
    double pnl = realized + unrealized;
    return new UserAdviseAbsolutePerformanceCalculated(
        getUsername(),
        userAdviseCompositeKey.getUsername(),
        userAdviseCompositeKey.getAdviser(),
        userAdviseCompositeKey.getFund(),
        userAdviseCompositeKey.getInvestingMode(),
        userAdviseCompositeKey.getAdviseId(),
        token,
        symbol,
        validDouble(realized), 
        validDouble(unrealized),
        validDouble(pnl),
        validDouble(pctReturn(price, DateUtils.currentTimeInMillis())),
        currentValue,
        doubleQuantityYetToBeExited(),
        cashFromExit,
        getIssueType());
  }

  public UserAdviseAbsolutePerformanceWithDate performanceForHistorical(PriceWithPaf price, long date) {
    double realized = realizedTill(date);
    double unrealized = unrealizedTill(price, date);
    double currentValue = doubleQuantityYetToBeExited(date) * price.getPrice().getPrice();
    double cashFromExit = exitedMFValue(date);
    double pnl = realized + unrealized;
    return new UserAdviseAbsolutePerformanceWithDate(
        getUsername(),
        userAdviseCompositeKey.getUsername(),
        userAdviseCompositeKey.getAdviser(),
        userAdviseCompositeKey.getFund(),
        userAdviseCompositeKey.getInvestingMode(),
        userAdviseCompositeKey.getAdviseId(),
        token,
        symbol,
        validDouble(realized), 
        validDouble(unrealized),
        validDouble(pnl),
        validDouble(pctReturn(price, date)),
        currentValue,
        doubleQuantityYetToBeExited(),
        cashFromExit,
        date,
        getIssueType());
  }

  private double realizedTill(long date) {
    double averageEntryPrice = isANonZeroValidDouble(tradedEntryQuantity()) ?
         issueAdviseOrder.stream().map(trade -> trade.getPrice() * trade.getQuantity())
            .collect(Collectors.summingDouble(o -> o)) / tradedEntryQuantity() : 0D;
    List<Trade> closeAdviceTrades = closeAdviseOrders.stream().filter(order -> order.getOrderTimestamp() <= date)
        .flatMap(o -> o.getTrades().stream()).collect(Collectors.toList());
    Double totalClosedAdviceQuantity = closeAdviceTrades.stream().map(trade -> trade.getQuantity())
        .collect(Collectors.summingDouble(o -> o));
    Double totalExitValue = closeAdviceTrades.stream().map(trade -> trade.getQuantity() * trade.getPrice())
        .collect(Collectors.summingDouble(o -> o));
    double averageExitPrice = (totalExitValue != 0 && totalClosedAdviceQuantity != 0.0)
        ? totalExitValue / totalClosedAdviceQuantity : 0.0;
    double totalAdvicePerformance = totalClosedAdviceQuantity * (averageExitPrice - averageEntryPrice);
    return totalAdvicePerformance;
  }

  private double unrealizedTill(PriceWithPaf price, long date) {
    Double totalTradeValue = validDouble(issueAdviseOrder.stream().map(trade -> trade.getPrice() * trade.getQuantity())
        .collect(Collectors.summingDouble(o -> o)));
    double averageEntryPrice = isANonZeroValidDouble(tradedEntryQuantity())
        ? totalTradeValue / tradedEntryQuantity() : 0.0;
    double totalUnrealizedPnl = doubleQuantityYetToBeExited(date) * (price.getPrice().getPrice() - averageEntryPrice);
    return totalUnrealizedPnl;
  }

  @Override
  public int integerQuantityYetToBeExited() {
    return 0;
  }

  @Override
  public double doubleQuantityYetToBeExited() {
    return tradedEntryQuantity() - exitedQuantity() - totalQuantityWithdrawn;
  }
  
  private double doubleQuantityYetToBeExited(long date) {
    return tradedEntryQuantity() - exitedQuantityTill(date);
    
  }

  private double exitedQuantityTill(long date) {
    Stream<Trade> trades = closeAdviseOrders.stream().filter(order -> order.getOrderTimestamp() <= date)
        .flatMap(o -> o.getTrades().stream());
    Stream<Double> quantities = trades.map(trade -> trade.getQuantity());
    Double sum = quantities.collect(Collectors.summingDouble(o -> o));
    return (sum == null) ? 0 : sum;
  }

  private double pctReturn(PriceWithPaf price, long date) {
    double remainingQuantity = doubleQuantityYetToBeExited(date);
    double exitedValue =  closeAdviseOrders.stream().map(order -> order.getTotalTradedQuantity() * order.getAverageTradedPrice())
        .collect(Collectors.summingDouble(o -> o)) ; 
    double remainingValue = remainingQuantity * price.getPrice().getPrice();
    double entryValue = averageEntryPrice.getPrice().getPrice() * tradedEntryQuantity();
    double pctReturn = (entryValue != 0.) ? (100 * (exitedValue + remainingValue - entryValue) / entryValue) : 0.;
    return pctReturn;
  }
  
  @DynamoDBIgnore
  public IssueType getIssueType() {
    return MutualFund;
  }

  @Override
  @DynamoDBIgnore
  public double getCumulativePaf() {
    return 1.;
  }

  @Override
  public void setCumulativePaf(double cumulativePaf) {
    
  }

  @Override
  @DynamoDBIgnore
  public Map<Long, Double> getDateToCumulativePafMap() {
    return new HashMap<Long, Double>();
  }

  @Override
  public void setDateToCumulativePafMap(Map<Long, Double> dateToCumulativePafMap) {
    
  }

  @Override
  @DynamoDBIgnore
  public Map<String, Double> getCorporateEventDetailstoPaf() {
    return new HashMap<String, Double>();
  }

  @Override
  public void setCorporateEventDetailstoPaf(Map<String, Double> corporateEventDetailstoPaf) {
    
  }

  @Override
  @DynamoDBIgnore
  public String getUsername() {
    return userAdviseCompositeKey.getUsername();
  }

  @Override
  @DynamoDBIgnore
  @Deprecated
  public void setUsername(String username) {
    userAdviseCompositeKey
        .setUsernameWithFundWithAdviser((userAdviseCompositeKey.getUsernameWithFundWithAdviser()).split(CompositeKeySeparator)[0]);
  }

  @Override
  public double getCumulativeDividendsReceived() {
    return 0;
  }

  @Override
  @DynamoDBIgnore
  public Optional<Order> getLatestCorpActionOrder() {
    return null;
  }

}