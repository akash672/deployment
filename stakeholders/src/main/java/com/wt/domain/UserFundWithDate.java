package com.wt.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
public class UserFundWithDate {
  
  private UserFund userFund;
  private long date;
  
  public UserFundWithDate(UserFund userFund, long date) {
    this.userFund = userFund;
    this.date = date;
  }
  
  public long getDate() {
    return this.date;
  }
  
  public UserFund getUserFund() {
    return this.userFund;
  }

}
