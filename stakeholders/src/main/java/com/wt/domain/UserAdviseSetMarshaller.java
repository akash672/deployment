package com.wt.domain;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class UserAdviseSetMarshaller implements DynamoDBTypeConverter<String, Set<UserAdvise>> {
  @Override
  public String convert(Set<UserAdvise> obj) {
    GsonBuilder builder = new GsonBuilder();
    builder.registerTypeAdapter(UserAdvise.class, new UserAdviseAdapter());

    Gson gsonExt = builder.create();

    if (obj instanceof UserAdvise) {
      UserAdvise userAdvise = (UserAdvise) obj;
      return gsonExt.toJson(userAdvise);
    } else if (obj instanceof Collection<?>) {
      Set<UserAdvise> setOfUserAdvises = (Set<UserAdvise>) obj;
      return gsonExt.toJson(setOfUserAdvises);
    }
    return gsonExt.toJson(obj);
  }

  @Override
  public Set<UserAdvise> unconvert(String object) {
    GsonBuilder builder = new GsonBuilder();
    builder.registerTypeAdapter(UserAdvise.class, new UserAdviseAdapter());
    Gson gsonExt = builder.create();

    Set<UserAdvise> fromDb;
    try {

      fromDb = gsonExt.fromJson(object, new TypeToken<Set<UserAdvise>>() {

        private static final long serialVersionUID = 1L;
      }.getType());
      fromDb = new CopyOnWriteArraySet<>(fromDb);
    } catch (JsonSyntaxException e) {
      fromDb = gsonExt.fromJson(object, new TypeToken<UserAdvise>() {

        private static final long serialVersionUID = 1L;
      }.getType());
    }
    return fromDb;
  }
}
