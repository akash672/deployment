package com.wt.domain;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UserAdviseAdapter implements JsonSerializer<UserAdvise>, JsonDeserializer<UserAdvise> {

  private static Map<String, Class<? extends UserAdvise>> map = new TreeMap<String, Class<? extends UserAdvise>>();

  static {
    map.put("UserAdvise", UserAdvise.class);
    map.put("UserEquityAdvise", UserEquityAdvise.class);
    map.put("UserMFAdvise", UserMFAdvise.class);
  }

  public UserAdvise deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    String type = json.getAsJsonObject().get("isA").getAsString();
    Class<? extends UserAdvise> c = map.get(type);
    if (c == null)
      throw new RuntimeException("Unknown class: " + type);
    return context.deserialize(json, c);
  }

  @Override
  public JsonElement serialize(UserAdvise src, Type typeOfSrc, JsonSerializationContext context) {
    Class<? extends UserAdvise> c = map.get(src.isA);
    if (c == null)
      throw new RuntimeException("Unknown class: " + src.isA);
    return context.serialize(src, c);
  }
}
