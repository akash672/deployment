package com.wt.domain;

import lombok.Getter;

public enum AdviseTransactionType {
  Buy("Buy"), Sell("Sell");

  @Getter
  private final String adviseTransactionType;

  private AdviseTransactionType(String adviseTransactionType) {
    this.adviseTransactionType = adviseTransactionType;
  }
}
