package com.wt.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
@EqualsAndHashCode
public class DeliveryIdWithSecurityType {
  private long deliveryId;
  private String securityType;
  
}
