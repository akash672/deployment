package com.wt.domain;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class CumulativePafMapMarshaller implements DynamoDBTypeConverter<String, Map<Long, Double>> {
  @Override
  public String convert(Map<Long, Double> object) {
    return new Gson().toJson(object);
  }

  @Override
  public Map<Long, Double> unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Map<Long, Double>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }

}
