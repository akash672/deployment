package com.wt.domain;

import static com.wt.utils.DateUtils.prettyDate;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UserResponseParametersSerializer implements JsonSerializer<UserResponseParameters> {

  @Override
  public JsonElement serialize(UserResponseParameters userResponseParameters, Type typeOfSrc,
      JsonSerializationContext context) {
    JsonObject userResponseParametersJson = new JsonObject();

    userResponseParametersJson.addProperty("adviseType", userResponseParameters.getAdviseType().getAdviseType());
    userResponseParametersJson.addProperty("username", userResponseParameters.getUsername());
    userResponseParametersJson.addProperty("fundName", userResponseParameters.getFundName());
    userResponseParametersJson.addProperty("adviserName", userResponseParameters.getAdviserName());
    userResponseParametersJson.addProperty("clientCode", userResponseParameters.getClientCode());
    userResponseParametersJson.addProperty("brokerName", userResponseParameters.getBrokerName());
    userResponseParametersJson.addProperty("exchange", userResponseParameters.getExchange());
    userResponseParametersJson.addProperty("symbol", userResponseParameters.getSymbol());
    userResponseParametersJson.addProperty("absoluteAllocation", userResponseParameters.getAbsoluteAllocation());
    userResponseParametersJson.addProperty("allocationValue", userResponseParameters.getAllocationValue());
    userResponseParametersJson.addProperty("allocationValue", userResponseParameters.getAllocationValue());
    userResponseParametersJson.addProperty("numberOfShares", userResponseParameters.getNumberOfShares());
    userResponseParametersJson.addProperty("investmentDate", prettyDate(userResponseParameters.getInvestmentDate()));

    return userResponseParametersJson;
  }

}
