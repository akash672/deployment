package com.wt.domain;

import lombok.Getter;
import lombok.ToString;

@ToString(callSuper=true)
public class UserAdviseCompositeKeyWithDate extends UserAdviseCompositeKey {

  private static final long serialVersionUID = 1L;
  @Getter
  private final long date;

  public UserAdviseCompositeKeyWithDate(String adviseId, String username, String fundName, String adviserUsername,
      long date, InvestingMode investingMode) {
    super(adviseId, username, fundName, adviserUsername, investingMode);
    this.date = date;
  }

}
