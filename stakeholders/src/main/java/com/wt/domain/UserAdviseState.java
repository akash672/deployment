package com.wt.domain;

import akka.persistence.fsm.PersistentFSM.FSMState;
import lombok.Getter;

public enum UserAdviseState implements FSMState {
  UserAdviseIssued("UserAdviseIssued"),
  ExpectingAdvisePrice("ExpectingAdvisePrice"),
  AdviseTokenPriceReceived("AdviseTokenPriceReceived"),
  OpenAdviseOrderPlacedWithExchange("OpenAdviseOrderPlacedWithExchange"),
  OpenAdviseOrderRejected("OpenAdviseOrderRejected"),
  OpenAdviseOrderReceivedWithBroker("OpenAdviseOrderReceivedWithBroker"),
  OpenAdviseOrderExecuted("OpenAdviseOrderExecuted"),
  PartialExitReceived("PartialExitReceived"),
  PartialExitAdviseOrderPlacedWithExchange("PartialExitAdviseOrderPlacedWithExchange"),
  PartialExitAdviseOrderReceivedWithBroker("PartialExitAdviseOrderReceivedWithBroker"),
  PartialExitAdviseOrderReceivedWithDealer("PartialExitAdviseOrderReceivedWithDealer"),
  PartialExitAdviseOrderExecuted("PartialExitAdviseOrderExecuted"),
  CompleteExitReceived("CompleteExitReceived"),
  CompleteExitAdviseOrderPlacedWithExchange("CompleteExitAdviseOrderPlacedWithExchange"),
  CompleteExitAdviseOrderReceivedWithBroker("CompleteExitAdviseOrderReceivedWithBroker"),
  CompleteExitAdviseOrderExecuted("CompleteExitAdviseOrderExecuted"),
  UserMFAdviseIssued("UserMFAdviseIssued"),
  MFAdviseTokenNAVReceived("MFAdviseTokenPriceReceived"),
  UserMFOpenAdviseOrderPlacedWithDealer("UserMFOpenAdviseOrderPlacedWithDealer"),
  UserMFOpenAdviseOrderExecuted("UserMFOpenAdviseOrderExecuted"),
  UserMFCompleteCloseAdviseReceived("UserMFCompleteCloseAdviseReceived"),
  UserMFCompleteCloseAdviseOrderPlacedWithDealer("UserMFCompleteCloseAdviseOrderPlacedWithDealer"),
  UserMFCompleteCloseAdviseOrderExecuted("UserMFCompleteCloseAdviseOrderExecuted"),
  ExecutingCorporateActionSell("ExecutingCorporateActionSell"),
  CorporateActionOrderPlaced("CorporateActionOrderPlaced"),
  CorporateActionOrderPlacedWithBroker("CorporateActionOrderPlacedWithBroker"),
  CorporateActionOrderPlacedWithExchange("CorporateActionOrderPlacedWithExchange"),  
  UserMFPartialExitAdviseOrderExecuted("UserMFPartialExitAdviseOrderExecuted"),
  AdditionalLumpSumToMF("AdditionalLumpSumToMF");

  @Getter private final String stateIdentifier;
 
  private UserAdviseState(String stateIdentifier) {
    this.stateIdentifier = stateIdentifier;
  }
  
  public String identifier() {
    return stateIdentifier;
  }

}