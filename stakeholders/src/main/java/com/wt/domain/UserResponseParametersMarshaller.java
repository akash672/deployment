package com.wt.domain;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class UserResponseParametersMarshaller implements DynamoDBTypeConverter<String, List<UserResponseParameters>> {

  @Override
  public String convert(List<UserResponseParameters> object) {
    if (object instanceof UserResponseParameters) {
      UserResponseParameters userResponseParameters = (UserResponseParameters) object;
      return new Gson().toJson(userResponseParameters);
    } else if (object instanceof Collection<?>) {
      List<UserResponseParameters> setOfuserResponseParameterss = (List<UserResponseParameters>) object;
      return new Gson().toJson(setOfuserResponseParameterss);
    }
    return new Gson().toJson(object);
  }

  @Override
  public List<UserResponseParameters> unconvert(String object) {

    List<UserResponseParameters> fromDb;
    try {

      fromDb = new Gson().fromJson(object, new TypeToken<List<UserResponseParameters>>() {

        private static final long serialVersionUID = 1L;
      }.getType());
      fromDb = new CopyOnWriteArrayList<UserResponseParameters>(fromDb);
    } catch (JsonSyntaxException e) {
      fromDb = new Gson().fromJson(object, new TypeToken<UserResponseParameters>() {

        private static final long serialVersionUID = 1L;
      }.getType());
    }
    return fromDb;
  }

}
