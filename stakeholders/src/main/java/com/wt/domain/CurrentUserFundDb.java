package com.wt.domain;

import static com.wt.domain.AdviseType.Close;
import static com.wt.domain.AdviseType.Issue;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.ONBOARDING;
import static com.wt.domain.UserAcceptanceMode.Auto;
import static com.wt.domain.UserAcceptanceMode.Manual;
import static com.wt.domain.UserAdviseState.CompleteExitAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.ExecutingCorporateActionSell;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderRejected;
import static com.wt.domain.UserAdviseState.UserMFCompleteCloseAdviseOrderExecuted;
import static com.wt.domain.UserResponseParameters.withBlankResponse;
import static com.wt.domain.UserResponseType.Approve;
import static com.wt.domain.UserResponseType.Archive;
import static com.wt.domain.UserResponseType.Awaiting;
import static com.wt.domain.UserResponseType.ExitFund;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.domain.write.events.IssueType.MutualFund;
import static com.wt.utils.CommonConstants.HYPHEN;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.persistence.Entity;

import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import com.wt.domain.write.events.AdditionalSumAddedToUserFund;
import com.wt.domain.write.events.AdjustFundCashForInsufficientCapitalAdvise;
import com.wt.domain.write.events.AdvicesIssuedPreviously;
import com.wt.domain.write.events.CorporateActionSellDetails;
import com.wt.domain.write.events.CorporateMergerPrimaryBuyEvent;
import com.wt.domain.write.events.CorporateMergerSecondaryBuyEvent;
import com.wt.domain.write.events.CorporateMergerSellEvent;
import com.wt.domain.write.events.CurrentFundPortfolioValue;
import com.wt.domain.write.events.ExecutedAdviseDetailsUpdated;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.FundAdviceTradeExecutedForWithdrawal;
import com.wt.domain.write.events.FundLumpSumAdded;
import com.wt.domain.write.events.FundSubscribed;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.FundUserSubscribed;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.MergerSellTradeExecuted;
import com.wt.domain.write.events.SpecialCircumstancesBuyEvent;
import com.wt.domain.write.events.UpdatedWdIdsForAdvisesReceived;
import com.wt.domain.write.events.UpdatingCorporateActionsMap;
import com.wt.domain.write.events.UpdatingUserFundCash;
import com.wt.domain.write.events.UserAcceptanceModeUpdateReceived;
import com.wt.domain.write.events.UserAdviseDetails;
import com.wt.domain.write.events.UserAdviseDetailsMapMarshaller;
import com.wt.domain.write.events.UserFundAdviceClosed;
import com.wt.domain.write.events.UserFundAdviceIssued;
import com.wt.domain.write.events.UserFundAdviceRetryInitiated;
import com.wt.domain.write.events.UserFundDividendReceived;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundMaxDrawdownCalculated;
import com.wt.domain.write.events.UserFundWithdrawalDoneSuccessfully;
import com.wt.domain.write.events.UserOpenAdviseOrderRejected;
import com.wt.domain.write.events.UserResponseReceived;
import com.wt.domain.write.events.WithdrawSumFromUserFundAttempted;
import com.wt.utils.DateUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Entity
@DynamoDBTable(tableName = "UserFund")
@ToString
@NoArgsConstructor
@Log4j
public class CurrentUserFundDb implements Serializable {
  private static final long serialVersionUID = 1L;

  @Getter
  @Setter
  @Id
  @DynamoDBIgnore
  private UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey();

  @Getter
  @Setter
  @DynamoDBAttribute
  private boolean isAutoApprovedOn;

  @Getter
  @Setter
  @DynamoDBAttribute
  private boolean isSubscribed;

  @Getter
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConvertedEnum
  private RiskLevel riskProfile;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String inceptionDate;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double requestedInvestment;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double outstandingInvestment;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double totalInvestment;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double interimTotalInvestment;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double currentLumpSum;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double withdrawal;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double cashComponent;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double sipAmount;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String sipDate;
  @DynamoDBAttribute
  @Getter
  @Setter
  @DynamoDBTypeConverted(converter = DrawdownsConverter.class)
  private List<MaxDrawdown> drawDowns;
  @Getter
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = UserAdviseSetMarshaller.class)
  private CopyOnWriteArraySet<UserAdvise> activeAdvises = new CopyOnWriteArraySet<UserAdvise>();
  @Getter
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = UserAdviseSetMarshaller.class)
  private Set<UserAdvise> activeRejectedBuySideAdvises = new CopyOnWriteArraySet<UserAdvise>();
  @Getter
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = EmptySetHandler.class)
  private Set<String> allAdvisesInCurrentCycle = new CopyOnWriteArraySet<String>();
  @DynamoDBAttribute
  @Getter
  @Setter
  @DynamoDBTypeConverted(converter = UserResponseParametersMarshaller.class)
  private List<UserResponseParameters> pendingApprovalRequests = new CopyOnWriteArrayList<UserResponseParameters>();
  @Getter
  @Setter
  @DynamoDBAttribute
  private double previousCashComponent;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double withdrawalAmountRequested;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double actualWithdrawalAmount;
  @Getter
  @Setter
  @DynamoDBAttribute
  private double currentFundValue;
  @Getter
  @Setter
  @DynamoDBTypeConverted(converter = UserAdviseDetailsMapMarshaller.class)
  @DynamoDBAttribute
  private Map<String, UserAdviseDetails> corpEventTokensToAdviseDetails = new HashMap<String, UserAdviseDetails>();
  @Getter
  @Setter
  @DynamoDBAttribute
  private double hypotheticalFundCashWhenApprovalsArePending;
  @Getter
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = StringToDoubleMapMarshaller.class)
  private Map<String, Double> closeAdviseIdsWithPricesPendingApproval = new HashMap<String, Double>();
  @Getter
  @Setter
  @DynamoDBAttribute
  private double fundDividendsReceived;
  
  public void update(UserFundEvent userFundEvent) {
    if (userFundEvent instanceof FundUserSubscribed) {
      FundUserSubscribed event = (FundUserSubscribed) userFundEvent;
      this.userFundCompositeKey = new UserFundCompositeKey(event.getUsername(), event.getFundName(),
          event.getAdviserUsername(), event.getInvestingMode());
      this.riskProfile = event.getRiskProfile();
      this.inceptionDate = event.getInceptionDate();
      this.currentLumpSum = 0;
      this.previousCashComponent = 0;
      this.totalInvestment = 0L;
      this.interimTotalInvestment = 0L;
      this.requestedInvestment = event.getLumpsumAmount();
      this.outstandingInvestment = 0L;
      this.sipAmount = event.getSipAmount();
      this.cashComponent = event.getLumpsumAmount();
      if (event.getInvestingPurpose() == ONBOARDING)
        this.cashComponent = this.sipAmount;
      this.sipDate = event.getSipDate();
      this.isSubscribed = false;
      this.pendingApprovalRequests =  new CopyOnWriteArrayList<UserResponseParameters>();
      this.isAutoApprovedOn = false;
    } else if (userFundEvent instanceof FundSubscribed) {
      FundSubscribed fundSubscribed = (FundSubscribed) userFundEvent;
      this.isSubscribed = true;
      this.totalInvestment += fundSubscribed.getTotalInvestment();
      this.interimTotalInvestment = 0.;
      this.outstandingInvestment = 0.;
    } else if (userFundEvent instanceof FundLumpSumAdded) {
      FundLumpSumAdded fundLumpSumAdded = (FundLumpSumAdded) userFundEvent;
      cashComponent = round(cashComponent + previousCashComponent);
      previousCashComponent = 0;
      this.totalInvestment += fundLumpSumAdded.getLumpSumAdded();
      this.interimTotalInvestment = 0.;
      this.outstandingInvestment = 0.;
    } else if (userFundEvent instanceof AdvicesIssuedPreviously) {
      AdvicesIssuedPreviously previouslyIssuedAdvices = (AdvicesIssuedPreviously) userFundEvent;
      double userFundCashInPercentage = 100.;
      
      for (FundConstituent fundConstituent : previouslyIssuedAdvices.getFundConstituents()) {
        UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(fundConstituent.getAdviseId(),
            previouslyIssuedAdvices.getUsername(), previouslyIssuedAdvices.getFundName(),
            previouslyIssuedAdvices.getAdviserUsername(), previouslyIssuedAdvices.getInvestingMode());
        double absoluteAllocation = 0.;
        if (!isZero(currentLumpSum) && !isZero(cashComponent)) 
          absoluteAllocation = fundConstituent.getAllocation() / (cashComponent / currentLumpSum);
        else if (!isZero(requestedInvestment) && !isZero(cashComponent))
          absoluteAllocation = isNotAClosedAdvise(userFundCashInPercentage)
              ? (fundConstituent.getAllocation() / userFundCashInPercentage) * 100. : 0;
        if (previouslyIssuedAdvices.getInvestingpurpose().equals(ONBOARDING)) {
          absoluteAllocation = (!isZero(requestedInvestment) && !isZero(cashComponent))
              ? fundConstituent.getAllocation() / (cashComponent / requestedInvestment) : 0.;
        }
        UserAdvise updatedUserAdvise = updateUserAdviseDuringReplication(fundConstituent, userAdviseCompositeKey, absoluteAllocation,
            previouslyIssuedAdvices);
        activeAdvises.add(updatedUserAdvise);
        if (previouslyIssuedAdvices.getInvestingpurpose().equals(EQUITYSUBSCRIPTION))
          cashComponent -= computeMonetaryAllocation(absoluteAllocation);
        userFundCashInPercentage -= fundConstituent.getAllocation();
      }
      outstandingInvestment = requestedInvestment - cashComponent; 
    } else if (userFundEvent instanceof AdjustFundCashForInsufficientCapitalAdvise) {
      AdjustFundCashForInsufficientCapitalAdvise adjustFundCashForInsufficientCapitalAdvise =
          (AdjustFundCashForInsufficientCapitalAdvise) userFundEvent;
      updateCashComponentWithTheResidualCash(adjustFundCashForInsufficientCapitalAdvise.getAdjustAllocationValue(), 0.);
      addAdviseToActiveRejectedMap(adjustFundCashForInsufficientCapitalAdvise.getAdviseId(), adjustFundCashForInsufficientCapitalAdvise.getTimestamp());
    } else if (userFundEvent instanceof UserOpenAdviseOrderRejected) {
      UserOpenAdviseOrderRejected userFundAdviseToBeRemoved = (UserOpenAdviseOrderRejected) userFundEvent;
      updateCashComponentWithTheResidualCash(userFundAdviseToBeRemoved.getAllocationValue(), 0.);
      addAdviseToActiveRejectedMap(userFundAdviseToBeRemoved.getAdviseId(), userFundAdviseToBeRemoved.getTimestamp());
    } else if (userFundEvent instanceof UserFundAdviceIssued) {
      UserFundAdviceIssued fundAdviceIssued = (UserFundAdviceIssued) userFundEvent;
      IssueType issueType = getIssueType(fundAdviceIssued.getToken());
      UserResponseParameters oldPendingResponse = getPreviousUnapprovedResponse(fundAdviceIssued.getAdviseId());
      UserAdvise updatedUserAdvise;
      currentFundValue = fundAdviceIssued.getPortfolioValue();
      if(oldPendingResponse.getAdviseType().equals(AdviseType.Issue)) {
        updatedUserAdvise = updateUserAdvise(fundAdviceIssued, issueType.userAdvise(),
            !oldPendingResponse.getAdviseId().isEmpty());
      } else {
        updatedUserAdvise = updateIssueAdviseWithPendingCloseAdvise(fundAdviceIssued, oldPendingResponse);
      }
      activeAdvises.removeIf(userAdvise -> userAdvise.getAdviseId().equals(fundAdviceIssued.getAdviseId()));
      activeAdvises.add(updatedUserAdvise);
      if (isAutoApprovedOn) 
        cashComponent -= computeMonetaryAllocation(updatedUserAdvise.getIssueAdviceAbsoluteAllocation());
    } else if(userFundEvent instanceof UserFundAdviceClosed){
      UserFundAdviceClosed adviceClosed = (UserFundAdviceClosed) userFundEvent;
      if(!isAutoApprovedOn && adviceClosed.getExitPrice() != null)
        closeAdviseIdsWithPricesPendingApproval.put(adviceClosed.getAdviseId(), adviceClosed.getExitPrice().getPrice().getPrice());
      else if(!isAutoApprovedOn && adviceClosed.getExitPrice() == null) {
        double entryPrice = getOldUserAdviseEntryPrice(adviceClosed.getAdviseId()); 
        closeAdviseIdsWithPricesPendingApproval.put(adviceClosed.getAdviseId(), entryPrice);
      }
      removeAdviseFromActiveRejectedMap(adviceClosed.getAdviseId());
    } else if (userFundEvent instanceof UserResponseReceived) {
      UserResponseReceived userResponseReceived = (UserResponseReceived) userFundEvent;
      UserResponseReceived updatedUserResponseReceived = userResponseReceived;
      if (userResponseReceived.getUserResponseType().equals(Awaiting)) {
        updatedUserResponseReceived = updateWithCurrentCloseApproval(userResponseReceived);
        if(!isZero(updatedUserResponseReceived.getAbsoluteAllocation()))
          pendingApprovalRequests.add(new UserResponseParameters(updatedUserResponseReceived.getAdviseId(),
              updatedUserResponseReceived.getFundName(), updatedUserResponseReceived.getClientCode(),
              updatedUserResponseReceived.getAdviseType(), updatedUserResponseReceived.getUsername(),
              updatedUserResponseReceived.getBrokerName(), updatedUserResponseReceived.getExchange(),
              updatedUserResponseReceived.getAdviserUsername(), updatedUserResponseReceived.getToken(),
              updatedUserResponseReceived.getSymbol(), updatedUserResponseReceived.getInvestingMode(),
              updatedUserResponseReceived.getUserResponseType(), updatedUserResponseReceived.getAbsoluteAllocation(),
              updatedUserResponseReceived.getAllocationValue(), updatedUserResponseReceived.getNumberOfShares(),
              updatedUserResponseReceived.getInvestmentDate()));
      }
      
      if (isPendingApprovalOnIssueAdvise(updatedUserResponseReceived)) {
        if (isZero(hypotheticalFundCashWhenApprovalsArePending)) {
          cashComponent -= computeMonetaryAllocation(updatedUserResponseReceived.getAbsoluteAllocation());
          hypotheticalFundCashWhenApprovalsArePending = cashComponent;
        } else {
          cashComponent -= computeNewAdviceWithPendingApprovalsMonetaryAllocation(
              updatedUserResponseReceived.getAbsoluteAllocation());
          hypotheticalFundCashWhenApprovalsArePending -= computeNewAdviceWithPendingApprovalsMonetaryAllocation(
              updatedUserResponseReceived.getAbsoluteAllocation());
        }
      }
      
      if (isPendingApprovalOnCloseAdvise(updatedUserResponseReceived)
          && closeAdviseIdsWithPricesPendingApproval.containsKey(updatedUserResponseReceived.getAdviseId())) {
        if (isZero(hypotheticalFundCashWhenApprovalsArePending)) {
          hypotheticalFundCashWhenApprovalsArePending = cashComponent + updatedUserResponseReceived.getNumberOfShares()
              * closeAdviseIdsWithPricesPendingApproval.get(updatedUserResponseReceived.getAdviseId());
        } else {
          hypotheticalFundCashWhenApprovalsArePending += updatedUserResponseReceived.getNumberOfShares()
              * closeAdviseIdsWithPricesPendingApproval.get(updatedUserResponseReceived.getAdviseId());
        }
      }
      
      if (isUserApprovesOrDeclines(updatedUserResponseReceived)) {
        pendingApprovalRequests
            .removeIf(userAdvise -> userResponseReceived.getAdviseId().equals(userAdvise.getAdviseId()));
        hypotheticalFundCashWhenApprovalsArePending=0;
        closeAdviseIdsWithPricesPendingApproval.clear();
      }

      if (isUserDoesNotApprove(updatedUserResponseReceived.getUserResponseType())
          && updatedUserResponseReceived.getAdviseType().equals(Issue)) {
        updateCashComponentWithTheResidualCash(updatedUserResponseReceived.getAllocationValue(), 0.);
        removeAdviseFromActiveAdviseList(updatedUserResponseReceived.getAdviseId());
      }
      if (pendingApprovalRequests.isEmpty())
        hypotheticalFundCashWhenApprovalsArePending = 0;
    } else if (userFundEvent instanceof FundAdviceTradeExecuted) {
      FundAdviceTradeExecuted tradesExecuted = getUpdatedTradeExecuted((FundAdviceTradeExecuted) userFundEvent);
      if (tradesExecuted.getUserAdviseState() != ExecutingCorporateActionSell) {
        updateCashComponentWithTheResidualCash(tradesExecuted.getAllocationValue(), tradesExecuted.getTradedValue());
        removeAdviseFromActiveRejectedMap(tradesExecuted.getAdviseId());
        allAdvisesInCurrentCycle.add(new UserAdviseCompositeKey(tradesExecuted.getAdviseId(), tradesExecuted.getUsername(), 
            tradesExecuted.getFundName(), tradesExecuted.getAdviserUsername(), tradesExecuted.getInvestingMode()).persistenceId());
      }
      if (hasCompletelyExited(tradesExecuted)) {
        removeAdviseFromActiveAdviseList(tradesExecuted.getAdviseId());
      }
    } else if (userFundEvent instanceof UserFundAdviceRetryInitiated) {
       if(activeRejectedBuySideAdvises != null){
           double totalAllocationValue = activeRejectedBuySideAdvises.stream().mapToDouble(advise -> advise.getIssueAdviseAllocationValue()).sum();
           cashComponent = round(cashComponent - totalAllocationValue);
         }
    } else if (userFundEvent instanceof MergerSellTradeExecuted) {
      MergerSellTradeExecuted tradesExecuted = (MergerSellTradeExecuted) userFundEvent;
      removeAdviseFromActiveAdviseList(tradesExecuted.getAdviseId());
    } else if (userFundEvent instanceof ExecutedAdviseDetailsUpdated) {
      ExecutedAdviseDetailsUpdated adviseTradeExecuted = (ExecutedAdviseDetailsUpdated) userFundEvent;
      allAdvisesInCurrentCycle.add(new UserAdviseCompositeKey(adviseTradeExecuted.getAdviseId(), adviseTradeExecuted.getUsername(), 
          adviseTradeExecuted.getFundName(), adviseTradeExecuted.getAdviserUsername(), adviseTradeExecuted.getInvestingMode()).persistenceId());
      interimTotalInvestment += adviseTradeExecuted.getTradedValue();
    } else if (userFundEvent instanceof FundSuccessfullyUnsubscribed) {
      this.isSubscribed = false;
      this.cashComponent = 0.;
      this.requestedInvestment = 0.;
      this.outstandingInvestment = 0.;
      this.totalInvestment = 0.;
      this.withdrawal = 0;
      this.activeAdvises = new CopyOnWriteArraySet<UserAdvise>();
      this.activeRejectedBuySideAdvises = new CopyOnWriteArraySet<UserAdvise>();
      this.pendingApprovalRequests =  new CopyOnWriteArrayList<UserResponseParameters>();
      this.fundDividendsReceived = 0.0;
      this.allAdvisesInCurrentCycle = new CopyOnWriteArraySet<String>();
    }  else if (userFundEvent instanceof UserFundMaxDrawdownCalculated) {
      this.setDrawDowns(((UserFundMaxDrawdownCalculated) userFundEvent).getDrawDowns());
    } else if (userFundEvent instanceof AdditionalSumAddedToUserFund) {
      AdditionalSumAddedToUserFund lumpSumAddedToUserFund = (AdditionalSumAddedToUserFund) userFundEvent;
      this.previousCashComponent = cashComponent;
      this.currentLumpSum = lumpSumAddedToUserFund.getLumpsumAmount();
      this.requestedInvestment = currentLumpSum;
      this.cashComponent = currentLumpSum;
      this.activeRejectedBuySideAdvises = new CopyOnWriteArraySet<UserAdvise>();
    } else if (userFundEvent instanceof CorporateActionSellDetails) {
      CorporateActionSellDetails sellEvent = (CorporateActionSellDetails) userFundEvent;
      UserAdviseDetails userAdviseDetails;
      if(!corpEventTokensToAdviseDetails.containsKey(sellEvent.getToken())){
        userAdviseDetails = new UserAdviseDetails(sellEvent.getAvgPrice(), sellEvent.getQuantity(),
            sellEvent.getAbsoluteAllocation(), sellEvent.getAllocationValue(), 1.0, 0.0, 0.0,0.0);
        corpEventTokensToAdviseDetails.put(sellEvent.getToken(), userAdviseDetails);
      }else{
        userAdviseDetails = corpEventTokensToAdviseDetails.get(sellEvent.getToken());
        userAdviseDetails.updateEntryPrice(sellEvent.getQuantity(), sellEvent.getAvgPrice());
        userAdviseDetails.updateQuantity(sellEvent.getQuantity());
        userAdviseDetails.updateAdviseAllocation(sellEvent.getAbsoluteAllocation());
        corpEventTokensToAdviseDetails.put(sellEvent.getToken(), userAdviseDetails);
      }
    } else if (userFundEvent instanceof CorporateMergerPrimaryBuyEvent) {
      CorporateMergerPrimaryBuyEvent buyEvent = (CorporateMergerPrimaryBuyEvent) userFundEvent;
      UserAdviseDetails adviseDetails = corpEventTokensToAdviseDetails.get(buyEvent.getToken());
      double paf = buyEvent.getOldPrice() / buyEvent.getNewPrice();
      int newQuantity = (int) buyEvent.getSharesRatio() * adviseDetails.getQuantity();
      double newAverageEntryPrice = adviseDetails.getOldAdviseEntryPrice() / paf;
      double absolueAllocation = adviseDetails.getOldAdviseAllocation() / paf;
      double allocationValue = newQuantity * newAverageEntryPrice;
      adviseDetails.setPrimaryAdviseAvgPrice(round(newAverageEntryPrice));
      adviseDetails.setPaf(round(paf));
      adviseDetails.setPrimaryAdviseAllocation(round(absolueAllocation));
      UserFundAdviceIssued fundAdviceIssued = new UserFundAdviceIssued(buyEvent.getUsername(),
          buyEvent.getAdviserUsername(), buyEvent.getFundName(), buyEvent.getInvestingMode(), buyEvent.getAdviseId(),
          buyEvent.getToken(), allocationValue, absolueAllocation, DateUtils.currentTimeInMillis(), new PriceWithPaf(),
          DateUtils.currentTimeInMillis(), new PriceWithPaf(), DateUtils.currentTimeInMillis());
      fundAdviceIssued.setSymbol(buyEvent.getToken());
      UserAdvise updatedUserAdvise = updateUserAdvise(fundAdviceIssued, new UserEquityAdvise(), false);
      activeAdvises.add(updatedUserAdvise);

    } else if (userFundEvent instanceof CorporateMergerSecondaryBuyEvent) {
      CorporateMergerSecondaryBuyEvent buyEvent = (CorporateMergerSecondaryBuyEvent) userFundEvent;
      UserAdviseDetails adviseDetails = corpEventTokensToAdviseDetails.get(buyEvent.getOldToken());
      double totalSecondaryCashUsed = adviseDetails.getSecondaryCashRatioUtilized() + buyEvent.getCashAllocationRatio();
      adviseDetails.setSecondaryCashRatioUtilized(totalSecondaryCashUsed);
      int newQuantity = (int) buyEvent.getSharesRatio() * adviseDetails.getQuantity();
      double newAverageEntryPrice = (adviseDetails.getOldAdviseEntryPrice() - adviseDetails.getPrimaryAdviseAvgPrice())
          * buyEvent.getCashAllocationRatio() / buyEvent.getSharesRatio();
      double absolueAllocation = (adviseDetails.getOldAdviseAllocation() - adviseDetails.getPrimaryAdviseAllocation())
          * buyEvent.getCashAllocationRatio();
      double allocationValue = newQuantity * newAverageEntryPrice;
      UserFundAdviceIssued fundAdviceIssued = new UserFundAdviceIssued(buyEvent.getUsername(),
          buyEvent.getAdviserUsername(), buyEvent.getFundName(), buyEvent.getInvestingMode(), buyEvent.getAdviseId(),
          buyEvent.getNewToken(), allocationValue, absolueAllocation, DateUtils.currentTimeInMillis(),
          new PriceWithPaf(), DateUtils.currentTimeInMillis(), new PriceWithPaf(), DateUtils.currentTimeInMillis());
      fundAdviceIssued.setSymbol(buyEvent.getNewToken());
      UserAdvise updatedUserAdvise = updateUserAdvise(fundAdviceIssued, new UserEquityAdvise(), false);
      activeAdvises.add(updatedUserAdvise);

    } else if (userFundEvent instanceof UpdatingCorporateActionsMap) {
      UpdatingCorporateActionsMap updatingCorporateActionsMap = (UpdatingCorporateActionsMap) userFundEvent;
      UserAdviseDetails adviseDetails = corpEventTokensToAdviseDetails.get(updatingCorporateActionsMap.getOldToken());
      if(adviseDetails.getSecondaryCashRatioUtilized()==1.0){
        corpEventTokensToAdviseDetails.remove(updatingCorporateActionsMap.getOldToken());
        log.info("Removed entry from corpEventTokensToAdviseDetails with key " + updatingCorporateActionsMap.getOldToken() + " for username: " + userFundCompositeKey.getUsername() + " and fund: " +userFundCompositeKey.getFundWithAdviserUsername());
      }
    } else if (userFundEvent instanceof UserAcceptanceModeUpdateReceived) {
      UserAcceptanceModeUpdateReceived userAcceptanceModeUpdateReceived = (UserAcceptanceModeUpdateReceived) userFundEvent;
      if (userAcceptanceModeUpdateReceived.getUserAcceptanceMode().equals(Auto))
        isAutoApprovedOn = true;
      else
        isAutoApprovedOn = false;
    } else if (userFundEvent instanceof SpecialCircumstancesBuyEvent) {
      SpecialCircumstancesBuyEvent specialEvenyBuy = (SpecialCircumstancesBuyEvent) userFundEvent;
      UserAdvise userAdvise = checkIfAdvisesContainsAdviseId(specialEvenyBuy.getAdviseId()) ?
          getUserAdviseWithAdviseId(specialEvenyBuy.getAdviseId()) : new UserEquityAdvise();
      UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(specialEvenyBuy.getAdviseId(),
          specialEvenyBuy.getUsername(), specialEvenyBuy.getFundName(), specialEvenyBuy.getAdviserUsername(),
          specialEvenyBuy.getInvestingMode());
      double absoluteAllocation = (specialEvenyBuy.getPrice() * specialEvenyBuy.getQuantity() * 100) / cashComponent;
      userAdvise.setUserAdviseCompositeKey(userAdviseCompositeKey);
      userAdvise.setIssueAdviceAbsoluteAllocation(absoluteAllocation);
      userAdvise.setIssueAdviseAllocationValue(computeMonetaryAllocation(absoluteAllocation));
      userAdvise.setToken(specialEvenyBuy.getToken());
      userAdvise.setClientCode(specialEvenyBuy.getClientCode());
      userAdvise.setBrokerName(specialEvenyBuy.getBrokerName());
      userAdvise.setSymbol(specialEvenyBuy.getSymbol());
      if(!checkIfAdvisesContainsAdviseId(specialEvenyBuy.getAdviseId()))
        activeAdvises.add(userAdvise);
      cashComponent -= computeMonetaryAllocation(absoluteAllocation);
      removeOldPendingOrders(specialEvenyBuy.getAdviseId(), true);
    } else if (userFundEvent instanceof CorporateMergerSellEvent) {
      CorporateMergerSellEvent corporateMergerSellEvent = (CorporateMergerSellEvent) userFundEvent;
      if (corporateMergerSellEvent.getAdviseId() != null)
        removeOldPendingOrders(corporateMergerSellEvent.getAdviseId(), true);
    }  else if (userFundEvent instanceof WithdrawSumFromUserFundAttempted) {
      WithdrawSumFromUserFundAttempted withdrawSumFromUserFund = (WithdrawSumFromUserFundAttempted) userFundEvent;
      this.withdrawalAmountRequested = withdrawSumFromUserFund.getWithdrawalAmount();
    }  else if (userFundEvent instanceof CurrentFundPortfolioValue) {
      CurrentFundPortfolioValue currentPortfolioValue = (CurrentFundPortfolioValue) userFundEvent;
      currentFundValue = currentPortfolioValue.getCurrentPortFolioValue();
      this.actualWithdrawalAmount += round((withdrawalAmountRequested / currentFundValue) * cashComponent);
    } else if (userFundEvent instanceof FundAdviceTradeExecutedForWithdrawal) {
      FundAdviceTradeExecutedForWithdrawal fundAdviceTradeExecutedForWithdrawal = (FundAdviceTradeExecutedForWithdrawal) userFundEvent;
      this.actualWithdrawalAmount += fundAdviceTradeExecutedForWithdrawal.getAllocationValue();
    }  else if (userFundEvent instanceof UserFundWithdrawalDoneSuccessfully) {
      if (isZero(actualWithdrawalAmount))
        cashComponent -= withdrawalAmountRequested;
      else 
        this.cashComponent = round(cashComponent - ((withdrawalAmountRequested / currentFundValue) * cashComponent));
      this.withdrawal += actualWithdrawalAmount;
      this.actualWithdrawalAmount = 0.;
      this.withdrawalAmountRequested = 0.;
    }  else if (userFundEvent instanceof UserFundDividendReceived) {
      UserFundDividendReceived userFundDividendReceived = (UserFundDividendReceived) userFundEvent;
      this.fundDividendsReceived += userFundDividendReceived.getDividendAmount();
    } else if (userFundEvent instanceof UpdatingUserFundCash) {
      UpdatingUserFundCash updatingUserFundCash = (UpdatingUserFundCash) userFundEvent;
      this.cashComponent += updatingUserFundCash.getCashToAddToUserFund();
    } else if(userFundEvent instanceof UpdatedWdIdsForAdvisesReceived){
      UpdatedWdIdsForAdvisesReceived updatedWdIdsForAdvisesReceived = (UpdatedWdIdsForAdvisesReceived) userFundEvent;
      for(UserAdvise advise : activeAdvises){
        String exchange = advise.getToken().split("-")[1];
        String symbolWithExchange = advise.getSymbol().concat(HYPHEN).concat(exchange);
        if(updatedWdIdsForAdvisesReceived.getSymbolsToUpdatedWdIds().containsKey(symbolWithExchange))
          advise.setToken(updatedWdIdsForAdvisesReceived.getSymbolsToUpdatedWdIds().get(symbolWithExchange));
        
      }
      for(UserResponseParameters responseParameter : pendingApprovalRequests){
        String symbol = responseParameter.getSymbol().concat(HYPHEN).concat(responseParameter.getExchange());
        if(updatedWdIdsForAdvisesReceived.getSymbolsToUpdatedWdIds().containsKey(symbol))
          responseParameter.setToken(updatedWdIdsForAdvisesReceived.getSymbolsToUpdatedWdIds().get(symbol));
        }
    }
    
  }

  protected FundAdviceTradeExecuted getUpdatedTradeExecuted(FundAdviceTradeExecuted tradesExecuted) {
    if (isZero(tradesExecuted.getTradedValue()))
      return tradesExecuted;
    else {
      UserAdvise userAdvise = null;
      userAdvise = activeAdvises.stream()
          .filter(advise -> advise.getAdviseId().equals(tradesExecuted.getAdviseId()))
          .findFirst()
          .get();
      return new FundAdviceTradeExecuted(tradesExecuted.getUsername(), tradesExecuted.getAdviserUsername(),
          tradesExecuted.getFundName(), tradesExecuted.getInvestingMode(), tradesExecuted.getAdviseId(),
          tradesExecuted.getUserAdviseState(), userAdvise.getIssueAdviseAllocationValue(),
          tradesExecuted.getTradedValue());
    }
  }
  
  private UserAdvise getUserAdviseWithAdviseId(String adviseId){
    UserAdvise userAdvise = activeAdvises.stream()
        .filter(advise -> advise.getAdviseId().equals(adviseId))
        .findFirst()
        .get();
    
    return userAdvise;
  }
  
  private boolean checkIfAdvisesContainsAdviseId(String adviseId){
    boolean containsAdviseId = activeAdvises.stream()
       .anyMatch(advise -> advise.getAdviseId().equals(adviseId));
    
    return containsAdviseId;
  }
  
  private boolean isUserApprovesOrDeclines(UserResponseReceived userResponseReceived) {
    return userResponseReceived.getUserResponseType().equals(Approve)
        || isUserDoesNotApprove(userResponseReceived.getUserResponseType());
  }

  @DynamoDBIgnore
  public static IssueType getIssueType(String ticker) {
    if (ticker.endsWith("-EQ"))
      return Equity;
    return MutualFund;
  }

  private boolean isPendingApprovalOnIssueAdvise(UserResponseReceived userResponseReceived) {
    return userResponseReceived.getAdviseType().equals(Issue)
        && userResponseReceived.getUserResponseType().equals(Awaiting)
        && userResponseReceived.getAllocationValue() > 0;
  }
  
  private boolean isPendingApprovalOnCloseAdvise(UserResponseReceived userResponseReceived) {
    return userResponseReceived.getAdviseType().equals(AdviseType.Close)
        && userResponseReceived.getUserResponseType().equals(Awaiting)
        && userResponseReceived.getAbsoluteAllocation() > 0;
  }


  private boolean isUserDoesNotApprove(UserResponseType userResponseType) {
    return userResponseType.equals(Archive) || userResponseType.equals(ExitFund);
  }

  protected UserAdvise updateUserAdviseDuringReplication(FundConstituent fundConstituent, UserAdviseCompositeKey userAdviseCompositeKey,
      double absoluteAllocation, AdvicesIssuedPreviously advicesIssuedPreviously) {
    IssueType issueType = getIssueType(fundConstituent.getTicker());
    UserAdvise userAdvise = issueType.userAdvise();
    userAdvise.setUserAdviseCompositeKey(userAdviseCompositeKey);
    userAdvise.setIssueAdviceAbsoluteAllocation(absoluteAllocation);
    userAdvise.setIssueAdviseAllocationValue(round(computeMonetaryAllocation(absoluteAllocation)));
    userAdvise.setToken(fundConstituent.getTicker());
    userAdvise.setSymbol(fundConstituent.getSymbol());
    userAdvise.setAverageEntryPrice(fundConstituent.getEntryPrice());
    if(advicesIssuedPreviously.getBrokerName() != null)
      userAdvise.setBrokerName(BrokerName.valueOf(advicesIssuedPreviously.getBrokerName()));
    if(advicesIssuedPreviously.getClientCode() != null)
      userAdvise.setClientCode(advicesIssuedPreviously.getClientCode());

    return userAdvise;
  }
  
  private void addAdviseToActiveRejectedMap(String adviseId, long timestamp) {
    UserAdvise userAdvise = getUserAdviseWithAdviseId(adviseId);
    if (activeRejectedBuySideAdvises == null)
        activeRejectedBuySideAdvises = new CopyOnWriteArraySet<UserAdvise>();

    activeRejectedBuySideAdvises.add(userAdvise);
  }

  private void removeAdviseFromActiveRejectedMap(String adviseId) {
    if (activeRejectedBuySideAdvises == null)
        activeRejectedBuySideAdvises = new CopyOnWriteArraySet<UserAdvise>();

    activeRejectedBuySideAdvises.removeIf(advise -> adviseId.equals(advise.getAdviseId()));
  }    

  private UserAdvise updateUserAdvise(UserFundAdviceIssued fundAdviceIssued, UserAdvise userAdvise,
      boolean hasPendingApprovalsInSameAdviseId) {
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(fundAdviceIssued.getAdviseId(),
        fundAdviceIssued.getUsername(), fundAdviceIssued.getFundName(), fundAdviceIssued.getAdviserUsername(),
        fundAdviceIssued.getInvestingMode());
    double absoluteAllocation = fundAdviceIssued.getAbsoluteAllocation();
    double aumAllocation = fundAdviceIssued.getAllocation();
    userAdvise.setUserAdviseCompositeKey(userAdviseCompositeKey);
    double cashComponentForAllocation = fundHasPendingApprovals() ? hypotheticalFundCashWhenApprovalsArePending
        : cashComponent;
    if (fundHasPendingApprovals() && isZero(hypotheticalFundCashWhenApprovalsArePending) && isZero(cashComponent)) {
      absoluteAllocation = 0.;
      log.error("Some error occurred with UserFundAdviseIssued event: " + fundAdviceIssued + ", with cashComponent: "
          + cashComponentForAllocation + ", with " + pendingApprovalRequests.size() + " pending advises");
    } else if (isZero(currentFundValue) && !isZero(cashComponentForAllocation)
        && (((cashComponentForAllocation * absoluteAllocation) / totalInvestment) > aumAllocation))
      absoluteAllocation = (totalInvestment / cashComponentForAllocation) * aumAllocation;
    else if (!isZero(currentFundValue) && !isZero(cashComponentForAllocation)
        && (((cashComponentForAllocation * absoluteAllocation) / currentFundValue) > aumAllocation))
      absoluteAllocation = (currentFundValue / cashComponentForAllocation) * aumAllocation;

    double oldApprovalAllocationValue = getOldApprovalAllocationValue(fundAdviceIssued.getAdviseId(),
        hasPendingApprovalsInSameAdviseId);
    double oldApprovalAbsoluteAllocation = getOldApprovalAbsoluteAllocation(fundAdviceIssued.getAdviseId(),
        hasPendingApprovalsInSameAdviseId);
    double updateCurrentAbsoluteAlocation = isZero(oldApprovalAbsoluteAllocation) ? absoluteAllocation
        : getUpdatedAbsoluteAllocation(oldApprovalAbsoluteAllocation, absoluteAllocation);
    userAdvise.setIssueAdviceAbsoluteAllocation(updateCurrentAbsoluteAlocation);

    if (isZero(hypotheticalFundCashWhenApprovalsArePending)) {
      userAdvise.setIssueAdviseAllocationValue(round(
          computeMonetaryAllocation(absoluteAllocation) + oldApprovalAllocationValue));
    } else {
      userAdvise.setIssueAdviseAllocationValue(round(
          computeNewAdviceWithPendingApprovalsMonetaryAllocation(absoluteAllocation) + oldApprovalAllocationValue));
    }
    this.cashComponent = round(cashComponent + oldApprovalAllocationValue);
    this.hypotheticalFundCashWhenApprovalsArePending = round(hypotheticalFundCashWhenApprovalsArePending + oldApprovalAllocationValue);
    removeOldPendingOrders(fundAdviceIssued.getAdviseId(), hasPendingApprovalsInSameAdviseId);
    userAdvise.setToken(fundAdviceIssued.getToken());
    userAdvise.setSymbol(fundAdviceIssued.getSymbol());
    userAdvise.setAverageEntryPrice(fundAdviceIssued.getEntryPrice());
    userAdvise.setClientCode(fundAdviceIssued.getClientCode());
    userAdvise.setBrokerName(fundAdviceIssued.getBrokerName());
    return userAdvise;
  }

  public double computeMonetaryAllocation(double absoluteAllocation) {
    return round((cashComponent * absoluteAllocation) / 100.0);
  }

  private UserAdvise updateIssueAdviseWithPendingCloseAdvise(UserFundAdviceIssued fundAdviceIssued,
      UserResponseParameters oldPendingResponse) {
    UserAdvise updateUserAdvise = activeAdvises.stream()
        .filter(advise -> advise.getAdviseId().equals(fundAdviceIssued.getAdviseId())).findFirst().get();
    double oldApprovalAbsoluteAllocation = oldPendingResponse.getAbsoluteAllocation();
    double oldAdviseAUMAllocation = round(updateUserAdvise.getIssueAdviseAllocationValue() * 100 / totalInvestment);
    double currentAdviseAUMAllocation = fundAdviceIssued.getAllocation();
    double closingAUMAllocation = round(oldAdviseAUMAllocation * oldApprovalAbsoluteAllocation / 100.0);
    if(currentAdviseAUMAllocation >= closingAUMAllocation) {
      double revisedAUMAllocation = round(currentAdviseAUMAllocation - closingAUMAllocation);
      double updateCurrentAbsoluteAlocationValue = 0;
      if (isZero(currentFundValue))
        updateCurrentAbsoluteAlocationValue = round(totalInvestment * revisedAUMAllocation / 100.);
      else
        updateCurrentAbsoluteAlocationValue = round(currentFundValue * revisedAUMAllocation / 100.);
      updateUserAdvise.setIssueAdviceAbsoluteAllocation(round(updateCurrentAbsoluteAlocationValue * 100 / hypotheticalFundCashWhenApprovalsArePending));
      updateUserAdvise.setIssueAdviseAllocationValue(updateCurrentAbsoluteAlocationValue);
      this.hypotheticalFundCashWhenApprovalsArePending = round(
          hypotheticalFundCashWhenApprovalsArePending - (oldPendingResponse.getNumberOfShares()
              * closeAdviseIdsWithPricesPendingApproval.get(oldPendingResponse.getAdviseId())));
      removeOldPendingOrders(fundAdviceIssued.getAdviseId(), true);
    } else {
      double updateCurrentAbsoluteAlocationValue = 0;
      if (isZero(currentFundValue))
        updateCurrentAbsoluteAlocationValue = round(totalInvestment * currentAdviseAUMAllocation / 100.);
      else
        updateCurrentAbsoluteAlocationValue = round(currentFundValue * currentAdviseAUMAllocation / 100.);
      updateUserAdvise.setIssueAdviceAbsoluteAllocation(round(updateCurrentAbsoluteAlocationValue * 100 / hypotheticalFundCashWhenApprovalsArePending));
      updateUserAdvise.setIssueAdviseAllocationValue(updateCurrentAbsoluteAlocationValue);
    }
    return updateUserAdvise;
  }

  protected UserResponseReceived updateWithCurrentCloseApproval(UserResponseReceived userResponseReceived) {
    if (userResponseReceived.getAdviseType().equals(Close)) {
      UserResponseParameters previousPendingApproval = getPreviousUnapprovedResponse(
          userResponseReceived.getAdviseId());
      if (previousPendingApproval.getAdviseId().isEmpty())
        return userResponseReceived;
      else if (previousPendingApproval.getAdviseType().equals(Issue)) {
        UserAdvise oldIssuedUserAdvise = activeAdvises.stream()
            .filter(advise -> advise.getAdviseId().equals(previousPendingApproval.getAdviseId())).findFirst().get();
        double oldAdviseAbsoluteAllocation = oldIssuedUserAdvise.getIssueAdviceAbsoluteAllocation();
        double oldAdviseAllocationValue = oldIssuedUserAdvise.getIssueAdviseAllocationValue();
        double updatedAdviseAbsoluteAllocation = round(
            oldAdviseAbsoluteAllocation * (100 - userResponseReceived.getAbsoluteAllocation()) / 100.);
        oldIssuedUserAdvise.setIssueAdviceAbsoluteAllocation(updatedAdviseAbsoluteAllocation);
        cashComponent = round(cashComponent + oldAdviseAllocationValue);
        hypotheticalFundCashWhenApprovalsArePending = round(hypotheticalFundCashWhenApprovalsArePending + oldAdviseAllocationValue);
        double updateIssueAdviseAllocationValue = round(updatedAdviseAbsoluteAllocation * cashComponent / 100.0);
        oldIssuedUserAdvise.setIssueAdviseAllocationValue(updateIssueAdviseAllocationValue);
        UserResponseReceived updatedUserResponse = new UserResponseReceived.UserResponseReceivedBuilder(
            userResponseReceived.getUsername(),
            userResponseReceived.getAdviserUsername(),
            userResponseReceived.getFundName(),
            userResponseReceived.getInvestingMode(), 
            userResponseReceived.getAdviseId(), 
            previousPendingApproval.getAdviseType(), 
            userResponseReceived.getClientCode(), 
            userResponseReceived.getBrokerName(),
            userResponseReceived.getExchange(),
            userResponseReceived.getToken(), 
            userResponseReceived.getSymbol(),
            updatedAdviseAbsoluteAllocation,
            userResponseReceived.getInvestmentDate(), 
            userResponseReceived.getUserResponseType())
            .withAllocationValue(updateIssueAdviseAllocationValue)
            .withNumberOfShares(0).build();
        
        activeAdvises.removeIf(userAdvise -> userAdvise.getAdviseId().equals(userResponseReceived.getAdviseId()));
        if(!isZero(updatedUserResponse.getAbsoluteAllocation()))
          activeAdvises.add(oldIssuedUserAdvise);
        removeOldPendingOrders(userResponseReceived.getAdviseId(), true);
        return updatedUserResponse;
      }
      else {
        double updatedAbsoluteAllocation = getUpdatedAbsoluteAllocation(previousPendingApproval.getAbsoluteAllocation(),
            userResponseReceived.getAbsoluteAllocation());
        int updatedNumberOfShares = previousPendingApproval.getNumberOfShares()
            + userResponseReceived.getNumberOfShares();
        
        UserResponseReceived updatedUserResponse = new UserResponseReceived.UserResponseReceivedBuilder(
            userResponseReceived.getUsername(),
            userResponseReceived.getAdviserUsername(),
            userResponseReceived.getFundName(),
            userResponseReceived.getInvestingMode(), 
            userResponseReceived.getAdviseId(), 
            userResponseReceived.getAdviseType(), 
            userResponseReceived.getClientCode(), 
            userResponseReceived.getBrokerName(),
            userResponseReceived.getExchange(),
            userResponseReceived.getToken(), 
            userResponseReceived.getSymbol(),
            updatedAbsoluteAllocation,
            userResponseReceived.getInvestmentDate(), 
            userResponseReceived.getUserResponseType())
            .withAllocationValue(userResponseReceived.getAllocationValue())
            .withNumberOfShares(updatedNumberOfShares).build();
        removeOldPendingOrders(userResponseReceived.getAdviseId(), true);
        return updatedUserResponse;
      }
    } else {
      UserResponseParameters previousPendingApproval = getPreviousUnapprovedResponse(
          userResponseReceived.getAdviseId());
      if (previousPendingApproval.getAdviseType().equals(Issue))
        return userResponseReceived;
      else {
        int updatedNumberOfShares = previousPendingApproval.getNumberOfShares()
            - (int) round(userResponseReceived.getAllocationValue()
                / closeAdviseIdsWithPricesPendingApproval.get(userResponseReceived.getAdviseId()), 0);
        double updatedAbsoluteAllocation = getUpdatedAbsoluteAllocation(previousPendingApproval.getAbsoluteAllocation(),
            previousPendingApproval.getNumberOfShares(), updatedNumberOfShares);

        UserResponseReceived updatedUserResponse = new UserResponseReceived.UserResponseReceivedBuilder(
            userResponseReceived.getUsername(), userResponseReceived.getAdviserUsername(),
            userResponseReceived.getFundName(), userResponseReceived.getInvestingMode(),
            previousPendingApproval.getAdviseId(), Close, userResponseReceived.getClientCode(),
            userResponseReceived.getBrokerName(), userResponseReceived.getExchange(), userResponseReceived.getToken(),
            userResponseReceived.getSymbol(), updatedAbsoluteAllocation, userResponseReceived.getInvestmentDate(),
            userResponseReceived.getUserResponseType()).withNumberOfShares(updatedNumberOfShares).build();
        removeOldPendingOrders(userResponseReceived.getAdviseId(), true);
        hypotheticalFundCashWhenApprovalsArePending -= previousPendingApproval.getNumberOfShares()
            * closeAdviseIdsWithPricesPendingApproval.get(userResponseReceived.getAdviseId());
        return updatedUserResponse;
      }
    }
  }

  private double getUpdatedAbsoluteAllocation(double oldApprovalAbsoluteAllocation, int numberOfShares,
      int updatedNumberOfShares) {
    return round(oldApprovalAbsoluteAllocation * updatedNumberOfShares / numberOfShares);
  }

  private double getUpdatedAbsoluteAllocation(double oldApprovalAbsoluteAllocation, double absoluteAllocation) {
    double currentAllocation = round((100. - oldApprovalAbsoluteAllocation) * absoluteAllocation / 100.); 
    return round(oldApprovalAbsoluteAllocation + currentAllocation);
  }

  private double getOldApprovalAbsoluteAllocation(String adviseId, boolean hasPendingApprovals) {
    if (hasPendingApprovals) {
      UserAdvise userAdvise = activeAdvises.stream()
          .filter(advise -> advise.getAdviseId().equals(adviseId)).findFirst().get();
      return userAdvise.getIssueAdviceAbsoluteAllocation();
    }
    return 0;
  }

  private void removeOldPendingOrders(String adviseId, boolean hasPendingApprovals) {
    if (hasPendingApprovals) {
      List<UserResponseParameters> tempResponseList = new ArrayList<UserResponseParameters>();
      for (UserResponseParameters oldResponse : pendingApprovalRequests) {
        if (oldResponse.getAdviseId().equals(adviseId))
          tempResponseList.add(oldResponse);
      }
      if(!tempResponseList.isEmpty())
        pendingApprovalRequests.removeAll(tempResponseList);
    }
  }

  private double getOldApprovalAllocationValue(String adviseId, boolean hasPendingApprovals) {
    if (hasPendingApprovals) {
      UserAdvise userAdvise = activeAdvises.stream()
          .filter(advise -> advise.getAdviseId().equals(adviseId)).findFirst().get();
      return userAdvise.getIssueAdviseAllocationValue();
    }
    return 0;
  }

  private UserResponseParameters getPreviousUnapprovedResponse(String adviseId) {
    for (UserResponseParameters userReponseParameters : pendingApprovalRequests) {
      if (userReponseParameters.getAdviseId().equals(adviseId))
        return userReponseParameters;
    }
    return withBlankResponse();
  }

  public double computeNewAdviceWithPendingApprovalsMonetaryAllocation(double absoluteAllocation) {
    return round((hypotheticalFundCashWhenApprovalsArePending * absoluteAllocation) / 100.0);
  }

  private void updateCashComponentWithTheResidualCash(double allocationValue, double tradedValue) {
    if(allocationValue > 0)
      cashComponent = round(cashComponent + allocationValue - tradedValue);
  }

  private boolean isNotAClosedAdvise(double tradedValue) {
    return !isZero(tradedValue);
  }

  protected void removeAdviseFromActiveAdviseList(String adviseId) {
    activeAdvises.removeIf(advise -> adviseId.equals(advise.getAdviseId()));
    allAdvisesInCurrentCycle.removeIf(currentCycleId -> currentCycleId.contains(adviseId));
  }

  private boolean fundHasPendingApprovals(){
    return !pendingApprovalRequests.isEmpty();
  }

  protected boolean hasCompletelyExited(FundAdviceTradeExecuted tradesExecuted) {
    return (tradesExecuted.getUserAdviseState().equals(OpenAdviseOrderRejected) || 
        tradesExecuted.getUserAdviseState().equals(CompleteExitAdviseOrderExecuted) || tradesExecuted.getUserAdviseState().equals(ExecutingCorporateActionSell)
            || tradesExecuted.getUserAdviseState().equals(UserMFCompleteCloseAdviseOrderExecuted));
  }
  
  @DynamoDBIgnore
  public boolean containsOnlyEquityAdvises() {
    for (UserAdvise userAdvise : activeAdvises) {
      if(userAdvise.getToken().endsWith("-MF"))
        return false;
    }
    return true;
  }

  @DynamoDBIgnore
  public double getOldUserAdviseEntryPrice(String adviseId) {
    double oldPrice = 0;
    try {
      oldPrice = (activeAdvises.stream().filter(advise -> advise.getAdviseId().equals(adviseId)).findFirst().get())
          .getAverageEntryPrice().getPrice().getPrice();
    } catch (Exception e) {
      //log.warn("Old advise not found, ", e);
    }
    return oldPrice;
  }

  @DynamoDBIgnore
  public UserAcceptanceMode getUserFundAcceptanceMode() {
    return isAutoApprovedOn ? Auto : Manual;
  }

  @DynamoDBHashKey(attributeName = "username")
  public String getUsername() {
    return userFundCompositeKey.getUsername();
  }

  public void setUsername(String username) {
    userFundCompositeKey.setUsername(username);
  }

  @DynamoDBRangeKey(attributeName = "fundWithAdviserUsername")
  public String getFundWithAdviserUsername() {
    return userFundCompositeKey.getFundWithAdviserUsername();
  }

  public void setFundWithAdviserUsername(String fundWithAdviserUsername) {
    userFundCompositeKey.setFundWithAdviserUsername(fundWithAdviserUsername);
  }

}