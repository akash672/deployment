package com.wt.domain;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;
import com.wt.domain.write.events.UserFundPerformanceCalculated;

public class UserPerformanceMarshaller implements DynamoDBTypeConverter<String, List<UserFundPerformanceCalculated>> {

  @Override
  public String convert(List<UserFundPerformanceCalculated> object) {
    return new Gson().toJson(object);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<UserFundPerformanceCalculated> unconvert(String object) {
    return new Gson().fromJson(object, List.class);
  }

}
