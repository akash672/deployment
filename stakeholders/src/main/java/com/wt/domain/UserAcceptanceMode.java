package com.wt.domain;

import lombok.Getter;

public enum UserAcceptanceMode {
  Auto("Auto"), Manual("Manual");

  @Getter
  private final String acceptanceMode;

  private UserAcceptanceMode(String acceptanceMode) {
    this.acceptanceMode = acceptanceMode;
  }
}
