package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class AddLumpSum {
  private String fundName;
  private double lumpSumAmount;
  private String adviserUsername;
  private InvestingMode investingMode;

  @JsonCreator
  public AddLumpSum(@JsonProperty("adviserUsername") String adviserUsername, @JsonProperty("fundName") String fundName,
      @JsonProperty("lumpSumAmount") double lumpSumAmount, 
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super();
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.lumpSumAmount = lumpSumAmount;
    this.investingMode = investingMode;
  }
}