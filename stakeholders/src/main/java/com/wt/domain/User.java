package com.wt.domain;

import static com.wt.domain.AdviseType.Close;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static java.lang.Double.compare;
import static java.util.Comparator.comparingLong;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.wt.domain.write.AmcCodeWithInvestingMode;
import com.wt.domain.write.commands.AcceptUserApprovalResponse;
import com.wt.domain.write.commands.GetPriceBasketFromTracker;
import com.wt.domain.write.commands.PriceSnapshotsFromPriceSource;
import com.wt.domain.write.commands.SubmitSecurityAnswers;
import com.wt.domain.write.commands.SubmitUserAnswers;
import com.wt.domain.write.commands.UpdateFolioNumber;
import com.wt.domain.write.events.AddToPostMarketCommandQueue;
import com.wt.domain.write.events.CognitoUserRegistered;
import com.wt.domain.write.events.DeviceTokenUpdated;
import com.wt.domain.write.events.ExistingUserRegistered;
import com.wt.domain.write.events.KycApprovedEvent;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.PostMarketCommandQueueProcessingStarted;
import com.wt.domain.write.events.SecurityAnswersSubmitted;
import com.wt.domain.write.events.UserEvent;
import com.wt.domain.write.events.UserLoggedIn;
import com.wt.domain.write.events.UserPasswordChanged;
import com.wt.domain.write.events.UserPasswordResetted;
import com.wt.domain.write.events.UserPerformanceCalculatedWithDate;
import com.wt.domain.write.events.UserRegistered;
import com.wt.domain.write.events.UserUnsubscribedToFund;
import com.wt.domain.write.events.WealthCodeUpdated;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Table(name = "user")
@Entity
@ToString(callSuper=true)
@Log4j
public class User extends CurrentUserDb implements Serializable {

  private static final long serialVersionUID = 1L;

  @Getter
  @Setter
  private byte[] password;
  @Getter
  @Setter
  private byte[] salt;
  @Getter
  private String accessToken;
  @Getter
  private boolean registered;
  @Getter
  private String wealthCode;
  @Getter
  @Setter
  private String deviceToken;

  @Getter
  @Setter
  private double realWalletAmount;

  @Getter
  @Setter
  private long lastFundActionTimestamp;

  @Getter
  private long performanceVirtualCalculatedTill;

  @Getter
  private long performanceRealCalculatedTill;

  @Getter
  @Setter
  private Map<String, String> securityAnswers = new HashMap<String, String>();

  @Getter
  private HashMap<AmcCodeWithInvestingMode, String> amcCodeToFolioNumber = new HashMap<AmcCodeWithInvestingMode, String>();

  @Getter
  private ArrayDeque<AddToPostMarketCommandQueue> postMarketSubscriptionCommandsQueue = new ArrayDeque<AddToPostMarketCommandQueue>();
  
  private double requiredMargin;

  public User() {
    super();
    this.registered = false;
  }

  private User(String email, String username, String firstName, String lastName, byte[] password, byte[] salt,
      String accessToken, boolean registered, String phoneNumber, String isWealthCode, String wealthCode, UserType role,
      String deviceToken, long registeredTimestamp, String clientCode, String panCard, String brokerCompany,
      double virtualWalletAmount, double realWalletAmount, long lastFundActionTimestamp,
      long performanceVirtualCalculatedTill, long performanceRealCalculatedTill,
      List<FundWithAdviserAndInvestingMode> advisorToFunds, Map<String, Map<String, String>> answers,
      Map<String, String> securityAnswers, HashMap<AmcCodeWithInvestingMode, String> amcCodeToFolioNumber,
      RiskLevel riskProfile, ArrayDeque<AddToPostMarketCommandQueue> postMarketSubscriptionCommandsQueue,
      double requiredMargin) {
    super(email, username, firstName, lastName, phoneNumber, isWealthCode, role, virtualWalletAmount, registeredTimestamp, clientCode, panCard, brokerCompany, advisorToFunds, answers, riskProfile);
    this.password = password;
    this.salt = salt;
    this.accessToken = accessToken;
    this.registered = registered;
    this.wealthCode = wealthCode;
    this.deviceToken = deviceToken;
    this.realWalletAmount = realWalletAmount;
    this.lastFundActionTimestamp = lastFundActionTimestamp;
    this.performanceVirtualCalculatedTill = performanceVirtualCalculatedTill;
    this.performanceRealCalculatedTill = performanceRealCalculatedTill;
    this.securityAnswers = securityAnswers;
    this.amcCodeToFolioNumber = amcCodeToFolioNumber;
    this.postMarketSubscriptionCommandsQueue = postMarketSubscriptionCommandsQueue;
    this.requiredMargin = requiredMargin;
  }
  
  public User copy() {
    return new User(getEmail(), getUsername(), getFirstName(), getLastName(), (password != null ? Arrays.copyOf(password, password.length) : null),
             (salt != null ? Arrays.copyOf(salt, salt.length) : null), 
        accessToken, registered, getPhoneNumber(), getIsWealthCode(), wealthCode, getRole(), 
        deviceToken, getRegisteredTimestamp(), getClientCode(), getPanCard(), getBrokerCompany(), 
        getVirtualWalletAmount(), realWalletAmount, lastFundActionTimestamp, performanceVirtualCalculatedTill,
        performanceRealCalculatedTill, new ArrayList<FundWithAdviserAndInvestingMode>(getAdvisorToFunds()), 
        new HashMap<String, Map<String, String>>(getAnswers()), new HashMap<String, String>(securityAnswers), 
        new HashMap<AmcCodeWithInvestingMode, String>(amcCodeToFolioNumber), 
        getRiskProfile(), new ArrayDeque<AddToPostMarketCommandQueue>(postMarketSubscriptionCommandsQueue), requiredMargin);
  }
  
  public void update(UserEvent evt) throws ParseException {
    super.update(evt);

    if (evt instanceof CognitoUserRegistered) {
      CognitoUserRegistered registerEvent = (CognitoUserRegistered) evt;
      setCognitoUserParams(registerEvent);
      setBrokerCompany(MOCK.getBrokerName());
    } else if (evt instanceof ExistingUserRegistered) {
      ExistingUserRegistered registerEvent = (ExistingUserRegistered) evt;
      setCognitoUserParams(registerEvent);
    } else if (evt instanceof UserLoggedIn) {
      UserLoggedIn loginEvent = (UserLoggedIn) evt;
      setLoginParams(loginEvent);
    } else if (evt instanceof UserPasswordResetted) {
      this.password = ((UserPasswordResetted) evt).getNewPassword();
      this.salt = ((UserPasswordResetted) evt).getSalt();
    }  else if (evt instanceof WealthCodeUpdated) {
      updateWealthCode(evt);
    } else if (evt instanceof SecurityAnswersSubmitted) {
      addSecurityAnswers(evt);
    } else if (evt instanceof UserPasswordChanged) {
      changepassword(evt);
    } else if (evt instanceof UserPerformanceCalculatedWithDate) {
      historicalPerformanceCalculated((UserPerformanceCalculatedWithDate) evt);
    } else if (evt instanceof DeviceTokenUpdated) {
      DeviceTokenUpdated event = (DeviceTokenUpdated) evt;
      if (nullAndEmptyCheck(event.getDeviceToken()))
        setDeviceToken(event.getDeviceToken());
    } else if (evt instanceof MarginReceived) {
      MarginReceived margin = (MarginReceived) evt;
      setWalletAmount(margin);
    } else if (evt instanceof AddToPostMarketCommandQueue) {
      AddToPostMarketCommandQueue command = (AddToPostMarketCommandQueue) evt;
      getPostMarketSubscriptionCommandsQueue().addLast(command);
    } else if (evt instanceof PostMarketCommandQueueProcessingStarted) {
      getPostMarketSubscriptionCommandsQueue().clear();
    } else if (evt instanceof UpdateFolioNumber) {
      updateFolioNumber(evt);
    } 
  }


  private void updateFolioNumber(UserEvent evt) {
    UpdateFolioNumber updateFolioNumber = (UpdateFolioNumber) evt;
    AmcCodeWithInvestingMode amcCodeWithInvestingMode = new AmcCodeWithInvestingMode(updateFolioNumber.getAmcCode(),
        updateFolioNumber.getInvestingMode());
    this.amcCodeToFolioNumber.put(amcCodeWithInvestingMode, updateFolioNumber.getFolioNumber());
  }

  private void setCognitoUserParams(CognitoUserRegistered registerEvent) {
    password = (registerEvent.getPassword());
    salt = (registerEvent.getSalt());
  }

  private void setWalletAmount(MarginReceived margin) {
    if (margin.getInvestingMode() == InvestingMode.REAL)
      setRealWalletAmount(round(margin.getMarginAmount()));
  }

  protected void kycApproval(KycApprovedEvent event) throws ParseException {
    super.kycApproval(event);
    this.performanceRealCalculatedTill = DateUtils.getBODFromDate(event.getTimestamp());
  }

  private void historicalPerformanceCalculated(UserPerformanceCalculatedWithDate evt) throws ParseException {
    if (evt.getInvestingMode() == InvestingMode.REAL) {
      if (evt.getDate() > performanceRealCalculatedTill) {
        performanceRealCalculatedTill = DateUtils.getEODFromDate(evt.getDate());
      }
    } else if (evt.getInvestingMode() == VIRTUAL) {
      if (evt.getDate() > performanceVirtualCalculatedTill) {
        performanceVirtualCalculatedTill = DateUtils.getEODFromDate(evt.getDate());
      }
    }
  }

  private void changepassword(UserEvent evt) {
    UserPasswordChanged event = (UserPasswordChanged) evt;
    this.setPassword(event.getPassword());
    this.setSalt(event.getSalt());
  }

  private boolean check(Stream<String> fundName, String[] fundNames) {
    return fundName.anyMatch(fund -> {
      for (int i = 0; i < fundNames.length; i++) {
        if (fundNames[i].equals(fund))
          return true;
      }
      return false;
    });
  }

  public void validate(AddToPostMarketCommandQueue event) throws IllegalStateException {
    if (event.getClazz().equals(AcceptUserApprovalResponse.class.getSimpleName())) {
      String[] fundNames = event.getVariables().get("fundNames").split(",");
      boolean isSameFund = getPostMarketSubscriptionCommandsQueue().clone().stream()
          .filter(command -> command.getClazz().equals(AcceptUserApprovalResponse.class.getSimpleName()))
          .map(command -> command.getVariables().getOrDefault("fundNames", "").split(","))
          .map(stringArray -> Arrays.stream(stringArray)).anyMatch(fundName -> check(fundName, fundNames));
      if (isSameFund)
        throw new IllegalStateException("Cannot accept multiple request in same fund");
    } else {
      String fundName = event.getVariables().get("fundName");
      if (fundName == null) {
        throw new IllegalArgumentException("Fund Name cannot be null");
      }
      AddToPostMarketCommandQueue cmd = getPostMarketSubscriptionCommandsQueue().clone().stream()
          .filter(command -> command.getVariables().getOrDefault("fundName", "").equals(fundName)).findFirst()
          .orElse(null);
      if (cmd != null)
        throw new IllegalStateException("Cannot accept multiple request in same fund");
    }
  }

  public void validate(SubmitSecurityAnswers command) {
    if (getSecurityAnswers().size() > 0)
      throw new IllegalStateException(
          "Security Answers are already added, send Change Security Question and Answer request");
    if (!getRole().equals(UserType.TU))
      throw new IllegalStateException("KYC is not completed yet");
  }

  private void addSecurityAnswers(UserEvent evt) {
    SecurityAnswersSubmitted event = (SecurityAnswersSubmitted) evt;
    securityAnswers.putAll(event.getSecurityAnswers());
  }

  protected void updateWealthCode(UserEvent evt) {
    super.updateWealthCode(evt);
    wealthCode = ((WealthCodeUpdated) evt).getWealthCode();
  }

  protected void removeFundUserSubscription(UserUnsubscribedToFund fundUserUnsubscribed) {
    super.removeFundUserSubscription(fundUserUnsubscribed);
    lastFundActionTimestamp = fundUserUnsubscribed.getLastFundActionTimestamp();
  }

  protected void setdefaultRegistrationParams(UserRegistered registerEvent) {
    super.setdefaultRegistrationParams(registerEvent);
    registered = true;
    if (nullAndEmptyCheck(registerEvent.getDeviceToken()))
      setDeviceToken(registerEvent.getDeviceToken());
    wealthCode = "";
    performanceVirtualCalculatedTill = DateUtils.lastDayEOD(registerEvent.getRegisterationTime());
  }

  private void setLoginParams(UserLoggedIn loginEvent) {
    if (nullAndEmptyCheck(loginEvent.getDeviceToken()))
      setDeviceToken(loginEvent.getDeviceToken());
  }

  public RiskLevel decideRiskProfile(SubmitUserAnswers command) {
    String riskProfileQuestion = "How do you want your money to grow?";
    if (command.getAnswers().get(riskProfileQuestion) == null)
      return RiskLevel.MEDIUM;

    String riskProfileAnswer = command.getAnswers().get(riskProfileQuestion);
    if (riskProfileAnswer
        .equalsIgnoreCase("Slow and steady wins the race, I prefer safe investments like Fixed Deposits.")) {
      return RiskLevel.LOW;
    } else if (riskProfileAnswer
        .equalsIgnoreCase("I believe in moderation, I'm happy as long as I beat inflation like Mutual Funds.")) {
      return RiskLevel.MEDIUM;
    }
    return RiskLevel.HIGH;
  }

  private boolean nullAndEmptyCheck(String parameter) {
    return !(parameter == null || parameter.isEmpty());
  }

  public boolean isSecurityAnswerEmpty() {
    return this.wealthCode.isEmpty();
  }

  public boolean handleApprovals(AcceptUserApprovalResponse acceptUserApprovalResponse, InvestingMode investingMode, WDActorSelections selections) {
    if (compare(requiredMarginForApproval(acceptUserApprovalResponse, investingMode, selections), 
        (investingMode.equals(VIRTUAL) ? getVirtualWalletAmount() : realWalletAmount)) <= 0) 
      return true;
    else
      return false;
  }

  public double requiredMarginForApproval(AcceptUserApprovalResponse acceptUserApprovalResponse,
      InvestingMode investingMode, WDActorSelections selections) {
    requiredMargin = 0.;
    Set<String> wdIdsInResponses = new HashSet<String>();
    acceptUserApprovalResponse.getUserResponseParameters().stream()
        .forEach(userResponses -> wdIdsInResponses.add(userResponses.getToken()));
    try {
      PriceSnapshotsFromPriceSource priceSet = (PriceSnapshotsFromPriceSource) askAndWaitForResult(
          selections.activeInstrumentTracker(), new GetPriceBasketFromTracker(wdIdsInResponses));
      streamOfResponses(acceptUserApprovalResponse, investingMode).forEach(userApprovalResponse -> {
        if (userApprovalResponse.getAdviseType().equals(Close)) {
          double potentialProceeds = userApprovalResponse.getNumberOfShares()
              * priceSet.getTickerToPriceWithPafMap().get(userApprovalResponse.getToken()).getPrice().getPrice();
          requiredMargin -= potentialProceeds;
        } else
          requiredMargin += userApprovalResponse.getAllocationValue();
      });
    } catch (Exception e1) {
      log.error("Could not get the prices for " + wdIdsInResponses);
    }
    return requiredMargin;
  }
  
  private Stream<UserResponseParameters> streamOfResponses(AcceptUserApprovalResponse acceptUserApprovalResponse, 
      InvestingMode investingMode) {
    return acceptUserApprovalResponse.getUserResponseParameters().stream()
    .sorted(comparingLong(userApprovalResponse -> userApprovalResponse.getInvestmentDate()))
    .filter(userApprovalResponse -> userApprovalResponse.getInvestingMode().equals(investingMode));
  }
}