package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class UserFundPerformanceKey implements Serializable {

  private static final long serialVersionUID = 1L;
  @Getter
  @Setter
  @DynamoDBHashKey
  private String usernameFundWithAdviserUsername;
  @Getter
  @Setter
  @DynamoDBRangeKey(attributeName = "date")
  private long date;

  public UserFundPerformanceKey(String email, String fund, InvestingMode investingMode, String adviser, long date) {
    this(email + CompositeKeySeparator + fund + CompositeKeySeparator + investingMode.name() + CompositeKeySeparator
        + adviser, date);
  }

  public String getUserEmail() {
    return usernameFundWithAdviserUsername.split(CompositeKeySeparator)[0];
  }

  public String getAdviser() {
    return usernameFundWithAdviserUsername.split(CompositeKeySeparator)[3];
  }

  public String getInvestingMode() {
    return usernameFundWithAdviserUsername.split(CompositeKeySeparator)[2];
  }

  public String getFund() {
    return usernameFundWithAdviserUsername.split(CompositeKeySeparator)[1];
  }

  @DynamoDBIgnore
  public String persistenceId() {
    return usernameFundWithAdviserUsername + CompositeKeySeparator + date;
  }

}
