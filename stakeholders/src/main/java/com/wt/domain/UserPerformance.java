package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@DynamoDBTable(tableName = "User_Performance")
@ToString
public class UserPerformance implements Serializable {

  private static final long serialVersionUID = 1L;

  @org.springframework.data.annotation.Id
  @DynamoDBIgnore
  private UserPerformanceKey userPerformanceKey = new UserPerformanceKey();

  @Getter
  @Setter
  private double realizedPnl;

  @Getter
  @Setter
  private double totalInvestmentAcrossFunds;
  
  @Getter
  @Setter
  private double unrealizedPnl;

  @Getter
  @Setter
  private double pnl;

  @Getter
  @Setter
  private double returnPct;

  public UserPerformance(String userEmail, double totalInvestmentAcrossFunds, double realizedPnl, double unrealizedPnl, double returnPct, long performanceTime,
      double pnl, InvestingMode investingMode) {
    super();
    this.userPerformanceKey = new UserPerformanceKey(userEmail, investingMode.getInvestingMode(), performanceTime);
    this.totalInvestmentAcrossFunds = totalInvestmentAcrossFunds;
    this.realizedPnl = realizedPnl;
    this.unrealizedPnl = unrealizedPnl;
    this.pnl = pnl;
    this.returnPct = returnPct;
  }

  @DynamoDBHashKey(attributeName = "usernameWithInvestingMode")
  public String getUsernameWithInvestingMode() {
    return userPerformanceKey.getUsernameWithInvestingMode();
  }
  
  public void setUsernameWithInvestingMode(String userEmailWithInvestingMode) {
    userPerformanceKey.setUsernameWithInvestingMode(userEmailWithInvestingMode);
  }

  @DynamoDBRangeKey(attributeName = "date")
  public long getDate() {
    return userPerformanceKey.getDate();
  }

  public void setDate(long date) {
    userPerformanceKey.setDate(date);
  }

}
