package com.wt.domain;

import com.wt.domain.write.events.MarginRequested;

public interface Wallet {

  void requestWalletMoney(MarginRequested marginRequested);

}
