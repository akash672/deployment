package com.wt.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
public class UserFundCompositeKeyWithDate extends UserFundCompositeKey {

  private static final long serialVersionUID = 1L;

  @Getter
  private final long date;

  public UserFundCompositeKeyWithDate(String username, String fund, String adviser, InvestingMode investingMode, long date) {
    super(username, fund, adviser, investingMode);
    this.date = date;
  }
}
