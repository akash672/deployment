package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AddCashToUserFund {

  private String fundName;
  private String adviserUsername;
  private InvestingMode investingMode;
  private double cashToAddToUserFund;

  @JsonCreator
  public AddCashToUserFund(@JsonProperty("fundName") String fundName,
      @JsonProperty("adviserUsername") String adviserUsername,
      @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("cashToAddToUserFund") double cashToAddToUserFund) {
    super();
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.investingMode = investingMode;
    this.cashToAddToUserFund = cashToAddToUserFund;
  }
}
