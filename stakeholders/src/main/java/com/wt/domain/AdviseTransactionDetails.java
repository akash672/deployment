package com.wt.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@Setter
public class AdviseTransactionDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int sharesQuanity;
	private double price;
	private double currentMarketValue;
	private double allocation;
	private String exchange;
	private AdviseTransactionType adviseType;
}
