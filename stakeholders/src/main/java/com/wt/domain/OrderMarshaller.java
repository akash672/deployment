package com.wt.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class OrderMarshaller implements DynamoDBTypeConverter<String, Order> {
  @Override
  public String convert(Order object) {
    return new Gson().toJson(object);
  }

  @Override
  public Order unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Order>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }

}
