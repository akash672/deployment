package com.wt.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class UserAdviseQuantityToBeExited {
  @Getter private int quantityToBeExited;
}
