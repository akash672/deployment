package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor(access=PRIVATE)
public class UserFundCompositeKey implements Serializable{

  private static final long serialVersionUID = 1L;

  @DynamoDBHashKey
  private String username;

  @DynamoDBRangeKey
  private String fundWithAdviserUsername;

  
  public UserFundCompositeKey(String username, String fund, String adviser, InvestingMode investingMode) {
    this(username, fund + CompositeKeySeparator + investingMode.name()
        + CompositeKeySeparator + adviser);
  }
  
  public String getAdviser() {
    return fundWithAdviserUsername.split(CompositeKeySeparator)[2];
  }
  
  public String getFund() {
    return fundWithAdviserUsername.split(CompositeKeySeparator)[0];
  }
  
  public InvestingMode getInvestingMode() {
    return InvestingMode.valueOf(fundWithAdviserUsername.split(CompositeKeySeparator)[1]);
  }
  
  @DynamoDBIgnore
  public String persistenceId() {
    return username + CompositeKeySeparator + fundWithAdviserUsername;
  }
  
}