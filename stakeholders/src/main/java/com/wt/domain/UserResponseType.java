package com.wt.domain;

import lombok.Getter;

public enum UserResponseType {
  Approve("Approve"), Archive("Archive"), ExitFund("ExitFund"), Awaiting("Awaiting");

  @Getter
  private final String responseType;

  private UserResponseType(String responseType) {
    this.responseType = responseType;
  }
}
