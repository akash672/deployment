package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class FundWithAdviserAndInvestingMode implements Serializable {
  private static final long serialVersionUID = 1L;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String fund;
  
  @Getter
  @Setter
  @DynamoDBAttribute
  private String adviserUsername;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String investingMode;
  
  public FundWithAdviserAndInvestingMode() {}
      
  public FundWithAdviserAndInvestingMode(String fund, String adviser, String investingMode) {
    super();
    this.fund = fund;
    this.adviserUsername = adviser;
    this.investingMode = investingMode;
  }
}
