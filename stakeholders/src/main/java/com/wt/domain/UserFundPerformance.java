package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import java.io.Serializable;
import java.util.Comparator;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
@DynamoDBTable(tableName = "User_Fund_Performance")
public class UserFundPerformance implements Serializable, Performance {

  private static final long serialVersionUID = 1L;

  @org.springframework.data.annotation.Id
  @DynamoDBIgnore
  private UserFundPerformanceKey userFundPerformanceKey = new UserFundPerformanceKey();

  @Getter
  @Setter
  private double initialInvestment;
  
  @Getter
  @Setter
  private double portfolioValue;
  
  @Getter
  @Setter
  private double realizedPnl;

  @Getter
  @Setter
  private double unrealizedPnl;

  @Getter
  @Setter
  private double pnl;

  @Getter
  @Setter
  private double performance;

  public UserFundPerformance(String username, String fundName, String adviserUsername, double initialInvestment,
      double portfolioValue, double realizedPnl, double unrealizedPnl, double performance, long date, double pnl,
      InvestingMode investingMode) {
    super();
    userFundPerformanceKey.setUsernameFundWithAdviserUsername(username + CompositeKeySeparator + fundName
        + CompositeKeySeparator + investingMode.name() + CompositeKeySeparator + adviserUsername);
    userFundPerformanceKey.setDate(date);
    this.initialInvestment = initialInvestment;
    this.portfolioValue = portfolioValue;
    this.realizedPnl = realizedPnl;
    this.unrealizedPnl = unrealizedPnl;
    this.pnl = pnl;
    this.performance = performance;
  }

  @DynamoDBHashKey(attributeName = "usernameFundWithAdviserUsername")
  public String getUsernameFundWithAdviseUsername() {
    return userFundPerformanceKey.getUsernameFundWithAdviserUsername();
  }

  public void setUsernameFundWithAdviseUsername(String userFundWithAdviserUsername) {
    userFundPerformanceKey.setUsernameFundWithAdviserUsername(userFundWithAdviserUsername);
  }
  
  @DynamoDBRangeKey(attributeName = "date")
  public long getDate() {
    return userFundPerformanceKey.getDate();
  }

  public void setDate(long date) {
    userFundPerformanceKey.setDate(date);
  }

  public void setUserFundWithAdviseUsername(String username, String fundName, String adviserUsername, InvestingMode investingMode) {
    userFundPerformanceKey.setUsernameFundWithAdviserUsername(username + CompositeKeySeparator + fundName
        + CompositeKeySeparator + investingMode.name() + CompositeKeySeparator + adviserUsername);
  }

  public static Comparator<UserFundPerformance> UserFundPerformanceDatewise = new Comparator<UserFundPerformance>() {

    public int compare(UserFundPerformance userFundPerformance1, UserFundPerformance userFundPerformance2) {

      long date1 = userFundPerformance1.getDate();
      long date2 = userFundPerformance2.getDate();

      return Long.compare(date1, date2);
    }
  };
}
