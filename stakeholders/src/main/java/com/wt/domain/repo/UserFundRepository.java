package com.wt.domain.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.CurrentUserFundDb;
import com.wt.domain.UserFundCompositeKey;

@EnableScan
public interface UserFundRepository extends CrudRepository<CurrentUserFundDb, UserFundCompositeKey> {
  
}