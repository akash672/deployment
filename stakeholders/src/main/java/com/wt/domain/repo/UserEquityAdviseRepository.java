package com.wt.domain.repo;


import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.transaction.annotation.Transactional;

import com.wt.domain.UserEquityAdvise;

@Transactional
@EnableScan
public interface UserEquityAdviseRepository extends UserAdviseRepository<UserEquityAdvise> {
  
}