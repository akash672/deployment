package com.wt.domain.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.UserPerformance;
import com.wt.domain.UserPerformanceKey;

@EnableScan
public interface UserPerformanceRepository extends CrudRepository<UserPerformance, UserPerformanceKey> {

}