package com.wt.domain.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.wt.domain.UserAdvise;
import com.wt.domain.UserAdviseCompositeKey;

@NoRepositoryBean
@EnableScan
public interface UserAdviseRepository<T extends UserAdvise> extends CrudRepository<T, UserAdviseCompositeKey> {
  public T findByToken(@Param("token") String token);
}