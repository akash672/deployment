package com.wt.domain.repo;

import java.util.Optional;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.CurrentUserDb;

@EnableScan
public interface UserRepository extends CrudRepository<CurrentUserDb, String> {
  Optional<CurrentUserDb> findByEmail(String email);
}
