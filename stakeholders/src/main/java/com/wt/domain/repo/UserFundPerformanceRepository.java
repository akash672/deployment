package com.wt.domain.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.UserFundPerformance;
import com.wt.domain.UserFundPerformanceKey;

@EnableScan
public interface UserFundPerformanceRepository extends CrudRepository<UserFundPerformance, UserFundPerformanceKey> {

}