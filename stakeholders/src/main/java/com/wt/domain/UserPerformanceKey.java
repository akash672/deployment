package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class UserPerformanceKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @Getter
  @Setter
  @DynamoDBHashKey(attributeName = "usernameWithInvestingMode")
  private String usernameWithInvestingMode;
  @Getter
  @Setter
  @DynamoDBRangeKey(attributeName = "date")
  private long date;
  
  public UserPerformanceKey(String userEmail, String investingMode, long date) {
    this(userEmail + CompositeKeySeparator + investingMode, date);
  }
  
  public String getUserEmail() {
    return usernameWithInvestingMode.split(CompositeKeySeparator)[0];
  }

  public InvestingMode getInvestingMode() {
    return InvestingMode.valueOf(usernameWithInvestingMode.split(CompositeKeySeparator)[1]);
  }

}
