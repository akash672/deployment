package com.wt.domain;

import lombok.Getter;

public enum ApprovalResponse {
  Approve("Approve"), Decline("Decline");

  @Getter
  private final String responseType;

  private ApprovalResponse(String responseType) {
    this.responseType = responseType;
  }
}
