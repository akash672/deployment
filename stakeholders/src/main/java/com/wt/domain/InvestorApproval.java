package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.Getter;

@Getter
public class InvestorApproval {

  private String fundName;
  private String adviserUsername;
  private String wealthCode;
  private InvestingMode investingMode;
  private String basketOrderId;
  private Purpose investingPurpose;
  private ApprovalResponse approvalResponse;
  private BrokerAuthInformation brokerAuthInformation;

  @JsonCreator
  public InvestorApproval(@JsonProperty("fundName") String fundName,
      @JsonProperty("adviserUsername") String adviserUsername, @JsonProperty("wealthCode") String wealthCode,
      @JsonProperty("investingMode") InvestingMode investingMode, @JsonProperty("basketOrderId") String basketOrderId,
      @JsonProperty("investingPurpose") Purpose investingPurpose,
      @JsonProperty("approvalResponse") ApprovalResponse approvalResponse,
      @JsonProperty("brokerAuthentication") BrokerAuthInformation brokerAuthInformation
      ) {
    super();
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.wealthCode = wealthCode;
    this.investingMode = investingMode;
    this.basketOrderId = basketOrderId;
    this.investingPurpose = investingPurpose;
    this.approvalResponse = approvalResponse;
    this.brokerAuthInformation = brokerAuthInformation;
  }
}
