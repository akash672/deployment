package com.wt.domain;

import static com.wt.domain.Purpose.ONBOARDING;
import static com.wt.domain.UserAdviseState.ExecutingCorporateActionSell;
import static com.wt.domain.UserFundState.ADDITIONALSUMMIRRORING;
import static com.wt.domain.UserFundState.ADDITIONALSUMMIRRORINGWITHOUTTIMEOUT;
import static com.wt.domain.UserFundState.MIRRORING;
import static com.wt.domain.UserFundState.MIRRORINGWITHOUTTIMEOUT;
import static com.wt.domain.UserResponseType.Approve;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.domain.write.events.IssueType.MutualFund;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.DoublesUtil.round;
import static java.lang.String.valueOf;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

import javax.persistence.Entity;

import com.wt.domain.write.SwitchingToArchivedState;
import com.wt.domain.write.events.AdditionalSumAddedToUserFund;
import com.wt.domain.write.events.AdjustFundCashForInsufficientCapitalAdvise;
import com.wt.domain.write.events.AdvicesIssuedPreviously;
import com.wt.domain.write.events.ExecutedAdviseDetailsUpdated;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.FundAdviceTradeExecutedForWithdrawal;
import com.wt.domain.write.events.FundLumpSumAdded;
import com.wt.domain.write.events.FundSubscribed;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.FundUserSubscribed;
import com.wt.domain.write.events.FundUserUnsubscribed;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.MergerSellTradeExecuted;
import com.wt.domain.write.events.UpdateUserFundComposition;
import com.wt.domain.write.events.UserAdviseWithdrawalOrderRejected;
import com.wt.domain.write.events.UserCloseAdviseOrderRejected;
import com.wt.domain.write.events.UserFundAdviceClosed;
import com.wt.domain.write.events.UserFundAdviceIssued;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundPerformanceCalculated;
import com.wt.domain.write.events.UserFundWithdrawalDoneSuccessfully;
import com.wt.domain.write.events.UserOpenAdviseOrderRejected;
import com.wt.domain.write.events.UserResponseReceived;
import com.wt.domain.write.events.WithdrawSumFromUserFundConfirmed;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString(callSuper=true)
@NoArgsConstructor
public class UserFund extends CurrentUserFundDb implements Serializable {
  private static final long serialVersionUID = 1L;

  @Getter
  @Setter
  private long userFundPerformanceCalculatedTill;
  @Getter
  @Setter
  private Set<UserAdvise> mirroringAdvises = new CopyOnWriteArraySet<UserAdvise>();
  @Getter
  @Setter
  private Set<String> childrenPersistenceIds = new CopyOnWriteArraySet<String>();
  @Getter
  @Setter
  private Map<String, Double> adviseIdToExecutedShares = new HashMap<String, Double>();
  @Getter
  @Setter
  private Set<String> mirroringAdviseIdsTraded = new HashSet<String>();
  @Getter
  @Setter
  private Set<String> mirroringAdviseIdsRejected = new HashSet<String>();
  @Getter
  @Setter
  private Set<String> activeAdviseIdsTraded = new HashSet<String>();
  @Getter
  @Setter
  private List<String> adviseIdsWithPendingTradeConfirmations = new CopyOnWriteArrayList<String>();
  @Getter
  @Setter
  private Purpose investingPurpose;
  @Getter
  @Setter
  private boolean isFundArchived;
  @Getter
  @Setter
  private Set<String> interimExcecutedAdvisesForWithdrawal = new CopyOnWriteArraySet<String>();
  @Getter
  @Setter
  private String basketOrderId;
  
  public void update(UserFundEvent userFundEvent) {
    super.update(userFundEvent);
    if (userFundEvent instanceof FundUserSubscribed) {
      FundUserSubscribed event = (FundUserSubscribed) userFundEvent;
      this.investingPurpose = event.getInvestingPurpose();
      this.isFundArchived = false;
      this.userFundPerformanceCalculatedTill = event.getLastFundActionTimestamp();
      if (event.getBasketOrderId() != null)
        this.basketOrderId = event.getBasketOrderId();
    } else if (userFundEvent instanceof FundSubscribed) {
      if (investingPurpose == ONBOARDING)
        this.basketOrderId = valueOf("USERONBOARDING_" + getUsername() + "_"
            + getUserFundCompositeKey().getFund() + "_" + valueOf(1516473000000L));
      mirroringAdvises = new CopyOnWriteArraySet<UserAdvise>();
      mirroringAdviseIdsTraded = new HashSet<String>();
      mirroringAdviseIdsRejected = new HashSet<String>();
    } else if (userFundEvent instanceof FundUserUnsubscribed) {
      FundUserUnsubscribed fundUserUnsubscribed = (FundUserUnsubscribed) userFundEvent;
      if (fundUserUnsubscribed.getBasketOrderId() != null)
        this.basketOrderId = fundUserUnsubscribed.getBasketOrderId();
    } else if (userFundEvent instanceof FundLumpSumAdded) {
      mirroringAdvises = new CopyOnWriteArraySet<UserAdvise>();
      mirroringAdviseIdsTraded = new HashSet<String>();
      mirroringAdviseIdsRejected = new HashSet<String>();
    } else if (userFundEvent instanceof AdjustFundCashForInsufficientCapitalAdvise) {
      AdjustFundCashForInsufficientCapitalAdvise adjustFundCashForInsufficientCapitalAdvise =
          (AdjustFundCashForInsufficientCapitalAdvise) userFundEvent;
      removeIdFromPendingTradeConfirmationsList(adjustFundCashForInsufficientCapitalAdvise.getAdviseId());
      if (isFundNotSubscribedYet(adjustFundCashForInsufficientCapitalAdvise.getUserFundState()))
        mirroringAdviseIdsRejected.add(adjustFundCashForInsufficientCapitalAdvise.getAdviseId());
    } else if (userFundEvent instanceof UserOpenAdviseOrderRejected) {
      UserOpenAdviseOrderRejected userFundAdviseToBeRemoved = (UserOpenAdviseOrderRejected) userFundEvent;
      if (isFundNotSubscribedYet(userFundAdviseToBeRemoved.getUserFundState())) 
        mirroringAdviseIdsRejected.add(userFundAdviseToBeRemoved.getAdviseId());
        removeIdFromPendingTradeConfirmationsList(userFundAdviseToBeRemoved.getAdviseId());
      } else if (userFundEvent instanceof UserCloseAdviseOrderRejected) {
        UserCloseAdviseOrderRejected userFundCloseToBeRemoved = (UserCloseAdviseOrderRejected) userFundEvent;
        removeIdFromPendingTradeConfirmationsList(userFundCloseToBeRemoved.getAdviseId());
    } else if (userFundEvent instanceof ExecutedAdviseDetailsUpdated) {
      ExecutedAdviseDetailsUpdated adviseTradeExecuted = (ExecutedAdviseDetailsUpdated) userFundEvent;
      mirroringAdviseIdsTraded.add(adviseTradeExecuted.getAdviseId());
      activeAdviseIdsTraded.add(adviseTradeExecuted.getAdviseId());
    } else if (userFundEvent instanceof FundAdviceTradeExecuted) {
      FundAdviceTradeExecuted tradesExecuted = getUpdatedTradeExecuted((FundAdviceTradeExecuted) userFundEvent);
      if (hasCompletelyExited(tradesExecuted)) {
        removeAdviseFromActiveTradedList(tradesExecuted.getAdviseId());
      }
      removeIdFromPendingTradeConfirmationsList(tradesExecuted.getAdviseId());
    }else if(userFundEvent instanceof UserFundAdviceIssued){
      UserFundAdviceIssued fundAdviceIssued = (UserFundAdviceIssued) userFundEvent;
      if(isAutoApprovedOn()){
        addIdToPendingTradeConfirmationsList(fundAdviceIssued.getAdviseId());
      }
    }  else if(userFundEvent instanceof UserFundAdviceClosed){
      UserFundAdviceClosed adviceClosed = (UserFundAdviceClosed) userFundEvent;
      if(isAutoApprovedOn()){
        if(activeAdviseIdsTraded.contains(adviceClosed.getAdviseId()))
          addIdToPendingTradeConfirmationsList(adviceClosed.getAdviseId());
      }
    } else if (userFundEvent instanceof UserResponseReceived) {
      UserResponseReceived userResponseReceived = (UserResponseReceived) userFundEvent;
      if (isUserApproves(userResponseReceived)) {
        addIdToPendingTradeConfirmationsList(userResponseReceived.getAdviseId());
      }
       
    } else if (userFundEvent instanceof MergerSellTradeExecuted) {
      MergerSellTradeExecuted tradesExecuted = (MergerSellTradeExecuted) userFundEvent;
      removeAdviseFromActiveTradedList(tradesExecuted.getAdviseId());
    } else if (userFundEvent instanceof FundSuccessfullyUnsubscribed) {
      this.adviseIdToExecutedShares = new HashMap<String, Double>();
      this.activeAdviseIdsTraded = new CopyOnWriteArraySet<String>();
    } else if (userFundEvent instanceof UserFundPerformanceCalculated) {
      UserFundPerformanceCalculated userFundPerformanceCalculated = (UserFundPerformanceCalculated) userFundEvent;
      if (userFundPerformanceCalculatedTill < userFundPerformanceCalculated.getDate()) 
          this.userFundPerformanceCalculatedTill = userFundPerformanceCalculated.getDate();
    } else if (userFundEvent instanceof AdditionalSumAddedToUserFund) {
      AdditionalSumAddedToUserFund lumpSumAddedToUserFund = (AdditionalSumAddedToUserFund) userFundEvent;
      if(lumpSumAddedToUserFund.getBasketOrderId() != null)
        this.basketOrderId = lumpSumAddedToUserFund.getBasketOrderId();
    }  else if(userFundEvent instanceof SwitchingToArchivedState){
      this.isFundArchived = true;
    }  else if (userFundEvent instanceof WithdrawSumFromUserFundConfirmed) {
      WithdrawSumFromUserFundConfirmed withdrawSumFromUserFundConfirmed = (WithdrawSumFromUserFundConfirmed) userFundEvent;
      this.basketOrderId = withdrawSumFromUserFundConfirmed.getBasketOrderId();
    } else if (userFundEvent instanceof FundAdviceTradeExecutedForWithdrawal) {
      FundAdviceTradeExecutedForWithdrawal fundAdviceTradeExecutedForWithdrawal = (FundAdviceTradeExecutedForWithdrawal) userFundEvent;
      this.interimExcecutedAdvisesForWithdrawal.add(fundAdviceTradeExecutedForWithdrawal.getAdviseId());
    } else if (userFundEvent instanceof UserAdviseWithdrawalOrderRejected) {
      UserAdviseWithdrawalOrderRejected UserAdviseWithdrawalOrderRejected = (UserAdviseWithdrawalOrderRejected) userFundEvent;
      this.interimExcecutedAdvisesForWithdrawal.add(UserAdviseWithdrawalOrderRejected.getAdviseId());
    } else if (userFundEvent instanceof UserFundWithdrawalDoneSuccessfully) {
      this.interimExcecutedAdvisesForWithdrawal = new CopyOnWriteArraySet<String>();
    } else if(userFundEvent instanceof UpdateUserFundComposition) {
      UpdateUserFundComposition updateUserFundComposition = (UpdateUserFundComposition) userFundEvent;
      updateAdviseIdToExecutedShares(updateUserFundComposition);
    }
  }

  private void updateAdviseIdToExecutedShares(UpdateUserFundComposition updateUserFundComposition) {
    adviseIdToExecutedShares = updateUserFundComposition.getAdviseIdToShares().entrySet().stream()
        .filter(adviseIdToShare -> adviseIdToShare.getValue() > 0.)
        .collect(Collectors.toMap(e -> getAdviseIdFromUserAdviseCompositeKey(e.getKey()), Map.Entry::getValue));
  }

  private String getAdviseIdFromUserAdviseCompositeKey(String id) {
    return id.split(CompositeKeySeparator)[0];
  }
  private boolean isFundNotSubscribedYet(UserFundState userFundState) {
    return (userFundState != null) && 
        (userFundState.equals(MIRRORING) || 
         userFundState.equals(ADDITIONALSUMMIRRORING) ||
         userFundState.equals(MIRRORINGWITHOUTTIMEOUT) ||
         userFundState.equals(ADDITIONALSUMMIRRORINGWITHOUTTIMEOUT));
  }

  protected FundAdviceTradeExecuted getUpdatedTradeExecuted(FundAdviceTradeExecuted tradesExecuted) {
    
     tradesExecuted = super.getUpdatedTradeExecuted(tradesExecuted);
    
    if (tradesExecuted.getUserAdviseState() != ExecutingCorporateActionSell) {
      activeAdviseIdsTraded.add(tradesExecuted.getAdviseId());
      childrenPersistenceIds.add(new UserAdviseCompositeKey(tradesExecuted.getAdviseId(), tradesExecuted.getUsername(), 
          tradesExecuted.getFundName(), tradesExecuted.getAdviserUsername(), tradesExecuted.getInvestingMode()).persistenceId());
    }
    
    return tradesExecuted;

  }
  
  private void removeAdviseFromActiveTradedList(String adviseId) {
     activeAdviseIdsTraded.removeIf(id -> adviseId.equals(id));
   }

  public static IssueType getIssueType(String ticker) {
    if (ticker.endsWith("-EQ"))
      return Equity;
    return MutualFund;
  }

  protected UserAdvise updateUserAdviseDuringReplication(FundConstituent fundConstituent, UserAdviseCompositeKey userAdviseCompositeKey,
      double absoluteAllocation, AdvicesIssuedPreviously advicesIssuedPreviously) {
    UserAdvise userAdvise = super.updateUserAdviseDuringReplication(fundConstituent, userAdviseCompositeKey, absoluteAllocation, advicesIssuedPreviously);
    mirroringAdvises.add(userAdvise);
    childrenPersistenceIds.add(userAdvise.getUserAdviseCompositeKey().persistenceId());

    return userAdvise;
  }


  protected void removeAdviseFromActiveAdviseList(String adviseId) {
    super.removeAdviseFromActiveAdviseList(adviseId);
    childrenPersistenceIds.removeIf(childPersistenceId -> childPersistenceId.contains(adviseId));
  }

  public int getAdviseIdsProcessedCount() {
    return (mirroringAdviseIdsTraded.size() + mirroringAdviseIdsRejected.size());
  }

  
  private static boolean isUserApproves(UserResponseReceived userResponseReceived) {
    return userResponseReceived.getUserResponseType().equals(Approve);
  }
  
  private void addIdToPendingTradeConfirmationsList(String adviseId) {
    if(adviseIdsWithPendingTradeConfirmations == null)
      adviseIdsWithPendingTradeConfirmations = new CopyOnWriteArrayList<String>();
    adviseIdsWithPendingTradeConfirmations.add(adviseId);
  }
  
  private void removeIdFromPendingTradeConfirmationsList(String id) {
    if(adviseIdsWithPendingTradeConfirmations == null)
      adviseIdsWithPendingTradeConfirmations = new CopyOnWriteArrayList<String>();
    adviseIdsWithPendingTradeConfirmations.removeIf(adviseId -> id.equals(adviseId));
  }
   
  public Map<String, Double> getAdviseIdToExecutedSharesforWithdrawal(double allocationpct) {
    Map<String, Double> adviseIdsToExecutedSharesWithAllocationValue = new HashMap<String, Double>();
    for (Map.Entry<String, Double> adviseIdToExecutedShare : adviseIdToExecutedShares.entrySet()) {
      if((int)((adviseIdToExecutedShare.getValue() * allocationpct)/100) >= 1)
        adviseIdsToExecutedSharesWithAllocationValue.put(adviseIdToExecutedShare.getKey(), adviseIdToExecutedShare.getValue());
  }
    return adviseIdsToExecutedSharesWithAllocationValue;
  }
  
  public double getCashComponentUpdatedWithCashBlockedForPendingAdvises() {
    double cashBlockedWithPendingAdvises = super.getPendingApprovalRequests().stream().mapToDouble(i -> i.getAllocationValue()).sum();
    return round(cashBlockedWithPendingAdvises + super.getCashComponent());
  }

}