package com.wt.domain;

import lombok.Getter;

public enum AdviseType {
  Issue("Issue"), Close("Close"), Buy("Buy"), Sell("Sell");

  @Getter
  private final String adviseType;

  private AdviseType(String adviseType) {
    this.adviseType = adviseType;
  }
}
