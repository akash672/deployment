package com.wt.domain;

import akka.persistence.fsm.PersistentFSM.FSMState;
import lombok.Getter;

public enum UserFundState implements FSMState {
  UNSUBSCRIBED("Unsubscribed"),
  UNSUBSCRIBING("Unsubscribing"),
  MIRRORING("Mirroring"),
  ADDITIONALSUMMIRRORING("AdditionalSumMirroring"),
  ARCHIVED("Archived"),
  SUBSCRIBED("Subscribed"),
  MIRRORINGWITHOUTTIMEOUT("MirroringWithoutTimeout"),
  ADDITIONALSUMMIRRORINGWITHOUTTIMEOUT("AdditionalSumMirroringWithoutTimeout"),
  UNSUBSCRIBINGWITHOUTTIMEOUT("UnsubscribingWithoutTimeout"),
  WITHDRAWING("Withdrawing");

  @Getter private final String stateIdentifier;
  
  
  private UserFundState(String stateIdentifier) {
    this.stateIdentifier = stateIdentifier;
  }


  @Override
  public String identifier() {
    return stateIdentifier;
  }
}