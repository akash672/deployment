package com.wt.domain.write.events;

import lombok.Getter;
import lombok.ToString;

@ToString
public class WealthCodeUpdated extends UserEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private String wealthCode;
  
  public WealthCodeUpdated (String username, String wealthCode) {
    super(username);
    this.wealthCode = wealthCode;
  }
  
}