package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CalculateUserFundMaxDrawDownCommand {
  private String adviserUsername;
  private String fundName;
}