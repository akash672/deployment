package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.ToString;

@ToString
public class UserCloseAdviseOrderRejected extends UserAdviseActorTerminatedDueToFailure {

  private static final long serialVersionUID = 1L;

  public UserCloseAdviseOrderRejected(String email, String adviserUsername, String fundName, String adviseId, InvestingMode investingMode) {
    super(email, adviserUsername, fundName, investingMode, adviseId);
  }

}
