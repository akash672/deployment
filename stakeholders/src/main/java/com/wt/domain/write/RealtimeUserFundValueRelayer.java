package com.wt.domain.write;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.wt.domain.write.commands.GetCurrentUserFundValue;
import com.wt.domain.write.commands.GetPriceBasketFromTracker;
import com.wt.domain.write.commands.PriceSnapshotsFromPriceSource;
import com.wt.domain.write.commands.SendTheRequestorActorThePortfolioValue;
import com.wt.domain.write.events.ProcessingComplete;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;

public class RealtimeUserFundValueRelayer extends UntypedAbstractActor {
  private ActorRef destinationActor;
  private Set<Double> updatedUniqueAssetValues;
  private Map<String, Double> adviseIdToExecutedShares;
  private Map<String, String> adviseIdToTicker;
  private WDActorSelections wdActorSelections;

  public RealtimeUserFundValueRelayer(Map<String, Double> adviseIdToExecutedShares,
      Map<String, String> adviseIdToTicker, WDActorSelections wdActorSelections) {
    super();
    this.adviseIdToExecutedShares = adviseIdToExecutedShares;
    this.adviseIdToTicker = adviseIdToTicker;
    this.wdActorSelections = wdActorSelections;
    this.updatedUniqueAssetValues = new HashSet<Double>();
  }

  @Override
  public void onReceive(Object msg) {
    if (msg instanceof GetCurrentUserFundValue) {
      this.destinationActor = sender();
      wdActorSelections.activeInstrumentTracker().tell(new GetPriceBasketFromTracker(adviseIdToTicker.values()), self());
    } else if (msg instanceof PriceSnapshotsFromPriceSource) {
      PriceSnapshotsFromPriceSource priceMap = (PriceSnapshotsFromPriceSource) msg;

      adviseIdToExecutedShares.entrySet().stream().forEach(advise -> {
        updatedUniqueAssetValues
            .add(priceMap.getTickerToPriceWithPafMap().get(adviseIdToTicker.get(advise.getKey())).getPrice().getPrice()
                * advise.getValue());
      });

      returnAssetsValue(updatedUniqueAssetValues);
    } else if (msg instanceof SendTheRequestorActorThePortfolioValue) {
      SendTheRequestorActorThePortfolioValue request = (SendTheRequestorActorThePortfolioValue) msg;
      destinationActor.tell(request.getPortfolioValue(), self());
      self().tell(new ProcessingComplete(), self());
    } else if (msg instanceof ProcessingComplete) {
      context().stop(self());
    }
  }

  private void returnAssetsValue(Set<Double> assetsValue) {
    self().tell(new SendTheRequestorActorThePortfolioValue(assetsValue.stream().mapToDouble(asset -> asset).sum()),
        self());
  }

}
