package com.wt.domain.write.commands;

import lombok.ToString;

@ToString
public class SubscribeToFundEventStream extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  public SubscribeToFundEventStream(String username) {
    super(username);
  }

}
