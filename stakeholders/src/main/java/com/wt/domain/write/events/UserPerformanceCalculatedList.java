package com.wt.domain.write.events;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class UserPerformanceCalculatedList implements Serializable{

  private static final long serialVersionUID = 1L;
  
  @Getter
  private List<UserPerformanceCalculatedWithDate> map = new ArrayList<>();
  
  @Getter
  private String username;

}