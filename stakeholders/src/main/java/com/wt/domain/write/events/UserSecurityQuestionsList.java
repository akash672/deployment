package com.wt.domain.write.events;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class UserSecurityQuestionsList extends Done implements Serializable {

  private static final long serialVersionUID = 1L;
  private final Set<String> questions;

  @JsonCreator
  public UserSecurityQuestionsList(@JsonProperty("id") String id,
      @JsonProperty("message") String message,@JsonProperty("questions")  Set<String> questions) {
    super(id, message);
    this.questions = questions;
  }
  
  public UserSecurityQuestionsList(String email, Set<String> questions) {
    super(email, "");
    this.questions = questions;
  }

}
