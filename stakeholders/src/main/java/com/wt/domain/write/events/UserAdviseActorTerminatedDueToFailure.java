package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserAdviseActorTerminatedDueToFailure extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  @Getter private String adviseId;

  public UserAdviseActorTerminatedDueToFailure(String email, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
  }
  
  public static UserAdviseActorTerminatedDueToFailure withUninitializedInvestingModeInWatcherActor(String email, String adviserUsername, String fundName, String adviseId) {
    return new UserAdviseActorTerminatedDueToFailure(email, adviserUsername, fundName, null, adviseId);
  }

}
