package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class ValidateOTP extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private @Getter final String otp;

  @JsonCreator
  public ValidateOTP(@JsonProperty("otp") String otp, @JsonProperty("username") String username) {
    super(username);
    this.otp = otp;
  }
}
