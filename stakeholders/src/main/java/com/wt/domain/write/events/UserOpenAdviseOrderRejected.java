package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserFundState;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserOpenAdviseOrderRejected extends UserAdviseActorTerminatedDueToFailure {

  private static final long serialVersionUID = 1L;
  @Getter private double allocationValue;
  @Getter private IssueType issueType;
  @Getter private UserFundState userFundState;

  private UserOpenAdviseOrderRejected(String email, String adviserUsername, String fundName,
      String adviseId, double allocationValue, InvestingMode investingMode, IssueType issueType, UserFundState userFundState) {
    super(email, adviserUsername, fundName, investingMode, adviseId);
    this.allocationValue = allocationValue;
    this.issueType = issueType;
    this.userFundState = userFundState;
  }

  @Getter
  public static class UserOpenAdviseOrderRejectedBuilder extends UserAdviseActorTerminatedDueToFailure {
    private static final long serialVersionUID = 1L;
    private String email;
    private String adviserUsername; 
    private String fundName;
    private String adviseId; 
    private double allocationValue; 
    private InvestingMode investingMode; 
    private IssueType issueType;
    private UserFundState userFundState;
    
    public UserOpenAdviseOrderRejectedBuilder(String email, String adviserUsername, String fundName, String adviseId,
        double allocationValue, InvestingMode investingMode, IssueType issueType) {
      super(email, adviserUsername, fundName, investingMode, adviseId);
      this.email = email;
      this.adviserUsername = adviserUsername;
      this.fundName = fundName;
      this.adviseId = adviseId;
      this.allocationValue = allocationValue;
      this.investingMode = investingMode;
      this.issueType = issueType;
    }
    
    public UserOpenAdviseOrderRejectedBuilder withUserFundState(UserFundState userFundState) {
      this.userFundState = userFundState;
      return this;
    }
    
    public UserOpenAdviseOrderRejected build() {
      return new UserOpenAdviseOrderRejected(email, adviserUsername, fundName, adviseId, allocationValue, investingMode, issueType, userFundState);
    }
  }
}
