package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@JsonIgnoreProperties({"id"})
@ToString
public class UserSubscribedToFundResponse extends Done  {

  
  private static final long serialVersionUID = 1L;
  private boolean isSubscribed;
  private String username;
  private String adviserUsername;
  private InvestingMode investingMode;
  private String fundName;
  @JsonCreator
  public UserSubscribedToFundResponse(@JsonProperty("id") String id, @JsonProperty("username") String username,
      @JsonProperty("adviserUsername") String adviserUsername, @JsonProperty("fundName") String fundName,
      @JsonProperty("investingMode") InvestingMode investingMode, @JsonProperty("isSubscribed") boolean isSubscribed) {
    super(id, "");
    this.username = username;
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.isSubscribed = isSubscribed;
    this.investingMode = investingMode;
  }

}
