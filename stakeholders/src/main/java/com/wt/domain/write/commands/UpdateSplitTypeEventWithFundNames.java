package com.wt.domain.write.commands;

import java.util.List;

import com.wt.domain.CorporateEventType;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdateSplitTypeEventWithFundNames extends CorporateEventUpdateWithFundNames{
  
  private static final long serialVersionUID = 1L;
  private double adjustPaf;

  public UpdateSplitTypeEventWithFundNames(String wdId, CorporateEventType eventType, long eventDate, double adjustPaf,
      List<String> fundNames) {
    super(wdId, eventType, eventDate, fundNames);
    this.adjustPaf = adjustPaf;
  }

  
}
