package com.wt.domain.write.events;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class MarginRequested extends UserEvent {

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String brokerCompany;

  public MarginRequested(String clientCode, String username, String brokerCompany) {
    super(username);
    this.clientCode = clientCode;
    this.brokerCompany = brokerCompany;
  }

}
