package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.PriceWithPaf;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor
public class AdviseAtSpecifiedPrice implements Serializable {

  private static final long serialVersionUID = 1L;
  private PriceWithPaf entryPrice;
}
