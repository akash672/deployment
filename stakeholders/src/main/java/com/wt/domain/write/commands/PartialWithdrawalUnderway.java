package com.wt.domain.write.commands;

import static java.lang.Double.parseDouble;
import static java.lang.String.valueOf;

import java.util.Map;
import java.util.Optional;

import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;

import lombok.Getter;
import lombok.ToString;
@Getter
@ToString
public class PartialWithdrawalUnderway extends WithdrawalFromFund {
  private static final long serialVersionUID = 1L;
  private Optional<Purpose> purpose = Optional.empty();
  private String wealthCode;
  private String basketOrderId;
  private BrokerAuthInformation brokerAuthInformation;

  private PartialWithdrawalUnderway(String username, String adviserUsername, String fundName, double withdrawalAmount,
      String wealthCode, InvestingMode investingMode, Optional<Purpose> purpose, String userEmail, String basketOrderId,
      BrokerAuthInformation brokerAuthInformation) {
    super(username, adviserUsername, fundName, withdrawalAmount, investingMode);
    this.purpose = purpose;
    this.wealthCode = wealthCode;
    this.basketOrderId = basketOrderId;
    this.brokerAuthInformation = brokerAuthInformation;
  }

  @Getter
  public static class PartialWithdrawalUnderwayBuilder {
    private String username;
    private String adviserUsername;
    private String fundName;
    private String wealthCode;
    private InvestingMode investingMode;
    private Optional<Purpose> purpose;
    private double withdrawalAmount;
    private String basketOrderId;
    private BrokerAuthInformation brokerAuthInformation;

    public PartialWithdrawalUnderwayBuilder(String username, String adviserUsername, String fundName,
        InvestingMode investingMode) {
      this.username = username;
      this.adviserUsername = adviserUsername;
      this.fundName = fundName;
      this.investingMode = investingMode;
      this.withdrawalAmount = 0.;
      this.purpose = Optional.empty();
    }

    public PartialWithdrawalUnderwayBuilder withWithdrawalAmount(double withdrawalAmount) {
      this.withdrawalAmount = withdrawalAmount;
      return this;
    }

    public PartialWithdrawalUnderwayBuilder withWealthCode(String wealthCode) {
      this.wealthCode = wealthCode;
      return this;
    }
    
    public PartialWithdrawalUnderwayBuilder withPurpose(Purpose purpose) {
      this.purpose = Optional.of(purpose);
      return this;
    }

    public PartialWithdrawalUnderwayBuilder withBasketOrderId(String basketOrderId) {
      this.basketOrderId = basketOrderId;
      return this;
    }
    
    public PartialWithdrawalUnderwayBuilder withBrokerAuthInformation(BrokerAuthInformation brokerAuthInformation) {
      this.brokerAuthInformation = brokerAuthInformation;
      return this;
    }

    public PartialWithdrawalUnderway build() {
      return new PartialWithdrawalUnderway(username, adviserUsername, fundName, withdrawalAmount, wealthCode,
          investingMode, purpose, username, basketOrderId, brokerAuthInformation);
    }
  }
  
  public static PartialWithdrawalUnderway fromPostMarketCommand(Map<String, String> value) {
    return new PartialWithdrawalUnderwayBuilder(value.get("username"), value.get("adviserUsername"), value.get("fundName"), InvestingMode.valueOf(value.get("investingMode")))
                  .withWithdrawalAmount(parseDouble(valueOf(value.get("withdrawalAmount"))))
                  .withWealthCode(valueOf(value.get("wealthcode")))
                  .build();
  }
}
