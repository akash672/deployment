package com.wt.domain.write.events;

import com.wt.aws.cognito.configuration.CredentialsProviderType;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UserRegistered extends UserEvent {
  private static final long serialVersionUID = 1L;
  private String firstName;
  private String lastName;
  private String identityID;
  private CredentialsProviderType type;
  private String deviceToken;
  private long registerationTime;
  private String phoneNumber;
  private String isWealthCode;

  public UserRegistered(String username, String firstName, String lastName, String identityID, long timestamp,
      CredentialsProviderType type, String deviceToken, String email) {
    super(username);
    this.firstName = firstName;
    this.lastName = lastName;
    this.identityID = identityID;
    this.type = type;
    this.deviceToken = deviceToken;
    this.registerationTime = timestamp;
    this.phoneNumber = "Not Updated";
    this.isWealthCode = "false";
    super.email = email;
  }
  
  @Deprecated
  public UserRegistered(String username, String firstName, String lastName, String identityID, long timestamp,
      CredentialsProviderType type, String deviceToken) {
    super(username);
    this.firstName = firstName;
    this.lastName = lastName;
    this.identityID = identityID;
    this.type = type;
    this.deviceToken = deviceToken;
    this.registerationTime = timestamp;
    this.phoneNumber = "Not Updated";
    this.isWealthCode = "false";
    this.email = username;
  }

  public String getEmail() {
    return super.email;
  }
}
