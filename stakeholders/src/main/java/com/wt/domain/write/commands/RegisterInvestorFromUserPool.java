package com.wt.domain.write.commands;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.utils.CommonUtils;

import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterInvestorFromUserPool extends UnAuthenticatedUserCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  private String name;
  private String email;
  private String phoneNumber;

  @JsonCreator
  public RegisterInvestorFromUserPool(@NotNull @JsonProperty("username") String investorUsername,
      @NotNull @JsonProperty("name") String name, @JsonProperty("phoneNumber") String phoneNumber,
      @JsonProperty("email") String email) {
    super(investorUsername);
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.email = email;
  }

  public void validate() throws BadRequestException {
    if (getUsername() == null || getUsername().isEmpty())
      throw new BadRequestException("Username is required");
    if (name.isEmpty() || name == null)
      throw new BadRequestException("Investor Name is required");
    if ((phoneNumber == null || phoneNumber.isEmpty()) && (email == null || email.isEmpty()))
      throw new BadRequestException("Email and Phone number both cannot be empty, either one is required");
    if (!email.isEmpty() && email != null) {
      if (!CommonUtils.isValidEmail(email))
        throw new BadRequestException("Email is invalid");
    }
  }

  public static RegisterInvestorFromUserPool createCommand(GetUserRoleStatus command) {
    return new RegisterInvestorFromUserPool(command.getUsername(), command.getName(), command.getPhoneNumber(),
        command.getEmail());
  }

}
