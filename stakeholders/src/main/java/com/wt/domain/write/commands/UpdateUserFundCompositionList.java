package com.wt.domain.write.commands;

import com.wt.domain.UserAdviseCompositeKey;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdateUserFundCompositionList {

  private UserAdviseCompositeKey userAdviseCompositeKey;
  private double quantity;

  public UpdateUserFundCompositionList(UserAdviseCompositeKey userAdviseCompositeKey, double quantity) {
    this.userAdviseCompositeKey = userAdviseCompositeKey;
    this.quantity = quantity;
  }

}