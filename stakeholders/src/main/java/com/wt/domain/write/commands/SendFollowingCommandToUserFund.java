package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.utils.CommandPurpose;

import lombok.Getter;

@Getter
public class SendFollowingCommandToUserFund {

  private String username;
  private CommandPurpose commandPurpose;

  @JsonCreator
  public SendFollowingCommandToUserFund(@JsonProperty("username") String username,
      @JsonProperty("commandPurpose") CommandPurpose commandPurpose) {
    super();
    this.username = username;
    this.commandPurpose = commandPurpose;
  }

}
