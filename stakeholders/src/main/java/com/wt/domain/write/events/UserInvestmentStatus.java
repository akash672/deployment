package com.wt.domain.write.events;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.domain.InvestingMode;
import com.wt.utils.DoublesUtil;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
@Getter
public class UserInvestmentStatus extends Done {
  private static final long serialVersionUID = 1L;
  private List<UserFundSubscribedData> result = new ArrayList<UserFundSubscribedData>();

  public UserInvestmentStatus(String id, String message, String resultString, InvestingMode investingMode) {
    super(id, message);
    try {
      ObjectMapper mapper = new ObjectMapper();
      UserInvestmentResultsFromProjection unmarshalledResults = mapper.readValue(resultString,
          UserInvestmentResultsFromProjection.class);
      int dataSize = unmarshalledResults.getFundName().size();
      for (int index = 0; index < dataSize; index++) {
        if (unmarshalledResults.getIsSubscribed().get(index)) {
          UserFundSubscribedData fund = new UserFundSubscribedData(unmarshalledResults.getFundName().get(index),
              unmarshalledResults.getAdviserUsername().get(index), investingMode,
              unmarshalledResults.getTotalInvested().get(index), DoublesUtil.round(unmarshalledResults.getTotalWithdraw().get(index)),
              unmarshalledResults.getIsSubscribed().get(index) + "");
          result.add(fund);
        }
      }
    } catch (Exception e) {
      log.warn(e);
      this.result = new ArrayList<UserFundSubscribedData>();
      // it can be blank but cannot be null else Json will be wrong
    }
  }

  @JsonCreator
  public UserInvestmentStatus(@JsonProperty("id") String id, @JsonProperty("message") String message,
      @JsonProperty("result") List<UserFundSubscribedData> result) {
    super(id, message);
    this.result = result;
  }

}
