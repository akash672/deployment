package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserAdviseOrderHasBeenSuccessfullySent extends UserAdviseEvent {
  
  private static final long serialVersionUID = 1L;
  @Getter private String clientOrderId;

  public UserAdviseOrderHasBeenSuccessfullySent(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String clientOrderId, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.clientOrderId = clientOrderId;
  }
  
  
}
