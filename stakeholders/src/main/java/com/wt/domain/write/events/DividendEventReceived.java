package com.wt.domain.write.events;

import com.wt.domain.CorporateEventType;
import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class DividendEventReceived extends UserAdviseEvent {


  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;
  private double dividendValue;
  private CorporateEventType eventType;
  private long exDate;
  private String token;
  

  public DividendEventReceived(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, IssueType issueType,UserAdviseState userAdviseState, double dividendValue,
      CorporateEventType eventType, long exDate, String token) {
    super(username, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
    this.dividendValue = dividendValue;
    this.eventType = eventType;
    this.exDate = exDate;
    this.token = token;
  }
 


}
