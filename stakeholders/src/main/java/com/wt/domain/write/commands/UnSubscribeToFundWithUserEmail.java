package com.wt.domain.write.commands;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UnSubscribeToFundWithUserEmail extends UnsubscribeToFund {

  private static final long serialVersionUID = 1L;
  private String userEmail;
  private String basketOrderId;
  private BrokerAuthInformation brokerAuthInformation;
  
  public UnSubscribeToFundWithUserEmail(String adviserName, String fundName,
      InvestingMode investingMode, String username, String userEmail, String basketOrderId,
      BrokerAuthInformation brokerAuthInformation) {
    super(adviserName, fundName, username, investingMode);
    this.userEmail = userEmail;
    this.basketOrderId = basketOrderId;
    this.brokerAuthInformation = brokerAuthInformation;
  }
  
}
