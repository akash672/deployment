package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GetWalletMoney extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private InvestingMode investingMode;

  @JsonCreator
  public GetWalletMoney(@JsonProperty("username") String username,
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super(username);
    this.investingMode = investingMode;
  }

}
