package com.wt.domain.write.events;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class SpecialCircumstancesBuyEvent extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private String token;
  private String symbol;
  private int quantity;
  private double price;
  private String adviseId;
  private String identityId;
  private OrderSide orderSide;
  private String clientCode;
  private BrokerName brokerName;

  @Deprecated
  public SpecialCircumstancesBuyEvent(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String token, String symbol, int quantity, double price, String adviseId,
      String identityId, OrderSide orderSide, String clientCode, BrokerName brokerName) {
    this(username, adviserUsername, fundName, investingMode, token, symbol, quantity, price, adviseId, orderSide,
        clientCode, brokerName);
  }

  public SpecialCircumstancesBuyEvent(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String token, String symbol, int quantity, double price, String adviseId,
      OrderSide orderSide, String clientCode, BrokerName brokerName) {
    super(username, adviserUsername, fundName, investingMode);
    this.token = token;
    this.symbol = symbol;
    this.quantity = quantity;
    this.price = price;
    this.adviseId = adviseId;
    this.identityId = username;
    this.orderSide = orderSide;
    this.clientCode = clientCode;
    this.brokerName = brokerName;
  }
}
