package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateKYCDetailsInRootActor implements Serializable {
  private static final long serialVersionUID = 1L;

  private final String username;
  private final String clientCode;
  private final String brokerCompany;

  public UpdateKYCDetailsInRootActor(String username, String clientCode, String brokerCompany) {
    this.username = username;
    this.clientCode = clientCode;
    this.brokerCompany = brokerCompany;
  }
}
