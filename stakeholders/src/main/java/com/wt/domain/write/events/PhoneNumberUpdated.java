package com.wt.domain.write.events;

import lombok.Getter;
import lombok.ToString;

@ToString
public class PhoneNumberUpdated extends UserEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private String phonenumber;
  
  public PhoneNumberUpdated (String username, String phonenumber) {
    super(username);
    this.phonenumber = phonenumber;
  }
  
}