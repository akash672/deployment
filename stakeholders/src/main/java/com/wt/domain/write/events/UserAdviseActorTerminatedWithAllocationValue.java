package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserAdviseActorTerminatedWithAllocationValue extends UserAdviseActorTerminatedDueToFailure {

  private static final long serialVersionUID = 1L;
  @Getter private double allocationValue;

  public UserAdviseActorTerminatedWithAllocationValue(String email, String adviserUsername, String fundName,
      String adviseId, double allocationValue, InvestingMode investingMode) {
    super(email, adviserUsername, fundName, investingMode, adviseId);
    this.allocationValue = allocationValue;
    
  }

}
