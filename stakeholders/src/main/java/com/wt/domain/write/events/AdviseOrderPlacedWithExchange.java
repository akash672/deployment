package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviseOrderPlacedWithExchange extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;
  private String brokerOrderId;
  private String exchangeOrderId;
  private String ctclId;
  private String token;
  private int quantity;
  private OrderSide side;

  public AdviseOrderPlacedWithExchange(String email, String fundName, String adviserName, InvestingMode investingMode, String adviseId, UserAdviseState userAdviseState, String exchangeOrderId, String brokerOrderId, String ctclId, String token, OrderSide side, int quantity, IssueType issueType) {
    super(email, adviserName, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
    this.exchangeOrderId = exchangeOrderId;
    this.brokerOrderId = brokerOrderId;
    this.ctclId = ctclId;
    this.token = token;
    this.side = side;
    this.quantity = quantity;
  }
  
}
