package com.wt.domain.write.events;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class UserAdviseDetailsMapMarshaller implements DynamoDBTypeConverter<String, Map<String, UserAdviseDetails>> {
  @Override
  public String convert(Map<String, UserAdviseDetails> object) {
    return new Gson().toJson(object);
  }

  @Override
  public Map<String, UserAdviseDetails> unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Map<String, UserAdviseDetails>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }

}
