package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class SellOrderDetails implements Serializable {
  
  private static final long serialVersionUID = 1L;
  private String token;
  private double avgPrice;
  private int quantity;
  private double absoluteAllocation;
  private double allocationValue;
  
  
}
