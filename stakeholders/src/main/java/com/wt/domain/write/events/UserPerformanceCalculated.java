package com.wt.domain.write.events;

import static com.wt.utils.DoublesUtil.isANonZeroValidDouble;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.DoublesUtil.validDouble;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class UserPerformanceCalculated extends UserEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  @Expose
  private final double currentValue;
  @Getter
  @Expose
  private double returnPct;
  @Getter
  private String cognitoIdentityId;
  @Getter
  @Expose
  private final double pnl;
  @Getter
  @Expose
  private final double realizedPnL;
  @Getter
  @Expose
  private final double unrealizedPnL;
  @Getter
  @Expose
  private List<UserFundPerformanceCalculated> allFundPerformance;
  @Getter
  @Expose
  private final InvestingMode investingMode;

  @Deprecated
  public UserPerformanceCalculated(String username, double currentValue, double returnPct, String cognitoIdentityId,
      double realizedPnL, double unrealizedPnL, double pnl, InvestingMode investingMode,
      List<UserFundPerformanceCalculated> allFundPerformance) {
    this(username, currentValue, returnPct, realizedPnL, unrealizedPnL, pnl, investingMode, allFundPerformance);
  }

  public UserPerformanceCalculated(String username, double currentValue, double returnPct, double realizedPnL,
      double unrealizedPnL, double pnl, InvestingMode investingMode,
      List<UserFundPerformanceCalculated> allFundPerformance) {
    super(username);
    this.currentValue = currentValue;
    this.returnPct = returnPct;
    this.cognitoIdentityId = username;
    this.pnl = pnl;
    this.realizedPnL = realizedPnL;
    this.unrealizedPnL = unrealizedPnL;
    this.allFundPerformance = allFundPerformance;
    this.investingMode = investingMode;
  }

  public static class UserPerformanceCalculatedBuilder {
    private List<UserFundPerformanceCalculated> all = new ArrayList<>();
    private String username;
    private String cognitoIdentityId;
    private InvestingMode investingMode;
    
    public UserPerformanceCalculatedBuilder(String username, InvestingMode investingMode) {
      super();
      this.username = username;
      this.cognitoIdentityId = username;
      this.investingMode = investingMode;
    }

    public UserPerformanceCalculatedBuilder withAdvisePerformance(
        UserFundPerformanceCalculated fundPerformanceCalculated) {
      all.add(fundPerformanceCalculated);
      return this;
    }

    public UserPerformanceCalculatedBuilder withUserPerformance(
        Collection<UserFundPerformanceCalculated> allFundPerformanceCalculated) {
      all.addAll(allFundPerformanceCalculated);
      return this;
    }

    public UserPerformanceCalculated build() {
      double currentValue = 0D;
      double pnl = 0D;
      double realizedPnL = 0D;
      double unRealizedPnL = 0D;
      double totalInvestmentAcrossFunds = all.stream()
          .mapToDouble(userFundPerformanceCalculated -> userFundPerformanceCalculated.getTotalInvestment()).sum();
      for (UserFundPerformanceCalculated fundPerformanceCalculated : all) {
        currentValue += fundPerformanceCalculated.getCurrentValue();
        pnl += fundPerformanceCalculated.getPnl();
        realizedPnL += fundPerformanceCalculated.getRealizedPnL();
        unRealizedPnL += fundPerformanceCalculated.getUnrealizedPnL();
      }
      double returnPct = isANonZeroValidDouble(totalInvestmentAcrossFunds) ? (pnl / totalInvestmentAcrossFunds) * 100.
          : 0D;
      UserPerformanceCalculated userPerformanceCalculated = new UserPerformanceCalculated(username,
          round(validDouble(currentValue)), round(validDouble(returnPct)), cognitoIdentityId,
          round(validDouble(realizedPnL)), round(validDouble(unRealizedPnL)), round(validDouble(pnl)), investingMode,
          all);
      return userPerformanceCalculated;
    }
  }

}
