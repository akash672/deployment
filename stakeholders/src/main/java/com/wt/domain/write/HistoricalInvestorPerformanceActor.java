package com.wt.domain.write;

import static com.google.common.collect.Multimaps.index;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimap;
import com.wt.aws.cognito.exception.InternalErrorException;
import com.wt.domain.FundWithAdviserAndInvestingMode;
import com.wt.domain.InvestingMode;
import com.wt.domain.MaxDrawdown;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.User;
import com.wt.domain.UserAdvise;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserFund;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.UserFundCompositeKeyWithDate;
import com.wt.domain.UserFundPerformance;
import com.wt.domain.UserFundWithDate;
import com.wt.domain.write.commands.CalculateUserFundMaxDrawDownCommand;
import com.wt.domain.write.commands.GetHistoricalPriceEventsByInstrument;
import com.wt.domain.write.commands.HistoricalPriceEventsList;
import com.wt.domain.write.commands.UserHistoricalPerformanceWithEmail;
import com.wt.domain.write.events.AdviseOrderRejected;
import com.wt.domain.write.events.BrokerNameReceived;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.ExecutedAdviseDetailsUpdated;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.InsufficientCapital;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.ProcessingComplete;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceWithDate;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserEvent;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.domain.write.events.UserFundDrawDownCalculated;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundMaxDrawdownCalculated;
import com.wt.domain.write.events.UserFundPerformanceCalculated;
import com.wt.domain.write.events.UserFundPerformanceCalculated.UserFundPerformanceCalculatedBuilder;
import com.wt.domain.write.events.UserPerformanceCalculated;
import com.wt.domain.write.events.UserPerformanceCalculated.UserPerformanceCalculatedBuilder;
import com.wt.domain.write.events.UserPerformanceCalculatedList;
import com.wt.domain.write.events.UserPerformanceCalculatedWithDate;
import com.wt.domain.write.events.UserSubscribedToFund;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.UntypedAbstractActor;
import akka.stream.ActorMaterializer;
import lombok.extern.log4j.Log4j;

@Log4j
public class HistoricalInvestorPerformanceActor extends UntypedAbstractActor{

  private ActorMaterializer materializer;
  private JournalProvider journalProvider;
  private Map<UserFundCompositeKey, UserFund> userFunds;
  private User user;
  private Map<UserAdviseCompositeKey, UserAdvise> userAdvises;
  private Map<String, Integer> drawDownConfig;
  private String persistenceId;
  private Map<String, UserAdviseAbsolutePerformanceWithDate> allAdvisePerformanceWithDate;
  private Map<String, Boolean> advisesContainedInFunds;
  private boolean userPerformanceCalculationRunning = false;
  private long performanceCalculatedTill;
  private InvestingMode investingMode;
  private WDActorSelections actorSelections;
  private int priceListEventsToExpect = 0;
  private int priceListEventsReceived = 0;

  public HistoricalInvestorPerformanceActor(String username, String persistenceId, ActorMaterializer materializer,
      JournalProvider journalProvider, Map<String, Integer> drawDownConfig, InvestingMode investingMode, WDActorSelections actorSelections) {
    super();
    this.user = new User();
    user.setUsername(username);
    this.journalProvider = journalProvider;
    this.drawDownConfig = drawDownConfig;
    this.materializer = materializer;
    this.persistenceId = persistenceId;
    this.userFunds = new HashMap<>();
    this.userAdvises = new HashMap<>();
    this.allAdvisePerformanceWithDate = new HashMap<>();
    this.advisesContainedInFunds = new HashMap<>();
    this.investingMode = investingMode;
    this.actorSelections = actorSelections;
  }

  @Override
  public void onReceive(Object msg) throws InterruptedException, ExecutionException, ParseException {
    if (msg instanceof UserHistoricalPerformanceWithEmail) {
      startHistoricalUserPerformance((UserHistoricalPerformanceWithEmail) msg);
    } else if (msg instanceof ProcessingComplete) {
      context().stop(self());
    } else if (msg instanceof CalculateUserFundMaxDrawDownCommand) {
      CalculateUserFundMaxDrawDownCommand calculateMaxDrawDownCommand = (CalculateUserFundMaxDrawDownCommand) msg;
      calculateMaxDrawDown(calculateMaxDrawDownCommand.getAdviserUsername(), calculateMaxDrawDownCommand.getFundName());
    } else if (msg instanceof HistoricalPriceEventsList) {
      List<PriceWithPaf> priceEvents = ((HistoricalPriceEventsList) msg).getListOfPriceEvent();
      UserAdvise ofUserAdvise = getReferenceOfUserAdvise(((HistoricalPriceEventsList) msg).getKey());
      updateAllAdvisePerformanceWithDate(ofUserAdvise,priceEvents);
    }
  }

  private void updateAllAdvisePerformanceWithDate(UserAdvise userAdvise,List<PriceWithPaf> priceEvents)
      throws InterruptedException, ExecutionException {

    List<UserAdviseAbsolutePerformanceWithDate> historicalPerformance = userAdvise
        .historicalPerformance(priceEvents, materializer);
    historicalPerformance.stream().forEach(perf -> {
      perf = (UserAdviseAbsolutePerformanceWithDate) perf;
      allAdvisePerformanceWithDate.put(perf.getAdviseId() + CompositeKeySeparator + perf.getDate(), perf);
    });

    priceListEventsReceived++;
    if (priceListEventsToExpect == priceListEventsReceived)
      sendCurrentPerformance();
  }

  private UserAdvise getReferenceOfUserAdvise(String userAdviseIdentifier) {
    String [] userAdviseIdentifiers=userAdviseIdentifier.split(CompositeKeySeparator);
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(userAdviseIdentifiers[0],
                                                                               userAdviseIdentifiers[1],
                                                                               userAdviseIdentifiers[2],
                                                                               userAdviseIdentifiers[3], 
                                                                               InvestingMode.valueOf(userAdviseIdentifiers[4]));
    return userAdvises.get(userAdviseCompositeKey);

  }

  private void sendMaxDrawDown(UserFundCompositeKey key, List<UserFundPerformance> performanceTimeSeries, String adviserUsername, String fundName) {
    
    if (performanceTimeSeries.isEmpty()) {
      sender().tell(
          new UserFundDrawDownCalculated(key.getFund(), "User Fund MaxDrawdown Error: No Performance calculated"),
          self());
      return;
    }

    long now = DateUtils.currentTimeInMillis();
    List<MaxDrawdown> maxDrawdowns = drawDownConfig.entrySet().stream().map(entry -> MaxDrawdown.from(entry.getKey(),
        DateUtils.minusMonths(now, drawDownConfig.get(entry.getKey())), performanceTimeSeries)).collect(toList());
    UserFundMaxDrawdownCalculated userFundMaxDrawDownCalculated = new UserFundMaxDrawdownCalculated(key.getUsername(), key.getFund(), key.getAdviser(), key.getInvestingMode(), maxDrawdowns);
    if (isUserFundKeyContains(key, adviserUsername, fundName))
      sender().tell(userFundMaxDrawDownCalculated, self());
  }

  private void calculateMaxDrawDown(String adviserUsername, String fundName) {
    List<Object> allUserEvents = journalProvider
        .fetchFilteredEventsByPersistenceId(user.getUsername(), event -> event.event() instanceof UserEvent).stream()
        .filter(event -> (event instanceof UserSubscribedToFund)).collect(Collectors.toList());
    allUserEvents.stream().forEach(event -> {
      UserSubscribedToFund userEvent = (UserSubscribedToFund) event;
      UserFundCompositeKey key = new UserFundCompositeKey(user.getUsername(), userEvent.getFundName(),
          userEvent.getAdviserUsername(), userEvent.getInvestingMode());
          List<UserFundPerformance> dataSet = journalProvider
                                              .fetchFilteredEventsByPersistenceId(key.persistenceId(),
                                                  (userFundEvent) -> userFundEvent.event() instanceof UserFundPerformanceCalculated)
                                              .stream().map(obj -> (UserFundPerformanceCalculated) obj)
                                              .map(userFundPerformanceEvent -> new UserFundPerformance(user.getUsername(),
                                                              userFundPerformanceEvent.getFundName(), 
                                                              userFundPerformanceEvent.getAdviserUsername(), 
                                                              userFundPerformanceEvent.getTotalInvestment(),
                                                              userFundPerformanceEvent.getCurrentValue(),
                                                              userFundPerformanceEvent.getRealizedPnL(), 
                                                              userFundPerformanceEvent.getUnrealizedPnL(), 
                                                              userFundPerformanceEvent.getReturnPct(), 
                                                              userFundPerformanceEvent.getDate(), 
                                                              userFundPerformanceEvent.getPnl(), 
                                                              userFundPerformanceEvent.getInvestingMode())).collect(toList());
      
      List<UserFundPerformance> performanceTimeSeries = new ArrayList<>(dataSet);
      performanceTimeSeries.sort(UserFundPerformance.UserFundPerformanceDatewise);
      sendMaxDrawDown(key, performanceTimeSeries, adviserUsername, fundName);
    });
    self().tell(new ProcessingComplete(), self());
  }

  private void startHistoricalUserPerformance(UserHistoricalPerformanceWithEmail userCommand) {
    log.info("Starting historical performance for  " + persistenceId);

    if (!isFundPerformanceStreamRunning()) {
      userPerformanceCalculationRunning = true;
      fetchAllEventsAndCalculatePerformance(userCommand);
    }
  }

  private void fetchAllEventsAndCalculatePerformance(UserHistoricalPerformanceWithEmail userCommand) {
    List<Object> allUserEvents = journalProvider.fetchFilteredEventsByPersistenceId(userCommand.getUsername(),
        event -> event.event() instanceof UserEvent);
    User localUser = new User();
    allUserEvents.stream().forEach(event -> {
      try {
        localUser.update((UserEvent) event);
      } catch (Exception e) {
        log.error(e);
      }
    });
    if (userCommand.getInvestingMode() == InvestingMode.VIRTUAL)
      performanceCalculatedTill = localUser.getPerformanceVirtualCalculatedTill();
    if (userCommand.getInvestingMode() == InvestingMode.REAL)
      performanceCalculatedTill = localUser.getPerformanceRealCalculatedTill();
    
    if (isEmpty(localUser.getAdvisorToFunds(), userCommand.getInvestingMode())) {
      actorSelections.userRootActor().tell(new UserPerformanceCalculatedList(new ArrayList<>(), user.getUsername()), self());
      userPerformanceCalculationRunning = false;
      return;
    }
    
    allUserEvents.stream().forEach(event -> {
      try {
        updateUser((UserEvent) event);
      } catch (Exception e) {
        log.error(e);
      }
    });
    checkAllFundWithoutAdvises();
  }

  private void sendCurrentPerformance() {
    try {
      sendPerformance();
    } catch (Exception e) {
      log.error("Error ", e);
    }
  }

  private void sendPerformance() {
    send(userPerformanceFrom(allFundPerformanceFrom(allAdvisePerformance())));
  }

  private void send(Map<Long, UserPerformanceCalculated> userPerformance) {
    List<UserPerformanceCalculatedWithDate> listOfUserPerformancesWithDate = userPerformance.entrySet().stream()
        .map(key -> new UserPerformanceCalculatedWithDate(key.getValue().getUsername(),
            key.getValue().getCurrentValue(), key.getValue().getReturnPct(),
            key.getValue().getRealizedPnL(), key.getValue().getUnrealizedPnL(), key.getValue().getPnl(),
            key.getValue().getAllFundPerformance(), key.getKey(), key.getValue().getInvestingMode()))
        .collect(Collectors.toList());
    actorSelections.userRootActor().tell(new UserPerformanceCalculatedList(listOfUserPerformancesWithDate, user.getUsername()), self());
    userPerformanceCalculationRunning = false;
  }

  private Map<Long, UserPerformanceCalculated> userPerformanceFrom(
      Map<UserFundCompositeKeyWithDate, UserFundPerformanceCalculated> allFundPerformance) {
    Map<Long, UserPerformanceCalculated> userPerformancesByDates = new HashMap<>();
    Map<Long, List<UserFundPerformanceCalculated>> userFundPerformanceMap = new HashMap<>();
    allFundPerformance.entrySet().stream().forEach(entry -> {
      if (userFundPerformanceMap.containsKey(entry.getKey().getDate()))
        userFundPerformanceMap.get(entry.getKey().getDate()).add(entry.getValue());
      else
        userFundPerformanceMap.put(entry.getKey().getDate(), new ArrayList<>(Arrays.asList(entry.getValue())));
    });
    userFundPerformanceMap.entrySet().stream().forEach(allFundPerformanceEntry -> {
      userPerformancesByDates.put(allFundPerformanceEntry.getKey(),
          new UserPerformanceCalculatedBuilder(user.getUsername(), investingMode)
              .withUserPerformance(allFundPerformanceEntry.getValue()).build());
      
    });
    
    return userPerformancesByDates;
  }

  private Map<UserFundCompositeKeyWithDate, UserFundPerformanceCalculated> allFundPerformanceFrom(
      Multimap<UserFundCompositeKey, UserAdviseAbsolutePerformanceWithDate> multimap) {

    Map<UserFundCompositeKeyWithDate, UserFundPerformanceCalculated> datewiseFundPerformances = new HashMap<>();
    Map<UserFundWithDate, List<UserFundPerformanceCalculated>> userFundPerformanceTimeSeries = new HashMap<>();
    for (UserFundCompositeKey key : userFunds.keySet()) {
      UserFund userFund = userFunds.get(key);
      Collection<UserAdviseAbsolutePerformanceWithDate> userAdvisePerformanceList = (multimap.containsKey(key))
          ? multimap.get(key) : new ArrayList<>();
      Map<Long, List<UserAdviseAbsolutePerformanceWithDate>> datewiseAdvisePerformance = userAdvisePerformanceList
          .stream().collect(Collectors.groupingBy(UserAdviseAbsolutePerformanceWithDate::getDate, Collectors.toList()));
      datewiseAdvisePerformance.entrySet().stream().forEach(e -> {
        UserFundPerformanceCalculated fundPerformanceCalculatedWithDate = new UserFundPerformanceCalculatedBuilder(
            key.getUsername(), key.getAdviser(), key.getFund(), key.getInvestingMode(),
            userFund.getTotalInvestment(), userFund.getCashComponent(), userFund.getWithdrawal(), e.getKey())
                .withAdvisePerformanceAndDate(e.getValue()).buildHistorical();
        datewiseFundPerformances.put(new UserFundCompositeKeyWithDate(userFund.getUsername(),
            userFund.getUserFundCompositeKey().getFund(), userFund.getUserFundCompositeKey().getAdviser(), userFund.getUserFundCompositeKey().getInvestingMode(), e.getKey()),
            fundPerformanceCalculatedWithDate);
        UserFundWithDate userFundWithDate = new UserFundWithDate(userFund, e.getKey());
        userFundPerformanceTimeSeries.computeIfAbsent(userFundWithDate, k -> new ArrayList<>())
            .add(fundPerformanceCalculatedWithDate);
      });
    }
    
    return datewiseFundPerformances;
  }

  private Multimap<UserFundCompositeKey, UserAdviseAbsolutePerformanceWithDate> allAdvisePerformance() {
    ImmutableListMultimap<UserFundCompositeKey, UserAdviseAbsolutePerformanceWithDate> userFundToAdvisePerformanceMultimap = index(
        allAdvisePerformanceWithDate.values(),
        advisePerf -> new UserFundCompositeKey(advisePerf.getUsername(),
            advisePerf.getFundName(), advisePerf.getAdviserUsername(), advisePerf.getInvestingMode()));
    return userFundToAdvisePerformanceMultimap;
  }

  private void updateUser(UserEvent userEvent) throws IllegalArgumentException, InternalErrorException, ParseException {
    user.update(userEvent);
    if (userEvent instanceof UserSubscribedToFund) {
      if (((UserSubscribedToFund) userEvent).getInvestingMode() == this.investingMode)
        startHistoricalFundPerformance((UserSubscribedToFund) userEvent);
    }
  }

  protected void startHistoricalFundPerformance(UserSubscribedToFund userSubscribeEvent) {
    UserFundCompositeKey key = new UserFundCompositeKey(user.getUsername(), userSubscribeEvent.getFundName(),
        userSubscribeEvent.getAdviserUsername(), userSubscribeEvent.getInvestingMode());
    userFunds.put(key, new UserFund());
    List<Object> allUserFundEvents = journalProvider.fetchFilteredEventsByPersistenceId(key.persistenceId(),
        event -> event.event() instanceof UserFundEvent);
    allUserFundEvents.stream().forEach(userFundEvent -> {
      try {
        updateUserFund((UserFundEvent) userFundEvent);
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }
    });
  }

  private void updateUserFund(UserFundEvent userFundEvent) throws InterruptedException, ExecutionException {
    UserFundCompositeKey key = new UserFundCompositeKey(userFundEvent.getUsername(), userFundEvent.getFundName(),
        userFundEvent.getAdviserUsername(),userFundEvent.getInvestingMode());
    UserFund userFund = userFunds.get(key);
    if(userFund == null)
      return;
    if(advisesContainedInFunds.get(userFundEvent.getFundName()) == null)
      advisesContainedInFunds.put(userFundEvent.getFundName(), false);
    userFund.update((UserFundEvent) userFundEvent);
    if (userFundEvent instanceof ExecutedAdviseDetailsUpdated) {
      ExecutedAdviseDetailsUpdated executedAdviseDetailsUpdated = (ExecutedAdviseDetailsUpdated) userFundEvent;
      UserAdviseCompositeKey adviseKey = new UserAdviseCompositeKey(executedAdviseDetailsUpdated.getAdviseId(),
          executedAdviseDetailsUpdated.getUsername(), executedAdviseDetailsUpdated.getFundName(),
          executedAdviseDetailsUpdated.getAdviserUsername(), executedAdviseDetailsUpdated.getInvestingMode());
      advisesContainedInFunds.put(userFundEvent.getFundName(), true);
      keepAdviseUpdated(adviseKey);
    } else if (userFundEvent instanceof FundAdviceTradeExecuted) {
      FundAdviceTradeExecuted fundAdviceTradeExecuted = (FundAdviceTradeExecuted) userFundEvent;
      UserAdviseCompositeKey adviseKey = new UserAdviseCompositeKey(fundAdviceTradeExecuted.getAdviseId(),
          fundAdviceTradeExecuted.getUsername(), fundAdviceTradeExecuted.getFundName(),
          fundAdviceTradeExecuted.getAdviserUsername(), fundAdviceTradeExecuted.getInvestingMode());
      advisesContainedInFunds.put(userFundEvent.getFundName(), true);
      keepAdviseUpdated(adviseKey);

    }
  }

  private void keepAdviseUpdated(UserAdviseCompositeKey key) throws InterruptedException, ExecutionException {
    List<Object> allAdviseEvents = journalProvider.fetchFilteredEventsByPersistenceId(key.persistenceId(),
        event -> ((event.event() instanceof UserAdviseEvent)));
    allAdviseEvents.stream().filter(adviseEvent -> !(adviseEvent instanceof CurrentUserAdviseFlushedDueToExit))
        .filter(adviseEvent -> !(adviseEvent instanceof BrokerNameReceived))
        .filter(adviseEvent -> !(adviseEvent instanceof InsufficientCapital))
        .filter(adviseEvent -> !(adviseEvent instanceof AdviseOrderRejected))
        .forEach(adviseEvent -> {
          try {
            IssueType issueType = (((UserAdviseEvent) adviseEvent).getIssueType()== null) ? Equity : ((UserAdviseEvent) adviseEvent).getIssueType();
			userAdvises.putIfAbsent(key, issueType.userAdvise());
            updateUserAdvise((UserAdviseEvent) adviseEvent);
          } catch (IOException | InterruptedException | ExecutionException e) {
            log.info("Failed to read advise events from the journal. Unable to update UserAdvise");
            e.printStackTrace();
          }

        });
    getPriceList(userAdvises.get(key).getToken(), key);
  }

  private void getPriceList(String token, UserAdviseCompositeKey key) {
    String responseIdentifier = key.getAdviseId() + CompositeKeySeparator + key.getUsername() + CompositeKeySeparator + key.getFund() + CompositeKeySeparator + key.getAdviser()+ CompositeKeySeparator + key.getInvestingMode().getInvestingMode();
    GetHistoricalPriceEventsByInstrument getPriceListEvent = new GetHistoricalPriceEventsByInstrument(token,
        performanceCalculatedTill, responseIdentifier);
    
    priceListEventsToExpect++;
    actorSelections.universeRootActor().tell(getPriceListEvent, getSelf());
  }

  private void updateUserAdvise(UserAdviseEvent event) throws IOException, InterruptedException, ExecutionException {
    UserAdviseCompositeKey key = new UserAdviseCompositeKey(
                                     event.getAdviseId(), 
                                     event.getUsername(), 
                                     event.getFundName(), 
                                     event.getAdviserUsername(), 
                                     event.getInvestingMode());
    UserAdvise userAdvise = userAdvises.get(key);
    if(event instanceof UserFundAdviceIssuedWithUserEmail) {
      userAdvise.update(new CurrentUserAdviseFlushedDueToExit(
                            event.getUsername(), 
                            event.getAdviserUsername(), 
                            event.getFundName(), 
                            event.getInvestingMode(), event.getAdviseId(),
                            event.getIssueType()));
    }
    userAdvise.update(event);
  }
  
  private void checkAllFundWithoutAdvises() {
    for(UserFundCompositeKey key : userFunds.keySet())
      if(advisesContainedInFunds.get(key.getFund()))
        return;
    sendPerformance();
  }
  
  private boolean isEmpty(List<FundWithAdviserAndInvestingMode> advisorToFunds, InvestingMode investingMode) {
    for( FundWithAdviserAndInvestingMode fundWithAdviserAndInvestingMode : advisorToFunds) {
      if(fundWithAdviserAndInvestingMode.getInvestingMode().equals(investingMode.getInvestingMode()))
        return false;
    }
    return true;
  }

  private boolean isUserFundKeyContains(UserFundCompositeKey key, String adviserUsername, String fundName) {
    return key.getAdviser().equals(adviserUsername) && key.getFund().equals(fundName) && key.getInvestingMode().equals(investingMode);
  }
  
  private boolean isFundPerformanceStreamRunning() {
    return userPerformanceCalculationRunning;
  }
  
}