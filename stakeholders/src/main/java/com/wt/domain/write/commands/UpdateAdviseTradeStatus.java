package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.write.events.IssueType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class UpdateAdviseTradeStatus implements Serializable {

  private static final long serialVersionUID = 1L;
  private IssueType issueType;
}
