package com.wt.domain.write.commands;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class SubmitSecurityAnswers extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  @Getter
  private Map<String, String> answers;

  @JsonCreator
  public SubmitSecurityAnswers(@JsonProperty("username") String username,
      @JsonProperty("answers") Map<String, String> answers) {
    super(username);
    this.answers = answers;
  }
  
  public void validate() {
    if (getAnswers().size() != 2)
      throw new IllegalArgumentException("Security Answers could not be added due to illegal no of Arguments");
  }

}
