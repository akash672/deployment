package com.wt.domain.write.events;

import lombok.Getter;

public class UserLoggedIn extends UserEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private String deviceToken;
  
  public UserLoggedIn(String username, String deviceToken) {
    super(username);
    this.deviceToken = deviceToken;
  }

}
