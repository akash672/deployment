package com.wt.domain.write.commands;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter

public class SubmitUserAnswers extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private String eventName;

  private Map<String, String> answers;

  @JsonCreator
  public SubmitUserAnswers(@JsonProperty("username") String username, @JsonProperty("eventName") String eventName,
      @JsonProperty("answers")  Map<String, String> answers) {
    super(username);
    this.eventName = eventName;
    this.answers = answers;
  }

}
