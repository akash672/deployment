package com.wt.domain.write.events;

import java.util.List;

import com.wt.domain.InvestingMode;
import com.wt.domain.MaxDrawdown;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserFundMaxDrawdownCalculated extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private List<MaxDrawdown> drawDowns;

  public UserFundMaxDrawdownCalculated(String userEmail, String fundName, String adviserName, InvestingMode investingMode, List<MaxDrawdown> drawDowns) {
    super(userEmail, adviserName, fundName, investingMode);
    this.drawDowns = drawDowns;
  }

}