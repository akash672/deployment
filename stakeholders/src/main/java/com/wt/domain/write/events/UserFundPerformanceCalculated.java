package com.wt.domain.write.events;

import static com.wt.domain.write.events.IssueType.Default;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.lastDayEOD;
import static com.wt.utils.DoublesUtil.isANonZeroValidDouble;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.DoublesUtil.validDouble;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UserFundPerformanceCalculated extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  private final String cognitoIdentityId;
  @Expose(serialize = false, deserialize = false)
  private final double totalInvestment;
  @Expose
  private final double returnPct;
  @Expose
  private final double realizedPnL;
  @Expose
  private final double unrealizedPnL;
  @Expose
  private final double pnl;
  @Expose
  private final double currentValue;
  private final long date;
  @Expose
  private List<UserAdvisePerformanceCalculated> allAdvisePerformance;

  @Deprecated
  public UserFundPerformanceCalculated(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String cognitoIdentityId, double totalInvestment, double returnPct,
      double realizedPnL, double unrealizedPnL, double pnl, double currentValue, long date,
      List<UserAdvisePerformanceCalculated> allAdvisePerformance) {
    this(username, adviserUsername, fundName, investingMode, totalInvestment, returnPct, realizedPnL, unrealizedPnL,
        pnl, currentValue, date, allAdvisePerformance);
  }

  public UserFundPerformanceCalculated(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, double totalInvestment, double returnPct, double realizedPnL, double unrealizedPnL,
      double pnl, double currentValue, long date, List<UserAdvisePerformanceCalculated> allAdvisePerformance) {
    super(username, adviserUsername, fundName, investingMode);
    this.cognitoIdentityId = username;
    this.totalInvestment = totalInvestment;
    this.returnPct = returnPct;
    this.realizedPnL = realizedPnL;
    this.unrealizedPnL = unrealizedPnL;
    this.pnl = pnl;
    this.currentValue = currentValue;
    this.date = date;
    this.allAdvisePerformance = allAdvisePerformance;
  }

  public static class UserFundPerformanceCalculatedBuilder {
    private final String username;
    private final String cognitoIdentityId;
    private final String fund;
    private final String adviser;
    private final InvestingMode investingMode;
    private List<UserAdviseAbsolutePerformanceCalculated> all;
    private List<UserAdviseAbsolutePerformanceWithDate> allAdvisePerformancesWithDate;
    private double cash;
    private double totalInvestment;
    private long date = 0;

    @Deprecated
    public UserFundPerformanceCalculatedBuilder(String username, String cognitoIdentityId, String adviser, String fund,
        InvestingMode investingMode, double totalInvestment, double cash, double withdrawal, long date) {
      this(username, adviser, fund, investingMode, totalInvestment, cash, withdrawal, date);
    }
    
    public UserFundPerformanceCalculatedBuilder(String username, String adviser, String fund,
        InvestingMode investingMode, double totalInvestment, double cash, double withdrawal, long date) {
      super();
      this.username = username;
      this.cognitoIdentityId = username;
      this.adviser = adviser;
      this.fund = fund;
      this.investingMode = investingMode;
      this.totalInvestment = totalInvestment;
      this.cash = cash;
      this.date = date;
      this.all = new ArrayList<>();
      this.allAdvisePerformancesWithDate = new ArrayList<>();
    }

    public UserFundPerformanceCalculatedBuilder withAdvisePerformance(
        UserAdviseAbsolutePerformanceCalculated advisePerformanceCalculated) {
      all.add(advisePerformanceCalculated);
      return this;
    }

    public UserFundPerformanceCalculatedBuilder withAdvisePerformance(
        Collection<UserAdviseAbsolutePerformanceCalculated> allAdvisePerformanceCalculated) {
      all.addAll(allAdvisePerformanceCalculated);
      return this;
    }

    public UserFundPerformanceCalculatedBuilder withAdvisePerformanceAndDate(
        UserAdviseAbsolutePerformanceWithDate advisePerformanceCalculated) {
      allAdvisePerformancesWithDate.add(advisePerformanceCalculated);
      return this;
    }

    public UserFundPerformanceCalculatedBuilder withAdvisePerformanceAndDate(
        Collection<UserAdviseAbsolutePerformanceWithDate> allAdvisePerformanceCalculated) {
      allAdvisePerformancesWithDate.addAll(allAdvisePerformanceCalculated);
      return this;
    }

    public UserFundPerformanceCalculated build() {
      double pnl = 0D;
      double realizedPnL = 0D;
      double unRealizedPnL = 0D;
      double portfolioValue = cash;
      List<UserAdvisePerformanceCalculated> allAdvisePerformance = new ArrayList<>();
      for (UserAdviseAbsolutePerformanceCalculated advisePerformance : all) {
        portfolioValue += advisePerformance.getCurrentValue();
      }
      for (UserAdviseAbsolutePerformanceCalculated advisePerformance : all) {
        realizedPnL += advisePerformance.getRealizedPnL();
        unRealizedPnL += advisePerformance.getUnrealizedPnL();
        pnl += advisePerformance.getPnl();
        double adviseAllocation = isANonZeroValidDouble(portfolioValue)
            ? (advisePerformance.getCurrentValue() / portfolioValue)
            : 0.;
        double adviseReturnPct = advisePerformance.getPctPnL() * adviseAllocation;
        UserAdvisePerformanceCalculated userAdvisePerformanceCalculated = new UserAdvisePerformanceCalculated(
            advisePerformance.getUsername(), adviser, fund, investingMode, advisePerformance.getAdviseId(),
            advisePerformance.getWdId(), advisePerformance.getSymbol(), advisePerformance.getRealizedPnL(),
            advisePerformance.getUnrealizedPnL(), advisePerformance.getPnl(), adviseReturnPct,
            advisePerformance.getCashFromExit(), advisePerformance.getCurrentValue(), adviseAllocation * 100.,
            advisePerformance.getNbShares(), date, advisePerformance.getIssueType());
        if (isZero(advisePerformance.getNbShares()))
          continue;
        allAdvisePerformance.add(userAdvisePerformanceCalculated);
      }
      addCash(portfolioValue, allAdvisePerformance);

      double returnPct = isANonZeroValidDouble(totalInvestment) ? (pnl / totalInvestment) * 100. : 0D;

      return new UserFundPerformanceCalculated(username, adviser, fund, investingMode, cognitoIdentityId,
          round(totalInvestment), round(returnPct), round(validDouble(realizedPnL)), round(validDouble(unRealizedPnL)),
          round(validDouble(pnl)), round(validDouble(portfolioValue)), date, allAdvisePerformance);
    }

    public UserFundPerformanceCalculated buildHistorical() {
      double pnl = 0D;
      double realizedPnL = 0D;
      double unRealizedPnL = 0D;
      double portfolioValue = cash;
      List<UserAdvisePerformanceCalculated> allAdvisePerformance = new ArrayList<>();
      for (UserAdviseAbsolutePerformanceWithDate advisePerformance : allAdvisePerformancesWithDate) {
        portfolioValue += advisePerformance.getCurrentValue();
      }
      for (UserAdviseAbsolutePerformanceWithDate advisePerformance : allAdvisePerformancesWithDate) {
        realizedPnL += advisePerformance.getRealizedPnL();
        unRealizedPnL += advisePerformance.getUnrealizedPnL();
        pnl += advisePerformance.getPnl();
        double adviseAllocation = isANonZeroValidDouble(portfolioValue)
            ? (advisePerformance.getCurrentValue() / portfolioValue)
            : 0.;
        double adviseReturnPct = advisePerformance.getPctPnL() * adviseAllocation;
        allAdvisePerformance.add(new UserAdvisePerformanceCalculated(advisePerformance.getUsername(), adviser, fund,
            investingMode, advisePerformance.getAdviseId(), advisePerformance.getWdId(), advisePerformance.getSymbol(),
            advisePerformance.getRealizedPnL(), advisePerformance.getUnrealizedPnL(), advisePerformance.getPnl(),
            adviseReturnPct, advisePerformance.getCashFromExit(), advisePerformance.getCurrentValue(),
            adviseAllocation * 100., advisePerformance.getNbShares(), advisePerformance.getDate(),
            advisePerformance.getIssueType()));
      }
      addCash(portfolioValue, allAdvisePerformance);

      double returnPct = isANonZeroValidDouble(totalInvestment) ? (pnl / totalInvestment) * 100. : 0.;

      return new UserFundPerformanceCalculated(username, adviser, fund, investingMode, cognitoIdentityId,
          round(totalInvestment), round(returnPct), round(validDouble(realizedPnL)), round(validDouble(unRealizedPnL)),
          round(validDouble(pnl)), round(validDouble(portfolioValue)), date, allAdvisePerformance);
    }

    public static UserFundPerformanceCalculated withDefaultPerformance(String username, String adviserUsername,
        String fundName, InvestingMode investingMode, double totalInvestment) {
      return new UserFundPerformanceCalculated(username, adviserUsername, fundName, investingMode, totalInvestment,
          0D, 0D, 0D, 0D, totalInvestment, lastDayEOD(currentTimeInMillis()), null);
    }

    private void addCash(double portfolioValue, List<UserAdvisePerformanceCalculated> allAdvisePerformance) {
      double cashAllocation = isANonZeroValidDouble(portfolioValue) ? (validDouble(cash) / portfolioValue) : 0.;
      allAdvisePerformance.add(new UserAdvisePerformanceCalculated("", adviser, fund, investingMode, "Cash", "Cash",
          "Cash", 0, 0, 0, 0, 0, round(validDouble(cash)), cashAllocation * 100., 0, date, Default));
    }
  }
}
