package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper=false)
public class UserAdviseEvent extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  private final String adviseId;
  private IssueType issueType;
  
  public UserAdviseEvent(String username, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, IssueType issueType) {
    super(username, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.issueType = issueType;
  }
}
