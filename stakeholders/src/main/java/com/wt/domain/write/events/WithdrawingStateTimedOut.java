package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class WithdrawingStateTimedOut extends UserFundEvent {

  public WithdrawingStateTimedOut(String email, String adviserUsername, String fundName,
      InvestingMode investingMode) {
    super(email, adviserUsername, fundName, investingMode);
  }

  private static final long serialVersionUID = 1L;

}
