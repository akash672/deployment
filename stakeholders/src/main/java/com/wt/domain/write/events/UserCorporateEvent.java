package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class UserCorporateEvent extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  
  public UserCorporateEvent(String email, String adviserUsername, String fundName, InvestingMode investingMode
                        ) {
    super(email, adviserUsername, fundName, investingMode);
  }

}
