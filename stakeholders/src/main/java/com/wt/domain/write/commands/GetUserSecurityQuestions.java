package com.wt.domain.write.commands;

import java.io.Serializable;

public class GetUserSecurityQuestions extends UnAuthenticatedUserCommand implements Serializable {

  private static final long serialVersionUID = 1L;

  public GetUserSecurityQuestions(String username) {
    super(username);
  }

}
