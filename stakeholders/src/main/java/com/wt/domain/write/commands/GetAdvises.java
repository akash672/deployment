package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;

@Getter
public class GetAdvises extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private String adviserUsername;
  private String fundName;
  private InvestingMode investingMode;

  @JsonCreator
  public GetAdvises(@JsonProperty("adviserUsername") String adviserUsername, @JsonProperty("fundName") String fundName,
      @JsonProperty("investingMode") InvestingMode investingMode, @JsonProperty("username") String username) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.investingMode = investingMode;
  }
}
