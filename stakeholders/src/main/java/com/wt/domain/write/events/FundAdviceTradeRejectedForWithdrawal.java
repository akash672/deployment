package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;

public class FundAdviceTradeRejectedForWithdrawal extends UserFundEvent {
  
  private static final long serialVersionUID = 1L;
  @Getter
  private String adviseId;

  public FundAdviceTradeRejectedForWithdrawal(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
  }


}
