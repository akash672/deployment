package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UserFundSubscriptionDetails extends Done {

  private static final long serialVersionUID = 1L;
  private String fundName;
  private String adviserUsername;
  private InvestingMode investingMode;
  private double subscriptionAmount;
  private double withdrawal;
  private String isFundSubscribed;
  private String isFundArchived;

  @JsonCreator
  public UserFundSubscriptionDetails(@JsonProperty("userEmail") String userEmail,
      @JsonProperty("fundName") String fundName, @JsonProperty("adviserUserName") String adviserUsername,
      @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("subscriptionAmount") double subscriptionAmount, @JsonProperty("withdrawal") double withdrawal,
      @JsonProperty("isFundSubscribed") String isFundSubscribed, @JsonProperty("isFundArchived") String isFundArchived) {
    super(userEmail, "");
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.investingMode = investingMode;
    this.subscriptionAmount = subscriptionAmount;
    this.isFundSubscribed = isFundSubscribed;
    this.withdrawal = withdrawal;
    this.isFundArchived = isFundArchived;
  }
}
