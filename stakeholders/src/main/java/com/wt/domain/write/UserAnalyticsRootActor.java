package com.wt.domain.write;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.write.commands.FetchAllUsersPendingFundRebalancingApprovals;
import com.wt.domain.write.commands.StopFetchingAllUsersPendingFundRebalancingApprovals;

import akka.actor.UntypedAbstractActor;

@Service("UserAnalyticsRootActor")
@Scope("prototype")
public class UserAnalyticsRootActor extends UntypedAbstractActor implements ParentActor<UserAnalyticsActor> {

  @Override
  public void onReceive(Object msg) {
    if (msg instanceof FetchAllUsersPendingFundRebalancingApprovals) {
      tellChildren(context(), sender(), msg,
          "Analytics-child-" + ((FetchAllUsersPendingFundRebalancingApprovals) msg).getConnectionId());
    } else if (msg instanceof StopFetchingAllUsersPendingFundRebalancingApprovals) {
      tellChildren(context(), sender(), msg,
          "Analytics-child-" + ((StopFetchingAllUsersPendingFundRebalancingApprovals) msg).getConnectionId());
    }

  }

  @Override
  public String getName() {
    return "UserAnalyticsRootActor";
  }

  @Override
  public String getChildBeanName() {
    return "UserAnalyticsActor";
  }

}
