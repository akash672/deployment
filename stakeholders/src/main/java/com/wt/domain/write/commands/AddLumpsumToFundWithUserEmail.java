package com.wt.domain.write.commands;

import com.wt.domain.InvestingMode;
import com.wt.domain.RiskLevel;
import com.wt.domain.write.events.FundConstituentListResponse;

import lombok.Getter;

public class AddLumpsumToFundWithUserEmail extends AddLumpsumToFund {
  private static final long serialVersionUID = 1L;
  @Getter
  private String userEmail;
  @Getter
  private String inceptionDate;
  @Getter
  private RiskLevel riskProfile;
  @Getter
  private FundConstituentListResponse fundConstituentListResponse;
  @Getter
  private String basketOrderId;
  @Getter
  private BrokerAuthInformation brokerAuthInfo;

  public AddLumpsumToFundWithUserEmail(String adviserName, String fundName, String identityId, String userEmail,
      double lumpsumAmount, RiskLevel riskProfile, String inceptionDate,
      InvestingMode investingMode, FundConstituentListResponse fundConstituentListResponse, String basketOrderId,
      BrokerAuthInformation brokerAuthInfo) {
    super(adviserName, fundName, identityId, lumpsumAmount, investingMode);
    this.userEmail = userEmail;
    this.riskProfile = riskProfile;
    this.inceptionDate = inceptionDate;
    this.fundConstituentListResponse = fundConstituentListResponse;
    this.basketOrderId = basketOrderId;
    this.brokerAuthInfo = brokerAuthInfo;
  }
  
  
}