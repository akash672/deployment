package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wt.domain.InvestingMode;

import lombok.Getter;

@Getter
public class UserHistoricalPerformanceWithEmail extends CalculateUserHistoricalPerformance {

  private final String username;
  private InvestingMode investingMode;

  @JsonCreator
  public UserHistoricalPerformanceWithEmail(String username, InvestingMode investingMode) {
    this.username = username;
    this.investingMode = investingMode;
  }
}
