package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.Order;
import com.wt.domain.PriceWithPaf;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FundAdviceIssuedWithUserEmailAndOrderAndPrice extends UserAdviseEvent{

  private static final long serialVersionUID = 1L;
  private String token;
  private double absoluteAllocation;
  private double monetaryAllocationValue;
  private long issueTime;
  private PriceWithPaf entryPrice;
  private long entryTime;
  private PriceWithPaf exitPrice;
  private long exitTime;
  private Order order;
  private double currentPrice;

  public FundAdviceIssuedWithUserEmailAndOrderAndPrice(String email, String adviserUsername, String fundName,
      String adviseId, InvestingMode investingMode, String token, double absoluteAllocation, long issueTime,
      PriceWithPaf entryPrice, long entryTime, PriceWithPaf exitPrice, long exitTime, Order order, double currentPrice, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.token = token;
    this.absoluteAllocation = absoluteAllocation;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.order = order;
    this.currentPrice = currentPrice;
  }

}
