package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class CorporateMergerPrimaryBuyEvent extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private String token;
  private String identityId;
  private String adviseId;
  private double oldPrice;
  private double newPrice;
  private OrderSide orderSide;
  private double sharesRatio;
  private String symbol;

  @Deprecated
  public CorporateMergerPrimaryBuyEvent(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String token, String identityId, String adviseId, double oldPrice, double newPrice,
      double sharesRatio, OrderSide orderSide, String symbol) {
    this(username, adviserUsername, fundName, investingMode, token, adviseId, oldPrice, newPrice, sharesRatio,
        orderSide, symbol);
  }

  public CorporateMergerPrimaryBuyEvent(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String token, String adviseId, double oldPrice, double newPrice, double sharesRatio,
      OrderSide orderSide, String symbol) {
    super(username, adviserUsername, fundName, investingMode);
    this.token = token;
    this.identityId = username;
    this.adviseId = adviseId;
    this.oldPrice = oldPrice;
    this.newPrice = newPrice;
    this.sharesRatio = sharesRatio;
    this.orderSide = orderSide;
    this.symbol = symbol;
  }

}