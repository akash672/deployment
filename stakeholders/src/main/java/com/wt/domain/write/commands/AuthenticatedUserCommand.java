package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@EqualsAndHashCode
public abstract class AuthenticatedUserCommand implements Serializable
{
  private static final long serialVersionUID = 1L;
  @NonNull
  @Getter
  @Setter
  protected String username;
}
