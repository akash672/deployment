package com.wt.domain.write.events;

import static com.wt.aws.cognito.configuration.CredentialsProviderType.Cognito;

import java.nio.ByteBuffer;

import com.wt.aws.cognito.configuration.CredentialsProviderType;

import lombok.Getter;

public class CognitoUserRegistered extends UserRegistered {
  private static final long serialVersionUID = 1L;
  @Getter
  private byte[] password;
  @Getter
  private byte[] salt;

  public CognitoUserRegistered(String firstName, String lastName, String username, ByteBuffer password, ByteBuffer salt,
      String identityID, long timestamp, String deviceToken) {
    super(username, firstName, lastName, identityID, timestamp, Cognito, deviceToken, identityID);
  }
    
  public CognitoUserRegistered(String firstName, 
      String lastName, String username, 
      ByteBuffer password, ByteBuffer salt, String identityID,
      long timestamp, String deviceToken, CredentialsProviderType type) {
    super(username, firstName, lastName, identityID, timestamp, type, deviceToken, username);
    this.password = password.array();
    this.salt = salt.array();
  }
}
