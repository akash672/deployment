package com.wt.domain.write.events;

import lombok.Getter;

@Getter
public class UserRegisteredFromUserPool extends UserEvent {

  private static final long serialVersionUID = 1L;
  String name;
  String phoneNumber;
  double virtualMoney = 5000000.0;

  public UserRegisteredFromUserPool(String username, String name, String phoneNumber, String email, double virtualMoney) {
    super(username, email);
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.virtualMoney = virtualMoney;
  }
  
  public String getEmail() {
    return super.email;
  }

}
