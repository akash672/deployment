package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdateUserFundCash extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private String fundName;
  private String adviserUsername;
  private InvestingMode investingMode;
  private double cashToAddToUserFund;

  @JsonCreator
  public UpdateUserFundCash(@JsonProperty("username") String username, @JsonProperty("fund") String fundName,
      @JsonProperty("adviser") String adviserUsername, @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("cashToAddToUserFund") double cashToAddToUserFund) {
    super(username);
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.investingMode = investingMode;
    this.cashToAddToUserFund = cashToAddToUserFund;
  }
}
