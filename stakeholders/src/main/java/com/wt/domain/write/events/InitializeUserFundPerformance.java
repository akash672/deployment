package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;

@Getter
public class InitializeUserFundPerformance {
  
  private final InvestingMode investingMode;

  public InitializeUserFundPerformance(InvestingMode investingMode) {
    super();
    this.investingMode = investingMode;
  }

}
