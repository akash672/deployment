package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class DeviceToken {

  @Getter
  private String deviceToken;
  
  @JsonCreator
  public DeviceToken(@JsonProperty("deviceToken") String deviceToken) {
    assertStringNotEmpty(deviceToken, "deviceToken");
    this.deviceToken = deviceToken;
  }
}
