package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.InvestingMode;

import lombok.Getter;

@Getter
public class GetUserAllInvestmentStatus extends AuthenticatedUserCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  private InvestingMode investingMode;

  public GetUserAllInvestmentStatus(String username, InvestingMode investingMode) {
    super(username);
    this.investingMode = investingMode;
  }

}
