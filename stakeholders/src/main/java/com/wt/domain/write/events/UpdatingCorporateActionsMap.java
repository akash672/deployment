package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class UpdatingCorporateActionsMap extends UserFundEvent{

  private static final long serialVersionUID = 1L;
  private String oldToken;

  public UpdatingCorporateActionsMap(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String oldToken) {
    super(username, adviserUsername, fundName, investingMode);
    this.oldToken = oldToken;
  }
  
  

}
