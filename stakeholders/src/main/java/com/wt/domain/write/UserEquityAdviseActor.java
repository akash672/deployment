package com.wt.domain.write;

import static com.wt.domain.AdviseType.Close;
import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.OrderSide.SELL;
import static com.wt.domain.PriceWithPaf.noPrice;
import static com.wt.domain.Purpose.CORPORATEACTION;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.ONBOARDING;
import static com.wt.domain.UserAcceptanceMode.Manual;
import static com.wt.domain.UserAdviseState.AdviseTokenPriceReceived;
import static com.wt.domain.UserAdviseState.CompleteExitAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.CompleteExitAdviseOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.CompleteExitAdviseOrderReceivedWithBroker;
import static com.wt.domain.UserAdviseState.CompleteExitReceived;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlaced;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlacedWithBroker;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.ExpectingAdvisePrice;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderReceivedWithBroker;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderRejected;
import static com.wt.domain.UserAdviseState.PartialExitAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.PartialExitAdviseOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.PartialExitAdviseOrderReceivedWithBroker;
import static com.wt.domain.UserAdviseState.PartialExitReceived;
import static com.wt.domain.UserAdviseState.UserAdviseIssued;
import static com.wt.domain.UserResponseType.Approve;
import static com.wt.domain.UserResponseType.Awaiting;
import static com.wt.domain.write.events.AdviseTradesExecuted.closeAdviseTradesExecutedWithExecutionDetails;
import static com.wt.domain.write.events.BrokerNameSent.noBrokerInformationAvailable;
import static com.wt.domain.write.events.InstrumentByWdIdSent.noInstrument;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.domain.write.events.UserFundAdditionalSumAdvice.fundLumpSumAdviceWithUndefinedExitParams;
import static com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail.fundAdviceWithUndefinedExitParams;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.CommonConstants.CorporateEventIdPrefix;
import static com.wt.utils.CommonConstants.EntryOrderIdPrefix;
import static com.wt.utils.CommonConstants.ExitOrderIdPrefix;
import static com.wt.utils.CommonConstants.HYPHEN;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static java.lang.String.valueOf;
import static java.util.concurrent.TimeUnit.MINUTES;
import static scala.concurrent.duration.Duration.create;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.BrokerName;
import com.wt.domain.CorporateEventType;
import com.wt.domain.DestinationAddress;
import com.wt.domain.InvestingMode;
import com.wt.domain.Order;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.OrderSegment;
import com.wt.domain.OrderSide;
import com.wt.domain.OrderType;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.UserAdviseState;
import com.wt.domain.UserEquityAdvise;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.write.commands.AdviseAtSpecifiedPrice;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CalculateUserAdviseComposition;
import com.wt.domain.write.commands.CalculateUserFundComposition;
import com.wt.domain.write.commands.CreatedOrder;
import com.wt.domain.write.commands.CreatedOrder.CreatedOrderBuilder;
import com.wt.domain.write.commands.GetBrokerName;
import com.wt.domain.write.commands.GetClientCode;
import com.wt.domain.write.commands.GetInstrumentByWdId;
import com.wt.domain.write.commands.GetPriceForSymbolAndClientCode;
import com.wt.domain.write.commands.GetPriceFromTracker;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.HasCorpActionOrderBeenExecutedQuery;
import com.wt.domain.write.commands.HasCorpActionOrderBeenPlacedWithExchangeQuery;
import com.wt.domain.write.commands.HasCorporateActionOrderBeenReceivedQuery;
import com.wt.domain.write.commands.HasCreatedOrderBeenReceived;
import com.wt.domain.write.commands.ProvideOrderBook;
import com.wt.domain.write.commands.ProvideTradeBook;
import com.wt.domain.write.commands.SellOrderDetails;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.commands.UpdateAdviseTradeStatus;
import com.wt.domain.write.commands.UpdateDividendEventWithFundNames;
import com.wt.domain.write.commands.UpdateSplitTypeEventWithFundNames;
import com.wt.domain.write.commands.UpdateUserFundCompositionList;
import com.wt.domain.write.events.AdviseOrderPlacedWithExchange;
import com.wt.domain.write.events.AdviseOrderReceivedPlacedWithExchangeTimedOut;
import com.wt.domain.write.events.AdviseOrderReceivedWithBroker;
import com.wt.domain.write.events.AdviseOrderRejected;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.AdviseOrderTradeExecutedTimedOut;
import com.wt.domain.write.events.AdvisePriceExpectedStateTimedOut;
import com.wt.domain.write.events.AdviseReceivedWithBrokerTimedOut;
import com.wt.domain.write.events.AdviseTokenPriceReceived;
import com.wt.domain.write.events.AdviseTradesExecuted;
import com.wt.domain.write.events.BrokerNameSent;
import com.wt.domain.write.events.CompleteExitAdviseOrderExecutedTimedOut;
import com.wt.domain.write.events.CurrentActiveOrderTerminated;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.DividendEventReceived;
import com.wt.domain.write.events.ExchangeOrderUpdateAcknowledged;
import com.wt.domain.write.events.ExchangeOrderUpdateEvent;
import com.wt.domain.write.events.ExitAdviseReceived;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.HackEventToLetFSMProcessPredicate;
import com.wt.domain.write.events.InstrumentByWdIdSent;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.MergerDemergerFundAdviseClosed;
import com.wt.domain.write.events.OrderHasBeenReceived;
import com.wt.domain.write.events.OrderHasNotBeenReceived;
import com.wt.domain.write.events.OrderPlacedWithExchange;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.OrderRejectedByBroker;
import com.wt.domain.write.events.OrderRejectedByExchange;
import com.wt.domain.write.events.OrderTerminatedAtEOD;
import com.wt.domain.write.events.PriceNotReceived;
import com.wt.domain.write.events.RequestedClientCode;
import com.wt.domain.write.events.SpecialCircumstancesSell;
import com.wt.domain.write.events.StateTransitioningPostRejection;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.domain.write.events.UpdateCorporateEvent;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserAdviseOrderHasBeenSuccessfullySent;
import com.wt.domain.write.events.UserAdviseOrderHasNotBeenSuccessfullySent;
import com.wt.domain.write.events.UserAdviseTransactionReceived;
import com.wt.domain.write.events.UserCloseAdviseOrderRejected;
import com.wt.domain.write.events.UserFundAdditionalSumAdvice;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.domain.write.events.UserFundAdviseIssuedInsufficientCapital;
import com.wt.domain.write.events.UserFundDividendReceived;
import com.wt.domain.write.events.UserOpenAdviseOrderRejected.UserOpenAdviseOrderRejectedBuilder;
import com.wt.domain.write.events.UserResponseReceived;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import lombok.extern.log4j.Log4j;
import scala.Option;

@Service("UserEquityAdviseActor")
@Scope("prototype")
@Log4j
public class UserEquityAdviseActor extends UserAdviseActor<UserAdviseState, UserEquityAdvise, UserAdviseEvent> {
  private String persistenceId;
  private WDActorSelections selections;
  private Purpose investingPurpose;
  
  @Override
  public void preRestart(Throwable reason, Option<Object> message) {
    super.preRestart(reason, message);
  }
  
  @Override
  public void postRestart(Throwable reason) throws Exception {
    super.postRestart(reason);
  }
  
  public UserEquityAdviseActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
    
    startWith(UserAdviseIssued, new UserEquityAdvise());
    
    when(UserAdviseIssued,
        matchEvent(UserFundAdviceIssuedWithUserEmail.class, 
            (adviseIssued, userAdvise) -> 
        {
          UserFundAdviceIssuedWithUserEmail userFundAdviseIssued = fundAdviceWithUndefinedExitParams(
              adviseIssued.getUsername(), 
              adviseIssued.getAdviserUsername(), 
              adviseIssued.getFundName(),
              adviseIssued.getInvestingMode(), 
              adviseIssued.getAdviseId(), 
              adviseIssued.getToken(),
              adviseIssued.getToken(),
              "",
              adviseIssued.getAbsoluteAllocation(), 
              adviseIssued.getMonetaryAllocationValue(), 
              adviseIssued.getEntryPrice(),
              adviseIssued.isUserBeingOnboarded(),
              adviseIssued.getBrokerName(),
              adviseIssued.getClientCode(),
              getIssueType(),
              adviseIssued.isSpecialAdvise(),
              adviseIssued.getBasketOrderCompositeId(),
              adviseIssued.getBrokerAuthInformation());
        return goTo(ExpectingAdvisePrice).
        applying(userFundAdviseIssued).
        andThen(exec(advise -> {
          if (adviseIssued.isUserBeingOnboarded()) {
            investingPurpose = ONBOARDING;
            getPriceForAnExistingUser(advise);
          } else if(adviseIssued.isSpecialAdvise()){
            investingPurpose = CORPORATEACTION;
            self().tell( new AdviseAtSpecifiedPrice(adviseIssued.getEntryPrice()), ActorRef.noSender());
          } else { 
            investingPurpose = EQUITYSUBSCRIPTION;
            getPriceFromBroadcast(advise);
          }
          }));    
        
        }).
        event(UserFundAdditionalSumAdvice.class, 
            (adviseIssued, userAdvise) -> 
        {
          UserFundAdditionalSumAdvice userFundAdviseIssued = fundLumpSumAdviceWithUndefinedExitParams(
              adviseIssued.getUsername(), 
              adviseIssued.getAdviserUsername(), 
              adviseIssued.getFundName(),
              adviseIssued.getInvestingMode(), 
              adviseIssued.getAdviseId(), 
              adviseIssued.getToken(),
              adviseIssued.getToken(),
              "",
              adviseIssued.getAbsoluteAllocation(), 
              adviseIssued.getMonetaryAllocationValue(), 
              adviseIssued.getEntryPrice(),
              adviseIssued.isUserBeingOnboarded(),
              adviseIssued.getBrokerName(),
              adviseIssued.getClientCode(),
              adviseIssued.getIssueType(),
              adviseIssued.isSpecialAdvise(),
              adviseIssued.getBasketOrderId(),
              adviseIssued.getBrokerAuthInformation());
        return goTo(ExpectingAdvisePrice).
        applying(userFundAdviseIssued).
        andThen(exec(advise -> {
          if(adviseIssued.isSpecialAdvise()){
            investingPurpose = CORPORATEACTION;
            self().tell( new AdviseAtSpecifiedPrice(adviseIssued.getEntryPrice()), ActorRef.noSender());
          }else{
            investingPurpose = EQUITYSUBSCRIPTION;
            getPriceFromBroadcast(advise);
          }
          }));    
        })
        .event(UpdateAdviseTradeStatus.class, 
            (updateAdviseTradeStatus, userAdvise) -> {
              UserFundAdviseIssuedInsufficientCapital userFundAdviseIssuedInsufficientCapital = new UserFundAdviseIssuedInsufficientCapital(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  userAdvise.getToken(),
                  userAdvise.getIssueAdviseAllocationValue(),
                  getIssueType());
                  selections.userRootActor().tell(userFundAdviseIssuedInsufficientCapital, self());
          return stay();
          
            }));
    
    when(ExpectingAdvisePrice, create(2, MINUTES),
        matchEvent(PriceWithPaf.class, (priceWithPaf, userAdvise) -> {
          int numberOfShares = userAdvise.calculateNumberOfShares(userAdvise.getIssueAdviseAllocationValue(),
              priceWithPaf.getPrice().getPrice());
          return (numberOfShares > 0) ? true: false;
        },
            (realTimePrice, userAdvise) -> 
        {
          String localOrderId = EntryOrderIdPrefix + HYPHEN + userAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN + userAdvise.getAdviseId() + HYPHEN + UUID.randomUUID().toString();
          return goTo(AdviseTokenPriceReceived).
          applying(new AdviseTokenPriceReceived(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              stateName(),
              realTimePrice,
              localOrderId,
              getIssueType(),
              investingPurpose)).
          andThen(exec(advise -> {
            CreatedOrderBuilder createdOrderBuilder = advise.getCreatedBuyOrder();
            createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
            createdOrderBuilder.withInvestingPurpose(createdOrderBuilder.getPurpose());
            selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
          }));
        }).
        event(AdviseAtSpecifiedPrice.class, UserEquityAdvise.class, (adviseAtSpecifiedPrice, userAdvise) -> {
          String localOrderId = EntryOrderIdPrefix + HYPHEN + userAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN + userAdvise.getAdviseId() + HYPHEN + UUID.randomUUID().toString();
          return goTo(AdviseTokenPriceReceived).
          applying(new AdviseTokenPriceReceived(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              stateName(),
              adviseAtSpecifiedPrice.getEntryPrice(),
              localOrderId,
              getIssueType(),
              investingPurpose)).
          andThen(exec(advise -> {
            selections.equityOrderRootActor().tell(new CreatedOrder(
                advise.getUserAdviseCompositeKey().persistenceId(),
                userAdvise.getLatestIssuedAdviseOrder().get().getOrderId(),
                advise.getBrokerName(),
                advise.getCreatedBuyOrder().getPurpose(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                advise.getClientCode(),
                new OrderExecutionMetaData.OrderExecutionMetaDataBuilder( 
                    userAdvise.getToken(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getSide(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getIntegerQuantity(), 
                    OrderType.MARKET, 
                    OrderSegment.Equity)
                .withSymbol(advise.getSymbol())
                .withLimitPrice(adviseAtSpecifiedPrice.getEntryPrice().getPrice().getPrice())
                .withbasketOrderCompositeId(userAdvise.getBasketOrderCompositeId())
                .build(),  
                userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                userAdvise.getBrokerAuthInformation()
                ), self());
              
          }));
        }).
        event(PriceWithPaf.class, UserEquityAdvise.class, (priceWithPaf, userAdvise) -> {
          int numberOfShares = userAdvise.calculateNumberOfIssueAdviseShares(userAdvise.getIssueAdviseAllocationValue(),
              priceWithPaf.getPrice().getPrice());
          return (numberOfShares > 0) ? false : true;
        },
            (realTimePrice, userAdvise) -> 
        {
          UserFundAdviseIssuedInsufficientCapital userFundAdviseIssuedInsufficientCapital = new UserFundAdviseIssuedInsufficientCapital(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              userAdvise.getToken(),
              userAdvise.getIssueAdviseAllocationValue(),
              getIssueType());
          return goTo(OpenAdviseOrderRejected)
              .applying(userFundAdviseIssuedInsufficientCapital)
              .andThen(exec(advise -> {
                selections.userRootActor().tell(userFundAdviseIssuedInsufficientCapital, self());
              }));
        })
        .
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdvisePriceExpectedStateTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            if(investingPurpose == ONBOARDING)
              getPriceForAnExistingUser(advise);
            if(investingPurpose == EQUITYSUBSCRIPTION)
              getPriceFromBroadcast(advise);
          }));
        }));
    
    when(AdviseTokenPriceReceived, create(5, MINUTES),
        matchEvent(OrderReceivedWithBroker.class,
            (orderReceivedWithBroker, userAdvise) -> 
        {
          return goTo(OpenAdviseOrderReceivedWithBroker).
              applying(new AdviseOrderReceivedWithBroker(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(),
                  orderReceivedWithBroker.getLocalOrderId(),
                  orderReceivedWithBroker.getBrokerOrderId(),
                  getIssueType()
              ))
              .andThen(exec(advise -> {
                    selections.userRootActor()
                        .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                            "Received exchange update " + orderReceivedWithBroker, orderReceivedWithBroker,
                            Equity.getIssueType()), self());
              }));
        }).
        event(OrderRejectedByBroker.class, 
                (orderRejected, userAdvise) -> {
                  return goTo(OpenAdviseOrderRejected)
                      .applying(new AdviseOrderRejected(
                          userAdvise.getUserAdviseCompositeKey().getUsername(),
                          userAdvise.getUserAdviseCompositeKey().getAdviser(),
                          userAdvise.getUserAdviseCompositeKey().getFund(),
                          userAdvise.getAdviseId(),
                          userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                          getIssueType())).
                       andThen(exec(advise -> {
                            selections.userRootActor()
                                .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                    "Received exchange update " + orderRejected, orderRejected, Equity.getIssueType()),
                                    self());
                         selections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                             advise.getUserAdviseCompositeKey().getUsername(),
                             advise.getUserAdviseCompositeKey().getAdviser(),
                             advise.getUserAdviseCompositeKey().getFund(),
                             advise.getAdviseId(), 
                             advise.getIssueAdviseAllocationValue(),
                             advise.getUserAdviseCompositeKey().getInvestingMode(),
                             getIssueType()), self());
                       })); 
        }).
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdviseReceivedWithBrokerTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            CreatedOrderBuilder createdOrderBuilder = advise.getCreatedBuyOrder();
            if(createdOrderBuilder.getPurpose() == CORPORATEACTION)
              selections.equityOrderRootActor().tell(new HasCorporateActionOrderBeenReceivedQuery(createdOrderBuilder.getClientOrderId()), self());
            else if(createdOrderBuilder.getPurpose() == EQUITYSUBSCRIPTION)
              selections.equityOrderRootActor().tell(new HasCreatedOrderBeenReceived(createdOrderBuilder.getClientOrderId()), self());
          }));
        }).
        event(OrderHasBeenReceived.class, 
            (orderHasBeenReceived, userAdvise) -> {
          return stay()
              .applying(new UserAdviseOrderHasBeenSuccessfullySent( 
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  orderHasBeenReceived.getClientOrderId(),
                  getIssueType()))
              .andThen(exec(advise -> {
                selections.equityOrderRootActor().tell(new ProvideOrderBook(orderHasBeenReceived.getClientOrderId()), self());
              }));
        }).
        event(OrderHasNotBeenReceived.class, 
            (orderHasNotBeenReceived, userAdvise) -> {
          return stay()
              .applying(new UserAdviseOrderHasNotBeenSuccessfullySent(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  orderHasNotBeenReceived.getClientOrderId(),
                  getIssueType()))
              .andThen(exec(advise -> {
                CreatedOrderBuilder createdOrderBuilder = advise.getCreatedBuyOrder();
                createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
                createdOrderBuilder.withInvestingPurpose(createdOrderBuilder.getPurpose());
                selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
        }));
    }));
        
      when(OpenAdviseOrderReceivedWithBroker, create(5, MINUTES),
          matchEvent(OrderPlacedWithExchange.class, 
          (orderPlaced, userAdvise) -> 
      {
        return goTo(OpenAdviseOrderPlacedWithExchange).
        applying(new AdviseOrderPlacedWithExchange(
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getAdviseId(),
                stateName(),
                orderPlaced.getExchangeOrderId(),
                orderPlaced.getBrokerOrderId(),
                orderPlaced.getCtclId(), 
                userAdvise.getToken(),
                orderPlaced.getSide(),
                orderPlaced.getQuantity(),
                getIssueType()))
        .andThen(exec(advise -> {
          selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderPlaced, orderPlaced, Equity.getIssueType()), self());
        }));
      }).
          event(OrderRejectedByExchange.class, 
                  (orderRejected, userAdvise) -> {
                    return goTo(OpenAdviseOrderRejected)
                        .applying(new AdviseOrderRejected(
                            userAdvise.getUserAdviseCompositeKey().getUsername(),
                            userAdvise.getUserAdviseCompositeKey().getAdviser(),
                            userAdvise.getUserAdviseCompositeKey().getFund(),
                            userAdvise.getAdviseId(),
                            userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                            getIssueType())).
                         andThen(exec(advise -> {
                           sender().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderRejected, orderRejected, Equity.getIssueType()), self());
                           
                           selections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                               advise.getUserAdviseCompositeKey().getUsername(),
                               advise.getUserAdviseCompositeKey().getAdviser(),
                               advise.getUserAdviseCompositeKey().getFund(),
                               advise.getAdviseId(), 
                               advise.getIssueAdviseAllocationValue(),
                               advise.getUserAdviseCompositeKey().getInvestingMode(),
                               getIssueType()), self());
                         })); 
      }).
          event(StateTimeout$.class, 
              (stateTimeout, userAdvise) -> {
            return stay().
            applying(new AdviseOrderReceivedPlacedWithExchangeTimedOut(
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getAdviseId(),
                getIssueType())).
            andThen(exec(advise -> {
              CreatedOrderBuilder createdOrderBuilder = advise.getCreatedBuyOrder();
              if(createdOrderBuilder.getPurpose() == CORPORATEACTION)
                selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenPlacedWithExchangeQuery(createdOrderBuilder.getClientOrderId(),
                    advise.getLatestIssuedAdviseOrder().get().getBrokerOrderId(),
                    advise.getLatestIssuedAdviseOrder().get().getSide(),
                  (int) advise.getLatestIssuedAdviseOrder().get().getQuantity()), self());
              else if(createdOrderBuilder.getPurpose() == EQUITYSUBSCRIPTION)
                selections.equityOrderRootActor().tell(new ProvideOrderBook(createdOrderBuilder.getClientOrderId()), self());            }));
          }));

    when(OpenAdviseOrderPlacedWithExchange, create(5, MINUTES),
            matchEvent(HackEventToLetFSMProcessPredicate.class,
                (hackEventToLetFSMProcessPredicate, userAdvise) -> {
                  return stay();
                }).
        event(TradeExecuted.class,
            (tradeExecuted, userAdvise) -> {
              return ((userAdvise.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity() + tradeExecuted.getQuantity()) == userAdvise.getLatestIssuedAdviseOrder().get().getQuantity() 
                  && !userAdvise.hasIssueAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? false : true;
            }, (tradeExecuted, userAdvise) -> 
        {
          AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(), 
                  tradeExecuted.getOrderSide(),
                  tradeExecuted.getTradeOrderId(),
                  tradeExecuted.getExchangeOrderId(),
                  userAdvise.getClientCode(),
                  tradeExecuted.getQuantity(), 
                  tradeExecuted.getPrice(), userAdvise.getToken(),
                  getIssueType());
          return stay().
          applying(adviseOrderTradeExecuted).
          andThen(exec(advise -> {
            selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());
          }));
        }).
        event(TradeExecuted.class,
            (tradeExecuted, userAdvise) -> {
              return ((userAdvise.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity() + tradeExecuted.getQuantity()) == userAdvise.getLatestIssuedAdviseOrder().get().getQuantity() 
                  && !userAdvise.hasIssueAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? true : false;
            }, (tradeExecuted, userAdvise) -> 
        { 
          AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(), 
                  tradeExecuted.getOrderSide(),
                  tradeExecuted.getTradeOrderId(),
                  tradeExecuted.getExchangeOrderId(),
                  userAdvise.getClientCode(),
                  tradeExecuted.getQuantity(), 
                  tradeExecuted.getPrice(), userAdvise.getToken(),
                  getIssueType());
          return goTo(OpenAdviseOrderExecuted).
          applying(adviseOrderTradeExecuted).
          andThen(exec(advise -> {
            selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());
                    
                    int totalTradedQuantity = advise.recentlyTradedEntryQuantity();
                    if (advise.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity() == totalTradedQuantity) {
                    selections.userRootActor().tell(new AdviseTradesExecuted(
                        userAdvise.getUserAdviseCompositeKey().getUsername(), 
                        userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                        userAdvise.getUserAdviseCompositeKey().getFund(),
                        userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                        advise.getAdviseId(),
                        stateName(),
                        advise.getIssueAdviseAllocationValue(),
                        round(advise.getLatestIssuedAdviseOrder().get().getQuantity() * advise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice()),
                        totalTradedQuantity,
                        advise.getSymbol(),
                        tradeExecuted.getOrderSide(),
                        advise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice(),
                        getIssueType()), self());
                    }
          }));
           }).
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdviseOrderTradeExecutedTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            CreatedOrderBuilder createdOrderBuilder = advise.getCreatedBuyOrder();
            if(createdOrderBuilder.getPurpose() == CORPORATEACTION)
              selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenExecutedQuery(createdOrderBuilder.getClientOrderId(),
                  advise.getToken(),
                  advise.getLatestIssuedAdviseOrder().get().getExchangeOrderId(),
                  advise.getLatestIssuedAdviseOrder().get().getSide(),
                (int) advise.getLatestIssuedAdviseOrder().get().getQuantity()), self());
            else if(createdOrderBuilder.getPurpose() == EQUITYSUBSCRIPTION)
              selections.equityOrderRootActor().tell(new ProvideTradeBook(createdOrderBuilder.getClientOrderId()), self());
          }));

        }));
    
    when(OpenAdviseOrderRejected, matchEvent(UserFundAdditionalSumAdvice.class,
        (event, userAdvise) -> {
          int activeTradedShares = userAdvise.integerQuantityYetToBeExited();
          return (activeTradedShares > 0) ? false : true;
        }, (event, userAdvise) -> {
      return goTo(UserAdviseIssued)
          .applying(new CurrentUserAdviseFlushedDueToExit(userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType()))
          .andThen(exec(advise -> {
            self().tell(event, self());
          }));
        }).event(UserFundAdditionalSumAdvice.class,
            (event, userAdvise) -> {
              int activeTradedShares = userAdvise.integerQuantityYetToBeExited();
              return (activeTradedShares > 0) ? true : false;
            }, (event, userAdvise) -> {
              return goTo(UserAdviseIssued)
                  .applying(new StateTransitioningPostRejection(userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      userAdvise.getAdviseId(),
                      getIssueType()))
                  .andThen(exec(advise -> {
                    self().tell(event, self());
                  }));
        }).event(UserFundAdviceIssuedWithUserEmail.class,
            (event, userAdvise) -> {
              return goTo(UserAdviseIssued)
                  .applying(new CurrentUserAdviseFlushedDueToExit(userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      userAdvise.getAdviseId(),
                      getIssueType()))
                  .andThen(exec(advise -> {
                    self().tell(event, self());
                  }));
        }).event(UserAdviseTransactionReceived.class, (adviseTransactionReceived, userAdvise) -> {
              int activeTradedShares = userAdvise.integerQuantityYetToBeExited();
              return (activeTradedShares > 0) ? false : true;
            }, (adviseTransactionReceived, userAdvise) -> {
          selections.userRootActor()
              .tell(closeAdviseTradesExecutedWithExecutionDetails(userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(), userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(), userAdvise.getAdviseId(), stateName(), 0d,
                  0, userAdvise.getSymbol(), SELL, 0, getIssueType()), self());
          return stay();
        }).event(UserAdviseTransactionReceived.class, (adviseTransactionReceived, userAdvise) -> {
              int activeTradedShares = userAdvise.integerQuantityYetToBeExited();
              return (activeTradedShares > 0) ? true : false;
            }, (adviseTransactionReceived, userAdvise) -> {
              return goTo(OpenAdviseOrderExecuted)
                  .applying(new StateTransitioningPostRejection(userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      userAdvise.getAdviseId(),
                      getIssueType()))
                  .andThen(exec(advise -> {
                    self().tell(adviseTransactionReceived, self());
                  }));
        }).event(UpdateAdviseTradeStatus.class, 
            (updateAdviseTradeStatus, userAdvise) -> {
              selections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getAdviseId(), 
                  userAdvise.getIssueAdviseAllocationValue(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  getIssueType()), self());
          return stay();
      
        }));                

    when(OpenAdviseOrderExecuted, 
        matchEvent(HackEventToLetFSMProcessPredicate.class, UserEquityAdvise.class, 
            (hackEventToLetFSMProcessPredicate, userAdvise) -> {
      return stay();
    }).
        event(UserAdviseTransactionReceived.class, 
            (userAdviseTransactionReceived, userAdvise) -> {
          return (userAdviseTransactionReceived.getUserAcceptanceMode().equals(Manual) && userAdviseTransactionReceived.getUserResponseType().equals(Awaiting)) ? true : false;
        }, (userAdviseTransactionReceived, userAdvise) -> 
      {
        int currentPendingQuantity = userAdvise.quantityInPendingPartialCloses();
        return stay()
            .applying(userAdviseTransactionReceived)
            .andThen(exec(advise -> {
              int calculatedNumberOfCloseAdviseShares = advise.calculateNumberOfCloseAdviseShares(advise.integerQuantityYetToBeExited() - currentPendingQuantity,
                  userAdviseTransactionReceived.getAbsoluteAllocation());
              if (calculatedNumberOfCloseAdviseShares >= 1) {
                String symbol = getSymbolFor(userAdvise.getToken());
                UserResponseParameters userResponseParameters = new UserResponseParameters.UserResponseParametersBuilder(
                    advise.getAdviseId(), 
                    Close, 
                    advise.getUserAdviseCompositeKey().getUsername(),
                    advise.getUserAdviseCompositeKey().getFund(),
                    advise.getUserAdviseCompositeKey().getAdviser(),
                    userAdvise.getClientCode(), 
                    userAdvise.getBrokerName().getBrokerName(),
                    getExchangeFromWdId(userAdvise.getToken()),
                    userAdvise.getToken(),
                    symbol,
                    advise.getUserAdviseCompositeKey().getInvestingMode(),
                    Awaiting,
                    userAdviseTransactionReceived.getExitDate())
                    .withAbsoluteAllocation(userAdviseTransactionReceived.getAbsoluteAllocation())
                    .withAllocationValue(0.)
                    .withNumberOfShares(calculatedNumberOfCloseAdviseShares)
                    .build();
                selections.userRootActor().forward(userResponseParameters, context());
              } else {
                log.info("Calculated close advise number of shares for advise Id: " + advise.getAdviseId() + " is " + calculatedNumberOfCloseAdviseShares + 
                    ". Hence no action is being taken!");
              }
            }));
      }).
        event(UserResponseReceived.class, 
            (userResponseReceived, userAdvise) -> {
              UserAdviseTransactionReceived userTransactionReceived = new UserAdviseTransactionReceived.UserAdviseTransactionReceivedBuilder(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                  userResponseReceived.getAdviseId(),
                  getIssueType())
                  .withAbsoluteAllocation(userResponseReceived.getAbsoluteAllocation())
                  .withExitPrice(getCurrentCloseAdvisePrice(userAdvise))
                  .withExitTime(userResponseReceived.getInvestmentDate())
                  .withUserAcceptanceMode(Manual)
                  .withUserResponseType(Approve)
                  .withBasketOrderId(userResponseReceived.getBasketOrderCompositeId())
                  .withBrokerAuthentication(userResponseReceived.getBrokerAuthInformation())
                  .build();
              
              return stay().
                  applying(userTransactionReceived).
                  andThen(exec(advise -> {
                    self().tell(userTransactionReceived, self());
                    }));
        }).
        event(UserAdviseTransactionReceived.class, UserEquityAdvise.class, 
            (userAdviseTransactionReceived, userAdvise) -> {
              int calculatedNumberOfCloseAdviseShares = userAdvise.calculateNumberOfCloseAdviseShares(userAdvise.integerQuantityYetToBeExited(), 
                  userAdviseTransactionReceived.getAbsoluteAllocation());
          return (!isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0) && calculatedNumberOfCloseAdviseShares >= 1) ? true : false;
        }, (userAdviseTransactionReceived, userAdvise) -> 
      {
      String localOrderId = ExitOrderIdPrefix + "-" + UUID.randomUUID().toString();
      ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
          userAdviseTransactionReceived.getUsername(), 
          userAdviseTransactionReceived.getAdviserUsername(), 
          userAdviseTransactionReceived.getFundName(), 
          userAdviseTransactionReceived.getInvestingMode(), 
          userAdvise.getToken(),
          userAdviseTransactionReceived.getAdviseId(), 
          userAdviseTransactionReceived.getAbsoluteAllocation(), 
          DateUtils.currentTimeInMillis(), 
          userAdviseTransactionReceived.getExitPrice(),
          localOrderId,
          getIssueType(),
          userAdviseTransactionReceived.getPurpose(),
          userAdviseTransactionReceived.getExitPrice().getPrice().getPrice(),
          ExitAdviseReceived.isNotASpecialSellAdvise(),
          ExitAdviseReceived.quantityNotYetSet(),
          userAdviseTransactionReceived.getBasketOrderCompositeId(),
          userAdviseTransactionReceived.getBrokerAuthInformation());
      return goTo(PartialExitReceived).
      applying(exitAdviseReceived).
      andThen(exec(advise -> {
        String clientCode = advise.getClientCode();
        if (clientCode == null) 
          clientCode = getClientCodeFromUserRootActor(advise.getUserAdviseCompositeKey().getUsername()).getClientCode();
        BrokerName brokerName = advise.getBrokerName();
        if (brokerName == null) 
          brokerName = getBrokerNameOfTheInvestorWith(advise.getUserAdviseCompositeKey().getUsername(), advise.getUserAdviseCompositeKey().getInvestingMode()).getBrokerName();
        String symbol = advise.getSymbol();
        if (symbol == null)
          symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
        CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrderWith(symbol)
                                                  .withBrokerName(brokerName)
                                                  .withClientCode(clientCode)
                                                  .withBrokerAuthentication(userAdviseTransactionReceived.getBrokerAuthInformation());
        createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
        createdOrderBuilder.withInvestingPurpose(EQUITYSUBSCRIPTION);
        selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
        selections.userRootActor().forward(exitAdviseReceived, context());
              }));
      }).
        event(UserAdviseTransactionReceived.class, UserEquityAdvise.class, 
            (userAdviseTransactionReceived, userAdvise) -> {
          return (isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0)) ? true : false;
      },
          (userAdviseTransactionReceived, userAdvise) -> 
        {
        String localOrderId = ExitOrderIdPrefix + "-" + UUID.randomUUID().toString();
        ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
            userAdvise.getUserAdviseCompositeKey().getUsername(), 
            userAdviseTransactionReceived.getAdviserUsername(), 
            userAdviseTransactionReceived.getFundName(), 
            userAdviseTransactionReceived.getInvestingMode(), 
            userAdvise.getToken(),
            userAdviseTransactionReceived.getAdviseId(), 
            userAdviseTransactionReceived.getAbsoluteAllocation(),
            DateUtils.currentTimeInMillis(), 
            userAdviseTransactionReceived.getExitPrice(),
            localOrderId,
            getIssueType(),
            userAdviseTransactionReceived.getPurpose(),
            userAdviseTransactionReceived.getExitPrice().getPrice().getPrice(),
            ExitAdviseReceived.isNotASpecialSellAdvise(),
            ExitAdviseReceived.quantityNotYetSet(),
            userAdviseTransactionReceived.getBasketOrderCompositeId(),
            userAdviseTransactionReceived.getBrokerAuthInformation());
        return goTo(CompleteExitReceived).
        applying(exitAdviseReceived).
        andThen(exec(advise -> {
          String clientCode = advise.getClientCode();
          if (clientCode == null) 
            clientCode = getClientCodeFromUserRootActor(advise.getUserAdviseCompositeKey().getUsername()).getClientCode();
          BrokerName brokerName = advise.getBrokerName();
          if (brokerName == null) 
            brokerName = getBrokerNameOfTheInvestorWith(advise.getUserAdviseCompositeKey().getUsername(), advise.getUserAdviseCompositeKey().getInvestingMode()).getBrokerName();
          String symbol = advise.getSymbol();
          if (symbol == null)
            symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
          CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrderWith(symbol)
                                                    .withBrokerName(brokerName)
                                                    .withClientCode(clientCode);
          createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
          createdOrderBuilder.withInvestingPurpose(EQUITYSUBSCRIPTION);
          
          selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
          selections.userRootActor().forward(exitAdviseReceived, context());
        }));
        }).
        event(UserFundAdditionalSumAdvice.class, 
            (adviseIssued, userAdvise) -> 
        {
          UserFundAdditionalSumAdvice userFundAdviseIssued = fundLumpSumAdviceWithUndefinedExitParams(
              adviseIssued.getUsername(), 
              adviseIssued.getAdviserUsername(), 
              adviseIssued.getFundName(),
              adviseIssued.getInvestingMode(), 
              adviseIssued.getAdviseId(), 
              adviseIssued.getToken(),
              adviseIssued.getToken(),
              "",
              adviseIssued.getAbsoluteAllocation(), 
              adviseIssued.getMonetaryAllocationValue(), 
              adviseIssued.getEntryPrice(),
              adviseIssued.isUserBeingOnboarded(),
              adviseIssued.getBrokerName(),
              adviseIssued.getClientCode(),
              adviseIssued.getIssueType(),
              adviseIssued.isSpecialAdvise(),
              adviseIssued.getBasketOrderId(),
              adviseIssued.getBrokerAuthInformation());
        return goTo(ExpectingAdvisePrice).
        applying(userFundAdviseIssued).
        andThen(exec(advise -> {
          if(adviseIssued.isSpecialAdvise()){
            investingPurpose = CORPORATEACTION;
            self().tell( new AdviseAtSpecifiedPrice(adviseIssued.getEntryPrice()), ActorRef.noSender());
          }else{
            investingPurpose = EQUITYSUBSCRIPTION;
            getPriceFromBroadcast(advise);
          }
          }));    
        }).
        event(AdviseAtSpecifiedPrice.class, UserEquityAdvise.class, (adviseAtSpecifiedPrice, userAdvise) -> {
          String localOrderId = EntryOrderIdPrefix + HYPHEN + userAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN + userAdvise.getAdviseId() + HYPHEN + UUID.randomUUID().toString();
          return goTo(AdviseTokenPriceReceived).
          applying(new AdviseTokenPriceReceived(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              stateName(),
              adviseAtSpecifiedPrice.getEntryPrice(),
              localOrderId,
              getIssueType(),
              investingPurpose)).
          andThen(exec(advise -> {
            selections.equityOrderRootActor().tell(new CreatedOrder(
                advise.getUserAdviseCompositeKey().persistenceId(),
                userAdvise.getLatestIssuedAdviseOrder().get().getOrderId(),
                advise.getBrokerName(),
                advise.getCreatedBuyOrder().getPurpose(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                advise.getClientCode(),
                new OrderExecutionMetaData.OrderExecutionMetaDataBuilder( 
                    userAdvise.getToken(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getSide(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getIntegerQuantity(), 
                    OrderType.MARKET, 
                    OrderSegment.Equity)
                .withSymbol(advise.getSymbol())
                .withLimitPrice(adviseAtSpecifiedPrice.getEntryPrice().getPrice().getPrice())
                .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                .build(),  
                userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                BrokerAuthInformation.withNoBrokerAuth()), self());
          }));
        }).
        event(UpdateDividendEventWithFundNames.class, 
            (corporateEventUpdate, userAdvise) -> {
              String corporateEventDetails = "" + corporateEventUpdate.getEventDate()+ "_" +  corporateEventUpdate.getEventType();
              return (corporateEventUpdate.getWdId().equals(userAdvise.getToken())
                    && (!userAdvise.getCorporateEventDetailstoPaf().containsKey(corporateEventDetails)))? true : false ;
              },
            (corporateEventUpdate, userAdvise) -> {
              DividendEventReceived updateDividendEvent = new DividendEventReceived(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                  userAdvise.getAdviseId(),
                  getIssueType(),
                  stateName(),
                  corporateEventUpdate.getDividendAmount(),
                  corporateEventUpdate.getEventType(),
                  corporateEventUpdate.getEventDate(),
                  corporateEventUpdate.getWdId());
                                
              return stay().
                  applying(updateDividendEvent).
                  andThen(exec(advise -> {
                   selections.userRootActor().tell(new UserFundDividendReceived(userAdvise.getUserAdviseCompositeKey().getUsername(),
                                                                                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                                                                                userAdvise.getUserAdviseCompositeKey().getFund(),
                                                                                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                                                                                advise.integerQuantityYetToBeExited()* corporateEventUpdate.getDividendAmount()), 
                                                                                ActorRef.noSender());
                    }));
        }).
        event(UpdateSplitTypeEventWithFundNames.class,
            (corporateEventUpdate, userAdvise) -> {
              String corporateEventDetails = "" + corporateEventUpdate.getEventDate()+ "_" +  corporateEventUpdate.getEventType();
              return ((corporateEventUpdate.getEventType() == CorporateEventType.BONUS  || corporateEventUpdate.getEventType() == CorporateEventType.SPLIT))
                    && (corporateEventUpdate.getWdId().equals(userAdvise.getToken()))
                    && (!userAdvise.getCorporateEventDetailstoPaf().containsKey(corporateEventDetails))? true : false ;
              },
            (corporateEventUpdate, userAdvise) -> {
              String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
              UpdateCorporateEvent updateCorporateEvent = new UpdateCorporateEvent(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getUserAdviseCompositeKey().getAdviseId(),
                  stateName(),
                  corporateEventUpdate.getAdjustPaf(),
                  corporateEventUpdate.getEventType(),
                  localOrderId,
                  corporateEventUpdate.getEventDate(),
                  corporateEventUpdate.getWdId(),
                  getIssueType(),
                  CORPORATEACTION,
                  UpdateCorporateEvent.executeAtZeroPrice());
              return goTo(CorporateActionOrderPlaced).
              applying(updateCorporateEvent).    
              andThen(exec(advise -> {
                String symbol = advise.getSymbol();
                if (symbol == null)
                  symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
                int newQuantity =0;
                if(updateCorporateEvent.getAdjustPaf() > 1)
                  newQuantity = (int) (advise.integerQuantityYetToBeExited()* updateCorporateEvent.getAdjustPaf()) - advise.integerQuantityYetToBeExited();
                else{
                  newQuantity = (int) (advise.integerQuantityYetToBeExited() - advise.integerQuantityYetToBeExited()* updateCorporateEvent.getAdjustPaf()) ;
                }
                selections.equityOrderRootActor().tell(new CreatedOrder(
                    advise.getUserAdviseCompositeKey().persistenceId(),
                    localOrderId,
                    advise.getBrokerName(),
                    Purpose.CORPORATEACTION,
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    userAdvise.getUserAdviseCompositeKey().getUsername(),
                    advise.getClientCode(),
                    new OrderExecutionMetaData.OrderExecutionMetaDataBuilder( 
                        userAdvise.getToken(), 
                        checkCorporateEventOrderSide(corporateEventUpdate.getAdjustPaf()),
                        newQuantity, 
                        OrderType.MARKET, 
                        OrderSegment.Equity)
                    .withSymbol(advise.getSymbol())
                    .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                    .build(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                    BrokerAuthInformation.withNoBrokerAuth()), self());
              })); 
            })
        .event(MergerDemergerFundAdviseClosed.class,
            (mergerDemergerSellEvent, userAdvise) -> {
              String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
              String basketCompositeOrderId = "CE_MDS_"+userAdvise.getUserAdviseCompositeKey().getUsername()
                  + "_" + userAdvise.getUserAdviseCompositeKey().getFund() + "_" + valueOf(currentTimeInMillis()
                  + CompositeKeySeparator+valueOf(1));
              ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                  userAdvise.getUserAdviseCompositeKey().getFund(), 
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getToken(),
                  userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                  100., 
                  DateUtils.currentTimeInMillis(),
                  new PriceWithPaf(),
                  localOrderId,
                  getIssueType(),
                  CORPORATEACTION,
                  mergerDemergerSellEvent.getPrice(),
                  ExitAdviseReceived.isNotASpecialSellAdvise(),
                  ExitAdviseReceived.quantityNotYetSet(),
                  basketCompositeOrderId, BrokerAuthInformation.withNoBrokerAuth());
              return goTo(CompleteExitReceived).
              applying(exitAdviseReceived).    
              andThen(exec(advise -> {
                double allocationRemaining = (advise.integerQuantityYetToBeExited() / (advise.tradedEntryQuantity())) * advise.getIssueAdviceAbsoluteAllocation();
                context().parent().tell(new SellOrderDetails(advise.getToken(), advise.averageEntryPrice(),
                                        advise.integerQuantityYetToBeExited(), round(allocationRemaining), 
                                        advise.getIssueAdviseAllocationValue()), ActorRef.noSender());
                
                Order closeAdviseOrder = advise.getLatestSellOrder().get();
                int newQuantity = closeAdviseOrder.getIntegerQuantity();
                selections.equityOrderRootActor().tell(new CreatedOrder(
                    advise.getUserAdviseCompositeKey().persistenceId(),
                    localOrderId,
                    advise.getBrokerName(),
                    Purpose.CORPORATEACTION,
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    userAdvise.getUserAdviseCompositeKey().getUsername(),
                    advise.getClientCode(),
                    new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                        userAdvise.getToken(), 
                        closeAdviseOrder.getSide(), 
                        newQuantity, 
                        OrderType.MARKET, 
                        OrderSegment.Equity)
                    .withSymbol(advise.getSymbol())
                    .withLimitPrice(mergerDemergerSellEvent.getPrice())
                    .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                    .build(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                    BrokerAuthInformation.withNoBrokerAuth()), self());
              })); 
            }
            ).event(SpecialCircumstancesSell.class,
                (specialCircumstancesSell, userAdvise) -> {
                  return (userAdvise.integerQuantityYetToBeExited() - specialCircumstancesSell.getQuantity() == 0 ) ? true : false;
              },
                (specialCircumstancesSell, userAdvise) -> {
                  String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
                  double absoluteAllocation = 100.;
                  String basketCompositeOrderId = "SCS_FULL_"+userAdvise.getUserAdviseCompositeKey().getUsername()
                      + "_" + userAdvise.getUserAdviseCompositeKey().getFund() + "_" + valueOf(currentTimeInMillis()
                      + CompositeKeySeparator+valueOf(1));
                  ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
                      userAdvise.getUserAdviseCompositeKey().getUsername(), 
                      userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                      userAdvise.getUserAdviseCompositeKey().getFund(), 
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      userAdvise.getToken(),
                      userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                      absoluteAllocation, 
                      DateUtils.currentTimeInMillis(),
                      new PriceWithPaf(),
                      localOrderId,
                      getIssueType(),
                      CORPORATEACTION,
                      specialCircumstancesSell.getPrice(),
                      ExitAdviseReceived.isASpecialSellAdvise(),
                      specialCircumstancesSell.getQuantity(),
                      basketCompositeOrderId,
                      BrokerAuthInformation.withNoBrokerAuth());
                  return goTo(CompleteExitReceived).
                  applying(exitAdviseReceived).    
                  andThen(exec(advise -> {
                    String symbol = advise.getSymbol();
                    if (symbol == null)
                      symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
                    Order closeAdviseOrder = advise.getLatestSellOrder().get();
                    int newQuantity = closeAdviseOrder.getIntegerQuantity();
                    selections.equityOrderRootActor().tell(new CreatedOrder(
                        advise.getUserAdviseCompositeKey().persistenceId(),
                        localOrderId,
                        advise.getBrokerName(),
                        Purpose.CORPORATEACTION,
                        userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                        userAdvise.getUserAdviseCompositeKey().getUsername(),
                        advise.getClientCode(),
                        new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                            userAdvise.getToken(),
                            closeAdviseOrder.getSide(), 
                            newQuantity, 
                            OrderType.MARKET, 
                            OrderSegment.Equity)
                        .withSymbol(symbol)
                        .withLimitPrice(specialCircumstancesSell.getPrice())
                        .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                        .build(),
                        userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                        BrokerAuthInformation.withNoBrokerAuth()), self());
                  })); 
                }
                ).event(SpecialCircumstancesSell.class,
                    (specialCircumstancesSell, userAdvise) -> {
                      return (userAdvise.integerQuantityYetToBeExited() - specialCircumstancesSell.getQuantity() != 0 ) ? true : false;
                  },
                    (specialCircumstancesSell, userAdvise) -> {
                      String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
                      double absoluteAllocation = round(100. * specialCircumstancesSell.getQuantity()/userAdvise.integerQuantityYetToBeExited());
                      String basketCompositeOrderId = "SCS_PART_"+userAdvise.getUserAdviseCompositeKey().getUsername()
                          + "_" + userAdvise.getUserAdviseCompositeKey().getFund() + "_" + valueOf(currentTimeInMillis()
                          + CompositeKeySeparator+valueOf(1));
                      ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
                          userAdvise.getUserAdviseCompositeKey().getUsername(), 
                          userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                          userAdvise.getUserAdviseCompositeKey().getFund(), 
                          userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                          userAdvise.getToken(),
                          userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                          absoluteAllocation, 
                          DateUtils.currentTimeInMillis(),
                          new PriceWithPaf(),
                          localOrderId,
                          getIssueType(),
                          CORPORATEACTION,
                          specialCircumstancesSell.getPrice(),
                          ExitAdviseReceived.isASpecialSellAdvise(),
                          specialCircumstancesSell.getQuantity(),
                          basketCompositeOrderId,
                          BrokerAuthInformation.withNoBrokerAuth());
                      return goTo(PartialExitReceived).
                      applying(exitAdviseReceived).    
                      andThen(exec(advise -> {
                        String symbol = userAdvise.getSymbol();
                        if (symbol == null)
                          symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
                        
                        Order closeAdviseOrder = advise.getLatestSellOrder().get();
                        int newQuantity = closeAdviseOrder.getIntegerQuantity();
                        selections.equityOrderRootActor().tell(new CreatedOrder(
                            advise.getUserAdviseCompositeKey().persistenceId(),
                            localOrderId,
                            advise.getBrokerName(),
                            Purpose.CORPORATEACTION,
                            userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                            userAdvise.getUserAdviseCompositeKey().getUsername(),
                            advise.getClientCode(),
                            new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                                userAdvise.getToken(),
                                closeAdviseOrder.getSide(), 
                                newQuantity, 
                                OrderType.MARKET, 
                                OrderSegment.Equity)
                            .withSymbol(symbol)
                            .withLimitPrice(specialCircumstancesSell.getPrice())
                            .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                            .build(),
                            userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                            BrokerAuthInformation.withNoBrokerAuth()
                            ), self());
                      })); 
                    }
                    ).event(UpdateAdviseTradeStatus.class, 
                        (updateAdviseTradeStatus, userAdvise) -> {
       
                          selections.userRootActor().tell(new AdviseTradesExecuted(
                              userAdvise.getUserAdviseCompositeKey().getUsername(), 
                              userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                              userAdvise.getUserAdviseCompositeKey().getFund(),
                              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                              userAdvise.getAdviseId(),
                              stateName(),
                              userAdvise.getIssueAdviseAllocationValue(),
                              round(userAdvise.getLatestIssuedAdviseOrder().get().getQuantity() * userAdvise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice()),
                              userAdvise.getLatestIssuedAdviseOrder().get().getIntegerQuantity(),
                              userAdvise.getSymbol(),
                              userAdvise.getLatestIssuedAdviseOrder().get().getSide(),
                              userAdvise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice(),
                              getIssueType()), self());
                      return stay();
                      
                        })
        );
    
    when(CorporateActionOrderPlaced, create(5, MINUTES),
        matchEvent(OrderReceivedWithBroker.class,
            (orderReceivedWithBroker, userAdvise) -> 
        {
          return goTo(CorporateActionOrderPlacedWithBroker).
              applying(new AdviseOrderReceivedWithBroker(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(),
                  orderReceivedWithBroker.getLocalOrderId(),
                  orderReceivedWithBroker.getBrokerOrderId(),
                  getIssueType()
              )) 
              .andThen(exec(advise -> {
                selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderReceivedWithBroker, orderReceivedWithBroker, Equity.getIssueType()), self());
              }));
        }).
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdviseReceivedWithBrokerTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            selections.equityOrderRootActor().tell(new HasCorporateActionOrderBeenReceivedQuery(advise.getLatestCorpActionOrder().get().getOrderId()), self());
          }));
        }).
        event(OrderHasNotBeenReceived.class, 
            (orderHasNotBeenReceived, userAdvise) -> {
          return stay()
              .applying(new UserAdviseOrderHasNotBeenSuccessfullySent(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  orderHasNotBeenReceived.getClientOrderId(),
                  getIssueType())).
          andThen(exec(advise -> {
            CreatedOrderBuilder createdOrderBuilder = advise.getCurrentCorpActionOrder();
            createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
            createdOrderBuilder.withInvestingPurpose(CORPORATEACTION);
            selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
          }));
        })
        );
    
    when(CorporateActionOrderPlacedWithBroker, create(5, MINUTES),
        matchEvent(OrderPlacedWithExchange.class,
            (orderPlaced, userAdvise) -> 
        {
          return goTo(CorporateActionOrderPlacedWithExchange).
              applying(new AdviseOrderPlacedWithExchange(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(),
                  orderPlaced.getExchangeOrderId(),
                  orderPlaced.getBrokerOrderId(),
                  orderPlaced.getCtclId(), 
                  userAdvise.getToken(),
                  orderPlaced.getSide(),
                  orderPlaced.getQuantity(),
                  getIssueType()
              )).andThen(exec(advise -> {
                selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderPlaced, orderPlaced, Equity.getIssueType()), self());
              }));
        }).
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdviseOrderReceivedPlacedWithExchangeTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenPlacedWithExchangeQuery(advise.getLatestCorpActionOrder().get().getOrderId(),
                                                       advise.getLatestCorpActionOrder().get().getBrokerOrderId(),
                                                       advise.getLatestCorpActionOrder().get().getSide(),
                                                     (int) advise.getLatestCorpActionOrder().get().getQuantity()), self());
          }));
        })
        );
    
    when(CorporateActionOrderPlacedWithExchange, create(5, MINUTES),
        matchEvent(TradeExecuted.class,
            (tradeExecuted, userAdvise) -> 
        { 
          return (userAdvise.getCloseAdviseOrders().size() == 0 && !userAdvise.hasIssueAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? true : false;
        },(tradeExecuted, userAdvise) -> 
        {
          AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
              userAdvise.getUserAdviseCompositeKey().getUsername(), 
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              stateName(), 
              tradeExecuted.getOrderSide(),
              tradeExecuted.getTradeOrderId(),
              tradeExecuted.getExchangeOrderId(),
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              tradeExecuted.getQuantity(), 
              tradeExecuted.getPrice(), userAdvise.getToken(),
              getIssueType());
      return goTo(OpenAdviseOrderExecuted).
      applying(adviseOrderTradeExecuted).
      andThen(exec(advise -> {
                selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());
        
                    selections.userRootActor()
                        .tell(new AdviseTradesExecuted(userAdvise.getUserAdviseCompositeKey().getUsername(),
                            userAdvise.getUserAdviseCompositeKey().getAdviser(),
                            userAdvise.getUserAdviseCompositeKey().getFund(),
                            userAdvise.getUserAdviseCompositeKey().getInvestingMode(), advise.getAdviseId(),
                            stateName(), tradeExecuted.getQuantity() * tradeExecuted.getPrice().getPrice().getPrice(),
                            tradeExecuted.getQuantity() * tradeExecuted.getPrice().getPrice().getPrice(),
                            tradeExecuted.getQuantity(), userAdvise.getSymbol(), tradeExecuted.getOrderSide(),
                            tradeExecuted.getPrice().getPrice().getPrice(), getIssueType()), self());
                UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(userAdvise.getUserAdviseCompositeKey().getUsername(),
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getAdviser(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode()); 
                selections.userRootActor().tell(new CalculateUserFundComposition(userFundCompositeKey), self());
                
      }));
    }).
        event(TradeExecuted.class,
            (tradeExecuted, userAdvise) -> 
        {
          return (userAdvise.getCloseAdviseOrders().size() !=0 && !userAdvise.hasIssueAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? true : false;
        },(tradeExecuted, userAdvise) -> 
        {
          AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(), 
                  tradeExecuted.getOrderSide(),
                  tradeExecuted.getTradeOrderId(),
                  tradeExecuted.getExchangeOrderId(),
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  tradeExecuted.getQuantity(), 
                  tradeExecuted.getPrice(), userAdvise.getToken(),
                  getIssueType());
          return goTo(PartialExitAdviseOrderExecuted).
          applying(adviseOrderTradeExecuted).
          andThen(exec(advise -> {
            selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());
            
            selections.userRootActor().tell(new AdviseTradesExecuted(
                userAdvise.getUserAdviseCompositeKey().getUsername(), 
                userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                advise.getAdviseId(),
                stateName(),
                tradeExecuted.getQuantity() * tradeExecuted.getPrice().getPrice().getPrice(),
                tradeExecuted.getQuantity() * tradeExecuted.getPrice().getPrice().getPrice(),
                tradeExecuted.getQuantity(), userAdvise.getSymbol(), tradeExecuted.getOrderSide(),
                tradeExecuted.getPrice().getPrice().getPrice(),
                getIssueType()), self());
          }));
        }). 
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdviseOrderTradeExecutedTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenExecutedQuery(advise.getLatestCorpActionOrder().get().getOrderId(),
                advise.getToken(),
                advise.getLatestCorpActionOrder().get().getExchangeOrderId(),
                advise.getLatestCorpActionOrder().get().getSide(),
              (int) advise.getLatestCorpActionOrder().get().getQuantity()), self());
          }));
        })
        );

    when(PartialExitReceived, create(5, MINUTES),
          matchEvent(OrderReceivedWithBroker.class,
            (orderReceivedWithBroker, userAdvise) -> 
        {
          return goTo(PartialExitAdviseOrderReceivedWithBroker).
              applying(new AdviseOrderReceivedWithBroker(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(),
                  orderReceivedWithBroker.getLocalOrderId(),
                  orderReceivedWithBroker.getBrokerOrderId(),
                  getIssueType())) 
              .andThen(exec(advise -> {
                selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderReceivedWithBroker, orderReceivedWithBroker, Equity.getIssueType()), self());
                  }));
        }).
          event(OrderRejectedByBroker.class, 
            (orderRejected, userAdvise) -> {
              return goTo(OpenAdviseOrderExecuted)
                  .applying(new AdviseOrderRejected(
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      getIssueType())).
                   andThen(exec(advise -> {
                     selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderRejected, orderRejected, Equity.getIssueType()), self());
                     
                     selections.userRootActor().tell(new UserCloseAdviseOrderRejected(
                         advise.getUserAdviseCompositeKey().getUsername(),
                         advise.getUserAdviseCompositeKey().getAdviser(),
                         advise.getUserAdviseCompositeKey().getFund(),
                         advise.getAdviseId(),
                         advise.getUserAdviseCompositeKey().getInvestingMode()), self());
                   })); 
            }).
          event(StateTimeout$.class, 
              (stateTimeout, userAdvise) -> {
            return stay().
            applying(new AdviseReceivedWithBrokerTimedOut(
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getAdviseId(),
                getIssueType())).
            andThen(exec(advise -> {
              if(advise.getCurrentSellOrder().getPurpose() == CORPORATEACTION)
                selections.equityOrderRootActor().tell(new HasCorporateActionOrderBeenReceivedQuery(advise.getCurrentSellOrder().getClientOrderId()), self());
              else if(advise.getCurrentSellOrder().getPurpose() == EQUITYSUBSCRIPTION)
                selections.equityOrderRootActor().tell(new HasCreatedOrderBeenReceived(advise.getCurrentSellOrder().getClientOrderId()), self());
            }));
          }).
          event(OrderHasBeenReceived.class, 
              (orderHasBeenReceived, userAdvise) -> {
            return stay()
                .applying(new UserAdviseOrderHasBeenSuccessfullySent( 
                    userAdvise.getUserAdviseCompositeKey().getUsername(),
                    userAdvise.getUserAdviseCompositeKey().getAdviser(),
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    userAdvise.getAdviseId(),
                    orderHasBeenReceived.getClientOrderId(),
                    getIssueType()))
                .andThen(exec(advise -> {
                  selections.equityOrderRootActor().tell(new ProvideOrderBook(orderHasBeenReceived.getClientOrderId()), self());
                }));
          }).
          event(OrderHasNotBeenReceived.class, 
              (orderHasNotBeenReceived, userAdvise) -> {
            return stay()
                .applying(new UserAdviseOrderHasNotBeenSuccessfullySent(
                    userAdvise.getUserAdviseCompositeKey().getUsername(),
                    userAdvise.getUserAdviseCompositeKey().getAdviser(),
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    userAdvise.getAdviseId(),
                    orderHasNotBeenReceived.getClientOrderId(),
                    getIssueType()))
                .andThen(exec(advise -> {
                  CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrder();
                  createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
                  createdOrderBuilder.withInvestingPurpose(advise.getCurrentSellOrder().getPurpose());
                  selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
          }));
      }));
    
    when(PartialExitAdviseOrderReceivedWithBroker, create(5, MINUTES),
        matchEvent(OrderPlacedWithExchange.class,
            (orderPlaced, userAdvise) -> 
        {
          return goTo(PartialExitAdviseOrderPlacedWithExchange).
          applying(new AdviseOrderPlacedWithExchange(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(),
                  orderPlaced.getExchangeOrderId(), 
                  orderPlaced.getBrokerOrderId(),
                  orderPlaced.getCtclId(), 
                  userAdvise.getToken(),
                  orderPlaced.getSide(),
                  orderPlaced.getQuantity(),
                  getIssueType()))
          .andThen(exec(advise -> {
            selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderPlaced, orderPlaced, Equity.getIssueType()), self());
                  }));
        }).
          event(OrderRejectedByExchange.class, 
            (orderRejected, userAdvise) -> {
              return goTo(OpenAdviseOrderExecuted)
                  .applying(new AdviseOrderRejected(
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      getIssueType())).
                   andThen(exec(advise -> {
                     selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderRejected, orderRejected, Equity.getIssueType()), self());
                     
                     selections.userRootActor().tell(new UserCloseAdviseOrderRejected(
                         advise.getUserAdviseCompositeKey().getUsername(),
                         advise.getUserAdviseCompositeKey().getAdviser(),
                         advise.getUserAdviseCompositeKey().getFund(),
                         advise.getAdviseId(),
                         advise.getUserAdviseCompositeKey().getInvestingMode()), self());
                   }));  
            }).
          event(StateTimeout$.class, 
              (stateTimeout, userAdvise) -> {
            return stay().
            applying(new AdviseOrderReceivedPlacedWithExchangeTimedOut(
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getAdviseId(),
                getIssueType())).
            andThen(exec(advise -> {
              CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrder();
              if(createdOrderBuilder.getPurpose() == CORPORATEACTION)
                selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenPlacedWithExchangeQuery(createdOrderBuilder.getClientOrderId(),
                    advise.getLatestSellOrder().get().getBrokerOrderId(),
                    advise.getLatestSellOrder().get().getSide(),
                  (int) advise.getLatestSellOrder().get().getQuantity()), self());
              else if(createdOrderBuilder.getPurpose() == EQUITYSUBSCRIPTION)
                selections.equityOrderRootActor().tell(new ProvideOrderBook(createdOrderBuilder.getClientOrderId()), self());
            }));
          }));
    
    when(PartialExitAdviseOrderPlacedWithExchange, create(5, MINUTES),
        matchEvent(HackEventToLetFSMProcessPredicate.class,
            (hackEventToLetFSMProcessPredicate, userAdvise) -> {
              return stay();
            }).
        event(TradeExecuted.class,
            (tradeExecuted, userAdvise) -> {
              int currentTotalTradedQuantity = userAdvise.currentCloseAdviseTradedQuantity(tradeExecuted) + (int)tradeExecuted.getQuantity();
              int originalOrderQuantity = userAdvise.currentCloseAdviseOrderQuantity(tradeExecuted);
              return (currentTotalTradedQuantity == originalOrderQuantity 
                  && !userAdvise.hasCloseAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? false : true;
            }, (tradeExecuted, userAdvise) ->
        {
          AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(), 
                  tradeExecuted.getOrderSide(),
                  tradeExecuted.getTradeOrderId(),
                  tradeExecuted.getExchangeOrderId(),
                  userAdvise.getClientCode(),
                  tradeExecuted.getIntegerQuantity(), 
                  tradeExecuted.getPrice(), userAdvise.getToken(),
                  getIssueType());
          return stay().
          applying(adviseOrderTradeExecuted).
          andThen(exec(advise -> {
                    selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());
                    
                    if (advise.currentCloseAdviseOrderQuantity(tradeExecuted) == advise.currentCloseAdviseTradedQuantity(tradeExecuted)) {
                    selections.userRootActor().tell(closeAdviseTradesExecutedWithExecutionDetails(
                        userAdvise.getUserAdviseCompositeKey().getUsername(), 
                        userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                        userAdvise.getUserAdviseCompositeKey().getFund(),
                        userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                        advise.getAdviseId(),
                        stateName(),
                        round(advise.currentCloseAdviseTradedQuantity(tradeExecuted) * advise.currentCloseAdviseAverageExitPrice(tradeExecuted)),
                        advise.currentCloseAdviseTradedQuantity(tradeExecuted),
                        advise.getSymbol(),
                        tradeExecuted.getOrderSide(),
                        advise.currentCloseAdviseAverageExitPrice(tradeExecuted),
                        getIssueType()), 
                        self());
                    }
          }));
        }).
        event(TradeExecuted.class,
            (tradeExecuted, userAdvise) -> {
              int currentTotalTradedQuantity = userAdvise.currentCloseAdviseTradedQuantity(tradeExecuted) + (int)tradeExecuted.getQuantity();
              int originalOrderQuantity = userAdvise.currentCloseAdviseOrderQuantity(tradeExecuted);
              return (currentTotalTradedQuantity == originalOrderQuantity 
                  && !userAdvise.hasCloseAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? true : false;
            }, (tradeExecuted, userAdvise) ->
        {
          AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(), 
                  tradeExecuted.getOrderSide(),
                  tradeExecuted.getTradeOrderId(),
                  tradeExecuted.getExchangeOrderId(),
                  userAdvise.getClientCode(),
                  tradeExecuted.getIntegerQuantity(), 
                  tradeExecuted.getPrice(), 
                  userAdvise.getToken(),
                  getIssueType());
          return goTo(PartialExitAdviseOrderExecuted).
          applying(adviseOrderTradeExecuted).
          andThen(exec(advise -> {
            selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());
                    if (advise.currentCloseAdviseOrderQuantity(tradeExecuted) == advise.currentCloseAdviseTradedQuantity(tradeExecuted)) {
                    selections.userRootActor().tell(closeAdviseTradesExecutedWithExecutionDetails(
                        userAdvise.getUserAdviseCompositeKey().getUsername(), 
                        userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                        userAdvise.getUserAdviseCompositeKey().getFund(),
                        userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                        advise.getAdviseId(),
                        stateName(),
                        round(advise.currentCloseAdviseTradedQuantity(tradeExecuted) * advise.currentCloseAdviseAverageExitPrice(tradeExecuted)),
                        advise.currentCloseAdviseTradedQuantity(tradeExecuted),
                        advise.getSymbol(),
                        tradeExecuted.getOrderSide(),
                        advise.currentCloseAdviseAverageExitPrice(tradeExecuted),
                        getIssueType()), 
                        self());
                    }
            }));
              }).
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdviseOrderTradeExecutedTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrder();
            if(createdOrderBuilder.getPurpose() == CORPORATEACTION)
              selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenExecutedQuery(createdOrderBuilder.getClientOrderId(),
                  advise.getToken(),
                  advise.getLatestSellOrder().get().getExchangeOrderId(),
                  advise.getLatestSellOrder().get().getSide(),
                (int) advise.getLatestSellOrder().get().getQuantity()), self());
            else if(createdOrderBuilder.getPurpose() == EQUITYSUBSCRIPTION)
              selections.equityOrderRootActor().tell(new ProvideTradeBook(advise.getCurrentSellOrder().getClientOrderId()), self());
          }));                                            
          }));
            
    
    when(PartialExitAdviseOrderExecuted, 
        matchEvent(HackEventToLetFSMProcessPredicate.class, UserEquityAdvise.class, 
            (hackEventToLetFSMProcessPredicate, userAdvise) -> {
      return stay();
    }).
        event(UserAdviseTransactionReceived.class, 
            (userAdviseTransactionReceived, userAdvise) -> {
          return (userAdviseTransactionReceived.getUserAcceptanceMode().equals(Manual) && userAdviseTransactionReceived.getUserResponseType().equals(Awaiting)) ? true : false;
        }, (userAdviseTransactionReceived, userAdvise) -> 
      {
        int currentPendingQuantity = userAdvise.quantityInPendingPartialCloses();
        return stay()
            .applying(userAdviseTransactionReceived)
            .andThen(exec(advise -> {
              int calculatedNumberOfCloseAdviseShares = advise.calculateNumberOfCloseAdviseShares(advise.integerQuantityYetToBeExited(),
                  userAdviseTransactionReceived.getAbsoluteAllocation()) - currentPendingQuantity;
              if (calculatedNumberOfCloseAdviseShares >= 1) {
                String symbol = getSymbolFor(userAdvise.getToken());
                UserResponseParameters userResponseParameters = new UserResponseParameters.UserResponseParametersBuilder(
                    advise.getAdviseId(), 
                    Close, 
                    advise.getUserAdviseCompositeKey().getUsername(),
                    advise.getUserAdviseCompositeKey().getFund(),
                    advise.getUserAdviseCompositeKey().getAdviser(),
                    userAdvise.getClientCode(), 
                    userAdvise.getBrokerName().getBrokerName(),
                    getExchangeFromWdId(userAdvise.getToken()),
                    userAdvise.getToken(),
                    symbol,
                    advise.getUserAdviseCompositeKey().getInvestingMode(),
                    Awaiting,
                    userAdviseTransactionReceived.getExitDate())
                    .withAbsoluteAllocation(userAdviseTransactionReceived.getAbsoluteAllocation())
                    .withAllocationValue(0.)
                    .withNumberOfShares(calculatedNumberOfCloseAdviseShares)
                    .build();
                selections.userRootActor().forward(userResponseParameters, context());
              } else {
                log.info("Calculated close advise number of shares for advise Id: " + advise.getAdviseId() + " is " + calculatedNumberOfCloseAdviseShares + 
                    ". Hence no action is being taken!");
              }
            }));
      }).
        event(UserResponseReceived.class, 
            (userResponseReceived, userAdvise) -> {
              UserAdviseTransactionReceived userTransactionReceived = new UserAdviseTransactionReceived.UserAdviseTransactionReceivedBuilder(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                  userResponseReceived.getAdviseId(),
                  getIssueType())
                  .withAbsoluteAllocation(userResponseReceived.getAbsoluteAllocation())
                  .withExitPrice(getCurrentCloseAdvisePrice(userAdvise))
                  .withExitTime(userResponseReceived.getInvestmentDate())
                  .withUserAcceptanceMode(Manual)
                  .withUserResponseType(Approve)
                  .withBrokerAuthentication(userResponseReceived.getBrokerAuthInformation())
                  .build();
              
              return stay().
                  applying(userTransactionReceived).
                  andThen(exec(advise -> {
                    self().tell(userTransactionReceived, self());
                    }));
        }).
        event(UserAdviseTransactionReceived.class,  
            (userAdviseTransactionReceived, userAdvise) -> {
              int calculatedNumberOfCloseAdviseShares = userAdvise.calculateNumberOfCloseAdviseShares(userAdvise.integerQuantityYetToBeExited(), 
                  userAdviseTransactionReceived.getAbsoluteAllocation());
          return (!isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0) && calculatedNumberOfCloseAdviseShares >= 1) ? true : false;
        }, (userAdviseTransactionReceived, userAdvise) -> 
      {
      String localOrderId = ExitOrderIdPrefix + "-" + UUID.randomUUID().toString();
      ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
          userAdviseTransactionReceived.getUsername(), 
          userAdviseTransactionReceived.getAdviserUsername(), 
          userAdviseTransactionReceived.getFundName(), 
          userAdviseTransactionReceived.getInvestingMode(), 
          userAdvise.getToken(),
          userAdviseTransactionReceived.getAdviseId(), 
          userAdviseTransactionReceived.getAbsoluteAllocation(), 
          DateUtils.currentTimeInMillis(), 
          userAdviseTransactionReceived.getExitPrice(),
          localOrderId,
          getIssueType(),
          userAdviseTransactionReceived.getPurpose(),
          userAdviseTransactionReceived.getExitPrice().getPrice().getPrice(),
          ExitAdviseReceived.isNotASpecialSellAdvise(),
          ExitAdviseReceived.quantityNotYetSet(),
          userAdviseTransactionReceived.getBasketOrderCompositeId(),
          userAdviseTransactionReceived.getBrokerAuthInformation());
      return goTo(PartialExitReceived).
      applying(exitAdviseReceived).
      andThen(exec(advise -> {
        String clientCode = advise.getClientCode();
        if (clientCode == null) 
          clientCode = getClientCodeFromUserRootActor(advise.getUserAdviseCompositeKey().getUsername()).getClientCode();
        BrokerName brokerName = advise.getBrokerName();
        if (brokerName == null) 
          brokerName = getBrokerNameOfTheInvestorWith(advise.getUserAdviseCompositeKey().getUsername(), advise.getUserAdviseCompositeKey().getInvestingMode()).getBrokerName();
        String symbol = advise.getSymbol();
        if (symbol == null)
          symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
        CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrderWith(symbol)
                                                  .withBrokerName(brokerName)
                                                  .withClientCode(clientCode);
        createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
        createdOrderBuilder.withInvestingPurpose(EQUITYSUBSCRIPTION);
        selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
        selections.userRootActor().forward(exitAdviseReceived, context());
              }));
      }).
        event(UserAdviseTransactionReceived.class, UserEquityAdvise.class,  
            (userAdviseTransactionReceived, userAdvise) -> {
          return (isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0)) ? true : false;
      },
          (userAdviseTransactionReceived, userAdvise) -> 
        {
        String localOrderId = ExitOrderIdPrefix + "-" + UUID.randomUUID().toString();
        
        ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
            userAdvise.getUserAdviseCompositeKey().getUsername(), 
            userAdviseTransactionReceived.getAdviserUsername(), 
            userAdviseTransactionReceived.getFundName(), 
            userAdviseTransactionReceived.getInvestingMode(), 
            userAdvise.getToken(),
            userAdviseTransactionReceived.getAdviseId(), 
            userAdviseTransactionReceived.getAbsoluteAllocation(),
            DateUtils.currentTimeInMillis(), 
            userAdviseTransactionReceived.getExitPrice(),
            localOrderId,
            getIssueType(),
            userAdviseTransactionReceived.getPurpose(),
            userAdviseTransactionReceived.getExitPrice().getPrice().getPrice(),
            ExitAdviseReceived.isNotASpecialSellAdvise(),
            ExitAdviseReceived.quantityNotYetSet(),
            userAdviseTransactionReceived.getBasketOrderCompositeId(),
            userAdviseTransactionReceived.getBrokerAuthInformation());
        return goTo(CompleteExitReceived).
        applying(exitAdviseReceived).
        andThen(exec(advise -> {
          String clientCode = advise.getClientCode();
          if (clientCode == null) 
            clientCode = getClientCodeFromUserRootActor(advise.getUserAdviseCompositeKey().getUsername()).getClientCode();
          BrokerName brokerName = advise.getBrokerName();
          if (brokerName == null) 
            brokerName = getBrokerNameOfTheInvestorWith(advise.getUserAdviseCompositeKey().getUsername(), advise.getUserAdviseCompositeKey().getInvestingMode()).getBrokerName();
          String symbol = advise.getSymbol();
          if (symbol == null)
            symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
          CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrderWith(symbol)
                                                    .withBrokerName(brokerName)
                                                    .withClientCode(clientCode);
          createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
          createdOrderBuilder.withInvestingPurpose(EQUITYSUBSCRIPTION);
          selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
          selections.userRootActor().forward(exitAdviseReceived, context());
        }));
    }).
        event(UserFundAdditionalSumAdvice.class, 
            (adviseIssued, userAdvise) -> 
        {
          UserFundAdditionalSumAdvice userFundAdviseIssued = fundLumpSumAdviceWithUndefinedExitParams(
              adviseIssued.getUsername(), 
              adviseIssued.getAdviserUsername(), 
              adviseIssued.getFundName(),
              adviseIssued.getInvestingMode(), 
              adviseIssued.getAdviseId(), 
              adviseIssued.getToken(),
              adviseIssued.getToken(),
              "",
              adviseIssued.getAbsoluteAllocation(), 
              adviseIssued.getMonetaryAllocationValue(), 
              adviseIssued.getEntryPrice(),
              adviseIssued.isUserBeingOnboarded(),
              adviseIssued.getBrokerName(),
              adviseIssued.getClientCode(),
              adviseIssued.getIssueType(),
              adviseIssued.isSpecialAdvise(),
              adviseIssued.getBasketOrderId(),
              adviseIssued.getBrokerAuthInformation());
        return goTo(ExpectingAdvisePrice).
        applying(userFundAdviseIssued).
        andThen(exec(advise -> {
          if(adviseIssued.isSpecialAdvise()){
            investingPurpose = CORPORATEACTION;
            self().tell( new AdviseAtSpecifiedPrice(adviseIssued.getEntryPrice()), ActorRef.noSender());
          }else{
            investingPurpose = EQUITYSUBSCRIPTION;
            getPriceFromBroadcast(advise);
          }
          }));    
        }).
        event(AdviseAtSpecifiedPrice.class, UserEquityAdvise.class, (adviseAtSpecifiedPrice, userAdvise) -> {
          String localOrderId = EntryOrderIdPrefix + HYPHEN + userAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN + userAdvise.getAdviseId() + HYPHEN + UUID.randomUUID().toString();
          return goTo(AdviseTokenPriceReceived).
          applying(new AdviseTokenPriceReceived(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              stateName(),
              adviseAtSpecifiedPrice.getEntryPrice(),
              localOrderId,
              getIssueType(),
              investingPurpose)).
          andThen(exec(advise -> {
            selections.equityOrderRootActor().tell(new CreatedOrder(
                advise.getUserAdviseCompositeKey().persistenceId(),
                userAdvise.getLatestIssuedAdviseOrder().get().getOrderId(),
                advise.getBrokerName(),
                investingPurpose,
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                advise.getClientCode(),
                new OrderExecutionMetaData.OrderExecutionMetaDataBuilder( 
                    userAdvise.getToken(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getSide(), 
                    userAdvise.getLatestIssuedAdviseOrder().get().getIntegerQuantity(), 
                    OrderType.MARKET, 
                    OrderSegment.Equity)
                .withSymbol(advise.getSymbol())
                .withLimitPrice(adviseAtSpecifiedPrice.getEntryPrice().getPrice().getPrice())
                .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                .build(),  
                userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                userAdvise.getBrokerAuthInformation()
                ), self());
            
          }));
        }).
        event(UpdateDividendEventWithFundNames.class, 
            (corporateEventUpdate, userAdvise) -> {
              String corporateEventDetails = "" + corporateEventUpdate.getEventDate()+ "_" +  corporateEventUpdate.getEventType();
              return ((corporateEventUpdate.getEventType() == CorporateEventType.DIVIDEND)
                    && (corporateEventUpdate.getWdId().equals(userAdvise.getToken()))
                    && (!userAdvise.getCorporateEventDetailstoPaf().containsKey(corporateEventDetails)))? true : false ;
              },
            (corporateEventUpdate, userAdvise) -> {
              DividendEventReceived dividendEventReceived = new DividendEventReceived(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                  userAdvise.getAdviseId(),
                  getIssueType(),
                  stateName(),
                  corporateEventUpdate.getDividendAmount(),
                  corporateEventUpdate.getEventType(),
                  corporateEventUpdate.getEventDate(),
                  corporateEventUpdate.getWdId());
                                
              return stay().
                  applying(dividendEventReceived).
                  andThen(exec(advise -> {
                   selections.userRootActor().tell(new UserFundDividendReceived(userAdvise.getUserAdviseCompositeKey().getUsername(),
                                                                                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                                                                                userAdvise.getUserAdviseCompositeKey().getFund(),
                                                                                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                                                                                advise.integerQuantityYetToBeExited() * corporateEventUpdate.getDividendAmount()), 
                                                                                ActorRef.noSender());
                    }));
        }).
        event(UpdateSplitTypeEventWithFundNames.class,
        (corporateEventUpdate, userAdvise) -> {
          String corporateEventDetails = "" + corporateEventUpdate.getEventDate()+ "_" +  corporateEventUpdate.getEventType();
          return ((corporateEventUpdate.getEventType() == CorporateEventType.BONUS  || corporateEventUpdate.getEventType() == CorporateEventType.SPLIT))
                && (corporateEventUpdate.getWdId().equals(userAdvise.getToken()))
                && (!userAdvise.getCorporateEventDetailstoPaf().containsKey(corporateEventDetails))? true : false ;
          },
        (corporateEventUpdate, userAdvise) -> {
          String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
          UpdateCorporateEvent updateCorporateEvent = new UpdateCorporateEvent(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getUserAdviseCompositeKey().getAdviseId(),
              stateName(),
              corporateEventUpdate.getAdjustPaf(),
              corporateEventUpdate.getEventType(),
              localOrderId,
              corporateEventUpdate.getEventDate(),
              corporateEventUpdate.getWdId(),
              getIssueType(),
              CORPORATEACTION,
              UpdateCorporateEvent.executeAtZeroPrice());
          return goTo(CorporateActionOrderPlaced).
          applying(updateCorporateEvent).    
          andThen(exec(advise -> {
            int newQuantity =0;
            if(updateCorporateEvent.getAdjustPaf() > 1)
              newQuantity = (int) (advise.integerQuantityYetToBeExited()* updateCorporateEvent.getAdjustPaf()) - advise.integerQuantityYetToBeExited();
            else{
              newQuantity = (int) (advise.integerQuantityYetToBeExited() - advise.integerQuantityYetToBeExited()* updateCorporateEvent.getAdjustPaf()) ;
            }
            selections.equityOrderRootActor().tell(new CreatedOrder(
                advise.getUserAdviseCompositeKey().persistenceId(),
                localOrderId,
                advise.getBrokerName(),
                Purpose.CORPORATEACTION,
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                advise.getClientCode(),
                new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                userAdvise.getToken(),
                checkCorporateEventOrderSide(corporateEventUpdate.getAdjustPaf()),
                newQuantity, 
                OrderType.MARKET, 
                OrderSegment.Equity)
                .withSymbol(advise.getSymbol())
                .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                .build(), 
                userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                BrokerAuthInformation.withNoBrokerAuth()
                ), self());
          })); 
        }
        )
                       
        .event(MergerDemergerFundAdviseClosed.class,
            (mergerDemergerSellEvent, userAdvise) -> {
              String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
              String basketCompositeOrderId = "CE_MDS_"+userAdvise.getUserAdviseCompositeKey().getUsername()
                  + "_" + userAdvise.getUserAdviseCompositeKey().getFund() + "_" + valueOf(currentTimeInMillis()
                  + CompositeKeySeparator+valueOf(1));
              ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                  userAdvise.getUserAdviseCompositeKey().getFund(), 
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getToken(),
                  userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                  100., 
                  DateUtils.currentTimeInMillis(),
                  new PriceWithPaf(),
                  localOrderId,
                  getIssueType(),
                  CORPORATEACTION,
                  mergerDemergerSellEvent.getPrice(),
                  ExitAdviseReceived.isNotASpecialSellAdvise(),
                  ExitAdviseReceived.quantityNotYetSet(),
                  basketCompositeOrderId,
                  BrokerAuthInformation.withNoBrokerAuth());
              return goTo(CompleteExitReceived).
              applying(exitAdviseReceived).    
              andThen(exec(advise -> {
                double allocationRemaining = (advise.integerQuantityYetToBeExited() / (advise.tradedEntryQuantity())) * advise.getIssueAdviceAbsoluteAllocation();
                context().parent().tell(new SellOrderDetails(advise.getToken(), advise.averageEntryPrice(), 
                                        advise.integerQuantityYetToBeExited(), round(allocationRemaining), 
                                        advise.getIssueAdviseAllocationValue()), ActorRef.noSender());
                
                Order closeAdviseOrder = advise.getLatestSellOrder().get();
                int newQuantity = closeAdviseOrder.getIntegerQuantity();
                selections.equityOrderRootActor().tell(new CreatedOrder(
                    advise.getUserAdviseCompositeKey().persistenceId(),
                    localOrderId,
                    advise.getBrokerName(),
                    Purpose.CORPORATEACTION,
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    userAdvise.getUserAdviseCompositeKey().getUsername(),
                    advise.getClientCode(),
                    new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                        userAdvise.getToken(), 
                        closeAdviseOrder.getSide(), 
                        newQuantity, 
                        OrderType.MARKET, 
                        OrderSegment.Equity)
                    .withSymbol(userAdvise.getSymbol())
                    .withLimitPrice(mergerDemergerSellEvent.getPrice())
                    .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                    .build(),
                    userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                    BrokerAuthInformation.withNoBrokerAuth()
                    ), self());
              })); 
            }
            ).event(SpecialCircumstancesSell.class,
                (specialCircumstancesSell, userAdvise) -> {
                  return (userAdvise.integerQuantityYetToBeExited() - specialCircumstancesSell.getQuantity() == 0) ? true : false;
              },
                (specialCircumstancesSell, userAdvise) -> {
                  String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
                  String basketCompositeOrderId = "SCS_FULL_"+userAdvise.getUserAdviseCompositeKey().getUsername()
                      + "_" + userAdvise.getUserAdviseCompositeKey().getFund() + "_" + valueOf(currentTimeInMillis()
                      + CompositeKeySeparator+valueOf(1));
                  ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
                      userAdvise.getUserAdviseCompositeKey().getUsername(), 
                      userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                      userAdvise.getUserAdviseCompositeKey().getFund(), 
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      userAdvise.getToken(),
                      userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                      100., 
                      DateUtils.currentTimeInMillis(),
                      new PriceWithPaf(),
                      localOrderId,
                      getIssueType(),
                      CORPORATEACTION,
                      specialCircumstancesSell.getPrice(),
                      ExitAdviseReceived.isASpecialSellAdvise(),
                      specialCircumstancesSell.getQuantity(),
                      basketCompositeOrderId,
                      BrokerAuthInformation.withNoBrokerAuth());
                  return goTo(CompleteExitReceived).
                  applying(exitAdviseReceived).    
                  andThen(exec(advise -> {
                    String symbol = advise.getSymbol();
                    if (symbol == null)
                      symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
                    Order closeAdviseOrder = advise.getLatestSellOrder().get();
                    int newQuantity = closeAdviseOrder.getIntegerQuantity();
                    selections.equityOrderRootActor().tell(new CreatedOrder(
                        advise.getUserAdviseCompositeKey().persistenceId(),
                        localOrderId,
                        advise.getBrokerName(),
                        Purpose.CORPORATEACTION,
                        userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                        userAdvise.getUserAdviseCompositeKey().getUsername(),
                        advise.getClientCode(),
                        new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                            userAdvise.getToken(),
                            closeAdviseOrder.getSide(), 
                            newQuantity, 
                            OrderType.MARKET, 
                            OrderSegment.Equity)
                        .withSymbol(symbol)
                        .withLimitPrice(specialCircumstancesSell.getPrice())
                        .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                        .build(),
                        userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                        BrokerAuthInformation.withNoBrokerAuth()
                        ), self());
                  })); 
                }
                ).event(SpecialCircumstancesSell.class,
                    (specialCircumstancesSell, userAdvise) -> {
                      return (userAdvise.integerQuantityYetToBeExited() - specialCircumstancesSell.getQuantity() != 0) ? true : false;
                  },
                    (specialCircumstancesSell, userAdvise) -> {
                      String localOrderId = CorporateEventIdPrefix + "-" + UUID.randomUUID().toString();
                      double absoluteAllocation = round(100. * specialCircumstancesSell.getQuantity() / userAdvise.integerQuantityYetToBeExited());
                      String basketCompositeOrderId = "SCS_PART_"+userAdvise.getUserAdviseCompositeKey().getUsername()
                          + "_" + userAdvise.getUserAdviseCompositeKey().getFund() + "_" + valueOf(currentTimeInMillis()
                          + CompositeKeySeparator+valueOf(1));
                      ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
                          userAdvise.getUserAdviseCompositeKey().getUsername(), 
                          userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                          userAdvise.getUserAdviseCompositeKey().getFund(), 
                          userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                          userAdvise.getToken(),
                          userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                          absoluteAllocation, 
                          DateUtils.currentTimeInMillis(),
                          new PriceWithPaf(),
                          localOrderId,
                          getIssueType(),
                          CORPORATEACTION,
                          specialCircumstancesSell.getPrice(),
                          ExitAdviseReceived.isASpecialSellAdvise(),
                          specialCircumstancesSell.getQuantity(),
                          basketCompositeOrderId,
                          BrokerAuthInformation.withNoBrokerAuth());
                      return goTo(PartialExitReceived).
                      applying(exitAdviseReceived).    
                      andThen(exec(advise -> {
                        String symbol = advise.getSymbol();
                        if (symbol == null)
                          symbol = getInstrumentFromUniverseRootActor(advise.getToken()).getInstrument().getSymbol();
                        Order closeAdviseOrder = advise.getLatestSellOrder().get();
                        int newQuantity = closeAdviseOrder.getIntegerQuantity();
                        selections.equityOrderRootActor().tell(new CreatedOrder(
                            advise.getUserAdviseCompositeKey().persistenceId(),
                            localOrderId,
                            advise.getBrokerName(),
                            Purpose.CORPORATEACTION,
                            userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                            userAdvise.getUserAdviseCompositeKey().getUsername(),
                            advise.getClientCode(),
                            new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                                userAdvise.getToken(), 
                                closeAdviseOrder.getSide(), 
                                newQuantity, 
                                OrderType.MARKET, 
                                OrderSegment.Equity)
                            .withSymbol(symbol)
                            .withLimitPrice(specialCircumstancesSell.getPrice())
                            .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                            .build(),
                            userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                            BrokerAuthInformation.withNoBrokerAuth()
                            ), self());
                      })); 
                    }).
                    event(UpdateAdviseTradeStatus.class, 
                        (updateAdviseTradeStatus, userAdvise) -> {
                          
                          selections.userRootActor().tell(new AdviseTradesExecuted(
                              userAdvise.getUserAdviseCompositeKey().getUsername(), 
                              userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                              userAdvise.getUserAdviseCompositeKey().getFund(),
                              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                              userAdvise.getAdviseId(),
                              stateName(),
                              userAdvise.getIssueAdviseAllocationValue(),
                              round(userAdvise.getLatestIssuedAdviseOrder().get().getQuantity() * userAdvise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice()),
                              userAdvise.getLatestIssuedAdviseOrder().get().getIntegerQuantity(),
                              userAdvise.getSymbol(),
                              userAdvise.getLatestIssuedAdviseOrder().get().getSide(),
                              userAdvise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice(),
                              getIssueType()), self());
                      return stay();
                      
                        })
        );

    
    when(CompleteExitReceived, create(5, MINUTES),
          matchEvent(OrderReceivedWithBroker.class,
            (orderReceivedWithBroker, userAdvise) -> 
        {
          return goTo(CompleteExitAdviseOrderReceivedWithBroker).
              applying(new AdviseOrderReceivedWithBroker(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  stateName(),
                  orderReceivedWithBroker.getLocalOrderId(),
                  orderReceivedWithBroker.getBrokerOrderId(),
                  getIssueType())) 
              .andThen(exec(advise -> {
                selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderReceivedWithBroker, orderReceivedWithBroker, Equity.getIssueType()), self());
                  }));
        }).
          event(OrderRejectedByBroker.class, 
            (orderRejected, userAdvise) -> {
              return goTo(OpenAdviseOrderExecuted)
                  .applying(new AdviseOrderRejected(
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      getIssueType())).
                   andThen(exec(advise -> {
                     selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderRejected, orderRejected, Equity.getIssueType()), self());
                     
                     selections.userRootActor().tell(new UserCloseAdviseOrderRejected(
                         advise.getUserAdviseCompositeKey().getUsername(),
                         advise.getUserAdviseCompositeKey().getAdviser(),
                         advise.getUserAdviseCompositeKey().getFund(),
                         advise.getAdviseId(),
                         advise.getUserAdviseCompositeKey().getInvestingMode()), self());
                   }));  
            }).
          event(StateTimeout$.class, 
              (stateTimeout, userAdvise) -> {
            return stay().
            applying(new AdviseReceivedWithBrokerTimedOut(
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getAdviseId(),
                getIssueType())).
            andThen(exec(advise -> {
              if(advise.getCurrentSellOrder().getPurpose() == CORPORATEACTION)
                selections.equityOrderRootActor().tell(new HasCorporateActionOrderBeenReceivedQuery(advise.getCurrentSellOrder().getClientOrderId()), self());
              else if(advise.getCurrentSellOrder().getPurpose() == EQUITYSUBSCRIPTION)
                selections.equityOrderRootActor().tell(new HasCreatedOrderBeenReceived(advise.getCurrentSellOrder().getClientOrderId()), self());
            }));
          }).
          event(OrderHasBeenReceived.class, 
              (orderHasBeenReceived, userAdvise) -> {
            return stay()
                .applying(new UserAdviseOrderHasBeenSuccessfullySent( 
                    userAdvise.getUserAdviseCompositeKey().getUsername(),
                    userAdvise.getUserAdviseCompositeKey().getAdviser(),
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    userAdvise.getAdviseId(),
                    orderHasBeenReceived.getClientOrderId(),
                    getIssueType()))
                .andThen(exec(advise -> {
                  selections.equityOrderRootActor().tell(new ProvideOrderBook(orderHasBeenReceived.getClientOrderId()), self());
                }));
          }).
          event(OrderHasNotBeenReceived.class, 
              (orderHasNotBeenReceived, userAdvise) -> {
            return stay()
                .applying(new UserAdviseOrderHasNotBeenSuccessfullySent(
                    userAdvise.getUserAdviseCompositeKey().getUsername(),
                    userAdvise.getUserAdviseCompositeKey().getAdviser(),
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    userAdvise.getAdviseId(),
                    orderHasNotBeenReceived.getClientOrderId(),
                    getIssueType()))
                .andThen(exec(advise -> {
                  CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrder();
                  createdOrderBuilder.withSenderPath(advise.getUserAdviseCompositeKey().persistenceId());
                  createdOrderBuilder.withInvestingPurpose(advise.getCurrentSellOrder().getPurpose());
                  selections.equityOrderRootActor().tell(createdOrderBuilder.build(), self());
          }));
      }));
    
    when(CompleteExitAdviseOrderReceivedWithBroker, create(5, MINUTES),
          matchEvent(OrderPlacedWithExchange.class,
            (orderPlaced, userAdvise) -> 
        {
          return goTo(CompleteExitAdviseOrderPlacedWithExchange).
          applying(new AdviseOrderPlacedWithExchange(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),              
                  userAdvise.getAdviseId(),
                  stateName(),
                  orderPlaced.getExchangeOrderId(), 
                  orderPlaced.getBrokerOrderId(),
                  orderPlaced.getCtclId(),
                  userAdvise.getToken(),
                  orderPlaced.getSide(),
                  orderPlaced.getQuantity(),
                  getIssueType()))
          .andThen(exec(advise -> {
            selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderPlaced, orderPlaced, Equity.getIssueType()), self());
                  }));
        }).
          event(OrderRejectedByExchange.class, 
            (orderRejected, userAdvise) -> {
              return goTo(OpenAdviseOrderExecuted)
                  .applying(new AdviseOrderRejected(
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      getIssueType())).
                   andThen(exec(advise -> {
                     selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + orderRejected, orderRejected, Equity.getIssueType()), self());
                     
                     selections.userRootActor().tell(new UserCloseAdviseOrderRejected(
                         advise.getUserAdviseCompositeKey().getUsername(),
                         advise.getUserAdviseCompositeKey().getAdviser(),
                         advise.getUserAdviseCompositeKey().getFund(),
                         advise.getAdviseId(),
                         advise.getUserAdviseCompositeKey().getInvestingMode()), self());
                   }));  
            }).
          event(StateTimeout$.class, 
              (stateTimeout, userAdvise) -> {
            return stay().
            applying(new AdviseOrderReceivedPlacedWithExchangeTimedOut(
                userAdvise.getUserAdviseCompositeKey().getUsername(),
                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                userAdvise.getAdviseId(),
                getIssueType())).
            andThen(exec(advise -> {
              CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrder();
              if(createdOrderBuilder.getPurpose() == CORPORATEACTION)
                selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenPlacedWithExchangeQuery(createdOrderBuilder.getClientOrderId(),
                    advise.getLatestSellOrder().get().getBrokerOrderId(),
                    advise.getLatestSellOrder().get().getSide(),
                  (int) advise.getLatestSellOrder().get().getQuantity()), self());
              else if(createdOrderBuilder.getPurpose() == EQUITYSUBSCRIPTION)
                selections.equityOrderRootActor().tell(new ProvideOrderBook(createdOrderBuilder.getClientOrderId()), self());
            }));
          }));
    
    when(CompleteExitAdviseOrderPlacedWithExchange, create(5, MINUTES),
        matchEvent(HackEventToLetFSMProcessPredicate.class,
            (hackEventToLetFSMProcessPredicate, userAdvise) -> {
              return stay();
            }).
        event(TradeExecuted.class,
            (tradeExecuted, userAdvise) -> {
              int currentTotalTradedQuantity = userAdvise.currentCloseAdviseTradedQuantity(tradeExecuted) + (int)tradeExecuted.getQuantity();
              int originalOrderQuantity = userAdvise.currentCloseAdviseOrderQuantity(tradeExecuted);
          return (currentTotalTradedQuantity == originalOrderQuantity 
              && !userAdvise.hasCloseAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? false : true;
        }, (tradeExecuted, userAdvise) ->
    {
      AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
              userAdvise.getUserAdviseCompositeKey().getUsername(), 
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              stateName(), 
              tradeExecuted.getOrderSide(),
              tradeExecuted.getTradeOrderId(),
              tradeExecuted.getExchangeOrderId(),
              userAdvise.getClientCode(),
              tradeExecuted.getQuantity(), 
              tradeExecuted.getPrice(), userAdvise.getToken(),
              getIssueType());
      return stay().
      applying(adviseOrderTradeExecuted).
      andThen(exec(advise -> {
        selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());
                
                if (advise.currentCloseAdviseOrderQuantity(tradeExecuted) == advise.currentCloseAdviseTradedQuantity(tradeExecuted)) {
                selections.userRootActor().tell(closeAdviseTradesExecutedWithExecutionDetails(
                    userAdvise.getUserAdviseCompositeKey().getUsername(), 
                    userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getAdviseId(),
                    stateName(),
                    round(advise.currentCloseAdviseTradedQuantity(tradeExecuted) * advise.currentCloseAdviseAverageExitPrice(tradeExecuted)),
                    advise.currentCloseAdviseTradedQuantity(tradeExecuted),
                    advise.getSymbol(),
                    tradeExecuted.getOrderSide(),
                    advise.currentCloseAdviseAverageExitPrice(tradeExecuted),
                    getIssueType()), 
                    self());
                }
      }));
    }).event(TradeExecuted.class,
        (tradeExecuted, userAdvise) -> {
          int currentTotalTradedQuantity = userAdvise.currentCloseAdviseTradedQuantity(tradeExecuted) + (int)tradeExecuted.getQuantity();
          int originalOrderQuantity = userAdvise.currentCloseAdviseOrderQuantity(tradeExecuted);
          return (currentTotalTradedQuantity == originalOrderQuantity && !userAdvise.hasCloseAdviceTradeBeenProcessed(tradeExecuted.getTradeOrderId())) ? true : false;
        }, (tradeExecuted, userAdvise) ->
    {
      AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted(
              userAdvise.getUserAdviseCompositeKey().getUsername(), 
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              stateName(), 
              tradeExecuted.getOrderSide(),
              tradeExecuted.getTradeOrderId(),
              tradeExecuted.getExchangeOrderId(),
              userAdvise.getClientCode(),
              tradeExecuted.getQuantity(), 
              tradeExecuted.getPrice(), userAdvise.getToken(),
              getIssueType());
      return goTo(CompleteExitAdviseOrderExecuted).
      applying(adviseOrderTradeExecuted).
      andThen(exec(advise -> {
        selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + tradeExecuted, tradeExecuted, Equity.getIssueType()), self());          
                if (advise.currentCloseAdviseOrderQuantity(tradeExecuted) == advise.currentCloseAdviseTradedQuantity(tradeExecuted)) {
                selections.userRootActor().tell(closeAdviseTradesExecutedWithExecutionDetails(
                    userAdvise.getUserAdviseCompositeKey().getUsername(), 
                    userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getAdviseId(),
                    stateName(),
                    round(advise.currentCloseAdviseTradedQuantity(tradeExecuted) * advise.currentCloseAdviseAverageExitPrice(tradeExecuted)),
                    advise.currentCloseAdviseTradedQuantity(tradeExecuted),
                    advise.getSymbol(),
                    tradeExecuted.getOrderSide(),
                    advise.currentCloseAdviseAverageExitPrice(tradeExecuted),
                    getIssueType()), 
                    self());
                }
                self().tell(new CurrentUserAdviseFlushedDueToExit(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviseId(),
                  getIssueType()), self());
        }));
      }).
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new AdviseOrderTradeExecutedTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            CreatedOrderBuilder createdOrderBuilder = advise.getCurrentSellOrder();
            if(createdOrderBuilder.getPurpose() == CORPORATEACTION)
              selections.equityOrderRootActor().tell(new HasCorpActionOrderBeenExecutedQuery(createdOrderBuilder.getClientOrderId(),
                  advise.getToken(),
                  advise.getLatestSellOrder().get().getExchangeOrderId(),
                  advise.getLatestSellOrder().get().getSide(),
                (int) advise.getLatestSellOrder().get().getQuantity()), self());
            else if(createdOrderBuilder.getPurpose() == EQUITYSUBSCRIPTION)
              selections.equityOrderRootActor().tell(new ProvideTradeBook(createdOrderBuilder.getClientOrderId()), self());
          }));
      }));
    
    when(CompleteExitAdviseOrderExecuted, create(1, MINUTES),
        matchEvent(CurrentUserAdviseFlushedDueToExit.class, 
            (currentAdviseFlushed , userAdvise) -> {
          return goTo(UserAdviseIssued).
                 applying(currentAdviseFlushed).
                 using(new UserEquityAdvise()); 
        }).
        event(StateTimeout$.class, 
            (stateTimeout, userAdvise) -> {
          return stay().
          applying(new CompleteExitAdviseOrderExecutedTimedOut(
              userAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdvise.getUserAdviseCompositeKey().getAdviser(),
              userAdvise.getUserAdviseCompositeKey().getFund(),
              userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              userAdvise.getAdviseId(),
              getIssueType())).
          andThen(exec(advise -> {
            self().tell(new CurrentUserAdviseFlushedDueToExit(
                userAdvise.getUserAdviseCompositeKey().getUsername(), 
                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                userAdvise.getUserAdviseCompositeKey().getFund(),
                userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                userAdvise.getUserAdviseCompositeKey().getAdviseId(),
                getIssueType()), self());
          }));
      }));
      
    
    
    whenUnhandled(
        matchEvent(ExchangeOrderUpdateEvent.class,
              (exchangeOrderUpdateEvent, userAdvise) -> {
                return !(exchangeOrderUpdateEvent instanceof OrderTerminatedAtEOD) ? true : false;
              },
              (exchangeOrderUpdateEvent, userAdvise) -> {
          log.warn("Received unhandled event " + exchangeOrderUpdateEvent.toString() + " in state " + stateName() + " for token " + userAdvise.getToken() + " with adviseId " + userAdvise.getAdviseId());
          selections.userRootActor().tell(new ExchangeOrderUpdateAcknowledged(persistenceId, "Received exchange update " + exchangeOrderUpdateEvent, exchangeOrderUpdateEvent, Equity.getIssueType()), self());
          return stay();
        }).
        event(CalculateUserAdviseComposition.class,
            (calculateUserAdviseComposition, userAdvise) -> {
          selections.userRootActor().tell(new UpdateUserFundCompositionList(userAdvise.getUserAdviseCompositeKey(),
              userAdvise.integerQuantityYetToBeExited()), self());
          return stay();
        }).
        event(UserAdviseTransactionReceived.class, (tradeExecuted, userAdvise) -> {
          if(stateName().equals(UserAdviseIssued))
            selections.userRootActor()
                .tell(closeAdviseTradesExecutedWithExecutionDetails(userAdvise.getUserAdviseCompositeKey().getUsername(),
                    userAdvise.getUserAdviseCompositeKey().getAdviser(), userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(), userAdvise.getAdviseId(), stateName(), 0d,
                    0, userAdvise.getSymbol(), SELL, 0, getIssueType()), self());
          return stay();
        }).
        event(OrderTerminatedAtEOD.class, 
            (orderTerminatedAtEOD, userAdvise) -> {
              return isActorStateValidToTerminateBuyOrder() ? true : false;
            },
            (orderTerminatedAtEOD, userAdvise) -> {
          return goTo(OpenAdviseOrderExecuted)
              .applying(new CurrentActiveOrderTerminated(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  getIssueType())).
              andThen(exec(advise -> {
                
                selections.userRootActor().tell(new AdviseTradesExecuted(
                    userAdvise.getUserAdviseCompositeKey().getUsername(), 
                    userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getAdviseId(),
                    stateName(),
                    advise.getIssueAdviseAllocationValue(),
                    round(advise.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity() * advise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice()),
                    advise.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity(),
                    advise.getSymbol(),
                    orderTerminatedAtEOD.getOrderSide(),
                    advise.getLatestIssuedAdviseOrder().get().getAverageTradedPrice(),
                    getIssueType()), self());
              }));
        }).event(OrderTerminatedAtEOD.class, 
            (orderTerminatedAtEOD, userAdvise) -> {
              return isActorStateValidToTerminateSellOrder() ? true : false;
            },
            (orderTerminatedAtEOD, userAdvise) -> {
          return goTo(OpenAdviseOrderExecuted)
              .applying(new CurrentActiveOrderTerminated(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(),
                  getIssueType())).
              andThen(exec(advise -> {
                
                TradeExecuted dummyTradeExecuted = new TradeExecuted(new DestinationAddress(""),orderTerminatedAtEOD.getExchangeOrderId(), 
                                                    "", OrderSide.SELL, PriceWithPaf.noPrice(userAdvise.getToken()), 0);
                selections.userRootActor().tell(closeAdviseTradesExecutedWithExecutionDetails(
                    userAdvise.getUserAdviseCompositeKey().getUsername(), 
                    userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                    userAdvise.getUserAdviseCompositeKey().getFund(),
                    userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getAdviseId(),
                    stateName(),
                    round(advise.currentCloseAdviseTradedQuantity(dummyTradeExecuted) * advise.currentCloseAdviseAverageExitPrice(dummyTradeExecuted)),
                    advise.currentCloseAdviseTradedQuantity(dummyTradeExecuted),
                    advise.getSymbol(),
                    orderTerminatedAtEOD.getOrderSide(),
                    advise.currentCloseAdviseAverageExitPrice(dummyTradeExecuted),
                    getIssueType()), 
                    self());
              }));
        }).
        anyEvent( 
        (event, userAdvise) -> {
          log.warn("Received unhandled event " + event.toString() + " in state " + stateName() + " for token " + userAdvise.getToken() + " with adviseId " + userAdvise.getAdviseId());
          return stay();
    }));
    
    onTermination(
        matchStop(Shutdown(), 
            (userAdviseState, userAdvise) -> {
              log.warn("Shutting down " + self() + " in state " + stateName());
    })
        .stop(Normal(), 
        (reason, userAdvise) -> {
          log.error("Normal shutdown " + userAdvise.getAdviseId() + " for user " + userAdvise.getUserAdviseCompositeKey().getUsername());
        }));
  }


  private String getSymbolFor(String ticker) {
    try {
      Object result = askAndWaitForResult(
          selections.userRootActor(), new GetSymbolForWdId(ticker));
      if (result instanceof Failed) {
        log.error("Error while getting symbol for wdId " + ticker);
        return ticker;
      }
      
      SymbolSentForWdId symbolReceived = (SymbolSentForWdId) result;
      return symbolReceived.getSymbol();
    } catch (Exception e) {
      log.error("UserActor: failed to get symbol from UserRootActor for ticker " + ticker);
      return ticker;
    }
  }


  private boolean isActorStateValidToTerminateBuyOrder() {
    return (stateName().equals(OpenAdviseOrderPlacedWithExchange)) ;
  }
  
  private boolean isActorStateValidToTerminateSellOrder() {
    return (stateName().equals(PartialExitAdviseOrderPlacedWithExchange) || stateName().equals(CompleteExitAdviseOrderPlacedWithExchange)) ;
  }


  private PriceWithPaf getCurrentCloseAdvisePrice(UserEquityAdvise userAdvise) {
    if(userAdvise.getLastestCloseAdvisePrice() != null)
      return userAdvise.getLastestCloseAdvisePrice();
    else
       return noPrice(userAdvise.getToken());
  }

  private IssueType getIssueType() {
    return Equity;
  }

  private void getPriceFromBroadcast(UserEquityAdvise advise) {
    selections.activeInstrumentTracker().tell(new GetPriceFromTracker(advise.getToken()), self());
  }

  private void getPriceForAnExistingUser(UserEquityAdvise advise) {
    PriceWithPaf priceWithPaf = noPrice(advise.getToken());
    try {
      priceWithPaf = (PriceWithPaf) askAndWaitForResult(selections.userRootActor(),
          new GetPriceForSymbolAndClientCode(advise.getSymbol(), advise.getClientCode()));
    } catch (Exception e) {
      log.error("Price subscription failed " + e.getMessage());
      self().tell(new PriceNotReceived("Price subscription failed"), self());
    }
    self().tell(priceWithPaf, self());
  }
  
  private OrderSide checkCorporateEventOrderSide(double adjustPaf){
    if(adjustPaf < 1){
      return SELL;
    }
    return BUY;
  }
  
  private String getExchangeFromWdId(String wdId) {
    return wdId.split("-")[1];
  }

  private BrokerNameSent getBrokerNameOfTheInvestorWith(String userEmail, InvestingMode investingMode) {
    BrokerNameSent brokerNameSent = noBrokerInformationAvailable();
    try {
      Object result = askAndWaitForResult(selections.userRootActor(), new GetBrokerName(userEmail, investingMode)) ;
    
      if (result instanceof Failed) {
          log.error("Error while getting broker name " + result);
          return null;
          }
       brokerNameSent =(BrokerNameSent) result;
     } catch (Exception e) {
          log.error("Error while getting fund ", e);
          return null;
       }
    return brokerNameSent;
  }
  
  private RequestedClientCode getClientCodeFromUserRootActor(String userEmail) {
    RequestedClientCode requestedClientCode = new RequestedClientCode("");
    try {
      Object result =  askAndWaitForResult(selections.userRootActor(),
          new GetClientCode(userEmail));
        
        if (result instanceof Failed) {
              log.error("Error while getting client code." + result);
              return null;
            }
        requestedClientCode = (RequestedClientCode) result;
      } catch (Exception e) {
            log.error("Error while getting instrument by wdId ", e);
            return null;
          }
    return requestedClientCode;
  }
  
  private InstrumentByWdIdSent getInstrumentFromUniverseRootActor(String token) {
    InstrumentByWdIdSent instrumentByWdIdSent = noInstrument();
    try {
      Object result =  askAndWaitForResult(selections.universeRootActor(),
          new GetInstrumentByWdId(token));
        
        if (result instanceof Failed) {
              log.error("Error while getting instrument by wdId " + result);
              return null;
            }
        instrumentByWdIdSent = (InstrumentByWdIdSent) result;
      } catch (Exception e) {
            log.error("Error while getting instrument by wdId ", e);
            return null;
          }
    return instrumentByWdIdSent;
  }
  
  @Override
  public String persistenceId() {
    return persistenceId;
  }

  @Override
  public Class<UserAdviseEvent> domainEventClass() {
    return UserAdviseEvent.class;
  }

  @Override
  public UserEquityAdvise applyEvent(UserAdviseEvent event, UserEquityAdvise userAdvise) {
    userAdvise.update(event);
    return userAdvise;
  }

  @Autowired
  public void setSelections(@Qualifier("WDActors") WDActorSelections selections) {
    this.selections = selections;
  }
  
}