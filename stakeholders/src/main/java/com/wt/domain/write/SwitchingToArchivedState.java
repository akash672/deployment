package com.wt.domain.write;

import com.wt.domain.InvestingMode;
import com.wt.domain.write.events.UserFundEvent;

public class SwitchingToArchivedState extends UserFundEvent {

  private static final long serialVersionUID = 1L;

  public SwitchingToArchivedState(String email, String adviserUsername, String fundName, InvestingMode investingMode) {
    super(email, adviserUsername, fundName, investingMode);
  }

}
