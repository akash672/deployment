package com.wt.domain.write.events;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.UserResponseParameters;

import lombok.Getter;
import lombok.ToString;

@Getter
@JsonIgnoreProperties({"id", "message", "previousApprovalsTradesPending"})
@ToString
public class AllPendingUserFundApprovals extends Done {
  private List<UserResponseParameters> userResponseParameters;
  private boolean previousApprovalsTradesPending;
  private static final long serialVersionUID = 1L;
  @JsonCreator
  public AllPendingUserFundApprovals(@JsonProperty("id") String id, @JsonProperty("userResponseParameters") List<UserResponseParameters> userResponseParameters,
      @JsonProperty("arePreviousApprovalsTradesPending") boolean previousApprovalsTradesPending) {
    super(id, "");
    this.userResponseParameters = userResponseParameters;
    this.previousApprovalsTradesPending = previousApprovalsTradesPending;
  }
}
