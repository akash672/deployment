package com.wt.domain.write.events;

import java.util.Map;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AddToPostMarketCommandQueue extends UserEvent {
  private static final long serialVersionUID = 1L;
  private final String clazz;
  private final Map<String, String> variables;

  public AddToPostMarketCommandQueue(String username, Map<String, String> variables, String clazz) {
    super(username);
    this.clazz = clazz;
    this.variables = variables;
  }

}
