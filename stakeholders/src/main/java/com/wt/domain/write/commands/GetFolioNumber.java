package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.InvestingMode;

import lombok.Getter;

@Getter
public class GetFolioNumber implements Serializable {

  private static final long serialVersionUID = 1L;
  private String username;
  private String amcCode;
  private InvestingMode investingMode;

  public GetFolioNumber(String username, String amcCode, InvestingMode investingMode) {
    this.username = username;
    this.amcCode = amcCode;
    this.investingMode = investingMode;
  }
}
