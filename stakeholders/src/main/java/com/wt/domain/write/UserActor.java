package com.wt.domain.write;

import static akka.http.javadsl.model.StatusCodes.BAD_REQUEST;
import static akka.http.javadsl.model.StatusCodes.EXPECTATION_FAILED;
import static akka.http.javadsl.model.StatusCodes.FAILED_DEPENDENCY;
import static akka.http.javadsl.model.StatusCodes.FORBIDDEN;
import static akka.http.javadsl.model.StatusCodes.NOT_FOUND;
import static akka.http.javadsl.model.StatusCodes.PAYMENT_REQUIRED;
import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;
import static akka.http.javadsl.model.StatusCodes.UNAUTHORIZED;
import static com.wt.domain.AdviseType.Close;
import static com.wt.domain.AdviseType.Issue;
import static com.wt.domain.ApprovalResponse.Approve;
import static com.wt.domain.ApprovalResponse.Decline;
import static com.wt.domain.BrokerName.fromBrokerName;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.NotificationTypes.Account;
import static com.wt.domain.NotificationTypes.Actionable;
import static com.wt.domain.NotificationTypes.ChangeInvestment;
import static com.wt.domain.NotificationTypes.Investment;
import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.OrderSide.SELL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.PARTIALWITHDRAWAL;
import static com.wt.domain.Purpose.UNSUBSCRIPTION;
import static com.wt.domain.UserResponseType.Archive;
import static com.wt.domain.UserResponseType.Awaiting;
import static com.wt.domain.UserResponseType.ExitFund;
import static com.wt.domain.UserType.NTU;
import static com.wt.domain.UserType.NTU_NA;
import static com.wt.domain.UserType.TU;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.CommandPurpose.CalculateAdviseIdToShares;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.CommonConstants.PHONENUMBERPREFIX;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.isExecutionWindowOpen;
import static com.wt.utils.DateUtils.prettyDate;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static java.lang.String.valueOf;
import static java.util.Comparator.comparingLong;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.BadRequestException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminGetUserResult;
import com.amazonaws.services.cognitoidp.model.AdminUpdateUserAttributesRequest;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.MessageActionType;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.wt.config.utils.WDProperties;
import com.wt.domain.AdviseTransactionDetails;
import com.wt.domain.AdviseTransactionType;
import com.wt.domain.AdviseType;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.BrokerName;
import com.wt.domain.FundConstituent;
import com.wt.domain.FundWithAdviserAndInvestingMode;
import com.wt.domain.InvestingMode;
import com.wt.domain.Notification;
import com.wt.domain.NotificationCompositeKey;
import com.wt.domain.NotificationTypes;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.RiskLevel;
import com.wt.domain.User;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserAdviseState;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.UserResponseType;
import com.wt.domain.UserType;
import com.wt.domain.write.commands.AcceptUserAcceptanceMode;
import com.wt.domain.write.commands.AcceptUserApprovalResponse;
import com.wt.domain.write.commands.AddLumpsumToFund;
import com.wt.domain.write.commands.AddLumpsumToFundWithUserEmail;
import com.wt.domain.write.commands.AuthenticatedUserCommand;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CalculateUserFundComposition;
import com.wt.domain.write.commands.CorporateEventUpdateWithFundNames;
import com.wt.domain.write.commands.ExecutePostMarketUserCommands;
import com.wt.domain.write.commands.FetchAllUsersPendingFundRebalancingApprovals;
import com.wt.domain.write.commands.ForceSaveSnapshot;
import com.wt.domain.write.commands.FundActionApproved;
import com.wt.domain.write.commands.FundAdviseIssuedWithClientInformation;
import com.wt.domain.write.commands.GetAdvises;
import com.wt.domain.write.commands.GetBrokerName;
import com.wt.domain.write.commands.GetClientCode;
import com.wt.domain.write.commands.GetCurrentFundComposition;
import com.wt.domain.write.commands.GetFolioNumber;
import com.wt.domain.write.commands.GetFundAdvises;
import com.wt.domain.write.commands.GetFundDetails;
import com.wt.domain.write.commands.GetInvestorName;
import com.wt.domain.write.commands.GetPendingUserFundApprovals;
import com.wt.domain.write.commands.GetPriceFromTracker;
import com.wt.domain.write.commands.GetRejectedAdviseFromUserFund;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.GetUnsubscriptionSnapshotOfFund;
import com.wt.domain.write.commands.GetUserAllInvestmentStatus;
import com.wt.domain.write.commands.GetUserCurrentApprovalStatus;
import com.wt.domain.write.commands.GetUserFundRejectedAdvisesList;
import com.wt.domain.write.commands.GetUserFundStatus;
import com.wt.domain.write.commands.GetUserPendingRebalancingList;
import com.wt.domain.write.commands.GetUserRejectedAdvisesList;
import com.wt.domain.write.commands.GetUserRoleStatus;
import com.wt.domain.write.commands.GetUserSecurityQuestions;
import com.wt.domain.write.commands.GetWalletMoney;
import com.wt.domain.write.commands.HasUserBeenOnBoarded;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.PartialWithdrawalUnderway;
import com.wt.domain.write.commands.PartialWithdrawalUnderway.PartialWithdrawalUnderwayBuilder;
import com.wt.domain.write.commands.PostDeviceToken;
import com.wt.domain.write.commands.RebalanceFundAlertData;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.RetryRejectedUserAdvise;
import com.wt.domain.write.commands.RetryUserAdviseWithClientInformation;
import com.wt.domain.write.commands.SendFollowingCommandToUserFund;
import com.wt.domain.write.commands.SendFundDetailsToUser;
import com.wt.domain.write.commands.SendOTP;
import com.wt.domain.write.commands.SubmitSecurityAnswers;
import com.wt.domain.write.commands.SubmitUserAnswers;
import com.wt.domain.write.commands.SubscribeToFund;
import com.wt.domain.write.commands.SubscribeToFundEventStream;
import com.wt.domain.write.commands.SubscribeToFundWithUserEmail;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.commands.UnSubscribeToFundWithUserEmail;
import com.wt.domain.write.commands.UnsubscribeToFund;
import com.wt.domain.write.commands.UpdateFolioNumber;
import com.wt.domain.write.commands.UpdateKYCDetailsInRootActor;
import com.wt.domain.write.commands.UpdateUserFundCash;
import com.wt.domain.write.commands.UpdateUserFundCompositionList;
import com.wt.domain.write.commands.UpdateWealthCode;
import com.wt.domain.write.commands.UserSubscribedToThisFund;
import com.wt.domain.write.commands.ValidateOTP;
import com.wt.domain.write.commands.WithdrawalFromFund;
import com.wt.domain.write.events.AddToPostMarketCommandQueue;
import com.wt.domain.write.events.AdviseTradesExecuted;
import com.wt.domain.write.events.AllPendingUserFundApprovals;
import com.wt.domain.write.events.AllRejectedUserFundAdvises;
import com.wt.domain.write.events.BrokerNameSent;
import com.wt.domain.write.events.DeviceTokenUpdated;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.ExchangeOrderUpdateEvent;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.Forbidden;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundConstituentListResponse;
import com.wt.domain.write.events.FundDetails;
import com.wt.domain.write.events.FundEvent;
import com.wt.domain.write.events.FundExited;
import com.wt.domain.write.events.FundLumpSumAdded;
import com.wt.domain.write.events.FundSubscribed;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.KycApprovedEvent;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.MarginRequested;
import com.wt.domain.write.events.MergerDemergerFundAdviseClosed;
import com.wt.domain.write.events.MergerDemergerPrimaryBuyIssued;
import com.wt.domain.write.events.MergerDemergerSecondaryBuyIssued;
import com.wt.domain.write.events.PhoneNumberUpdated;
import com.wt.domain.write.events.PostMarketCommandQueueProcessingStarted;
import com.wt.domain.write.events.RequestedClientCode;
import com.wt.domain.write.events.RequestedFolioNumber;
import com.wt.domain.write.events.SecurityAnswersSubmitted;
import com.wt.domain.write.events.SpecialCircumstancesBuy;
import com.wt.domain.write.events.SpecialCircumstancesSell;
import com.wt.domain.write.events.SpecificUserMergerDemergerPrimaryBuy;
import com.wt.domain.write.events.SpecificUserMergerDemergerSecondaryBuy;
import com.wt.domain.write.events.SpecificUserMergerDemergerSellEvent;
import com.wt.domain.write.events.UpdateKYCDetails;
import com.wt.domain.write.events.UserCloseAdviseOrderRejected;
import com.wt.domain.write.events.UserCurrentRoleStatus;
import com.wt.domain.write.events.UserEvent;
import com.wt.domain.write.events.UserFundAdviseIssuedInsufficientCapital;
import com.wt.domain.write.events.UserFundDividendReceived;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundSubscriptionDetails;
import com.wt.domain.write.events.UserFundWithdrawalDoneSuccessfully;
import com.wt.domain.write.events.UserInvestmentStatus;
import com.wt.domain.write.events.UserLumpSumAddedToFund;
import com.wt.domain.write.events.UserOnboardingStatus;
import com.wt.domain.write.events.UserOpenAdviseOrderRejected.UserOpenAdviseOrderRejectedBuilder;
import com.wt.domain.write.events.UserRegisteredFromUserPool;
import com.wt.domain.write.events.UserRiskProfileIdentified;
import com.wt.domain.write.events.UserSecurityQuestionsList;
import com.wt.domain.write.events.UserSubscribedToFund;
import com.wt.domain.write.events.UserSubscribedToFundResponse;
import com.wt.domain.write.events.UserUnsubscribedToFund;
import com.wt.domain.write.events.WealthCodeUpdated;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;
import com.wt.utils.NotificationFacade;
import com.wt.utils.OTPUtils;
import com.wt.utils.WalletFactory;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;
import com.wt.utils.email.templates.wealthdesk.AlertForUserApprovalEmail;
import com.wt.utils.email.templates.wealthdesk.KYCDoneEmail;
import com.wt.utils.email.templates.wealthdesk.OTPEmail;
import com.wt.utils.email.templates.wealthdesk.UserAppliedForKYC;
import com.wt.utils.email.templates.wealthdesk.UserSignedOnMailer;
import com.wt.utils.sms.factory.BaseSMSTemplate;
import com.wt.utils.sms.factory.SMSFactory;
import com.wt.utils.sms.templates.wealthdesk.AlertForUserApprovalSMS;
import com.wt.utils.sms.templates.wealthdesk.KYCDoneSMS;
import com.wt.utils.sms.templates.wealthdesk.UserAppliedForKYCSMS;
import com.wt.utils.sms.templates.wealthdesk.UserSignedOnSMS;

import akka.actor.ActorRef;
import akka.dispatch.OnComplete;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.Recovery;
import akka.persistence.RecoveryCompleted;
import akka.persistence.SaveSnapshotFailure;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotMetadata;
import akka.persistence.SnapshotOffer;
import akka.persistence.SnapshotSelectionCriteria;
import eventstore.ProjectionsClient;
import lombok.extern.log4j.Log4j;
import scala.Option;

@Log4j
@Service("UserActor")
@Scope("prototype")
public class UserActor extends AbstractPersistentActor implements ParentActor<UserFundActor> {
  private String persistenceId;
  private User user = new User();
  private String lastSentOTP;
  private long lastSentOTPTimestamp;
  private String phoneNumberYetToBeVerified;
  private WDActorSelections selections;
  private Map<String, Double> fundNametoAmountForSubscription = new HashMap<String, Double>();
  private Map<String, FundConstituentListResponse> fundNametoFundConstituentsMap = new HashMap<String, FundConstituentListResponse>();
  private Map<String, String> fundNametoAdviserUserName = new HashMap<String, String>();
  private Map<String, InvestingMode> fundNametoInvestingMode = new HashMap<String, InvestingMode>();

  private JournalProvider journalProvider;
  private WalletFactory walletFactory;
  private NotificationFacade notificationFacade;
  private OTPUtils otpUtils;
  private SESEmailUtil emailer;
  @Autowired private SMSFactory smsFactory;
  private WDProperties properties;
  private ProjectionsClient projectionsClient;
  private ActorRef actorRefForReplyingWhenSubscribing;
  private AWSCognitoIdentityProvider userPoolClient;
  private AdminGetUserResult adminGetUser;
  private AWSCredentialsProvider credentialsProvider;

  @Override
  public void preStart() throws Exception {
    super.preStart();
    context().system().eventStream().subscribe(self(), FundEvent.class);
  }

  public UserActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(RegisterInvestorFromUserPool.class, command -> {
      registerInvestorFromUserPool(command);
    }).match(UserSubscribedToThisFund.class, command -> {
      sender().tell(new UserSubscribedToFundResponse(user.getUsername(), user.getUsername(), command.getAdviserName(),
          command.getFundName(), command.getInvestingMode(),
          user.isSubscribed(command.getAdviserName(), command.getFundName(), command.getInvestingMode())), self());
    }).match(AuthenticatedUserCommand.class, command -> {
      handleAuthenticatedUser(command);
    }).match(KycApproveCommand.class, command -> {
      handleKycCommand(command);
    }).match(UpdateKycCommand.class, command -> {
      handleUpdateKycCommand(command);
    }).match(UserFundEvent.class, command -> {
      requestMarginIn(command.getInvestingMode());
      handleUserEvents(command);
    }).match(FundEvent.class, command -> {
      handleFundEvent(command);
    }).match(MarginReceived.class, command -> {
      handleMarginReceivedCommand(command);
    }).match(GetBrokerName.class, command -> {
      handleGetBrokerName(command);
    }).match(GetClientCode.class, command -> {
      handleGetClientCode(command);
    }).match(GetInvestorName.class, command -> {
      sender().tell(new Done(user.getUsername(), user.getFirstName()), self());
    }).match(UserResponseParameters.class, command -> {
      handleUserResponseParameters((UserResponseParameters) command);
    }).match(CorporateEventUpdateWithFundNames.class, command -> {
      checkIfFundsContainWdId(command);
    }).match(SpecificUserMergerDemergerSellEvent.class, command -> {
        self().tell(new MergerDemergerFundAdviseClosed(command.getAdviserUsername(), command.getFundName(), command.getAdviseId(), 
                command.getToken(), command.getPrice()), ActorRef.noSender());
    }).match(SpecificUserMergerDemergerPrimaryBuy.class, command -> {
        self().tell(new MergerDemergerPrimaryBuyIssued(command.getAdviserUsername(), command.getFundName(), command.getAdviseId(), 
                command.getToken(), command.getOldPrice(), command.getNewPrice(), command.getSharesRatio(), command.getSymbol()), ActorRef.noSender());
    }).match(SpecificUserMergerDemergerSecondaryBuy.class, command -> {
        self().tell(new MergerDemergerSecondaryBuyIssued(command.getAdviserUsername(), command.getFundName(), command.getAdviseId(), 
                command.getOldToken(), command.getNewToken(), command.getSharesRatio(), command.getCashAllocationRatio(), command.getSymbol()), ActorRef.noSender());
    }).match(SpecialCircumstancesBuy.class, command -> {
      if (emailIdIsMappedToClientCode(command.getClientCode())) {
        String persistenceId = new UserFundCompositeKey(command.getInvestorUsername(), command.getFund(),
            command.getAdviserUsername(), command.getInvestingMode()).persistenceId();
        forwardToChildren(context(), command, persistenceId);
      } else {
        log.info("The specified client code " + command.getClientCode() + " for the user " + user.getUsername()
            + " is incorrect");
      }
    }).match(SpecialCircumstancesSell.class, command -> {
      if (emailIdIsMappedToClientCode(command.getClientCode())) {
        String persistenceId = new UserFundCompositeKey(command.getInvestorUsername(), command.getFund(),
            command.getAdviserUsername(), command.getInvestingMode()).persistenceId();
        forwardToChildren(context(), command, persistenceId);
      } else {
        log.info("The specified client code " + command.getClientCode() + " for the user " + user.getUsername()
            + " is incorrect");
      }
    }).match(GetUserSecurityQuestions.class, command -> {
      sender().tell(new UserSecurityQuestionsList(user.getUsername(), user.getSecurityAnswers().keySet()), self());
    }).match(ExecutePostMarketUserCommands.class, command -> {
      handlePostMarketCommands();
    }).match(UpdateFolioNumber.class, command -> {
      handleUpdateFolioNumber(command);
    }).match(GetFolioNumber.class, command -> {
      handleGetFolioNumber(command);
    }).match(ExchangeOrderUpdateEvent.class, command -> {
      String userFundPersistenceId = userFundPersistenceId(
          command.getDestinationAddress().getSenderPath().split(CompositeKeySeparator));
      forwardToChildren(context(), command, userFundPersistenceId);
    }).match(FetchAllUsersPendingFundRebalancingApprovals.class, command -> {
      handleResponsesAcrossUsers(command);
    }).match(FundConstituentListResponse.class, command -> {
      sendFundConstituentListToUserForApproval(command);
    }).match(SaveSnapshotSuccess.class, snapshotSuccess -> {
      handleSnapshotSuccess(snapshotSuccess);
    }).match(ForceSaveSnapshot.class, forceSaveSnapshot -> {
      log.debug("Received force saving snapshot for user " + user.getEmail());
      saveSnapshot(user.copy());
      forwardCommandToAllChildren(forceSaveSnapshot);
    }).match(SaveSnapshotFailure.class, snapshotFailure -> {
      handleSnapshotFailure(snapshotFailure);
    }).match(UpdateUserFundCompositionList.class, command -> {
      UserAdviseCompositeKey userAdviseCompositeKey = command.getUserAdviseCompositeKey();
      String userFundPersistenceId = ""
          + new UserFundCompositeKey(userAdviseCompositeKey.getUsername(), userAdviseCompositeKey.getFund(),
              userAdviseCompositeKey.getAdviser(), userAdviseCompositeKey.getInvestingMode()).persistenceId();
      forwardToChildren(context(), command, userFundPersistenceId);
    }).match(CalculateUserFundComposition.class, command -> {
      forwardToChildren(context(), command, command.getUserFundCompositeKey().persistenceId());
    }).match(SendFollowingCommandToUserFund.class, command -> {
      handlemasterCommand(command);
    }).build();
  }

  private void handlemasterCommand(SendFollowingCommandToUserFund command) {
    if (command.getCommandPurpose().equals(CalculateAdviseIdToShares))
      calculateAndPersistUserFundComposition();
  }

  private void calculateAndPersistUserFundComposition() {
    user.getAdvisorToFunds().stream().forEach(fundWithAdviserAndInvestingMode -> {
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(user.getUsername(),
          fundWithAdviserAndInvestingMode.getFund(), fundWithAdviserAndInvestingMode.getAdviserUsername(),
          getInvestingMode(fundWithAdviserAndInvestingMode.getInvestingMode()));
      forwardToChildren(context(), new CalculateUserFundComposition(userFundCompositeKey),
          userFundCompositeKey.persistenceId());
    });
  }

  private void checkIfFundsContainWdId(CorporateEventUpdateWithFundNames command) {
    for (FundWithAdviserAndInvestingMode fund : user.getAdvisorToFunds()) {
      String fundNameWithAdviserUserName = fund.getFund() + CompositeKeySeparator + fund.getAdviserUsername();
      if (command.getFundNames().contains(fundNameWithAdviserUserName)) {
        String userFundPersistenceId = new UserFundCompositeKey(user.getUsername(), fund.getFund(),
            fund.getAdviserUsername(), InvestingMode.valueOf(fund.getInvestingMode())).persistenceId();
        forwardToChildren(context(), command, userFundPersistenceId);
      }
    }
  }

  private void sendFundConstituentListToUserForApproval(FundConstituentListResponse command) {
    FundConstituentListResponse fundConstituentListResponse = (FundConstituentListResponse) command;
    log.info("Receieved fund composition from relayer for Fund: " + fundConstituentListResponse.getFundName()
        + " requested by user: " + user.getUsername());
    if (fundNametoAdviserUserName.containsKey(fundConstituentListResponse.getFundName())) {
      String basketOrderId = valueOf(
          "POSTMARKETAPPROVAL_" + command.getId() + "_" + command.getFundName() + "_" + valueOf(currentTimeInMillis()));
      self().tell(new FundActionApproved(user.getUsername(), fundConstituentListResponse.getFundName(),
          fundNametoAdviserUserName.get(fundConstituentListResponse.getFundName()), user.getWealthCode(),
          fundNametoInvestingMode.get(fundConstituentListResponse.getFundName()), basketOrderId,
          Purpose.EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()), ActorRef.noSender());
      fundNametoFundConstituentsMap.put(fundConstituentListResponse.getFundName(), fundConstituentListResponse);
      return;
    }
    HashMap<String, AdviseTransactionDetails> symbolToPossibleSharesOnSubscription = new HashMap<String, AdviseTransactionDetails>();
    double cashforFundSubscription = fundNametoAmountForSubscription.get(fundConstituentListResponse.getFundName());
    calculateTheSharesForAdvisesAndAddToMap(cashforFundSubscription, fundConstituentListResponse,
        symbolToPossibleSharesOnSubscription);

    fundNametoFundConstituentsMap.put(fundConstituentListResponse.getFundName(), fundConstituentListResponse);
    actorRefForReplyingWhenSubscribing
        .tell(new SendFundDetailsToUser(user.getUsername(), symbolToPossibleSharesOnSubscription), self());
  }

  private void handleSnapshotSuccess(SaveSnapshotSuccess snapshotSuccess) {
    SnapshotMetadata metadata = snapshotSuccess.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId()
        + " has sucessfully been persisted with sequence number " + metadata.sequenceNr() + " at "
        + prettyDate(metadata.timestamp()));
  }

  private void handleSnapshotReceive(SnapshotOffer snapshotOffer) {
    SnapshotMetadata metadata = snapshotOffer.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId()
        + " has sucessfully been restored with sequence number " + metadata.sequenceNr() + " at "
        + prettyDate(metadata.timestamp()));
  }

  private void handleSnapshotFailure(SaveSnapshotFailure snapshotFailure) {
    SnapshotMetadata metadata = snapshotFailure.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId()
        + " has failed to persist with sequence number " + metadata.sequenceNr() + " at "
        + prettyDate(metadata.timestamp()) + " due to reason " + snapshotFailure.cause());
  }

  private void calculateTheSharesForAdvisesAndAddToMap(double cashforFundSubscription,
      FundConstituentListResponse fundConstituentListResponse,
      HashMap<String, AdviseTransactionDetails> symbolToPossibleSharesOnSubscription) {

    for (FundConstituent fundConstituent : fundConstituentListResponse.getFundConstituentList()) {
      String symbol = getSymbolFor(fundConstituent.getTicker());
      String exchange = getExchangeFromWdId(fundConstituent.getTicker());
      double currentMarketPrice = fundConstituent.getEntryPrice().getPrice().getPrice();
      double investmentValue = round(cashforFundSubscription * fundConstituent.getAllocation() / 100.);
      int numberofShares = (int) (investmentValue / currentMarketPrice);
      double adviseCurrentMarketValue = round(currentMarketPrice * numberofShares);
      if (symbolToPossibleSharesOnSubscription.containsKey(symbol)) {
        AdviseTransactionDetails adviseDetails = symbolToPossibleSharesOnSubscription.get(symbol);
        adviseDetails.setSharesQuanity(adviseDetails.getSharesQuanity() + numberofShares);
        adviseDetails.setCurrentMarketValue(adviseDetails.getCurrentMarketValue() + investmentValue);
      } else {
        symbolToPossibleSharesOnSubscription.put(symbol,
            new AdviseTransactionDetails(numberofShares, currentMarketPrice, adviseCurrentMarketValue,
                fundConstituent.getAllocation(), exchange, AdviseTransactionType.Buy));
      }
    }
  }

  private String getSymbolFor(String ticker) {
    try {
      Object result = askAndWaitForResult(selections.userRootActor(), new GetSymbolForWdId(ticker));
      if (result instanceof Failed) {
        log.error("Error while getting symbol for wdId " + ticker);
        return ticker;
      }

      SymbolSentForWdId symbolReceived = (SymbolSentForWdId) result;
      return symbolReceived.getSymbol();
    } catch (Exception e) {
      log.error("UserActor: failed to get symbol from UserRootActor for ticker " + ticker);
      return ticker;
    }
  }

  private void handleUpdateKycCommand(UpdateKycCommand command) {
    if (user.getRole() == UserType.TU)
      updateKycApplication(command);
    else
      sender().tell(failed(user.getUsername(), "User KYC is not done yet", PRECONDITION_FAILED), self());
  }

  private void updateKycApplication(UpdateKycCommand command) {
    command.validate();
    UpdateKYCDetails evt = new UpdateKYCDetails(command.getUsername(), command.getPanCard(), command.getBrokerCompany(),
        command.getPhoneNumber(), command.getClientCode(), command.getEmail());
    persist(evt, (UpdateKYCDetails event) -> {
      user.update(event);
      requestMarginIn(REAL);
      sender().tell(new Done(event.getUsername(), "KYC details updated successfully"), self());
      forwardToParentActor(event);
    });
    saveSnapshot();
    if (properties.isSendingUserEmailUpdatesAllowed())
      sendSMSOrEmailToUser(evt);
    sendSNSNotification("Wealth Desk", "NA", "KYC UPDATED", "Your KYC is updated.", Account, REAL);
  }

  private void sendSMSOrEmailToUser(Object evt) {
    boolean isEmailPresent = isEmailPresent();
    boolean isPhoneNumberPresent = isPhoneNumberPresent();

    try {
      if (evt instanceof UpdateKYCDetails) {
        UpdateKYCDetails event = (UpdateKYCDetails) evt;
        if (isEmailPresent) {
          emailer.prepareAndSendEmail(KYCDoneEmail.class, user.getEmail(), event.getBrokerCompany(),
              event.getClientCode(), user.getFirstName(), properties);
        }
        if (isPhoneNumberPresent) {
          BaseSMSTemplate message = smsFactory.getSMS(KYCDoneSMS.class.getSimpleName(), event.getBrokerCompany(),
              event.getClientCode(), user.getFirstName());
          notificationFacade.sendSMSMessage(message.getPlainTextFormat(),
              getValidatedUserPhoneNumber(user.getPhoneNumber()));
        }
      } else if (evt instanceof KycApprovedEvent) {
        KycApprovedEvent event = (KycApprovedEvent) evt;
        if (isEmailPresent)
          emailer.prepareAndSendEmail(KYCDoneEmail.class, user.getEmail(), event.getBrokerCompany(),
              event.getClientCode(), user.getFirstName(), properties);
        if (isPhoneNumberPresent) {
          BaseSMSTemplate message = smsFactory.getSMS(KYCDoneSMS.class.getSimpleName(), event.getBrokerCompany(),
              event.getClientCode(), user.getFirstName());
          notificationFacade.sendSMSMessage(message.getPlainTextFormat(),
              getValidatedUserPhoneNumber(user.getPhoneNumber()));
        }
      } else if (evt instanceof UserRegisteredFromUserPool) {
        if (isEmailPresent)
          emailer.prepareAndSendEmail(UserSignedOnMailer.class, user.getEmail(),
              user.getFirstName() + " " + user.getLastName(), properties);
        if (isPhoneNumberPresent) {
          BaseSMSTemplate message = smsFactory.getSMS(UserSignedOnSMS.class.getSimpleName(), "");
          notificationFacade.sendSMSMessage(message.getPlainTextFormat(),
              getValidatedUserPhoneNumber(user.getPhoneNumber()));
        }
      } else if (evt instanceof UserResponseParameters) {
        UserResponseParameters userResponseParameters = (UserResponseParameters) evt;
        if (properties.isSendingUserEmailUpdatesAllowed()) {
          if (isEmailPresent) {
            double currentPrice = 0d;
            try {
              PriceWithPaf priceWithPaf = (PriceWithPaf) askAndWaitForResult(selections.activeInstrumentTracker(),
                  new GetPriceFromTracker(userResponseParameters.getToken()));
              currentPrice = round(priceWithPaf.getPrice().getPrice(), 2);
            } catch (Exception e) {
              log.error("Could not get price of " + userResponseParameters.getToken() + " due to " + e.getMessage());
            }
            RebalanceFundAlertData rebalanceFundAlertData = new RebalanceFundAlertData(
                userResponseParameters.getSymbol(), round(userResponseParameters.getAllocationValue(), 2),
                userResponseParameters.getExchange(), userResponseParameters.getClientCode(), user.getEmail(),
                user.getPhoneNumber(), userResponseParameters.getNumberOfShares(), userResponseParameters.getFundName(),
                userResponseParameters.getAdviseType().getAdviseType(), userResponseParameters.getAdviserName(),
                userResponseParameters.getBrokerName(), properties, userResponseParameters.getInvestmentDate(),
                user.getFirstName() + " " + user.getLastName(), currentPrice,
                userResponseParameters.getAbsoluteAllocation());
            emailer.prepareAndSendEmail(AlertForUserApprovalEmail.class, rebalanceFundAlertData);
          }
          if (isPhoneNumberPresent) {
            BaseSMSTemplate message = smsFactory.getSMS(AlertForUserApprovalSMS.class.getSimpleName(),
                userResponseParameters.getFundName());
            notificationFacade.sendSMSMessage(message.getPlainTextFormat(),
                getValidatedUserPhoneNumber(user.getPhoneNumber()));
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private boolean isEmailPresent() {
    return !user.getEmail().isEmpty() && user.getEmail() != null;
  }

  private boolean isPhoneNumberPresent() {
    return getValidatedUserPhoneNumber(user.getPhoneNumber()) != null && !getValidatedUserPhoneNumber(user.getPhoneNumber()).isEmpty();
  }

  private String getValidatedUserPhoneNumber(String phoneNumber) {
    if (phoneNumber.startsWith(PHONENUMBERPREFIX) && phoneNumber.substring(1).length() == 12
        && isNumeric(phoneNumber.substring(1)))
      return phoneNumber.substring(1);
    else if (phoneNumber.startsWith(PHONENUMBERPREFIX) && phoneNumber.substring(1).length() == 10
        && isNumeric(phoneNumber.substring(1)))
      return "91" + phoneNumber.substring(1);
    else if (phoneNumber.length() == 10 && isNumeric(phoneNumber))
      return "91" + phoneNumber;
    else if (phoneNumber.length() == 12 && isNumeric(phoneNumber))
      return phoneNumber;
    else
      return null;
  }

  private boolean isNumeric(String phoneNumber) {
    try {
      Long.parseLong(phoneNumber);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  private void handleResponsesAcrossUsers(FetchAllUsersPendingFundRebalancingApprovals command) {
    List<UserResponseParametersFromFund> userResponseParametersFromFunds;
    userResponseParametersFromFunds = getUserResponseParamtersFromFunds(command.getInvestingMode());
    UserPendingFundRebalancingApprovals userPendingApprovalWithPhoneNumber = new UserPendingFundRebalancingApprovals(
        user.getUsername(), getValidatedUserPhoneNumber(user.getPhoneNumber()), userResponseParametersFromFunds);
    if ((userResponseParametersFromFunds != null) && (!userResponseParametersFromFunds.isEmpty()))
      sender().tell(userPendingApprovalWithPhoneNumber, self());
  }

  private String userFundPersistenceId(String[] persistenceIdSplit) {
    return persistenceIdSplit[1] + CompositeKeySeparator + persistenceIdSplit[2] + CompositeKeySeparator
        + persistenceIdSplit[3] + CompositeKeySeparator + persistenceIdSplit[4];
  }

  private void handleGetFolioNumber(GetFolioNumber command) {
    GetFolioNumber getFolioNumber = (GetFolioNumber) command;
    AmcCodeWithInvestingMode amcCodeWithInvestingMode = new AmcCodeWithInvestingMode(getFolioNumber.getAmcCode(),
        getFolioNumber.getInvestingMode());
    String folioNumber = user.getAmcCodeToFolioNumber().get(amcCodeWithInvestingMode) == null ? "0"
        : user.getAmcCodeToFolioNumber().get(amcCodeWithInvestingMode);
    sender().tell(new RequestedFolioNumber(getFolioNumber.getAmcCode(), folioNumber), self());
  }

  private void handleUpdateFolioNumber(UpdateFolioNumber event) {
    persist(event, evt -> {
      user.update(evt);
    });
    saveSnapshot();
  }

  private void handleUserEvents(UserFundEvent command) {
    if (command instanceof FundSubscribed)
      handleUserFundSubscribed(command);
    else if (command instanceof FundLumpSumAdded)
      handleUserFundLumpSumAdded(command);
    else if (command instanceof UserFundAdviseIssuedInsufficientCapital)
      handleInsufficientCapital(command);
    else if (command instanceof UserOpenAdviseOrderRejectedBuilder)
      handleUserOpenAdviseRejection(command);
    else if (command instanceof UserCloseAdviseOrderRejected)
      handleUserCloseAdviseRejection(command);
    else if (command instanceof AdviseTradesExecuted)
      handleAdviseOrderExecutionAndForwardToChild(command);
    else if (command instanceof UserFundWithdrawalDoneSuccessfully)
      handleUserFundWithdrawalDoneSuccessfully(command);
    else if (command instanceof FundExited)
      handleExitFundEvent(command);
    else if (command instanceof UserFundDividendReceived)
      handleUserFundDividendReceived(command);
  }

  private void handleUserFundDividendReceived(UserFundEvent event) {
    UserFundDividendReceived userFundDividendReceived = (UserFundDividendReceived) event;
    forwardToChildren(context(), userFundDividendReceived,
        new UserFundCompositeKey(user.getUsername(), userFundDividendReceived.getFundName(),
            userFundDividendReceived.getAdviserUsername(), userFundDividendReceived.getInvestingMode())
                .persistenceId());
  }

  private void handleUserFundWithdrawalDoneSuccessfully(UserFundEvent command) {
    UserFundWithdrawalDoneSuccessfully userFundWithdrawalDoneSuccessfully = (UserFundWithdrawalDoneSuccessfully) command;
    sendSNSNotification(userFundWithdrawalDoneSuccessfully.getAdviserUsername(),
        userFundWithdrawalDoneSuccessfully.getFundName(), "Partial withdrawal ",
        "Successfully with Rs." + round(userFundWithdrawalDoneSuccessfully.getTotalWithdrawalAmount()), Investment,
        userFundWithdrawalDoneSuccessfully.getInvestingMode());
    WithdrawalMadefromFund WithdrawalMadefromFund = new WithdrawalMadefromFund(user.getUsername(),
        userFundWithdrawalDoneSuccessfully.getAdviserUsername(), userFundWithdrawalDoneSuccessfully.getFundName(),
        userFundWithdrawalDoneSuccessfully.getInvestingMode(),
        userFundWithdrawalDoneSuccessfully.getTotalWithdrawalAmount());
    persist(WithdrawalMadefromFund, (WithdrawalMadefromFund evt) -> {
      user.update(evt);
      requestMarginIn(WithdrawalMadefromFund.getInvestingMode());
      context().parent().tell(WithdrawalMadefromFund, self());
    });
    saveSnapshot();
  }

  private void handleKycCommand(KycApproveCommand command) {
    if (user.getRole() != UserType.TU)
      approveKycApplication(command);
    else
      sender().tell(failed(user.getUsername(), "User KYC is already done", PRECONDITION_FAILED), self());
  }

  private void handleAuthenticatedUser(AuthenticatedUserCommand command) throws ParseException {
    handleAuthenticatedCommand(command);
  }

  protected void handleAuthenticatedCommand(AuthenticatedUserCommand command) throws ParseException {
    if (command instanceof SendOTP) {
      handleGetOTP((SendOTP) command);
      return;
    } else if (command instanceof ValidateOTP) {
      handleValidateOTP((ValidateOTP) command);
      return;
    } else if (command instanceof UpdateWealthCode) {
      handleUpdateWealthCode((UpdateWealthCode) command);
    } else if (command instanceof SubscribeToFundEventStream) {
      handleSubscribeToFundEventStreamCommand(command);
    } else if (command instanceof GetUserRoleStatus) {
      handleUserRoleStatus(command);
    } else if (command instanceof GetUserFundStatus) {
      handleGetUserFundStatus((GetUserFundStatus) command);
    } else if (command instanceof GetPendingUserFundApprovals) {
      handleGetPendingUserFundApprovals((GetPendingUserFundApprovals) command);
    } else if (command instanceof AcceptUserAcceptanceMode) {
      handleAcceptUserAcceptanceMode((AcceptUserAcceptanceMode) command);
    } else if (command instanceof GetUserCurrentApprovalStatus) {
      handleGetUserCurrentApprovalStatus((GetUserCurrentApprovalStatus) command);
    } else if (command instanceof GetUserAllInvestmentStatus) {
      handleUserInvestmentStatus((GetUserAllInvestmentStatus) command);
    } else if (command instanceof GetWalletMoney) {
      handleGetWalletMoney(command);
    } else if (command instanceof PostDeviceToken) {
      handleDeviceToken(command);
    } else if (command instanceof GetUserPendingRebalancingList) {
      GetUserPendingRebalancingList getUserApprovalPendingList = (GetUserPendingRebalancingList) command;
      requestMarginIn(VIRTUAL);
      requestMarginIn(REAL);
      handleGetApprovalPendingList(getUserApprovalPendingList);
    } else if (command instanceof GetUserRejectedAdvisesList) {
      handleGetRejectedAdviseList((GetUserRejectedAdvisesList) command);
    } else if (command instanceof GetUserFundRejectedAdvisesList) {
      handleGetRejectedAdviseListFromFund((GetUserFundRejectedAdvisesList) command);
    } else if (command instanceof UpdateUserFundCash) {
      handleUpdateUserFundCash((UpdateUserFundCash) command);
    }
    if (!user.isNotAutorized()) {
      checkAndUpdateRole();
      if (command instanceof SubscribeToFund) {
        handleSubscribeToFundUser(command);
      } else if (command instanceof FundActionApproved) {
        handleFundActionApproval(command);
      } else if (command instanceof UnsubscribeToFund) {
        handleUnsubscribeToFundUser(command);
      } else if (command instanceof AddLumpsumToFund) {
        handleAddLumpsumToFundUser(command);
      } else if (command instanceof WithdrawalFromFund) {
        handleWithdrawalFromFundUser(command);
      } else if (command instanceof GetAdvises) {
        handleGetAdvises(command);
      } else if (command instanceof SubmitUserAnswers) {
        handleUserAnswers((SubmitUserAnswers) command);
      } else if (command instanceof SubmitSecurityAnswers) {
        handleSecurityAnswers((SubmitSecurityAnswers) command);
      } else if (command instanceof AcceptUserApprovalResponse) {
        handleUserResponseOnAdvise((AcceptUserApprovalResponse) command);
      } else if (command instanceof RetryRejectedUserAdvise) {
        handleRetryingOfRejectedUserAdvise((RetryRejectedUserAdvise) command);
      } else {
        selections.advisersRootActor().forward(command, context());
      }
    } else if (isAuthorisedCommand(command)) {
      sender().tell(Forbidden.forbidden(user.getUsername(), "Your trial period has expired."
          + " Please complete your KYC for uninterrupted access to your account."), self());
    }
  }

  private void handleFundActionApproval(AuthenticatedUserCommand command) throws ParseException {
    
    FundActionApproved userFundCommand = (FundActionApproved) command;
    
    if (userFundCommand.isBaskerOrderIdBlank()) {
      log.warn("Blank BasketOrderId in " + userFundCommand.getInvestingPurpose() + " Approval command for user : "
          + user.getUsername() + " and fund " + userFundCommand.getFundName());
      sender().tell(new Failed(user.getUsername(),
          "BasketOrderId is blank. Your request to Subscribe/AddLumpSum cannot be processed."), self());
      return;
    }

    if (!fundNametoFundConstituentsMap.containsKey(userFundCommand.getFundName())
        && !userFundCommand.getInvestingPurpose().equals(PARTIALWITHDRAWAL)
        && !userFundCommand.getInvestingPurpose().equals(UNSUBSCRIPTION)) {
      log.warn("Incorrect call to AddLumpSum/SubscribeToFund for user : " + user.getUsername() + " and fund "
          + userFundCommand.getFundName());
      sender().tell(new Failed(user.getUsername(), "Your request to Subscribe/AddLumpSum cannot be processed."),
          self());
      return;
    }
    
    if (userFundCommand.isAuthticationInfoBlank()) {
      userFundCommand.setBrokerAuthInformation(BrokerAuthInformation.withNoBrokerAuth());
    }

    if (userFundCommand.getApprovalResponse().equals(Decline)) {

      if (userFundCommand.getInvestingPurpose().equals(EQUITYSUBSCRIPTION))
        clearAllSubscriptionFunds(userFundCommand.getFundName());

      sender().tell(new Done(user.getUsername(), "Your fund action has been cancelled."), self());
      return;
    }
    if (!user.isSubscribed(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode()) && userFundCommand.getInvestingPurpose().equals(EQUITYSUBSCRIPTION)) {
      continueToSubscribeToFund(userFundCommand);
    } else if (userFundCommand.getInvestingPurpose().equals(PARTIALWITHDRAWAL)) {
      continueToWithdrawPartially(userFundCommand);
    } else if (userFundCommand.getInvestingPurpose().equals(UNSUBSCRIPTION)) {
      continueToUnsubscribeFromFund(userFundCommand);
    } else {
      continueToAddLumpsumToFund(userFundCommand);
    }
  }

  private void continueToWithdrawPartially(FundActionApproved userFundCommand) {
    PartialWithdrawalUnderwayBuilder partialWithdrawalUnderwayBuilder = new PartialWithdrawalUnderwayBuilder(
        userFundCommand.getUsername(), userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode());
    if (!fundNametoAmountForSubscription.containsKey(userFundCommand.getFundName())) {
      sender().tell(
          new Failed(user.getUsername(),
              "You cannot send this request before getting the list of advises to be exited.", PRECONDITION_FAILED),
          self());
      return;
    }
    PartialWithdrawalUnderway partialWithdrawalUnderway = partialWithdrawalUnderwayBuilder
      .withPurpose(PARTIALWITHDRAWAL)
      .withWealthCode(userFundCommand.getWealthCode())
      .withWithdrawalAmount(fundNametoAmountForSubscription.get(userFundCommand.getFundName()))
      .withBasketOrderId(userFundCommand.getBasketOrderId())
      .withBrokerAuthInformation(userFundCommand.getBrokerAuthInformation()).build();
    UserOnboardingStatus onboardingStatus = getOnboardingStatus(userFundCommand.getUsername());
    if (!isExecutionWindowOpen() && !onboardingStatus.isOnboarded() && properties.MARKET_OFFLINE_ENABLED()) {
      withdrawalFromFundPostMarket(partialWithdrawalUnderway);
      return;
    } else if (!isExecutionWindowOpen() && !onboardingStatus.isOnboarded() && !properties.MARKET_OFFLINE_ENABLED()) {
      sender().tell(
          new Failed(user.getUsername(), "You cannot send this request during non-market hours.", PRECONDITION_FAILED),
          self());
      return;
    }

    forwardToChildren(context(), partialWithdrawalUnderway,
        "" + new UserFundCompositeKey(user.getUsername(), userFundCommand.getFundName(),
            userFundCommand.getAdviserUsername(), userFundCommand.getInvestingMode()).persistenceId());

    clearAllSubscriptionFunds(userFundCommand.getFundName());
  }

  private void continueToAddLumpsumToFund(FundActionApproved userFundCommand) throws ParseException {
    
    AddLumpsumToFund addLumpsumToFund = new AddLumpsumToFund(userFundCommand.getAdviserUsername(),
        userFundCommand.getFundName(), user.getUsername(),
        fundNametoAmountForSubscription.get(userFundCommand.getFundName()), userFundCommand.getInvestingMode());

    if (!isExecutionWindowOpen() && properties.MARKET_OFFLINE_ENABLED()) {
      addLumpSumPostMarket(addLumpsumToFund);
      return;
    } else if (!isExecutionWindowOpen() && !properties.MARKET_OFFLINE_ENABLED()) {
      sender().tell(
          new Failed(user.getUsername(), "You cannot send this request during non-market hours.", PRECONDITION_FAILED),
          self());
      return;
    }

    FundDetails fundDetails = getFundDetails(userFundCommand.getAdviserUsername(), userFundCommand.getFundName());
    if (fundDetails == null) {
      sender().tell(failed(user.getUsername(),
          "AddLumpSum Approval to fund " + userFundCommand.getFundName() + " failed, fund not found", NOT_FOUND), self());
      return;
    }
    String basketOrderId = userFundCommand.getBasketOrderId();
    AddLumpsumToFundWithUserEmail addLumpsumToFundWithUserEmail = new AddLumpsumToFundWithUserEmail(
        userFundCommand.getAdviserUsername(), userFundCommand.getFundName(), userFundCommand.getUsername(),
        user.getEmail(), fundNametoAmountForSubscription.get(userFundCommand.getFundName()), fundDetails.getRiskLevel(),
        DateUtils.todayBOD() + "", userFundCommand.getInvestingMode(),
        fundNametoFundConstituentsMap.get(userFundCommand.getFundName()), basketOrderId,
        userFundCommand.getBrokerAuthInformation());

    forwardToChildren(context(), addLumpsumToFundWithUserEmail,
        "" + new UserFundCompositeKey(user.getUsername(), userFundCommand.getFundName(),
            userFundCommand.getAdviserUsername(), userFundCommand.getInvestingMode()).persistenceId());

    clearAllSubscriptionFunds(userFundCommand.getFundName());

  }

  private void continueToSubscribeToFund(FundActionApproved userFundCommand) throws ParseException {
    
    SubscribeToFund subscribeToFund = new SubscribeToFund(userFundCommand.getAdviserUsername(),
        userFundCommand.getFundName(), user.getUsername(),
        fundNametoAmountForSubscription.get(userFundCommand.getFundName()), 0.0, DateUtils.date() + "",
        userFundCommand.getInvestingMode(), userFundCommand.getInvestingPurpose());

    if (!isExecutionWindowOpen() && properties.MARKET_OFFLINE_ENABLED()) {
      addSubscribeCommandToPostMarket(subscribeToFund);
      return;
    } else if (!isExecutionWindowOpen() && !properties.MARKET_OFFLINE_ENABLED()) {
      sender().tell(
          new Failed(user.getUsername(), "You cannot send this request during non-market hours.", PRECONDITION_FAILED),
          self());
      return;
    }

    FundDetails fundDetails = getFundDetails(userFundCommand.getAdviserUsername(), userFundCommand.getFundName());
    if (fundDetails == null) {
      sender().tell(failed(user.getUsername(),
          "Subscription Approval to fund " + userFundCommand.getFundName() + " failed, fund not found", NOT_FOUND), self());
      return;
    }
    SubscribeToFundWithUserEmail subscribeToFundWithUserEmailCommand = new SubscribeToFundWithUserEmail(
        userFundCommand.getAdviserUsername(), userFundCommand.getFundName(), userFundCommand.getUsername(),
        user.getEmail(), fundNametoAmountForSubscription.get(userFundCommand.getFundName()), 0.0, DateUtils.date(),
        fundDetails.getRiskLevel(), DateUtils.todayBOD() + "", userFundCommand.getInvestingMode(),
        userFundCommand.getInvestingPurpose(), fundNametoFundConstituentsMap.get(userFundCommand.getFundName()),
        userFundCommand.getBasketOrderId(), userFundCommand.getBrokerAuthInformation());

    forwardToChildren(context(), subscribeToFundWithUserEmailCommand,
        "" + new UserFundCompositeKey(user.getUsername(), userFundCommand.getFundName(),
            userFundCommand.getAdviserUsername(), userFundCommand.getInvestingMode()).persistenceId());

    sender().tell(
        new Done(user.getUsername(), "Subscription in process. You will receive a notification once its done."),
        self());

    clearAllSubscriptionFunds(userFundCommand.getFundName());

  }

  private void addSubscribeCommandToPostMarket(SubscribeToFund subscribeToFund) {
    try {
      AddToPostMarketCommandQueue event = new AddToPostMarketCommandQueue(user.getUsername(),
          getUserFundCommandVariablesInMap(subscribeToFund), subscribeToFund.getClass().getSimpleName());
      user.validate(event);
      persist(event, evt -> {
        user.update(evt);
        sender().tell(new Done(user.getUsername(),
            "Your subscription request has been received.You will be subscribed to fund when"
                + " the markets open on the next trading day."),
            self());
      });
      saveSnapshot();
    } catch (JsonProcessingException e) {
      sender().tell(new Failed(user.getUsername(), "Could not add your request in queue.", FAILED_DEPENDENCY), self());
    } catch (Exception e) {
      sender().tell(
          new Failed(user.getUsername(),
              "You cannot send more than one request in the same fund during non-market hours.", PRECONDITION_FAILED),
          self());
    }
  }

  private boolean isAuthorisedCommand(AuthenticatedUserCommand command) {
    if (command instanceof SubscribeToFund) {
      return true;
    } else if (command instanceof UnsubscribeToFund) {
      return true;
    } else if (command instanceof GetAdvises) {
      return true;
    } else if (command instanceof SubmitUserAnswers) {
      return true;
    } else if (command instanceof SubmitSecurityAnswers) {
      return true;
    } else if (command instanceof AcceptUserApprovalResponse) {
      return true;
    } else
      return false;
  }

  private void handleUserInvestmentStatus(GetUserAllInvestmentStatus cmd) {
    ActorRef destinationActor = sender();
    projectionsClient
        .fetchProjectionState("SummaryOfUserFundList",
            Option.apply(
                "projection_postTradeAnalyticsFor_UserFund-" + user.getUsername() + "_" + cmd.getInvestingMode()))
        .onComplete(new OnComplete<Option<String>>() {
          @Override
          public void onComplete(Throwable failure, Option<String> result) {
            if (failure == null) {
              destinationActor.tell(new UserInvestmentStatus(user.getUsername(), "UserAllInvestmentReport",
                  result.get(), cmd.getInvestingMode()), self());
            } else {
              log.error(failure);
              destinationActor.tell(
                  failed(user.getUsername(), "Sorry, could not fetch the required data for you", NOT_FOUND), self());
            }
          }
        }, context().dispatcher());
  }

  private void approveKycApplication(KycApproveCommand command) {
    command.validate();
    KycApprovedEvent evt = new KycApprovedEvent(command.getUsername(), command.getPanCard(), command.getBrokerCompany(),
        command.getPhoneNumber(), command.getClientCode(), command.getEmail());
    persist(evt, (KycApprovedEvent event) -> {
      user.update(event);
      requestMarginIn(REAL);
      sender().tell(new Done(user.getUsername(), "KYC Process Completed"), self());
      forwardToParentActor(event);
    });
    saveSnapshot();
    if (properties.isSendingUserEmailUpdatesAllowed())
      sendSMSOrEmailToUser(evt);
    sendSNSNotification("Wealth Desk", "NA", "KYC Approved", "Your KYC Process is now complete", Account, REAL);
  }

  private void handleUserFundSubscribed(UserFundEvent command) {
    FundSubscribed fundSubscribed = (FundSubscribed) command;
    sendSNSNotification(fundSubscribed.getAdviserUsername(), fundSubscribed.getFundName(), "Fund Subscription",
        "Successfully subscribed to " + fundSubscribed.getFundName(), Investment, fundSubscribed.getInvestingMode());
    UserSubscribedToFund userSubscribedToFund = new UserSubscribedToFund(user.getUsername(),
        fundSubscribed.getAdviserUsername(), fundSubscribed.getFundName(), fundSubscribed.getInvestingMode(),
        fundSubscribed.getTotalInvestment());
    persist(userSubscribedToFund, (UserSubscribedToFund evt) -> {
      user.update(userSubscribedToFund);
      requestMarginIn(userSubscribedToFund.getInvestingMode());
      context().parent().tell(userSubscribedToFund, self());
    });
    saveSnapshot();
  }

  private void handleUserFundLumpSumAdded(UserFundEvent command) {
    FundLumpSumAdded fundLumpSumAdded = (FundLumpSumAdded) command;
    sendSNSNotification(fundLumpSumAdded.getAdviserUsername(), fundLumpSumAdded.getFundName(), "Fund LumpSum added",
        "Successfully added lumpsum to " + fundLumpSumAdded.getFundName(), Investment,
        fundLumpSumAdded.getInvestingMode());
    UserLumpSumAddedToFund userLumpSumAddedToFund = new UserLumpSumAddedToFund(user.getUsername(),
        fundLumpSumAdded.getAdviserUsername(), fundLumpSumAdded.getFundName(), fundLumpSumAdded.getInvestingMode(),
        fundLumpSumAdded.getLumpSumAdded());
    persist(userLumpSumAddedToFund, (UserLumpSumAddedToFund evt) -> {
      user.update(userLumpSumAddedToFund);
      requestMarginIn(userLumpSumAddedToFund.getInvestingMode());
      context().parent().tell(userLumpSumAddedToFund, self());
    });
    saveSnapshot();
  }

  private void registerInvestorFromUserPool(RegisterInvestorFromUserPool command) {
    try {
      command.validate();
    } catch (BadRequestException e) {
      log.error("Exception " + e);
      sender().tell(failed(command.getUsername(), e.getMessage(), BAD_REQUEST), self());
    }
    try {
      UserRegisteredFromUserPool userRegistered = new UserRegisteredFromUserPool(command.getUsername(),
          command.getName(), command.getPhoneNumber(), command.getEmail(), properties.INITIAL_VIRTUAL_MONEY_AMOUNT());

      persist(userRegistered, (UserRegisteredFromUserPool evt) -> {
        user.update(evt);
        sender().tell(new Done(evt.getUsername(), "Registration is successful"), self());
        if (properties.isSendingUserEmailUpdatesAllowed())
          sendSMSOrEmailToUser(evt);
      });
      saveSnapshot();
    } catch (Exception e) {
      log.error("Exception " + e);
      sender().tell(failed(command.getUsername(), "failed to register investor", EXPECTATION_FAILED), self());
    }
  }

  protected void registerUserInCognitoInvestorPool(RegisterInvestorFromUserPool command) {

    String investorUserPoolId = properties.INVESTOR_USER_POOL_ID();
    try {
      adminGetUser = userPoolClient.adminGetUser(new AdminGetUserRequest().withUsername(command.getEmail())
          .withUserPoolId(investorUserPoolId).withRequestCredentialsProvider(credentialsProvider));
      if (adminGetUser.getUserStatus().equals("FORCE_CHANGE_PASSWORD")) {
        userPoolClient
            .adminUpdateUserAttributes(new AdminUpdateUserAttributesRequest().withUsername(adminGetUser.getUsername())
                .withUserAttributes(new AttributeType().withName("email_verified").withValue("false"))
                .withUserPoolId(investorUserPoolId).withRequestCredentialsProvider(credentialsProvider));
      }
      adminGetUser = null;
    } catch (UserNotFoundException ex) {
      // do nothing - let him register
    } catch (Exception e) {
      log.error("Get User Details call failed for error:" + e.getMessage());
      throw new BadRequestException("Some error occured");
    }

    if (adminGetUser != null)
      sender().tell(Failed.failed(command.getUsername(), "Your specified Email is already taken", BAD_REQUEST), self());

    try {
      userPoolClient.adminCreateUser(new AdminCreateUserRequest().withUsername(command.getUsername())
          .withUserAttributes(getUserAttributes(command)).withMessageAction(MessageActionType.SUPPRESS)
          .withTemporaryPassword("Desk@123").withUserPoolId(investorUserPoolId)
          .withRequestCredentialsProvider(credentialsProvider));

    } catch (Exception e) {
      sender().tell(Failed.failed(command.getUsername(), e.getMessage(), BAD_REQUEST), self());
    }

  }

  private AttributeType getUserAttributes(RegisterInvestorFromUserPool command) {
    List<AttributeType> list = new ArrayList<>(5);
    list.add(new AttributeType().withName("name").withValue(command.getName()));
    list.add(new AttributeType().withName("username").withValue(command.getUsername()));
    list.add(new AttributeType().withName("email").withValue(command.getEmail()));
    return null;
  }

  private void checkAndUpdateRole() {
    if (user.getRole() == TU)
      return;
    if (user.getRole() == NTU && calculateRemainingDays(user.getRegisteredTimestamp()) == 0) {
      user.setRole(NTU_NA);
    }
  }

  private void handleUserRoleStatus(AuthenticatedUserCommand command) {
    sender().tell(new UserCurrentRoleStatus(user.getUsername(), "", user.getRole(), user.getIsWealthCode(),
        calculateRemainingDays(user.getRegisteredTimestamp())), self());
  }

  private int calculateRemainingDays(long registeredTimestamp) {
    int days = DateUtils.millisecondsToDays(DateUtils.currentTimeInMillis() - registeredTimestamp);
    int remainingDays = properties.USER_TRIAL_PERIOD() - days;
    if (remainingDays >= 0)
      return remainingDays;
    else
      return 0;
  }

  private void handleSecurityAnswers(SubmitSecurityAnswers command) {
    try {
      command.validate();
      user.validate(command);
    } catch (Exception e) {
      sender().tell(failed(user.getUsername(), e.getMessage(), BAD_REQUEST), self());
      return;
    }

    SecurityAnswersSubmitted event = new SecurityAnswersSubmitted(user.getUsername(), command.getAnswers());
    persist(event, evt -> {
      user.update(evt);
      sender().tell(new Done(user.getUsername(), "Security Answer have been added successfully"), self());
    });
    saveSnapshot();
  }

  private void handleUserResponseParameters(UserResponseParameters userResponseParameters) {

    if (userResponseParameters.getAdviseType().equals(Issue)) {
      log.info("Advise Issued: Sending In-App notifications to device: " + user.getDeviceToken());
      String message = "New investment in " + getSymbolFor(userResponseParameters.getToken()) + " in "
          + userResponseParameters.getFundName() + " is being made for a total sum of Rs. "
          + round(userResponseParameters.getAllocationValue()) + " at " + userResponseParameters.getExchange()
          + ". Please approve a market order for client code " + userResponseParameters.getClientCode()
          + " associated with broker " + userResponseParameters.getBrokerName() + ".";
      sendSNSNotification(userResponseParameters.getAdviserName(), userResponseParameters.getFundName(),
          "Advice Issued", message, Actionable, userResponseParameters.getInvestingMode());
      sendEmailToUserForApproval(userResponseParameters);
    } else if (userResponseParameters.getAdviseType().equals(Close)) {
      log.info("Advise Closed: Sending In-App notifications to device: " + user.getDeviceToken());
      String message = "Closing investment in " + getSymbolFor(userResponseParameters.getToken()) + " in "
          + userResponseParameters.getFundName() + ". A total of " + userResponseParameters.getNumberOfShares()
          + " will be traded at " + userResponseParameters.getExchange()
          + ". Please approve a market order for client code " + user.getClientCode() + " associated with broker "
          + user.getBrokerCompany() + ".";
      sendSNSNotification(userResponseParameters.getAdviserName(), userResponseParameters.getFundName(),
          "Advice Closed", message, Actionable, userResponseParameters.getInvestingMode());
      sendEmailToUserForApproval(userResponseParameters);
      forwardToChildren(context(), userResponseParameters,
          "" + new UserFundCompositeKey(user.getUsername(), userResponseParameters.getFundName(),
              userResponseParameters.getAdviserName(), userResponseParameters.getInvestingMode()).persistenceId());
    }
  }

  private void sendEmailToUserForApproval(UserResponseParameters userResponseParameters) {
    if (properties.isSendingUserEmailUpdatesAllowed())
      sendSMSOrEmailToUser(userResponseParameters);
  }

  private void handleInsufficientCapital(UserFundEvent event) {
    UserFundAdviseIssuedInsufficientCapital userFundAdviseIssuedInsufficientCapital = (UserFundAdviseIssuedInsufficientCapital) event;
    log.info("Insufficient Funds: Sending In-App notifications to device: " + user.getDeviceToken());
    tellChildren(context(), self(), userFundAdviseIssuedInsufficientCapital,
        "" + new UserFundCompositeKey(user.getUsername(), userFundAdviseIssuedInsufficientCapital.getFundName(),
            userFundAdviseIssuedInsufficientCapital.getAdviserUsername(),
            userFundAdviseIssuedInsufficientCapital.getInvestingMode()).persistenceId());
    String message = "Insufficient funds in " + userFundAdviseIssuedInsufficientCapital.getFundName()
        + " to process adviseId " + userFundAdviseIssuedInsufficientCapital.getAdviseId() + " for token "
        + userFundAdviseIssuedInsufficientCapital.getToken() + " with allocation value of Rs. "
        + userFundAdviseIssuedInsufficientCapital.getAdjustAllocation();
    sendSNSNotification(userFundAdviseIssuedInsufficientCapital.getAdviserUsername(),
        userFundAdviseIssuedInsufficientCapital.getFundName(), "Insufficient Funds", message, ChangeInvestment,
        userFundAdviseIssuedInsufficientCapital.getInvestingMode());
  }

  private void handleUserOpenAdviseRejection(UserFundEvent event) {
    UserOpenAdviseOrderRejectedBuilder userOpenAdviseOrderRejected = (UserOpenAdviseOrderRejectedBuilder) event;
    log.info("Open advise order rejected: Sending In-App notification to device: " + user.getDeviceToken());
    tellChildren(context(), self(), userOpenAdviseOrderRejected,
        "" + new UserFundCompositeKey(user.getUsername(), userOpenAdviseOrderRejected.getFundName(),
            userOpenAdviseOrderRejected.getAdviserUsername(), userOpenAdviseOrderRejected.getInvestingMode())
                .persistenceId());
    String message = "Advise  " + userOpenAdviseOrderRejected.getAdviseId()
        + " cannot be honored due to the associated order rejection. An amount of "
        + userOpenAdviseOrderRejected.getAllocationValue() + " is being reversed to your account.";
    sendSNSNotification(userOpenAdviseOrderRejected.getAdviserUsername(), userOpenAdviseOrderRejected.getFundName(),
        "Open Advise Order Rejected", message, Investment, userOpenAdviseOrderRejected.getInvestingMode());
  }

  private void handleUserCloseAdviseRejection(UserFundEvent event) {
    UserCloseAdviseOrderRejected userCloseAdviseOrderRejected = (UserCloseAdviseOrderRejected) event;
    log.info("Close advise order rejected: Sending In-App notification to device: " + user.getDeviceToken());
    tellChildren(context(), self(), userCloseAdviseOrderRejected,
        "" + new UserFundCompositeKey(user.getUsername(), userCloseAdviseOrderRejected.getFundName(),
            userCloseAdviseOrderRejected.getAdviserUsername(), userCloseAdviseOrderRejected.getInvestingMode())
                .persistenceId());

    String message = "Close advise  " + userCloseAdviseOrderRejected.getAdviseId()
        + " cannot be honored due to the associated order rejection.";

    sendSNSNotification(userCloseAdviseOrderRejected.getAdviserUsername(), userCloseAdviseOrderRejected.getFundName(),
        "Close Advise Order Rejected", message, Investment, userCloseAdviseOrderRejected.getInvestingMode());
  }

  private void handleAdviseOrderExecutionAndForwardToChild(UserFundEvent event) {
    if (event instanceof AdviseTradesExecuted) {
      AdviseTradesExecuted adviseTradeExecuted = (AdviseTradesExecuted) event;
      tellChildren(context(), self(), adviseTradeExecuted,
          "" + new UserFundCompositeKey(user.getUsername(), adviseTradeExecuted.getFundName(),
              adviseTradeExecuted.getAdviserUsername(), adviseTradeExecuted.getInvestingMode()).persistenceId());

      if (adviseTradeExecuted.getUserAdviseState().equals(UserAdviseState.OpenAdviseOrderRejected))
        return;

      try {
        if (adviseTradeExecuted.getOrderSide().equals(BUY))
          sendSNSNotification(adviseTradeExecuted.getAdviserUsername(), adviseTradeExecuted.getFundName(),
              "Open Advise Trade Executed",
              "A total of " + adviseTradeExecuted.getNumberOfShares() + " shares of "
                  + getSymbolFor(adviseTradeExecuted.getSymbol()) + " has been bought at an average price of Rs."
                  + round(adviseTradeExecuted.getExecutionPrice()),
              Investment, adviseTradeExecuted.getInvestingMode());
        else if (adviseTradeExecuted.getOrderSide().equals(SELL))
          sendSNSNotification(adviseTradeExecuted.getAdviserUsername(), adviseTradeExecuted.getFundName(),
              "Close Advise Trade Executed",
              "A total of " + adviseTradeExecuted.getNumberOfShares() + " shares of "
                  + getSymbolFor(adviseTradeExecuted.getSymbol()) + " has been sold at an average price of Rs."
                  + round(adviseTradeExecuted.getExecutionPrice()),
              Investment, adviseTradeExecuted.getInvestingMode());
      } catch (NullPointerException e) {
        log.error("Null pointer exception due to " + e.getMessage());
      }
    }
  }

  private void handleExitFundEvent(UserFundEvent event) {
    if (event instanceof FundExited) {
      FundExited fundExited = (FundExited) event;

      sendSNSNotification(fundExited.getAdviserUsername(), fundExited.getFundName(), "Fund Exited",
          fundExited.getMessage(), Investment, fundExited.getInvestingMode());

      UserUnsubscribedToFund userUnSubscribedToFund = new UserUnsubscribedToFund(user.getUsername(),
          fundExited.getAdviserUsername(), fundExited.getFundName(), fundExited.getInvestingMode(),
          fundExited.getAmount(), currentTimeInMillis());

      persist(userUnSubscribedToFund, (UserUnsubscribedToFund evt) -> {
        user.update(userUnSubscribedToFund);
        requestMarginIn(userUnSubscribedToFund.getInvestingmode());
      });
      saveSnapshot();
    }
  }

  private void sendSNSNotification(String adviserUsername, String fundName, String title, String message,
      NotificationTypes type, InvestingMode investingMode) {
    if (properties.USER_SNS_ENABLED()) {
      Map<String, String> attributes = setAttributes(adviserUsername, fundName, title, type, message, investingMode);
      log.info("Advise Issued: Sending In-App notifications to device: " + user.getDeviceToken());

      notificationFacade.sendNotification(user.getDeviceToken(), message, attributes);
    }
  }

  private Map<String, String> setAttributes(String adviserUsername, String fundName, String title,
      NotificationTypes type, String message, InvestingMode investingMode) {
    Map<String, String> additionalAttributes = new HashMap<String, String>();
    additionalAttributes.put("fund_name", fundName);
    additionalAttributes.put("author", adviserUsername);
    additionalAttributes.put("title", title);
    additionalAttributes.put("type", type.name());

    Notification notificationDomain = createNotificationDomain(adviserUsername, fundName, getRequiredUsername(), title,
        message, type, investingMode);
    String recordString = (new Gson().toJson(notificationDomain).toString());
    additionalAttributes.put("recordString", recordString);
    additionalAttributes.put("id", getRequiredUsername());
    additionalAttributes.put("userType", "User");
    additionalAttributes.put("investingMode", investingMode.name());
    additionalAttributes.put("tablename", "notification");

    return additionalAttributes;
  }

  private String getRequiredUsername() {
    if (user.getUsername().equals("rhunadkat@outlook.com"))
      return "rhunadkat1";
    return user.getUsername().split("@")[0];
  }

  private Notification createNotificationDomain(String adviserUsername, String fundName, String username, String title,
      String message, NotificationTypes type, InvestingMode investingMode) {
    Map<String, String> notificationMap = Maps.newHashMap();
    notificationMap.put("author", adviserUsername);
    notificationMap.put("fund_name", fundName);
    notificationMap.put("title", title);
    notificationMap.put("type", type.name());
    if (investingMode != null)
      notificationMap.put("investingMode", investingMode.getInvestingMode());
    else
      notificationMap.put("investingMode", "NONE");
    notificationMap.put("message", message);

    NotificationCompositeKey notificationCompositeKey = new NotificationCompositeKey();
    notificationCompositeKey.setUsername(username);
    notificationCompositeKey.setTimestamp(DateUtils.currentTimeInMillis());
    Notification notification = new Notification(notificationCompositeKey, notificationMap);
    return notification;
  }

  private void handleUserAnswers(SubmitUserAnswers command) {
    RiskLevel riskProfile = user.decideRiskProfile(command);
    UserRiskProfileIdentified event = new UserRiskProfileIdentified(user.getUsername(), command.getEventName(),
        command.getAnswers(), riskProfile);
    persist(event, evt -> {
      user.update(evt);
      sender().tell(new Done(evt.getEventName(), " Risk profile identified"), self());
    });
    saveSnapshot();
  }

  private void handleUpdateWealthCode(UpdateWealthCode command) {
    if (user.getRole().equals(UserType.TU)) {
      try {
        command.validate(user);
      } catch (IllegalArgumentException e) {
        sender().tell(failed(user.getUsername(), e.getMessage(), BAD_REQUEST), self());
        return;
      }

      String wealthCode = command.getWealthCode();
      WealthCodeUpdated event = new WealthCodeUpdated(user.getUsername(), wealthCode);
      persist(event, (WealthCodeUpdated evt) -> {
        user.update(evt);
        sender().tell(new Done(user.getUsername(), "Wealth Code updated"), self());
      });
      saveSnapshot();
    } else {
      sender().tell(Forbidden.forbidden(user.getUsername(),
          "User is not allowed to update Wealth Code, Get your KYC done to set Wealth Code."), self());
    }
  }

  private void handleGetOTP(SendOTP getOTPCommand) {
    try {
      getOTPCommand.validate();
    } catch (IllegalArgumentException e) {
      sender().tell(failed(user.getUsername(), e.getMessage(), BAD_REQUEST), self());
      return;
    }
    String phoneNumber = getOTPCommand.getPhonenumber();

    String generatedOTP = otpUtils.getNewOtp();

    try {
      String otpMessage = generatedOTP + " is your One Time Password for WealthDesk";
      notificationFacade.sendOTP(otpMessage, getValidatedUserPhoneNumber(getOTPCommand.getPhonenumber()));
      if (user.getEmail() != null && !user.getEmail().isEmpty() && properties.isSendingUserEmailUpdatesAllowed())
        emailer.prepareAndSendEmail(OTPEmail.class, generatedOTP, user.getEmail());
    } catch (IOException e) {
      e.printStackTrace();
    }
    phoneNumberYetToBeVerified = phoneNumber;
    lastSentOTP = generatedOTP;
    lastSentOTPTimestamp = DateUtils.currentTimeInMillis();
    sender().tell(new Done(user.getUsername(), "OTP sent"), self());
  }

  private void handleValidateOTP(AuthenticatedUserCommand command) {
    ValidateOTP validateOTPCommand = (ValidateOTP) command;
    String otp = validateOTPCommand.getOtp();
    if (otpUtils.isOTPTimeValid(otp, lastSentOTP, lastSentOTPTimestamp)) {
      PhoneNumberUpdated event = new PhoneNumberUpdated(user.getUsername(), phoneNumberYetToBeVerified);
      persist(event, (PhoneNumberUpdated evt) -> {
        user.update(evt);
        sender().tell(new Done(user.getUsername(), "Phone number updated"), self());
      });
      saveSnapshot();
      updateSalesTeam();
    } else {
      sender().tell(failed(otp, "Invalid OTP or OTP has been expired", UNAUTHORIZED), self());
    }
  }

  private void updateSalesTeam() {
    emailer
        .sendEmail(new UserAppliedForKYC(user.getFirstName(), user.getEmail(), phoneNumberYetToBeVerified, properties));
    BaseSMSTemplate message = smsFactory.getSMS(UserAppliedForKYCSMS.class.getSimpleName(), user.getUsername(),
        user.getEmail(), user.getPhoneNumber());
    String[] listOfPhoneNumbers = properties.BROKER_SALES_TEAM_PHONE_NUMBER().split("(;)|(,)");
    for (String phoneNumber : listOfPhoneNumbers) {
      notificationFacade.sendSMSMessage(message.getPlainTextFormat(), getValidatedUserPhoneNumber(phoneNumber));
    }
  }

  private void handlePostMarketCommands() {
    if (isExecutionWindowOpen() && properties.MARKET_OFFLINE_ENABLED()) {
      ArrayDeque<AddToPostMarketCommandQueue> postMarketCommandQueue = user.getPostMarketSubscriptionCommandsQueue();
      if (!postMarketCommandQueue.isEmpty()) {
        persist(new PostMarketCommandQueueProcessingStarted(user.getUsername(), postMarketCommandQueue), evt -> {
          user.update(evt);
        });
        saveSnapshot();

        postMarketCommandQueue.forEach(command -> {
          if (command.getClazz().equals(UnsubscribeToFund.class.getSimpleName())) {
            UnsubscribeToFund unsubscribeToFund = UnsubscribeToFund.fromPostMarketCommand(command.getVariables());
            unsubscribeToFund.setThisaPostMarketCommand(true);
            self().tell(unsubscribeToFund, self());
          } else if (command.getClazz().equals(SubscribeToFund.class.getSimpleName())) {
            SubscribeToFund subscribeToFund = SubscribeToFund.fromPostMarketCommand(command.getVariables());
            subscribeToFund.setThisaPostMarketCommand(true);
            self().tell(subscribeToFund, self());
          } else if (command.getClazz().equals(AddLumpsumToFund.class.getSimpleName())) {
            AddLumpsumToFund addLumpsumToFund = AddLumpsumToFund.fromPostMarketCommand(command.getVariables());
            addLumpsumToFund.setThisaPostMarketCommand(true);
            self().tell(addLumpsumToFund, self());
          } else if (command.getClazz().equals(AcceptUserApprovalResponse.class.getSimpleName())) {
            self().tell(AcceptUserApprovalResponse.fromPostMarketCommand(command.getVariables()), self());
          } else if (command.getClazz().equals(RetryRejectedUserAdvise.class.getSimpleName())) {
            self().tell(RetryRejectedUserAdvise.fromPostMarketCommand(command.getVariables()), self());
          } else if (command.getClazz().equals(PartialWithdrawalUnderway.class.getSimpleName())) {
            PartialWithdrawalUnderway partialWithdrawalUnderway = PartialWithdrawalUnderway
                .fromPostMarketCommand(command.getVariables());
            partialWithdrawalUnderway.setThisaPostMarketCommand(true);
            self().tell(partialWithdrawalUnderway, self());
          } else {
            log.error("Unhandled Commands in Post Market Command Queue, " + "Class Name:" + command.getClazz()
                + "Variables: " + command.getVariables().entrySet().stream()
                    .map(entry -> entry.getKey() + ":" + entry.getValue()).collect(Collectors.joining(","))
                + ", User " + user.getUsername());
          }
        });
      }
    }
  }

  private void handleSubscribeToFundUser(AuthenticatedUserCommand command) throws ParseException {
    SubscribeToFund userFundCommand = (SubscribeToFund) command;
    
    FundDetails checkFundDetails = getFundDetails(userFundCommand.getAdviserUsername(), userFundCommand.getFundName());
    if (checkFundDetails == null) {
      sender().tell(failed(user.getUsername(),
          "Subscription to fund " + userFundCommand.getFundName() + " failed, fund not found", NOT_FOUND), self());
      return;
    }
    if (user.isSubscribed(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode())) {
      sender().tell(failed(userFundCommand.getFundName(), "Already subscribed", BAD_REQUEST), self());
      return;
    }

    if (userFundCommand.getInvestingMode() == REAL
        && (user.getBrokerCompany().isEmpty() || user.getBrokerCompany() == null)) {
      sender().tell(failed(user.getUsername(),
          "Subscription to fund " + userFundCommand.getFundName() + " failed, your KYC is still pending", UNAUTHORIZED),
          self());
      return;
    }

    UserOnboardingStatus onboardingStatus = getOnboardingStatus(userFundCommand.getUsername());

    requestMarginIn(userFundCommand.getInvestingMode());

    double walletMoney = 0;
    if (userFundCommand.getInvestingMode() == VIRTUAL)
      walletMoney = user.getVirtualWalletAmount();
    else if (userFundCommand.getInvestingMode() == REAL)
      walletMoney = user.getRealWalletAmount();

    if (walletMoney < userFundCommand.getLumpSumAmount() && !onboardingStatus.isOnboarded()) {
      log.error("Error while subscribing fund.");
      sender().tell(
          failed(user.getUsername(),
              "Subscription to fund " + userFundCommand.getFundName()
                  + " failed. Reason: Insufficient capital in wallet. Required capital - "
                  + (userFundCommand.getLumpSumAmount()) + ". Available capital - " + walletMoney,
              PAYMENT_REQUIRED),
          self());
      return;
    }

    fundNametoAmountForSubscription.put(userFundCommand.getFundName(), userFundCommand.getLumpSumAmount());
    if (userFundCommand.isThisaPostMarketCommand()) {

      fundNametoAdviserUserName.put(userFundCommand.getFundName(), userFundCommand.getAdviserUsername());
      fundNametoInvestingMode.put(userFundCommand.getFundName(), userFundCommand.getInvestingMode());
      selections.advisersRootActor().tell(new GetCurrentFundComposition(userFundCommand.getFundName(),
          userFundCommand.getAdviserUsername(), userFundCommand.getUsername(), self()), self());

      return;
    }

    if (onboardingStatus.isOnboarded()) {
      FundDetails fundDetails = getFundDetails(userFundCommand.getAdviserUsername(), userFundCommand.getFundName());
      if (fundDetails == null) {
        sender().tell(failed(user.getUsername(),
            "Onboarding to fund " + userFundCommand.getFundName() + " failed, fund not found", NOT_FOUND), self());
        return;
      }
      String basketOrderId = valueOf("USERONBOARDING_" + userFundCommand.getUsername() + "_"
          + userFundCommand.getFundName() + "_" + valueOf(currentTimeInMillis()));
      SubscribeToFundWithUserEmail subscribeToFundWithUserEmailCommand = new SubscribeToFundWithUserEmail(
          userFundCommand.getAdviserUsername(), userFundCommand.getFundName(), userFundCommand.getUsername(),
          user.getEmail(), fundNametoAmountForSubscription.get(userFundCommand.getFundName()),
          userFundCommand.getSipAmount(), DateUtils.date(), fundDetails.getRiskLevel(), DateUtils.todayBOD() + "",
          userFundCommand.getInvestingMode(), userFundCommand.getInvestingPurpose(), null, basketOrderId, null);
      forwardToChildren(context(), subscribeToFundWithUserEmailCommand,
          "" + new UserFundCompositeKey(user.getUsername(), userFundCommand.getFundName(),
              userFundCommand.getAdviserUsername(), userFundCommand.getInvestingMode()).persistenceId());

      return;
    }

    this.actorRefForReplyingWhenSubscribing = sender();
    fundNametoAmountForSubscription.put(userFundCommand.getFundName(), userFundCommand.getLumpSumAmount());
    selections.advisersRootActor().tell(new GetCurrentFundComposition(userFundCommand.getFundName(),
        userFundCommand.getAdviserUsername(), userFundCommand.getUsername(), self()), self());

  }

  private void handleAddLumpsumToFundUser(AuthenticatedUserCommand command) {

    AddLumpsumToFund userFundCommand = (AddLumpsumToFund) command;

    if (!user.isSubscribed(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode())) {
      sender().tell(failed(userFundCommand.getFundName(), "Fund not subscribed", BAD_REQUEST), self());
      return;
    }

    if (isBrokerNameEmptyOrNull(userFundCommand.getInvestingMode())) {
      sender().tell(Failed.failed(user.getUsername(),
          "Adding lumpsum to fund " + userFundCommand.getFundName() + " failed, your KYC is still pending",
          UNAUTHORIZED), self());
      return;
    }

    if (!isPendingOrdersListEmpty(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode())) {
      sender().tell(Failed.failed(user.getUsername(),
          "Adding lumpsum to fund " + userFundCommand.getFundName()
              + " failed. Kindly approve the pending approvals in the fund, then proceed with adding LumpSum. "
              + "If approved, please wait till the trade confirmations are received from the exchange",
          BAD_REQUEST), self());
      return;
    }

    if (isFundArchived(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode())) {
      sender().tell(failed(user.getUsername(), "Adding lumpsum to fund " + userFundCommand.getFundName()
          + " failed. Your Fund is in archived state, you can take this action after successful re-subscription to this fund",
          BAD_REQUEST), self());
      return;
    }

    requestMarginIn(userFundCommand.getInvestingMode());

    double walletMoney = 0;
    if (userFundCommand.getInvestingMode() == VIRTUAL)
      walletMoney = user.getVirtualWalletAmount();
    else if (userFundCommand.getInvestingMode() == REAL)
      walletMoney = user.getRealWalletAmount();

    if (walletMoney < userFundCommand.getLumpSumAmount()) {
      log.error("Error while adding lumpsum to fund.");
      sender().tell(
          failed(user.getUsername(),
              "Adding lumpsum to fund " + userFundCommand.getFundName()
                  + " failed. Reason: Insufficient capital in wallet. Required capital - "
                  + (userFundCommand.getLumpSumAmount()) + ". Available capital - " + walletMoney,
              PAYMENT_REQUIRED),
          self());
      return;
    }

    if (userFundCommand.isThisaPostMarketCommand()) {

      fundNametoAdviserUserName.put(userFundCommand.getFundName(), userFundCommand.getAdviserUsername());
      fundNametoAmountForSubscription.put(userFundCommand.getFundName(), userFundCommand.getLumpSumAmount());
      fundNametoInvestingMode.put(userFundCommand.getFundName(), userFundCommand.getInvestingMode());
      selections.advisersRootActor().tell(new GetCurrentFundComposition(userFundCommand.getFundName(),
          userFundCommand.getAdviserUsername(), userFundCommand.getUsername(), self()), self());

      return;
    }

    this.actorRefForReplyingWhenSubscribing = sender();
    fundNametoAmountForSubscription.put(userFundCommand.getFundName(), userFundCommand.getLumpSumAmount());
    selections.advisersRootActor().tell(new GetCurrentFundComposition(userFundCommand.getFundName(),
        userFundCommand.getAdviserUsername(), userFundCommand.getUsername(), self()), self());

  }

  private void handleWithdrawalFromFundUser(AuthenticatedUserCommand command) {
    WithdrawalFromFund userFundCommand = (WithdrawalFromFund) command;

    if (!user.isSubscribed(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode())) {
      sender().tell(failed(userFundCommand.getFundName(), "Not subscribed", BAD_REQUEST), self());
      return;
    }

    if (!isPendingOrdersListEmpty(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode())) {
      sender().tell(Failed.failed(user.getUsername(),
          "Withdrawal from" + userFundCommand.getFundName()
              + " failed. Kindly approve the pending approvals in the fund, then proceed with Withdrawal."
              + "If approved, please wait till please wait till previous trade confirmations are received from the exchange",
          BAD_REQUEST), self());
      return;
    }

    if (isFundArchived(userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode())) {
      sender()
          .tell(
              Failed.failed(user.getUsername(),
                  "Withdrawal from" + userFundCommand.getFundName()
                      + " failed. Your Fund is in archived state, you cannot" + " take this action ",
                  BAD_REQUEST),
              self());
      return;
    }
    fundNametoAmountForSubscription.put(userFundCommand.getFundName(), userFundCommand.getWithdrawalAmount());
    String basketOrderId = valueOf("POSTMARKETWITHDRAWAL_" + userFundCommand.getUsername() + "_"
        + userFundCommand.getFundName() + "_" + valueOf(currentTimeInMillis()));
    PartialWithdrawalUnderwayBuilder partialWithdrawalUnderway = new PartialWithdrawalUnderwayBuilder(
        userFundCommand.getUsername(), userFundCommand.getAdviserUsername(), userFundCommand.getFundName(),
        userFundCommand.getInvestingMode()).withWithdrawalAmount(userFundCommand.getWithdrawalAmount())
            .withBasketOrderId(basketOrderId);

    forwardToChildren(context(), partialWithdrawalUnderway.build(),
        "" + new UserFundCompositeKey(user.getUsername(), userFundCommand.getFundName(),
            userFundCommand.getAdviserUsername(), userFundCommand.getInvestingMode()).persistenceId());
    if (userFundCommand.isThisaPostMarketCommand()) {
      continueToWithdrawPartially(new FundActionApproved(userFundCommand.getUsername(), userFundCommand.getFundName(),
          userFundCommand.getAdviserUsername(), user.getWealthCode(), userFundCommand.getInvestingMode(), basketOrderId,
          PARTIALWITHDRAWAL, Approve, BrokerAuthInformation.withNoBrokerAuth()));
    }
  }

  private void withdrawalFromFundPostMarket(PartialWithdrawalUnderway userFundCommand) {
    try {
      AddToPostMarketCommandQueue event = new AddToPostMarketCommandQueue(user.getUsername(),
          getUserFundCommandVariablesInMap(userFundCommand), userFundCommand.getClass().getSimpleName());
      user.validate(event);
      persist(event, evt -> {
        user.update(evt);
        sender().tell(new Done(user.getUsername(),
            "Your withdrawal request has been received.You will get your withdrawl amount when"
                + " the markets open on the next trading day."),
            self());
      });
      saveSnapshot();
    } catch (JsonProcessingException e) {
      sender().tell(new Failed(user.getUsername(), "Could not add your request in queue.", FAILED_DEPENDENCY), self());
    } catch (Exception e) {
      sender().tell(
          new Failed(user.getUsername(),
              "You cannot send more than one request in the same fund during non-market hours.", PRECONDITION_FAILED),
          self());
    }
    return;
  }

  private boolean isFundArchived(String adviserUsername, String fundName, InvestingMode investingMode) {
    UserFundSubscriptionDetails userFundSubscriptionDetails;
    try {
      userFundSubscriptionDetails = (UserFundSubscriptionDetails) askChild(getContext(),
          new GetUserFundStatus(user.getUsername(), adviserUsername, fundName, investingMode),
          new UserFundCompositeKey(user.getUsername(), fundName, adviserUsername, investingMode).persistenceId());
    } catch (Exception e) {
      return false;
    }
    return userFundSubscriptionDetails.getIsFundArchived().equals("true");
  }

  private boolean isPendingOrdersListEmpty(String adviserUsername, String fundName, InvestingMode investingMode) {
    AllPendingUserFundApprovals allPendingUserFundApprovals;
    try {
      GetPendingUserFundApprovals getPendingUserFundApprovals = new GetPendingUserFundApprovals(user.getUsername(),
          adviserUsername, fundName, investingMode);
      allPendingUserFundApprovals = getPendingApprovalsFromUserFund(getPendingUserFundApprovals);
    } catch (Exception e) {
      return false;
    }
    return allPendingUserFundApprovals.getUserResponseParameters().isEmpty() && allPendingUserFundApprovals.isPreviousApprovalsTradesPending();
  }

  private AllRejectedUserFundAdvises getRejectedAdvisesFromUserFund(
      GetRejectedAdviseFromUserFund getRejectedAdviseFromUserFund) throws Exception {
    AllRejectedUserFundAdvises allRejectedUserFundAdvises;
    allRejectedUserFundAdvises = (AllRejectedUserFundAdvises) askChild(getContext(), getRejectedAdviseFromUserFund,
        new UserFundCompositeKey(user.getUsername(), getRejectedAdviseFromUserFund.getFundName(),
            getRejectedAdviseFromUserFund.getAdviserUsername(), getRejectedAdviseFromUserFund.getInvestingMode())
                .persistenceId());
    return allRejectedUserFundAdvises;
  }

  private AllPendingUserFundApprovals getPendingApprovalsFromUserFund(
      GetPendingUserFundApprovals getPendingUserFundApprovals) throws Exception {
    AllPendingUserFundApprovals allPendingUserFundApprovals;
    allPendingUserFundApprovals = (AllPendingUserFundApprovals) askChild(getContext(), getPendingUserFundApprovals,
        new UserFundCompositeKey(user.getUsername(), getPendingUserFundApprovals.getFundName(),
            getPendingUserFundApprovals.getAdviserUsername(), getPendingUserFundApprovals.getInvestingMode())
                .persistenceId());
    return allPendingUserFundApprovals;
  }

  private boolean isBrokerNameEmptyOrNull(InvestingMode investingMode) {
    return investingMode.equals(REAL) && (user.getBrokerCompany().isEmpty() || user.getBrokerCompany() == null);
  }

  private void addLumpSumPostMarket(AddLumpsumToFund userFundCommand) {
    try {
      AddToPostMarketCommandQueue event = new AddToPostMarketCommandQueue(user.getUsername(),
          getUserFundCommandVariablesInMap(userFundCommand), userFundCommand.getClass().getSimpleName());
      user.validate(event);
      persist(event, evt -> {
        user.update(evt);
        sender().tell(new Done(user.getUsername(),
            "Your add lumpsum request has been received.Lumpsum will be added to the subscribed to fund when"
                + " the markets open on the next trading day."),
            self());
      });
      saveSnapshot();
    } catch (JsonProcessingException e) {
      sender().tell(new Failed(user.getUsername(), "Could not add your request in queue.", FAILED_DEPENDENCY), self());
    } catch (Exception e) {
      sender().tell(
          new Failed(user.getUsername(),
              "You cannot send more than one request in the same fund during non-market hours.", PRECONDITION_FAILED),
          self());
    }
  }

  private Map<String, String> getUserFundCommandVariablesInMap(AuthenticatedUserCommand command)
      throws JsonProcessingException {
    Map<String, String> variables = new HashMap<String, String>();

    if (command instanceof SubscribeToFund) {
      SubscribeToFund cmd = (SubscribeToFund) command;
      variables.put("adviserUsername", cmd.getAdviserUsername());
      variables.put("fundName", cmd.getFundName());
      variables.put("username", cmd.getUsername());
      variables.put("investingMode", cmd.getInvestingMode().getInvestingMode());
      variables.put("investingPurpose", cmd.getInvestingPurpose().getPurpose());
      variables.put("lumpSumAmount", cmd.getLumpSumAmount() + "");
      variables.put("sipAmount", cmd.getSipAmount() + "");
      variables.put("sipDate", cmd.getSipDate());

      variables.put("clazz", cmd.getClass().getSimpleName());
    } else if (command instanceof AddLumpsumToFund) {
      AddLumpsumToFund cmd = (AddLumpsumToFund) command;
      variables.put("adviserUsername", cmd.getAdviserUsername());
      variables.put("fundName", cmd.getFundName());
      variables.put("username", cmd.getUsername());
      variables.put("investingMode", cmd.getInvestingMode().getInvestingMode());
      variables.put("lumpSumAmount", cmd.getLumpSumAmount() + "");

      variables.put("clazz", cmd.getClass().getSimpleName());
    } else if (command instanceof UnsubscribeToFund) {
      UnsubscribeToFund cmd = (UnsubscribeToFund) command;
      variables.put("adviserUsername", cmd.getAdviserUsername());
      variables.put("fundName", cmd.getFundName());
      variables.put("username", cmd.getUsername());
      variables.put("investingMode", cmd.getInvestingMode().name());

      variables.put("clazz", cmd.getClass().getSimpleName());
    } else if (command instanceof AcceptUserApprovalResponse) {
      AcceptUserApprovalResponse cmd = (AcceptUserApprovalResponse) command;
      variables.put("userResponseParameters", new ObjectMapper().writeValueAsString(cmd.getUserResponseParameters()));
      variables.put("username", cmd.getUsername());
      variables.put("basketOrderId", cmd.getBasketOrderId());
      variables.put("fundNames", cmd.getUserResponseParameters().stream().map(responses -> responses.getFundName())
          .collect(Collectors.joining(",")));

      variables.put("clazz", cmd.getClass().getSimpleName());
    } else if (command instanceof RetryRejectedUserAdvise) {
      RetryRejectedUserAdvise cmd = (RetryRejectedUserAdvise) command;
      variables.put("rejectedAdviseDetails", new ObjectMapper().writeValueAsString(cmd.getRejectedAdviseDetails()));
      variables.put("username", cmd.getUsername());
      variables.put("fundName", cmd.getRejectedAdviseDetails().stream().findAny().get().getFundName());
      variables.put("basketOrderId", cmd.getBasketOrderId());
      variables.put("clazz", cmd.getClass().getSimpleName());
    } else if (command instanceof PartialWithdrawalUnderway) {
      PartialWithdrawalUnderway cmd = (PartialWithdrawalUnderway) command;
      variables.put("adviserUsername", cmd.getAdviserUsername());
      variables.put("fundName", cmd.getFundName());
      variables.put("username", cmd.getUsername());
      variables.put("investingMode", cmd.getInvestingMode().name());
      variables.put("wealthcode", cmd.getWealthCode());
      variables.put("withdrawalAmount", cmd.getWithdrawalAmount() + "");
      variables.put("clazz", cmd.getClass().getSimpleName());
    }

    return variables;
  }

  private FundDetails getFundDetails(String adviserName, String fundName) {
    try {
      Object result = askAndWaitForResult(selections.advisersRootActor(), new GetFundDetails(adviserName, fundName));
      if (result instanceof Failed)
        throw new Exception(((Failed) result).getMessage());
      return (FundDetails) result;
    } catch (Exception e) {
      log.error("Error while getting fund: ", e);
      return null;
    }
  }

  private UserOnboardingStatus getOnboardingStatus(String username) {
    UserOnboardingStatus onboardingStatus = null;

    try {
      onboardingStatus = (UserOnboardingStatus) askAndWaitForResult(context().parent(),
          new HasUserBeenOnBoarded(username));
    } catch (Exception e) {
      log.error("Unable to get onboarding status " + e.getMessage());
    }
    return onboardingStatus;
  }

  private void handleMarginReceivedCommand(MarginReceived command) throws ParseException {
    user.update(command);
    sender().tell(new Done(user.getUsername(), "User wallet amount updated"), self());
  }

  private void handleGetBrokerName(GetBrokerName command) {
    sender().tell(new BrokerNameSent(fromBrokerName(user.getBrokerCompany(), command.getInvestingMode())), self());
  }

  private void handleGetClientCode(GetClientCode command) {
    sender().tell(new RequestedClientCode(user.getClientCode()), self());
  }

  private void handleRetryingOfRejectedUserAdvise(RetryRejectedUserAdvise command) {
    RetryRejectedUserAdvise retryRejectedUserAdvise = (RetryRejectedUserAdvise) command;
    try {
      retryRejectedUserAdvise.validate();
    } catch (Exception e) {
      sender().tell(failed(retryRejectedUserAdvise.getUsername(), e.getMessage(), PRECONDITION_FAILED), self());
      return;
    }
    
    if (retryRejectedUserAdvise.isAuthticationInfoBlank()) {
      retryRejectedUserAdvise.setBrokerAuthInformation(BrokerAuthInformation.withNoBrokerAuth());
    }
    
    if (isExecutionWindowOpen())
      handleRetrials(retryRejectedUserAdvise);
    else if (!isExecutionWindowOpen() && properties.MARKET_OFFLINE_ENABLED()) {
      try {
        AddToPostMarketCommandQueue event = new AddToPostMarketCommandQueue(user.getUsername(),
            getUserFundCommandVariablesInMap(retryRejectedUserAdvise),
            retryRejectedUserAdvise.getClass().getSimpleName());
        user.validate(event);
        persist(event, evt -> {
          user.update(evt);
          sender().tell(new Done(user.getUsername(),
              "Your retrying request has been received and will be executed on next trading day."), self());
        });
      } catch (JsonProcessingException e) {
        sender().tell(new Failed(user.getUsername(), "Could not add your request in queue.", FAILED_DEPENDENCY), self());
      } catch (IllegalStateException e) {
        sender().tell(
            new Failed(user.getUsername(),
                "You cannot send more than one request in the same fund during non-market hours.", PRECONDITION_FAILED),
            self());
      }
    } else if (!isExecutionWindowOpen() && !properties.MARKET_OFFLINE_ENABLED()) {
      sender().tell( new Failed(user.getUsername(),
          "You cannot send this request during non-market hours.", PRECONDITION_FAILED), self());
      return;
    }
  }

  private void handleRetrials(RetryRejectedUserAdvise retryRejectedUserAdvise) {
    String username = retryRejectedUserAdvise.getUsername();
    UserResponseParameters anyUserResponse = retryRejectedUserAdvise.getRejectedAdviseDetails().stream().findAny()
        .orElse(null);
    String adviserUserName = anyUserResponse.getAdviserName();
    String fundName = anyUserResponse.getFundName();
    InvestingMode investingMode = anyUserResponse.getInvestingMode();
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(username, fundName, adviserUserName,
        anyUserResponse.getInvestingMode());
    RetryUserAdviseWithClientInformation retryUserAdvise = new RetryUserAdviseWithClientInformation(adviserUserName,
        fundName, retryRejectedUserAdvise.getRejectedAdviseDetails(), user.getClientCode(),
        fromBrokerName(user.getBrokerCompany(), investingMode), investingMode, retryRejectedUserAdvise.getBrokerAuthInformation());
    forwardToChildren(context(), retryUserAdvise, userFundCompositeKey.persistenceId());
  }

  private void handleUserResponseOnAdvise(AcceptUserApprovalResponse command) {
    AcceptUserApprovalResponse acceptUserApprovalResponse = (AcceptUserApprovalResponse) command;
    UserResponseParameters anyUserResponse = acceptUserApprovalResponse.getUserResponseParameters().stream().findAny()
        .orElse(null);
    String basketOrderId = command.getBasketOrderId();
    if (acceptUserApprovalResponse.isAuthticationInfoBlank()) {
      acceptUserApprovalResponse.setBrokerAuthInformation(BrokerAuthInformation.withNoBrokerAuth());
    }
    if (anyUserResponse != null) {
      if (acceptUserApprovalResponse.isBaskerOrderIdBlank()) {
        log.warn("Blank BasketOrderId in Rebalancing Approval command for user : " + user.getUsername() + " and fund "
            + anyUserResponse.getFundName());
        sender().tell(failed(user.getUsername(),
            "BasketOrderId is blank. Your request to approve pending approvals cannot be processed.",
            PRECONDITION_FAILED), self());
      } else if (anyUserResponse.getUserResponse().equals(Awaiting)) {
        sender().tell(failed(user.getUsername(), "Cannot send Awaiting in approvals!", PRECONDITION_FAILED), self());
      } else if (acceptUserApprovalResponse.isMultiFundApprovalsReceived()) {
        sender().tell(failed(user.getUsername(),
            "Could not process approvals as multiple fund approvals received for user: " + user.getUsername(),
            PRECONDITION_FAILED), self());
      } else if (!acceptUserApprovalResponse.arePendingApprovalsUnique()) {
        log.warn("Pending approvals are not unique and are not valid for the user : " + user.getUsername());
        sender().tell(failed(user.getUsername(),
            "Pending approvals are not unique and are not valid for this user. Your request to approve pending approvals cannot be processed.",
            PRECONDITION_FAILED), self());
      } else if (!isValidPendingApprovalsFromUserFund(acceptUserApprovalResponse)) {
        log.warn("Pending approvals received are not valid for the user : " + user.getUsername());
        sender().tell(failed(user.getUsername(),
            "Pending approvals received are not valid for this user. Your request to approve pending approvals cannot be processed.",
            PRECONDITION_FAILED), self());
      } else if (!isExecutionWindowOpen() && !properties.MARKET_OFFLINE_ENABLED()) {
        sender().tell(new Failed(user.getUsername(), "You cannot send this request during non-market hours.",
            PRECONDITION_FAILED), self());
      } else if (!isExecutionWindowOpen() && properties.MARKET_OFFLINE_ENABLED()) {
        addApprovalsResponseToPostMarketQueue(acceptUserApprovalResponse);
      } else
        handleApprovals(acceptUserApprovalResponse, basketOrderId);
    } else {
      sender().tell(failed(user.getUsername(), "Could not process blank approvals!", PRECONDITION_FAILED), self());
      return;
    }
  }

  private void addApprovalsResponseToPostMarketQueue(AcceptUserApprovalResponse acceptUserApprovalResponse) {
    try {
      AddToPostMarketCommandQueue event = new AddToPostMarketCommandQueue(user.getUsername(),
          getUserFundCommandVariablesInMap(acceptUserApprovalResponse),
          acceptUserApprovalResponse.getClass().getSimpleName());
      user.validate(event);
      persist(event, evt -> {
        user.update(evt);
        sender().tell(new Done(user.getUsername(),
            "Your approval request has been received and will be executed on next trading day."), self());
      });
      saveSnapshot();
    } catch (JsonProcessingException e) {
      sender().tell(new Failed(user.getUsername(), "Could not add your request in queue.", FAILED_DEPENDENCY), self());
    } catch (IllegalStateException e) {
      sender().tell(
          new Failed(user.getUsername(),
              "You cannot send more than one request in the same fund during non-market hours.", PRECONDITION_FAILED),
          self());
    }
  }

  private void handleApprovals(AcceptUserApprovalResponse acceptUserApprovalResponse, String basketOrderId) {

    UserResponseParameters anyUserResponse = acceptUserApprovalResponse.getUserResponseParameters().stream().findAny()
        .orElse(null);
    InvestingMode investingMode = anyUserResponse.getInvestingMode();
    if (user.handleApprovals(acceptUserApprovalResponse, investingMode, selections)) {
      if (anyUserResponse.getUserResponse().equals(UserResponseType.Approve)) {
        forwardApprovalsToUserFund(acceptUserApprovalResponse, basketOrderId, investingMode);
        sender().tell(
            new Done(user.getUsername(), "Approvals in progress. Scoot over to your investments to see the status!"),
            self());
      } else if (anyUserResponse.getUserResponse().equals(Archive)) {
        forwardApprovalsToUserFund(acceptUserApprovalResponse, basketOrderId, investingMode);
        sender().tell(new Done(user.getUsername(), "Archiving fund " + anyUserResponse.getFundName()
            + ". You will not be receiving any updates pertaining to this fund now!"), self());
      } else if (anyUserResponse.getUserResponse().equals(ExitFund)) {
        forwardApprovalsToUserFund(acceptUserApprovalResponse, basketOrderId, investingMode);
        sender().tell(
            new Done(user.getUsername(), "Approvals in progress. Scoot over to your investments to see the status!"),
            self());
      }
    } else {
      String failureMessage = "Could not process approvals as required margin for execution of Rs. "
          + round(user.requiredMarginForApproval(acceptUserApprovalResponse, investingMode, selections))
          + " is more than the available margin of Rs. "
          + (investingMode.equals(VIRTUAL) ? user.getVirtualWalletAmount() : user.getRealWalletAmount())
          + ". Kindly consult your broker " + user.getBrokerCompany() + "!";
      log.warn(failureMessage);
      sender().tell(failed(user.getUsername(), failureMessage, PRECONDITION_FAILED), self());
      return;
    }
  }

  private boolean isValidPendingApprovalsFromUserFund(AcceptUserApprovalResponse acceptUserApprovalResponse) {
    UserResponseParameters anyUserResponse = acceptUserApprovalResponse.getUserResponseParameters().stream().findAny()
        .orElse(null);
    try {
      AllPendingUserFundApprovals allPendingUserFundApprovals = getPendingApprovalsFromUserFund(
          new GetPendingUserFundApprovals(anyUserResponse.getUsername(), anyUserResponse.getAdviserName(),
              anyUserResponse.getFundName(), anyUserResponse.getInvestingMode()));
      for (UserResponseParameters userResponse : acceptUserApprovalResponse.getUserResponseParameters()) {
        List<String> allUserFundPendingApprovalAdviseIds = new ArrayList<String>();
        allPendingUserFundApprovals.getUserResponseParameters().stream()
            .forEach(userFundApprovalResponse -> allUserFundPendingApprovalAdviseIds
                .add(userFundApprovalResponse.getAdviseId()));
        if (!allUserFundPendingApprovalAdviseIds.contains(userResponse.getAdviseId()))
          return false;
      }
      if (allPendingUserFundApprovals.getUserResponseParameters().size() != acceptUserApprovalResponse
          .getUserResponseParameters().size())
        return false;
      else
        return true;
    } catch (Exception e) {
      log.error("Unable to get pending orders for user : " + anyUserResponse.getUsername() + " for fund: "
          + anyUserResponse.getFundName());
    }
    return false;
  }

  private void forwardApprovalsToUserFund(AcceptUserApprovalResponse acceptUserApprovalResponse, String basketOrderId,
      InvestingMode investingMode) {
    streamOfResponses(acceptUserApprovalResponse, investingMode, basketOrderId).forEach(userResponse -> {
      userResponse.setBrokerAuthInformation(acceptUserApprovalResponse.getBrokerAuthInformation());
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(user.getUsername(),
          userResponse.getFundName(), userResponse.getAdviserName(), userResponse.getInvestingMode());
      forwardToChildren(context(), userResponse, userFundCompositeKey.persistenceId());
    });
  }

  private Stream<UserResponseParameters> streamOfResponses(AcceptUserApprovalResponse acceptUserApprovalResponse,
      InvestingMode investingMode, String basketOrderId) {

    List<UserResponseParameters> userResponseParametersList = new ArrayList<UserResponseParameters>();
    BrokerAuthInformation brokerAuthInformation = acceptUserApprovalResponse.getBrokerAuthInformation();
    AtomicInteger atomicInteger = new AtomicInteger(0);
    try {
      acceptUserApprovalResponse.getUserResponseParameters().stream().forEach(userResponseParameter -> {
        UserResponseParameters userResponseParameters = new UserResponseParameters.UserResponseParametersBuilder(
            userResponseParameter.getAdviseId(), userResponseParameter.getAdviseType(),
            userResponseParameter.getUsername(), userResponseParameter.getFundName(),
            userResponseParameter.getAdviserName(), userResponseParameter.getClientCode(),
            userResponseParameter.getBrokerName(), userResponseParameter.getExchange(), userResponseParameter.getToken(),
            userResponseParameter.getSymbol(), investingMode, userResponseParameter.getUserResponse(),
            userResponseParameter.getInvestmentDate())
            .withAbsoluteAllocation(userResponseParameter.getAbsoluteAllocation())
            .withAllocationValue(userResponseParameter.getAllocationValue())
            .withNumberOfShares(userResponseParameter.getNumberOfShares())
            .withBasketOrderCompositeId(basketOrderId + CompositeKeySeparator + atomicInteger.getAndIncrement())
            .withBrokerAuthInformation(brokerAuthInformation)
            .withAdviseType(getWriteSideAdviseType(userResponseParameter.getAdviseType()))
            .build();
        userResponseParametersList.add(userResponseParameters);
      });
    } catch (NullPointerException npe) {
      log.error("Could not get the adviseType: "+npe);
    }
    return userResponseParametersList.stream()
        .sorted(comparingLong(userApprovalResponse -> userApprovalResponse.getInvestmentDate()))
        .filter(userApprovalResponse -> userApprovalResponse.getInvestingMode().equals(investingMode));
  }

  private AdviseType getWriteSideAdviseType(AdviseType adviseType) {
    if (adviseType.equals(AdviseType.Issue))
      return AdviseType.Issue;
    else if (adviseType.equals(AdviseType.Close))
      return AdviseType.Close;
    else if (adviseType.equals(AdviseType.Buy))
      return AdviseType.Issue;
    else if (adviseType.equals(AdviseType.Sell))
      return AdviseType.Close;
    else
      throw new NullPointerException();
  }

  private void handleGetUserFundStatus(GetUserFundStatus command) {
    GetUserFundStatus fundStatus = (GetUserFundStatus) command;

    if (!user.isSubscribed(fundStatus.getAdviserUsername(), fundStatus.getFundName(), fundStatus.getInvestingMode())) {
      sender().tell(
          new UserFundSubscriptionDetails(user.getUsername(), command.getFundName(), command.getAdviserUsername(),
              command.getInvestingMode(), 0d,
              getWithdrawal(new UserFundCompositeKey(user.getUsername(), command.getFundName(),
                  command.getAdviserUsername(), command.getInvestingMode()).persistenceId()),
              "false", "false"),
          self());
      return;
    }

    forwardToChildren(context(), fundStatus, "" + new UserFundCompositeKey(user.getUsername(), command.getFundName(),
        command.getAdviserUsername(), command.getInvestingMode()).persistenceId());
  }

  private void handleGetPendingUserFundApprovals(GetPendingUserFundApprovals pendingUserFundApprovals) {
    String adviserName = pendingUserFundApprovals.getAdviserUsername();
    String fundName = pendingUserFundApprovals.getFundName();
    InvestingMode investingMode = pendingUserFundApprovals.getInvestingMode();
    if (user.isSubscribed(adviserName, fundName, investingMode))
      forwardToChildren(context(), pendingUserFundApprovals,
          "" + new UserFundCompositeKey(user.getUsername(), fundName, adviserName, investingMode).persistenceId());
    else
      sender().tell(failed(user.getUsername(), "You are not subscribed to this fund!", FORBIDDEN), self());
  }

  private void handleAcceptUserAcceptanceMode(AcceptUserAcceptanceMode acceptUserAcceptanceMode) {
    String adviserName = acceptUserAcceptanceMode.getAdviserName();
    String fundName = acceptUserAcceptanceMode.getFundName();
    InvestingMode investingMode = acceptUserAcceptanceMode.getInvestingMode();
    if (user.isSubscribed(adviserName, fundName, investingMode))
      forwardToChildren(context(), acceptUserAcceptanceMode,
          new UserFundCompositeKey(user.getUsername(), fundName, adviserName, investingMode).persistenceId());
    else
      sender().tell(failed(user.getUsername(), "You are not subscribed to this fund!", FORBIDDEN), self());
  }
  
  private void handleUpdateUserFundCash(UpdateUserFundCash updateUserFundCash) {
    String adviserName = updateUserFundCash.getAdviserUsername();
    String fundName = updateUserFundCash.getFundName();
    InvestingMode investingMode = updateUserFundCash.getInvestingMode();
    if (user.isSubscribed(adviserName, fundName, investingMode))
      forwardToChildren(context(), updateUserFundCash,
          new UserFundCompositeKey(user.getUsername(), fundName, adviserName, investingMode).persistenceId());
    else
      sender().tell(failed(user.getUsername(), "You are not subscribed to this fund!", FORBIDDEN), self());
  }

  private void handleGetUserCurrentApprovalStatus(GetUserCurrentApprovalStatus getUserCurrentApprovalStatus) {
    String adviserName = getUserCurrentApprovalStatus.getAdviserUsername();
    String fundName = getUserCurrentApprovalStatus.getFundName();
    InvestingMode investingMode = getUserCurrentApprovalStatus.getInvestingMode();
    if (user.isSubscribed(adviserName, fundName, investingMode))
      forwardToChildren(context(), getUserCurrentApprovalStatus,
          new UserFundCompositeKey(user.getUsername(), fundName, adviserName, investingMode).persistenceId());
    else
      sender().tell(failed(user.getUsername(), "You are not subscribed to this fund!", FORBIDDEN), self());
  }

  private double getWithdrawal(String persistenceId) {
    return journalProvider.fetchEventsByPersistenceId(persistenceId).stream()
        .filter(evt -> evt instanceof FundSuccessfullyUnsubscribed).map(evt -> (FundSuccessfullyUnsubscribed) evt)
        .mapToDouble(evt -> evt.getCurrentCashComponent()).findFirst().orElse(0d);
  }

  private void requestMarginIn(InvestingMode investingMode) {
    if (investingMode.equals(VIRTUAL))
      return;
    try {
      if (user.getBrokerCompany() != null && user.getUsername() != null) {
        walletFactory.getWallet(user.getBrokerCompany(), investingMode)
            .requestWalletMoney(new MarginRequested(user.getClientCode(), user.getUsername(), user.getBrokerCompany()));
      } else {
        log.debug("Stopping request for margin with NULL broker or NULL username");
      }
    } catch (IOException e) {
      log.error("Unable to retrieve margin as " + e.getMessage());
    }
  }

  private void handleSubscribeToFundEventStreamCommand(AuthenticatedUserCommand command) {
    log.info("Actor " + persistenceId + " is alive.");
  }

  private void handleFundEvent(FundEvent command) {
    if (!(user.getRole() == UserType.NTU || user.getRole() == UserType.TU)) {
      return;
    }
    if (command instanceof FundAdviceIssued) {
      FundAdviceIssued fundAdviceIssued = (FundAdviceIssued) command;
      String symbol = getSymbolFor(fundAdviceIssued.getToken());
      checkSubscriptionStatusAndSendCommandToChildren(new FundAdviseIssuedWithClientInformation(
          command.getAdviserUsername(), command.getFundName(), fundAdviceIssued.getAdviseId(),
          fundAdviceIssued.getToken(), fundAdviceIssued.getAllocation(), fundAdviceIssued.getAbsoluteAllocation(),
          fundAdviceIssued.getIssueTime(), fundAdviceIssued.getEntryPrice(), fundAdviceIssued.getEntryTime(),
          fundAdviceIssued.getExitPrice(), fundAdviceIssued.getExitTime(), user.getClientCode(),
          fromBrokerName(user.getBrokerCompany(), REAL), symbol, fundAdviceIssued.isSpecialAdvise(), false), REAL);

      checkSubscriptionStatusAndSendCommandToChildren(
          new FundAdviseIssuedWithClientInformation(command.getAdviserUsername(), command.getFundName(),
              fundAdviceIssued.getAdviseId(), fundAdviceIssued.getToken(), fundAdviceIssued.getAllocation(),
              fundAdviceIssued.getAbsoluteAllocation(), fundAdviceIssued.getIssueTime(),
              fundAdviceIssued.getEntryPrice(), fundAdviceIssued.getEntryTime(), fundAdviceIssued.getExitPrice(),
              fundAdviceIssued.getExitTime(), user.getClientCode(), fromBrokerName(user.getBrokerCompany(), VIRTUAL),
              symbol, fundAdviceIssued.isSpecialAdvise(), false),
          VIRTUAL);
    } else if (command instanceof FundAdviceClosed) {
      checkSubscriptionStatusAndSendCommandToChildren(command, REAL);
      checkSubscriptionStatusAndSendCommandToChildren(command, VIRTUAL);
    } else if (command instanceof MergerDemergerFundAdviseClosed){
      checkSubscriptionStatusAndSendCommandToChildren(command, REAL);
      checkSubscriptionStatusAndSendCommandToChildren(command, VIRTUAL);
    } else if (command instanceof MergerDemergerPrimaryBuyIssued){
      MergerDemergerPrimaryBuyIssued mergerDemergerPrimaryBuyIssued = (MergerDemergerPrimaryBuyIssued) command;
      mergerDemergerPrimaryBuyIssued.setClientCode(user.getClientCode());
      if(user.isSubscribed(command.getAdviserUsername(), command.getFundName(), VIRTUAL)){
        BrokerName brokerName = fromBrokerName(user.getBrokerCompany(),
            VIRTUAL);
        mergerDemergerPrimaryBuyIssued.setBrokerName(brokerName);
        checkSubscriptionStatusAndSendCommandToChildren(mergerDemergerPrimaryBuyIssued, VIRTUAL);
      }
      if(user.isSubscribed(command.getAdviserUsername(), command.getFundName(), REAL)){
        BrokerName brokerName = fromBrokerName(user.getBrokerCompany(),
            REAL);
        mergerDemergerPrimaryBuyIssued.setBrokerName(brokerName);
        checkSubscriptionStatusAndSendCommandToChildren(mergerDemergerPrimaryBuyIssued, REAL);
      }
    } else if (command instanceof MergerDemergerSecondaryBuyIssued){
      MergerDemergerSecondaryBuyIssued mergerDemergerSecondaryBuyIssued = (MergerDemergerSecondaryBuyIssued) command;
      mergerDemergerSecondaryBuyIssued.setClientCode(user.getClientCode());
      if(user.isSubscribed(command.getAdviserUsername(), command.getFundName(), VIRTUAL)){
        BrokerName brokerName = fromBrokerName(user.getBrokerCompany(),
            VIRTUAL);
        mergerDemergerSecondaryBuyIssued.setBrokerName(brokerName);
        checkSubscriptionStatusAndSendCommandToChildren(mergerDemergerSecondaryBuyIssued, VIRTUAL);
      }
      if(user.isSubscribed(command.getAdviserUsername(), command.getFundName(), REAL)){
        BrokerName brokerName = fromBrokerName(user.getBrokerCompany(),
            REAL);
        mergerDemergerSecondaryBuyIssued.setBrokerName(brokerName);
        checkSubscriptionStatusAndSendCommandToChildren(mergerDemergerSecondaryBuyIssued, REAL);
      }
    }
    
  }

  private void checkSubscriptionStatusAndSendCommandToChildren(FundEvent command, InvestingMode investingMode) {
    if (user.isSubscribed(command.getAdviserUsername(), command.getFundName(), investingMode)) {
      tellChildren(context(), self(), command, new UserFundCompositeKey(user.getUsername(), command.getFundName(),
          command.getAdviserUsername(), investingMode).persistenceId());
    }
  }

  private boolean emailIdIsMappedToClientCode(String clientCode) {
    if (user.getClientCode().equals(clientCode))
      return true;
    return false;
  }

  private void handleUnsubscribeToFundUser(AuthenticatedUserCommand command) {
    UnsubscribeToFund fundUserCommand = (UnsubscribeToFund) command;

    if (fundUserCommand.isThisaPostMarketCommand()) {
      String basketOrderId = valueOf("POSTMARKETUNSUBSCRIPTION_" + fundUserCommand.getUsername() + "_"
          + fundUserCommand.getFundName() + "_" + valueOf(currentTimeInMillis()));
      self().tell(new FundActionApproved(user.getUsername(), fundUserCommand.getFundName(),
          fundUserCommand.getAdviserUsername(), user.getWealthCode(), fundUserCommand.getInvestingMode(), basketOrderId,
          UNSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()), ActorRef.noSender());
    }
    if (!user.isSubscribed(fundUserCommand.getAdviserUsername(), fundUserCommand.getFundName(),
        fundUserCommand.getInvestingMode())) {
      sender().tell(failed(user.getUsername(), "Not subscribed", FORBIDDEN), self());
      return;
    }

    GetUnsubscriptionSnapshotOfFund getUnsubscriptionSnapshotOfFund = new GetUnsubscriptionSnapshotOfFund();

    forwardToChildren(context(), getUnsubscriptionSnapshotOfFund,
        "" + new UserFundCompositeKey(user.getUsername(), fundUserCommand.getFundName(),
            fundUserCommand.getAdviserUsername(), fundUserCommand.getInvestingMode()).persistenceId());

  }

  private void continueToUnsubscribeFromFund(AuthenticatedUserCommand command) {
    FundActionApproved fundUnsubscriptionApproved = (FundActionApproved) command;
    UnsubscribeToFund fundUserCommand = new UnsubscribeToFund(fundUnsubscriptionApproved.getAdviserUsername(),
        fundUnsubscriptionApproved.getFundName(), user.getUsername(), fundUnsubscriptionApproved.getInvestingMode());
    if (!user.isSubscribed(fundUserCommand.getAdviserUsername(), fundUserCommand.getFundName(),
        fundUserCommand.getInvestingMode())) {
      sender().tell(failed(user.getUsername(), "Not subscribed", FORBIDDEN), self());
      return;
    }

    if (!isExecutionWindowOpen() && properties.MARKET_OFFLINE_ENABLED()) {
      try {
        AddToPostMarketCommandQueue event = new AddToPostMarketCommandQueue(user.getUsername(),
            getUserFundCommandVariablesInMap(fundUserCommand), fundUserCommand.getClass().getSimpleName());
        user.validate(event);
        persist(event, evt -> {
          user.update(evt);
          sender().tell(new Done(user.getUsername(),
              "Your request to unsubscribe has been received. You will be unsubscribed from "
                  + evt.getVariables().get("fundName") + " on the next trading day."),
              self());
        });
        saveSnapshot();
      } catch (JsonProcessingException e) {
        sender().tell(new Failed(user.getUsername(), "Could not add your request in queue.", FAILED_DEPENDENCY),
            self());
      } catch (IllegalStateException e) {
        sender().tell(
            failed(user.getUsername(),
                "You cannot send more than one request in the same fund during non-market hours.", PRECONDITION_FAILED),
            self());
      }
      return;
    } else if (!isExecutionWindowOpen() && !properties.MARKET_OFFLINE_ENABLED()) {
      sender().tell(
          new Failed(user.getUsername(), "You cannot send this request during non-market hours.", PRECONDITION_FAILED),
          self());
      return;
    }

    UnSubscribeToFundWithUserEmail unSubscribeToFundWithUserEmail = new UnSubscribeToFundWithUserEmail(
        fundUserCommand.getAdviserUsername(), fundUserCommand.getFundName(), fundUserCommand.getInvestingMode(),
        user.getUsername(), user.getEmail(), fundUnsubscriptionApproved.getBasketOrderId(), fundUnsubscriptionApproved.getBrokerAuthInformation());

    forwardToChildren(context(), unSubscribeToFundWithUserEmail,
        "" + new UserFundCompositeKey(user.getUsername(), fundUserCommand.getFundName(),
            fundUserCommand.getAdviserUsername(), fundUserCommand.getInvestingMode()).persistenceId());

    requestMarginIn(fundUserCommand.getInvestingMode());

  }

  private void saveSnapshot() {
    long seqNum = lastSequenceNr();
    if (seqNum != 0 && seqNum % properties.SNAPSHOT_INTERVAL() == 0)
      saveSnapshot(user.copy());
  }

  private void handleGetAdvises(AuthenticatedUserCommand command) {
    GetAdvises subscribeToFundUser = (GetAdvises) command;
    selections.advisersRootActor().tell(
        new GetFundAdvises(subscribeToFundUser.getAdviserUsername(), subscribeToFundUser.getFundName()), sender());
  }

  private void handleGetWalletMoney(AuthenticatedUserCommand command) {
    GetWalletMoney walletMoneyRequest = (GetWalletMoney) command;
    requestMarginIn(walletMoneyRequest.getInvestingMode());
    if (walletMoneyRequest.getInvestingMode() == VIRTUAL)
      sender().tell(new Done(user.getUsername(), "" + round(user.getVirtualWalletAmount())), self());
    else if (walletMoneyRequest.getInvestingMode() == REAL && user.getRole() == UserType.TU)
      sender().tell(new Done(user.getUsername(), "" + user.getRealWalletAmount()), self());
    else if (walletMoneyRequest.getInvestingMode() == REAL && user.getRole() != UserType.TU)
      sender().tell(
          Failed.failed(user.getUsername(),
              "To get your real wallet money, you need to get your KYC done with your broker.", PRECONDITION_FAILED),
          self());
    else
      sender().tell(Failed.failed(user.getUsername(), "No option matched", PRECONDITION_FAILED), self());
    return;
  }

  private void handleDeviceToken(AuthenticatedUserCommand command) {

    PostDeviceToken getDeviceToken = (PostDeviceToken) command;
    DeviceTokenUpdated event = new DeviceTokenUpdated(user.getUsername(), getDeviceToken.getDeviceToken());

    persist(event, (DeviceTokenUpdated evt) -> {
      user.update(evt);
    });
    saveSnapshot();
    sender().tell(new Done(user.getUsername(), "Device token updated"), self());
  }

  private void handleGetApprovalPendingList(GetUserPendingRebalancingList getUserApprovalPendingList) {
    List<UserResponseParametersFromFund> userResponseParametersFromFunds;
    userResponseParametersFromFunds = getUserResponseParamtersFromFunds(getUserApprovalPendingList.getInvestingMode());
    if (userResponseParametersFromFunds != null) {
      ConsolidatedPendingRebalancingUserResponse accumulatedUserResponseParametersForUser = new ConsolidatedPendingRebalancingUserResponse(
          user.getUsername(), getUserApprovalPendingList.getInvestingMode(), "Success",
          userResponseParametersFromFunds);
      sender().tell(accumulatedUserResponseParametersForUser, self());
    }
  }

  private List<UserResponseParametersFromFund> getUserResponseParamtersFromFunds(String investingMode) {
    List<UserResponseParametersFromFund> userResponseParametersFromFunds = new ArrayList<UserResponseParametersFromFund>();
    try {
      for (FundWithAdviserAndInvestingMode fundWithAdviserAndInvestingMode : user.getAdvisorToFunds()) {
        if (fundWithAdviserAndInvestingMode.getInvestingMode().equals(investingMode)) {
          GetPendingUserFundApprovals getPendingUserFundApprovals = new GetPendingUserFundApprovals(user.getUsername(),
              fundWithAdviserAndInvestingMode.getAdviserUsername(), fundWithAdviserAndInvestingMode.getFund(),
              getInvestingMode(fundWithAdviserAndInvestingMode.getInvestingMode()));
          AllPendingUserFundApprovals allPendingUserFundApprovals = getPendingApprovalsFromUserFund(
              getPendingUserFundApprovals);
          if (!allPendingUserFundApprovals.getUserResponseParameters().isEmpty())
            userResponseParametersFromFunds.add(new UserResponseParametersFromFund(
                fundWithAdviserAndInvestingMode.getFund(), allPendingUserFundApprovals.getUserResponseParameters()));
        }
      }
    } catch (Exception e) {
      sender().tell(
          failed(user.getUsername(), "Internal Server error while getting pending approvals", EXPECTATION_FAILED),
          self());
      return null;
    }
    return userResponseParametersFromFunds;
  }

  private void handleGetRejectedAdviseListFromFund(GetUserFundRejectedAdvisesList command) {
    try {
      command.validate(user.getAdvisorToFunds());
    } catch (Exception e) {
      log.error("Exception " + e);
      sender().tell(failed(command.getUsername(), e.getMessage(), BAD_REQUEST), self());
      return;
    }
    GetRejectedAdviseFromUserFund getRejectedAdviseFromUserFund = new GetRejectedAdviseFromUserFund(user.getUsername(),
        command.getAdviserName(), command.getFundName(), InvestingMode.valueOf(command.getInvestingMode()));
    try {
      AllRejectedUserFundAdvises allRejectedUserFundAdvises = getRejectedAdvisesFromUserFund(
          getRejectedAdviseFromUserFund);
      sender().tell(allRejectedUserFundAdvises, self());
    } catch (Exception e) {
      sender().tell(
          failed(user.getUsername(), "Internal Server error while getting rejected advises", EXPECTATION_FAILED),
          self());
      e.printStackTrace();
    }
  }

  private void handleGetRejectedAdviseList(GetUserRejectedAdvisesList command) {
    try {
      command.validate();
    } catch (Exception e) {
      log.error("Exception " + e);
      sender().tell(failed(command.getUsername(), e.getMessage(), BAD_REQUEST), self());
      return;
    }
    List<UserResponseParametersFromFund> userResponseParametersFromFunds;
    userResponseParametersFromFunds = getUserRejectedAdviseParamtersFromFunds(command.getInvestingMode());
    if (userResponseParametersFromFunds != null) {
      ConsolidatedRejectedUserAdviseResponses accumulatedUserResponseParametersForUser = new ConsolidatedRejectedUserAdviseResponses(
          user.getUsername(), command.getInvestingMode(), "Success", userResponseParametersFromFunds);
      sender().tell(accumulatedUserResponseParametersForUser, self());
    }
  }

  private List<UserResponseParametersFromFund> getUserRejectedAdviseParamtersFromFunds(String investingMode) {
    List<UserResponseParametersFromFund> userResponseParametersFromFunds = new ArrayList<UserResponseParametersFromFund>();
    try {
      for (FundWithAdviserAndInvestingMode fundWithAdviserAndInvestingMode : user.getAdvisorToFunds()) {
        if (fundWithAdviserAndInvestingMode.getInvestingMode().equals(investingMode)) {
          GetRejectedAdviseFromUserFund getRejectedAdviseFromUserFund = new GetRejectedAdviseFromUserFund(
              user.getUsername(), fundWithAdviserAndInvestingMode.getAdviserUsername(),
              fundWithAdviserAndInvestingMode.getFund(),
              getInvestingMode(fundWithAdviserAndInvestingMode.getInvestingMode()));
          AllRejectedUserFundAdvises allRejectedUserFundAdvises = getRejectedAdvisesFromUserFund(
              getRejectedAdviseFromUserFund);
          if (!allRejectedUserFundAdvises.getUserResponseParameters().isEmpty())
            userResponseParametersFromFunds.add(new UserResponseParametersFromFund(
                fundWithAdviserAndInvestingMode.getFund(), allRejectedUserFundAdvises.getUserResponseParameters()));
        }
      }
    } catch (Exception e) {
      sender().tell(
          failed(user.getUsername(), "Internal Server error while getting rejected advises", EXPECTATION_FAILED),
          self());
      return null;
    }
    return userResponseParametersFromFunds;
  }

  private void clearAllSubscriptionFunds(String fundName) {
    if (fundNametoAdviserUserName.containsKey(fundName)) {
      fundNametoAdviserUserName.remove(fundName);
      fundNametoInvestingMode.remove(fundName);
    }
    fundNametoFundConstituentsMap.remove(fundName);
    fundNametoAmountForSubscription.remove(fundName);

  }

  private InvestingMode getInvestingMode(String investingMode) {
    if (investingMode.equals("VIRTUAL"))
      return VIRTUAL;
    return InvestingMode.REAL;
  }

  private void forwardCommandToAllChildren(Object obj) {
    List<FundWithAdviserAndInvestingMode> advisorToFunds = user.getAdvisorToFunds();
    List<String> childIds = new ArrayList<String>();
    for (FundWithAdviserAndInvestingMode fundWithAdviserAndInvestingMode : advisorToFunds) {
      String persistenceId = "" + new UserFundCompositeKey(user.getUsername(),
          fundWithAdviserAndInvestingMode.getFund(), fundWithAdviserAndInvestingMode.getAdviserUsername(),
          getInvestingMode(fundWithAdviserAndInvestingMode.getInvestingMode())).persistenceId();
      childIds.add(persistenceId);
    }
    forwardToChildren(context(), obj, childIds.toArray(new String[0]));
  }

  private String getExchangeFromWdId(String wdId) {
    return wdId.split("-")[1];
  }

  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(UserEvent.class, e -> {
      user.update(e);
      forwardToParentActor(e);
    }).match(SnapshotOffer.class, snapshotOffer -> {
      user = (User) snapshotOffer.snapshot();
      handleSnapshotReceive(snapshotOffer);
    }).match(RecoveryCompleted.class, e -> {
      requestMarginIn(REAL);
    }).match(UpdateFolioNumber.class, e -> {
      user.update(e);
    }).build();

  }
  
  @Override
  public Recovery recovery() {
    if (properties.disableRecoveryFromSnapshot()) {
      return Recovery.create(SnapshotSelectionCriteria.none());
    }
    return Recovery.create(SnapshotSelectionCriteria.Latest());
  }


  private void forwardToParentActor(UserEvent userEvent) {
    if (userEvent instanceof KycApprovedEvent) {
      KycApprovedEvent kycApprovedEvent = (KycApprovedEvent) userEvent;
      UpdateKYCDetailsInRootActor updateKYC = new UpdateKYCDetailsInRootActor(kycApprovedEvent.getUsername(),
          kycApprovedEvent.getClientCode(), kycApprovedEvent.getBrokerCompany());
      selections.userRootActor().tell(updateKYC, self());
    } else if (userEvent instanceof UpdateKYCDetails) {
      String clientCode = user.getClientCode();
      String brokerCompany = user.getBrokerCompany();
      UpdateKYCDetails kycUpdateEvent = (UpdateKYCDetails) userEvent;
      if (!(kycUpdateEvent.getClientCode().isEmpty()))
        clientCode = kycUpdateEvent.getClientCode();
      if (!(kycUpdateEvent.getBrokerCompany().isEmpty()))
        brokerCompany = kycUpdateEvent.getBrokerCompany();

      UpdateKYCDetailsInRootActor updateKYC = new UpdateKYCDetailsInRootActor(userEvent.getUsername(), clientCode,
          brokerCompany);
      selections.userRootActor().tell(updateKYC, self());
    }
  }

  @Override
  public String persistenceId() {
    return persistenceId;
  }

  @Autowired
  public void setSelections(@Qualifier("WDActors") WDActorSelections selections) {
    this.selections = selections;
  }

  @Autowired
  public void setNotificationFacade(NotificationFacade notificationFacade) {
    this.notificationFacade = notificationFacade;
  }

  @Autowired
  public void setOtpUtils(OTPUtils otpUtils) {
    this.otpUtils = otpUtils;
  }

  @Autowired
  public void setEmailUtility(SESEmailUtil emailUtility) {
    this.emailer = emailUtility;
  }

  @Autowired
  public void setProperties(WDProperties properties) {
    this.properties = properties;
  }

  @Override
  public String getName() {
    return persistenceId();
  }

  @Override
  public String getChildBeanName() {
    return "UserFundActor";
  }

  @Autowired
  public void setJournalProvider(JournalProvider journalProvider) {
    this.journalProvider = journalProvider;
  }

  @Autowired
  public void setWalletFactory(WalletFactory walletFactory) {
    this.walletFactory = walletFactory;
  }

  @Autowired
  public void setProjectionsClient(ProjectionsClient projectionsClient) {
    this.projectionsClient = projectionsClient;
  }

  @Autowired
  public void setUserPoolClient(@Qualifier("CognitoUserPoolForInvestor") AWSCognitoIdentityProvider userPoolClient) {
    this.userPoolClient = userPoolClient;
  }

  @Autowired
  public void setCredentialsProvider(@Qualifier("AWSCredentialsProvider") AWSCredentialsProvider credentialsProvider) {
    this.credentialsProvider = credentialsProvider;
  }


}