package com.wt.domain.write;

import static akka.actor.ActorRef.noSender;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.wt.domain.Outgoing;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.UserResponseParametersSerializer;
import com.wt.domain.write.commands.FetchAllUsersPendingFundRebalancingApprovals;
import com.wt.domain.write.commands.StopFetchingAllUsersPendingFundRebalancingApprovals;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.UntypedAbstractActor;

@Service("UserAnalyticsActor")
@Scope("prototype")
public class UserAnalyticsActor extends UntypedAbstractActor {

  @SuppressWarnings("unused")
  private String id;
  private WDActorSelections actorSelections;
  @Expose
  private List<UserPendingFundRebalancingApprovals> userPendingFundRebalancingApprovals;
  private ActorRef destinationActor;
  private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
      .registerTypeAdapter(UserResponseParameters.class, new UserResponseParametersSerializer()).create();

  @Autowired
  public void setActorSelections(WDActorSelections actorSelections) {
    this.actorSelections = actorSelections;
  }

  public UserAnalyticsActor(String id) {
    this.id = id;
  }

  @Override
  public void onReceive(Object msg) {

    if (msg instanceof FetchAllUsersPendingFundRebalancingApprovals) {
      destinationActor = ((FetchAllUsersPendingFundRebalancingApprovals) msg).getDestinationActor();
      userPendingFundRebalancingApprovals = new ArrayList<>();
      actorSelections.userRootActor().tell(msg, self());
    } else if (msg instanceof UserPendingFundRebalancingApprovals) {
      userPendingFundRebalancingApprovals.add((UserPendingFundRebalancingApprovals) msg);
      send(userPendingFundRebalancingApprovals);
    } else if (msg instanceof StopFetchingAllUsersPendingFundRebalancingApprovals) {
      self().tell(PoisonPill.getInstance(), noSender());

    }

  }

  private void send(List<UserPendingFundRebalancingApprovals> userPendingFundRebalancingApprovals) {
    String json = gson.toJson(userPendingFundRebalancingApprovals);
    Outgoing msg = new Outgoing(json);
    destinationActor.tell(msg, getSelf());
  }

}
