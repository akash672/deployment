package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper=true)
public class ActivationHashCodeGenerated extends UserEvent {
  @Getter
  private final String hashCode;

  public ActivationHashCodeGenerated(String username, String hashCode) {
    super(username);
    this.hashCode = hashCode;
  }

  private static final long serialVersionUID = 1L;

}
