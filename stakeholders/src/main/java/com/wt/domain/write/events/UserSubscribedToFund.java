package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=false)
public class UserSubscribedToFund extends UserEvent {

  private static final long serialVersionUID = 1L;
  private String fundName;
  private String adviserUsername;
  private double subscriptionAmount;
  private InvestingMode investingMode;

  public UserSubscribedToFund(String username, String adviserUsername, String fundName, InvestingMode investingMode, double subscriptionAmount) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.subscriptionAmount = subscriptionAmount;
    this.investingMode = investingMode;
  }

}
