package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviseOrderRejectedByBroker extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  private String localOrderId;
  private String rejectionMessage;
  
  public AdviseOrderRejectedByBroker(String email, String adviserUsername, String fundName, String adviseId, String localOrderId, String rejectionMessage, InvestingMode investingMode, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId,  issueType);
    this.localOrderId = localOrderId;
    this.rejectionMessage = rejectionMessage;
  }

}
