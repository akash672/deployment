package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.UserType;

import lombok.Getter;

@Getter
public class UserCurrentRoleStatus extends Done {

  private static final long serialVersionUID = 1L;
  private UserType role;
  private String isWealthCode;
  
  private int daysRemaining;

  @JsonCreator
  public UserCurrentRoleStatus(@JsonProperty("id") String id, @JsonProperty("message") String message,
      @JsonProperty("role") UserType userType, @JsonProperty("isWealthCode") String isWealthCode,
      @JsonProperty("daysRemaining") int daysRemaining) {
    super(id, message);
    this.daysRemaining = daysRemaining;
    this.role = userType;
    this.isWealthCode = isWealthCode;
  }
}
