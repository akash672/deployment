package com.wt.domain.write.events;

import java.util.List;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserPerformanceCalculatedWithDate extends UserPerformanceCalculated {

  private static final long serialVersionUID = 1L;
  @Getter
  private final long date;

  @Deprecated
  public UserPerformanceCalculatedWithDate(String email, String cognitoIdentityId, double currentValue,
      double returnPct, double realizedPnL, double unrealizedPnL, double pnl,
      List<UserFundPerformanceCalculated> allFundPerformance, long date, InvestingMode investingMode) {
    super(email, currentValue, returnPct, realizedPnL, unrealizedPnL, pnl, investingMode,
        allFundPerformance);
    this.date = date;
  }
  
  public UserPerformanceCalculatedWithDate(String username, double currentValue,
      double returnPct, double realizedPnL, double unrealizedPnL, double pnl,
      List<UserFundPerformanceCalculated> allFundPerformance, long date, InvestingMode investingMode) {
    super(username, currentValue, returnPct, realizedPnL, unrealizedPnL, pnl, investingMode,
        allFundPerformance);
    this.date = date;
  }

}
