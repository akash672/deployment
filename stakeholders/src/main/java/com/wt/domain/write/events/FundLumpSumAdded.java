package com.wt.domain.write.events;

import static akka.http.javadsl.model.StatusCodes.OK;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import akka.http.javadsl.model.StatusCode;
import lombok.Getter;
import lombok.ToString;

@ToString
public class FundLumpSumAdded extends UserFundEvent implements Result {
  private static final long serialVersionUID = 1L;
  private String id;
  private String message;
  @Getter private double lumpSumAdded;

  @JsonCreator
  public FundLumpSumAdded(@JsonProperty("id") String id, @JsonProperty("message") String message,
      @JsonProperty("fundName") String fundName, @JsonProperty("adviserName") String adviserName,
      @JsonProperty("investingMode") InvestingMode investingMode, @JsonProperty("lumpSumAdded") double lumpSumAdded) {
    super(id, adviserName, fundName, investingMode);
    this.lumpSumAdded = lumpSumAdded;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public StatusCode getStatusCode() {
    return OK;
  }

}