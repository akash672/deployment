package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper=true)
public class UserFundDividendReceived extends UserFundEvent {
  
  private static final long serialVersionUID = 1L;
  private double dividendAmount;
  
  public UserFundDividendReceived(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, double dividendAmount ) {
    super(username, adviserUsername, fundName, investingMode);
    this.dividendAmount = dividendAmount;
  }

}
