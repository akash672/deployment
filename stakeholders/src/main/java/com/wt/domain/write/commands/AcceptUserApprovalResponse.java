package com.wt.domain.write.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.domain.UserResponseParameters;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Log4j
@ToString(exclude="objectMapper")
public class AcceptUserApprovalResponse extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  
  @Getter private List<UserResponseParameters> userResponseParameters;
  @Getter private String basketOrderId;
  private ObjectMapper objectMapper = new ObjectMapper();
  @Getter @Setter private BrokerAuthInformation brokerAuthInformation;

  public AcceptUserApprovalResponse(String username, String allUserResponses, String basketOrderId, BrokerAuthInformation authenticationInfo) {
    super(username);
    this.basketOrderId = basketOrderId;
    this.brokerAuthInformation = authenticationInfo;
    TypeReference<List<UserResponseParameters>> mapType = new TypeReference<List<UserResponseParameters>>() {};
    try {
      userResponseParameters = objectMapper.readValue(allUserResponses, mapType);
    } catch (IOException e) {
      log.error("Could not map json due to " + e.getMessage());
    }
    
  }
  
  public static AcceptUserApprovalResponse fromPostMarketCommand(Map<String, String> value) {
    return new AcceptUserApprovalResponse(value.get("username"), value.get("userResponseParameters"), value.get("basketOrderId"), BrokerAuthInformation.withNoBrokerAuth());
  }
  
  public boolean isBaskerOrderIdBlank() {
    return (this.basketOrderId == null || this.basketOrderId.equals(""));
  }

  public boolean arePendingApprovalsUnique() {
    List<String> adviseIds = new ArrayList<String>();
    AtomicInteger atomicInteger = new AtomicInteger(0);
    this.userResponseParameters.stream().forEach(userResponse -> {
      if (adviseIds.contains(userResponse.getAdviseId()))
        atomicInteger.set(1);
      adviseIds.add(userResponse.getAdviseId());
    });
    if (atomicInteger.get() != 0)
      return false;
    else
      return true;
  }

  public boolean isMultiFundApprovalsReceived() {
    Set<String> funds = new HashSet<String>();
    this.userResponseParameters.stream().forEach(userResponse -> funds.add(userResponse.getFundName()));
    if (funds.size() != 1)
      return true;
    else
      return false;
  }
  
  public boolean isAuthticationInfoBlank() {
    return (this.brokerAuthInformation == null);
  }
}
