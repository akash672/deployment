package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.User;

import lombok.Getter;
import lombok.ToString;

@ToString(callSuper = true)
public class UpdateWealthCode extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private @Getter final String wealthCode;

  @JsonCreator
  public UpdateWealthCode(@JsonProperty("wealthCode") String wealthCode, @JsonProperty("username") String username) {
    super(username);
    this.wealthCode = wealthCode;
  }

  public void validate(User user) {
    if (wealthCode != null) {
      if (!wealthCode.matches("(\\d{4})"))
        throw new IllegalArgumentException("Invalid wealth code provided, Only 4 Digits are required");
      if (user.getPanCard() == null || user.getPanCard() == "") {
        throw new IllegalArgumentException("Pancard details required");
      } else if (user.getClientCode() == null || user.getClientCode() == "") {
        throw new IllegalArgumentException("Client Code required");
      } else if (user.getBrokerCompany() == null) {
        throw new IllegalArgumentException("Broker Company name required");
      }
    } else {
      throw new IllegalArgumentException("Wealth Code cannot be empty");
    }
  }
}