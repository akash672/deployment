package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetUserRoleStatus extends AuthenticatedUserCommand{

  private static final long serialVersionUID = 1L;
  private String phoneNumber;
  private String email;
  private String name;
  
  @JsonCreator
  public GetUserRoleStatus(@JsonProperty("username") String username, 
      @JsonProperty("phoneNumber") String phoneNumber, 
      @JsonProperty("email") String email, 
      @JsonProperty("name") String name) {
    super(username);
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.name = name;
  }

}
