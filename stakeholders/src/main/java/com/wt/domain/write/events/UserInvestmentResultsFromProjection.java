package com.wt.domain.write.events;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserInvestmentResultsFromProjection {
  List<Double> totalInvested;
  List<Double> totalWithdraw;
  List<Boolean> isSubscribed;
  List<String> fundName;
  List<String> adviserUsername;

  @JsonCreator
  public UserInvestmentResultsFromProjection(@JsonProperty("totalInvested") List<Double> totalInvested,
      @JsonProperty("totalWithdraw") List<Double> totalWithdraw,
      @JsonProperty("isSubscribed") List<Boolean> isSubscribed, @JsonProperty("fundName") List<String> fundname,
      @JsonProperty("adviserUsername") List<String> adviserUsername) {
    super();
    this.totalInvested = totalInvested;
    this.totalWithdraw = totalWithdraw;
    this.isSubscribed = isSubscribed;
    this.fundName = fundname;
    this.adviserUsername = adviserUsername;
  }
}
