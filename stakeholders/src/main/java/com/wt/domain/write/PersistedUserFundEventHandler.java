package com.wt.domain.write;

import com.wt.domain.User;
import com.wt.domain.write.events.UserHistoricalPerformanceCalculated;
import com.wt.domain.write.events.UserPerformanceCalculatedWithDate;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.japi.Procedure;

public class PersistedUserFundEventHandler implements Procedure<UserPerformanceCalculatedWithDate> {
  private int count = 0;
  private final ActorSelection originalSender;
  private final int expectedCount;
  private final User user;
  private final ActorRef self;

  public PersistedUserFundEventHandler(User user, ActorSelection originalSender, ActorRef self, int expectedCount) {
    this.user = user;
    this.originalSender = originalSender;
    this.expectedCount = expectedCount;
    this.self = self;
  }

  @Override
  public void apply(UserPerformanceCalculatedWithDate event) throws Exception {
    user.update(event);
    ++count;
    if (count == expectedCount) {
      originalSender.tell(new UserHistoricalPerformanceCalculated(user.getUsername(), "User Performance Calculated"),
          self);
    }
  }
}