package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@ToString
public class FundAdviceTradeExecuted extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private String adviseId;
  @Getter
  private double allocationValue;
  @Getter
  private double tradedValue;
  @Getter
  private UserAdviseState userAdviseState;

  public FundAdviceTradeExecuted(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, UserAdviseState userAdviseState, double allocationValue, double tradedValue) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.userAdviseState = userAdviseState;
    this.allocationValue = allocationValue;
    this.tradedValue = tradedValue;
  }

}
