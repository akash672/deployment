package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class CorporateActionSellDetails extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private String token;
  private double avgPrice;
  private int quantity;
  private double absoluteAllocation;
  private double allocationValue;
  
  public CorporateActionSellDetails(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String token, double avgPrice, int quantity, 
      double absoluteAllocation, double allocationValue) {
    super(email, adviserUsername, fundName, investingMode);
    this.token = token;
    this.avgPrice = avgPrice;
    this.quantity = quantity;
    this.absoluteAllocation = absoluteAllocation;
    this.allocationValue = allocationValue;
  }

}
