package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.List;

import com.wt.domain.CorporateEventType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class CorporateEventUpdateWithFundNames implements Serializable {


  private static final long serialVersionUID = 1L;
  private String wdId;
  private CorporateEventType eventType;
  private long eventDate;
  List<String> fundNames;
}
