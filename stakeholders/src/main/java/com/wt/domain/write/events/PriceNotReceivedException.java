package com.wt.domain.write.events;

public class PriceNotReceivedException extends Exception {
  private static final long serialVersionUID = 1L;
  
  public PriceNotReceivedException() {
  }
  
  public PriceNotReceivedException(String message) {
    super(message);
  }
}