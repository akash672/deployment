package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAcceptanceMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserAcceptanceModeUpdateReceived extends UserFundEvent {
  
  private static final long serialVersionUID = 1L;
  @Getter private UserAcceptanceMode userAcceptanceMode;

  public UserAcceptanceModeUpdateReceived(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, UserAcceptanceMode userAcceptanceMode) {
    super(email, adviserUsername, fundName, investingMode);
    this.userAcceptanceMode = userAcceptanceMode;
  }
  
  
}
