package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class MergerExitTradesExecuted extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  public double tradedValue;
  @Getter
  public UserAdviseState userAdviseState;

  public MergerExitTradesExecuted(String email, String adviserUsername, String fundName, InvestingMode investingMode,  String adviseId,
      UserAdviseState userAdviseState, double tradedValue, IssueType issueType ) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
    this.tradedValue = tradedValue;
  }
  
}
