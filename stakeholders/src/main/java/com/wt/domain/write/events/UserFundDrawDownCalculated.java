package com.wt.domain.write.events;

public class UserFundDrawDownCalculated extends Done {

  private static final long serialVersionUID = 1L;
  public UserFundDrawDownCalculated(String id, String message) {
    super(id, message);
  }

}
