package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.ToString;

@ToString
public class AllAdviseOrdersRejected extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  
  public AllAdviseOrdersRejected(String email, String adviserUsername, String fundName, InvestingMode investingMode) {
    super(email, adviserUsername, fundName, investingMode);
  }

}
