package com.wt.domain.write.events;

import com.wt.domain.AdviseType;
import com.wt.domain.InvestingMode;
import com.wt.domain.UserResponseType;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper=true)
public class UserResponseReceived extends UserFundEvent {
  private static final long serialVersionUID = 1L;

  private String adviseId;
  private UserResponseType userResponseType;
  private AdviseType adviseType;
  private String clientCode;
  private String brokerName;
  private String token;
  private String symbol;
  private String exchange;
  private double absoluteAllocation;
  private double allocationValue;
  private int numberOfShares;
  private long investmentDate;
  private String basketOrderCompositeId;
  private BrokerAuthInformation brokerAuthInformation;

  public UserResponseReceived(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, AdviseType adviseType, String clientCode, String brokerName, String exchange, String token, String symbol,
      double absoluteAllocation, double allocationValue, int numberOfShares, long investmentDate, UserResponseType response,
      String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.userResponseType = response;
    this.adviseType = adviseType;
    this.clientCode = clientCode;
    this.brokerName = brokerName;
    this.exchange = exchange;
    this.token = token;
    this.symbol = symbol;
    this.absoluteAllocation = absoluteAllocation;
    this.allocationValue = allocationValue;
    this.numberOfShares = numberOfShares;
    this.investmentDate = investmentDate;
    this.basketOrderCompositeId = basketOrderCompositeId;
    this.brokerAuthInformation = brokerAuthInformation;
  }

  public static class UserResponseReceivedBuilder {
    private String email;
    private String adviserUsername;
    private String fundName;
    private InvestingMode investingMode;
    private String adviseId;
    private AdviseType adviseType;
    private String clientCode;
    private String brokerName;
    private String exchange;
    private String token;
    private String symbol;
    private double absoluteAllocation;
    private double allocationValue;
    private int numberOfShares;
    private long investmentDate;
    private UserResponseType response;
    private String basketOrderCompositeId;
    private BrokerAuthInformation brokerAuthInformation;

    public UserResponseReceivedBuilder(String email, String adviserUsername, String fundName,
        InvestingMode investingMode, String adviseId, AdviseType adviseType, String clientCode, String brokerName,
        String exchange, String token, String symbol, double absoluteAllocation, long investmentDate, UserResponseType response) {
      super();
      this.email = email;
      this.adviserUsername = adviserUsername;
      this.fundName = fundName;
      this.investingMode = investingMode;
      this.adviseId = adviseId;
      this.adviseType = adviseType;
      this.clientCode = clientCode;
      this.brokerName = brokerName;
      this.exchange = exchange;
      this.token = token;
      this.symbol = symbol;
      this.absoluteAllocation = absoluteAllocation;
      this.investmentDate = investmentDate;
      this.response = response;
    }

    public UserResponseReceivedBuilder withAllocationValue(double allocationValue) {
      this.allocationValue = allocationValue;
      return this;
    }
    
    public UserResponseReceivedBuilder withNumberOfShares(int numberOfShares) {
      this.numberOfShares = numberOfShares;
      return this;
    }
    
    public UserResponseReceivedBuilder withBrokerAuthInformation(BrokerAuthInformation brokerAuthInformation) {
      this.brokerAuthInformation = brokerAuthInformation;
      return this;
    }
    
    public UserResponseReceivedBuilder withBasketOrderCompositeId(String basketOrderCompositeId) {
      this.basketOrderCompositeId = basketOrderCompositeId;
      return this;
    }

    public UserResponseReceived build() {
      return new UserResponseReceived(email, adviserUsername, fundName, investingMode, adviseId, adviseType, clientCode,
          brokerName, exchange, token, symbol, absoluteAllocation, allocationValue, numberOfShares, investmentDate, response,
          basketOrderCompositeId, brokerAuthInformation);
    }

  }
}
