package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UserFundSubscribedData {

  private String fundName;
  private String adviserUsername;
  private InvestingMode investingMode;
  private double subscriptionAmount;
  private double withdrawal;
  private String isFundSubscribed;

  @JsonCreator
  public UserFundSubscribedData(@JsonProperty("fundName") String fundName,
      @JsonProperty("adviserUserName") String adviserUsername,
      @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("subscriptionAmount") double subscriptionAmount, @JsonProperty("withdrawal") double withdrawal,
      @JsonProperty("isFundSubscribed") String isFundSubscribed) {
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.investingMode = investingMode;
    this.subscriptionAmount = subscriptionAmount;
    this.isFundSubscribed = isFundSubscribed;
    this.withdrawal = withdrawal;
  }
}
