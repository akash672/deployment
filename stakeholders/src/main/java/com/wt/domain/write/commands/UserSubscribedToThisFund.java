package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
public class UserSubscribedToThisFund extends UnAuthenticatedUserCommand  {

  private static final long serialVersionUID = 1L;
  @Deprecated
  private String userEmail;
  private String adviserName;
  private String fundName;
  private InvestingMode investingMode;
  
  @JsonCreator
  public UserSubscribedToThisFund(@JsonProperty("username") String username,
      @JsonProperty("adviserName") String adviserName, @JsonProperty("fundName") String fundName,
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super(username);
    this.userEmail = username;
    this.adviserName = adviserName;
    this.fundName = fundName;
    this.investingMode = investingMode;
  }
  
}