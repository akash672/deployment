package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

public class AdviseOrderTradeExecutedTimedOut extends UserAdviseEvent {
  
  private static final long serialVersionUID = 1L;
  
  public AdviseOrderTradeExecutedTimedOut(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
  }

}
