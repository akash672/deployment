package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

public class InsufficientCapital extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;

  public InsufficientCapital(String email, String adviserUsername, String fundName, String adviseId, InvestingMode investingMode, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
  }
}
