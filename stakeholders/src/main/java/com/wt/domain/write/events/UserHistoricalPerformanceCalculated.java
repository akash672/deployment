package com.wt.domain.write.events;

import akka.http.javadsl.model.StatusCode;
import akka.http.javadsl.model.StatusCodes;

public class UserHistoricalPerformanceCalculated extends Done {

  private static final long serialVersionUID = 1L;
  public UserHistoricalPerformanceCalculated(String id, String message) {
    super(id, message);
  }

  @Override
  public StatusCode getStatusCode() {
    return StatusCodes.OK;
  }

}
