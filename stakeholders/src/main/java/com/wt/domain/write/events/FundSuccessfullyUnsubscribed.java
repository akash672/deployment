package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = false)
public class FundSuccessfullyUnsubscribed extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private String identityId;
  @Getter
  private double currentCashComponent;

  @Deprecated
  public FundSuccessfullyUnsubscribed(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String identityId, double currentCashComponent) {
    this(username, adviserUsername, fundName, investingMode, currentCashComponent);
  }

  public FundSuccessfullyUnsubscribed(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, double currentCashComponent) {
    super(username, adviserUsername, fundName, investingMode);
    this.identityId = username;
    this.currentCashComponent = currentCashComponent;
  }
}
