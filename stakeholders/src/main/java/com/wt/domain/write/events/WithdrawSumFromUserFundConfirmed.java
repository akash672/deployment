package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class WithdrawSumFromUserFundConfirmed extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private String identityId;
  private long lastFundActionTimestamp;
  private double withdrawalAmount;
  private String basketOrderId;

  public WithdrawSumFromUserFundConfirmed(String id, String email, String adviserUsername, String fundName,
      InvestingMode investingMode, double withdrawalAmount, long lastFundActionTimestamp, String basketOrderId) {
    super(email, adviserUsername, fundName, investingMode);
    this.identityId = id;
    this.withdrawalAmount = withdrawalAmount;
    this.lastFundActionTimestamp = lastFundActionTimestamp;
    this.basketOrderId = basketOrderId;
  }

}
