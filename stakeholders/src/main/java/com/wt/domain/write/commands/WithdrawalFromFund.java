package com.wt.domain.write.commands;

import static java.lang.Double.parseDouble;
import static java.lang.String.valueOf;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class WithdrawalFromFund extends AuthenticatedUserCommand {
  
  private static final long serialVersionUID = 1L;
  protected String fundName;
  protected double withdrawalAmount;
  protected String adviserUsername;
  protected InvestingMode investingMode;
  @Setter private boolean isThisaPostMarketCommand = false;
  
  @JsonCreator
  public WithdrawalFromFund(@JsonProperty("userName") String userName, @JsonProperty("adviserUsername") String adviserUsername,
      @JsonProperty("fundName") String fundName, @JsonProperty("withdrawalAmount") double withdrawalAmount,
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super(userName);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.withdrawalAmount = withdrawalAmount;
    this.investingMode = investingMode;
  }
  
  public static WithdrawalFromFund fromPostMarketCommand(Map<String, String> value) {
    return new WithdrawalFromFund( value.get("username"), value.get("adviserUsername"), value.get("fundName"),
        parseDouble(valueOf(value.get("withdrawalAmount"))),
        InvestingMode.valueOf(value.get("investingMode")));
  }
}
