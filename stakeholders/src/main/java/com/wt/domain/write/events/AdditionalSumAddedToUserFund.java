package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.RiskLevel;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdditionalSumAddedToUserFund extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  private String identityId;
  private long lastFundActionTimestamp;
  private double lumpsumAmount;
  private RiskLevel riskProfile;
  private String inceptionDate;
  private String basketOrderId;

  @Deprecated
  public AdditionalSumAddedToUserFund(String identityId, String username, String adviserUsername, String fundName,
      InvestingMode investingMode, double lumpsumAmount, long lastFundActionTimestamp, RiskLevel riskProfile,
      String inceptionDate) {
    this(username, adviserUsername, fundName, investingMode, lumpsumAmount, lastFundActionTimestamp, riskProfile,
        inceptionDate, "");
  }

  public AdditionalSumAddedToUserFund(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, double lumpsumAmount, long lastFundActionTimestamp, RiskLevel riskProfile,
      String inceptionDate, String basketOrderId) {
    super(username, adviserUsername, fundName, investingMode);
    this.identityId = username;
    this.lumpsumAmount = lumpsumAmount;
    this.lastFundActionTimestamp = lastFundActionTimestamp;
    this.riskProfile = riskProfile;
    this.inceptionDate = inceptionDate;
    this.basketOrderId = basketOrderId;
  }
}