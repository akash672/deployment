package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@ToString
public class MergerSellTradeExecuted extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  @Getter private String adviseId;
  @Getter private double tradedValue;
  @Getter private UserAdviseState userAdviseState;
  public MergerSellTradeExecuted(String email, String adviserUsername, String fundName, InvestingMode investingMode,String adviseId, UserAdviseState userAdviseState, double tradedValue) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.userAdviseState = userAdviseState;
    this.tradedValue = tradedValue;
  }

}
