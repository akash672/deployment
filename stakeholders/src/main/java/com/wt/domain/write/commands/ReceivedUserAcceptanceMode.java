package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;
import com.wt.domain.UserAcceptanceMode;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class ReceivedUserAcceptanceMode {

  private String fundName;
  private String adviserName;
  private InvestingMode investingMode;
  private UserAcceptanceMode userAcceptanceMode;

  @JsonCreator
  public ReceivedUserAcceptanceMode(@JsonProperty("fundName") String fundName,
      @JsonProperty("adviserName") String adviserName, @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("userAcceptanceMode") UserAcceptanceMode userAcceptanceMode) {
    super();
    this.fundName = fundName;
    this.adviserName = adviserName;
    this.investingMode = investingMode;
    this.userAcceptanceMode = userAcceptanceMode;
  }

}
