package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.UserType;
import com.wt.domain.WDCredentials;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class InvestorCredentialsWithType extends Done {
  private static final long serialVersionUID = 1L;
  private WDCredentials credentials;
  private UserType role;

  @JsonCreator
  public InvestorCredentialsWithType(@JsonProperty("id") String id, @JsonProperty("credentials") WDCredentials credentials,
      @JsonProperty("role") UserType role,@JsonProperty("message") String message) {
    super(id, (message != null ? message : ""));
    this.credentials = credentials;
    this.role = role;
  }
}
