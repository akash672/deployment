package com.wt.domain.write.events;

import java.util.List;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserResponseParameters;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class UserFundAdviceRetryInitiated extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private List<UserResponseParameters> rejectedAdviseDetails;

  public UserFundAdviceRetryInitiated(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, List<UserResponseParameters> rejectedAdviseDetails) {
    super(username, adviserUsername, fundName, investingMode);
    this.rejectedAdviseDetails = rejectedAdviseDetails;
  }

}
