package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class GetClientCode implements Serializable {

  private static final long serialVersionUID = 1L;
  @Getter private String username;
}
