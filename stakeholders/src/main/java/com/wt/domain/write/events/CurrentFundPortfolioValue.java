package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;

@Getter
public class CurrentFundPortfolioValue extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private double currentPortFolioValue;
  
  public CurrentFundPortfolioValue(String email, String adviserUsername, String fundName, InvestingMode investingMode, double currentPortFolioValue) {
    super(email, adviserUsername, fundName, investingMode);
    this.currentPortFolioValue = currentPortFolioValue;
  }

}
