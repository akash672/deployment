package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString(callSuper = true)
public class UserAdviseAbsolutePerformanceWithDate extends UserAdviseAbsolutePerformanceCalculated {

  private static final long serialVersionUID = 1L;
  @Getter
  private final long date;

  public UserAdviseAbsolutePerformanceWithDate(String cognitoIdentityId, String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String wdId, String symbol, double realizedPnL, double unrealizedPnL, double pnl,
      double pctPnL, double currentValue, double nbShares, double actualAllocationValue, long date, IssueType issueType) {
    super(cognitoIdentityId, email, adviserUsername, fundName, investingMode, adviseId, wdId, symbol, realizedPnL, unrealizedPnL, pnl, pctPnL,
        currentValue, nbShares, actualAllocationValue, issueType);
    this.date = date;
  }

  public UserAdviseAbsolutePerformanceWithDate(String cognitoIdentityId, String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String wdId, double realizedPnL, double unrealizedPnL, double pnl,
      double pctPnL, double currentValue, double nbShares, double actualAllocationValue, long date, IssueType issueType) {
    super(cognitoIdentityId, email, adviserUsername, fundName, investingMode, adviseId, wdId, realizedPnL, unrealizedPnL, pnl, pctPnL,
        currentValue, nbShares, actualAllocationValue, issueType);
    this.date = date;
  }

}
