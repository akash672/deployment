package com.wt.domain.write.events;


import java.util.Map;

import com.wt.domain.InvestingMode;

import lombok.Getter;

@Getter
public class UpdatedWdIdsForAdvisesReceived extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private Map<String, String> symbolsToUpdatedWdIds;
  
  public UpdatedWdIdsForAdvisesReceived(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, Map<String, String> symbolsToUpdatedWdIds) {
    super(email, adviserUsername, fundName, investingMode);
    this.symbolsToUpdatedWdIds = symbolsToUpdatedWdIds;
  }

}
