package com.wt.domain.write.events;

import lombok.Getter;

public class UserOnboardingStatus extends Done {
  private static final long serialVersionUID = 1L;

  @Getter private boolean isOnboarded;
  
  public UserOnboardingStatus(String id, String message, boolean isOnboarded) {
    super(id, message);
    this.isOnboarded = isOnboarded;
  }

}
