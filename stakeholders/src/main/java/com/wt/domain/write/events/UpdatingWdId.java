package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdatingWdId extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  private String updatedWdId;

  public UpdatingWdId(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, IssueType issueType, String updatedWdId) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.updatedWdId = updatedWdId;
  }

}