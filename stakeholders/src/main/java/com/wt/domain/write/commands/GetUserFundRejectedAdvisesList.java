package com.wt.domain.write.commands;

import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;

import java.util.List;

import javax.ws.rs.BadRequestException;

import com.wt.domain.FundWithAdviserAndInvestingMode;

import lombok.Getter;

@Getter
public class GetUserFundRejectedAdvisesList extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private String adviserName;
  private String fundName;
  private String investingMode;

  public GetUserFundRejectedAdvisesList(String username, String adviserName, String fundName,
      String investingMode) {
    super(username);
    this.adviserName = adviserName;
    this.fundName = fundName;
    this.investingMode = investingMode;
  }

  public void validate(List<FundWithAdviserAndInvestingMode> advisorToFunds) {
    if (this.adviserName.isEmpty() || !isValidAdviserName(advisorToFunds))
      throw new BadRequestException("adviserName is not valid");
    if (this.fundName.isEmpty() || !isValidFundName(advisorToFunds))
      throw new BadRequestException("fundName is not valid");
    if (this.investingMode.isEmpty() || !isValidInvestingMode())
      throw new BadRequestException("investingMode is not valid");
  }

  private boolean isValidInvestingMode() {
    return this.investingMode.equals(REAL.getInvestingMode())
        || this.investingMode.equals(VIRTUAL.getInvestingMode());
  }

  private boolean isValidAdviserName(List<FundWithAdviserAndInvestingMode> advisorToFunds) {
    for (FundWithAdviserAndInvestingMode fund : advisorToFunds) {
      if (fund.getAdviserUsername().equals(this.adviserName))
        return true;
    }
    return false;
  }

  private boolean isValidFundName(List<FundWithAdviserAndInvestingMode> advisorToFunds) {
    for (FundWithAdviserAndInvestingMode fund : advisorToFunds) {
      if (fund.getFund().equals(this.fundName))
        return true;
    }
    return false;
  }
}
