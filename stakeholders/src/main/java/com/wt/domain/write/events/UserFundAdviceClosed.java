package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class UserFundAdviceClosed extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  private String adviseId;
  private double absoluteAllocation;
  private long exitTime;
  private PriceWithPaf exitPrice;

  public UserFundAdviceClosed(String email, String adviserUsername, String fundName, InvestingMode investingMode, PriceWithPaf exitPrice) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = "";
    this.absoluteAllocation = 0.;
    this.exitTime = 0L;
    this.exitPrice = exitPrice;
  }

  public UserFundAdviceClosed(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, double absoluteAllocation, long exitTime, PriceWithPaf exitPrice ) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.absoluteAllocation = absoluteAllocation;
    this.exitTime = exitTime;
    this.exitPrice = exitPrice;
  }

}
