package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@ToString
public class RootUserRegistered implements Serializable {
  private static final long serialVersionUID = 1L;

  @Getter private String identityID;
  @Getter private String userEmail;
  @Getter private boolean isOnboarded;
  
  public RootUserRegistered(String identityID, String userEmail, boolean isOnBoarded) {
    this.identityID = identityID;
    this.userEmail = userEmail;
    this.isOnboarded = isOnBoarded; 
  }
  
}

