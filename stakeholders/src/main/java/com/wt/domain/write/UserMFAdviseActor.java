package com.wt.domain.write;

import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static com.wt.domain.Purpose.ONBOARDING;
import static com.wt.domain.UserAdviseState.AdditionalLumpSumToMF;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderRejected;
import static com.wt.domain.UserAdviseState.PartialExitAdviseOrderReceivedWithDealer;
import static com.wt.domain.UserAdviseState.PartialExitReceived;
import static com.wt.domain.UserAdviseState.UserMFAdviseIssued;
import static com.wt.domain.UserAdviseState.UserMFCompleteCloseAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.UserMFCompleteCloseAdviseOrderPlacedWithDealer;
import static com.wt.domain.UserAdviseState.UserMFCompleteCloseAdviseReceived;
import static com.wt.domain.UserAdviseState.UserMFOpenAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.UserMFOpenAdviseOrderPlacedWithDealer;
import static com.wt.domain.UserAdviseState.UserMFPartialExitAdviseOrderExecuted;
import static com.wt.domain.write.commands.CreateMutualFundOrder.orderPlacementForMutualFundSell;
import static com.wt.domain.write.events.InstrumentByWdIdSent.noInstrument;
import static com.wt.domain.write.events.IssueType.MutualFund;
import static com.wt.domain.write.events.UserFundAdditionalSumAdvice.fundLumpSumAdviceWithUndefinedExitParams;
import static com.wt.utils.CommonConstants.EntryOrderIdPrefix;
import static com.wt.utils.CommonConstants.ExitOrderIdPrefix;
import static com.wt.utils.CommonConstants.HYPHEN;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static java.util.UUID.randomUUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.MutualFundInstrument;
import com.wt.domain.Order;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.OrderSegment;
import com.wt.domain.OrderType;
import com.wt.domain.Purpose;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserAdviseState;
import com.wt.domain.UserMFAdvise;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CalculateUserAdviseComposition;
import com.wt.domain.write.commands.CreateMutualFundOrder;
import com.wt.domain.write.commands.GetFolioNumber;
import com.wt.domain.write.commands.GetInstrumentByWdId;
import com.wt.domain.write.commands.UpdateFolioNumber;
import com.wt.domain.write.commands.UpdateUserFundCompositionList;
import com.wt.domain.write.events.AdviseOrderReceivedWithBroker;
import com.wt.domain.write.events.AdviseOrderRejected;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.AdviseTradesExecuted;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.ExchangeOrderUpdateAcknowledged;
import com.wt.domain.write.events.ExchangeOrderUpdateEvent;
import com.wt.domain.write.events.ExitAdviseReceived;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.GenerateClientOrderId;
import com.wt.domain.write.events.InstrumentByWdIdSent;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.OrderReceivedByDealer;
import com.wt.domain.write.events.OrderRejectedByBSEStar;
import com.wt.domain.write.events.RequestedFolioNumber;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserAdviseTransactionReceived;
import com.wt.domain.write.events.UserFundAdditionalSumAdvice;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.domain.write.events.UserFundAdviseIssuedInsufficientCapital;
import com.wt.domain.write.events.UserOpenAdviseOrderRejected.UserOpenAdviseOrderRejectedBuilder;
import com.wt.utils.akka.WDActorSelections;

import lombok.extern.log4j.Log4j;

@Service("UserMFAdviseActor")
@Scope("prototype")
@Log4j
public class UserMFAdviseActor extends UserAdviseActor<UserAdviseState, UserMFAdvise, UserAdviseEvent> {
  private String persistenceId;
  private WDActorSelections wdActorSelections;
  private Purpose investingPurpose;

  @Autowired
  public void setWdActorSelections(WDActorSelections wdActorSelections) {
    this.wdActorSelections = wdActorSelections;
  }

  public UserMFAdviseActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;

    startWith(UserMFAdviseIssued, new UserMFAdvise());

    when(UserMFAdviseIssued, 
        matchEvent(UserFundAdviceIssuedWithUserEmail.class, 
            (adviseIssued, userAdvise) -> {
              UserFundAdviceIssuedWithUserEmail userFundAdviseIssued = new UserFundAdviceIssuedWithUserEmail(
                  adviseIssued.getUsername(),
                  adviseIssued.getAdviserUsername(),
                  adviseIssued.getFundName(),
                  adviseIssued.getInvestingMode(),
                  adviseIssued.getAdviseId(),
                  adviseIssued.getToken(),
                  adviseIssued.getSymbol(),
                  "",
                  adviseIssued.getAbsoluteAllocation(),
                  adviseIssued.getMonetaryAllocationValue(),
                  adviseIssued.getIssueTime(),
                  adviseIssued.getEntryPrice(),
                  adviseIssued.getEntryTime(),
                  adviseIssued.getExitPrice(),
                  adviseIssued.getExitTime(),
                  adviseIssued.isUserBeingOnboarded(),
                  adviseIssued.getBrokerName(),
                  adviseIssued.getClientCode(),
                  getIssueType(),
                  adviseIssued.isSpecialAdvise(),
                  adviseIssued.getBasketOrderCompositeId(),
                  BrokerAuthInformation.withNoBrokerAuth());
              return stay()
                  .applying(userFundAdviseIssued)
                  .andThen(exec(advise -> {
                    if (adviseIssued.isUserBeingOnboarded()) {
                      investingPurpose = ONBOARDING;
                    } else {
                      investingPurpose = MUTUALFUNDSUBSCRIPTION;
                    }
                    generateClientOrderId(userAdvise);
      }));
    })
        .event(GenerateClientOrderId.class, 
        (generateClientOrderId, userAdvise) -> {
                return stay()
                    .applying(generateClientOrderId)
                    .andThen(exec(advise -> {
                  RequestedFolioNumber requestedFolioNumber = getRequestedFolioNumber(advise.getAmcCode(), advise.getUserAdviseCompositeKey());
                  InstrumentByWdIdSent instrumentByWdIdSent = getInstrumentFromUniverseRootActor(
                      advise.getToken());
                  MutualFundInstrument mutualFundInstrument = (MutualFundInstrument)(instrumentByWdIdSent.getInstrument());
                  wdActorSelections.mforderRootActor()
                      .tell(new CreateMutualFundOrder(advise.getUserAdviseCompositeKey().persistenceId(),
                          userAdvise.getLatestIssuedAdviseOrder().get().getOrderId(),
                          advise.getBrokerName(),
                          investingPurpose,
                          userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                          userAdvise.getUserAdviseCompositeKey().getUsername(),
                          userAdvise.getClientCode(),
                          new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                              userAdvise.getToken().split("-")[0],
                              userAdvise.getLatestIssuedAdviseOrder().get().getSide(),
                              0,
                              OrderType.MARKET,
                              OrderSegment.MutualFund)
                          .withSymbol(mutualFundInstrument.getSymbol())
                          .withbasketOrderCompositeId(userAdvise.getBasketOrderCompositeId())
                          .build(),
                          userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                          advise.getIssueAdviseAllocationValue(),
                          mutualFundInstrument.getSchemeCode(),
                          mutualFundInstrument.getSchemeName(),
                          mutualFundInstrument.getSchemeCode(),
                          requestedFolioNumber.getFolioNumber(),
                          generateClientOrderId.getTxCode(),
                          generateClientOrderId.getDpcTxn(),
                          generateClientOrderId.getAllRedeem(),
                          generateClientOrderId.getMinRedeem(),
                          generateClientOrderId.getDpcFlag(),
                          BrokerAuthInformation.withNoBrokerAuth()), self());
                }));
    })
        .event(OrderReceivedByDealer.class, 
                (orderReceivedByDealer, mfAdvise) -> {
                    return goTo(UserMFOpenAdviseOrderPlacedWithDealer)
                          .applying(new AdviseOrderReceivedWithBroker(mfAdvise.getUserAdviseCompositeKey().getUsername(),
                              mfAdvise.getUserAdviseCompositeKey().getFund(),
                              mfAdvise.getUserAdviseCompositeKey().getAdviser(),
                              mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                              mfAdvise.getAdviseId(),
                              stateName(),
                              orderReceivedByDealer.getWdOrderId(),
                              orderReceivedByDealer.getDealerOrderId(),
                              getIssueType()))
                          .andThen(exec(advise -> {
                            wdActorSelections.userRootActor()
                                .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                    "Received exchange update " + orderReceivedByDealer, orderReceivedByDealer,
                                    MutualFund.getIssueType()), self());
                              }));
                })
        .event(OrderRejectedByBSEStar.class, 
            (orderRejected, userAdvise) -> {
              return goTo(OpenAdviseOrderRejected)
                  .applying(new AdviseOrderRejected(
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      getIssueType())).
                   andThen(exec(advise -> {
                            wdActorSelections.userRootActor()
                                .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                    "Received exchange update " + orderRejected, orderRejected,
                                    MutualFund.getIssueType()), self());
                     
                     wdActorSelections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                         advise.getUserAdviseCompositeKey().getUsername(),
                         advise.getUserAdviseCompositeKey().getAdviser(),
                         advise.getUserAdviseCompositeKey().getFund(),
                         advise.getAdviseId(), 
                         advise.getIssueAdviseAllocationValue(),
                         advise.getUserAdviseCompositeKey().getInvestingMode(),
                         getIssueType()), self());
                   })); 
            }));

    when(UserMFOpenAdviseOrderPlacedWithDealer, 
        matchEvent(TradeExecuted.class, 
            (mutualFundTradeExecuted, mfAdvise) -> {
              return !mfAdvise.hasIssueAdviceTradeBeenProcessed(mutualFundTradeExecuted.getTradeOrderId()) ? true : false;
            }, 
            (mutualFundTradeExecuted, mfAdvise) -> {
              AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted. AdviseOrderTradeExecutedBuilder(mfAdvise.getUserAdviseCompositeKey().getUsername(),
                  mfAdvise.getUserAdviseCompositeKey().getAdviser(), mfAdvise.getUserAdviseCompositeKey().getFund(),
                  mfAdvise.getUserAdviseCompositeKey().getInvestingMode(), mfAdvise.getAdviseId(), stateName(),
                  mutualFundTradeExecuted.getOrderSide(), mutualFundTradeExecuted.getTradeOrderId(),
                  mutualFundTradeExecuted.getExchangeOrderId(), mfAdvise.getUserAdviseCompositeKey().getUsername(),
                  mutualFundTradeExecuted.getQuantity(), mutualFundTradeExecuted.getPrice(), mfAdvise.getToken(), getIssueType()).withFolioNumber(mutualFundTradeExecuted.getFolioNumber()).build();
      return goTo(UserMFOpenAdviseOrderExecuted)
          .applying(adviseOrderTradeExecuted)
              .andThen(exec(advise -> {
                    wdActorSelections.userRootActor()
                        .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                            "Received exchange update " + mutualFundTradeExecuted, mutualFundTradeExecuted,
                            MutualFund.getIssueType()), self());
                    
                    wdActorSelections.userRootActor()
                        .tell(new UpdateFolioNumber(mfAdvise.getUserAdviseCompositeKey().getUsername(),
                            mfAdvise.getAmcCode(), mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                            mutualFundTradeExecuted.getFolioNumber()), self());
                    wdActorSelections.userRootActor().tell(new AdviseTradesExecuted(
                    mfAdvise.getUserAdviseCompositeKey().getUsername(), 
                    mfAdvise.getUserAdviseCompositeKey().getAdviser(), 
                    mfAdvise.getUserAdviseCompositeKey().getFund(),
                    mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getAdviseId(),
                    stateName(),
                    advise.getIssueAdviseAllocationValue(),
                    round(mutualFundTradeExecuted.getQuantity() * mutualFundTradeExecuted.getPrice().getPrice().getPrice()),
                    mutualFundTradeExecuted.getQuantity(),
                    advise.getSymbol(),
                    mutualFundTradeExecuted.getOrderSide(),
                    advise.averageEntryPrice(),
                    getIssueType()), self());
                }));
        
    })
        .event(OrderRejectedByBSEStar.class, 
            (orderRejected, userAdvise) -> {
              return goTo(OpenAdviseOrderRejected)
                  .applying(new AdviseOrderRejected(
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      getIssueType())).
                   andThen(exec(advise -> {
                            wdActorSelections.userRootActor()
                                .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                    "Received exchange update " + orderRejected, orderRejected,
                                    MutualFund.getIssueType()), self());
                     
                     wdActorSelections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                         advise.getUserAdviseCompositeKey().getUsername(),
                         advise.getUserAdviseCompositeKey().getAdviser(),
                         advise.getUserAdviseCompositeKey().getFund(),
                         advise.getAdviseId(), 
                         advise.getIssueAdviseAllocationValue(),
                         advise.getUserAdviseCompositeKey().getInvestingMode(),
                         getIssueType()), self());
                   })); 
            }));

    when(UserMFOpenAdviseOrderExecuted,
        matchEvent(UserAdviseTransactionReceived.class,  
            (userAdviseTransactionReceived, mfAdvise) -> {
          return (!isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0)) ? true : false;
            },  (userAdviseTransactionReceived, mfAdvise) -> {
          String localOrderId = ExitOrderIdPrefix +  HYPHEN + mfAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN
              + mfAdvise.getAdviseId() + HYPHEN + randomUUID().toString();
          ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
              mfAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdviseTransactionReceived.getAdviserUsername(),
              userAdviseTransactionReceived.getFundName(),
              mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              mfAdvise.getToken(),
              userAdviseTransactionReceived.getAdviseId(),
              userAdviseTransactionReceived.getAbsoluteAllocation(),
              userAdviseTransactionReceived.getExitDate(),
              userAdviseTransactionReceived.getExitPrice(),
              localOrderId,
              getIssueType(),
              userAdviseTransactionReceived.getPurpose(),
              ExitAdviseReceived.atMarketPrice(),
              ExitAdviseReceived.isNotASpecialSellAdvise(),
              ExitAdviseReceived.quantityNotYetSet(),
              userAdviseTransactionReceived.getBasketOrderCompositeId(),
              userAdviseTransactionReceived.getBrokerAuthInformation());
          return goTo(PartialExitReceived)
              .applying(exitAdviseReceived)
              .andThen(exec(advise -> {
            InstrumentByWdIdSent instrumentByWdIdSent = getInstrumentFromUniverseRootActor(
                advise.getToken());
            MutualFundInstrument mutualFundInstrument = (MutualFundInstrument)(instrumentByWdIdSent.getInstrument());
            Order closeAdviseOrder = advise.getLatestSellOrder().get();
            wdActorSelections.mforderRootActor()
                .tell(orderPlacementForMutualFundSell(advise.getUserAdviseCompositeKey().persistenceId(),
                    closeAdviseOrder.getOrderId(),
                    advise.getBrokerName(),
                    MUTUALFUNDSUBSCRIPTION,
                    advise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getUserAdviseCompositeKey().getUsername(),
                    advise.getClientCode(),
                    new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                        mutualFundInstrument.getToken(),
                        closeAdviseOrder.getSide(),
                        closeAdviseOrder.getQuantity(),
                        OrderType.MARKET,
                        OrderSegment.MutualFund)
                    .withSymbol(mutualFundInstrument.getSymbol())
                    .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                    .build(),
                    0.,
                    mutualFundInstrument.getSchemeCode(),
                    mutualFundInstrument.getSchemeName(),
                    mutualFundInstrument.getSchemeCode(),
                    mfAdvise.getFolioNumber(),
                    BrokerAuthInformation.withNoBrokerAuth()), self());
          }));
        }).event(UserAdviseTransactionReceived.class, 
            (userAdviseTransactionReceived, userAdvise) -> {
          return (isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0)) ? true : false;
      },
          (userAdviseTransactionReceived, mfAdvise) -> 
        {
          String localOrderId = ExitOrderIdPrefix +  HYPHEN + mfAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN
              + mfAdvise.getAdviseId() + HYPHEN + randomUUID().toString();
          ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
              mfAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdviseTransactionReceived.getAdviserUsername(),
              userAdviseTransactionReceived.getFundName(),
              mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              mfAdvise.getToken(),
              userAdviseTransactionReceived.getAdviseId(),
              userAdviseTransactionReceived.getAbsoluteAllocation(),
              userAdviseTransactionReceived.getExitDate(),
              userAdviseTransactionReceived.getExitPrice(),
              localOrderId,
              getIssueType(),
              userAdviseTransactionReceived.getPurpose(),
              ExitAdviseReceived.atMarketPrice(),
              ExitAdviseReceived.isNotASpecialSellAdvise(),
              ExitAdviseReceived.quantityNotYetSet(),
              userAdviseTransactionReceived.getBasketOrderCompositeId(),
              userAdviseTransactionReceived.getBrokerAuthInformation());
          return goTo(UserMFCompleteCloseAdviseReceived).applying(exitAdviseReceived).andThen(exec(advise -> {
            InstrumentByWdIdSent instrumentByWdIdSent = getInstrumentFromUniverseRootActor(
                advise.getToken());
            MutualFundInstrument mutualFundInstrument = (MutualFundInstrument)(instrumentByWdIdSent.getInstrument());
            Order closeAdviseOrder = advise.getLatestSellOrder().get();
            wdActorSelections.mforderRootActor()
                .tell(orderPlacementForMutualFundSell(advise.getUserAdviseCompositeKey().persistenceId(),
                    closeAdviseOrder.getOrderId(),
                    advise.getBrokerName(),
                    MUTUALFUNDSUBSCRIPTION,
                    advise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getUserAdviseCompositeKey().getUsername(),
                    mfAdvise.getClientCode(),
                    new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                        mutualFundInstrument.getToken(),
                        closeAdviseOrder.getSide(),
                        closeAdviseOrder.getQuantity(),
                        OrderType.MARKET,
                        OrderSegment.MutualFund)
                    .withSymbol(mutualFundInstrument.getSymbol())
                    .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                    .build(),
                    0.,
                    mutualFundInstrument.getSchemeCode(),
                    mutualFundInstrument.getSchemeName(),
                    mutualFundInstrument.getSchemeCode(),
                    mfAdvise.getFolioNumber(),
                    BrokerAuthInformation.withNoBrokerAuth()), self());
          }));
        }).
        event(UserFundAdditionalSumAdvice.class, 
            (adviseIssued, userAdvise) -> {
              UserFundAdditionalSumAdvice userFundAdviseIssued = fundLumpSumAdviceWithUndefinedExitParams(
                  adviseIssued.getUsername(), 
                  adviseIssued.getAdviserUsername(), 
                  adviseIssued.getFundName(),
                  adviseIssued.getInvestingMode(), 
                  adviseIssued.getAdviseId(), 
                  adviseIssued.getToken(),
                  adviseIssued.getSymbol(),
                  "",
                  adviseIssued.getAbsoluteAllocation(), 
                  adviseIssued.getMonetaryAllocationValue(), 
                  adviseIssued.getEntryPrice(),
                  adviseIssued.isUserBeingOnboarded(),
                  adviseIssued.getBrokerName(),
                  adviseIssued.getClientCode(),
                  adviseIssued.getIssueType(),
                  adviseIssued.isSpecialAdvise(),
                  adviseIssued.getBasketOrderId(),
                  BrokerAuthInformation.withNoBrokerAuth());
              return goTo(AdditionalLumpSumToMF)
                  .applying(userFundAdviseIssued)
                  .andThen(exec(advise -> {
                    investingPurpose = MUTUALFUNDSUBSCRIPTION;
                    generateClientOrderId(userAdvise);
          }));
        }));
    
        when(AdditionalLumpSumToMF,
            matchEvent(GenerateClientOrderId.class, 
                    (generateClientOrderId, userAdvise) -> {
                            return stay()
                                .applying(generateClientOrderId)
                                .andThen(exec(advise -> {
                              RequestedFolioNumber requestedFolioNumber = getRequestedFolioNumber(advise.getAmcCode(), advise.getUserAdviseCompositeKey());
                              InstrumentByWdIdSent instrumentByWdIdSent = getInstrumentFromUniverseRootActor(
                                  advise.getToken());
                              MutualFundInstrument mutualFundInstrument = (MutualFundInstrument)(instrumentByWdIdSent.getInstrument());
                              wdActorSelections.mforderRootActor()
                                  .tell(new CreateMutualFundOrder(advise.getUserAdviseCompositeKey().persistenceId(),
                                      userAdvise.getLatestIssuedAdviseOrder().get().getOrderId(),
                                      advise.getBrokerName(),
                                      investingPurpose,
                                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                                      userAdvise.getClientCode(),
                                      new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                                          userAdvise.getToken().split("-")[0],
                                          userAdvise.getLatestIssuedAdviseOrder().get().getSide(),
                                          0,
                                          OrderType.MARKET,
                                          OrderSegment.MutualFund)
                                      .withSymbol(mutualFundInstrument.getSymbol())
                                      .withbasketOrderCompositeId(userAdvise.getBasketOrderCompositeId())
                                      .build(),
                                      userAdvise.getLatestIssuedAdviseOrder().get().getExchange(),
                                      advise.getIssueAdviseAllocationValue(),
                                      mutualFundInstrument.getSchemeCode(),
                                      mutualFundInstrument.getSchemeName(),
                                      mutualFundInstrument.getSchemeCode(),
                                      requestedFolioNumber.getFolioNumber(),
                                      generateClientOrderId.getTxCode(),
                                      generateClientOrderId.getDpcTxn(),
                                      generateClientOrderId.getAllRedeem(),
                                      generateClientOrderId.getMinRedeem(),
                                      generateClientOrderId.getDpcFlag(),
                                      BrokerAuthInformation.withNoBrokerAuth()), self());
                            }));
                    }).
                    event(OrderReceivedByDealer.class, 
                            (orderReceivedByDealer, mfAdvise) -> {
                                return goTo(UserMFOpenAdviseOrderPlacedWithDealer)
                                      .applying(new AdviseOrderReceivedWithBroker(mfAdvise.getUserAdviseCompositeKey().getUsername(),
                                          mfAdvise.getUserAdviseCompositeKey().getFund(),
                                          mfAdvise.getUserAdviseCompositeKey().getAdviser(),
                                          mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                                          mfAdvise.getAdviseId(),
                                          stateName(),
                                          orderReceivedByDealer.getWdOrderId(),
                                          orderReceivedByDealer.getDealerOrderId(),
                                          getIssueType()))
                                      .andThen(exec(advise -> {
                                        wdActorSelections.userRootActor()
                                            .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                                "Received exchange update " + orderReceivedByDealer, orderReceivedByDealer,
                                                MutualFund.getIssueType()), self());
                                          }));
                            })
                    .event(OrderRejectedByBSEStar.class, 
                        (orderRejected, userAdvise) -> {
                          return goTo(OpenAdviseOrderRejected)
                              .applying(new AdviseOrderRejected(
                                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                                  userAdvise.getUserAdviseCompositeKey().getFund(),
                                  userAdvise.getAdviseId(),
                                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                                  getIssueType())).
                               andThen(exec(advise -> {
                                        wdActorSelections.userRootActor()
                                            .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                                "Received exchange update " + orderRejected, orderRejected,
                                                MutualFund.getIssueType()), self());
                                 
                                 wdActorSelections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                                     advise.getUserAdviseCompositeKey().getUsername(),
                                     advise.getUserAdviseCompositeKey().getAdviser(),
                                     advise.getUserAdviseCompositeKey().getFund(),
                                     advise.getAdviseId(), 
                                     advise.getIssueAdviseAllocationValue(),
                                     advise.getUserAdviseCompositeKey().getInvestingMode(),
                                     getIssueType()), self());
                               })); 
                    }));
    
    when(PartialExitReceived, 
        matchEvent(OrderReceivedByDealer.class, (orderReceivedByDealer, mfAdvise) -> {
          return goTo(PartialExitAdviseOrderReceivedWithDealer)
              .applying(new AdviseOrderReceivedWithBroker(mfAdvise.getUserAdviseCompositeKey().getUsername(),
                  mfAdvise.getUserAdviseCompositeKey().getFund(),
                  mfAdvise.getUserAdviseCompositeKey().getAdviser(),
                  mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  mfAdvise.getAdviseId(),
                  stateName(),
                  orderReceivedByDealer.getWdOrderId(),
                  orderReceivedByDealer.getDealerOrderId(),
                  getIssueType()))
              .andThen(exec(advise -> {
                wdActorSelections.userRootActor()
                    .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                        "Received exchange update " + orderReceivedByDealer, orderReceivedByDealer,
                        MutualFund.getIssueType()), self());
                    }));
        }));
    
    when(PartialExitAdviseOrderReceivedWithDealer,
        matchEvent(TradeExecuted.class, 
            (mutualFundTradeExecuted, mfAdvise) -> {
              return !mfAdvise.hasCloseAdviceTradeBeenProcessed(mutualFundTradeExecuted.getTradeOrderId()) ? true : false;
            }, 
            (mutualFundTradeExecuted, mfAdvise) -> {
              AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted.AdviseOrderTradeExecutedBuilder(
                  mfAdvise.getUserAdviseCompositeKey().getUsername(),
                  mfAdvise.getUserAdviseCompositeKey().getAdviser(),
                  mfAdvise.getUserAdviseCompositeKey().getFund(),
                  mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  mfAdvise.getAdviseId(),
                  stateName(),
                  mutualFundTradeExecuted.getOrderSide(),
                  mutualFundTradeExecuted.getTradeOrderId(),
                  mutualFundTradeExecuted.getExchangeOrderId(),
                  mfAdvise.getUserAdviseCompositeKey().getUsername(),
                  mutualFundTradeExecuted.getQuantity(),
                  mutualFundTradeExecuted.getPrice(),
                  mfAdvise.getToken(),
                  getIssueType()).withFolioNumber(mutualFundTradeExecuted.getFolioNumber()).build();
              return goTo(UserMFPartialExitAdviseOrderExecuted).applying(adviseOrderTradeExecuted)
                  .andThen(exec(advise -> {
                    wdActorSelections.userRootActor()
                        .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                            "Received exchange update " + mutualFundTradeExecuted, mutualFundTradeExecuted,
                            MutualFund.getIssueType()), self());
                    
                    wdActorSelections.userRootActor()
                        .tell(new AdviseTradesExecuted(advise.getUserAdviseCompositeKey().getUsername(),
                            advise.getUserAdviseCompositeKey().getAdviser(),
                            advise.getUserAdviseCompositeKey().getFund(),
                            advise.getUserAdviseCompositeKey().getInvestingMode(),
                            advise.getAdviseId(),
                            stateName(),
                            round(advise.currentCloseAdviseTradedQuantity(mutualFundTradeExecuted) * advise.currentCloseAdviseAverageExitPrice(mutualFundTradeExecuted)),
                            0d,
                            mutualFundTradeExecuted.getQuantity(),
                            advise.getSymbol(),
                            mutualFundTradeExecuted.getOrderSide(),
                            mutualFundTradeExecuted.getPrice().getPrice().getPrice(),
                            getIssueType()), self());
                  }));
    }).event(OrderRejectedByBSEStar.class, 
        (orderRejected, userAdvise) -> {
          return goTo(OpenAdviseOrderRejected)
              .applying(new AdviseOrderRejected(
                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getAdviseId(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  getIssueType())).
               andThen(exec(advise -> {
                        wdActorSelections.userRootActor()
                            .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                "Received exchange update " + orderRejected, orderRejected, MutualFund.getIssueType()),
                                self());
                 
                 wdActorSelections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                     advise.getUserAdviseCompositeKey().getUsername(),
                     advise.getUserAdviseCompositeKey().getAdviser(),
                     advise.getUserAdviseCompositeKey().getFund(),
                     advise.getAdviseId(), 
                     advise.getIssueAdviseAllocationValue(),
                     advise.getUserAdviseCompositeKey().getInvestingMode(),
                     getIssueType()), self());
               })); 
        }));
    
    when(UserMFPartialExitAdviseOrderExecuted,
        matchEvent(UserAdviseTransactionReceived.class, 
            (userAdviseTransactionReceived, mfAdvise) -> {
          return (!isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0)) ? true : false;
            },  (userAdviseTransactionReceived, mfAdvise) -> {
          String localOrderId = ExitOrderIdPrefix +  HYPHEN + mfAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN
              + mfAdvise.getAdviseId() + HYPHEN + randomUUID().toString();
          ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
              mfAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdviseTransactionReceived.getAdviserUsername(),
              userAdviseTransactionReceived.getFundName(),
              mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              mfAdvise.getToken(),
              userAdviseTransactionReceived.getAdviseId(),
              userAdviseTransactionReceived.getAbsoluteAllocation(),
              userAdviseTransactionReceived.getExitDate(),
              userAdviseTransactionReceived.getExitPrice(),
              localOrderId,
              getIssueType(),
              userAdviseTransactionReceived.getPurpose(),
              ExitAdviseReceived.atMarketPrice(),
              ExitAdviseReceived.isNotASpecialSellAdvise(),
              ExitAdviseReceived.quantityNotYetSet(),
              userAdviseTransactionReceived.getBasketOrderCompositeId(),
              userAdviseTransactionReceived.getBrokerAuthInformation());
          return goTo(PartialExitReceived)
              .applying(exitAdviseReceived)
              .andThen(exec(advise -> {
            InstrumentByWdIdSent instrumentByWdIdSent = getInstrumentFromUniverseRootActor(
                advise.getToken());
            MutualFundInstrument mutualFundInstrument = (MutualFundInstrument)(instrumentByWdIdSent.getInstrument());
            Order closeAdviseOrder = advise.getLatestSellOrder().get();
            wdActorSelections.mforderRootActor()
                .tell( orderPlacementForMutualFundSell(advise.getUserAdviseCompositeKey().persistenceId(),
                    closeAdviseOrder.getOrderId(),
                    advise.getBrokerName(),
                    MUTUALFUNDSUBSCRIPTION,
                    advise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getUserAdviseCompositeKey().getUsername(),
                    advise.getClientCode(),
                    new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                        mutualFundInstrument.getToken(),
                        closeAdviseOrder.getSide(),
                        closeAdviseOrder.getQuantity(),
                        OrderType.MARKET,
                        OrderSegment.MutualFund)
                    .withSymbol(mutualFundInstrument.getSymbol())
                    .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                    .build(),
                    0.,
                    mutualFundInstrument.getSchemeCode(),
                    mutualFundInstrument.getSchemeName(),
                    mutualFundInstrument.getSchemeCode(),
                    mfAdvise.getFolioNumber(),
                    BrokerAuthInformation.withNoBrokerAuth()), self());
          }));
        }).event(UserAdviseTransactionReceived.class, 
            (userAdviseTransactionReceived, userAdvise) -> {
          return (isZero(userAdviseTransactionReceived.getAbsoluteAllocation() - 100.0)) ? true : false;
      },
          (userAdviseTransactionReceived, mfAdvise) -> 
        {
          String localOrderId = ExitOrderIdPrefix +  HYPHEN + mfAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN
              + mfAdvise.getAdviseId() + HYPHEN + randomUUID().toString();
          ExitAdviseReceived exitAdviseReceived = new ExitAdviseReceived(
              mfAdvise.getUserAdviseCompositeKey().getUsername(),
              userAdviseTransactionReceived.getAdviserUsername(),
              userAdviseTransactionReceived.getFundName(),
              mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              mfAdvise.getToken(),
              userAdviseTransactionReceived.getAdviseId(),
              userAdviseTransactionReceived.getAbsoluteAllocation(),
              userAdviseTransactionReceived.getExitDate(),
              userAdviseTransactionReceived.getExitPrice(),
              localOrderId,
              getIssueType(),
              userAdviseTransactionReceived.getPurpose(),
              ExitAdviseReceived.atMarketPrice(),
              ExitAdviseReceived.isNotASpecialSellAdvise(),
              ExitAdviseReceived.quantityNotYetSet(),
              userAdviseTransactionReceived.getBasketOrderCompositeId(),
              userAdviseTransactionReceived.getBrokerAuthInformation());
          return goTo(UserMFCompleteCloseAdviseReceived).applying(exitAdviseReceived).andThen(exec(advise -> {
            InstrumentByWdIdSent instrumentByWdIdSent = getInstrumentFromUniverseRootActor(
                advise.getToken());
            MutualFundInstrument mutualFundInstrument = (MutualFundInstrument)(instrumentByWdIdSent.getInstrument());
            Order closeAdviseOrder = advise.getLatestSellOrder().get();
            wdActorSelections.mforderRootActor()
                .tell( orderPlacementForMutualFundSell(advise.getUserAdviseCompositeKey().persistenceId(),
                    closeAdviseOrder.getOrderId(),
                    advise.getBrokerName(),
                    MUTUALFUNDSUBSCRIPTION,
                    advise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getUserAdviseCompositeKey().getUsername(),
                    advise.getClientCode(),
                    new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
                        mutualFundInstrument.getToken(),
                        closeAdviseOrder.getSide(),
                        closeAdviseOrder.getQuantity(),
                        OrderType.MARKET,
                        OrderSegment.MutualFund)
                    .withSymbol(mutualFundInstrument.getSymbol())
                    .withbasketOrderCompositeId(advise.getBasketOrderCompositeId())
                    .build(),
                    0.,
                    mutualFundInstrument.getSchemeCode(),
                    mutualFundInstrument.getSchemeName(),
                    mutualFundInstrument.getSchemeCode(),
                    mfAdvise.getFolioNumber(),
                    BrokerAuthInformation.withNoBrokerAuth()), self());
          }));
        }).
        event(UserFundAdditionalSumAdvice.class, 
            (adviseIssued, userAdvise) -> {
              UserFundAdditionalSumAdvice userFundAdviseIssued = fundLumpSumAdviceWithUndefinedExitParams(
                  adviseIssued.getUsername(), 
                  adviseIssued.getAdviserUsername(), 
                  adviseIssued.getFundName(),
                  adviseIssued.getInvestingMode(), 
                  adviseIssued.getAdviseId(), 
                  adviseIssued.getToken(),
                  adviseIssued.getSymbol(),
                  "",
                  adviseIssued.getAbsoluteAllocation(), 
                  adviseIssued.getMonetaryAllocationValue(), 
                  adviseIssued.getEntryPrice(),
                  adviseIssued.isUserBeingOnboarded(),
                  adviseIssued.getBrokerName(),
                  adviseIssued.getClientCode(),
                  adviseIssued.getIssueType(),
                  adviseIssued.isSpecialAdvise(),
                  adviseIssued.getBasketOrderId(),
                  BrokerAuthInformation.withNoBrokerAuth());
              return goTo(AdditionalLumpSumToMF)
                  .applying(userFundAdviseIssued)
                  .andThen(exec(advise -> {
                    investingPurpose = MUTUALFUNDSUBSCRIPTION;
                    generateClientOrderId(userAdvise);
          }));
        }));

    
    when(UserMFCompleteCloseAdviseReceived,
        matchEvent(OrderReceivedByDealer.class, (orderReceivedByDealer, mfAdvise) -> {
          return goTo(UserMFCompleteCloseAdviseOrderPlacedWithDealer)
              .applying(new AdviseOrderReceivedWithBroker(mfAdvise.getUserAdviseCompositeKey().getUsername(),
                  mfAdvise.getUserAdviseCompositeKey().getFund(),
                  mfAdvise.getUserAdviseCompositeKey().getAdviser(),
                  mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  mfAdvise.getAdviseId(),
                  stateName(),
                  orderReceivedByDealer.getWdOrderId(),
                  orderReceivedByDealer.getDealerOrderId(),
                  getIssueType()))
              .andThen(exec(advise -> {
                wdActorSelections.userRootActor()
                    .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                        "Received exchange update " + orderReceivedByDealer, orderReceivedByDealer,
                        MutualFund.getIssueType()), self());
                    }));
        }));

    when(UserMFCompleteCloseAdviseOrderPlacedWithDealer,
        matchEvent(TradeExecuted.class, 
            (mutualFundTradeExecuted, mfAdvise) -> {
              return !mfAdvise.hasCloseAdviceTradeBeenProcessed(mutualFundTradeExecuted.getTradeOrderId()) ? true : false;
            }, 
            (mutualFundTradeExecuted, mfAdvise) -> {
          AdviseOrderTradeExecuted adviseOrderTradeExecuted = new AdviseOrderTradeExecuted.AdviseOrderTradeExecutedBuilder(
              mfAdvise.getUserAdviseCompositeKey().getUsername(),
              mfAdvise.getUserAdviseCompositeKey().getAdviser(),
              mfAdvise.getUserAdviseCompositeKey().getFund(),
              mfAdvise.getUserAdviseCompositeKey().getInvestingMode(),
              mfAdvise.getAdviseId(),
              stateName(),
              mutualFundTradeExecuted.getOrderSide(),
              mutualFundTradeExecuted.getTradeOrderId(),
              mutualFundTradeExecuted.getExchangeOrderId(),
              mfAdvise.getUserAdviseCompositeKey().getUsername(),
              mutualFundTradeExecuted.getQuantity(),
              mutualFundTradeExecuted.getPrice(),
              mfAdvise.getToken(),
              getIssueType()).withFolioNumber(mutualFundTradeExecuted.getFolioNumber()).build();
          return goTo(UserMFCompleteCloseAdviseOrderExecuted).applying(adviseOrderTradeExecuted)
              .andThen(exec(advise -> {
                    wdActorSelections.userRootActor()
                        .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                            "Received exchange update " + mutualFundTradeExecuted, mutualFundTradeExecuted,
                            MutualFund.getIssueType()), self());
                
                wdActorSelections.userRootActor()
                    .tell(new AdviseTradesExecuted(advise.getUserAdviseCompositeKey().getUsername(),
                        advise.getUserAdviseCompositeKey().getAdviser(),
                        advise.getUserAdviseCompositeKey().getFund(),
                        advise.getUserAdviseCompositeKey().getInvestingMode(),
                        advise.getAdviseId(),
                        stateName(),
                        round(advise.currentCloseAdviseTradedQuantity(mutualFundTradeExecuted) * advise.currentCloseAdviseAverageExitPrice(mutualFundTradeExecuted)),
                        0d,
                        mutualFundTradeExecuted.getQuantity(),
                        advise.getSymbol(),
                        mutualFundTradeExecuted.getOrderSide(),
                        mutualFundTradeExecuted.getPrice().getPrice().getPrice(),
                        getIssueType()), self());
                self().tell(new CurrentUserAdviseFlushedDueToExit(advise.getUserAdviseCompositeKey().getUsername(),
                    advise.getUserAdviseCompositeKey().getAdviser(),
                    advise.getUserAdviseCompositeKey().getFund(),
                    advise.getUserAdviseCompositeKey().getInvestingMode(),
                    advise.getUserAdviseCompositeKey().getAdviseId(),
                    getIssueType()), self());
              }));
        }).event(OrderRejectedByBSEStar.class, 
            (orderRejected, userAdvise) -> {
              return goTo(OpenAdviseOrderRejected)
                  .applying(new AdviseOrderRejected(
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                      getIssueType())).
                   andThen(exec(advise -> {
                        wdActorSelections.userRootActor()
                            .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                                "Received exchange update " + orderRejected, orderRejected, MutualFund.getIssueType()),
                                self());
                     
                     wdActorSelections.userRootActor().tell(new UserOpenAdviseOrderRejectedBuilder(
                         advise.getUserAdviseCompositeKey().getUsername(),
                         advise.getUserAdviseCompositeKey().getAdviser(),
                         advise.getUserAdviseCompositeKey().getFund(),
                         advise.getAdviseId(), 
                         advise.getIssueAdviseAllocationValue(),
                         advise.getUserAdviseCompositeKey().getInvestingMode(),
                         getIssueType()), self());
                   })); 
            }));

    when(UserMFCompleteCloseAdviseOrderExecuted,
        matchEvent(CurrentUserAdviseFlushedDueToExit.class, 
            (currentAdviseFlushed, userAdvise) -> {
          return goTo(UserMFAdviseIssued).applying(currentAdviseFlushed).using(new UserMFAdvise());
        }));

    whenUnhandled(
        matchEvent(ExchangeOrderUpdateEvent.class,
            (exchangeOrderUpdateEvent, userAdvise) -> {
          log.warn("Received unhandled event " + exchangeOrderUpdateEvent.toString() + " in state " + stateName() + " for token " + userAdvise.getToken() + " with adviseId " + userAdvise.getAdviseId() + " for user " + userAdvise.getUserAdviseCompositeKey().getUsername());
              wdActorSelections.userRootActor()
                  .tell(new ExchangeOrderUpdateAcknowledged(persistenceId,
                      "Received exchange update " + exchangeOrderUpdateEvent, exchangeOrderUpdateEvent,
                      MutualFund.getIssueType()), self());
          return stay();
        }).
        event(CalculateUserAdviseComposition.class,
            (calculateUserAdviseComposition, userAdvise) -> {
              wdActorSelections.userRootActor().tell(new UpdateUserFundCompositionList(userAdvise.getUserAdviseCompositeKey(),
              userAdvise.doubleQuantityYetToBeExited()), self());
          return stay();
        }).
        anyEvent( 
        (event, userAdvise) -> {
          log.warn("Received unhandled event " + event.toString() + " in state " + stateName() + " for token " + userAdvise.getToken() + " with adviseId " + userAdvise.getAdviseId() + " for user " + userAdvise.getUserAdviseCompositeKey().getUsername());
          return stay();
    }));
    
    onTermination(
        matchStop(Failure.class, 
            (reason, userAdviseState, userAdvise) -> {
              if (reason.cause() instanceof UserFundAdviseIssuedInsufficientCapital) {
                UserFundAdviseIssuedInsufficientCapital userFundAdviseIssuedInsufficientCapital = (UserFundAdviseIssuedInsufficientCapital) reason.cause();
                log.error("Shutting down " + userAdvise.getAdviseId() + " for user " + userAdvise.getUserAdviseCompositeKey().getUsername() + " due to insufficient allocation value of " + userFundAdviseIssuedInsufficientCapital.getAdjustAllocation());
              }
    }).
        stop(Shutdown(), 
            (userAdviseState, userAdvise) -> {
              log.warn("Shutting down " + self() + " in state " + stateName());
    })
        .stop(Normal(), 
        (reason, userAdvise) -> {
          log.error("Normal shutdown " + userAdvise.getAdviseId() + " for user " + userAdvise.getUserAdviseCompositeKey().getUsername());
        }));
  }

  private RequestedFolioNumber getRequestedFolioNumber(String amcCode, UserAdviseCompositeKey userAdviseCompositeKey) {
    RequestedFolioNumber requestedfolioNumber = new RequestedFolioNumber("0", amcCode);
    try {
      Object result = askAndWaitForResult(wdActorSelections.userRootActor(),
          new GetFolioNumber(userAdviseCompositeKey.getUsername(), amcCode, userAdviseCompositeKey.getInvestingMode()));
      requestedfolioNumber = (RequestedFolioNumber) result;
    } catch (Exception e) {
      log.error("Error while getting instrument by wdId ", e);
      return requestedfolioNumber;
    }
    return requestedfolioNumber;
}

  private IssueType getIssueType() {
    return IssueType.MutualFund;
  }

  private void generateClientOrderId(UserMFAdvise userAdvise) {
    String localOrderId = EntryOrderIdPrefix + HYPHEN + userAdvise.getUserAdviseCompositeKey().getUsername() + HYPHEN
        + userAdvise.getAdviseId() + HYPHEN + randomUUID().toString();
    InstrumentByWdIdSent instrumentByWdIdSent = getInstrumentFromUniverseRootActor(userAdvise.getToken());
    MutualFundInstrument mutualFundInstrument = (MutualFundInstrument) (instrumentByWdIdSent.getInstrument());
    self().tell(new GenerateClientOrderId(userAdvise.getUserAdviseCompositeKey().getUsername(),
        userAdvise.getUserAdviseCompositeKey().getFund(), userAdvise.getUserAdviseCompositeKey().getAdviser(),
        userAdvise.getUserAdviseCompositeKey().getInvestingMode(), userAdvise.getAdviseId(), localOrderId,
        mutualFundInstrument.getAmcCode(), stateName(), getIssueType(), "NEW", "Y", "1", "N", "Y"), self());
  }

  private InstrumentByWdIdSent getInstrumentFromUniverseRootActor(String token) {
    InstrumentByWdIdSent instrumentByWdIdSent = noInstrument();
    try {
      Object result = askAndWaitForResult(wdActorSelections.universeRootActor(), new GetInstrumentByWdId(token));

      if (result instanceof Failed) {
        log.error("Error while getting instrument by wdId " + result);
        return null;
      }
      instrumentByWdIdSent = (InstrumentByWdIdSent) result;
    } catch (Exception e) {
      log.error("Error while getting instrument by wdId ", e);
      return null;
    }
    return instrumentByWdIdSent;
  }

  @Override
  public UserMFAdvise applyEvent(UserAdviseEvent userAdviseEvent, UserMFAdvise userAdvise) {
    userAdvise.update(userAdviseEvent);
    return userAdvise;
  }

  @Override
  public String persistenceId() {
    return persistenceId;
  }

  @Override
  public Class<UserAdviseEvent> domainEventClass() {
    return UserAdviseEvent.class;
  }

}