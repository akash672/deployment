package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class CorporateEventOrderExecuted implements Serializable {

  private static final long serialVersionUID = 1L;
  private String ticker;
  

}
