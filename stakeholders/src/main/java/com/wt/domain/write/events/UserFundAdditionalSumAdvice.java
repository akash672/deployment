package com.wt.domain.write.events;

import static com.wt.domain.BrokerName.NOBROKER;
import static com.wt.domain.PriceWithPaf.noPrice;
import static java.lang.System.currentTimeMillis;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.utils.DateUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserFundAdditionalSumAdvice extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  @SuppressWarnings("unused")
  private String identityId;
  private String token;
  private String symbol;
  private String description;
  private double absoluteAllocation;
  private double monetaryAllocationValue;
  private long issueTime;
  private PriceWithPaf entryPrice;
  private long entryTime;
  private PriceWithPaf exitPrice;
  private long exitTime;
  private boolean isUserBeingOnboarded;
  private BrokerName brokerName;
  private String clientCode;
  private boolean isSpecialAdvise;
  private String basketOrderId;
  private BrokerAuthInformation brokerAuthInformation;
  
  public String getIdentityId() {
    return super.getUsername();
  }

  @Deprecated
  public UserFundAdditionalSumAdvice(String identityId, String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, IssueType issueType, String token, String symbol,
      String description, double absoluteAllocation, double monetaryAllocationValue, long issueTime,
      PriceWithPaf entryPrice, long entryTime, PriceWithPaf exitPrice, long exitTime, boolean isUserBeingOnboarded,
      BrokerName brokerName, String clientCode, boolean isSpecialAdvise,
      BrokerAuthInformation brokerAuthInformation) {
    this(username, adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description,
        absoluteAllocation, monetaryAllocationValue, issueTime, entryPrice, entryTime, exitPrice, exitTime,
        isUserBeingOnboarded, brokerName, clientCode, isSpecialAdvise, "",brokerAuthInformation);
  }

  public UserFundAdditionalSumAdvice(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, IssueType issueType, String token, String symbol,
      String description, double absoluteAllocation, double monetaryAllocationValue, long issueTime,
      PriceWithPaf entryPrice, long entryTime, PriceWithPaf exitPrice, long exitTime, boolean isUserBeingOnboarded,
      BrokerName brokerName, String clientCode, boolean isSpecialAdvise, String basketOrderId, BrokerAuthInformation brokerAuthInformation) {
    super(username, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.identityId = username;
    this.token = token;
    this.symbol = symbol;
    this.description = description;
    this.absoluteAllocation = absoluteAllocation;
    this.monetaryAllocationValue = monetaryAllocationValue;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.isUserBeingOnboarded = isUserBeingOnboarded;
    this.brokerName = brokerName;
    this.clientCode = clientCode;
    this.isSpecialAdvise = isSpecialAdvise;
    this.basketOrderId = basketOrderId;
    this.brokerAuthInformation = brokerAuthInformation;
  }

  @Deprecated
  public UserFundAdditionalSumAdvice(String identityId, String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, IssueType issueType, String token, String description,
      double absoluteAllocation, double monetaryAllocationValue, long issueTime, PriceWithPaf entryPrice,
      long entryTime, PriceWithPaf exitPrice, long exitTime) {
    this(username, adviserUsername, fundName, investingMode, adviseId, issueType, token, description,
        absoluteAllocation, monetaryAllocationValue, issueTime, entryPrice, entryTime, exitPrice, exitTime);
  }

  @Deprecated
  public UserFundAdditionalSumAdvice(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, IssueType issueType, String token, String description,
      double absoluteAllocation, double monetaryAllocationValue, long issueTime, PriceWithPaf entryPrice,
      long entryTime, PriceWithPaf exitPrice, long exitTime) {
    super(username, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.identityId = username;
    this.token = token;
    this.symbol = token;
    this.description = description;
    this.absoluteAllocation = absoluteAllocation;
    this.monetaryAllocationValue = monetaryAllocationValue;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.isUserBeingOnboarded = false;
    this.brokerName = NOBROKER;
    this.clientCode = "None";
    this.isSpecialAdvise = false;
  }

  @Deprecated
  public static UserFundAdditionalSumAdvice fundLumpSumAdviceWithUndefinedExitParams(String identityId, String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      PriceWithPaf entryPrice, boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType,
      boolean isSpecialAdvise, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(identityId, username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, currentTimeMillis(), entryPrice, currentTimeMillis(), noPrice(token), 0L,
        isUserOnboarded, brokerName, clientCode, isSpecialAdvise, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  public static UserFundAdditionalSumAdvice fundLumpSumAdviceWithUndefinedExitParams(String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      PriceWithPaf entryPrice, boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType,
      boolean isSpecialAdvise, String basketOrderId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, currentTimeMillis(), entryPrice, currentTimeMillis(), noPrice(token), 0L,
        isUserOnboarded, brokerName, clientCode, isSpecialAdvise, basketOrderId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  @Deprecated
  public static UserFundAdditionalSumAdvice fundLumpSumAdviceWithUndefinedExitParamsAndMarketPrice(String identityId,
      String username, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId,
      String token, String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType, boolean isSpecialAdvise,
      BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(identityId, username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, currentTimeMillis(), noPrice(token), currentTimeMillis(), noPrice(token), 0L,
        isUserOnboarded, brokerName, clientCode, isSpecialAdvise, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  public static UserFundAdditionalSumAdvice fundLumpSumAdviceWithUndefinedExitParamsAndMarketPrice(String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType, boolean isSpecialAdvise,
      String basketOrderId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, currentTimeMillis(), noPrice(token), currentTimeMillis(), noPrice(token), 0L,
        isUserOnboarded, brokerName, clientCode, isSpecialAdvise, basketOrderId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  @Deprecated
  public static UserFundAdditionalSumAdvice nonSpecialFundAdviceForANonOnboardedUser(String identityId, String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      BrokerName brokerName, String clientCode, IssueType issueType, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(identityId, username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, false, brokerName, clientCode, false , brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  public static UserFundAdditionalSumAdvice nonSpecialFundAdviceForANonOnboardedUser(String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      BrokerName brokerName, String clientCode, IssueType issueType, String basketOrderId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, false, brokerName, clientCode, false, basketOrderId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  @Deprecated
  public static UserFundAdditionalSumAdvice nonSpecialUserFundAdvice(String identityId, String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      boolean isUserBeingOnboarded, BrokerName brokerName, String clientCode, IssueType issueType, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(identityId, username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserBeingOnboarded, brokerName, clientCode, false, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  public static UserFundAdditionalSumAdvice nonSpecialUserFundAdvice(String username, String adviserUsername,
      String fundName, InvestingMode investingMode, String adviseId, String token, String symbol, String description,
      double absoluteAllocation, double monetaryAllocationValue, boolean isUserBeingOnboarded, BrokerName brokerName,
      String clientCode, IssueType issueType, String basketOrderId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = new UserFundAdditionalSumAdvice(username,
        adviserUsername, fundName, investingMode, adviseId, issueType, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserBeingOnboarded, brokerName, clientCode, false, basketOrderId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

}