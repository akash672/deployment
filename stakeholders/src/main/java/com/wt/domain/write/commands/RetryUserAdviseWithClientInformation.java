package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.UserFund;
import com.wt.domain.UserResponseParameters;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class RetryUserAdviseWithClientInformation implements Serializable {

  private static final long serialVersionUID = 1L;
  private String adviserUsername;
  private String fundName;
  private List<UserResponseParameters> rejectedAdviseDetails;
  private String clientCode;
  private BrokerName brokerName;
  private InvestingMode investingMode;
  private BrokerAuthInformation brokerAuthInformation;

  public RetryUserAdviseWithClientInformation(String adviserUsername, String fundName, List<UserResponseParameters> adviseIds,
      String clientCode, BrokerName brokerName, InvestingMode investingMode, BrokerAuthInformation brokerAuthInformation) {
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.rejectedAdviseDetails = adviseIds;
    this.clientCode = clientCode;
    this.brokerName = brokerName;
    this.investingMode = investingMode;
    this.brokerAuthInformation = brokerAuthInformation;
  }

  public boolean isValidated(UserFund userFund) {
    List<String> rejectedAdviseIds = new ArrayList<String>();
    userFund.getActiveRejectedBuySideAdvises().stream().forEach(userAdvise -> rejectedAdviseIds.add(userAdvise.getAdviseId()));
    for (UserResponseParameters rejectedUserAdvise : rejectedAdviseDetails) {
      if (!rejectedAdviseIds.contains(rejectedUserAdvise.getAdviseId()))
        return false;
    }
    return true;
  }
}
