package com.wt.domain.write.commands;

import java.text.ParseException;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class RegisterExistingUser extends RegisterInvestorFromUserPool {
  private static final long serialVersionUID = 1L;
  
  public RegisterExistingUser(String userEmail, String firstName, String lastName, String email) throws ParseException {
    super(userEmail.toLowerCase().split("@")[0], firstName+ "" + lastName, "", email);
  }
    
}
