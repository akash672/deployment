package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.ToString;

@ToString
public class CurrentUserAdviseFlushedDueToExit extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;

  public CurrentUserAdviseFlushedDueToExit(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, IssueType issueType ) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
  }

}
