package com.wt.domain.write.events;

import static akka.http.javadsl.model.StatusCodes.OK;

import com.wt.domain.InvestingMode;

import akka.http.javadsl.model.StatusCode;
import lombok.Getter;
import lombok.ToString;

@ToString
public class FundExited extends UserFundEvent implements Result {

  private static final long serialVersionUID = 1L;
  private String id;
  private String message;
  @Getter
  private double amount;
  
  public FundExited(String email, String adviserUsername, String fundName, InvestingMode investingMode, String message, double amount) {
    super(email, adviserUsername, fundName, investingMode);
    this.id = fundName;
    this.message = message;
    this.amount = amount;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public StatusCode getStatusCode() {
    return OK;
  }


}
