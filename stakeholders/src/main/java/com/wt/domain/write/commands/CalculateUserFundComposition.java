package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wt.domain.UserFundCompositeKey;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CalculateUserFundComposition {

  private UserFundCompositeKey userFundCompositeKey;

  @JsonCreator
  public CalculateUserFundComposition(UserFundCompositeKey userFundCompositeKey) {
    this.userFundCompositeKey = userFundCompositeKey;
  }
}
