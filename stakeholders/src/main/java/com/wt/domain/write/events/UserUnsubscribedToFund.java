package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserUnsubscribedToFund extends UserEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private String adviserUsername;
  @Getter
  private String fundName;
  @Getter
  private InvestingMode investingmode;
  @Getter
  private double unsubscribedAmount = 0.;
  @Getter
  private long lastFundActionTimestamp;
  
  public UserUnsubscribedToFund(String username, String adviserUsername, String fundName, InvestingMode investingMode, double unsubscribedAmount, long lastFundActionTimestamp) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.investingmode = investingMode;
    this.unsubscribedAmount = unsubscribedAmount;
    this.lastFundActionTimestamp = lastFundActionTimestamp;
  }
  

}
