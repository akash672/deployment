package com.wt.domain.write.events;

import com.wt.domain.CorporateEventType;
import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;
import com.wt.domain.UserAdviseState;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class UpdateCorporateEvent extends UserAdviseEvent {


  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;
  private double adjustPaf;
  private CorporateEventType eventType;
  private String localOrderId;
  private long exDate;
  private String token;
  private Purpose purpose;
  private double price;
 
  public UpdateCorporateEvent(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, UserAdviseState userAdviseState, double adjustPaf, CorporateEventType eventType,
      String localOrderId, long exDate, String token, IssueType issueType, Purpose purpose, double price) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
    this.adjustPaf = adjustPaf;
    this.eventType = eventType;
    this.localOrderId = localOrderId;
    this.exDate = exDate;
    this.token = token;
    this.purpose = purpose;
    this.price = price;
        
  }
  
  public static double executeAtZeroPrice(){
    return 0.0;
  }

}
