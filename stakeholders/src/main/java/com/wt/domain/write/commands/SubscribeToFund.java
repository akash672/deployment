package com.wt.domain.write.commands;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class SubscribeToFund extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private String adviserUsername;
  private String fundName;
  private double lumpSumAmount;
  private double sipAmount;
  private String sipDate;
  private InvestingMode investingMode;
  private Purpose investingPurpose;
  @Setter
  private boolean isThisaPostMarketCommand = false;

  @JsonCreator
  public SubscribeToFund(@JsonProperty("adviser") String adviserUsername, @JsonProperty("fund") String fundName,
      @JsonProperty("username") String username, @JsonProperty("lumpSumAmount") double lumpSumAmount,
      @JsonProperty("sipAmount") double sipAmount, @JsonProperty("sipDate") String sipDate,
      @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("investingPurpose") Purpose investingPurpose) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.lumpSumAmount = lumpSumAmount;
    this.sipAmount = sipAmount;
    this.sipDate = sipDate;
    this.investingMode = investingMode;
    this.investingPurpose = investingPurpose;
  }

  public static SubscribeToFund fromPostMarketCommand(Map<String, String> value) {
    return new SubscribeToFund(value.get("adviserUsername"), value.get("fundName"), value.get("username"),
        Double.parseDouble(value.get("lumpSumAmount") + ""), Double.parseDouble(value.get("sipAmount") + ""),
        value.get("sipDate"), InvestingMode.valueOf(value.get("investingMode")),
        Purpose.valueOf(value.get("investingPurpose")));
  }
}
