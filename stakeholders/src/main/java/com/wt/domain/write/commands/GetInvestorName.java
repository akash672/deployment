package com.wt.domain.write.commands;

public class GetInvestorName extends UnAuthenticatedUserCommand{
  private static final long serialVersionUID = 1L;

  public GetInvestorName(String email) {
    super(email);
  }
  
}
