package com.wt.domain.write.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.domain.UserResponseParameters;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Log4j
@Getter
@ToString(callSuper = true)
public class RetryRejectedUserAdvise extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private List<UserResponseParameters> rejectedAdviseDetails;
  private String basketOrderId;
  private ObjectMapper objectMapper = new ObjectMapper();
  @Setter private BrokerAuthInformation brokerAuthInformation;

  @JsonCreator
  public RetryRejectedUserAdvise(@JsonProperty("username") String username, @JsonProperty("rejectedAdviseDetails") String rejectedAdviseDetails,
      @JsonProperty("basketOrderId") String basketOrderId, @JsonProperty("brokerAuthInformation") BrokerAuthInformation authenticationInfo) {
    super(username);
    this.basketOrderId = basketOrderId;
    this.brokerAuthInformation = authenticationInfo;
    TypeReference<List<UserResponseParameters>> mapType = new TypeReference<List<UserResponseParameters>>() {
    };
    try {
      this.rejectedAdviseDetails = objectMapper.readValue(rejectedAdviseDetails, mapType);
    } catch (IOException e) {
      log.error("Could not map json due to " + e.getMessage());
    }
  }

 
  public static RetryRejectedUserAdvise fromPostMarketCommand(Map<String, String> value) {
    return new RetryRejectedUserAdvise(value.get("username"), value.get("rejectedAdviseDetails"), value.get("basketOrderId"), BrokerAuthInformation.withNoBrokerAuth());
  }

  public void validate() {
    if (this.basketOrderId.isEmpty()) {
      throw new IllegalArgumentException("BasketOrderId is required");
    } else if (!areRejectionRetrialsUnique()) {
      throw new IllegalArgumentException("Could not process as multiple fund rejections received");
    } else if (isMultiFundRejectionsReceived()) {
      throw new IllegalArgumentException("Rejected retrials are not unique and are not valid for this user."
          + " Your request to approve pending approvals cannot be processed");
    }
  }

  public boolean areRejectionRetrialsUnique() {
    List<String> adviseIds = new ArrayList<String>();
    AtomicInteger atomicInteger = new AtomicInteger(0);
    this.rejectedAdviseDetails.stream().forEach(userResponse -> {
      if (adviseIds.contains(userResponse.getAdviseId()))
        atomicInteger.set(1);
      adviseIds.add(userResponse.getAdviseId());
    });
    if (atomicInteger.get() != 0)
      return false;
    else
      return true;
  }

  public boolean isMultiFundRejectionsReceived() {
    Set<String> funds = new HashSet<String>();
    this.rejectedAdviseDetails.stream().forEach(userResponse -> funds.add(userResponse.getFundName()));
    if (funds.size() != 1)
      return true;
    else
      return false;
  }
  
  public boolean isAuthticationInfoBlank() {
    return (this.brokerAuthInformation == null);
  }
}