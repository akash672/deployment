package com.wt.domain.write.events;

import static com.wt.utils.DoublesUtil.round;

import com.google.gson.annotations.Expose;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UserAdvisePerformanceCalculated extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  @Expose private final String wdId;
  @Expose private final String symbol;
  @Expose private final double realizedPnL;
  @Expose private final double unrealizedPnL;
  @Expose private final double pnl;
  @Expose private final double pctPnL;
  private final double cashFromExit;
  @Expose private final double currentValue;
  @Expose private final double allocation;
  @Expose private final double nbShares;
  private final long date;
  
  public UserAdvisePerformanceCalculated(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String wdId, String symbol, double realizedPnL, double unrealizedPnL, double pnl,
      double pctPnL, double cashFromExit, double currentValue, double allocation, double nbShares, long date, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.wdId = wdId;
    this.symbol = symbol;
    this.realizedPnL = round(realizedPnL);
    this.unrealizedPnL = round(unrealizedPnL);
    this.pnl = round(pnl);
    this.pctPnL = round(pctPnL);
    this.cashFromExit = round(cashFromExit);
    this.currentValue = round(currentValue);
    this.allocation = round(allocation);
    this.nbShares = nbShares;
    this.date = date;
  }

  public UserAdvisePerformanceCalculated(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String wdId, double realizedPnL, double unrealizedPnL, double pnl,
      double pctPnL, double cashFromExit, double currentValue, double allocation, double nbShares, long date, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.wdId = wdId;
    this.symbol = wdId;
    this.realizedPnL = round(realizedPnL);
    this.unrealizedPnL = round(unrealizedPnL);
    this.pnl = round(pnl);
    this.pctPnL = round(pctPnL);
    this.currentValue = round(currentValue);
    this.cashFromExit = round(cashFromExit);
    this.allocation = round(allocation);
    this.nbShares = nbShares;
    this.date = date;
  }

}
