package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviseOrderTradeExecuted extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;
  private String exchangeOrderId;
  private String tradeId;
  private OrderSide orderSide;
  private String clientCode;
  private double quantity;
  private PriceWithPaf price;
  private String wdId;
  private String folioNumber;

  public AdviseOrderTradeExecuted(String email, String adviserName, String fundName, InvestingMode investingMode,
      String adviseId, UserAdviseState userAdviseState, OrderSide orderSide, String tradeId, String exchangeOrderId,
      String clientCode, double quantity, PriceWithPaf price, String wdId, IssueType issueType) {
    super(email, adviserName, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
    this.tradeId = tradeId;
    this.exchangeOrderId = exchangeOrderId;
    this.orderSide = orderSide;
    this.clientCode = clientCode;
    this.quantity = quantity;
    this.price = price;
    this.wdId = wdId;
  }
  
  public static class AdviseOrderTradeExecutedBuilder extends UserAdviseEvent {
    private static final long serialVersionUID = 1L;
    private UserAdviseState userAdviseState;
    private String exchangeOrderId;
    private String tradeId;
    private OrderSide orderSide;
    private String clientCode;
    private double quantity;
    private PriceWithPaf price;
    private String wdId;
    private String folioNumber;

    public  AdviseOrderTradeExecutedBuilder(String email, String adviserName, String fundName, InvestingMode investingMode,
        String adviseId, UserAdviseState userAdviseState, OrderSide orderSide, String tradeId, String exchangeOrderId,
        String clientCode, double quantity, PriceWithPaf price, String wdId, IssueType issueType) {
      super(email, adviserName, fundName, investingMode, adviseId, issueType);
      this.userAdviseState = userAdviseState;
      this.tradeId = tradeId;
      this.exchangeOrderId = exchangeOrderId;
      this.orderSide = orderSide;
      this.clientCode = clientCode;
      this.quantity = quantity;
      this.price = price;
      this.wdId = wdId;
    }
    
    public AdviseOrderTradeExecutedBuilder withFolioNumber(String folioNumber) {
      this.folioNumber = folioNumber;
      return this;
    }
    
    public AdviseOrderTradeExecuted build() {
      return new AdviseOrderTradeExecuted(this);
    }
    
  }
  
  public AdviseOrderTradeExecuted(AdviseOrderTradeExecutedBuilder builder) {
    super(builder.getUsername(), builder.getAdviserUsername(), builder.getFundName(), builder.getInvestingMode(), builder.getAdviseId(), builder.getIssueType());
    this.userAdviseState = builder.userAdviseState;
    this.tradeId = builder.tradeId;
    this.exchangeOrderId = builder.exchangeOrderId;
    this.orderSide = builder.orderSide;
    this.clientCode = builder.clientCode;
    this.quantity = builder.quantity;
    this.price = builder.price;
    this.wdId = builder.wdId;
    this.folioNumber =builder.folioNumber;
  }
  
}
