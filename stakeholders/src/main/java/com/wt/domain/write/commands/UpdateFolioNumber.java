package com.wt.domain.write.commands;

import com.wt.domain.InvestingMode;
import com.wt.domain.write.events.UserEvent;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdateFolioNumber extends UserEvent {

  private static final long serialVersionUID = 1L;
  private String amcCode;
  private InvestingMode investingMode;
  private String folioNumber;
  
  public UpdateFolioNumber(String username, String amcCode, InvestingMode investingMode, String folioNumber) {
    super(username);
    this.amcCode = amcCode;
    this.investingMode = investingMode;
    this.folioNumber = folioNumber;
  }

}
