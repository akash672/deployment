package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class SendTheRequestorActorThePortfolioValue {
  @Getter private double portfolioValue;
}
