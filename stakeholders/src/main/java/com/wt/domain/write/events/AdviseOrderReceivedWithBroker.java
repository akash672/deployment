package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviseOrderReceivedWithBroker extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;
  private String localOrderId;
  private String brokerOrderId;

  public AdviseOrderReceivedWithBroker(String email, String fundName, String adviserName, InvestingMode investingMode,
      String adviseId, UserAdviseState userAdviseState, String localOrderId, String brokerOrderId, IssueType issuetype) {
    super(email, adviserName, fundName, investingMode, adviseId, issuetype);
    this.userAdviseState = userAdviseState;
    this.localOrderId = localOrderId;
    this.brokerOrderId = brokerOrderId;
  }

}
