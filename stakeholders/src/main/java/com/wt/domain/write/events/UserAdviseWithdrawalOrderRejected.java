package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserFundState;

import lombok.Getter;
import lombok.ToString;


@Getter
@ToString
public class UserAdviseWithdrawalOrderRejected extends UserFundEvent {
  
  private static final long serialVersionUID = 1L;
  private UserFundState stateName;
  private String adviseId;

  
  public UserAdviseWithdrawalOrderRejected( UserFundState stateName, String adviseId, String adviserUsername, String username,
      String fundName, InvestingMode investingMode) {
    super(username, adviserUsername, fundName, investingMode);
    this.stateName = stateName;
    this.adviseId = adviseId;
  }
  

}
