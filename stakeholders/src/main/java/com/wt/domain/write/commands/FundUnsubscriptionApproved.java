package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FundUnsubscriptionApproved extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private String fundName;
  private String adviserUsername;
  private String wealthCode;
  private InvestingMode investingMode;
  private Purpose investingPurpose;
  private ApprovalResponse approvalResponse;
  
  @JsonCreator
  public FundUnsubscriptionApproved(@JsonProperty("username") String username,@JsonProperty("fund") String fundName, 
      @JsonProperty("adviser")String adviserUsername, @JsonProperty("wealthCode") String wealthCode,
      @JsonProperty("investingMode") InvestingMode investingMode, @JsonProperty("investingPurpose") Purpose investingPurpose,
      @JsonProperty("approvalResponse") ApprovalResponse approvalResponse) {
    super(username);
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.wealthCode = wealthCode;
    this.investingMode = investingMode;
    this.investingPurpose = investingPurpose;
    this.approvalResponse = approvalResponse;
  }

}
