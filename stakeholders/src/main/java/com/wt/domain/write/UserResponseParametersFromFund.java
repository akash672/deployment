package com.wt.domain.write;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.wt.domain.UserResponseParameters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class UserResponseParametersFromFund implements Serializable{

  private static final long serialVersionUID = 1L;
  
  @Expose
  private String fundName;
  @Expose
  private List<UserResponseParameters> userResponseParameters;

}
