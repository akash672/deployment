package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;
import com.wt.domain.RiskLevel;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FundUserSubscribed extends UserFundEvent {
  private static final long serialVersionUID = 1L;
  private String identityId;
  private long lastFundActionTimestamp;
  private double lumpsumAmount;
  private double sipAmount;
  private String sipDate;
  private RiskLevel riskProfile;
  private String inceptionDate;
  private Purpose investingPurpose;
  private String basketOrderId;

  @Deprecated
  public FundUserSubscribed(String identityId, String username, String adviserUsername, String fundName,
      InvestingMode investingMode, double lumpsumAmount, double sipAmount, String sipDate, long lastFundActionTimestamp,
      RiskLevel riskProfile, String inceptionDate, Purpose investingPurpose) {
    this(username, adviserUsername, fundName, investingMode, lumpsumAmount, sipAmount, sipDate, lastFundActionTimestamp,
        riskProfile, inceptionDate, investingPurpose);
  }

  public FundUserSubscribed(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      double lumpsumAmount, double sipAmount, String sipDate, long lastFundActionTimestamp, RiskLevel riskProfile,
      String inceptionDate, Purpose investingPurpose) {
    super(username, adviserUsername, fundName, investingMode);
    this.identityId = username;
    this.lumpsumAmount = lumpsumAmount;
    this.sipAmount = sipAmount;
    this.sipDate = sipDate;
    this.lastFundActionTimestamp = lastFundActionTimestamp;
    this.riskProfile = riskProfile;
    this.inceptionDate = inceptionDate;
    this.investingPurpose = investingPurpose;
    this.basketOrderId = "DUMMY-BATCH-ID";
  }
  
  public FundUserSubscribed(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      double lumpsumAmount, double sipAmount, String sipDate, long lastFundActionTimestamp, RiskLevel riskProfile,
      String inceptionDate, Purpose investingPurpose, String basketOrderId) {
    super(username, adviserUsername, fundName, investingMode);
    this.identityId = username;
    this.lumpsumAmount = lumpsumAmount;
    this.sipAmount = sipAmount;
    this.sipDate = sipDate;
    this.lastFundActionTimestamp = lastFundActionTimestamp;
    this.riskProfile = riskProfile;
    this.inceptionDate = inceptionDate;
    this.investingPurpose = investingPurpose;
    this.basketOrderId = basketOrderId;
  }
}
