package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RequestedFolioNumber {
  
  private String amcCode;
  private String folioNumber;

}
