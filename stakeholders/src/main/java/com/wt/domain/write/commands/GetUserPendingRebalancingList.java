package com.wt.domain.write.commands;

import lombok.Getter;

@Getter
public class GetUserPendingRebalancingList extends AuthenticatedUserCommand {
  
  private static final long serialVersionUID = 1L;
  private String investingMode;
  public GetUserPendingRebalancingList( String username, String investingMode) {
    super(username);
    this.investingMode = investingMode;
  }

}
