package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdatingUserFundCash extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private double cashToAddToUserFund;

  public UpdatingUserFundCash(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      double cashToAddToUserFund) {
    super(username, adviserUsername, fundName, investingMode);
    this.cashToAddToUserFund = cashToAddToUserFund;
  }

}
