package com.wt.domain.write.commands;

import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;
import com.wt.domain.RiskLevel;
import com.wt.domain.write.events.FundConstituentListResponse;

import lombok.Getter;

public class SubscribeToFundWithUserEmail extends SubscribeToFund {

  private static final long serialVersionUID = 1L;
  @Getter
  private String userEmail;
  @Getter
  private String inceptionDate;
  @Getter
  private RiskLevel riskProfile;
  @Getter
  private FundConstituentListResponse fundConstituentListResponse;
  @Getter
  private String basketOrderId;
  @Getter
  private BrokerAuthInformation authInformation;

  public SubscribeToFundWithUserEmail(String adviserName, String fundName, String username, String userEmail,
      double lumpsumAmount, double sipAmount, String date, RiskLevel riskProfile, String inceptionDate,
      InvestingMode investingMode, Purpose investingPurpose, FundConstituentListResponse fundConstituentListResponse,
      String basketOrderId, BrokerAuthInformation authInformation) {
    super(adviserName, fundName, username, lumpsumAmount, sipAmount, date, investingMode, investingPurpose);
    this.userEmail = userEmail;
    this.riskProfile = riskProfile;
    this.inceptionDate = inceptionDate;
    this.fundConstituentListResponse = fundConstituentListResponse;
    this.basketOrderId = basketOrderId;
    this.authInformation = authInformation;
  }
  
}
