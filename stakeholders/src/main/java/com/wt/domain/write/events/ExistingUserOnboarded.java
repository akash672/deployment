package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@ToString
public class ExistingUserOnboarded implements Serializable {
  private static final long serialVersionUID = 1L;
  private String username;
  private String identityId;
  @Getter
  private boolean isOnboardingInProcess;

  public String getUsername() {
    if (username != null) {
      return username;
    }
    return identityId;
  }

  public ExistingUserOnboarded(String username, boolean isOnboardingInProcess) {
    super();
    this.username = username;
    this.isOnboardingInProcess = isOnboardingInProcess;
  }

}
