package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@ToString
public class ZeroPriceReceived implements Serializable {

  private static final long serialVersionUID = 1L;
  @Getter private String message;
  
  public ZeroPriceReceived(String message) {
    this.message = message;
  }

}
