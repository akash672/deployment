package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class ExecutedAdviseDetailsUpdated extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private String adviseId;
  @Getter
  private double allocationValue;
  @Getter
  private double tradedValue;

  public ExecutedAdviseDetailsUpdated(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, double allocationValue, double tradedValue) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.allocationValue = allocationValue;
    this.tradedValue = tradedValue;
  }

}
