package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=false)
public class UserLumpSumAddedToFund extends UserEvent {

  private static final long serialVersionUID = 1L;
  private String fundName;
  private String adviserUsername;
  private double lumpSumAmount;
  private InvestingMode investingMode;

  public UserLumpSumAddedToFund(String username, String adviserUsername, String fundName, InvestingMode investingMode, double lumpSumAmount) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.lumpSumAmount = lumpSumAmount;
    this.investingMode = investingMode;
  }

}