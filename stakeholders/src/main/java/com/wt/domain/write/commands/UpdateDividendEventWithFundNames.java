package com.wt.domain.write.commands;

import java.util.List;

import com.wt.domain.CorporateEventType;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdateDividendEventWithFundNames extends CorporateEventUpdateWithFundNames {
  
  private static final long serialVersionUID = 1L;
  private double dividendAmount;
  
  public UpdateDividendEventWithFundNames(String wdId, CorporateEventType eventType, long eventDate, double dividendAmount,
      List<String> fundNames) {
    super(wdId, eventType, eventDate, fundNames);
    this.dividendAmount = dividendAmount;
  }

}
