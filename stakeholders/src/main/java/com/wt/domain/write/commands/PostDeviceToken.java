package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class PostDeviceToken extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  
  @Getter
  private String deviceToken;
  
  @JsonCreator
  public PostDeviceToken(@JsonProperty("username") String username, @JsonProperty("deviceToken") String deviceToken) {
    super(username);
    assertStringNotEmpty(deviceToken, "deviceToken");
    this.deviceToken = deviceToken;
  }

}
