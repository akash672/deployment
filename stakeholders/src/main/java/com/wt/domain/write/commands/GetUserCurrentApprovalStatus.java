package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GetUserCurrentApprovalStatus extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private String adviserUsername;
  private String fundName;
  private InvestingMode investingMode;

  @JsonCreator
  public GetUserCurrentApprovalStatus(@JsonProperty("username") String username,
      @JsonProperty("adviser") String adviserUserName, @JsonProperty("fund") String fundName,
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super(username);
    this.adviserUsername = adviserUserName;
    this.fundName = fundName;
    this.investingMode = investingMode;
  }

}
