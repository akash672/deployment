package com.wt.domain.write.events;

import lombok.Getter;

@Getter
public class UserPasswordChanged extends UserEvent {

  private static final long serialVersionUID = 1L;

  private byte[] password;
  private byte[] salt;

  public UserPasswordChanged(String email, byte[] password, byte[] salt) {
    super(email);
    this.password = password;
    this.salt = salt;
  }

}
