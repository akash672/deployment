package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class WithdrawSumFromUserFundAttempted extends UserFundEvent {
  
  private static final long serialVersionUID = 1L;
  private String identityId;
  private long lastFundActionTimestamp;
  private double withdrawalAmount;

  public WithdrawSumFromUserFundAttempted(String id,String email, String adviserUsername, String fundName, InvestingMode investingMode, double withdrawalAmount, long lastFundActionTimestamp) {
    super(email, adviserUsername, fundName, investingMode);
    this.identityId = id;
    this.withdrawalAmount = withdrawalAmount;
    this.lastFundActionTimestamp = lastFundActionTimestamp;

  }

}
