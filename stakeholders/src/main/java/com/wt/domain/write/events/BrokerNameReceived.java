package com.wt.domain.write.events;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class BrokerNameReceived extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  @Getter private BrokerName brokerName;
  
  public BrokerNameReceived(String email, String adviserUsername, String fundName, String adviseId, BrokerName brokerName, InvestingMode investingMode, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.brokerName = brokerName;
  }

}
