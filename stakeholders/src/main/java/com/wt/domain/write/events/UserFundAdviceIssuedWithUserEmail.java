package com.wt.domain.write.events;

import static com.wt.domain.PriceWithPaf.noPrice;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.utils.DateUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserFundAdviceIssuedWithUserEmail extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  @SuppressWarnings("unused")
  private String identityId;
  private String token;
  private String symbol;
  private String description;
  private double absoluteAllocation;
  private double monetaryAllocationValue;
  private long issueTime;
  private PriceWithPaf entryPrice;
  private long entryTime;
  private PriceWithPaf exitPrice;
  private long exitTime;
  private boolean isUserBeingOnboarded;
  private boolean isSpecialAdvise;
  private BrokerName brokerName;
  private String clientCode;
  private String basketOrderCompositeId;
  private BrokerAuthInformation brokerAuthInformation;
  
  @Deprecated
  public String getIdentityId() {
    return super.getUsername();
  }

  @Deprecated
  public UserFundAdviceIssuedWithUserEmail(String identityId, String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String token, String symbol, String description,
      double absoluteAllocation, double monetaryAllocationValue, long issueTime, PriceWithPaf entryPrice,
      long entryTime, PriceWithPaf exitPrice, long exitTime, boolean isUserBeingOnboarded, BrokerName brokerName,
      String clientCode, IssueType issueType, boolean isSpecialAdvise, String basketOrderCompositeId,
      BrokerAuthInformation brokerAuthInformation) {
    this(username, adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, issueTime, entryPrice, entryTime, exitPrice, exitTime, isUserBeingOnboarded,
        brokerName, clientCode, issueType, isSpecialAdvise, basketOrderCompositeId, brokerAuthInformation);
  }

  public UserFundAdviceIssuedWithUserEmail(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String token, String symbol, String description,
      double absoluteAllocation, double monetaryAllocationValue, long issueTime, PriceWithPaf entryPrice,
      long entryTime, PriceWithPaf exitPrice, long exitTime, boolean isUserBeingOnboarded, BrokerName brokerName,
      String clientCode, IssueType issueType, boolean isSpecialAdvise, String basketOrderCompositeId,
      BrokerAuthInformation brokerAuthInformation) {
    super(username, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.identityId = username;
    this.token = token;
    this.symbol = symbol;
    this.description = description;
    this.absoluteAllocation = absoluteAllocation;
    this.monetaryAllocationValue = monetaryAllocationValue;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.isUserBeingOnboarded = isUserBeingOnboarded;
    this.brokerName = brokerName;
    this.clientCode = clientCode;
    this.isSpecialAdvise = isSpecialAdvise;
    this.basketOrderCompositeId = basketOrderCompositeId;
    this.brokerAuthInformation = brokerAuthInformation;
  }

  @Deprecated
  public static UserFundAdviceIssuedWithUserEmail fundAdviceWithUndefinedExitParams(String identityId, String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      PriceWithPaf entryPrice, boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType,
      boolean isSpecialAdvise, String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(identityId,
        username, adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), entryPrice, DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserOnboarded, brokerName, clientCode, issueType, isSpecialAdvise, basketOrderCompositeId,
        brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;

  }

  public static UserFundAdviceIssuedWithUserEmail fundAdviceWithUndefinedExitParams(String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      PriceWithPaf entryPrice, boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType,
      boolean isSpecialAdvise, String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(username,
        adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), entryPrice, DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserOnboarded, brokerName, clientCode, issueType, isSpecialAdvise, basketOrderCompositeId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;

  }

  @Deprecated
  public static UserFundAdviceIssuedWithUserEmail fundAdviceWithUndefinedExitParamsAndMarketPrice(String identityId,
      String username, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId,
      String token, String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType, boolean isSpecialAdvise,
      String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(identityId,
        username, adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserOnboarded, brokerName, clientCode, issueType, isSpecialAdvise, basketOrderCompositeId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  public static UserFundAdviceIssuedWithUserEmail fundAdviceWithUndefinedExitParamsAndMarketPrice(String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      boolean isUserOnboarded, BrokerName brokerName, String clientCode, IssueType issueType, boolean isSpecialAdvise,
      String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(username,
        adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserOnboarded, brokerName, clientCode, issueType, isSpecialAdvise, basketOrderCompositeId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  @Deprecated
  public static UserFundAdviceIssuedWithUserEmail nonSpecialFundAdviceForANonOnboardedUser(String identityId,
      String username, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId,
      String token, String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      BrokerName brokerName, String clientCode, IssueType issueType, String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(identityId,
        username, adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, false, brokerName, clientCode, issueType, false, basketOrderCompositeId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  public static UserFundAdviceIssuedWithUserEmail nonSpecialFundAdviceForANonOnboardedUser(String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      BrokerName brokerName, String clientCode, IssueType issueType, String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(username,
        adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, false, brokerName, clientCode, issueType, false, basketOrderCompositeId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  @Deprecated
  public static UserFundAdviceIssuedWithUserEmail nonSpecialUserFundAdvice(String identityId, String username,
      String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String token,
      String symbol, String description, double absoluteAllocation, double monetaryAllocationValue,
      boolean isUserBeingOnboarded, BrokerName brokerName, String clientCode, IssueType issueType, String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(identityId,
        username, adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserBeingOnboarded, brokerName, clientCode, issueType, false, basketOrderCompositeId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }

  public static UserFundAdviceIssuedWithUserEmail nonSpecialUserFundAdvice(String username, String adviserUsername,
      String fundName, InvestingMode investingMode, String adviseId, String token, String symbol, String description,
      double absoluteAllocation, double monetaryAllocationValue, boolean isUserBeingOnboarded, BrokerName brokerName,
      String clientCode, IssueType issueType, String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = new UserFundAdviceIssuedWithUserEmail(username,
        adviserUsername, fundName, investingMode, adviseId, token, symbol, description, absoluteAllocation,
        monetaryAllocationValue, DateUtils.currentTimeInMillis(), noPrice(token), DateUtils.currentTimeInMillis(),
        noPrice(token), 0L, isUserBeingOnboarded, brokerName, clientCode, issueType, false, basketOrderCompositeId, brokerAuthInformation);
    return fundAdviceIssuedWithUserEmail;
  }
}
