package com.wt.domain.write.events;

import static com.wt.domain.BrokerName.BPWEALTH;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.VIRTUAL;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserFundAdviceIssued extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  @SuppressWarnings("unused")
  private String identityId;
  private String token;
  private double allocation;
  private double absoluteAllocation;
  private double portfolioValue;
  private long issueTime;
  private PriceWithPaf entryPrice;
  private long entryTime;
  private PriceWithPaf exitPrice;
  private long exitTime;
  private String adviseId;
  private String clientCode;
  private BrokerName brokerName;
  private String symbol;
  
  @Deprecated
  public String getIdentityId() {
    return getUsername();
  }

  @Deprecated
  public UserFundAdviceIssued(String identityId, String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, String token, double allocation, double absoluteAllocation,
      long issueTime, PriceWithPaf entryPrice, long entryTime, PriceWithPaf exitPrice, long exitTime) {
    this(username, adviserUsername, fundName, investingMode, adviseId, token, allocation, absoluteAllocation, issueTime,
        entryPrice, entryTime, exitPrice, exitTime);
  }

  public UserFundAdviceIssued(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, String token, double allocation, double absoluteAllocation, long issueTime,
      PriceWithPaf entryPrice, long entryTime, PriceWithPaf exitPrice, long exitTime) {
    super(username, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.identityId = username;
    this.token = token;
    this.allocation = allocation;
    this.absoluteAllocation = absoluteAllocation;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.clientCode = "";
    if (investingMode.equals(VIRTUAL))
      this.brokerName = MOCK;
    else
      this.brokerName = BPWEALTH;
    this.symbol = "";
  }

  @Deprecated
  public UserFundAdviceIssued(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      String identityId, String token, double allocation, double absoluteAllocation, double portfolioValue, long issueTime,
      PriceWithPaf entryPrice, long entryTime, PriceWithPaf exitPrice, long exitTime, String adviseId,
      String clientCode, BrokerName brokerName, String symbol) {
    this(username, adviserUsername, fundName, investingMode, token, allocation, absoluteAllocation, portfolioValue, issueTime,
        entryPrice, entryTime, exitPrice, exitTime, adviseId, clientCode, brokerName, symbol);
  }

  public UserFundAdviceIssued(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      String token, double allocation, double absoluteAllocation, double portfolioValue, long issueTime, PriceWithPaf entryPrice,
      long entryTime, PriceWithPaf exitPrice, long exitTime, String adviseId, String clientCode, BrokerName brokerName,
      String symbol) {
    super(username, adviserUsername, fundName, investingMode);
    this.identityId = username;
    this.token = token;
    this.allocation = allocation;
    this.absoluteAllocation = absoluteAllocation;
    this.portfolioValue = portfolioValue;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.adviseId = adviseId;
    this.clientCode = clientCode;
    this.brokerName = brokerName;
    this.symbol = symbol;
  }

}
