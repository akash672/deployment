package com.wt.domain.write.commands;

import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;

import javax.ws.rs.BadRequestException;

import lombok.Getter;

@Getter
public class GetUserRejectedAdvisesList extends AuthenticatedUserCommand {
  
  private static final long serialVersionUID = 1L;
  private String investingMode;
  public GetUserRejectedAdvisesList( String username, String investingMode) {
    super(username);
    this.investingMode = investingMode;
  }

  public void validate() {
    if (this.investingMode.isEmpty() || !isValidInvestingMode())
      throw new BadRequestException("investingMode is not valid");
  }

  private boolean isValidInvestingMode() {
    return this.investingMode.equals(REAL.getInvestingMode())
        || this.investingMode.equals(VIRTUAL.getInvestingMode());
  }
}
