package com.wt.domain.write.commands;

import static java.lang.Double.parseDouble;
import static java.lang.String.valueOf;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class AddLumpsumToFund extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private String adviserUsername;
  private String fundName;
  private double lumpSumAmount;
  private InvestingMode investingMode;
  @Setter private boolean isThisaPostMarketCommand = false;
  
  @JsonCreator
  public AddLumpsumToFund(@JsonProperty("adviser") String adviserUsername, @JsonProperty("fund") String fundName,
      @JsonProperty("username") String username, @JsonProperty("lumpSumAmount") double lumpSumAmount,
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.lumpSumAmount = lumpSumAmount;
    this.investingMode = investingMode;
  }

  public static AddLumpsumToFund fromPostMarketCommand(Map<String, String> value) {
    return new AddLumpsumToFund(value.get("adviserUsername"), value.get("fundName"), value.get("username"),
        parseDouble(valueOf(value.get("lumpSumAmount"))), 
        InvestingMode.valueOf(value.get("investingMode")));
  }
}