package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class CorporateMergerSellEvent extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private String adviseId;
  private String oldToken;
  private double oldPrice;
  private OrderSide orderSide;

  public CorporateMergerSellEvent(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, String oldToken, double oldPrice, OrderSide orderSide) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.oldToken = oldToken;
    this.oldPrice = oldPrice;
    this.orderSide = orderSide;

  }

}