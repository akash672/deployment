package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class AdviseTradesExecuted extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  private double allocationValue;
  private double tradedValue;
  private double numberOfShares; 
  private UserAdviseState userAdviseState;
  private String symbol;
  private OrderSide orderSide;
  private double executionPrice;
  
  public AdviseTradesExecuted(String email, String adviserUsername, String fundName, InvestingMode investingMode,  String adviseId,
      UserAdviseState userAdviseState, double allocationValue, double tradedValue, double numberOfShares, String symbol, OrderSide orderSide, double executionPrice, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
    this.allocationValue = allocationValue;
    this.tradedValue = tradedValue;
    this.numberOfShares = numberOfShares;
    this.symbol = symbol;
    this.orderSide = orderSide;
    this.executionPrice = executionPrice;
  }
  
  public static AdviseTradesExecuted closeAdviseTradesExecutedWithExecutionDetails(String email, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId,
      UserAdviseState userAdviseState, double allocationValue, double numberOfShares, String symbol,  OrderSide orderSide, double executionPrice, IssueType issueType) {
    return new AdviseTradesExecuted(email, adviserUsername, fundName, investingMode, adviseId, userAdviseState, allocationValue, 0d, numberOfShares, symbol, orderSide, executionPrice, issueType);
  }
}
