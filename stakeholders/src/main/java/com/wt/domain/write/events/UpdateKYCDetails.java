package com.wt.domain.write.events;

import lombok.Getter;

public class UpdateKYCDetails extends UserEvent {

  private static final long serialVersionUID = 1L;
  private @Getter String panCard;
  private @Getter String brokerCompany;
  private @Getter String phoneNumber;
  private @Getter String clientCode;
  private @Getter String identityId;
  private @Getter String emailAddress;

  public UpdateKYCDetails(String username, String panCard, String brokerCompany, String phoneNumber, String clientCode,
      String emailAddress) {
    super(username);
    this.panCard = panCard;
    this.brokerCompany = brokerCompany;
    this.phoneNumber = phoneNumber;
    this.clientCode = clientCode;
    this.identityId = username;
    this.emailAddress = emailAddress;
  }

}
