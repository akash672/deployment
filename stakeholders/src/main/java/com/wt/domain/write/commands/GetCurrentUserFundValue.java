package com.wt.domain.write.commands;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GetCurrentUserFundValue extends InternalFundCommand {

  private String username;

  public GetCurrentUserFundValue(String fundName, String adviserusername, String username) {
    super(adviserusername, fundName);
    this.username = username;
  }

}
