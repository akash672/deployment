package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class CorporateActionSell extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;

  public CorporateActionSell(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      String adviseId, UserAdviseState userAdviseState, IssueType issueType ) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
  }

}
