package com.wt.domain.write.events;

import java.nio.ByteBuffer;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ExistingUserRegistered extends CognitoUserRegistered {
  private static final long serialVersionUID = 1L;
  
  public ExistingUserRegistered(String firstName, String lastName, String userName, ByteBuffer password,
      ByteBuffer salt, String identityID, long timestamp, String deviceToken) {
    super(firstName, lastName, userName, password, salt, identityID, timestamp, deviceToken);
  }
  
}
