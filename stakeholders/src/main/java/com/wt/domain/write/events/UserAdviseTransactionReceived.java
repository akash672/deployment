package com.wt.domain.write.events;

import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static com.wt.domain.UserResponseType.Awaiting;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.utils.DateUtils.currentTimeInMillis;

import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.UserAcceptanceMode;
import com.wt.domain.UserResponseType;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UserAdviseTransactionReceived extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  private double absoluteAllocation;
  private long exitDate;
  private PriceWithPaf exitPrice;
  private UserAcceptanceMode userAcceptanceMode;
  private UserResponseType userResponseType;
  private String basketOrderCompositeId;
  private Purpose purpose;
  private BrokerAuthInformation brokerAuthInformation;

  public UserAdviseTransactionReceived(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, double absoluteAllocation, long exitDate, PriceWithPaf exitPrice, IssueType issueType, Purpose purpose,
      BrokerAuthInformation brokerAuthInformation) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.absoluteAllocation = absoluteAllocation;
    this.exitDate = exitDate;
    this.exitPrice = exitPrice;
    this.purpose = purpose;
    this.brokerAuthInformation = brokerAuthInformation;
  }
  
  public static class UserAdviseTransactionReceivedBuilder extends UserAdviseEvent {
    private static final long serialVersionUID = 1L;
    private double absoluteAllocation;
    private long exitDate = currentTimeInMillis();
    private PriceWithPaf exitPrice;
    private UserAcceptanceMode userAcceptanceMode;
    private UserResponseType userResponseType = Awaiting;
    private String basketOrderCompositeId;
    private Purpose purpose;
    private BrokerAuthInformation brokerAuthInformation;

    public UserAdviseTransactionReceivedBuilder(String email, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, IssueType issueType) {
      super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
      this.purpose = (issueType.equals(Equity)) ? EQUITYSUBSCRIPTION : MUTUALFUNDSUBSCRIPTION;
    }
    
    public UserAdviseTransactionReceivedBuilder withAbsoluteAllocation(double absoluteAllocation) {
      this.absoluteAllocation = absoluteAllocation;
      return this;
    }
    
    public UserAdviseTransactionReceivedBuilder withUserAcceptanceMode(UserAcceptanceMode userAcceptanceMode) {
      this.userAcceptanceMode = userAcceptanceMode;
      return this;
    }

    public UserAdviseTransactionReceivedBuilder withUserResponseType(UserResponseType userResponseType) {
      this.userResponseType = userResponseType;
      return this;
    }
    
    public UserAdviseTransactionReceivedBuilder withPurpose(Purpose purpose) {
      this.purpose = purpose;
      return this;
    }
    
    public UserAdviseTransactionReceivedBuilder withBrokerAuthentication(BrokerAuthInformation brokerAuthInformation) {
      this.brokerAuthInformation = brokerAuthInformation;
      return this;
    }
    
    public UserAdviseTransactionReceivedBuilder withExitPrice(PriceWithPaf exitPrice) {
      this.exitPrice = exitPrice;
      return this;
    }
    
    public UserAdviseTransactionReceivedBuilder withExitTime(long exitDate) {
      this.exitDate = exitDate;
      return this;
    }
    
    public UserAdviseTransactionReceivedBuilder withBasketOrderId(String basketOrderCompositeId) {
      this.basketOrderCompositeId = basketOrderCompositeId;
      return this;
    }
    
    public UserAdviseTransactionReceived build() {
      return new UserAdviseTransactionReceived(this);
    }
    
  }
  
  private UserAdviseTransactionReceived(UserAdviseTransactionReceivedBuilder builder) {
    super(builder.getUsername(), builder.getAdviserUsername(), builder.getFundName(), builder.getInvestingMode(), builder.getAdviseId(), builder.getIssueType());
    this.absoluteAllocation = builder.absoluteAllocation;
    this.exitDate = builder.exitDate;
    this.exitPrice = builder.exitPrice;
    this.userAcceptanceMode = builder.userAcceptanceMode;
    this.userResponseType = builder.userResponseType;
    this.basketOrderCompositeId = builder.basketOrderCompositeId;
    this.purpose = builder.purpose;
    this.brokerAuthInformation = builder.brokerAuthInformation;
   
  }
}
