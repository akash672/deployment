package com.wt.domain.write.events;

import lombok.Getter;
import lombok.ToString;

@ToString
public class OrderSentForIssuedAdvise extends UserEvent {

  private static final long serialVersionUID = 1L;
  @Getter private String fundName;
  public OrderSentForIssuedAdvise(String username, String fundName) {
    super(username);
    this.fundName = fundName;
  }

}
