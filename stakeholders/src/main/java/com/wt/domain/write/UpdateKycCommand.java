package com.wt.domain.write;

import static com.wt.domain.BrokerName.NOBROKER;
import static com.wt.domain.InvestingMode.REAL;

import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.BrokerName;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;

import lombok.Getter;
import lombok.NonNull;

public class UpdateKycCommand extends UnAuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private @Getter final String clientCode;
  private @Getter final String panCard;
  private @Getter final String brokerCompany;
  private @Getter final String phoneNumber;
  private @Getter final String email;

  @JsonCreator
  public UpdateKycCommand(@NonNull @JsonProperty("username") String username,
      @JsonProperty("clientCode") String clientCode, @JsonProperty("panCard") String panCard,
      @JsonProperty("brokerCompany") String brokerCompany, @JsonProperty("phoneNumber") String phoneNumber,
      @NonNull @JsonProperty("email") String email) {
    super(username);
    this.panCard = panCard;
    this.clientCode = clientCode;
    this.brokerCompany = brokerCompany;
    this.phoneNumber = phoneNumber;
    this.email = email;
  }

  public void validate() throws IllegalArgumentException {
    boolean validatePancard = true;
    boolean validatePhoneNumber = true;

    if (getPanCard() != null && !getPanCard().isEmpty())
      validatePancard = Pattern.compile("^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$").matcher(getPanCard()).matches();
    if (getPhoneNumber() != null && !getPhoneNumber().isEmpty())
      validatePhoneNumber = (getPhoneNumber().length() == 10 && getPhoneNumber().matches("\\d+")) ? true : false;

    BrokerName broker = NOBROKER;
    try {
      if (brokerCompany != null && !brokerCompany.isEmpty()) 
        broker = BrokerName.fromBrokerName(brokerCompany, REAL);
    } catch (Exception e) {
    }
    if (broker == null)
      throw new IllegalArgumentException("Incorrect Broker Name Received");
    if (!validatePancard) {
      throw new IllegalArgumentException("Incorrect Pancard number submitted");
    } else if (!validatePhoneNumber) {
      throw new IllegalArgumentException("Phone Number needs to be 10 Digits only");
    } else if (email.isEmpty()) {
      throw new IllegalArgumentException("Email Address is required");
    }
  }
}
