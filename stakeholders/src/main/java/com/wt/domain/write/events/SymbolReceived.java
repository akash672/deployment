package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
public class SymbolReceived extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private String symbol;

  public SymbolReceived(String email, String adviserUsername, String fundName, String adviseId, String symbol, InvestingMode investingMode, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.symbol = symbol;
  }

}
