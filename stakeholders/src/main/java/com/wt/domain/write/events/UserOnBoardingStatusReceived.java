package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.ToString;

@ToString
public class UserOnBoardingStatusReceived extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  private boolean isOnboarded;
  
  public UserOnBoardingStatusReceived(String email, String adviserUsername, String fundName, String adviseId,
      boolean isOnboarded, InvestingMode investingMode, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.isOnboarded = isOnboarded;
  }
  
}
