package com.wt.domain.write.commands;

public class HasUserBeenOnBoarded extends AuthenticatedUserCommand {
  
  private static final long serialVersionUID = 1L;
  public HasUserBeenOnBoarded(String username) {
    super(username);
  }

}
