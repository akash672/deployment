package com.wt.domain.write.events;

import java.util.Map;

import com.wt.domain.RiskLevel;

import lombok.Getter;

@Getter
public class UserRiskProfileIdentified extends UserEvent {
  private static final long serialVersionUID = -1767790708652144006L;
  private String eventName;
  private Map<String, String> answers;
  private RiskLevel riskProfile;;

  public UserRiskProfileIdentified(String username, String eventName, Map<String, String> answers, RiskLevel riskProfile) {
    super(username);
    this.eventName = eventName;
    this.answers = answers;
    this.riskProfile = riskProfile;
  }

}
