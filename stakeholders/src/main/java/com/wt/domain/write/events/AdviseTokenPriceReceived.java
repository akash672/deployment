package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class AdviseTokenPriceReceived extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;
  private PriceWithPaf realTimePrice;
  private String localOrderId;
  private Purpose purpose;
  private double price;

  public AdviseTokenPriceReceived(String email, String fundName, String adviserName, InvestingMode investingMode,
      String adviseId, UserAdviseState userAdviseState, PriceWithPaf realTimePrice, String localOrderId, IssueType issueType,
      Purpose purpose) {
    super(email, adviserName, fundName, investingMode, adviseId, issueType);
    this.userAdviseState = userAdviseState;
    this.realTimePrice = realTimePrice;
    this.localOrderId = localOrderId;
    this.purpose = purpose;
    this.price = realTimePrice.getPrice().getPrice();
  }

}
