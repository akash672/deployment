package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserFundState;

import lombok.Getter;
import lombok.ToString;

@ToString
public class AdjustFundCashForInsufficientCapitalAdvise extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private String adviseId;
  @Getter
  private double adjustAllocationValue;
  @Getter
  private UserFundState userFundState;

  public AdjustFundCashForInsufficientCapitalAdvise(String email, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, double adjustAllocationValue, UserFundState userFundState) {
    super(email, adviserUsername, fundName, investingMode);
    this.adviseId = adviseId;
    this.adjustAllocationValue = adjustAllocationValue;
    this.userFundState = userFundState;
  }

}
