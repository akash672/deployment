package com.wt.domain.write;

import java.io.Serializable;

import com.wt.domain.InvestingMode;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class AmcCodeWithInvestingMode implements Serializable{
  private static final long serialVersionUID = 1L;
  private String amcCode;
  private InvestingMode investingMode;

}
