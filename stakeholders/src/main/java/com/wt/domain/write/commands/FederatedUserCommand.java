package com.wt.domain.write.commands;

public interface FederatedUserCommand {
  String getAccessToken();
}
