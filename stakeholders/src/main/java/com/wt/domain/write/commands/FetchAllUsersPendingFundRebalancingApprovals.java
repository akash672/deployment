package com.wt.domain.write.commands;

import java.io.Serializable;

import akka.actor.ActorRef;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FetchAllUsersPendingFundRebalancingApprovals implements Serializable {

  private static final long serialVersionUID = 1L;
  private String investingMode;
  private String connectionId;
  private ActorRef destinationActor;

}
