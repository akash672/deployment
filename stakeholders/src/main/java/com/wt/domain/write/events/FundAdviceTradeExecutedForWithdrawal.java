package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class FundAdviceTradeExecutedForWithdrawal extends UserFundEvent {
  
    private static final long serialVersionUID = 1L;
    private String adviseId;
    private double allocationValue;
    private double tradedValue;
    private UserAdviseState userAdviseState;

    public FundAdviceTradeExecutedForWithdrawal(String email, String adviserUsername, String fundName, InvestingMode investingMode,
        String adviseId, UserAdviseState userAdviseState, double allocationValue, double tradedValue) {
      super(email, adviserUsername, fundName, investingMode);
      this.adviseId = adviseId;
      this.userAdviseState = userAdviseState;
      this.allocationValue = allocationValue;
      this.tradedValue = tradedValue;
    }
  }
