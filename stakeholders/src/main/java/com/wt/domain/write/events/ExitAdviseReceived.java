package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ExitAdviseReceived extends UserAdviseEvent {

  private static final long serialVersionUID = 1L;
  private String token;
  private double absoluteAllocation;
  private long exitDate;
  private PriceWithPaf exitPrice;
  private String localOrderId;
  private Purpose purpose;
  private double price;
  private boolean isSpecialExit;
  private int specialExitshares;
  private String basketOrderCompositeId;
  private BrokerAuthInformation brokerAuthInformation;
  
  public ExitAdviseReceived(String email, String adviserUsername, String fundName, InvestingMode investingMode, String token, String adviseId,
      double absoluteAllocation, long exitDate, PriceWithPaf exitPrice, String localOrderId, IssueType issueType,
      Purpose purpose, double price, boolean isSpecialExit, int specialExitShares, String basketOrderCompositeId, BrokerAuthInformation brokerAuthInformation) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.token = token;
    this.absoluteAllocation = absoluteAllocation;
    this.exitDate = exitDate;
    this.exitPrice = exitPrice;
    this.localOrderId = localOrderId;
    this.purpose = purpose;
    this.price = price;
    this.isSpecialExit = isSpecialExit;
    this.specialExitshares = specialExitShares;
    this.basketOrderCompositeId = basketOrderCompositeId;
    this.brokerAuthInformation = brokerAuthInformation;
  }
  
  public static double atMarketPrice(){
    return 0.0;
  }
  
  public static boolean isNotASpecialSellAdvise(){
    return false;
  }
  
  public static boolean isASpecialSellAdvise(){
    return true;
  }
  
  public static int quantityNotYetSet(){
    return 0;
  }
  


}
