package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserAdviseDetails implements Serializable {
  
  private static final long serialVersionUID = 1L;
  @Setter private double oldAdviseEntryPrice;
  @Setter private int quantity;
  @Setter private double oldAdviseAllocation;
  @Setter private double adviseAllocationValue;
  @Setter private double paf;
  @Setter private double primaryAdviseAvgPrice;
  @Setter private double primaryAdviseAllocation;
  @Setter private double secondaryCashRatioUtilized;
  
  public double allocationPending(){
    return oldAdviseAllocation - primaryAdviseAllocation;
  }
 
  public double averagePriceDifferential(){
    return oldAdviseEntryPrice - primaryAdviseAvgPrice;
  }
  
  
  public void updateEntryPrice(int newAdviseQuantity, double newAdviseAvgPrice){
    double updatedEntryPrice = (quantity* oldAdviseEntryPrice + newAdviseQuantity*newAdviseAvgPrice)/(newAdviseQuantity + quantity);
    this.oldAdviseEntryPrice = updatedEntryPrice;
        
  }
  
  public void updateQuantity(int additionalQuantity){
    this.quantity += additionalQuantity;
        
  }
  
  public void updateAdviseAllocation(double additionalAllocation){
    this.oldAdviseAllocation += additionalAllocation;
        
  }
  

}
