package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class SendOTP extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private @Getter final String phonenumber;

  @JsonCreator
  public SendOTP(@JsonProperty("phonenumber") String phonenumber, @JsonProperty("username") String username) {
    super(username);
    this.phonenumber = phonenumber;
  }

  public void validate() {
    if (!phonenumber.matches("\\+?\\d[\\d -]{8,12}\\d"))
      throw new IllegalArgumentException("Invalid phone number : " + phonenumber);
  }
}