package com.wt.domain.write.events;

import static com.wt.utils.DoublesUtil.round;

import com.google.gson.annotations.Expose;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class UserAdviseAbsolutePerformanceCalculated extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  private final String cognitoIdentityId;
  @Expose
  private final String wdId;
  @Expose
  private final String symbol;
  @Expose
  private final double realizedPnL;
  @Expose
  private final double unrealizedPnL;
  @Expose
  private final double pnl;
  @Expose
  private final double pctPnL;
  @Expose
  private final double currentValue;
  @Expose
  private final double nbShares;
  private final double cashFromExit;
  
  public UserAdviseAbsolutePerformanceCalculated(String cognitoIdentityId, String email, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId, String wdId,
      String symbol, double realizedPnL,double unrealizedPnL, double pnl, double pctPnL, double currentValue, double nbShares, double cashFromExit, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.cognitoIdentityId = cognitoIdentityId;
    this.wdId = wdId;
    this.symbol = symbol;
    this.cashFromExit = cashFromExit;
    this.realizedPnL = round(realizedPnL);
    this.unrealizedPnL = round(unrealizedPnL);
    this.pnl = round(pnl);
    this.pctPnL = round(pctPnL);
    this.currentValue = round(currentValue);
    this.nbShares = nbShares;
  }

  public UserAdviseAbsolutePerformanceCalculated(String cognitoIdentityId, String email, String adviserUsername, String fundName, InvestingMode investingMode, String adviseId,  String wdId,
      double realizedPnL,double unrealizedPnL, double pnl, double pctPnL, double currentValue, double nbShares, double cashFromExit, IssueType issueType) {
    super(email, adviserUsername, fundName, investingMode, adviseId, issueType);
    this.cognitoIdentityId = cognitoIdentityId;
    this.wdId = wdId;
    this.symbol = wdId;
    this.cashFromExit = cashFromExit;
    this.realizedPnL = round(realizedPnL);
    this.unrealizedPnL = round(unrealizedPnL);
    this.pnl = round(pnl);
    this.pctPnL = round(pctPnL);
    this.currentValue = round(currentValue);
    this.nbShares = nbShares;
  }
  
}
