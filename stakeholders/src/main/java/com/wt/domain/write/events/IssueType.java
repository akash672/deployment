package com.wt.domain.write.events;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import com.wt.domain.UserAdvise;
import com.wt.domain.UserEquityAdvise;
import com.wt.domain.UserMFAdvise;

import akka.actor.ActorContext;
import akka.actor.ActorRef;

public enum IssueType {

  Equity("Equity") {
    @Override
    public ActorRef child(ActorContext context, String childName, String name) {
      return context.actorOf(SpringExtProvider.get(context.system()).props("UserEquityAdviseActor", name), childName);
    }

    @Override
    public UserAdvise userAdvise() {
      return new UserEquityAdvise();
    }

  },
  MutualFund("MutualFund") {
    @Override
    public ActorRef child(ActorContext context, String childName, String name) {
      return context.actorOf(SpringExtProvider.get(context.system()).props("UserMFAdviseActor", name), childName);
    }

    @Override
    public UserAdvise userAdvise() {
      return new UserMFAdvise();
    }

  },
  Default("Default") {
    @Override
    public ActorRef child(ActorContext context, String childName, String name) {
      return null;
    }

    @Override
    public UserAdvise userAdvise() {
      return null;
    }
  };

  private String type;

  IssueType(String type) {
    this.type = type;
  }

  public String getIssueType() {
    return type;
  }

  public abstract ActorRef child(ActorContext context, String childName, String name);

  public abstract UserAdvise userAdvise();


}
