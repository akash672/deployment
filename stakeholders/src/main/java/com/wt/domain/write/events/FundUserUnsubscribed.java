package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class FundUserUnsubscribed extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private String identityId;
  private String basketOrderId;

  @Deprecated
  public FundUserUnsubscribed(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      String identityId, String basketOrderId) {
    this(username, adviserUsername, fundName, investingMode, basketOrderId);
  }

  public FundUserUnsubscribed(String username, String adviserUsername, String fundName, InvestingMode investingMode,
      String basketOrderId) {
    super(username, adviserUsername, fundName, investingMode);
    this.identityId = username;
    this.basketOrderId = basketOrderId;
  }

}
