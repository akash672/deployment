package com.wt.domain.write.commands;

import static com.wt.domain.InvestingMode.REAL;

import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.BrokerName;

import lombok.Getter;
import lombok.NonNull;

public class KycApproveCommand extends UnAuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;

  private @Getter final String clientCode;
  private @Getter final String panCard;
  private @Getter final String brokerCompany;
  private @Getter final String phoneNumber;
  private @Getter final String email;

  @JsonCreator
  public KycApproveCommand(@NonNull @JsonProperty("clientCode") String clientCode,
      @NonNull @JsonProperty("panCard") String panCard, @NonNull @JsonProperty("username") String username,
      @NonNull @JsonProperty("brokerCompany") String brokerCompany,
      @NonNull @JsonProperty("phoneNumber") String phoneNumber,
      @NonNull @JsonProperty("email") String email) {
    super(username);
    this.panCard = panCard;
    this.clientCode = clientCode;
    this.brokerCompany = brokerCompany;
    this.phoneNumber = phoneNumber;
    this.email = email;
  }

  public void validate() throws IllegalArgumentException {
    boolean validatePancard = Pattern.compile("^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$").matcher(getPanCard())
        .matches();
    boolean validateClientCode = (getClientCode() != null && getClientCode().isEmpty()) ? false : true;
    boolean validatePhoneNumber = (getPhoneNumber().length() == 10 && getPhoneNumber().matches("\\d+")) ? true : false;
    boolean validateEmail = email.isEmpty();
    if (brokerCompany == null)
      throw new IllegalArgumentException("Broker Name Cannot be empty");

    BrokerName broker = null;
    try {
      broker = BrokerName.fromBrokerName(brokerCompany, REAL);
    } catch (Exception e) {
    }
    if (broker == null)
      throw new IllegalArgumentException("Incorrect Broker Name Received");

    if (!validatePancard || !validateClientCode) {
      if (!validatePancard) {
        throw new IllegalArgumentException("Incorrect Pancard number submitted");
      } else if (!validateClientCode) {
        throw new IllegalArgumentException("Client code cannot be blank");
      } else if (!validatePhoneNumber) {
        throw new IllegalArgumentException("Phone Number needs to be 10 Digits only");
      } else if (!validateEmail) {
        throw new IllegalArgumentException("Email Address cannot be empty");
      }
    }
  }

}
