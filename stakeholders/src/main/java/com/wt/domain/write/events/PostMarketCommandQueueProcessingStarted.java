package com.wt.domain.write.events;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.stream.Collectors;

import lombok.Getter;

@Getter
public class PostMarketCommandQueueProcessingStarted extends UserEvent implements Serializable {

  private static final long serialVersionUID = 1L;
  private String listOfCommands;

  public PostMarketCommandQueueProcessingStarted(String username, ArrayDeque<AddToPostMarketCommandQueue> queue) {
    super(username);
    listOfCommands = queue.stream().map(action -> action.getClazz()).collect(Collectors.joining(","));
  }
}
