package com.wt.domain.write;

import java.io.Serializable;

import com.wt.domain.write.events.UserAdviseEvent;

import akka.persistence.fsm.AbstractPersistentFSM;
import akka.persistence.fsm.PersistentFSM.FSMState;


public abstract class UserAdviseActor<S extends FSMState, T extends Serializable, E extends UserAdviseEvent> extends AbstractPersistentFSM<S, T, E> {
 
}