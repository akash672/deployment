package com.wt.domain.write;

import static akka.http.javadsl.model.StatusCodes.BAD_REQUEST;
import static akka.http.javadsl.model.StatusCodes.NOT_FOUND;
import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;
import static akka.stream.ThrottleMode.shaping;
import static com.wt.domain.write.commands.RegisterInvestorFromUserPool.createCommand;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.CommonConstants.HYPHEN;
import static com.wt.utils.DateUtils.prettyDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;
import com.wt.domain.DeliveryIdWithSecurityType;
import com.wt.domain.EquityInstrument;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.UserRootState;
import com.wt.domain.write.commands.AuthenticatedUserCommand;
import com.wt.domain.write.commands.CalculateUserFundComposition;
import com.wt.domain.write.commands.CorporateEventUpdate;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.ExchangeOrderUpdateEventWithDeliveryId;
import com.wt.domain.write.commands.ExecutePostMarketUserCommands;
import com.wt.domain.write.commands.FetchAllUsersPendingFundRebalancingApprovals;
import com.wt.domain.write.commands.ForceSaveSnapshot;
import com.wt.domain.write.commands.FundNamesForWdIdSent;
import com.wt.domain.write.commands.GetBrokerName;
import com.wt.domain.write.commands.GetClientCode;
import com.wt.domain.write.commands.GetFolioNumber;
import com.wt.domain.write.commands.GetFundNamesWithWdId;
import com.wt.domain.write.commands.GetIdentityIdByUsername;
import com.wt.domain.write.commands.GetInvestorName;
import com.wt.domain.write.commands.GetPriceForSymbolAndClientCode;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.GetUserRoleStatus;
import com.wt.domain.write.commands.GetUserSecurityQuestions;
import com.wt.domain.write.commands.GetWdIdToInstrumentMap;
import com.wt.domain.write.commands.HasUserBeenOnBoarded;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.MessageReceptionConfirmation;
import com.wt.domain.write.commands.RegisterExistingUser;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.SendFollowingCommandToUserFund;
import com.wt.domain.write.commands.SubscribeToFundEventStream;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;
import com.wt.domain.write.commands.UpdateDividendEvent;
import com.wt.domain.write.commands.UpdateDividendEventWithFundNames;
import com.wt.domain.write.commands.UpdateFolioNumber;
import com.wt.domain.write.commands.UpdateKYCDetailsInRootActor;
import com.wt.domain.write.commands.UpdateSplitTypeEvent;
import com.wt.domain.write.commands.UpdateSplitTypeEventWithFundNames;
import com.wt.domain.write.commands.UpdateUserFundCompositionList;
import com.wt.domain.write.commands.UserSubscribedToThisFund;
import com.wt.domain.write.commands.WdIdToInstrumentMapSent;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.ExchangeOrderUpdateAcknowledged;
import com.wt.domain.write.events.ExchangeOrderUpdateEvent;
import com.wt.domain.write.events.ExistingUserOnboarded;
import com.wt.domain.write.events.Exists;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.Result;
import com.wt.domain.write.events.RootUserRegistered;
import com.wt.domain.write.events.RootUserRegisteredfromUserPool;
import com.wt.domain.write.events.SpecialCircumstancesBuy;
import com.wt.domain.write.events.SpecialCircumstancesSell;
import com.wt.domain.write.events.SpecificUserMergerDemergerPrimaryBuy;
import com.wt.domain.write.events.SpecificUserMergerDemergerSecondaryBuy;
import com.wt.domain.write.events.SpecificUserMergerDemergerSellEvent;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserHistoricalPerformanceCalculated;
import com.wt.domain.write.events.UserOnboardingStatus;
import com.wt.domain.write.events.UserSubscribedToFund;
import com.wt.domain.write.events.UserSubscribedToFundResponse;
import com.wt.onboarding.utils.CustomerOnboardingService;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.http.javadsl.model.StatusCodes;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.RecoveryCompleted;
import akka.persistence.SaveSnapshotFailure;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotMetadata;
import akka.persistence.SnapshotOffer;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;
import scala.collection.JavaConverters;

@Service("UserRootActor")
@Scope("prototype")
@Log4j
public class UserRootActor extends AbstractPersistentActor implements ParentActor<UserActor> {
  private UserRootState userRootState = new UserRootState();
  private Map<ExchangeOrderUpdateEvent, DeliveryIdWithSecurityType> exchangeOrderupdateToDeliveryId = new HashMap<ExchangeOrderUpdateEvent, DeliveryIdWithSecurityType>();
  private CustomerOnboardingService customerOnboardingService;
  private WDActorSelections actorSelections;
  private WDProperties wdProperties;
  private ActorMaterializer materializer;
  private ChildrenResultsAggregator resultAwaitingActor;
  private Map<String, EquityInstrument> wdIdToEquityInstrumentMap = new HashMap<>();
  
  @Autowired
  public void setActorSelections(WDActorSelections actorSelections) {
    this.actorSelections = actorSelections;
  }

  @Autowired
  public void setCustomerOnBoardingSource(CustomerOnboardingService customerOnboardingService) {
    this.customerOnboardingService = customerOnboardingService;
  }

  @Autowired
  public void setWdProperties(WDProperties wdProperties) {
    this.wdProperties = wdProperties;
  }
  
  @Autowired
  public void setMaterializer(ActorMaterializer materializer) { 
    this.materializer = materializer;
  }
  
  @PostConstruct
  public void initialize() {
    resultAwaitingActor = new ChildrenResultsAggregator(self(),
        new Done(getName(), "All users"), materializer);
  }

  @Override
  public Receive createReceive() {

    return receiveBuilder().match(UnAuthenticatedUserCommand.class, command -> {
      UnAuthenticatedUserCommand userCommand = (UnAuthenticatedUserCommand) command;
      log.info("Received unauthenticated command: " + command.getClass().getSimpleName() + ", Username: "
          + command.getUsername());
      try {
        if (isRegistrationCommand(userCommand)) {
            registerInvestorFromUserPool((RegisterInvestorFromUserPool) userCommand);
        } else {
          String username = userRootState.userEmailFrom(getRequiredUsername(userCommand.getUsername()));
          if (username != null)
            userCommand.setUsername(username);
          else {
            sender().tell(failed(username, "User does not exist.", NOT_FOUND), self());
            return;
          }
          if (userCommand instanceof KycApproveCommand || userCommand instanceof UpdateKycCommand) {
            boolean isUniqueClientCode = validateClientCode(userCommand);
            if (!isUniqueClientCode)
              sender().tell(failed(userCommand.getUsername(), "Client Code already exists.", PRECONDITION_FAILED),
                  self());
            else
              sendToUserActor(userCommand);
          }
          else if (shallForwardToUserActor(userCommand)) {
            sendToUserActor(userCommand);
          } else if (userCommand instanceof UserSubscribedToThisFund) {
            handleUserSubscribedToThisFund(userCommand);
          } else {
            log.error("Unhandled Command at User Root Actor: " + userCommand);
          }
        }
      } catch (IllegalArgumentException e) {
        sender().tell(failed(userCommand.getUsername(), e.getMessage(), BAD_REQUEST), self());
        return;
      }
    }).match(AuthenticatedUserCommand.class, command -> {
      String username = userRootState.userEmailFrom(getRequiredUsername(command.getUsername()));
      if (username == null) {
        if (command instanceof GetUserRoleStatus) {
          GetUserRoleStatus cmd = (GetUserRoleStatus) command;
          registerInvestorUsingGetRoleRequest(createCommand(cmd));
          forwardToChildren(context(), command, cmd.getUsername());
          return;
        } else {
          sender().tell(failed(username, "User does not exist.", NOT_FOUND), self());
          return;
        }
      }
      command.setUsername(username);
      log.info("Received authenticated command: " + command.getClass().getSimpleName() + ", Username: " + username);
      if (command instanceof HasUserBeenOnBoarded) {
        if (userRootState.onboardingStatusFrom(command.getUsername()) != null)
          sender().tell(
              new UserOnboardingStatus(username, "", userRootState.onboardingStatusFrom(command.getUsername())),
              self());
        else
          sender().tell(new UserOnboardingStatus(username, "", false), self());
      }

      forwardToChildren(context(), command, username);
    }).match(UserFundEvent.class, command -> {
      log.info("Received command: " + command);
      String username = userRootState.userEmailFrom(getRequiredUsername(command.getUsername()));
      if (username == null) {
        sender().tell(failed(username, "User does not exist.", NOT_FOUND), self());
        return;
      }
      forwardToChildren(context(), command, username);
    }).match(GetBrokerName.class, command -> {
      log.debug("Received command: " + command + " for username: " + command.getUsername());
      String username = userRootState.userEmailFrom(getRequiredUsername(command.getUsername()));
      if (username == null) {
        sender().tell(failed(username, "User does not exist.", NOT_FOUND), self());
        return;
      }
      forwardToChildren(context(), command, username);
    }).match(GetPriceForSymbolAndClientCode.class, command -> {
      log.info("Received command: " + command);
      sender().tell(
          customerOnboardingService.getPriceForClientCodeAndSymbol(command.getClientCode(), command.getSymbol()),
          self());
    }).match(GetClientCode.class, command -> {
      log.info("Received command: " + command);
      String username = userRootState.userEmailFrom(getRequiredUsername(command.getUsername()));
      if (username == null) {
        sender().tell(failed(username, "User does not exist.", NOT_FOUND), self());
        return;
      }
      forwardToChildren(context(), command, username);
    }).match(ForceSaveSnapshot.class, command -> {
        log.info("Received command " + command);
        if (sender() != null)
          sender().tell(new Done("ForceSaveSnapshot", "Received save snapshot message."), self());
        Source.from(userRootState.allUserEmails())
        .throttle(wdProperties.NUMBER_OF_CHILDREN_TO_RECOVER(),
            Duration.ofSeconds(wdProperties.CHILD_RECOVERY_FREQUENCY()),
            wdProperties.CHILD_RECOVERY_MAXIMUM_BURST(), shaping())
        .runForeach(username -> {
          forwardToChildren(context(), command, username);
        }, materializer);
    }).match(UserHistoricalPerformanceCalculated.class, command -> {
      log.info("Received command " + command);
      Consumer<Pair<ActorRef, List<Result>>> aggregatorFunction = pair -> {
        pair.getLeft().tell(new Done(getName(), "All"), self());
      };
      resultAwaitingActor.onResultReceived(command, aggregatorFunction);

    }).match(ExecutePostMarketUserCommands.class, command -> {
      log.info("Received command " + command);
      tellChildren(context(), self(), command, userRootState.allUserEmails().toArray(new String[0]));
    }).match(EODCompleted.class, command -> {
      log.info("Received command " + command);
      if (sender() != null)
        sender().tell(new Done("EOD", "Received EOD message."), self());
      updateWdIdtoInstrumentsMap();
      saveSnapshot(userRootState.copy());
    }).match(MarginReceived.class, command -> {
      if (command.getUsername() != null) {
        String username = userRootState.userEmailFrom(getRequiredUsername(command.getUsername()));
        if (username == null) {
          sender().tell(failed(username, "User does not exist.", NOT_FOUND), self());
          return;
        }
        log.debug("Received command: " + command + ", Username/Email: " + username);
        forwardToChildren(context(), command, username);
      } else {
        log.debug("Received NULL username in command: " + command);
        sender().tell(failed("", "Username does not exist.", NOT_FOUND), self());
        return;
      }
    }).match(UserSubscribedToFund.class, command -> {
      handleUserOnboardingCompleted(command);
    }).match(UpdateUserFundCompositionList.class, command -> {
      forwardToChildren(context(), command, command.getUserAdviseCompositeKey().getUsername());
    }).match(CalculateUserFundComposition.class, command -> {
      forwardToChildren(context(), command, command.getUserFundCompositeKey().getUsername());
    }).match(UpdateKYCDetailsInRootActor.class, command -> {
      userRootState.update(command);
    }).match(GetIdentityIdByUsername.class, command -> {
      sender().tell(userRootState.userEmailFrom(command.getUsername()), self());
    }).match(ExchangeOrderUpdateAcknowledged.class, command -> {
      ExchangeOrderUpdateAcknowledged exchangeOrderUpdateReceived = (ExchangeOrderUpdateAcknowledged) command;
      ActorSelection destination = (exchangeOrderUpdateReceived.getIssueType().equals(Equity.getIssueType())) ? actorSelections.equityOrderRootActor() : actorSelections.mforderRootActor();
      Long deliveryId = deliveryIdFromExchangeOrderupdate(exchangeOrderUpdateReceived.getExchangeOrderUpdate());
      destination.tell(new MessageReceptionConfirmation(deliveryId), self());
    }).match(ExchangeOrderUpdateEventWithDeliveryId.class, command -> {
      ExchangeOrderUpdateEventWithDeliveryId exchangeOrderUpdateEventWithDeliveryId = (ExchangeOrderUpdateEventWithDeliveryId) command;
      if (isNewExchangeOrderUpdate(exchangeOrderUpdateEventWithDeliveryId)) {
        forwardToChildren(context(), exchangeOrderUpdateEventWithDeliveryId.getExchangeOrderUpdateEvent(), userPersistenceIdFromPath(exchangeOrderUpdateEventWithDeliveryId.senderPath()));
        exchangeOrderupdateToDeliveryId.put(exchangeOrderUpdateEventWithDeliveryId.getExchangeOrderUpdateEvent(),
            new DeliveryIdWithSecurityType(exchangeOrderUpdateEventWithDeliveryId.getDeliveryId(),
                exchangeOrderUpdateEventWithDeliveryId.getSecurityType()));
      }
    }).match(UserResponseParameters.class, command -> {
      forwardToChildren(context(), command, command.getUsername());
    }).match(CorporateEventUpdate.class, command -> {
      handleCorporateEventUpdate(command);
    }).match(SpecificUserMergerDemergerSellEvent.class, command -> {
       forwardToChildren(context(), command, command.getUserName());
    }).match(SpecificUserMergerDemergerPrimaryBuy.class, command -> {
       forwardToChildren(context(), command, command.getUserName());
    }).match(SpecificUserMergerDemergerSecondaryBuy.class, command -> {
       forwardToChildren(context(), command, command.getUserName());
    }).match(SpecialCircumstancesBuy.class, command -> {
      forwardToChildren(context(), command, userRootState.userEmailFrom(getRequiredUsername(command.getInvestorUsername())));
    }).match(SpecialCircumstancesSell.class, command -> {
      forwardToChildren(context(), command, userRootState.userEmailFrom(getRequiredUsername(command.getInvestorUsername())));
    }).match(GetFolioNumber.class, command -> {
      forwardToChildren(context(), command, userRootState.userEmailFrom(getRequiredUsername(command.getUsername())));
    }).match(UpdateFolioNumber.class, command -> {
      forwardToChildren(context(), command, userRootState.userEmailFrom(getRequiredUsername(command.getUsername())));
    }).match(FetchAllUsersPendingFundRebalancingApprovals.class, command -> {
      handleFetchAwaitingResponseAcrossAllUsers(command);
    }).match(SendFollowingCommandToUserFund.class, sendUserFundFollowingCommand -> {
      handleSendUserFundFollowingCommand(sendUserFundFollowingCommand);
    }).match(GetSymbolForWdId.class, command -> {
      sendSymbolForWdId(command);
    }).match(SaveSnapshotSuccess.class, snapshotSuccess -> {
      handleSnapshotSuccess(snapshotSuccess);
    }).match(SaveSnapshotFailure.class, snapshotFailure -> {
      handleSnapshotFailure(snapshotFailure);
    }).build();
  }
  
  private void sendSymbolForWdId(GetSymbolForWdId command) {
    if (wdIdToEquityInstrumentMap.containsKey(command.getWdId())) {
      String symbol = wdIdToEquityInstrumentMap.get(command.getWdId()).getSymbol();
      sender().tell(new SymbolSentForWdId(command.getWdId(), symbol), self());
    } else{
      sender().tell(new SymbolSentForWdId(command.getWdId(), command.getWdId()), self());
    }
  }
  
  private void handleSnapshotFailure(SaveSnapshotFailure snapshotFailure) {
    SnapshotMetadata metadata = snapshotFailure.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has failed to persist with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()) + " due to reason " + snapshotFailure.cause());
  }

  private void handleSnapshotSuccess(SaveSnapshotSuccess snapshotSuccess) {
    SnapshotMetadata metadata = snapshotSuccess.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has sucessfully been persisted with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }
  
  private void handleSnapshotReceive(SnapshotOffer snapshotOffer) {
    SnapshotMetadata metadata = snapshotOffer.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has sucessfully been restored with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private void handleSendUserFundFollowingCommand(SendFollowingCommandToUserFund sendUserFundFollowingCommand) {
    if (sendUserFundFollowingCommand.getUsername().isEmpty())
      Source.from(userRootState.allUserEmails())
          .throttle(wdProperties.NUMBER_OF_CHILDREN_TO_RECOVER(),
              Duration.ofSeconds(wdProperties.CHILD_RECOVERY_FREQUENCY()),
              wdProperties.CHILD_RECOVERY_MAXIMUM_BURST(), shaping())
          .runForeach(username -> {
            forwardToChildren(context(), sendUserFundFollowingCommand, username);
          }, materializer);
    else {
      forwardToChildren(context(), sendUserFundFollowingCommand, sendUserFundFollowingCommand.getUsername());
    }
  }
  
  public long deliveryIdFromExchangeOrderupdate(ExchangeOrderUpdateEvent exchangeOrderUpdateEvent) {
    return exchangeOrderupdateToDeliveryId.containsKey(exchangeOrderUpdateEvent)
        ? exchangeOrderupdateToDeliveryId.get(exchangeOrderUpdateEvent).getDeliveryId()
        : -1L;
  }

  private void handleCorporateEventUpdate(CorporateEventUpdate command) {
    log.info("UserRootActor :Received  " + command.getEventType() + " type event for WdId " + command.getWdId());
    FundNamesForWdIdSent fundNamesForWdIdSent = null;
    try {
      Object result = askAndWaitForResult(actorSelections.activeInstrumentTracker(),
          new GetFundNamesWithWdId(command.getWdId()));

      if (result instanceof Failed) {
        log.error("Failed to get fund names with wdId " + command.getWdId() + " " + result);
        return;
      }
      fundNamesForWdIdSent = (FundNamesForWdIdSent) result;
    } catch (Exception e) {
      log.error("Exception while trying to get fund names with wdId " + command.getWdId(), e);
      return;
    }
    if (!fundNamesForWdIdSent.getFundNames().isEmpty()) {
      if (command instanceof UpdateDividendEvent) {
        UpdateDividendEvent updateDividendEvent = (UpdateDividendEvent) command;
        UpdateDividendEventWithFundNames eventWithFundNames = new UpdateDividendEventWithFundNames(
            updateDividendEvent.getWdId(), updateDividendEvent.getEventType(), updateDividendEvent.getEventDate(),
            updateDividendEvent.getDividendAmount(), fundNamesForWdIdSent.getFundNames());
        forwardCommandToAllChildren(eventWithFundNames);

      } else if (command instanceof UpdateSplitTypeEvent) {
        UpdateSplitTypeEvent updateSplitTypeEvent = (UpdateSplitTypeEvent) command;
        UpdateSplitTypeEventWithFundNames eventWithFundNames = new UpdateSplitTypeEventWithFundNames(
            updateSplitTypeEvent.getWdId(), updateSplitTypeEvent.getEventType(), updateSplitTypeEvent.getEventDate(),
            updateSplitTypeEvent.getAdjustPaf(), fundNamesForWdIdSent.getFundNames());
        forwardCommandToAllChildren(eventWithFundNames);
      }
    }
  }

  private boolean validateClientCode(UnAuthenticatedUserCommand userCommand) {
    if (userCommand instanceof KycApproveCommand) {
      KycApproveCommand kycApproveCommand = (KycApproveCommand) userCommand;
      String clientCode = kycApproveCommand.getClientCode();
      String brokerCompany = kycApproveCommand.getBrokerCompany();
      return isClientCodeBrokerNameUnique(clientCode, brokerCompany);
    } else {
      UpdateKycCommand kycUpdateEvent = (UpdateKycCommand) userCommand;
      String clientCode = getOldClientCode(kycUpdateEvent);
      String brokerCompany = getOldBrokerName(kycUpdateEvent);
      if ((kycUpdateEvent.getClientCode().isEmpty()) && (kycUpdateEvent.getBrokerCompany().isEmpty()))
        return true;
      else if (!(kycUpdateEvent.getClientCode().isEmpty()))
        clientCode = kycUpdateEvent.getClientCode();

      if (!(kycUpdateEvent.getBrokerCompany().isEmpty()))
        brokerCompany = kycUpdateEvent.getBrokerCompany();

      return isClientCodeBrokerNameUnique(clientCode, brokerCompany);
    }
  }

  private boolean isClientCodeBrokerNameUnique(String clientCode, String brokerCompany) {
    if (userRootState.containsClientCode(clientCode + HYPHEN + brokerCompany))
      return false;
    else
      return true;
  }

  private String getOldBrokerName(UpdateKycCommand kycUpdateCommand) {
    String username = userRootState.userEmailFrom(getRequiredUsername(kycUpdateCommand.getUsername()));
    String clientCodeWithBrokerName = userRootState.clientCodeFrom(username);
    return clientCodeWithBrokerName.split(HYPHEN)[1];
  }

  private String getOldClientCode(UpdateKycCommand kycUpdateCommand) {
    String username = userRootState.userEmailFrom(getRequiredUsername(kycUpdateCommand.getUsername()));
    String clientCodeWithBrokerName = userRootState.clientCodeFrom(username);
    return clientCodeWithBrokerName.split(HYPHEN)[0];
  }

  private String getRequiredUsername(String username) {
    if(username.equals("rhunadkat@outlook.com"))
      return "rhunadkat1";
    return username.split("@")[0];
  }

  private boolean isNewExchangeOrderUpdate(ExchangeOrderUpdateEventWithDeliveryId exchangeOrderUpdateEventWithDeliveryId) {
    return !exchangeOrderupdateToDeliveryId.containsValue(new DeliveryIdWithSecurityType(exchangeOrderUpdateEventWithDeliveryId.getDeliveryId(), 
        exchangeOrderUpdateEventWithDeliveryId.getSecurityType()));
  }

  private void handleFetchAwaitingResponseAcrossAllUsers(FetchAllUsersPendingFundRebalancingApprovals command) {
    JavaConverters.asJavaCollectionConverter(context().children()).asJavaCollection().stream()
    .forEach(actor -> actor.forward(command, context()));
  }

  private String userPersistenceIdFromPath(String senderPath) {
    return senderPath.split(CompositeKeySeparator)[1];
  }

  private boolean shallForwardToUserActor(UnAuthenticatedUserCommand userCommand) {
    return userCommand instanceof GetInvestorName || userCommand instanceof GetUserSecurityQuestions;
  }

  private void handleUserOnboardingCompleted(UserSubscribedToFund command) {
    String username = userRootState.userNameFromEmail(command.getUsername());
    if (userRootState.isUserOnboarded(username)) {
      ExistingUserOnboarded existingUserOnboarded = new ExistingUserOnboarded(username, false);
      persist(existingUserOnboarded, onboardingEvent -> {
         userRootState.update(existingUserOnboarded);
      });
    }
  }

  private void handleUserSubscribedToThisFund(UnAuthenticatedUserCommand userCommand) throws Exception {
    if (userRootState.doesUserExist(userCommand.getUsername())) {
      Result result = (Result) askChild(context(), userCommand, userCommand.getUsername());
      sender().tell(result, self());
    } else {
      UserSubscribedToThisFund userSubscribedToThisFund = (UserSubscribedToThisFund) userCommand;
      sender().tell(new UserSubscribedToFundResponse(userSubscribedToThisFund.getUsername(),
          userSubscribedToThisFund.getUsername(), userSubscribedToThisFund.getAdviserName(),
          userSubscribedToThisFund.getFundName(), userSubscribedToThisFund.getInvestingMode(), false), self());
    }
  }

  private void sendToUserActor(UnAuthenticatedUserCommand userCommand) {
    if (userRootState.doesUserExist(userCommand.getUsername())) {
      forwardToChildren(context(), userCommand, userCommand.getUsername());
    } else {
      sender().tell(failed(userCommand.getUsername(), "User does not exist", NOT_FOUND), self());
    }
  }

  private void registerInvestorFromUserPool(RegisterInvestorFromUserPool userCommand) throws Exception {
    if (userRegistered(userCommand.getUsername())) {
      sender().tell(Exists.exists(userCommand.getUsername()), self());
      return;
    }

    Result result = (Result) askChild(context(), userCommand, userCommand.getUsername());

    if (result instanceof Failed) {
      sender().tell((Failed) result, self());
      return;
    }

    RootUserRegisteredfromUserPool rootUserRegisteredfromUserPool = new RootUserRegisteredfromUserPool(
        userCommand.getUsername());
    RootUserRegistered rootUser;

    if (userCommand instanceof RegisterExistingUser)
      rootUser = new RootUserRegistered(userCommand.getUsername(), userCommand.getUsername(), true);
    else
      rootUser = new RootUserRegistered(userCommand.getUsername(), userCommand.getUsername(), false);

    persist(rootUser, evt -> {
      if (userCommand instanceof RegisterExistingUser)
        userRootState.update(userCommand);
    });

    persist(rootUserRegisteredfromUserPool, evt -> {
      userRootState.update(rootUserRegisteredfromUserPool);
      sender().tell(result, self());
    });
    return;
  }

  private void registerInvestorUsingGetRoleRequest(RegisterInvestorFromUserPool userCommand) throws Exception {
    Result result = (Result) askChild(context(), userCommand, userCommand.getUsername());

    if (result instanceof Failed) {
      sender().tell(Failed.failed(userCommand.getUsername(), "Failed to get user role for this user",
          StatusCodes.FAILED_DEPENDENCY), self());
      return;
    }

    RootUserRegisteredfromUserPool rootUser = new RootUserRegisteredfromUserPool(userCommand.getUsername());

    persist(rootUser, evt -> {
     userRootState.update(rootUser);
    });
    return;
  }
  
  private boolean userRegistered(String username) {
    return userRootState.doesUserExist(username);
  }

  private boolean isRegistrationCommand(UnAuthenticatedUserCommand command) {
    return (command instanceof RegisterExistingUser || command instanceof RegisterInvestorFromUserPool);
  }

  private void forwardCommandToAllChildren(Object obj) {
    List<String> usernames = new ArrayList<String>(userRootState.allUserEmails());
    forwardToChildren(context(), obj, usernames.toArray(new String[0]));
  }

  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(RootUserRegistered.class, command -> {
      log.info("Recover " + command);
      userRootState.update(command);
    }).match(SnapshotOffer.class, snapshotOffer -> {
      handleSnapshotReceive(snapshotOffer);
      userRootState = (UserRootState) snapshotOffer.snapshot();
    }).match(ExistingUserOnboarded.class, command -> {
      userRootState.update(command);
    }).match(RootUserRegisteredfromUserPool.class, command -> {
      userRootState.update(command);
    }).match(RecoveryCompleted.class, e -> {
      updateWdIdtoInstrumentsMap();
      Source.from(userRootState.allUserEmails())
      .throttle(wdProperties.NUMBER_OF_CHILDREN_TO_RECOVER(), Duration.ofSeconds(wdProperties.CHILD_RECOVERY_FREQUENCY()), wdProperties.CHILD_RECOVERY_MAXIMUM_BURST(), shaping())
      .runForeach(username -> {
        forwardToChildren(context(), new SubscribeToFundEventStream(username), username);
      }, materializer);
    }).build();
  }

  private void updateWdIdtoInstrumentsMap() {

    WdIdToInstrumentMapSent wdIdToInstrumentMapSent = null;
    boolean instrumentsMapReceived = false;
    int retryCounter = 1;
    while (!instrumentsMapReceived && retryCounter <= 5) {
      try {
        Object result = askAndWaitForResult(actorSelections.universeRootActor(), new GetWdIdToInstrumentMap());

        if (result instanceof Failed) {
          retryCounter++;
          log.warn("UserRootActor :Failed to get instruments by wdId from EquityActor. Retrying " + result);
          continue;
        }
        wdIdToInstrumentMapSent = (WdIdToInstrumentMapSent) result;
        wdIdToEquityInstrumentMap = wdIdToInstrumentMapSent.getWdIdtoInstruments();
        instrumentsMapReceived = true;
        log.info("UserRootActor : wdIdToInstrument Map updated");
      } catch (Exception e) {
        log.warn("UserRootActor :Exception while getting instruments by wdId from EquityActor. Retrying ", e);
        retryCounter++;
      }
    }
    if(retryCounter >5 )
      log.error("UserRootActor : Failed to get instruments map for universe actor after multiple attempts.");
  }
 
  @Override
  public String getChildBeanName() {
    return "UserActor";
  }

  @Override
  public String getName() {
    return self().path().name();
  }

  @Override
  public String persistenceId() {
    return "users-root";
  }

}
