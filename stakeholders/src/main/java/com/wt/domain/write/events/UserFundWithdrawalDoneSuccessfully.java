package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UserFundWithdrawalDoneSuccessfully extends UserFundEvent{
  
  private static final long serialVersionUID = 1L;
  private String identityId;
  private double totalWithdrawalAmount;

  public UserFundWithdrawalDoneSuccessfully(String username, String adviser, String fund, InvestingMode investingMode,
      String cognitoIdentityId, double totalWithdrawalAmount) {
    super(username, adviser, fund, investingMode);
    this.identityId = cognitoIdentityId;
    this.totalWithdrawalAmount = totalWithdrawalAmount;
  }

}
