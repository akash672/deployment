package com.wt.domain.write;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserPendingFundRebalancingApprovals implements Serializable {

  private static final long serialVersionUID = 1L;
  @Expose
  private String userEmail;
  @Expose
  private String phoneNumber;
  @Expose
  private List<UserResponseParametersFromFund> userResponseParametersFromFunds;

  public UserPendingFundRebalancingApprovals(@JsonProperty("id") String userEmail,
      @JsonProperty("phoneNumber") String phoneNumber,
      @JsonProperty("userResponseParametersFromFunds") List<UserResponseParametersFromFund> userResponseParametersFromFunds) {
    this.userEmail = userEmail;
    this.phoneNumber = phoneNumber;
    this.userResponseParametersFromFunds = userResponseParametersFromFunds;
  }

}