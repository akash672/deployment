package com.wt.domain.write.events;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.UserResponseParameters;

import lombok.Getter;
import lombok.ToString;

@Getter
@JsonIgnoreProperties({"id", "message"})
@ToString
public class AllRejectedUserFundAdvises extends Done {
  private List<UserResponseParameters> userResponseParameters;
  private static final long serialVersionUID = 1L;
  @JsonCreator
  public AllRejectedUserFundAdvises(@JsonProperty("id") String id, @JsonProperty("userResponseParameters") List<UserResponseParameters> userResponseParameters) {
    super(id, "");
    this.userResponseParameters = userResponseParameters;
  }
}
