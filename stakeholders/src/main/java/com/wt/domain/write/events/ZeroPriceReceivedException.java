package com.wt.domain.write.events;

public class ZeroPriceReceivedException extends Exception {
  private static final long serialVersionUID = 1L;
  
  public ZeroPriceReceivedException() {
  }
  
  public ZeroPriceReceivedException(String message) {
    super(message);
  }
}