package com.wt.domain.write.commands;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class UnsubscribeToFund extends AuthenticatedUserCommand {
  private static final long serialVersionUID = 1L;
  private String adviserUsername;
  private String fundName;
  private InvestingMode investingMode;
  @Setter private boolean isThisaPostMarketCommand = false;

  @JsonCreator
  public UnsubscribeToFund(@JsonProperty("adviserName") String adviserUsername,
      @JsonProperty("fundName") String fundName, @JsonProperty("username") String username,
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.investingMode = investingMode;
  }

  public static UnsubscribeToFund fromPostMarketCommand(Map<String, String> value) {
    return new UnsubscribeToFund(value.get("adviserUsername"), value.get("fundName"), value.get("username"),
        InvestingMode.valueOf(value.get("investingMode")));
  }
  

}
