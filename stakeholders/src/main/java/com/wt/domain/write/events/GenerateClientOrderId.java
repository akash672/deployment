package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseState;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class GenerateClientOrderId extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  private UserAdviseState userAdviseState;
  private String localOrderId;
  private String amcCode;
  private String txCode;
  private String dpcTxn;
  private String allRedeem;
  private String minRedeem;
  private String dpcFlag;

  public GenerateClientOrderId(String email, String fundName, String adviserName, InvestingMode investingMode,
      String adviseId, String localOrderId, String amcCode, UserAdviseState userAdviseState, IssueType issueType, String txCode, String dpcTxn,
      String allRedeem, String minRedeem, String dpcFlag) {
    super(email, adviserName, fundName, investingMode, adviseId, issueType);
    this.localOrderId = localOrderId;
    this.amcCode =amcCode;
    this.userAdviseState = userAdviseState;
    this.txCode = txCode;
    this.dpcTxn = dpcTxn;
    this.allRedeem = allRedeem;
    this.minRedeem = minRedeem;
    this.dpcFlag = dpcFlag;
  }

}
