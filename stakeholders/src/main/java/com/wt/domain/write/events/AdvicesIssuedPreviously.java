package com.wt.domain.write.events;

import java.util.List;

import com.wt.domain.FundConstituent;
import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=false)
public class AdvicesIssuedPreviously extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private List<FundConstituent> fundConstituents;
  private String clientCode;
  private String brokerName;
  private Purpose investingpurpose;
  
  public AdvicesIssuedPreviously(String email, String adviserUsername, String fundName, InvestingMode investingMode,
      List<FundConstituent> fundConstituents, String clientCode, String brokerName, Purpose investingpurpose) {
    super(email, adviserUsername, fundName, investingMode);
    this.fundConstituents = fundConstituents;
    this.investingpurpose = investingpurpose;
    this.brokerName = brokerName;
    this.clientCode = clientCode;
   }
  
}
