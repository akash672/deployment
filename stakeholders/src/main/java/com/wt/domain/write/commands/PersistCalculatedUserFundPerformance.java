package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.List;

import com.wt.domain.InvestingMode;
import com.wt.domain.write.events.UserAdvisePerformanceCalculated;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class PersistCalculatedUserFundPerformance implements Serializable{

  private static final long serialVersionUID = 1L;
  
  private String username;
  private String adviserUsername;
  private String fund;
  private double totalInvestment;
  private double returnPct;
  private double realizedPnl;
  private double unrealizedPnl;
  private double pnl;
  private double currentValue;
  private long date;
  private List<UserAdvisePerformanceCalculated> allAdvisePerformances;
  private InvestingMode investingMode;
  
  public PersistCalculatedUserFundPerformance(String username, String adviserUsername,
      String fund, InvestingMode investingMode, double totalInvestment, double returnPct, double realizedPnl, double unrealizedPnl, double pnl,
      double currentValue, long date, List<UserAdvisePerformanceCalculated> allAdvisePerformances) {
    this.username = username;
    this.adviserUsername = adviserUsername;
    this.fund = fund;
    this.investingMode = investingMode;
    this.totalInvestment = totalInvestment;
    this.returnPct = returnPct;
    this.realizedPnl = realizedPnl;
    this.unrealizedPnl = unrealizedPnl;
    this.pnl = pnl;
    this.currentValue = currentValue;
    this.date = date;
    this.allAdvisePerformances = allAdvisePerformances;
  }

}
