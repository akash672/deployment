package com.wt.domain.write.events;

import lombok.Getter;

@Getter
public class UserPasswordResetted extends UserEvent {

  private byte[] newPassword;
  private byte[] salt;
  private String otp;

  public UserPasswordResetted(String username, byte[] newPassword, byte[] salt, String otp) {
    super(username);
    this.newPassword = newPassword;
    this.salt = salt;
    this.otp = otp;
  }

  private static final long serialVersionUID = 1L;

}
