package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

public class AdvisePriceExpectedStateTimedOut extends UserAdviseEvent{

  private static final long serialVersionUID = 1L;

  public AdvisePriceExpectedStateTimedOut(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String adviseId, IssueType issueType) {
    super(username, adviserUsername, fundName, investingMode, adviseId, issueType);
  }

}
