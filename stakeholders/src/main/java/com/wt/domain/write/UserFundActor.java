package com.wt.domain.write;

import static akka.http.javadsl.model.StatusCodes.CONFLICT;
import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;
import static com.wt.domain.AdviseType.Issue;
import static com.wt.domain.BrokerName.fromBrokerName;
import static com.wt.domain.PriceWithPaf.noPrice;
import static com.wt.domain.Purpose.NONE;
import static com.wt.domain.Purpose.PARTIALWITHDRAWAL;
import static com.wt.domain.UserAcceptanceMode.Auto;
import static com.wt.domain.UserAcceptanceMode.Manual;
import static com.wt.domain.UserFund.getIssueType;
import static com.wt.domain.UserFundState.ADDITIONALSUMMIRRORING;
import static com.wt.domain.UserFundState.ADDITIONALSUMMIRRORINGWITHOUTTIMEOUT;
import static com.wt.domain.UserFundState.ARCHIVED;
import static com.wt.domain.UserFundState.MIRRORING;
import static com.wt.domain.UserFundState.MIRRORINGWITHOUTTIMEOUT;
import static com.wt.domain.UserFundState.SUBSCRIBED;
import static com.wt.domain.UserFundState.UNSUBSCRIBED;
import static com.wt.domain.UserFundState.UNSUBSCRIBING;
import static com.wt.domain.UserFundState.UNSUBSCRIBINGWITHOUTTIMEOUT;
import static com.wt.domain.UserFundState.WITHDRAWING;
import static com.wt.domain.UserResponseType.Approve;
import static com.wt.domain.UserResponseType.Archive;
import static com.wt.domain.UserResponseType.Awaiting;
import static com.wt.domain.UserResponseType.ExitFund;
import static com.wt.domain.write.events.BrokerNameSent.noBrokerInformationAvailable;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.domain.write.events.IssueType.Default;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.domain.write.events.UserFundAdditionalSumAdvice.fundLumpSumAdviceWithUndefinedExitParams;
import static com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail.fundAdviceWithUndefinedExitParams;
import static com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail.nonSpecialFundAdviceForANonOnboardedUser;
import static com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail.nonSpecialUserFundAdvice;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.CommonConstants.HYPHEN;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.prettyDate;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static java.lang.String.valueOf;
import static java.util.concurrent.TimeUnit.MINUTES;
import static scala.concurrent.duration.Duration.create;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;
import com.wt.domain.AdviseTransactionDetails;
import com.wt.domain.AdviseTransactionType;
import com.wt.domain.AdviseType;
import com.wt.domain.FundConstituent;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.UserAdvise;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserFund;
import com.wt.domain.UserFundState;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.write.commands.AcceptUserAcceptanceMode;
import com.wt.domain.write.commands.AddLumpsumToFundWithUserEmail;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CalculateUserAdviseComposition;
import com.wt.domain.write.commands.CalculateUserFundComposition;
import com.wt.domain.write.commands.CorporateEventUpdateWithFundNames;
import com.wt.domain.write.commands.CurrentUserFundValue;
import com.wt.domain.write.commands.ForceSaveSnapshot;
import com.wt.domain.write.commands.FundAdviseIssuedWithClientInformation;
import com.wt.domain.write.commands.GetBrokerName;
import com.wt.domain.write.commands.GetClientCode;
import com.wt.domain.write.commands.GetCurrentUserFundValue;
import com.wt.domain.write.commands.GetPendingUserFundApprovals;
import com.wt.domain.write.commands.GetPriceBasketFromTracker;
import com.wt.domain.write.commands.GetRejectedAdviseFromUserFund;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.GetUnsubscriptionSnapshotOfFund;
import com.wt.domain.write.commands.GetUserCurrentApprovalStatus;
import com.wt.domain.write.commands.GetUserFundStatus;
import com.wt.domain.write.commands.HasUserBeenOnBoarded;
import com.wt.domain.write.commands.PartialWithdrawalUnderway;
import com.wt.domain.write.commands.PriceSnapshotsFromPriceSource;
import com.wt.domain.write.commands.RetryUserAdviseWithClientInformation;
import com.wt.domain.write.commands.SellOrderDetails;
import com.wt.domain.write.commands.SendFundDetailsToUser;
import com.wt.domain.write.commands.SubscribeToFundWithUserEmail;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.commands.UnSubscribeToFundWithUserEmail;
import com.wt.domain.write.commands.UpdateAdviseTradeStatus;
import com.wt.domain.write.commands.UpdateCorporateActionsMap;
import com.wt.domain.write.commands.UpdateUserFundCash;
import com.wt.domain.write.commands.UpdateUserFundCompositionList;
import com.wt.domain.write.events.AdditionalSumAddedToUserFund;
import com.wt.domain.write.events.AdjustFundCashForInsufficientCapitalAdvise;
import com.wt.domain.write.events.AdvicesIssuedPreviously;
import com.wt.domain.write.events.AdviseTradesExecuted;
import com.wt.domain.write.events.AllAdviseOrdersRejected;
import com.wt.domain.write.events.AllAdviseTradeExecuted;
import com.wt.domain.write.events.AllPendingUserFundApprovals;
import com.wt.domain.write.events.AllRejectedUserFundAdvises;
import com.wt.domain.write.events.BrokerNameSent;
import com.wt.domain.write.events.CorporateActionSellDetails;
import com.wt.domain.write.events.CorporateMergerPrimaryBuyEvent;
import com.wt.domain.write.events.CorporateMergerSecondaryBuyEvent;
import com.wt.domain.write.events.CorporateMergerSellEvent;
import com.wt.domain.write.events.CurrentFundPortfolioValue;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.ExchangeOrderUpdateEvent;
import com.wt.domain.write.events.ExchangeOrderUpdateEventWithIssueType;
import com.wt.domain.write.events.ExecutedAdviseDetailsUpdated;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.FundAdviceTradeExecutedForWithdrawal;
import com.wt.domain.write.events.FundConstituentListResponseWithBrokerAuth;
import com.wt.domain.write.events.FundExited;
import com.wt.domain.write.events.FundLumpSumAdded;
import com.wt.domain.write.events.FundSubscribed;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.FundUserSubscribed;
import com.wt.domain.write.events.FundUserUnsubscribed;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.MergerDemergerEvent;
import com.wt.domain.write.events.MergerDemergerFundAdviseClosed;
import com.wt.domain.write.events.MergerDemergerPrimaryBuyIssued;
import com.wt.domain.write.events.MergerDemergerSecondaryBuyIssued;
import com.wt.domain.write.events.MergerExitTradesExecuted;
import com.wt.domain.write.events.MergerSellTradeExecuted;
import com.wt.domain.write.events.MirroringStateTimedOut;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.RequestedClientCode;
import com.wt.domain.write.events.SpecialCircumstancesBuy;
import com.wt.domain.write.events.SpecialCircumstancesBuyEvent;
import com.wt.domain.write.events.SpecialCircumstancesSell;
import com.wt.domain.write.events.UnsubscribingStateTimedOut;
import com.wt.domain.write.events.UpdateUserFundComposition;
import com.wt.domain.write.events.UpdatingCorporateActionsMap;
import com.wt.domain.write.events.UpdatingUserFundCash;
import com.wt.domain.write.events.UserAcceptanceModeUpdateReceived;
import com.wt.domain.write.events.UserAdviseDetails;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserAdviseTransactionReceived;
import com.wt.domain.write.events.UserAdviseWithdrawalOrderRejected;
import com.wt.domain.write.events.UserCloseAdviseOrderRejected;
import com.wt.domain.write.events.UserFundAdditionalSumAdvice;
import com.wt.domain.write.events.UserFundAdviceClosed;
import com.wt.domain.write.events.UserFundAdviceIssued;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.domain.write.events.UserFundAdviceRetryInitiated;
import com.wt.domain.write.events.UserFundAdviseIssuedInsufficientCapital;
import com.wt.domain.write.events.UserFundDividendReceived;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundSubscriptionDetails;
import com.wt.domain.write.events.UserFundWithdrawalDoneSuccessfully;
import com.wt.domain.write.events.UserOnboardingStatus;
import com.wt.domain.write.events.UserOpenAdviseOrderRejected.UserOpenAdviseOrderRejectedBuilder;
import com.wt.domain.write.events.UserResponseReceived;
import com.wt.domain.write.events.UserResponseReceived.UserResponseReceivedBuilder;
import com.wt.domain.write.events.WithdrawSumFromUserFundAttempted;
import com.wt.domain.write.events.WithdrawSumFromUserFundConfirmed;
import com.wt.domain.write.events.WithdrawingStateTimedOut;
import com.wt.onboarding.utils.CustomerOnboardingService;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.actor.InvalidActorNameException;
import akka.actor.Props;
import akka.http.javadsl.model.StatusCodes;
import akka.persistence.Recovery;
import akka.persistence.SaveSnapshotFailure;
import akka.persistence.SaveSnapshotSuccess;
import akka.persistence.SnapshotMetadata;
import akka.persistence.SnapshotSelectionCriteria;
import akka.persistence.fsm.AbstractPersistentFSM;
import lombok.extern.log4j.Log4j;
import scala.Option;

@SuppressWarnings("rawtypes")
@Service("UserFundActor")
@Scope("prototype")
@Log4j
public class UserFundActor extends AbstractPersistentFSM<UserFundState, UserFund, UserFundEvent>
    implements ParentActor<UserAdviseActor> {
  private String persistenceId;
  private WDActorSelections wdActorSelection;
  private CustomerOnboardingService customerOnboardingService;
  private Map<String, Double> adviseIdToShares = new HashMap<String, Double>();
  private WDProperties wdProperties;
  private boolean isRecoveryCompleted = false;  

  @Autowired
  public void setWdActorSelection(@Qualifier("WDActors") WDActorSelections wdActorSelection) {
    this.wdActorSelection = wdActorSelection;
  }
  
  @Autowired
  public void setCustomerOnBoardingSource(CustomerOnboardingService customerOnboardingService) {
    this.customerOnboardingService = customerOnboardingService;
  }

  public UserFundActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
    startWith(UNSUBSCRIBED, new UserFund());
    
    when(UNSUBSCRIBED,
        matchEvent(SubscribeToFundWithUserEmail.class, 
            (subscribeToFund, userFund) ->
        {
          return stay().
          applying(new FundUserSubscribed(
              subscribeToFund.getUsername(),
              subscribeToFund.getAdviserUsername(), 
              subscribeToFund.getFundName(),
              subscribeToFund.getInvestingMode(),
              subscribeToFund.getLumpSumAmount(),
              subscribeToFund.getSipAmount(), 
              subscribeToFund.getSipDate(), 
              DateUtils.currentTimeInMillis(), 
              subscribeToFund.getRiskProfile(), 
              subscribeToFund.getInceptionDate(),
              subscribeToFund.getInvestingPurpose(),
              subscribeToFund.getBasketOrderId())).
          andThen(exec(fund -> {
                if (getOnboardingProcessStatus(subscribeToFund.getUsername()))
                 self().tell(customerOnboardingService.getFundCompositionForUser(subscribeToFund.getUserEmail()), self());
                else {
                  self().tell(new FundConstituentListResponseWithBrokerAuth(subscribeToFund.getFundConstituentListResponse(),
                      subscribeToFund.getAuthInformation()), self());
                 
                }
          }));
        }).
        event(FundConstituentListResponseWithBrokerAuth.class, 
            (fundConstituentListWrapper, userFund) -> {
              return (fundConstituentListWrapper.getFundListResponse().getFundConstituentList().size() == 0) ? false : true;
            }, 
            (fundConstituentListWrapper, userFund) -> 
        {
          BrokerNameSent brokerNameSent = getBrokerNameOfTheInvestorWith(userFund.getUserFundCompositeKey().getUsername(),
              userFund.getUserFundCompositeKey().getInvestingMode());
          UserFundState userFundState = identifyTransitionState(fundConstituentListWrapper.getFundListResponse().getFundConstituentList()) ? MIRRORING : MIRRORINGWITHOUTTIMEOUT;
          RequestedClientCode requestedClientCode = getClientCodeFromUserRootActor(userFund.getUserFundCompositeKey().getUsername());
          return goTo(userFundState).
          applying(new AdvicesIssuedPreviously(
              userFund.getUsername(),
              userFund.getUserFundCompositeKey().getAdviser(),
              userFund.getUserFundCompositeKey().getFund(),
              userFund.getUserFundCompositeKey().getInvestingMode(),
              fundConstituentListWrapper.getFundListResponse().getFundConstituentList(),
              requestedClientCode.getClientCode(),
              brokerNameSent.getBrokerName().getBrokerName(),
              userFund.getInvestingPurpose())).
          andThen(exec(fund -> {
            boolean userOnBoardingStatus = getOnboardingProcessStatus(fund.getUserFundCompositeKey().getUsername());
            int i=1;
                    for (UserAdvise userAdvise : fund.getMirroringAdvises()) {
                      UserFundAdviceIssuedWithUserEmail fundAdviceIssuedWithUserEmail = fundAdviceWithUndefinedExitParams(
                          userAdvise.getUserAdviseCompositeKey().getUsername(), 
                          userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                          userAdvise.getUserAdviseCompositeKey().getFund(), 
                          userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                          userAdvise.getAdviseId(), 
                          userAdvise.getToken(), 
                          userAdvise.getToken(),
                          "Replicating previously issued advices",
                          userAdvise.getIssueAdviceAbsoluteAllocation(), 
                          userAdvise.getIssueAdviseAllocationValue(),
                          userAdvise.getAverageEntryPrice(),
                          userOnBoardingStatus,
                          userAdvise.getBrokerName(),
                          userAdvise.getClientCode(),
                          getIssueType(userAdvise.getToken()),
                          false,
                          userFund.getBasketOrderId()+CompositeKeySeparator+valueOf(i),
                          fundConstituentListWrapper.getBrokerAuthInformation());
                      forwardToChildren(context(), self(), 
                          fundAdviceIssuedWithUserEmail, 
                          new UserAdviseCompositeKey( fundAdviceIssuedWithUserEmail.getAdviseId(),
                                                      fundAdviceIssuedWithUserEmail.getUsername(), 
                                                      fundAdviceIssuedWithUserEmail.getFundName(),
                                                      fundAdviceIssuedWithUserEmail.getAdviserUsername(),
                                                      fundAdviceIssuedWithUserEmail.getInvestingMode()).persistenceId());
                      i++;
                    }
                })
              );
        }).
        event(FundConstituentListResponseWithBrokerAuth.class, 
            (fundConstituentListWarpper, userFund) -> {
              return (fundConstituentListWarpper.getFundListResponse().getFundConstituentList().size() == 0) ? true : false;
            }, 
            (fundConstituentListWarpper, userFund) -> 
        {
          return goTo(SUBSCRIBED).
              applying(new FundSubscribed(
                  userFund.getUsername(), " subscribed", 
                  userFund.getUserFundCompositeKey().getFund(), 
                  userFund.getUserFundCompositeKey().getAdviser(),
                  userFund.getUserFundCompositeKey().getInvestingMode(),
                  userFund.getRequestedInvestment())).
              andThen(exec(fund -> {
                wdActorSelection.userRootActor().tell(new FundSubscribed(fund.getUsername(), " subscribed ",
                    fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getAdviser(),
                    fund.getUserFundCompositeKey().getInvestingMode(), fund.getRequestedInvestment()),
                    self());
              }));
        })
        );
    
        when(MIRRORING, create(15, MINUTES),
            matchEvent(UnSubscribeToFundWithUserEmail.class, 
              (unSubscribeToFundWithUserEmail, userFund) -> 
          {
            return 
            stay().replying(failed(userFund.getUsername(),
                " cannot unsubscribe to " + userFund.getUserFundCompositeKey().getFund() + " while mirroring the fund.",
                StatusCodes.CONFLICT));
          }).
          event(UserFundAdviseIssuedInsufficientCapital.class,
              (userFundAdviseIssuedInsufficientCapital, userFund) -> {
                boolean adviseDetailsNotReceived = false;
                for(UserAdvise advise: userFund.getMirroringAdvises()){
                  if(advise.getAdviseId().equals(userFundAdviseIssuedInsufficientCapital.getAdviseId())) {
                    adviseDetailsNotReceived = true;
                    break;
                  }
                }
               return adviseDetailsNotReceived ? true : false; 
              }, (userFundAdviseIssuedInsufficientCapital, userFund) ->
          {
               return stay().
               applying(new AdjustFundCashForInsufficientCapitalAdvise(
                      userFund.getUsername(), 
                      userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode(),
                      userFundAdviseIssuedInsufficientCapital.getAdviseId(),
                      userFundAdviseIssuedInsufficientCapital.getAdjustAllocation(),
                      stateName()
                      )).
               andThen(exec(fund -> {
                 if (fund.getMirroringAdviseIdsRejected().size() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseOrdersRejected(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 else if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseTradeExecuted(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 context().stop(findChild(context(), new UserAdviseCompositeKey(
                     userFundAdviseIssuedInsufficientCapital.getAdviseId(), 
                     userFundAdviseIssuedInsufficientCapital.getUsername(), 
                     userFundAdviseIssuedInsufficientCapital.getFundName(), 
                     userFundAdviseIssuedInsufficientCapital.getAdviserUsername(),
                     userFundAdviseIssuedInsufficientCapital.getInvestingMode()).persistenceId(),
                     userFundAdviseIssuedInsufficientCapital.getIssueType()));
                      }));
        }).
          event(UserOpenAdviseOrderRejectedBuilder.class, 
              (userAdviseOrderRejected, userFund) -> {
                boolean adviseDetailsNotReceived = false;
                for(UserAdvise advise: userFund.getMirroringAdvises()){
                  if(advise.getAdviseId().equals(userAdviseOrderRejected.getAdviseId())){
                    adviseDetailsNotReceived = true;
                    break;
                  }
                }
               return adviseDetailsNotReceived ? true : false; 
              }, (userAdviseOrderRejected, userFund) ->
              
          {
               return stay().
               applying(userAdviseOrderRejected.withUserFundState(stateName()).build()).
               andThen(exec(fund -> {
                 if (fund.getMirroringAdviseIdsRejected().size() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseOrdersRejected(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 else if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseTradeExecuted(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 context().stop(findChild(context(), new UserAdviseCompositeKey(
                     userAdviseOrderRejected.getAdviseId(), 
                     userAdviseOrderRejected.getUsername(), 
                     userAdviseOrderRejected.getFundName(), 
                     userAdviseOrderRejected.getAdviserUsername(),
                     userAdviseOrderRejected.getInvestingMode()).persistenceId(),
                     userAdviseOrderRejected.getIssueType()));
               }));
        }).
        event(AdviseTradesExecuted.class, 
            (adviseTradesExecuted, userFund) -> {
              boolean adviseDetailsNotReceived = true;
              if(userFund.getMirroringAdviseIdsTraded().contains(adviseTradesExecuted.getAdviseId())){
                adviseDetailsNotReceived = false;
              }
             return adviseDetailsNotReceived ? true : false; 
            }, (adviseTradesExecuted, userFund) ->
            
        {
             return stay().
             applying(new ExecutedAdviseDetailsUpdated(
                    userFund.getUsername(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getFund(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    adviseTradesExecuted.getAdviseId(),
                    adviseTradesExecuted.getAllocationValue(),
                    adviseTradesExecuted.getTradedValue()
                    )).
             andThen(exec(fund -> {
               if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                 self().forward(new AllAdviseTradeExecuted(
                     fund.getUsername(), 
                     userFund.getUserFundCompositeKey().getAdviser(),
                     userFund.getUserFundCompositeKey().getFund(), 
                     userFund.getUserFundCompositeKey().getInvestingMode()), context());
               }
                    }));
      }).
        event(AllAdviseTradeExecuted.class, 
            (allAdviseTradeExecuted, userFund) -> {
            double fundSubscriptionAmount = userFund.getInterimTotalInvestment() + userFund.getCashComponent();
            return goTo(SUBSCRIBED).
            applying(new FundSubscribed(
                    userFund.getUsername(), " subscribed", 
                    userFund.getUserFundCompositeKey().getFund(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    fundSubscriptionAmount)).
            andThen(exec(fund -> {
              wdActorSelection.userRootActor().tell(new FundSubscribed(fund.getUsername(), " subscribed",
                  fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getAdviser(),
                  fund.getUserFundCompositeKey().getInvestingMode(), 
                  fundSubscriptionAmount), self());
              self().tell(new CalculateUserFundComposition(fund.getUserFundCompositeKey()), self());
            }));
                    }).
        event(AllAdviseOrdersRejected.class, 
            (allAdviseOrdersRejected, userFund) -> {
            double fundSubscriptionAmount = userFund.getInterimTotalInvestment() + userFund.getCashComponent();
            return goTo(SUBSCRIBED).
            applying(new FundSubscribed(
                    userFund.getUsername(), " subscribed", 
                    userFund.getUserFundCompositeKey().getFund(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    fundSubscriptionAmount)).
            andThen(exec(fund -> {
              wdActorSelection.userRootActor().tell(new FundSubscribed(fund.getUsername(), " subscribed",
                  fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getAdviser(),
                  fund.getUserFundCompositeKey().getInvestingMode(),
                  fundSubscriptionAmount), self());
              self().tell(new UnSubscribeToFundWithUserEmail(userFund.getUsername(),
                  userFund.getUserFundCompositeKey().getFund(),
                  userFund.getUserFundCompositeKey().getInvestingMode(),
                  userFund.getUsername(),
                  userFund.getUsername(),
                  userFund.getBasketOrderId(),
                  BrokerAuthInformation.withNoBrokerAuth()),
                  self());
            }));
                    }).
        event(StateTimeout$.class, 
            (stateTimeout, userFund) -> {
              return (userFund.getAdviseIdsProcessedCount() == userFund.getMirroringAdvises().size());
            }, 
             (stateTimeout, userFund) -> {
               if (userFund.getMirroringAdviseIdsRejected().size() == userFund.getMirroringAdvises().size())
                 self().forward(new AllAdviseOrdersRejected(
                     userFund.getUsername(), 
                     userFund.getUserFundCompositeKey().getAdviser(),
                     userFund.getUserFundCompositeKey().getFund(),
                     userFund.getUserFundCompositeKey().getInvestingMode()), context());
               else 
                self().forward(new AllAdviseTradeExecuted(
                   userFund.getUsername(), 
                   userFund.getUserFundCompositeKey().getAdviser(),
                   userFund.getUserFundCompositeKey().getFund(),
                   userFund.getUserFundCompositeKey().getInvestingMode()), context());
              return stay();
            }).
        event(StateTimeout$.class,
            (stateTimeout, userFund) -> {
              return !(userFund.getAdviseIdsProcessedCount() == userFund.getMirroringAdvises().size());
            },
             (stateTimeout, userFund) -> {
              return stay().
              applying(new MirroringStateTimedOut(
                      userFund.getUsername(),
                      userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode()
                      )).
               andThen(exec(fund -> {
                    for(UserAdvise userAdvise: userFund.getMirroringAdvises()){
                      String id = "" + new UserAdviseCompositeKey(
                        userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                        userFund.getUserFundCompositeKey().getUsername(), 
                        userFund.getUserFundCompositeKey().getFund(), 
                        userFund.getUserFundCompositeKey().getAdviser(),
                        userFund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
                      IssueType issueType = getIssueType(userAdvise.getToken());
                      if(!userFund.getMirroringAdviseIdsTraded().contains(userAdvise.getUserAdviseCompositeKey().getAdviseId())
                          || !userFund.getMirroringAdviseIdsRejected().contains(userAdvise.getUserAdviseCompositeKey().getAdviseId()))
                        forwardToChildren(context(), self(), new UpdateAdviseTradeStatus(issueType), id);
                     }
                  }));
                }));
        
        when(MIRRORINGWITHOUTTIMEOUT,
            matchEvent(UnSubscribeToFundWithUserEmail.class, (unSubscribeToFundWithUserEmail, userFund) -> {
              return stay().replying(failed(userFund.getUsername(),
                  " cannot unsubscribe to " + userFund.getUserFundCompositeKey().getFund() + " while mirroring the fund.",
                  StatusCodes.CONFLICT));
            }).
            event(UserFundAdviseIssuedInsufficientCapital.class, (userFundAdviseIssuedInsufficientCapital, userFund) -> {
              boolean adviseDetailsNotReceived = false;
              for (UserAdvise advise : userFund.getMirroringAdvises()) {
                if (advise.getAdviseId().equals(userFundAdviseIssuedInsufficientCapital.getAdviseId())) {
                  adviseDetailsNotReceived = true;
                  break;
                }
              }
              return adviseDetailsNotReceived ? true : false;
            }, (userFundAdviseIssuedInsufficientCapital, userFund) -> {
              return stay()
                  .applying(new AdjustFundCashForInsufficientCapitalAdvise(userFund.getUsername(),
                  userFund.getUserFundCompositeKey().getAdviser(), userFund.getUserFundCompositeKey().getFund(),
                  userFund.getUserFundCompositeKey().getInvestingMode(),
                  userFundAdviseIssuedInsufficientCapital.getAdviseId(),
                  userFundAdviseIssuedInsufficientCapital.getAdjustAllocation(), stateName()))
                  .andThen(exec(fund -> {
                    if (fund.getMirroringAdviseIdsRejected().size() == fund.getMirroringAdvises().size()) {
                      self().forward(new AllAdviseOrdersRejected(
                          fund.getUsername(), 
                          fund.getUserFundCompositeKey().getAdviser(),
                          fund.getUserFundCompositeKey().getFund(),
                          fund.getUserFundCompositeKey().getInvestingMode()), context());
                    }
                    else if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                      self().forward(
                          new AllAdviseTradeExecuted(fund.getUsername(), fund.getUserFundCompositeKey().getAdviser(),
                              fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getInvestingMode()),
                          context());
                    }
                    context().stop(findChild(context(),
                        new UserAdviseCompositeKey(userFundAdviseIssuedInsufficientCapital.getAdviseId(),
                            userFundAdviseIssuedInsufficientCapital.getUsername(),
                            userFundAdviseIssuedInsufficientCapital.getFundName(),
                            userFundAdviseIssuedInsufficientCapital.getAdviserUsername(),
                            userFundAdviseIssuedInsufficientCapital.getInvestingMode()).persistenceId(),
                        userFundAdviseIssuedInsufficientCapital.getIssueType()));
                  }));
            }).
            event(UserOpenAdviseOrderRejectedBuilder.class, (userAdviseOrderRejected, userFund) -> {
              boolean adviseDetailsNotReceived = false;
              for (UserAdvise advise : userFund.getMirroringAdvises()) {
                if (advise.getAdviseId().equals(userAdviseOrderRejected.getAdviseId())) {
                  adviseDetailsNotReceived = true;
                  break;
                }
              }
              return adviseDetailsNotReceived ? true : false;
            }, (userAdviseOrderRejected, userFund) ->

            {
              return stay().applying(userAdviseOrderRejected.withUserFundState(stateName()).build()).andThen(exec(fund -> {
                if (fund.getMirroringAdviseIdsRejected().size() == fund.getMirroringAdvises().size()) {
                  self().forward(new AllAdviseOrdersRejected(
                      fund.getUsername(), 
                      fund.getUserFundCompositeKey().getAdviser(),
                      fund.getUserFundCompositeKey().getFund(),
                      fund.getUserFundCompositeKey().getInvestingMode()), context());
                }
                else if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                  self().forward(
                      new AllAdviseTradeExecuted(fund.getUsername(), fund.getUserFundCompositeKey().getAdviser(),
                          fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getInvestingMode()),
                      context());
                }
                context().stop(findChild(context(),
                    new UserAdviseCompositeKey(userAdviseOrderRejected.getAdviseId(), userAdviseOrderRejected.getUsername(),
                        userAdviseOrderRejected.getFundName(), userAdviseOrderRejected.getAdviserUsername(),
                        userAdviseOrderRejected.getInvestingMode()).persistenceId(),
                    userAdviseOrderRejected.getIssueType()));
              }));
            }).event(AdviseTradesExecuted.class, (adviseTradesExecuted, userFund) -> {
              boolean adviseDetailsNotReceived = true;
              if (userFund.getMirroringAdviseIdsTraded().contains(adviseTradesExecuted.getAdviseId())) {
                adviseDetailsNotReceived = false;
              }
              return adviseDetailsNotReceived ? true : false;
            }, (adviseTradesExecuted, userFund) ->

            {
              return stay()
                  .applying(new ExecutedAdviseDetailsUpdated(userFund.getUsername(),
                      userFund.getUserFundCompositeKey().getAdviser(), userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode(), adviseTradesExecuted.getAdviseId(),
                      adviseTradesExecuted.getAllocationValue(), adviseTradesExecuted.getTradedValue()))
                  .andThen(exec(fund -> {
                    if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                      self().forward(new AllAdviseTradeExecuted(fund.getUsername(),
                          userFund.getUserFundCompositeKey().getAdviser(), userFund.getUserFundCompositeKey().getFund(),
                          userFund.getUserFundCompositeKey().getInvestingMode()), context());
                    }
                  }));
            }).event(AllAdviseTradeExecuted.class, (allAdviseTradeExecuted, userFund) -> {
              double totalInvestment = userFund.getInterimTotalInvestment() + userFund.getCashComponent();
              return goTo(SUBSCRIBED)
                  .applying(new FundSubscribed(userFund.getUsername(), " subscribed",
                      userFund.getUserFundCompositeKey().getFund(), userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getInvestingMode(), 
                      totalInvestment))
                  .andThen(exec(fund -> {
                    wdActorSelection.userRootActor()
                        .tell(new FundSubscribed(fund.getUsername(), " subscribed", fund.getUserFundCompositeKey().getFund(),
                            fund.getUserFundCompositeKey().getAdviser(), fund.getUserFundCompositeKey().getInvestingMode(),
                            totalInvestment), self());
                    self().tell(new CalculateUserFundComposition(fund.getUserFundCompositeKey()), self());
                  }));
            }));
    
        when(ADDITIONALSUMMIRRORING, create(15, MINUTES),
            matchEvent(UnSubscribeToFundWithUserEmail.class, 
              (unSubscribeToFundWithUserEmail, userFund) -> 
          {
            return 
                stay().
                replying(failed(userFund.getUsername(), " cannot unsubscribe to "
                    + userFund.getUserFundCompositeKey().getFund() + " while lumpsum is being added to the fund.", CONFLICT));
          }).
          event(UserFundAdviseIssuedInsufficientCapital.class,
              (userFundAdviseIssuedInsufficientCapital, userFund) -> {
                boolean adviseDetailsNotReceived = false;
                for(UserAdvise advise: userFund.getMirroringAdvises()){
                  if(advise.getAdviseId().equals(userFundAdviseIssuedInsufficientCapital.getAdviseId())){
                    adviseDetailsNotReceived = true;
                    break;
                  }
                }
               return adviseDetailsNotReceived ? true : false; 
              }, (userFundAdviseIssuedInsufficientCapital, userFund) ->
          {
               return stay().
               applying(new AdjustFundCashForInsufficientCapitalAdvise(
                      userFund.getUsername(), 
                      userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode(),
                      userFundAdviseIssuedInsufficientCapital.getAdviseId(),
                      userFundAdviseIssuedInsufficientCapital.getAdjustAllocation(),
                      stateName()
                      )).
               andThen(exec(fund -> {
                 if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseTradeExecuted(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 context().stop(findChild(context(), new UserAdviseCompositeKey(
                     userFundAdviseIssuedInsufficientCapital.getAdviseId(), 
                     userFundAdviseIssuedInsufficientCapital.getUsername(), 
                     userFundAdviseIssuedInsufficientCapital.getFundName(), 
                     userFundAdviseIssuedInsufficientCapital.getAdviserUsername(),
                     userFundAdviseIssuedInsufficientCapital.getInvestingMode()).persistenceId(),
                     userFundAdviseIssuedInsufficientCapital.getIssueType()));
                      }));
        }).
          event(UserOpenAdviseOrderRejectedBuilder.class, 
              (userAdviseOrderRejected, userFund) -> {
                boolean adviseDetailsNotReceived = false;
                for(UserAdvise advise: userFund.getMirroringAdvises()){
                  if(advise.getAdviseId().equals(userAdviseOrderRejected.getAdviseId())){
                    adviseDetailsNotReceived = true;
                    break;
                  }
                }
               return adviseDetailsNotReceived ? true : false; 
              }, (userAdviseOrderRejected, userFund) ->
              
          {
               return stay().
               applying(userAdviseOrderRejected.withUserFundState(stateName()).build()).
               andThen(exec(fund -> {
                 if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseTradeExecuted(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 context().stop(findChild(context(), new UserAdviseCompositeKey(
                     userAdviseOrderRejected.getAdviseId(), 
                     userAdviseOrderRejected.getUsername(), 
                     userAdviseOrderRejected.getFundName(), 
                     userAdviseOrderRejected.getAdviserUsername(),
                     userAdviseOrderRejected.getInvestingMode()).persistenceId(),
                     userAdviseOrderRejected.getIssueType()));
               }));
        }).
        event(AdviseTradesExecuted.class, 
            (adviseTradesExecuted, userFund) -> {
              boolean adviseDetailsNotReceived = true;
              if(userFund.getMirroringAdviseIdsTraded().contains(adviseTradesExecuted.getAdviseId())){
                adviseDetailsNotReceived = false;
              }
             return adviseDetailsNotReceived ? true : false; 
            }, (adviseTradesExecuted, userFund) ->
            
        {
             return stay().
             applying(new ExecutedAdviseDetailsUpdated(
                    userFund.getUsername(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getFund(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    adviseTradesExecuted.getAdviseId(),
                    adviseTradesExecuted.getAllocationValue(),
                    adviseTradesExecuted.getTradedValue()
                    )).
             andThen(exec(fund -> {
               if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                 self().forward(new AllAdviseTradeExecuted(
                     fund.getUsername(), 
                     userFund.getUserFundCompositeKey().getAdviser(),
                     userFund.getUserFundCompositeKey().getFund(), 
                     userFund.getUserFundCompositeKey().getInvestingMode()), context());
               }
                    }));
      }).
        event(AllAdviseTradeExecuted.class, 
            (allAdviseTradeExecuted, userFund) -> {
            double lumpSumAdded = userFund.getInterimTotalInvestment() + userFund.getCashComponent();
            return goTo(SUBSCRIBED).
            applying(new FundLumpSumAdded(
                    userFund.getUsername(), " lumpsum added", 
                    userFund.getUserFundCompositeKey().getFund(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    lumpSumAdded)).
            andThen(exec(fund -> {
              wdActorSelection.userRootActor().tell(new FundLumpSumAdded(fund.getUsername(), " lumpsum added",
                  fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getAdviser(),
                  fund.getUserFundCompositeKey().getInvestingMode(), 
                  lumpSumAdded),
                  self());
              self().tell(new CalculateUserFundComposition(fund.getUserFundCompositeKey()), self());
            }));
                    }).
        event(StateTimeout$.class, 
            (stateTimeout, userFund) -> {
              return (userFund.getAdviseIdsProcessedCount() == userFund.getMirroringAdvises().size());
            }, 
             (stateTimeout, userFund) -> {
              self().forward(new AllAdviseTradeExecuted(
                 userFund.getUsername(), 
                 userFund.getUserFundCompositeKey().getAdviser(),
                 userFund.getUserFundCompositeKey().getFund(),
                 userFund.getUserFundCompositeKey().getInvestingMode()), context());
              return stay();
            }).
        event(StateTimeout$.class,
            (stateTimeout, userFund) -> {
              return !(userFund.getAdviseIdsProcessedCount() == userFund.getMirroringAdvises().size());
            },
             (stateTimeout, userFund) -> {
              return stay().
              applying(new MirroringStateTimedOut(
                      userFund.getUsername(),
                      userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode()
                      )).
               andThen(exec(fund -> {
                    for(UserAdvise userAdvise: userFund.getMirroringAdvises()){
                      String id = "" + new UserAdviseCompositeKey(
                        userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                        userFund.getUserFundCompositeKey().getUsername(), 
                        userFund.getUserFundCompositeKey().getFund(), 
                        userFund.getUserFundCompositeKey().getAdviser(),
                        userFund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
                      IssueType issueType = getIssueType(userAdvise.getToken());
                      if(!userFund.getMirroringAdviseIdsTraded().contains(userAdvise.getUserAdviseCompositeKey().getAdviseId())
                          || !userFund.getMirroringAdviseIdsRejected().contains(userAdvise.getUserAdviseCompositeKey().getAdviseId()))
                        forwardToChildren(context(), self(), new UpdateAdviseTradeStatus(issueType), id);
                     }
                  }));
                }));
        
        when(ADDITIONALSUMMIRRORINGWITHOUTTIMEOUT,
            matchEvent(UnSubscribeToFundWithUserEmail.class, 
              (unSubscribeToFundWithUserEmail, userFund) -> 
          {
            return 
                stay().
                replying(failed(userFund.getUsername(), " cannot unsubscribe to "
                    + userFund.getUserFundCompositeKey().getFund() + " while lumpsum is being added to the fund.", CONFLICT));
          }).
          event(UserFundAdviseIssuedInsufficientCapital.class,
              (userFundAdviseIssuedInsufficientCapital, userFund) -> {
                boolean adviseDetailsNotReceived = false;
                for(UserAdvise advise: userFund.getMirroringAdvises()){
                  if(advise.getAdviseId().equals(userFundAdviseIssuedInsufficientCapital.getAdviseId())){
                    adviseDetailsNotReceived = true;
                    break;
                  }
                }
               return adviseDetailsNotReceived ? true : false; 
              }, (userFundAdviseIssuedInsufficientCapital, userFund) ->
          {
               return stay().
               applying(new AdjustFundCashForInsufficientCapitalAdvise(
                      userFund.getUsername(), 
                      userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode(),
                      userFundAdviseIssuedInsufficientCapital.getAdviseId(),
                      userFundAdviseIssuedInsufficientCapital.getAdjustAllocation(),
                      stateName()
                      )).
               andThen(exec(fund -> {
                 if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseTradeExecuted(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 context().stop(findChild(context(), new UserAdviseCompositeKey(
                     userFundAdviseIssuedInsufficientCapital.getAdviseId(), 
                     userFundAdviseIssuedInsufficientCapital.getUsername(), 
                     userFundAdviseIssuedInsufficientCapital.getFundName(), 
                     userFundAdviseIssuedInsufficientCapital.getAdviserUsername(),
                     userFundAdviseIssuedInsufficientCapital.getInvestingMode()).persistenceId(),
                     userFundAdviseIssuedInsufficientCapital.getIssueType()));
                      }));
        }).
          event(UserOpenAdviseOrderRejectedBuilder.class, 
              (userAdviseOrderRejected, userFund) -> {
                boolean adviseDetailsNotReceived = false;
                for(UserAdvise advise: userFund.getMirroringAdvises()){
                  if(advise.getAdviseId().equals(userAdviseOrderRejected.getAdviseId())){
                    adviseDetailsNotReceived = true;
                    break;
                  }
                }
               return adviseDetailsNotReceived ? true : false; 
              }, (userAdviseOrderRejected, userFund) ->
              
          {
               return stay().
               applying(userAdviseOrderRejected.withUserFundState(stateName()).build()).
               andThen(exec(fund -> {
                 if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                   self().forward(new AllAdviseTradeExecuted(
                       fund.getUsername(), 
                       fund.getUserFundCompositeKey().getAdviser(),
                       fund.getUserFundCompositeKey().getFund(),
                       fund.getUserFundCompositeKey().getInvestingMode()), context());
                 }
                 context().stop(findChild(context(), new UserAdviseCompositeKey(
                     userAdviseOrderRejected.getAdviseId(), 
                     userAdviseOrderRejected.getUsername(), 
                     userAdviseOrderRejected.getFundName(), 
                     userAdviseOrderRejected.getAdviserUsername(),
                     userAdviseOrderRejected.getInvestingMode()).persistenceId(),
                     userAdviseOrderRejected.getIssueType()));
               }));
        }).
        event(AdviseTradesExecuted.class, 
            (adviseTradesExecuted, userFund) -> {
              boolean adviseDetailsNotReceived = true;
              if(userFund.getMirroringAdviseIdsTraded().contains(adviseTradesExecuted.getAdviseId())){
                adviseDetailsNotReceived = false;
              }
             return adviseDetailsNotReceived ? true : false; 
            }, (adviseTradesExecuted, userFund) ->
            
        {
             return stay().
             applying(new ExecutedAdviseDetailsUpdated(
                    userFund.getUsername(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getFund(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    adviseTradesExecuted.getAdviseId(),
                    adviseTradesExecuted.getAllocationValue(),
                    adviseTradesExecuted.getTradedValue()
                    )).
             andThen(exec(fund -> {
               if (fund.getAdviseIdsProcessedCount() == fund.getMirroringAdvises().size()) {
                 self().forward(new AllAdviseTradeExecuted(
                     fund.getUsername(), 
                     userFund.getUserFundCompositeKey().getAdviser(),
                     userFund.getUserFundCompositeKey().getFund(), 
                     userFund.getUserFundCompositeKey().getInvestingMode()), context());
               }
                    }));
      }).
        event(AllAdviseTradeExecuted.class, 
            (allAdviseTradeExecuted, userFund) -> {
            double lumpSumAdded = userFund.getInterimTotalInvestment() + userFund.getCashComponent();
            return goTo(SUBSCRIBED).
            applying(new FundLumpSumAdded(
                    userFund.getUsername(), " lumpsum added", 
                    userFund.getUserFundCompositeKey().getFund(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    lumpSumAdded)).
            andThen(exec(fund -> {
              wdActorSelection.userRootActor().tell(new FundLumpSumAdded(fund.getUsername(), " lumpsum added",
                  fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getAdviser(),
                  fund.getUserFundCompositeKey().getInvestingMode(), 
                  lumpSumAdded),
                  self());
              self().tell(new CalculateUserFundComposition(fund.getUserFundCompositeKey()), self());
            }));
                    })
     );
        
    when(SUBSCRIBED, 
        matchEvent(FundAdviseIssuedWithClientInformation.class, 
            (adviseIssued, userFund) -> {
              return (userFund.isAutoApprovedOn() ? false : true);
        },
            (adviseIssued, userFund) -> 
        {
          double portfolioValue = getCurrentPortfolioValue(userFund);
          return stay().
          applying(new UserFundAdviceIssued(
            userFund.getUserFundCompositeKey().getUsername(),
            adviseIssued.getAdviserUsername(), 
            adviseIssued.getFundName(), 
            userFund.getUserFundCompositeKey().getInvestingMode(),
            adviseIssued.getToken(),
            adviseIssued.getAllocation(),
            adviseIssued.getAbsoluteAllocation(), 
            portfolioValue,
            adviseIssued.getIssueTime(),
            adviseIssued.getEntryPrice(),
            adviseIssued.getEntryTime(), 
            adviseIssued.getExitPrice(),
            adviseIssued.getExitTime(),
            adviseIssued.getAdviseId(),
            adviseIssued.getClientCode(),
            adviseIssued.getBrokerName(),
            adviseIssued.getSymbol())).
        andThen(exec(fund -> {
          UserAdvise userAdvise = userFund.getActiveAdvises()
              .stream()
              .filter(advise -> advise.getAdviseId().equals(adviseIssued.getAdviseId())).findFirst().get();
          String exchangeName = getIssueType(userAdvise.getToken()).equals(Equity)
                                ? getExchangeFromWdId(userAdvise.getToken())
                                     : "";
          UserResponseParameters userResponseParameters = new UserResponseParameters.UserResponseParametersBuilder(
                userAdvise.getAdviseId(), 
                Issue, 
                userFund.getUserFundCompositeKey().getUsername(),
                userFund.getUserFundCompositeKey().getFund(),
                userFund.getUserFundCompositeKey().getAdviser(),
                userAdvise.getClientCode(), 
                userAdvise.getBrokerName().getBrokerName(),
                exchangeName,
                userAdvise.getToken(),
                userAdvise.getSymbol(),
                userFund.getUserFundCompositeKey().getInvestingMode(),
                Awaiting,
                adviseIssued.getEntryTime())
                .withAbsoluteAllocation(userAdvise.getIssueAdviceAbsoluteAllocation())
                .withAllocationValue(userAdvise.getIssueAdviseAllocationValue())
                .withNumberOfShares(0)
                .build();
            
            wdActorSelection.userRootActor().tell(userResponseParameters, self());
            
            self().tell(userResponseParameters, self());
            
          }));
        }).
        event(FundAdviseIssuedWithClientInformation.class, 
            (adviseIssued, userFund) -> {
              return userFund.isAutoApprovedOn() ? true : false;
        }, (adviseIssued, userFund) -> {
          double portfolioValue = getCurrentPortfolioValue(userFund);
          return stay().
              applying(new UserFundAdviceIssued(
                  userFund.getUserFundCompositeKey().getUsername(),
                  adviseIssued.getAdviserUsername(), 
                  adviseIssued.getFundName(), 
                  userFund.getUserFundCompositeKey().getInvestingMode(),
                  adviseIssued.getToken(),
                  adviseIssued.getAllocation(),
                  adviseIssued.getAbsoluteAllocation(), 
                  portfolioValue,
                  adviseIssued.getIssueTime(),
                  adviseIssued.getEntryPrice(),
                  adviseIssued.getEntryTime(), 
                  adviseIssued.getExitPrice(),
                  adviseIssued.getExitTime(),
                  adviseIssued.getAdviseId(),
                  adviseIssued.getClientCode(),
                  adviseIssued.getBrokerName(),
                  adviseIssued.getSymbol())).
              andThen(exec(fund -> {
                  UserAdvise userAdvise = fund.getActiveAdvises().stream()
                      .filter(advise -> advise.getAdviseId().equals(adviseIssued.getAdviseId()))
                      .findFirst().get();
                  handleUserAdviceIssued(adviseIssued, getIssueType(userAdvise.getToken()), fund, userAdvise);
              }));
        }).
        event(RetryUserAdviseWithClientInformation.class, 
            (retryAdvise, userFund) -> {
              return retryAdvise.isValidated(userFund) ? true : false;
        }, (retryAdvise, userFund) -> {
          return stay().
              applying(new UserFundAdviceRetryInitiated(
                  userFund.getUserFundCompositeKey().getUsername(),
                  userFund.getUserFundCompositeKey().getAdviser(),
                  userFund.getUserFundCompositeKey().getFund(),
                  userFund.getUserFundCompositeKey().getInvestingMode(),
                  retryAdvise.getRejectedAdviseDetails())).
              andThen(exec(fund -> {
                boolean userOnBoardingStatus = getOnboardingProcessStatus(fund.getUserFundCompositeKey().getUsername());
                Collection<String> collectionOfWdId = new ArrayList<String>();
                retryAdvise.getRejectedAdviseDetails().stream().forEach(userResponseParam -> {
                  collectionOfWdId.add(userResponseParam.getToken());
                });
                HashMap<String, PriceWithPaf> tickerToPriceWithPafMap = getCurrentPriceSnapshotBasket(collectionOfWdId);
                int i = 1;
                for (UserResponseParameters userResponse : retryAdvise.getRejectedAdviseDetails()) {
                  UserFundAdditionalSumAdvice retryFundAdvice = fundLumpSumAdviceWithUndefinedExitParams(
                      userResponse.getUsername(), 
                      userResponse.getAdviserName(), 
                      userResponse.getFundName(), 
                      userResponse.getInvestingMode(),
                      userResponse.getAdviseId(), 
                      userResponse.getToken(),
                      userResponse.getSymbol(),
                      "Retrying for a previously rejected advice",
                      userResponse.getAbsoluteAllocation(), 
                      userResponse.getAllocationValue(),
                      tickerToPriceWithPafMap.get(userResponse.getToken()),
                      userOnBoardingStatus,
                      retryAdvise.getBrokerName(),
                      retryAdvise.getClientCode(),
                      getIssueType(userResponse.getToken()),
                      false,
                      userFund.getBasketOrderId()+CompositeKeySeparator+valueOf(i),
                      retryAdvise.getBrokerAuthInformation());
                  forwardToChildren(context(), self(), 
                      retryFundAdvice, 
                      new UserAdviseCompositeKey( retryFundAdvice.getAdviseId(), 
                          retryFundAdvice.getUsername(), 
                          retryFundAdvice.getFundName(),
                          retryFundAdvice.getAdviserUsername(),
                          retryFundAdvice.getInvestingMode()).persistenceId());
                  i++;
                }
              sender().tell(
              new Done(userFund.getUserFundCompositeKey().getUsername(),
                  "Retrials in progress. Scoot over to your investments to see the status!"), self());
              }));
        }).
        event(RetryUserAdviseWithClientInformation.class, 
            (retryAdvise, userFund) -> {
              return retryAdvise.isValidated(userFund) ? false : true;
        }, (retryAdvise, userFund) -> {
          sender().tell(failed(userFund.getUserFundCompositeKey().getUsername(),
              "Advise not found in active rejected advises list", PRECONDITION_FAILED), self());
          return stay();
        }).
        event(UserResponseParameters.class,
            (userResponseParameters, userFund) -> {
              return userResponseParameters.getUserResponse().equals(Approve) && userResponseParameters.getAdviseType().equals(Issue);
            },
            (userResponseParameters, userFund) -> {
              UserAdvise userAdvise = userFund.getActiveAdvises().stream()
                  .filter(advise -> advise.getAdviseId().equals(userResponseParameters.getAdviseId())).findFirst().get();
              
              UserResponseReceivedBuilder userResponseReceivedBuilder = new UserResponseReceivedBuilder(
                  userFund.getUserFundCompositeKey().getUsername(),
                  userFund.getUserFundCompositeKey().getAdviser(),
                  userFund.getUserFundCompositeKey().getFund(),
                  userFund.getUserFundCompositeKey().getInvestingMode(), 
                  userResponseParameters.getAdviseId(), 
                  userResponseParameters.getAdviseType(), 
                  userResponseParameters.getClientCode(), 
                  userResponseParameters.getBrokerName(), 
                  userResponseParameters.getToken(), 
                  userResponseParameters.getSymbol(), 
                  userResponseParameters.getExchange(),
                  userResponseParameters.getAbsoluteAllocation(),
                  userResponseParameters.getInvestmentDate(), 
                  userResponseParameters.getUserResponse())
                  .withAllocationValue(userResponseParameters.getAllocationValue())
                  .withBrokerAuthInformation(userResponseParameters.getBrokerAuthInformation())
                  .withBasketOrderCompositeId(userResponseParameters.getBasketOrderCompositeId());
              
              return stay().
              applying(userResponseReceivedBuilder.build()).
              andThen(exec(fund -> {
                handleUserResponse(userResponseParameters, getIssueType(userAdvise.getToken()), userAdvise, userFund);
                  }));
        }).
        event(UserResponseParameters.class,
            (userResponseParameters, userFund) -> {
              return userResponseParameters.getUserResponse().equals(Awaiting);
            },
            (userResponseParameters, userFund) -> {
              
              UserResponseReceived userResponseReceived = new UserResponseReceived.UserResponseReceivedBuilder(
                  userFund.getUserFundCompositeKey().getUsername(),
                  userResponseParameters.getAdviserName(),
                  userResponseParameters.getFundName(),
                  userResponseParameters.getInvestingMode(), 
                  userResponseParameters.getAdviseId(), 
                  userResponseParameters.getAdviseType(), 
                  userResponseParameters.getClientCode(), 
                  userResponseParameters.getBrokerName(),
                  userResponseParameters.getExchange(),
                  userResponseParameters.getToken(), 
                  userResponseParameters.getSymbol(),
                  userResponseParameters.getAbsoluteAllocation(),
                  userResponseParameters.getInvestmentDate(), 
                  userResponseParameters.getUserResponse())
                  .withAllocationValue(userResponseParameters.getAllocationValue())
                  .withNumberOfShares(userResponseParameters.getNumberOfShares()).build();
              return stay().
              applying(userResponseReceived);
        }).
        event(UserResponseParameters.class,
            (userResponseParameters, userFund) -> {
              return userResponseParameters.getUserResponse().equals(Archive) ? true : false;
            },
            (userResponseParameters, userFund) -> {
              UserResponseReceivedBuilder userResponseReceivedBuilder = new UserResponseReceivedBuilder(
                  userFund.getUserFundCompositeKey().getUsername(),
                  userFund.getUserFundCompositeKey().getAdviser(),
                  userFund.getUserFundCompositeKey().getFund(),
                  userFund.getUserFundCompositeKey().getInvestingMode(), 
                  userResponseParameters.getAdviseId(), 
                  userResponseParameters.getAdviseType(), 
                  userResponseParameters.getClientCode(), 
                  userResponseParameters.getBrokerName(), 
                  userResponseParameters.getExchange(),
                  userResponseParameters.getToken(), 
                  userResponseParameters.getSymbol(), 
                  userResponseParameters.getAbsoluteAllocation(),
                  userResponseParameters.getInvestmentDate(), 
                  userResponseParameters.getUserResponse());
              userResponseReceivedBuilder.withAllocationValue(userResponseParameters.getAllocationValue());
              userResponseReceivedBuilder.withNumberOfShares(userResponseParameters.getNumberOfShares());
              return stay().
              applying(userResponseReceivedBuilder.build())
              .andThen(exec(fund -> {
                if (fund.getPendingApprovalRequests().size() == 0) {
                  self().tell(new SwitchingToArchivedState(
                      userFund.getUserFundCompositeKey().getUsername(),
                      userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode()), self());
                }
              }));
        }).
        event(SwitchingToArchivedState.class,
            (switchingToArchivedState, userFund) -> {
              return goTo(ARCHIVED).
              applying(switchingToArchivedState);
        }).
        event(UserResponseParameters.class,
            (userResponseParameters, userFund) -> {
              return userResponseParameters.getUserResponse().equals(ExitFund) ? true : false;
            },
            (userResponseParameters, userFund) -> {
              UserResponseReceivedBuilder userResponseReceivedBuilder = new UserResponseReceivedBuilder(
                  userFund.getUserFundCompositeKey().getUsername(),
                  userFund.getUserFundCompositeKey().getAdviser(),
                  userFund.getUserFundCompositeKey().getFund(),
                  userFund.getUserFundCompositeKey().getInvestingMode(), 
                  userResponseParameters.getAdviseId(), 
                  userResponseParameters.getAdviseType(), 
                  userResponseParameters.getClientCode(), 
                  userResponseParameters.getBrokerName(),
                  userResponseParameters.getExchange(),
                  userResponseParameters.getToken(), 
                  userResponseParameters.getSymbol(), 
                  userResponseParameters.getAbsoluteAllocation(),
                  userResponseParameters.getInvestmentDate(), 
                  userResponseParameters.getUserResponse())
                  .withAllocationValue(userResponseParameters.getAllocationValue())
                  .withBrokerAuthInformation(userResponseParameters.getBrokerAuthInformation())
                  .withBasketOrderCompositeId(userResponseParameters.getBasketOrderCompositeId());
              return stay().
              applying(userResponseReceivedBuilder.build()).
              andThen(exec(fund -> {
                if (fund.getPendingApprovalRequests().size() == 0) {
                self().tell(new UnSubscribeToFundWithUserEmail(
                    fund.getUserFundCompositeKey().getAdviser(), 
                    fund.getUserFundCompositeKey().getFund(), 
                    fund.getUserFundCompositeKey().getInvestingMode(), 
                    fund.getUserFundCompositeKey().getUsername(),
                    fund.getUserFundCompositeKey().getUsername(),
                    "", userResponseParameters.getBrokerAuthInformation()), self());
                }
              }));
              
        }).
       event(MergerExitTradesExecuted.class, 
          (mergerExitTradesExecuted, userFund) ->
       {
        return stay().
        applying(new MergerSellTradeExecuted(
            userFund.getUserFundCompositeKey().getUsername(),
            userFund.getUserFundCompositeKey().getAdviser(),
            userFund.getUserFundCompositeKey().getFund(),
            userFund.getUserFundCompositeKey().getInvestingMode(), 
            mergerExitTradesExecuted.getAdviseId(),
            mergerExitTradesExecuted.getUserAdviseState(),
            mergerExitTradesExecuted.getTradedValue()));
       }).
       event(AdviseTradesExecuted.class, 
           (adviseTradesExecuted, userFund) ->
        {
         return stay().
         applying(new FundAdviceTradeExecuted(
             userFund.getUserFundCompositeKey().getUsername(),
             userFund.getUserFundCompositeKey().getAdviser(),
             userFund.getUserFundCompositeKey().getFund(),
             userFund.getUserFundCompositeKey().getInvestingMode(), 
             adviseTradesExecuted.getAdviseId(),
             adviseTradesExecuted.getUserAdviseState(),
             adviseTradesExecuted.getAllocationValue(),
             adviseTradesExecuted.getTradedValue())).
         andThen(exec(fund -> {
             self().tell(new CalculateUserFundComposition(userFund.getUserFundCompositeKey()), self());
           }));
        }).
       event(UserFundAdviseIssuedInsufficientCapital.class, 
           (userFundAdviseIssuedInsufficientCapital, userFund) -> 
       {
            return stay().
            applying(new AdjustFundCashForInsufficientCapitalAdvise(
                   userFund.getUsername(), 
                   userFund.getUserFundCompositeKey().getAdviser(),
                   userFund.getUserFundCompositeKey().getFund(),
                   userFund.getUserFundCompositeKey().getInvestingMode(),
                   userFundAdviseIssuedInsufficientCapital.getAdviseId(),
                   userFundAdviseIssuedInsufficientCapital.getAdjustAllocation(),
                   stateName()
                   )).
            andThen(exec(fund -> {
             context().stop(findChild(context(), new UserAdviseCompositeKey(
                 userFundAdviseIssuedInsufficientCapital.getAdviseId(), 
                 userFundAdviseIssuedInsufficientCapital.getUsername(), 
                 userFundAdviseIssuedInsufficientCapital.getFundName(), 
                 userFundAdviseIssuedInsufficientCapital.getAdviserUsername(),
                 userFundAdviseIssuedInsufficientCapital.getInvestingMode()).persistenceId(),
                 userFundAdviseIssuedInsufficientCapital.getIssueType()));
                   }));
     }).
       event(UserOpenAdviseOrderRejectedBuilder.class, 
           (userAdviseOrderRejected, userFund) -> 
       {
         return stay().
         applying(userAdviseOrderRejected.withUserFundState(stateName()).build()).
         andThen(exec(fund -> {
           context().stop(findChild(context(), new UserAdviseCompositeKey(
               userAdviseOrderRejected.getAdviseId(), 
               userAdviseOrderRejected.getUsername(), 
               userAdviseOrderRejected.getFundName(), 
               userAdviseOrderRejected.getAdviserUsername(),
               userAdviseOrderRejected.getInvestingMode()).persistenceId(),
               userAdviseOrderRejected.getIssueType()));
                   })
             );
     }).
       event(UserCloseAdviseOrderRejected.class, (userCloseAdviseOrderRejected, userFund) -> {
         return stay().
             applying(userCloseAdviseOrderRejected);
     }).
       event(CorporateEventUpdateWithFundNames.class, 
           (corporateEventUpdate, userFund) -> {
             for(UserAdvise userAdvise: userFund.getActiveAdvises()){
              String id = "" + new UserAdviseCompositeKey(
                userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                userFund.getUserFundCompositeKey().getUsername(), 
                userFund.getUserFundCompositeKey().getFund(), 
                userFund.getUserFundCompositeKey().getAdviser(),
                userFund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
              if(corporateEventUpdate.getWdId().equals(userAdvise.getToken()))
                forwardToChildren(context(), self(), corporateEventUpdate, id);
             }
             return stay();
           }).
      event(FundAdviceClosed.class, 
          (fundAdviceClosed, userFund) -> {
            return doesUserAdviseExist(userFund, fundAdviceClosed.getAdviseId());
          },
          (fundAdviceClosed, userFund) -> {
            return stay().
         applying(new UserFundAdviceClosed(
             userFund.getUserFundCompositeKey().getUsername(),
             fundAdviceClosed.getAdviserUsername(), 
             fundAdviceClosed.getFundName(), 
             userFund.getUserFundCompositeKey().getInvestingMode(),
             fundAdviceClosed.getAdviseId(),
             fundAdviceClosed.getAbsoluteAllocation(),
             fundAdviceClosed.getExitDate(),
             fundAdviceClosed.getExitPrice())).
         andThen(exec(fund -> {
           UserAdvise userAdvise = fund.getActiveAdvises().
               stream().
               filter(advise -> advise.getAdviseId().equals(fundAdviceClosed.getAdviseId())).findAny().get();
           if(fund.getActiveAdviseIdsTraded().contains(userAdvise.getAdviseId())) {
             forwardToChildren(context(), 
                 self(), 
                 new UserAdviseTransactionReceived.UserAdviseTransactionReceivedBuilder(
                     userAdvise.getUserAdviseCompositeKey().getUsername(),
                     userAdvise.getUserAdviseCompositeKey().getAdviser(),
                     userAdvise.getUserAdviseCompositeKey().getFund(),
                     fund.getUserFundCompositeKey().getInvestingMode(), 
                     fundAdviceClosed.getAdviseId(),
                     getIssueType(userAdvise.getToken()))
                 .withAbsoluteAllocation(fundAdviceClosed.getAbsoluteAllocation())
                 .withExitPrice(fundAdviceClosed.getExitPrice())
                 .withExitTime(fundAdviceClosed.getExitDate())
                 .withUserAcceptanceMode(userFund.getUserFundAcceptanceMode())
                 .build(),
                 "" + new UserAdviseCompositeKey(userAdvise.getAdviseId(),
                     userAdvise.getUserAdviseCompositeKey().getUsername(),
                     userAdvise.getUserAdviseCompositeKey().getFund(),
                     userAdvise.getUserAdviseCompositeKey().getAdviser(),
                     userAdvise.getUserAdviseCompositeKey().getInvestingMode()).persistenceId());
           } else if (findInActivePendingAdvisesList(userAdvise.getAdviseId(), fund)) {
             String brokerName = getBrokerName(userAdvise);
             UserResponseParameters userResponseParameters = new UserResponseParameters.UserResponseParametersBuilder(
                 userAdvise.getAdviseId(), 
                 AdviseType.Close, 
                 userAdvise.getUserAdviseCompositeKey().getUsername(),
                 userAdvise.getUserAdviseCompositeKey().getFund(),
                 userAdvise.getUserAdviseCompositeKey().getAdviser(),
                 userAdvise.getClientCode(), 
                 brokerName,
                 getExchangeFromWdId(userAdvise.getToken()),
                 userAdvise.getToken(),
                 userAdvise.getSymbol(),
                 userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                 Awaiting,
                 fundAdviceClosed.getExitDate())
                 .withAbsoluteAllocation(fundAdviceClosed.getAbsoluteAllocation())
                 .withAllocationValue(0.)
                 .withNumberOfShares(0)
                 .build();
             self().tell(userResponseParameters, self());
           }
          }));
       }).
      event(FundAdviceClosed.class, 
          (fundAdviceClosed, userFund) -> {
            return !doesUserAdviseExist(userFund, fundAdviceClosed.getAdviseId());
          },
          (fundAdviceClosed, userFund) -> {
            return stay();
       }).
      event(UserResponseParameters.class,
          (userResponseParameters, userFund) -> {
            return userResponseParameters.getUserResponse().equals(Approve) && userResponseParameters.getAdviseType().equals(AdviseType.Close);
          },
          (userResponseParameters, userFund) -> {
            UserResponseReceivedBuilder userResponseReceivedBuilder = new UserResponseReceivedBuilder(
                userFund.getUserFundCompositeKey().getUsername(),
                userFund.getUserFundCompositeKey().getAdviser(),
                userFund.getUserFundCompositeKey().getFund(),
                userFund.getUserFundCompositeKey().getInvestingMode(), 
                userResponseParameters.getAdviseId(), 
                userResponseParameters.getAdviseType(), 
                userResponseParameters.getClientCode(), 
                userResponseParameters.getBrokerName(), 
                userResponseParameters.getExchange(),
                userResponseParameters.getToken(), 
                userResponseParameters.getSymbol(), 
                userResponseParameters.getAbsoluteAllocation(),
                userResponseParameters.getInvestmentDate(), 
                userResponseParameters.getUserResponse());
                userResponseReceivedBuilder.withNumberOfShares(userResponseParameters.getNumberOfShares());
                userResponseReceivedBuilder.withBasketOrderCompositeId(userResponseParameters.getBasketOrderCompositeId());
                userResponseReceivedBuilder.withBrokerAuthInformation(userResponseParameters.getBrokerAuthInformation());
                
            return stay().
                applying(userResponseReceivedBuilder.build()).
                andThen(exec(fund -> {
                  forwardToChildren(context(), 
                        self(),
                        userResponseReceivedBuilder.build(),
                        "" + new UserAdviseCompositeKey(
                                userResponseParameters.getAdviseId(),
                                fund.getUserFundCompositeKey().getUsername(),
                                fund.getUserFundCompositeKey().getFund(),
                                fund.getUserFundCompositeKey().getAdviser(),
                                fund.getUserFundCompositeKey().getInvestingMode()).persistenceId());
                  }));
      }).
      event(UnSubscribeToFundWithUserEmail.class, 
          (unSubscribeToFundWithUserEmail, userFund) -> { 
            return (userFund.getActiveAdviseIdsTraded().size() == 0 && userFund.getAdviseIdsWithPendingTradeConfirmations().size() == 0) ? true : false;
          }, 
          (unSubscribeToFundWithUserEmail, userFund) ->{
            
            UserFundState userFundState = userFund.containsOnlyEquityAdvises() ? UNSUBSCRIBING : UNSUBSCRIBINGWITHOUTTIMEOUT;
            return goTo(userFundState).
                applying(new FundUserUnsubscribed(unSubscribeToFundWithUserEmail.getUsername(),
                    unSubscribeToFundWithUserEmail.getAdviserUsername(), 
                    unSubscribeToFundWithUserEmail.getFundName(), 
                    unSubscribeToFundWithUserEmail.getInvestingMode(),
                    unSubscribeToFundWithUserEmail.getBasketOrderId())).
                andThen(exec(fund -> {
                      self().forward(new FundSuccessfullyUnsubscribed(
                          fund.getUserFundCompositeKey().getUsername(),
                          fund.getUserFundCompositeKey().getAdviser(),
                          fund.getUserFundCompositeKey().getFund(),
                          fund.getUserFundCompositeKey().getInvestingMode(),
                          fund.getCashComponent()), context());
                sender().tell(new Done(userFund.getUserFundCompositeKey().getUsername(),
                          "UnSubscribe in process. You will receive a notification once its done."), self());   
                    }));
          }).
      event(UpdateUserFundCash.class, 
          (updateUserFundCash, userFund) ->{
            return stay().
                applying(new UpdatingUserFundCash(updateUserFundCash.getUsername(),
                    updateUserFundCash.getAdviserUsername(), 
                    updateUserFundCash.getFundName(), 
                    updateUserFundCash.getInvestingMode(),
                    updateUserFundCash.getCashToAddToUserFund()));
          }).
      event(GetUnsubscriptionSnapshotOfFund.class, 
          (getUnsubscriptionSnapshotOfFund, userFund) -> {
            if(userFund.getAdviseIdsWithPendingTradeConfirmations().size() == 0){
              appriseInvestorWithPreWithdrawalSnapshot(userFund, 100.);
          }else{
            sender().tell(failed(userFund.getUserFundCompositeKey().getFund(),
                "Cannot unsubscribe as previously approved trade confirmations are expected from the exchange. Please wait a while", StatusCodes.NOT_ACCEPTABLE), self());
          }
            return stay();
          }).
      event(UnSubscribeToFundWithUserEmail.class,  
          (unSubscribeToFundWithUserEmail, userFund) -> {
            return (userFund.getActiveAdviseIdsTraded().size() != 0  && userFund.getAdviseIdsWithPendingTradeConfirmations().size() == 0) ? true : false;
          },
          (unSubscribeToFundWithUserEmail, userFund) -> {
            UserFundState userFundState = userFund.containsOnlyEquityAdvises() ? UNSUBSCRIBING : UNSUBSCRIBINGWITHOUTTIMEOUT;
            return goTo(userFundState).
                applying(new FundUserUnsubscribed(userFund.getUserFundCompositeKey().getUsername(),
                    unSubscribeToFundWithUserEmail.getAdviserUsername(), 
                    unSubscribeToFundWithUserEmail.getFundName(), 
                    unSubscribeToFundWithUserEmail.getInvestingMode(),
                    unSubscribeToFundWithUserEmail.getBasketOrderId())).
                andThen(exec(fund -> {
                  sender().tell(new Done(userFund.getUserFundCompositeKey().getUsername(),
                      "UnSubscribe in process. You will receive a notification once its done."), self());
                  int i = 1;
                      for (UserAdvise userAdvise : fund.getActiveAdvises()) {
                        if(userFund.getActiveAdviseIdsTraded().contains(userAdvise.getAdviseId())){
                          UserAdviseTransactionReceived userAdviseTransactionReceived = new 
                              UserAdviseTransactionReceived.UserAdviseTransactionReceivedBuilder(
                                  userAdvise.getUserAdviseCompositeKey().getUsername(),
                                  userAdvise.getUserAdviseCompositeKey().getAdviser(),
                                  userAdvise.getUserAdviseCompositeKey().getFund(),
                                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                                  userAdvise.getAdviseId(),
                                  getIssueType(userAdvise.getToken()))
                              .withUserAcceptanceMode(Auto)
                              .withAbsoluteAllocation(100.)
                              .withExitPrice(noPrice(userAdvise.getToken()))
                              .withBasketOrderId(userFund.getBasketOrderId()+CompositeKeySeparator+valueOf(i))
                              .withBrokerAuthentication(unSubscribeToFundWithUserEmail.getBrokerAuthInformation())
                              .build();
                          
                          forwardToChildren(context(), self(), userAdviseTransactionReceived, "" + new UserAdviseCompositeKey(
                              userAdvise.getAdviseId(), 
                              fund.getUserFundCompositeKey().getUsername(), 
                              fund.getUserFundCompositeKey().getFund(), 
                              fund.getUserFundCompositeKey().getAdviser(),
                              fund.getUserFundCompositeKey().getInvestingMode()).persistenceId());
                        }
                        i++;
                      }
                    }));
          }).
      event(UnSubscribeToFundWithUserEmail.class, 
          (unSubscribeToFundWithUserEmail, userFund) -> {
            if(userFund.getAdviseIdsWithPendingTradeConfirmations().size() != 0)
              sender().tell(failed(userFund.getUserFundCompositeKey().getFund(),
                  "Cannot unsubscribe as previously approved trade confirmations are expected from the exchange. Please wait a while", StatusCodes.NOT_ACCEPTABLE), self());
            return stay();
          }).
      event(AddLumpsumToFundWithUserEmail.class, 
          (addLumpsumToFundWithUserEmail, userFund) -> {
        return stay()
            .applying(new AdditionalSumAddedToUserFund(
                addLumpsumToFundWithUserEmail.getUsername(),
                addLumpsumToFundWithUserEmail.getAdviserUsername(), 
                addLumpsumToFundWithUserEmail.getFundName(),
                addLumpsumToFundWithUserEmail.getInvestingMode(),
                addLumpsumToFundWithUserEmail.getLumpSumAmount(),
                currentTimeInMillis(), 
                addLumpsumToFundWithUserEmail.getRiskProfile(), 
                addLumpsumToFundWithUserEmail.getInceptionDate(),
                addLumpsumToFundWithUserEmail.getBasketOrderId()))
            .andThen(exec(fund -> {
                sender().tell(new Done(addLumpsumToFundWithUserEmail.getUsername(),
                    "Add Lumpsum in process. You will receive a notification once its done."), self());
                self().tell(new FundConstituentListResponseWithBrokerAuth(addLumpsumToFundWithUserEmail.getFundConstituentListResponse(),
                    addLumpsumToFundWithUserEmail.getBrokerAuthInfo()), self());
            }));
          }).
      event(PartialWithdrawalUnderway.class,
          (partialWithdrawalUnderway, userFund) -> {
            double portFolioValue = getCurrentPortfolioValue(userFund);
            return Double.compare((0.9 * portFolioValue), partialWithdrawalUnderway
                .getWithdrawalAmount()) <= 0;
          }, 
          (partialWithdrawalUnderway, userFund) -> {
                return stay()
                    .replying(failed(partialWithdrawalUnderway.getUsername(),
                      "Withdrawal is greater than the 90% of portfolio value, please exit from the fund", StatusCodes.PRECONDITION_FAILED));
          }).
      event(PartialWithdrawalUnderway.class,
              (partialWithdrawalUnderway, userFund) -> {
                double portFolioValue = getCurrentPortfolioValue(userFund);
                return ((Double.compare((0.9 * portFolioValue), partialWithdrawalUnderway
                    .getWithdrawalAmount()) > 0) && !partialWithdrawalUnderway.getPurpose().isPresent());
              }, 
              (partialWithdrawalUnderway, userFund) -> {
                return stay()
                    .applying(new WithdrawSumFromUserFundAttempted(partialWithdrawalUnderway.getUsername(),
                        partialWithdrawalUnderway.getUsername(),
                        partialWithdrawalUnderway.getAdviserUsername(),
                        partialWithdrawalUnderway.getFundName(),
                        partialWithdrawalUnderway.getInvestingMode(),
                        partialWithdrawalUnderway.getWithdrawalAmount(), currentTimeInMillis()))
                    .andThen(exec(fund -> {
                      double portFolioValue = getCurrentPortfolioValue(fund);
                      double allocation = DoublesUtil.isANonZeroValidDouble(portFolioValue) ?  
                          ((partialWithdrawalUnderway.getWithdrawalAmount()/portFolioValue) * 100) : 0.;
                      appriseInvestorWithPreWithdrawalSnapshot(fund, allocation);
            }));
         }).
      event(PartialWithdrawalUnderway.class,
          (partialWithdrawalUnderway, userFund) -> {
            double portFolioValue = getCurrentPortfolioValue(userFund);
            return ((Double.compare((0.9 * portFolioValue), userFund.getWithdrawalAmountRequested()) > 0) && 
                partialWithdrawalUnderway.getPurpose()
                                                 .orElseGet(() -> NONE).equals(PARTIALWITHDRAWAL));
          }, 
          (partialWithdrawalUnderway, userFund) -> {
            return goTo(WITHDRAWING)
                .applying(new WithdrawSumFromUserFundConfirmed(partialWithdrawalUnderway.getUsername(),
                    partialWithdrawalUnderway.getUsername(),
                    partialWithdrawalUnderway.getAdviserUsername(),
                    partialWithdrawalUnderway.getFundName(),
                    partialWithdrawalUnderway.getInvestingMode(),
                    partialWithdrawalUnderway.getWithdrawalAmount(),
                    currentTimeInMillis(),
                    partialWithdrawalUnderway.getBasketOrderId()))
                .andThen(exec(fund -> {
                  double portFolioValue = getCurrentPortfolioValue(fund);
                  double allocation = DoublesUtil.isANonZeroValidDouble(portFolioValue) ?  
                      ((fund.getWithdrawalAmountRequested() /portFolioValue) * 100) : 0.;
                  if (userFund.getAdviseIdToExecutedSharesforWithdrawal(allocation).size() == 0) {
                    self().tell(new UserFundWithdrawalDoneSuccessfully(
                        userFund.getUserFundCompositeKey().getUsername(),
                        userFund.getUserFundCompositeKey().getAdviser(),
                        userFund.getUserFundCompositeKey().getFund(),
                        userFund.getUserFundCompositeKey().getInvestingMode(),
                        userFund.getUsername(),
                        userFund.getWithdrawalAmountRequested()), sender());
                  } else {
                    self().tell(new CurrentUserFundValue(portFolioValue, partialWithdrawalUnderway.getBrokerAuthInformation()), sender());
                  }
                  sender().tell(
                      new Done(partialWithdrawalUnderway.getUsername(),
                          "Withdrawal in progress. You will receive a notification once its done."), self());
        }));
     }).
      event(FundConstituentListResponseWithBrokerAuth.class, 
          (fundConstituentListWrapper, userFund) -> {
            return (fundConstituentListWrapper.getFundListResponse().getFundConstituentList().size() == 0) ? false : true;
          }, 
          (fundConstituentListWrapper, userFund) -> 
      {
        BrokerNameSent brokerNameSent = getBrokerNameOfTheInvestorWith(userFund.getUserFundCompositeKey().getUsername(),
            userFund.getUserFundCompositeKey().getInvestingMode());
        UserFundState userfundState = identifyTransitionState(fundConstituentListWrapper.getFundListResponse().getFundConstituentList()) ? ADDITIONALSUMMIRRORING : ADDITIONALSUMMIRRORINGWITHOUTTIMEOUT;
        RequestedClientCode requestedClientCode = getClientCodeFromUserRootActor(userFund.getUserFundCompositeKey().getUsername());
        return goTo(userfundState).
        applying(new AdvicesIssuedPreviously(
            userFund.getUsername(),
            userFund.getUserFundCompositeKey().getAdviser(),
            userFund.getUserFundCompositeKey().getFund(),
            userFund.getUserFundCompositeKey().getInvestingMode(),
            fundConstituentListWrapper.getFundListResponse().getFundConstituentList(),
            requestedClientCode.getClientCode(),
            brokerNameSent.getBrokerName().getBrokerName(),
            userFund.getInvestingPurpose())).
        andThen(exec(fund -> {
                  boolean userOnBoardingStatus = getOnboardingProcessStatus(fund.getUserFundCompositeKey().getUsername());
                  int i=1;
                  for (UserAdvise userAdvise : fund.getMirroringAdvises()) {
                    UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = fundLumpSumAdviceWithUndefinedExitParams(
                        userAdvise.getUserAdviseCompositeKey().getUsername(), 
                        userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                        userAdvise.getUserAdviseCompositeKey().getFund(), 
                        userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                        userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                        userAdvise.getToken(), 
                        userAdvise.getToken(),
                        "Replicating previously issued advices in lumpsum",
                        userAdvise.getIssueAdviceAbsoluteAllocation(), 
                        userAdvise.getIssueAdviseAllocationValue(),
                        userAdvise.getAverageEntryPrice(),
                        userOnBoardingStatus,
                        userAdvise.getBrokerName(),
                        userAdvise.getClientCode(),
                        getIssueType(userAdvise.getToken()),
                        false,
                        userFund.getBasketOrderId()+CompositeKeySeparator+valueOf(i),
                        fundConstituentListWrapper.getBrokerAuthInformation());
                    forwardToChildren(context(), self(), 
                        fundAdviceIssuedWithUserEmail, 
                        new UserAdviseCompositeKey( fundAdviceIssuedWithUserEmail.getAdviseId(), 
                                                    fundAdviceIssuedWithUserEmail.getUsername(), 
                                                    fundAdviceIssuedWithUserEmail.getFundName(),
                                                    fundAdviceIssuedWithUserEmail.getAdviserUsername(),
                                                    fundAdviceIssuedWithUserEmail.getInvestingMode()).persistenceId());
                    i++;
                  }
               }));
          }).
      event(FundConstituentListResponseWithBrokerAuth.class, 
          (fundConstituentListWarpper, userFund) -> {
            return (fundConstituentListWarpper.getFundListResponse().getFundConstituentList().size() == 0) ? true : false;
          }, 
          (fundConstituentListWarpper, userFund) -> 
      {
        return goTo(SUBSCRIBED).
            applying(new FundLumpSumAdded(
                    userFund.getUsername(), " lumpsum added", 
                    userFund.getUserFundCompositeKey().getFund(), 
                    userFund.getUserFundCompositeKey().getAdviser(),
                    userFund.getUserFundCompositeKey().getInvestingMode(),
                    userFund.getCurrentLumpSum())).
            andThen(exec(fund -> {
              wdActorSelection.userRootActor().tell(new FundLumpSumAdded(fund.getUsername(), " lumpsum added",
                  fund.getUserFundCompositeKey().getFund(), fund.getUserFundCompositeKey().getAdviser(),
                  fund.getUserFundCompositeKey().getInvestingMode(), fund.getCurrentLumpSum()),
                  self());
            }));
                    }).
      event(MergerDemergerFundAdviseClosed.class, 
          (mergerDemergerSellEvent, userFund) -> {
            List<UserResponseParameters> advises = userFund.getPendingApprovalRequests();
            boolean advisePendingApproval = false;
            for (UserResponseParameters userResponseParameters: advises) {
              if (userResponseParameters.getAdviseId().equals(mergerDemergerSellEvent.getAdviseId()) ) {
                advisePendingApproval = true;
                break;
              }
            }
            return (advisePendingApproval) ? false : true ;
      },
          (mergerDemergerSellEvent, userFund) -> 
      {
        return stay()
            .applying(new CorporateMergerSellEvent(userFund.getUserFundCompositeKey().getUsername(),
                                                    userFund.getUserFundCompositeKey().getAdviser(), 
                                                    userFund.getUserFundCompositeKey().getFund(),
                                                    userFund.getUserFundCompositeKey().getInvestingMode(),
                                                    mergerDemergerSellEvent.getAdviseId(),
                                                    mergerDemergerSellEvent.getToken(),
                                                    mergerDemergerSellEvent.getPrice(), 
                                                    OrderSide.SELL)).
            andThen(exec(fund -> {
              Set<UserAdvise> advises = userFund.getActiveAdvises();
              UserAdvise advise = null ;
              for (UserAdvise userAdvise: advises) {
                if (userAdvise.getToken().equals(mergerDemergerSellEvent.getToken())) {
                  advise = userAdvise;
                  String id = "" + new UserAdviseCompositeKey(
                      advise.getUserAdviseCompositeKey().getAdviseId(), 
                      fund.getUserFundCompositeKey().getUsername(), 
                      fund.getUserFundCompositeKey().getFund(), 
                      fund.getUserFundCompositeKey().getAdviser(),
                      fund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
                  
                  forwardToChildren(context(), self(), mergerDemergerSellEvent, id);
                }
              }
              
            }));

      }).
           event(SpecialCircumstancesSell.class, 
          (specialCircumstancesSell, userFund) -> {
            String adviseId = specialCircumstancesSell.getAdviseId();
            String symbol = specialCircumstancesSell.getSymbol();
            String token = specialCircumstancesSell.getToken();
            return (userFund.getActiveAdvises().stream().filter(o -> o.getAdviseId().equals(adviseId) && o.getSymbol().equals(symbol) && o.getToken().equals(token)).findFirst().isPresent())? true : false; 
          },
          (specialCircumstancesSell, userFund) -> {
        return stay()
            .applying(new CorporateMergerSellEvent(userFund.getUserFundCompositeKey().getUsername(),
                                                    userFund.getUserFundCompositeKey().getAdviser(), 
                                                    userFund.getUserFundCompositeKey().getFund(),
                                                    userFund.getUserFundCompositeKey().getInvestingMode(),
                                                    specialCircumstancesSell.getAdviseId(),
                                                    specialCircumstancesSell.getToken(),
                                                    specialCircumstancesSell.getPrice(), 
                                                    OrderSide.SELL)).
            andThen(exec(fund -> {
                  String id = "" + new UserAdviseCompositeKey(
                      specialCircumstancesSell.getAdviseId(), 
                      specialCircumstancesSell.getInvestorUsername(), 
                      specialCircumstancesSell.getFund(), 
                      specialCircumstancesSell.getAdviserUsername(),
                      specialCircumstancesSell.getInvestingMode()).persistenceId();
                  
                  forwardToChildren(context(), self(), specialCircumstancesSell, id);
            }));

      }).
      event(SellOrderDetails.class, 
          (sellOrderDetails, userFund) -> {
        return stay()
            .applying(new CorporateActionSellDetails(userFund.getUserFundCompositeKey().getUsername(),
                                                    userFund.getUserFundCompositeKey().getAdviser(), 
                                                    userFund.getUserFundCompositeKey().getFund(),
                                                    userFund.getUserFundCompositeKey().getInvestingMode(),
                                                    sellOrderDetails.getToken(),
                                                    sellOrderDetails.getAvgPrice(),
                                                    sellOrderDetails.getQuantity(),
                                                    sellOrderDetails.getAbsoluteAllocation(),
                                                    sellOrderDetails.getAllocationValue()));
      }).
      event(MergerDemergerPrimaryBuyIssued.class, 
          (mergerDemergerPrimaryBuy, userFund) -> {
            Map<String , UserAdviseDetails> tokenToAdviseDetails = userFund.getCorpEventTokensToAdviseDetails();
            boolean wdIdInAdvises = false;
            if(tokenToAdviseDetails.containsKey(mergerDemergerPrimaryBuy.getToken())){
              wdIdInAdvises = true;
            }
            return (wdIdInAdvises) ? true : false ;
      },
          (mergerDemergerPrimaryBuy, userFund) -> 
      {
        return stay()
            .applying(new CorporateMergerPrimaryBuyEvent(userFund.getUserFundCompositeKey().getUsername(),
                                                    userFund.getUserFundCompositeKey().getAdviser(), 
                                                    userFund.getUserFundCompositeKey().getFund(),
                                                    userFund.getUserFundCompositeKey().getInvestingMode(),
                                                    mergerDemergerPrimaryBuy.getToken(),
                                                    mergerDemergerPrimaryBuy.getAdviseId(),
                                                    mergerDemergerPrimaryBuy.getOldPrice(),
                                                    mergerDemergerPrimaryBuy.getNewPrice(),
                                                    mergerDemergerPrimaryBuy.getSharesRatio(),
                                                    OrderSide.BUY,
                                                    mergerDemergerPrimaryBuy.getSymbol())).
            andThen(exec(fund -> {
              Map<String , UserAdviseDetails> tokenToAdviseDetails = userFund.getCorpEventTokensToAdviseDetails();
              UserAdviseDetails adviseDetails = tokenToAdviseDetails.get(mergerDemergerPrimaryBuy.getToken());
              int newQuantity =  calculateNewAdviseQuantity(mergerDemergerPrimaryBuy.getSharesRatio(),adviseDetails.getQuantity());
              double newAverageEntryPrice = adviseDetails.getPrimaryAdviseAvgPrice();
              double absolueAllocation = adviseDetails.getPrimaryAdviseAllocation();
              double allocationValue = calculateNewAdviseValue(newQuantity, newAverageEntryPrice );
              PriceWithPaf avgEntryPrice = getPrice(newAverageEntryPrice, mergerDemergerPrimaryBuy.getToken());
              UserAdvise userAdvise = fund.getActiveAdvises().stream().filter(advise -> advise.getAdviseId().equals(mergerDemergerPrimaryBuy.getAdviseId())).findFirst().get();
              forwardToChildren(
                  context(), 
                  self(), 
                  fundAdviceWithUndefinedExitParams(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(), 
                  userAdvise.getToken(), 
                  userAdvise.getSymbol(),
                  "",
                  round(absolueAllocation),
                  allocationValue,
                  avgEntryPrice,
                  false,
                  mergerDemergerPrimaryBuy.getBrokerName(),
                  mergerDemergerPrimaryBuy.getClientCode(),
                  getIssueType(userAdvise.getToken()),
                  true,
                  userFund.getBasketOrderId()+CompositeKeySeparator+"1",
                  BrokerAuthInformation.withNoBrokerAuth()),
                  "" + new UserAdviseCompositeKey(userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode()).persistenceId());
            }));

      }).
      event(MergerDemergerSecondaryBuyIssued.class, 
          (mergerDemergerSecondaryBuy, userFund) -> {
            Map<String , UserAdviseDetails> tokenToAdviseDetails = userFund.getCorpEventTokensToAdviseDetails();
            boolean wdIdInAdvises = false;
            if(tokenToAdviseDetails.containsKey(mergerDemergerSecondaryBuy.getOldToken())){
              wdIdInAdvises = true;
            }
            return (wdIdInAdvises) ? true : false ;
      },
          (mergerDemergerSecondaryBuy, userFund) -> 
      {
        return stay()
            .applying(new CorporateMergerSecondaryBuyEvent(userFund.getUserFundCompositeKey().getUsername(),
                                                    userFund.getUserFundCompositeKey().getAdviser(), 
                                                    userFund.getUserFundCompositeKey().getFund(),
                                                    userFund.getUserFundCompositeKey().getInvestingMode(),
                                                    mergerDemergerSecondaryBuy.getOldToken(),
                                                    mergerDemergerSecondaryBuy.getNewToken(),
                                                    mergerDemergerSecondaryBuy.getAdviseId(),
                                                    OrderSide.BUY,
                                                    mergerDemergerSecondaryBuy.getSharesRatio(),
                                                    mergerDemergerSecondaryBuy.getCashAllocationRatio(),
                                                    mergerDemergerSecondaryBuy.getSymbol())).
            andThen(exec(fund -> {
              Map<String , UserAdviseDetails> tokenToAdviseDetails = userFund.getCorpEventTokensToAdviseDetails();
              UserAdviseDetails adviseDetails = tokenToAdviseDetails.get(mergerDemergerSecondaryBuy.getOldToken());
              int newQuantity =  calculateNewAdviseQuantity(mergerDemergerSecondaryBuy.getSharesRatio(), adviseDetails.getQuantity()); 
              double newAverageEntryPrice = calculateSecondaryAdvisePrice(adviseDetails.averagePriceDifferential(), mergerDemergerSecondaryBuy.getCashAllocationRatio(), mergerDemergerSecondaryBuy.getSharesRatio());
              double absolueAllocation = calculateAdviseAllocation(adviseDetails.allocationPending() , mergerDemergerSecondaryBuy.getCashAllocationRatio());
              double allocationValue = calculateNewAdviseValue(newQuantity, newAverageEntryPrice );
              PriceWithPaf avgEntryPrice = getPrice(newAverageEntryPrice,  mergerDemergerSecondaryBuy.getNewToken());
              UserAdvise userAdvise = fund.getActiveAdvises().stream().filter(advise -> advise.getAdviseId().equals(mergerDemergerSecondaryBuy.getAdviseId())).findFirst().get();
              forwardToChildren(
                  context(), 
                  self(), 
                  fundAdviceWithUndefinedExitParams(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(), 
                  userAdvise.getToken(), 
                  userAdvise.getSymbol(),
                  "",
                  round(absolueAllocation),
                  allocationValue,
                  avgEntryPrice,
                  false,
                  mergerDemergerSecondaryBuy.getBrokerName(),
                  mergerDemergerSecondaryBuy.getClientCode(),
                  getIssueType(userAdvise.getToken()),
                  true,
                  userFund.getBasketOrderId()+CompositeKeySeparator+"1",
                  BrokerAuthInformation.withNoBrokerAuth()),
                  "" + new UserAdviseCompositeKey(userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode()).persistenceId());
              
              self().tell(new UpdateCorporateActionsMap(mergerDemergerSecondaryBuy.getOldToken()), ActorRef.noSender());
            }));

      })
      .
      event(UpdateCorporateActionsMap.class, (updateCorporateActionsMap, userFund) -> {
        return stay()
            .applying(new UpdatingCorporateActionsMap(
                userFund.getUserFundCompositeKey().getUsername(),
                userFund.getUserFundCompositeKey().getAdviser(), 
                userFund.getUserFundCompositeKey().getFund(),
                userFund.getUserFundCompositeKey().getInvestingMode(),
                updateCorporateActionsMap.getOldToken()));
      }).
      event(SpecialCircumstancesBuy.class, 
          (specialCircumstanceBuy, userFund) -> {
            String adviseId = specialCircumstanceBuy.getAdviseId();
            return userFund.getActiveAdviseIdsTraded().stream().filter(adviseIdTraded -> adviseIdTraded.equals(adviseId)).findFirst().isPresent() ? false : true;
          },
          (specialCircumstanceBuy, userFund) -> {
        return stay()
            .applying(new SpecialCircumstancesBuyEvent(userFund.getUserFundCompositeKey().getUsername(),
                                                    userFund.getUserFundCompositeKey().getAdviser(), 
                                                    userFund.getUserFundCompositeKey().getFund(),
                                                    userFund.getUserFundCompositeKey().getInvestingMode(),
                                                    specialCircumstanceBuy.getToken(),
                                                    specialCircumstanceBuy.getSymbol(),
                                                    specialCircumstanceBuy.getQuantity(),
                                                    specialCircumstanceBuy.getPrice(),
                                                    specialCircumstanceBuy.getAdviseId(),
                                                    OrderSide.BUY,
                                                    specialCircumstanceBuy.getClientCode(),
                                                    specialCircumstanceBuy.getBrokerName()
                                                    )).
            andThen(exec(fund -> {
              double absolueAllocation = (specialCircumstanceBuy.getPrice() * specialCircumstanceBuy.getQuantity()) / userFund.getCashComponent();
              double allocationValue = specialCircumstanceBuy.getPrice() * specialCircumstanceBuy.getQuantity();
              InstrumentPrice instrumentPrice = new InstrumentPrice(specialCircumstanceBuy.getPrice(), DateUtils.currentTimeInMillis(), specialCircumstanceBuy.getToken());
              PriceWithPaf avgEntryPrice = new PriceWithPaf(instrumentPrice , new Pafs());
              UserAdvise userAdvise = fund.getActiveAdvises().stream().filter(advise -> advise.getAdviseId().equals(specialCircumstanceBuy.getAdviseId())).findFirst().get();
              forwardToChildren(
                  context(), 
                  self(), 
                  fundAdviceWithUndefinedExitParams(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                  userAdvise.getUserAdviseCompositeKey().getFund(),
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getAdviseId(), 
                  userAdvise.getToken(), 
                  specialCircumstanceBuy.getSymbol(),
                  "",
                  absolueAllocation,
                  allocationValue,
                  avgEntryPrice,
                  false,
                  specialCircumstanceBuy.getBrokerName(),
                  specialCircumstanceBuy.getClientCode(),
                  getIssueType(userAdvise.getToken()),
                  true,
                  userFund.getBasketOrderId()+CompositeKeySeparator+"1",
                  BrokerAuthInformation.withNoBrokerAuth()),
                  "" + new UserAdviseCompositeKey(userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode()).persistenceId());
            }));

      })
      .
      event(SpecialCircumstancesBuy.class, 
          (specialCircumstanceBuy, userFund) -> {
            String adviseId = specialCircumstanceBuy.getAdviseId();
            String token = specialCircumstanceBuy.getToken();
            return (userFund.getActiveAdvises().stream().filter(o -> o.getAdviseId().equals(adviseId) && o.getToken().equals(token)).findFirst().isPresent())? true : false; 
          },
          (specialCircumstanceBuy, userFund) -> {
        return stay()
            .applying(new SpecialCircumstancesBuyEvent(userFund.getUserFundCompositeKey().getUsername(),
                                                    userFund.getUserFundCompositeKey().getAdviser(), 
                                                    userFund.getUserFundCompositeKey().getFund(),
                                                    userFund.getUserFundCompositeKey().getInvestingMode(),
                                                    specialCircumstanceBuy.getToken(),
                                                    specialCircumstanceBuy.getSymbol(),
                                                    specialCircumstanceBuy.getQuantity(),
                                                    specialCircumstanceBuy.getPrice(),
                                                    specialCircumstanceBuy.getAdviseId(),
                                                    OrderSide.BUY,
                                                    specialCircumstanceBuy.getClientCode(),
                                                    specialCircumstanceBuy.getBrokerName()
                                                    )).
            andThen(exec(fund -> {
              double absolueAllocation = (specialCircumstanceBuy.getPrice() * specialCircumstanceBuy.getQuantity()) / userFund.getCashComponent();
              double allocationValue = specialCircumstanceBuy.getPrice() * specialCircumstanceBuy.getQuantity();
              InstrumentPrice instrumentPrice = new InstrumentPrice(specialCircumstanceBuy.getPrice(), DateUtils.currentTimeInMillis(), specialCircumstanceBuy.getToken());
              PriceWithPaf avgEntryPrice = new PriceWithPaf(instrumentPrice , new Pafs());
              UserAdvise userAdvise = fund.getActiveAdvises().stream().filter(advise -> advise.getAdviseId().equals(specialCircumstanceBuy.getAdviseId())).findFirst().get();
              UserFundAdditionalSumAdvice fundAdviceIssuedWithUserEmail = fundLumpSumAdviceWithUndefinedExitParams(
                  userAdvise.getUserAdviseCompositeKey().getUsername(), 
                  userAdvise.getUserAdviseCompositeKey().getAdviser(), 
                  userAdvise.getUserAdviseCompositeKey().getFund(), 
                  userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                  userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                  userAdvise.getToken(), 
                  specialCircumstanceBuy.getSymbol(),
                  "Replicating previously issued advices in lumpsum",
                  absolueAllocation,   
                  allocationValue,   
                  avgEntryPrice,
                  false,
                  specialCircumstanceBuy.getBrokerName(),
                  specialCircumstanceBuy.getClientCode(),
                  getIssueType(userAdvise.getToken()),
                  true,
                  userFund.getBasketOrderId()+CompositeKeySeparator+"1",
                  BrokerAuthInformation.withNoBrokerAuth());
              
              forwardToChildren(
                  context(), 
                  self(), 
                  fundAdviceIssuedWithUserEmail,
                  "" + new UserAdviseCompositeKey(userAdvise.getAdviseId(),
                      userAdvise.getUserAdviseCompositeKey().getUsername(),
                      userAdvise.getUserAdviseCompositeKey().getFund(),
                      userAdvise.getUserAdviseCompositeKey().getAdviser(),
                      userAdvise.getUserAdviseCompositeKey().getInvestingMode()).persistenceId());
            }));

      })
        );
    
    when(WITHDRAWING, create(15, MINUTES),
        matchEvent(CurrentUserFundValue.class,
            (currentUserFundPortfolio, userFund) -> {
          return stay()
              .applying(new CurrentFundPortfolioValue(userFund.getUserFundCompositeKey().getUsername(),
              userFund.getUserFundCompositeKey().getAdviser(), userFund.getUserFundCompositeKey().getFund(),
              userFund.getUserFundCompositeKey().getInvestingMode(), currentUserFundPortfolio.getCurrentFundValue()))
              .andThen(exec(fund -> {
                double portFolioValue =  currentUserFundPortfolio.getCurrentFundValue();
                double allocationValue = (portFolioValue > 0.) ?  ((userFund.getWithdrawalAmountRequested()/portFolioValue) * 100) : 0.;
                Map<String, Double> adviseIdToExecutedSharesforWithdrawal = fund.getAdviseIdToExecutedSharesforWithdrawal(allocationValue);
                int i = 1;
                for (UserAdvise userAdvise : fund.getActiveAdvises()) {
                  if (adviseIdToExecutedSharesforWithdrawal.containsKey(userAdvise.getAdviseId())) {
                    UserAdviseTransactionReceived userAdviseTransactionReceived = new UserAdviseTransactionReceived.UserAdviseTransactionReceivedBuilder(
                        userAdvise.getUserAdviseCompositeKey().getUsername(),
                        userAdvise.getUserAdviseCompositeKey().getAdviser(),
                        userAdvise.getUserAdviseCompositeKey().getFund(),
                        userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
                        userAdvise.getAdviseId(),
                        getIssueType(userAdvise.getToken())).withUserAcceptanceMode(Auto)
                            .withAbsoluteAllocation(allocationValue)
                            .withExitPrice(noPrice(userAdvise.getToken()))
                            .withPurpose(PARTIALWITHDRAWAL)
                            .withBasketOrderId(userFund.getBasketOrderId()+CompositeKeySeparator+valueOf(i))
                            .withBrokerAuthentication(currentUserFundPortfolio.getBrokerAuthInformation()).build();

                    forwardToChildren(context(), self(), userAdviseTransactionReceived,
                        "" + new UserAdviseCompositeKey(userAdvise.getAdviseId(),
                            fund.getUserFundCompositeKey().getUsername(),
                            fund.getUserFundCompositeKey().getFund(),
                            fund.getUserFundCompositeKey().getAdviser(),
                            fund.getUserFundCompositeKey().getInvestingMode()).persistenceId());
                  }
                  i++;
                }
              }));
        }).event(UserFundWithdrawalDoneSuccessfully.class, (userFundWithdrawalDoneSuccessfully, userFund) -> {
          return goTo(SUBSCRIBED).applying(userFundWithdrawalDoneSuccessfully).andThen(exec(fund -> {
            wdActorSelection.userRootActor().tell(userFundWithdrawalDoneSuccessfully, self());
            self().tell(new CalculateUserFundComposition(fund.getUserFundCompositeKey()), self());
          }));
        }).event(AdviseTradesExecuted.class, (adviseTradesExecuted, userFund) -> {
          return stay()
              .applying(new FundAdviceTradeExecutedForWithdrawal(userFund.getUserFundCompositeKey().getUsername(),
                  userFund.getUserFundCompositeKey().getAdviser(), userFund.getUserFundCompositeKey().getFund(),
                  userFund.getUserFundCompositeKey().getInvestingMode(), adviseTradesExecuted.getAdviseId(),
                  adviseTradesExecuted.getUserAdviseState(), adviseTradesExecuted.getAllocationValue(),
                  adviseTradesExecuted.getTradedValue()))
              .andThen(exec(fund -> {
                double portFolioValue = fund.getCurrentFundValue();
                double allocationpct = (portFolioValue > 0.) ?  ((userFund.getWithdrawalAmountRequested()/portFolioValue) * 100) : 0.;
                if (userFund.getInterimExcecutedAdvisesForWithdrawal().size() == (fund.getAdviseIdToExecutedSharesforWithdrawal(allocationpct).size())) {
                  self().forward(new UserFundWithdrawalDoneSuccessfully(fund.getUserFundCompositeKey().getUsername(),
                      fund.getUserFundCompositeKey().getAdviser(), fund.getUserFundCompositeKey().getFund(),
                      fund.getUserFundCompositeKey().getInvestingMode(), fund.getUsername(),
                      fund.getActualWithdrawalAmount()), context());
                }
              }));
        }).
        event(UserCloseAdviseOrderRejected.class, (userCloseAdviseOrderRejected, userFund) -> {
          return stay()
              .applying(new UserAdviseWithdrawalOrderRejected(
                  stateName(),
                  userCloseAdviseOrderRejected.getAdviseId(),
                  userCloseAdviseOrderRejected.getAdviserUsername(),
                  userCloseAdviseOrderRejected.getUsername(),
                  userCloseAdviseOrderRejected.getFundName(),
                  userCloseAdviseOrderRejected.getInvestingMode()));
        }).
        event(StateTimeout$.class,
            (stateTimeout, userFund) -> {
              if(!userFund.containsOnlyEquityAdvises())
                return stay();
              return stay()
                  .applying(new WithdrawingStateTimedOut(userFund.getUserFundCompositeKey().getUsername(),
                      userFund.getUserFundCompositeKey().getAdviser(), userFund.getUserFundCompositeKey().getFund(),
                      userFund.getUserFundCompositeKey().getInvestingMode()))
                  .andThen(exec(fund -> {
                    double allocationpct = (fund.getWithdrawalAmountRequested() / fund.getCurrentFundValue()) * 100;
                    Map<String, Double> adviseIdToExecutedSharesforWithdrawal = fund.getAdviseIdToExecutedSharesforWithdrawal(allocationpct);
                    for (UserAdvise userAdvise : userFund.getActiveAdvises()) {
                      if (adviseIdToExecutedSharesforWithdrawal.containsKey(userAdvise.getAdviseId()) && !fund.getInterimExcecutedAdvisesForWithdrawal().contains(userAdvise.getAdviseId())) {
                        self().tell(new UserCloseAdviseOrderRejected(
                            userAdvise.getUserAdviseCompositeKey().getUsername(),
                            userAdvise.getUserAdviseCompositeKey().getAdviser(),
                            userAdvise.getUserAdviseCompositeKey().getFund(),
                            userAdvise.getAdviseId(),
                            userAdvise.getUserAdviseCompositeKey().getInvestingMode()), self());
                      }
                    }
                  }));
        }));
    
    when(UNSUBSCRIBING, create(15, MINUTES),
        matchEvent(AdviseTradesExecuted.class, 
            (adviseTradesExecuted, userFund) ->
    {
    return stay().
    applying(new FundAdviceTradeExecuted(
        userFund.getUserFundCompositeKey().getUsername(),
        userFund.getUserFundCompositeKey().getAdviser(),
        userFund.getUserFundCompositeKey().getFund(),
        userFund.getUserFundCompositeKey().getInvestingMode(), 
        adviseTradesExecuted.getAdviseId(),
        adviseTradesExecuted.getUserAdviseState(),
        adviseTradesExecuted.getAllocationValue(),
        adviseTradesExecuted.getTradedValue())).
    andThen(exec(fund -> {
        if(fund.getActiveAdviseIdsTraded().size() == 0) {
          self().forward(new FundSuccessfullyUnsubscribed(
              fund.getUserFundCompositeKey().getUsername(),
              fund.getUserFundCompositeKey().getAdviser(),
              fund.getUserFundCompositeKey().getFund(),
              fund.getUserFundCompositeKey().getInvestingMode(), 
              fund.getCashComponentUpdatedWithCashBlockedForPendingAdvises()), context());
        }
        }));
    }).
        event(UserCloseAdviseOrderRejected.class, (userCloseAdviseOrderRejected, userFund) -> {
          return (!userFund.isFundArchived()) ? true : false;
        }, 
        (userCloseAdviseOrderRejected, userFund) ->{
          return goTo(SUBSCRIBED).
              applying(userCloseAdviseOrderRejected);
    }).
        event(UserCloseAdviseOrderRejected.class, (userCloseAdviseOrderRejected, userFund) -> {
        return (userFund.isFundArchived()) ? true : false;
      }, 
      (userCloseAdviseOrderRejected, userFund) ->{
          return goTo(ARCHIVED).
              applying(userCloseAdviseOrderRejected);
    }).
      event(FundSuccessfullyUnsubscribed.class, 
        (fundSuccessfullyUnsubscribed, userFund) -> 
    {
      return goTo(UNSUBSCRIBED).
          applying(fundSuccessfullyUnsubscribed).
          andThen(exec(fund -> {
            wdActorSelection.userRootActor().forward(new FundExited(fund.getUsername(), 
                fund.getUserFundCompositeKey().getAdviser(), 
                fund.getUserFundCompositeKey().getFund(), 
                fund.getUserFundCompositeKey().getInvestingMode(), "Successfully exited from " + fund.getUserFundCompositeKey().getFund(), fundSuccessfullyUnsubscribed.getCurrentCashComponent()), context());
          })).
          using(new UserFund());
    }).
      event(StateTimeout$.class, 
          (stateTimeout, userFund) -> {
            return (userFund.isFundArchived()) ? true : false;
          }, 
          (stateTimeout, userFund) ->{
        return goTo(ARCHIVED).
        applying(new UnsubscribingStateTimedOut(
            userFund.getUserFundCompositeKey().getUsername(),
            userFund.getUserFundCompositeKey().getAdviser(),
            userFund.getUserFundCompositeKey().getFund(),
            userFund.getUserFundCompositeKey().getInvestingMode())).
        andThen(exec(fund -> {
          for(UserAdvise userAdvise: userFund.getActiveAdvises()){
            String id = "" + new UserAdviseCompositeKey(
              userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
              userFund.getUserFundCompositeKey().getUsername(), 
              userFund.getUserFundCompositeKey().getFund(), 
              userFund.getUserFundCompositeKey().getAdviser(),
              userFund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
            IssueType issueType = userAdvise.getToken().endsWith("-EQ") ? IssueType.Equity : IssueType.MutualFund;
              forwardToChildren(context(), self(), new UpdateAdviseTradeStatus(issueType), id);
           }
        }));
      })
      .
      event(StateTimeout$.class, 
          (stateTimeout, userFund) -> {
            return (userFund.isFundArchived()) ? false : true;
          }, 
          (stateTimeout, userFund) ->{
        return goTo(SUBSCRIBED).
        applying(new UnsubscribingStateTimedOut(
            userFund.getUserFundCompositeKey().getUsername(),
            userFund.getUserFundCompositeKey().getAdviser(),
            userFund.getUserFundCompositeKey().getFund(),
            userFund.getUserFundCompositeKey().getInvestingMode())).
        andThen(exec(fund -> {
          for(UserAdvise userAdvise: userFund.getActiveAdvises()){
            String id = "" + new UserAdviseCompositeKey(
              userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
              userFund.getUserFundCompositeKey().getUsername(), 
              userFund.getUserFundCompositeKey().getFund(), 
              userFund.getUserFundCompositeKey().getAdviser(),
              userFund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
            IssueType issueType = userAdvise.getToken().endsWith("-EQ") ? IssueType.Equity : IssueType.MutualFund;
            if(userFund.getActiveAdviseIdsTraded().contains(userAdvise.getAdviseId()))
              forwardToChildren(context(), self(), new UpdateAdviseTradeStatus(issueType), id);
           }
        }));
          }));
    
    when(UNSUBSCRIBINGWITHOUTTIMEOUT,
        matchEvent(AdviseTradesExecuted.class, 
            (adviseTradesExecuted, userFund) ->
    {
    return stay().
    applying(new FundAdviceTradeExecuted(
        userFund.getUserFundCompositeKey().getUsername(),
        userFund.getUserFundCompositeKey().getAdviser(),
        userFund.getUserFundCompositeKey().getFund(),
        userFund.getUserFundCompositeKey().getInvestingMode(), 
        adviseTradesExecuted.getAdviseId(),
        adviseTradesExecuted.getUserAdviseState(),
        adviseTradesExecuted.getAllocationValue(),
        adviseTradesExecuted.getTradedValue())).
    andThen(exec(fund -> {
        if (fund.getActiveAdviseIdsTraded().size() == 0) {
          self().forward(new FundSuccessfullyUnsubscribed(
              fund.getUserFundCompositeKey().getUsername(),
              fund.getUserFundCompositeKey().getAdviser(),
              fund.getUserFundCompositeKey().getFund(),
              fund.getUserFundCompositeKey().getInvestingMode(), 
              fund.getCashComponentUpdatedWithCashBlockedForPendingAdvises()), context());
        }
        }));
    }).
        event(UserCloseAdviseOrderRejected.class, (userCloseAdviseOrderRejected, userFund) -> {
          return (!userFund.isFundArchived()) ? true : false;
        }, 
        (userCloseAdviseOrderRejected, userFund) ->{
          return goTo(SUBSCRIBED).
              applying(userCloseAdviseOrderRejected);
    }).
        event(UserCloseAdviseOrderRejected.class, (userCloseAdviseOrderRejected, userFund) -> {
        return (userFund.isFundArchived()) ? true : false;
      }, 
      (userCloseAdviseOrderRejected, userFund) ->{
          return goTo(ARCHIVED).
              applying(userCloseAdviseOrderRejected);
    }).
      event(FundSuccessfullyUnsubscribed.class, 
        (fundSuccessfullyUnsubscribed, userFund) -> 
    {
      return goTo(UNSUBSCRIBED).
          applying(fundSuccessfullyUnsubscribed).
          andThen(exec(fund -> {
            wdActorSelection.userRootActor().forward(new FundExited(fund.getUsername(), 
                fund.getUserFundCompositeKey().getAdviser(), 
                fund.getUserFundCompositeKey().getFund(), 
                fund.getUserFundCompositeKey().getInvestingMode(), "Successfully exited from " + fund.getUserFundCompositeKey().getFund(), fundSuccessfullyUnsubscribed.getCurrentCashComponent()), context());
          })).
          using(new UserFund());
    }));

    when(ARCHIVED,
        matchEvent(UnSubscribeToFundWithUserEmail.class, 
        (unSubscribeToFundWithUserEmail, userFund) -> { 
          return (userFund.getActiveAdviseIdsTraded().size() == 0) ? true : false;
        }, 
        (unSubscribeToFundWithUserEmail, userFund) ->{
          
          return goTo(UNSUBSCRIBING).
              applying(new FundUserUnsubscribed(userFund.getUserFundCompositeKey().getUsername(),
                  unSubscribeToFundWithUserEmail.getAdviserUsername(), 
                  unSubscribeToFundWithUserEmail.getFundName(), 
                  unSubscribeToFundWithUserEmail.getInvestingMode(),
                  unSubscribeToFundWithUserEmail.getBasketOrderId())).
              andThen(exec(fund -> {
                    self().forward(new FundSuccessfullyUnsubscribed(
                        fund.getUserFundCompositeKey().getUsername(),
                        fund.getUserFundCompositeKey().getAdviser(),
                        fund.getUserFundCompositeKey().getFund(),
                        fund.getUserFundCompositeKey().getInvestingMode(),
                        fund.getCashComponent()), context());
              sender().tell(new Done(userFund.getUserFundCompositeKey().getUsername(),
                        "UnSubscribe in process. You will receive a notification once its done."), self());   
                  }));
        }).
        event(GetUnsubscriptionSnapshotOfFund.class, 
            (getUnsubscriptionSnapshotOfFund, userFund) -> {
              if(userFund.getAdviseIdsWithPendingTradeConfirmations().size() == 0){
                  appriseInvestorWithPreWithdrawalSnapshot(userFund, 100.);
              }else{
                sender().tell(failed(userFund.getUserFundCompositeKey().getFund(),
                    "Cannot unsubscribe as previous trade confirmations as expected", StatusCodes.NOT_ACCEPTABLE), self());
              }
              return stay();
       }).
        event(CorporateEventUpdateWithFundNames.class, 
            (corporateEventUpdate, userFund) -> {
              for(UserAdvise userAdvise: userFund.getActiveAdvises()){
               String id = "" + new UserAdviseCompositeKey(
                 userAdvise.getUserAdviseCompositeKey().getAdviseId(), 
                 userFund.getUserFundCompositeKey().getUsername(), 
                 userFund.getUserFundCompositeKey().getFund(), 
                 userFund.getUserFundCompositeKey().getAdviser(),
                 userFund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
               if(corporateEventUpdate.getWdId().equals(userAdvise.getToken())) 
                 forwardToChildren(context(), self(), corporateEventUpdate, id);
              }
              return stay();
            }).
    event(UnSubscribeToFundWithUserEmail.class,  
        (unSubscribeToFundWithUserEmail, userFund) -> { 
          return (userFund.getActiveAdviseIdsTraded().size() == 0) ? false : true;
        },
        (unSubscribeToFundWithUserEmail, userFund) -> {
          UserFundState userFundState = userFund.containsOnlyEquityAdvises() ? UNSUBSCRIBING : UNSUBSCRIBINGWITHOUTTIMEOUT;
          
          return goTo(userFundState).
              applying(new FundUserUnsubscribed(userFund.getUserFundCompositeKey().getUsername(),
                  unSubscribeToFundWithUserEmail.getAdviserUsername(), 
                  unSubscribeToFundWithUserEmail.getFundName(), 
                  unSubscribeToFundWithUserEmail.getInvestingMode(),
                  unSubscribeToFundWithUserEmail.getBasketOrderId())).
              andThen(exec(fund -> {
                sender().tell(new Done(userFund.getUserFundCompositeKey().getUsername(),
                    "UnSubscribe in process. You will receive a notification once its done."), self());
                int i = 1;
                    for (UserAdvise userAdvise : fund.getActiveAdvises()) {
                      if (fund.getActiveAdviseIdsTraded().contains(userAdvise.getAdviseId())) {
                        UserAdviseTransactionReceived userAdviseTransactionReceived = 
                            new UserAdviseTransactionReceived.UserAdviseTransactionReceivedBuilder(
                                userAdvise.getUserAdviseCompositeKey().getUsername(),
                                userAdvise.getUserAdviseCompositeKey().getAdviser(),
                                userAdvise.getUserAdviseCompositeKey().getFund(),
                                userAdvise.getUserAdviseCompositeKey().getInvestingMode(), 
                                userAdvise.getAdviseId(),
                                getIssueType(userAdvise.getToken()))
                            .withUserAcceptanceMode(Auto)
                            .withAbsoluteAllocation(100.)
                            .withExitPrice(noPrice(userAdvise.getToken()))
                            .withBasketOrderId(userFund.getBasketOrderId()+CompositeKeySeparator+valueOf(i))
                            .withBrokerAuthentication(unSubscribeToFundWithUserEmail.getBrokerAuthInformation())
                            .build();
                        
                        forwardToChildren(context(), self(), userAdviseTransactionReceived, "" + new UserAdviseCompositeKey(
                            userAdvise.getAdviseId(), 
                            fund.getUserFundCompositeKey().getUsername(), 
                            fund.getUserFundCompositeKey().getFund(), 
                            fund.getUserFundCompositeKey().getAdviser(),
                            fund.getUserFundCompositeKey().getInvestingMode()).persistenceId());
                      }
                      i++;
                    }
                  }));
        }));
    
    whenUnhandled(
        matchEvent(SubscribeToFundWithUserEmail.class, 
            (subscribeToFundWithUserEmail, userFund) -> {
          return stay()
                  .replying(failed(subscribeToFundWithUserEmail.getFundName(),
                      "Cannot re-subscribe while subscription is in process!", StatusCodes.NOT_ACCEPTABLE));
        }).event(UnSubscribeToFundWithUserEmail.class, 
            (unSubscribeToFundWithUserEmail, userFund) -> {
          return stay()
                      .replying(failed(unSubscribeToFundWithUserEmail.getFundName(),
                          "Cannot unsubscribe at this moment while "+stateName().toString().toLowerCase(), StatusCodes.NOT_ACCEPTABLE));
        }).event(AddLumpsumToFundWithUserEmail.class,
            (addLumpsumToFundWithUserEmail, userFund) -> {
          return stay()
                  .replying(Failed.failed(addLumpsumToFundWithUserEmail.getUsername(),
                      "Cannot add lumpsum at this moment while "+stateName().toString().toLowerCase(), StatusCodes.NOT_ACCEPTABLE));
        }).event(PartialWithdrawalUnderway.class,
            (partialWithdrawalUnderway, userFund) -> {
          return stay()
                  .replying(Failed.failed(partialWithdrawalUnderway.getUsername(),
                      "Cannot do partial withdrawal at this moment while "+stateName().toString().toLowerCase(), StatusCodes.NOT_ACCEPTABLE));
        }).event(UpdateUserFundCompositionList.class, 
            (updateUserFundCompositionList, userFund) -> {
              if(userFund.getActiveAdviseIdsTraded().contains(updateUserFundCompositionList.getUserAdviseCompositeKey().getAdviseId()))
                adviseIdToShares.put(updateUserFundCompositionList.getUserAdviseCompositeKey().persistenceId(), updateUserFundCompositionList.getQuantity());
              if (adviseIdToShares.size() == userFund.getActiveAdviseIdsTraded().size()) {
                return stay()
                    .applying(new UpdateUserFundComposition(userFund.getUserFundCompositeKey().getUsername(),
                        userFund.getUserFundCompositeKey().getFund(),
                        userFund.getUserFundCompositeKey().getAdviser(),
                        userFund.getUserFundCompositeKey().getInvestingMode(),
                        adviseIdToShares))
                    .andThen(exec(fund -> {
                          adviseIdToShares.clear();
                    }));
              } else if (userFund.getActiveAdviseIdsTraded().size() == 0)
                adviseIdToShares.clear();
              return stay();
        }).event(GetUserFundStatus.class, 
            (fundStatus, userFund) -> {
          return stay()
              .replying(new UserFundSubscriptionDetails(userFund.getUserFundCompositeKey().getUsername(),
                              userFund.getUserFundCompositeKey().getFund(),
                              userFund.getUserFundCompositeKey().getAdviser(),
                              userFund.getUserFundCompositeKey().getInvestingMode(),
                              userFund.getTotalInvestment(),
                              userFund.getWithdrawal(), userFund.isSubscribed() ? "true" : "false",
                              userFund.isFundArchived() ? "true" : "false"));
        }).event(GetPendingUserFundApprovals.class, 
            (fundStatus, userFund) -> {
              List<UserResponseParameters> pendingApprovalRequests = appendResponsesWithMissingParameters(userFund.getPendingApprovalRequests());
          return stay()
              .replying(new AllPendingUserFundApprovals(userFund.getUserFundCompositeKey().getFund(), pendingApprovalRequests, userFund.getAdviseIdsWithPendingTradeConfirmations().size()==0 ));
        }).event(GetRejectedAdviseFromUserFund.class, 
            (rejectedStatus, userFund) -> {
              List<UserResponseParameters> vanillaRejectedAdviseDetails = new ArrayList<UserResponseParameters>();
              userFund.getActiveRejectedBuySideAdvises().stream().forEach(userAdvise -> {
                UserResponseParameters userRejectedAdvise = new UserResponseParameters(userAdvise.getAdviseId(), userFund.getUserFundCompositeKey().getFund(),
                    userAdvise.getClientCode(), Issue, userAdvise.getUsername(), userAdvise.getBrokerName().getBrokerName(),
                    userAdvise.getToken().split(HYPHEN)[1], userFund.getUserFundCompositeKey().getAdviser(), userAdvise.getToken(), userAdvise.getSymbol(),
                    userFund.getUserFundCompositeKey().getInvestingMode(), Awaiting, userAdvise.getIssueAdviceAbsoluteAllocation(),
                    userAdvise.getIssueAdviseAllocationValue(), 0, userAdvise.getIssueTime());
                vanillaRejectedAdviseDetails.add(userRejectedAdvise);
              });
              List<UserResponseParameters> rejectedAdviseDetails = appendResponsesWithMissingParameters(vanillaRejectedAdviseDetails);
          return stay()
              .replying(new AllRejectedUserFundAdvises(userFund.getUserFundCompositeKey().getFund(), rejectedAdviseDetails));
        }).event(CalculateUserFundComposition.class, 
            (calculateUserFundComposition, userFund) -> {
              if(userFund.getActiveAdviseIdsTraded().size() > 0){
                for (UserAdvise userAdvise : userFund.getActiveAdvises()) {
                  String userAdvisePersistenceId = new UserAdviseCompositeKey(
                      userAdvise.getAdviseId(), 
                      userFund.getUserFundCompositeKey().getUsername(), 
                      userFund.getUserFundCompositeKey().getFund(), 
                      userFund.getUserFundCompositeKey().getAdviser(),
                      userFund.getUserFundCompositeKey().getInvestingMode()).persistenceId();
                  if(userFund.getActiveAdviseIdsTraded().contains(userAdvise.getAdviseId()))
                    forwardToChildren(context(), self(), new CalculateUserAdviseComposition(getIssueType(userAdvise.getToken())),
                        userAdvisePersistenceId);
                }
              }
              return stay();
        }).event(GetUserCurrentApprovalStatus.class, 
            (userCurrentApprovalStatus, userFund) -> {
          return stay()
              .replying(new Done(userFund.getUserFundCompositeKey().getFund(), userFund.isAutoApprovedOn() ? ("" + Auto) : ("" + Manual)));
        }).event(AcceptUserAcceptanceMode.class, 
            (acceptUserAcceptanceMode, userFund) -> {
          return stay()
              .applying(new UserAcceptanceModeUpdateReceived(
                  userFund.getUsername(), 
                  userFund.getUserFundCompositeKey().getAdviser(), 
                  userFund.getUserFundCompositeKey().getFund(), 
                  userFund.getUserFundCompositeKey().getInvestingMode(),
                  acceptUserAcceptanceMode.getUserAcceptanceMode()))
              .replying(new Done(userFund.getUserFundCompositeKey().getFund(), "Acceptance mode updated to " + acceptUserAcceptanceMode.getUserAcceptanceMode() 
              + " for fund " + userFund.getUserFundCompositeKey().getFund()));
        }).
        event(SaveSnapshotSuccess.class, 
            (saveSnapshotSuccess, userFund) -> {
              handleSnapshotSuccess(saveSnapshotSuccess);
          return stay();
        }).
        event(ForceSaveSnapshot.class,
            (forceSaveSnapshot, userFund) -> {
              log.debug("Received force saving snapshot for " + persistenceId);
              saveStateSnapshot();
              return stay();
        }).event(SaveSnapshotFailure.class, 
            (saveSnapshotFailure, userFund) -> {
              handleSnapshotFailure(saveSnapshotFailure);
          return stay();
        }).
        event(ExchangeOrderUpdateEvent.class, 
            (exchangeOrderUpdateEvent, userFund) -> {
              String userAdvisePersistenceId = exchangeOrderUpdateEvent.getDestinationAddress().getSenderPath();
              boolean doesAdviseExist = userFund.getActiveAdvises().stream()
                                        .anyMatch(advise -> advise.getUserAdviseCompositeKey().persistenceId().equals(userAdvisePersistenceId));
              if (doesAdviseExist) {
                UserAdvise userAdvise = userFund.getActiveAdvises().stream()
                .filter(advise -> advise.getUserAdviseCompositeKey().persistenceId().equals(userAdvisePersistenceId))
                .findFirst().get();
                ExchangeOrderUpdateEventWithIssueType exchangeOrderUpdateEventWithIssueType = new ExchangeOrderUpdateEventWithIssueType(exchangeOrderUpdateEvent, getIssueType(userAdvise.getToken()));
                forwardToChildren(context(), self(), exchangeOrderUpdateEventWithIssueType, userAdvisePersistenceId);
              } 
              return stay();
        }).
        event(UserFundDividendReceived.class, 
            (userFundDividendReceived, userFund) -> {
              return stay()
                  .applying(userFundDividendReceived);
        }).
        anyEvent( 
        (event, userFund) -> {
          log.warn("Received unhandled event " + event.toString() + " in state " + stateName() + " for persistenceId " + persistenceId);
          return stay();
    }));
    
    onTermination(
        matchStop(new Shutdown$(), 
            (userFundState, userFund) -> {
              log.info("Shutting down the actor " + self() + " in state " + userFundState);
    }));
  }

  private String getBrokerName(UserAdvise userAdvise) {
    return userAdvise.getBrokerName().getBrokerName() == null ? getBrokerNameOfTheInvestorWith(userAdvise.getUsername(),
        userAdvise.getUserAdviseCompositeKey().getInvestingMode()).getBrokerName().getBrokerName() : userAdvise.getBrokerName().getBrokerName();
  }
  
  private boolean findInActivePendingAdvisesList(String adviseId, UserFund userFund) {
    return userFund.getPendingApprovalRequests().stream().anyMatch(userResponse -> userResponse.getAdviseId().equals(adviseId));
  }

  private List<UserResponseParameters> appendResponsesWithMissingParameters(List<UserResponseParameters> userResponses) {
    List<UserResponseParameters> appendPendingApprovals = new ArrayList<UserResponseParameters>();
    Collection<String> collectionOfWdId = new ArrayList<String>();
    userResponses.stream().forEach(userResponseParam -> {
      collectionOfWdId.add(userResponseParam.getToken());
    });
    HashMap<String, PriceWithPaf> tickerToPriceWithPafMap = getCurrentPriceSnapshotBasket(collectionOfWdId);
    for (UserResponseParameters userResponseParam : userResponses) {
      UserResponseParameters userResponseParameters = new UserResponseParameters.UserResponseParametersBuilder(
          userResponseParam.getAdviseId(),
          userResponseParam.getAdviseType(),
          userResponseParam.getUsername(),
          userResponseParam.getFundName(),
          userResponseParam.getAdviserName(),
          userResponseParam.getClientCode(),
          userResponseParam.getBrokerName(),
          userResponseParam.getExchange(),
          userResponseParam.getToken(),
          userResponseParam.getSymbol(),
          userResponseParam.getInvestingMode(),
          userResponseParam.getUserResponse(),
          userResponseParam.getInvestmentDate())
          .withAbsoluteAllocation(round(userResponseParam.getAbsoluteAllocation()))
          .withAllocationValue(getAllocationValue(tickerToPriceWithPafMap, userResponseParam))
          .withNumberOfShares(getNumberOfShares(tickerToPriceWithPafMap, userResponseParam))
          .withAdviseType(getViewSideAdviseType(userResponseParam.getAdviseType()))
          .build();
      appendPendingApprovals.add(userResponseParameters);
    }
    return appendPendingApprovals;
  }

  private HashMap<String, PriceWithPaf> getCurrentPriceSnapshotBasket(Collection<String> collectionOfWdId) {
    HashMap<String, PriceWithPaf> tickerToPriceWithPafMap = new HashMap<String, PriceWithPaf>();
    try {
      tickerToPriceWithPafMap = ((PriceSnapshotsFromPriceSource) askAndWaitForResult(
          wdActorSelection.activeInstrumentTracker(), new GetPriceBasketFromTracker(collectionOfWdId)))
              .getTickerToPriceWithPafMap();
    } catch (Exception e) {
      log.error("Could not get prices from active instrument tracker due to " + e.getMessage());
    }
    return tickerToPriceWithPafMap;
  }

  private AdviseType getViewSideAdviseType(AdviseType adviseType) {
    if (adviseType.equals(AdviseType.Issue))
      return AdviseType.Buy;
    else if (adviseType.equals(AdviseType.Close))
      return AdviseType.Sell;
    return adviseType;
  }

  private double getAllocationValue(HashMap<String, PriceWithPaf> tickerToPriceWithPafMap,
      UserResponseParameters userResponseParam) {
    if (userResponseParam.getAdviseType().equals(AdviseType.Close)) {
      double allocationValue = 0;
      double price = 0;
      try {
        price = tickerToPriceWithPafMap.get(userResponseParam.getToken()).getPrice().getPrice();
      } catch (NullPointerException e) {
        log.error("Price not found in the active instruments for token: " + userResponseParam.getToken());
      }
      allocationValue = price * userResponseParam.getNumberOfShares();
      return round(allocationValue, 2);
    } else
      return userResponseParam.getAllocationValue();
  }

  private int getNumberOfShares(HashMap<String, PriceWithPaf> tickerToPriceWithPafMap,
      UserResponseParameters userResponseParam) {
    if (userResponseParam.getAdviseType().equals(Issue)) {
      double price = 0;
      try {
        price = tickerToPriceWithPafMap.get(userResponseParam.getToken()).getPrice().getPrice();
      } catch (NullPointerException e) {
        log.error("Price not found in the active instruments for token: " + userResponseParam.getToken());
      }
      if (!isZero(price)) {
        return (int) (userResponseParam.getAllocationValue() / price);
      } else
        return 0;
    } else
      return userResponseParam.getNumberOfShares();
  }

  private void saveSnapshot() {
    long seqNum = lastSequenceNr();
    if (seqNum != 0 && seqNum % wdProperties.SNAPSHOT_INTERVAL() == 0)
      saveStateSnapshot();
  }
  
  
  @Override
  public Recovery recovery() {
    if (wdProperties.disableRecoveryFromSnapshot()) {
      return Recovery.create(SnapshotSelectionCriteria.none());
    }
    return Recovery.create(SnapshotSelectionCriteria.Latest());
  }


  
  @Autowired
  public void setWDProperties(WDProperties properties) {
    this.wdProperties = properties;
  }
 
  private void appriseInvestorWithPreWithdrawalSnapshot(UserFund fund, double allocation) {
    Map<String, AdviseTransactionDetails> subscriptionSharesDetails = new HashMap<String, AdviseTransactionDetails>();
    Map<String, Double> adviseIdToSharesToBeWithdrawn = fund.getAdviseIdToExecutedSharesforWithdrawal(allocation);
    Map<String, String> tokenToAdviseId = new HashMap<String, String>();

    for (String adviseId : adviseIdToSharesToBeWithdrawn.keySet()) {
      if (doesAdviseIdExists(fund, adviseId)) {
        for (UserAdvise userAdvise : fund.getActiveAdvises()) {
          if (userAdvise.getAdviseId().equals(adviseId)) {
            tokenToAdviseId.put(userAdvise.getToken(), adviseId);
          }
        }
      }
    }
    PriceSnapshotsFromPriceSource symbolWithPrices = new PriceSnapshotsFromPriceSource(new HashMap<>());
    try {
      symbolWithPrices = (PriceSnapshotsFromPriceSource) askAndWaitForResult(wdActorSelection.activeInstrumentTracker(),
          new GetPriceBasketFromTracker(tokenToAdviseId.keySet()));
    } catch (Exception e) {
      log.error("Could not get price of due to " + e.getMessage());
      sender().tell(failed(fund.getUsername(), " withdrawal action has failed, kindly retry.", StatusCodes.PRECONDITION_FAILED),
          self());
      return;
    }
    symbolWithPrices.getTickerToPriceWithPafMap().forEach((token, currentPrice) -> {
      double numberOfShares = adviseIdToSharesToBeWithdrawn.get(tokenToAdviseId.get(token));
      double currentMarketValue = ((numberOfShares * allocation) / 100.) * currentPrice.getPrice().getPrice();
      int numberOfSharesToBeSold = (int) ((numberOfShares * allocation) / 100.);
      String symbol = getSymbolFor(token);
      subscriptionSharesDetails.put(symbol,
          new AdviseTransactionDetails((int) round(numberOfSharesToBeSold), currentPrice.getPrice().getPrice(),
              round(currentMarketValue), round(allocation), getExchangeFromWdId(token), AdviseTransactionType.Sell));
    });
    sender().tell(new SendFundDetailsToUser(fund.getUsername(), subscriptionSharesDetails), self());
  }
  
  private String getSymbolFor(String ticker) {
    
    try {
      Object result;
      result = askAndWaitForResult(
          wdActorSelection.userRootActor(), new GetSymbolForWdId(ticker));
      if (result instanceof Failed) {
        log.error("Error while getting symbol for wdId " + ticker);
        return ticker;
      }
      SymbolSentForWdId symbolReceived = (SymbolSentForWdId) result;
      return symbolReceived.getSymbol();
      
    } catch (Exception e) {
      e.printStackTrace();
      return ticker;
    }
  }

  private boolean doesAdviseIdExists(UserFund fund, String adviseId) {
    return fund.getActiveAdvises().stream()
    .filter(userAdvise -> adviseId.equals(userAdvise.getAdviseId()))
    .findFirst().isPresent();
  }
  
  private void handleSnapshotSuccess(SaveSnapshotSuccess snapshotSuccess) {
    SnapshotMetadata metadata = snapshotSuccess.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has sucessfully been persisted with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()));
  }

  private void handleSnapshotFailure(SaveSnapshotFailure snapshotFailure) {
    SnapshotMetadata metadata = snapshotFailure.metadata();
    log.debug("Snapshot with persistenceId: " + metadata.persistenceId() + " has failed to persist with sequence number " 
    + metadata.sequenceNr() + " at " + prettyDate(metadata.timestamp()) + " due to reason " + snapshotFailure.cause()) ;
  }
  
  private void handleUserAdviceIssued(FundAdviseIssuedWithClientInformation adviseIssued, IssueType issueType,
      UserFund userFund, UserAdvise userAdvise) {
    String childName = "".concat(new UserAdviseCompositeKey(userAdvise.getAdviseId(),
        userAdvise.getUserAdviseCompositeKey().getUsername(),
        userAdvise.getUserAdviseCompositeKey().getFund(),
        userAdvise.getUserAdviseCompositeKey().getAdviser(),
        userAdvise.getUserAdviseCompositeKey().getInvestingMode()).persistenceId());
    UserAdviseEvent userAdviseEvent = userAdviseEventFor(adviseIssued, userFund, userAdvise, issueType, childName);
    forwardToChildren(context(), self(), userAdviseEvent, childName);
  }

  private void handleUserResponse(UserResponseParameters userResponseParameters, IssueType issueType,
      UserAdvise userAdvise, UserFund userFund) {
    String childName = "".concat(new UserAdviseCompositeKey(userAdvise.getAdviseId(),
        userAdvise.getUserAdviseCompositeKey().getUsername(),
        userAdvise.getUserAdviseCompositeKey().getFund(),
        userAdvise.getUserAdviseCompositeKey().getAdviser(),
        userAdvise.getUserAdviseCompositeKey().getInvestingMode()).persistenceId());
    
    UserAdviseEvent userAdviseEvent = userAdviseEventFor(userResponseParameters, userFund, issueType, childName);
    forwardToChildren(context(), self(), userAdviseEvent, childName);
  }

  private UserAdviseEvent userAdviseEventFor(FundAdviseIssuedWithClientInformation adviseIssued, UserFund userFund, UserAdvise userAdvise, IssueType issueType, 
      String childName) {
    return (!doesTheChildExist(userFund, userAdvise.getUserAdviseCompositeKey().persistenceId())) ?
        nonSpecialUserFundAdvice(
            userAdvise.getUserAdviseCompositeKey().getUsername(), 
            userAdvise.getUserAdviseCompositeKey().getAdviser(), 
            userAdvise.getUserAdviseCompositeKey().getFund(),
            userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
            userAdvise.getAdviseId(), 
            userAdvise.getToken(), 
            adviseIssued.getSymbol(),
            "",
            userAdvise.getIssueAdviceAbsoluteAllocation(),
            userAdvise.getIssueAdviseAllocationValue(),
            adviseIssued.isBeingOnboarded(),
            adviseIssued.getBrokerName(),
            adviseIssued.getClientCode(),
            issueType,
            userFund.getBasketOrderId()+CompositeKeySeparator+"1",
            BrokerAuthInformation.withNoBrokerAuth()) :
        UserFundAdditionalSumAdvice.nonSpecialUserFundAdvice(
            userAdvise.getUserAdviseCompositeKey().getUsername(), 
            userAdvise.getUserAdviseCompositeKey().getAdviser(), 
            userAdvise.getUserAdviseCompositeKey().getFund(),
            userAdvise.getUserAdviseCompositeKey().getInvestingMode(),
            userAdvise.getAdviseId(), 
            userAdvise.getToken(), 
            adviseIssued.getSymbol(),
            "",
            userAdvise.getIssueAdviceAbsoluteAllocation(),
            userAdvise.getIssueAdviseAllocationValue(),
            adviseIssued.isBeingOnboarded(),
            adviseIssued.getBrokerName(),
            adviseIssued.getClientCode(),
            issueType,
            userFund.getBasketOrderId()+CompositeKeySeparator+"1",
            BrokerAuthInformation.withNoBrokerAuth());
  }
  
  private UserAdviseEvent userAdviseEventFor(UserResponseParameters userResponseParameters, UserFund userFund, IssueType issueType, String childName) {
    String persistenceId = new UserAdviseCompositeKey(userResponseParameters.getAdviseId(), userFund.getUserFundCompositeKey().getUsername(), 
            userResponseParameters.getFundName(), userResponseParameters.getAdviserName(), userResponseParameters.getInvestingMode()).persistenceId();
    return (!doesTheChildExist(userFund, persistenceId)) ?
        nonSpecialFundAdviceForANonOnboardedUser(
            userFund.getUserFundCompositeKey().getUsername(),
            userResponseParameters.getAdviserName(),
            userResponseParameters.getFundName(),
            userResponseParameters.getInvestingMode(),
            userResponseParameters.getAdviseId(), 
            userResponseParameters.getToken(), 
            userResponseParameters.getSymbol(),
            "",
            userResponseParameters.getAbsoluteAllocation(),
            userResponseParameters.getAllocationValue(),
            fromBrokerName(userResponseParameters.getBrokerName(), userResponseParameters.getInvestingMode()),
            userResponseParameters.getClientCode(),
            issueType,
            userResponseParameters.getBasketOrderCompositeId(),
            userResponseParameters.getBrokerAuthInformation()) :
        UserFundAdditionalSumAdvice.nonSpecialFundAdviceForANonOnboardedUser(
              userFund.getUserFundCompositeKey().getUsername(),
              userResponseParameters.getAdviserName(),
              userResponseParameters.getFundName(),
              userResponseParameters.getInvestingMode(),
              userResponseParameters.getAdviseId(), 
              userResponseParameters.getToken(), 
              userResponseParameters.getSymbol(),
              "",
              userResponseParameters.getAbsoluteAllocation(),
              userResponseParameters.getAllocationValue(),
              fromBrokerName(userResponseParameters.getBrokerName(), userResponseParameters.getInvestingMode()),
              userResponseParameters.getClientCode(),
              issueType,
              userResponseParameters.getBasketOrderCompositeId(),
              userResponseParameters.getBrokerAuthInformation());
  }
  
  private boolean doesTheChildExist(UserFund userFund, String persistenceId) {
    return userFund.getChildrenPersistenceIds().stream()
        .anyMatch(id -> persistenceId.equals(id));
  }
  
  private boolean doesUserAdviseExist(UserFund userFund, String adviseId) {
    return userFund.getActiveAdvises().stream()
        .anyMatch(advise -> adviseId.equals(advise.getAdviseId()));
  }

  private double calculateAdviseAllocation(double allocationPending, double cashRatio) {
    return allocationPending * cashRatio;
  }

  private PriceWithPaf getPrice(double entryPrice, String token) {
    InstrumentPrice instrumentPrice = new InstrumentPrice((entryPrice), DateUtils.currentTimeInMillis(), token);
    PriceWithPaf price = new PriceWithPaf(instrumentPrice , new Pafs());
    return price;
  }

  private double calculateSecondaryAdvisePrice(double priceDifference, double cashAllocationRatio,
      double sharesRatio) {
    double normalizedAllocation =  cashAllocationRatio / sharesRatio;
    return (priceDifference * normalizedAllocation);
  }

  private double calculateNewAdviseValue(int quantity, double entryPrice) {
    return (quantity * entryPrice);
  }

  private int calculateNewAdviseQuantity(double sharesRatio, int quantity) {
    return (int)(sharesRatio * quantity);
  }

  private boolean getOnboardingProcessStatus(String username) {
    try {
      UserOnboardingStatus userOnboardingStatus = (UserOnboardingStatus) askAndWaitForResult(wdActorSelection.userRootActor(), new HasUserBeenOnBoarded(username));
      return userOnboardingStatus.isOnboarded();
    } catch (Exception e) {
        log.error("Could not get onboarding status because " + e.getMessage());
        return false;
    }
  }

  private double getCurrentPortfolioValue(UserFund userFund) {
    double portfolioValue = userFund.getCashComponent();
    Map<String, Double> adviseIdToExecutedShares = userFund.getAdviseIdToExecutedShares();
    if (adviseIdToExecutedShares.size() > 0) {
      Map<String, String> adviseIdToTicker = new HashMap<String, String>();
      if (userFund.getActiveAdviseIdsTraded().size() > 0) {
        for (UserAdvise userAdvise : userFund.getActiveAdvises()) {
          if (adviseIdToExecutedShares.containsKey(userAdvise.getAdviseId()))
            adviseIdToTicker.put(userAdvise.getAdviseId(), userAdvise.getToken());
        }
        if(isAdviseIdToExecutedSharesMapUpdated(adviseIdToTicker, adviseIdToExecutedShares)) {
          GetCurrentUserFundValue getCurrentUserFundValue = new GetCurrentUserFundValue(
              userFund.getUserFundCompositeKey().getFund(), userFund.getUserFundCompositeKey().getAdviser(),
              userFund.getUsername());
          portfolioValue += tellRelayerTo(getCurrentUserFundValue, adviseIdToExecutedShares, adviseIdToTicker, userFund);
        } else {
          log.error("Active Traded List map size : "+adviseIdToTicker.size()+" different from adviseIdToExecutedShares"
              + " map size: "+adviseIdToExecutedShares.size()+", for: "+persistenceId);
          self().tell(new CalculateUserFundComposition(userFund.getUserFundCompositeKey()), ActorRef.noSender());
          return userFund.getTotalInvestment();
        }
      }
    } else {
      portfolioValue = userFund.getTotalInvestment();
    }
    return portfolioValue;
  }

  private boolean isAdviseIdToExecutedSharesMapUpdated(Map<String, String> adviseIdToTicker,
      Map<String, Double> adviseIdToExecutedShares) {
    for (String adviseId : adviseIdToTicker.keySet()) {
      if (!adviseIdToExecutedShares.keySet().contains(adviseId))
        return false;
    }
    return true;
  }

  private double tellRelayerTo(GetCurrentUserFundValue getCurrentUserFundValue,
      Map<String, Double> adviseIdToExecutedShares, Map<String, String> adviseIdToTicker, UserFund userFund) {
    ActorRef internalRequestProcessor = null;
    double assetsValue = 0;
    try {
      internalRequestProcessor = context().actorOf(Props.create(RealtimeUserFundValueRelayer.class,
          adviseIdToExecutedShares, adviseIdToTicker, wdActorSelection),
          "RealtimeUserFundValueRelayer-" + getCurrentUserFundValue.getUsername());
    } catch (InvalidActorNameException e) {
      internalRequestProcessor = context().actorOf(Props.create(RealtimeUserFundValueRelayer.class,
          adviseIdToExecutedShares, adviseIdToTicker, wdActorSelection),
          "RealtimeUserFundValueRelayer-" + UUID.randomUUID().toString());
    }
    try {
      Object result = askAndWaitForResult(internalRequestProcessor, getCurrentUserFundValue);
      if (result instanceof Failed) {
        log.error("Error while getting assets value " + result);
        return (userFund.getTotalInvestment() - userFund.getCashComponent());
      }
      assetsValue = (double) result;
    } catch (Exception e) {
      log.error("Error while getting fund ", e);
      return (userFund.getTotalInvestment() - userFund.getCashComponent());
    }
    return assetsValue;
  }

  private BrokerNameSent getBrokerNameOfTheInvestorWith(String userEmail, InvestingMode investingMode) {
    BrokerNameSent brokerNameSent = noBrokerInformationAvailable();
    try {
      Object result = askAndWaitForResult(wdActorSelection.userRootActor(), new GetBrokerName(userEmail, investingMode)) ;
    
      if (result instanceof Failed) {
          log.error("Error while getting broker name " + result);
          return null;
          }
       brokerNameSent =(BrokerNameSent) result;
     } catch (Exception e) {
          log.error("Error while getting fund ", e);
          return null;
       }
    return brokerNameSent;
  }
  
  private RequestedClientCode getClientCodeFromUserRootActor(String userEmail) {
    RequestedClientCode requestedClientCode = new RequestedClientCode("");
    try {
      Object result =  askAndWaitForResult(wdActorSelection.userRootActor(),
          new GetClientCode(userEmail));
        
        if (result instanceof Failed) {
              log.error("Error while getting client code." + result);
              return null;
            }
        requestedClientCode = (RequestedClientCode) result;
      } catch (Exception e) {
            log.error("Error while getting instrument by wdId ", e);
            return null;
          }
    return requestedClientCode;
  }

  private boolean identifyTransitionState(List<FundConstituent> fundConstituentList) {
    
      for(FundConstituent fundConstituent : fundConstituentList) {
        if (fundConstituent.getTicker().endsWith("-MF"))
          return false;
      }
      return true;
  }
  
  @Override
  public String persistenceId() {
    return persistenceId;
  }

  @Override
  public UserFund applyEvent(UserFundEvent userFundEvent, UserFund userFund) {
    userFund.update(userFundEvent);
    if (isRecoveryCompleted) {
      saveSnapshot();
    }
    return userFund;
  }
  
  @Override
  public void onRecoveryCompleted() {
    super.onRecoveryCompleted();
    log.debug("Recovery completed for UserFundActor with persistenceId: " + persistenceId);
    isRecoveryCompleted  = true;
  }

  @Override
  public Class<UserFundEvent> domainEventClass() {
    return UserFundEvent.class;
  }

  @Override
  public String getName() {
    return persistenceId;
  }

  @Override
  public String getChildBeanName() {
    return "UserAdviseActor";
  }
  
  public ActorRef findChild(ActorContext context, String name, IssueType issueType) {
    String childName = getName() + "-" + name;
    Option<ActorRef> maybeActor = context.child(childName);
    if (maybeActor.isEmpty()) {
      log.debug("maybeActor is empty! Creating " + childName);
      ActorRef child = issueType.child(context, childName, name);
      return child;
    } else {
      log.debug("maybeActor is not empty! Using " + childName);
      return maybeActor.get();
    }
  }
  
  public void forwardToChildren(ActorContext context, ActorRef sender, Object command, String children) {
    ActorRef childActor = findChild(context, children, issueType(command));
    log.debug("Forwarding " + command.toString() + " to " + childActor);
    if (command instanceof ExchangeOrderUpdateEventWithIssueType) {
      ExchangeOrderUpdateEventWithIssueType exchangeOrderUpdateEventWithIssueType = (ExchangeOrderUpdateEventWithIssueType) command;
      childActor.forward(exchangeOrderUpdateEventWithIssueType.getExchangeOrderUpdateEvent(), context());
    } else
      childActor.forward(command, context());
  }

  private IssueType issueType(Object command) {
      IssueType issueType = Default;
      try {
        UserAdviseEvent userAdviseEvent = (UserAdviseEvent) command;
        issueType = userAdviseEvent.getIssueType();
      } catch (ClassCastException e) {
        if(command instanceof UpdateAdviseTradeStatus)
          issueType = ((UpdateAdviseTradeStatus) command).getIssueType();
        else if (command instanceof MergerDemergerEvent || command instanceof CorporateEventUpdateWithFundNames 
            || command instanceof MergerDemergerFundAdviseClosed || command instanceof MergerDemergerPrimaryBuyIssued ||
            command instanceof MergerDemergerSecondaryBuyIssued)
          issueType = Equity;
        else if (command instanceof ExchangeOrderUpdateEventWithIssueType) {
          ExchangeOrderUpdateEventWithIssueType exchangeOrderUpdateEventWithIssueType = (ExchangeOrderUpdateEventWithIssueType) command;
          issueType = exchangeOrderUpdateEventWithIssueType.getIssueType();
        } else if (command instanceof UserResponseReceived) {
          UserResponseReceived userResponseReceived = (UserResponseReceived) command;
          issueType = getIssueType(userResponseReceived.getToken());
        } else if (command instanceof CalculateUserAdviseComposition) {
          CalculateUserAdviseComposition calculateUserAdviseComposition = (CalculateUserAdviseComposition) command;
          issueType = calculateUserAdviseComposition.getIssueType();
        }
      }
      return issueType;
  }
  
  private String getExchangeFromWdId(String wdId) {
    return wdId.split("-")[1];
  }
}