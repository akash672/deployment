package com.wt.domain.write.events;

import java.util.Map;

import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class UpdateUserFundComposition extends UserFundEvent {
  private static final long serialVersionUID = 1L;

  private final Map<String, Double> adviseIdToShares;

  public UpdateUserFundComposition(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, Map<String, Double> adviseIdToShares) {
    super(username, adviserUsername, fundName, investingMode);
    this.adviseIdToShares = adviseIdToShares;
  }

}
