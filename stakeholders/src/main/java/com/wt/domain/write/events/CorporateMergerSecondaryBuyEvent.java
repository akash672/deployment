package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class CorporateMergerSecondaryBuyEvent extends UserFundEvent {

  private static final long serialVersionUID = 1L;
  private String oldToken;
  private String newToken;
  private String identityId;
  private String adviseId;
  private OrderSide orderSide;
  private double sharesRatio;
  private double cashAllocationRatio;
  private String symbol;

  @Deprecated
  public CorporateMergerSecondaryBuyEvent(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String oldToken, String newToken, String identityId, String adviseId,
      OrderSide orderSide, double sharesRatio, double cashAllocationRatio, String symbol) {
    this(username, adviserUsername, fundName, investingMode, oldToken, newToken, adviseId, orderSide, sharesRatio,
        cashAllocationRatio, symbol);
  }

  public CorporateMergerSecondaryBuyEvent(String username, String adviserUsername, String fundName,
      InvestingMode investingMode, String oldToken, String newToken, String adviseId, OrderSide orderSide,
      double sharesRatio, double cashAllocationRatio, String symbol) {
    super(username, adviserUsername, fundName, investingMode);
    this.oldToken = oldToken;
    this.newToken = newToken;
    this.identityId = username;
    this.adviseId = adviseId;
    this.orderSide = orderSide;
    this.sharesRatio = sharesRatio;
    this.cashAllocationRatio = cashAllocationRatio;
    this.symbol = symbol;
  }
}
