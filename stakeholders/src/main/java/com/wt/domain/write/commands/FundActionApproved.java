package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.InvestingMode;
import com.wt.domain.Purpose;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class FundActionApproved extends AuthenticatedUserCommand {

  private static final long serialVersionUID = 1L;
  private String fundName;
  private String adviserUsername;
  private String wealthCode;
  private InvestingMode investingMode;
  private String basketOrderId;
  private Purpose investingPurpose;
  private ApprovalResponse approvalResponse;
  @Setter
  private BrokerAuthInformation brokerAuthInformation;
  private ObjectMapper objectMapper = new ObjectMapper();
  
  @JsonCreator
  public FundActionApproved(@JsonProperty("username") String username,@JsonProperty("fund") String fundName, 
      @JsonProperty("adviser")String adviserUsername, @JsonProperty("wealthCode") String wealthCode,
      @JsonProperty("investingMode") InvestingMode investingMode,  @JsonProperty("basketOrderId") String basketOrderId,
      @JsonProperty("investingPurpose") Purpose investingPurpose, @JsonProperty("approvalResponse") ApprovalResponse approvalResponse,
      @JsonProperty("authInfo") BrokerAuthInformation authenticationInfo) {
    super(username);
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.wealthCode = wealthCode;
    this.investingMode = investingMode;
    this.basketOrderId = basketOrderId;
    this.investingPurpose = investingPurpose;
    this.approvalResponse = approvalResponse;
    this.brokerAuthInformation = authenticationInfo;
  }

  public boolean isBaskerOrderIdBlank() {
    return (this.basketOrderId == null || this.basketOrderId.equals(""));
  }
  
  public boolean isAuthticationInfoBlank() {
    return (this.brokerAuthInformation == null);
  }
}
