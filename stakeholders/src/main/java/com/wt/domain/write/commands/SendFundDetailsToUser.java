package com.wt.domain.write.commands;

import java.util.Map;

import com.wt.domain.AdviseTransactionDetails;
import com.wt.domain.write.events.Done;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SendFundDetailsToUser extends Done {
  
  private static final long serialVersionUID = 1L;
  private Map<String, AdviseTransactionDetails> subscriptionSharesDetails;

  
	public SendFundDetailsToUser(String id, Map<String, AdviseTransactionDetails> subscriptionSharesDetails) {
    super(id, "Please confirm to continue");
    this.subscriptionSharesDetails = subscriptionSharesDetails;
  }

}
