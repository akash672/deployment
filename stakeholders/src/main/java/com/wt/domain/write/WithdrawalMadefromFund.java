package com.wt.domain.write;

import com.wt.domain.InvestingMode;
import com.wt.domain.write.events.UserEvent;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class WithdrawalMadefromFund extends UserEvent {

  private static final long serialVersionUID = 1L;
  private String fundName;
  private String adviserUsername;
  private double withdrawalAmount;
  private InvestingMode investingMode;

  public WithdrawalMadefromFund(String username, String adviserUsername, String fundName, InvestingMode investingMode, double withdrawalAmount) {
    super(username);
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.withdrawalAmount = withdrawalAmount;
    this.investingMode = investingMode;
  }

}
