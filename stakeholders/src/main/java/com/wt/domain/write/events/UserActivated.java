package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserActivated extends UserEvent {

  @Getter
  private final String hashCode;

  public UserActivated(String username, String hashCode) {
    super(username);
    this.hashCode = hashCode;
  }

  private static final long serialVersionUID = 1L;

}
