package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import java.io.Serializable;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UnAuthenticatedUserCommand implements Serializable {
  
  private static final long serialVersionUID = 1L;
  @NonNull
  private String username;

  public UnAuthenticatedUserCommand(String username) {
    super();
    this.username = username;
    assertStringNotEmpty(username, "username");      
  }

}
