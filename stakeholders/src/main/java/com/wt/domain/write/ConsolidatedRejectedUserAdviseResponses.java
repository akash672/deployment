package com.wt.domain.write;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.write.events.Done;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsolidatedRejectedUserAdviseResponses extends Done {
  
  private static final long serialVersionUID = 1L;
  private String userEmail;
  private String investingMode;
  private List<UserResponseParametersFromFund> userResponseParametersFromFunds;

  public ConsolidatedRejectedUserAdviseResponses(@JsonProperty("id") String userEmail,
      @JsonProperty("investingMode") String investingMode, @JsonProperty("message") String message,
      @JsonProperty("userResponseParametersFromFunds") List<UserResponseParametersFromFund> userResponseParametersFromFunds) {
    super(userEmail, message);
    this.userEmail = userEmail;
    this.investingMode = investingMode;
    this.userResponseParametersFromFunds = userResponseParametersFromFunds;
  }

}
