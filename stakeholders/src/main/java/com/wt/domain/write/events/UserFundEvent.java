package com.wt.domain.write.events;

import com.google.gson.annotations.Expose;
import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class UserFundEvent extends UserEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  @Expose
  private String fundName;
  @Getter
  @Expose
  private String adviserUsername;
  @Getter
  @Expose
  private InvestingMode investingMode;
  
  public UserFundEvent(String username, String adviserUsername, String fundName, InvestingMode investingMode) {
    super(username);
    this.fundName = fundName;
    this.adviserUsername = adviserUsername;
    this.investingMode = investingMode;
  }
}
