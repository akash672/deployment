package com.wt.domain.write.events;

import java.util.Map;

import lombok.Getter;

public class SecurityAnswersSubmitted extends UserEvent {

  @Getter
  private Map<String, String> securityAnswers;

  public SecurityAnswersSubmitted(String username, Map<String, String> securityAnswers) {
    super(username);
    this.securityAnswers = securityAnswers;
  }

  private static final long serialVersionUID = 1L;

}
