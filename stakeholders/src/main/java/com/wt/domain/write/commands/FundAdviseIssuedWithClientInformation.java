package com.wt.domain.write.commands;

import com.wt.domain.BrokerName;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.events.FundAdviceIssued;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class FundAdviseIssuedWithClientInformation extends FundAdviceIssued {
  
  private static final long serialVersionUID = 1L;
  private String clientCode;
  @Setter private BrokerName brokerName;
  private String symbol;
  private boolean isBeingOnboarded;
  
  public FundAdviseIssuedWithClientInformation(String adviserUsername, String fundName, String adviseId, String token,
      double allocation, double absoluteAllocation, long issueTime, PriceWithPaf entryPrice, long entryTime,
      PriceWithPaf exitPrice, long exitTime, String clientCode, BrokerName brokerName,
      String symbol, boolean isSpecialAdvise, boolean isBeingOnboarded) {
    super(adviserUsername, fundName, adviseId, token, symbol, allocation, absoluteAllocation, issueTime, entryPrice, entryTime,
          exitPrice, exitTime, isSpecialAdvise);
    this.clientCode = clientCode;
    this.brokerName = brokerName;
    this.symbol = symbol;
    this.isBeingOnboarded = isBeingOnboarded;
  }
}
