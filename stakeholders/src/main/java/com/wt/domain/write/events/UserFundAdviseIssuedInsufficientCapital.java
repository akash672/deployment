package com.wt.domain.write.events;


import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;
@ToString
public class UserFundAdviseIssuedInsufficientCapital extends UserAdviseEvent {
  private static final long serialVersionUID = 1L;
  @Getter 
  private double adjustAllocation;
  @Getter
  private String token;

  public UserFundAdviseIssuedInsufficientCapital(String email, String fundName, String adviserName, InvestingMode investingMode,
      String adviseId, String token, double adjustAllocation, IssueType issueType) {
    super(email, adviserName, fundName, investingMode, adviseId, issueType);
    this.adjustAllocation = adjustAllocation;
    this.token = token;
  }

}
