package com.wt.domain;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class StringToDoubleMapMarshaller implements DynamoDBTypeConverter<String, Map<String, Double>> {
  @Override
  public String convert(Map<String, Double> object) {
    return new Gson().toJson(object);
  }

  @Override
  public Map<String, Double> unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Map<String, Double>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }

}
