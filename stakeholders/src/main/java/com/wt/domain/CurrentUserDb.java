package com.wt.domain;

import static com.wt.aws.cognito.configuration.CredentialsProviderType.Cognito;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.UserType.NTU_NA;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshalling;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import com.wt.domain.write.WithdrawalMadefromFund;
import com.wt.domain.write.commands.SubmitUserAnswers;
import com.wt.domain.write.events.KycApprovedEvent;
import com.wt.domain.write.events.PhoneNumberUpdated;
import com.wt.domain.write.events.UpdateKYCDetails;
import com.wt.domain.write.events.UserEvent;
import com.wt.domain.write.events.UserLumpSumAddedToFund;
import com.wt.domain.write.events.UserRegistered;
import com.wt.domain.write.events.UserRegisteredFromUserPool;
import com.wt.domain.write.events.UserRiskProfileIdentified;
import com.wt.domain.write.events.UserSubscribedToFund;
import com.wt.domain.write.events.UserUnsubscribedToFund;
import com.wt.domain.write.events.WealthCodeUpdated;
import com.wt.utils.DoublesUtil;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@SuppressWarnings("deprecation")
@DynamoDBTable(tableName = "User")
@ToString
public class CurrentUserDb implements Serializable {

  private static final long serialVersionUID = 1L;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String email;

  @Id
  @Getter
  @Setter
  @DynamoDBHashKey
  private String username;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private String firstName;
  @Getter
  @Setter
  private String lastName;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String phoneNumber;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String isWealthCode;
  @Getter
  @Setter
  @DynamoDBTypeConvertedEnum
  private UserType role = NTU_NA;

  @Getter
  @Setter
  @DynamoDBAttribute
  private long registeredTimestamp;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String clientCode;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String panCard;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String brokerCompany;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double virtualWalletAmount;

  @DynamoDBAttribute
  @Getter
  @Setter
  @DynamoDBMarshalling(marshallerClass = FundWithAdviserAndInvestingModeMarshaller.class)
  private List<FundWithAdviserAndInvestingMode> advisorToFunds;

  @DynamoDBAttribute
  @Getter
  @Setter
  @DynamoDBTypeConverted(converter = UserAnswersConverter.class)
  private Map<String, Map<String, String>> answers = new HashMap<String, Map<String, String>>();

  @DynamoDBAttribute
  @Getter
  @Setter
  @DynamoDBTypeConvertedEnum
  private RiskLevel riskProfile = RiskLevel.LOW;
  private static final double INITIAL_VIRTUAL_MONEY_AMOUNT = 5000000.0;


  public CurrentUserDb() {
    advisorToFunds = new ArrayList<>();
  }

  protected CurrentUserDb(String email, String username, String firstName, String lastName, 
       String phoneNumber, String isWealthCode, UserType role, double virtualWalletAmount,
      long registeredTimestamp, String clientCode, String panCard, String brokerCompany,
      List<FundWithAdviserAndInvestingMode> advisorToFunds, Map<String, Map<String, String>> answers,
      RiskLevel riskProfile) {
    super();
    this.email = email;
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.isWealthCode = isWealthCode;
    this.virtualWalletAmount = virtualWalletAmount;
    this.role = role;
    this.registeredTimestamp = registeredTimestamp;
    this.clientCode = clientCode;
    this.panCard = panCard;
    this.brokerCompany = brokerCompany;
    this.advisorToFunds = advisorToFunds;
    this.answers = answers;
    this.riskProfile = riskProfile;
  }
  
  public CurrentUserDb copy() {
    return new CurrentUserDb(email, username, firstName, lastName, 
          phoneNumber, isWealthCode, role, virtualWalletAmount,
         registeredTimestamp, clientCode, panCard, brokerCompany, 
         new ArrayList<FundWithAdviserAndInvestingMode>(advisorToFunds), 
        new HashMap<String, Map<String, String>>(answers), 
        riskProfile);
  }
  
  public void update(UserEvent evt) throws ParseException {

    if (evt instanceof UserRegistered) {
      UserRegistered registerEvent = (UserRegistered) evt;
      setdefaultRegistrationParams(registerEvent);
      virtualWalletAmount = INITIAL_VIRTUAL_MONEY_AMOUNT;
    } else if (evt instanceof UserRegisteredFromUserPool) {
      UserRegisteredFromUserPool registerEvent = (UserRegisteredFromUserPool) evt;
      setdefaultRegistrationParamsFromUserPool(registerEvent);
    } else if (evt instanceof UserSubscribedToFund) {
      UserSubscribedToFund userSubscribedToFund = (UserSubscribedToFund)evt;
      if(userSubscribedToFund.getInvestingMode().equals(VIRTUAL))
        virtualWalletAmount -= userSubscribedToFund.getSubscriptionAmount();
      addFundUserSubscription((UserSubscribedToFund) evt);
    } else if (evt instanceof UserUnsubscribedToFund) {
      UserUnsubscribedToFund userUnsubscribedToFund = (UserUnsubscribedToFund) evt;
      if(userUnsubscribedToFund.getInvestingmode().equals(VIRTUAL))
        virtualWalletAmount += userUnsubscribedToFund.getUnsubscribedAmount();
      removeFundUserSubscription(userUnsubscribedToFund);
      } else if (evt instanceof PhoneNumberUpdated) {
      phoneNumberUpdated(evt);
    } else if (evt instanceof WealthCodeUpdated) {
      updateWealthCode(evt);
    } else if (evt instanceof UserRiskProfileIdentified) {
      userAnswersUpdated(evt);
    }  else if (evt instanceof KycApprovedEvent) {
      kycApproval((KycApprovedEvent) evt);
    } else if (evt instanceof UpdateKYCDetails) {
      updateKyc(evt);
    }else if (evt instanceof UserLumpSumAddedToFund) {
      UserLumpSumAddedToFund userLumpSumAddedToFund = (UserLumpSumAddedToFund) evt;
      if (userLumpSumAddedToFund.getInvestingMode().equals(VIRTUAL))
        virtualWalletAmount -= userLumpSumAddedToFund.getLumpSumAmount();
    } else if (evt instanceof WithdrawalMadefromFund) {
      WithdrawalMadefromFund withdrawalMadefromFund = (WithdrawalMadefromFund) evt;
      if (withdrawalMadefromFund.getInvestingMode().equals(VIRTUAL)) {
        virtualWalletAmount += withdrawalMadefromFund.getWithdrawalAmount();
      }
    }
  }

  protected void updateKyc(UserEvent evt) {
    UpdateKYCDetails updateKycEvent = (UpdateKYCDetails) evt;
    if (!(updateKycEvent.getPanCard().isEmpty()))
      setPanCard(updateKycEvent.getPanCard());
    if (!(updateKycEvent.getClientCode().isEmpty()))
      setClientCode(updateKycEvent.getClientCode());
    if (!(updateKycEvent.getBrokerCompany().isEmpty()))
      setBrokerCompany(updateKycEvent.getBrokerCompany());
    if (!updateKycEvent.getPhoneNumber().isEmpty())
      setPhoneNumber(updateKycEvent.getPhoneNumber());
    updateEmail(updateKycEvent);
  }

  protected void updateEmail(UpdateKYCDetails updateKycEvent) {
    if (updateKycEvent.getEmailAddress() != null)
      setEmail(updateKycEvent.getEmailAddress());
    else
      setEmail(updateKycEvent.getEmail());
  }

  protected void updateEmail(KycApprovedEvent updateKycEvent) {
    if (updateKycEvent.getEmailAddress() != null)
      setEmail(updateKycEvent.getEmailAddress());
    else
      setEmail(updateKycEvent.getEmail());
  }
  protected void kycApproval(KycApprovedEvent event) throws ParseException {
    this.clientCode = event.getClientCode();
    this.panCard = event.getPanCard();
    this.brokerCompany = event.getBrokerCompany();
    this.phoneNumber = event.getPhoneNumber();
    this.role = UserType.TU;
    updateEmail(event);
  }

  protected void userAnswersUpdated(UserEvent evt) {
    UserRiskProfileIdentified event = (UserRiskProfileIdentified) evt;
    answers.put(event.getEventName(), event.getAnswers());
    riskProfile = event.getRiskProfile();
  }

  protected void updateWealthCode(UserEvent evt) {
    isWealthCode = "true";
  }

  protected void phoneNumberUpdated(UserEvent evt) {
    phoneNumber = ((PhoneNumberUpdated) evt).getPhonenumber();
  }

  protected void addFundUserSubscription(UserSubscribedToFund fundUserSubscribed) {
    advisorToFunds.add(new FundWithAdviserAndInvestingMode(fundUserSubscribed.getFundName(),
        fundUserSubscribed.getAdviserUsername(), fundUserSubscribed.getInvestingMode().name()));
  }

  protected void removeFundUserSubscription(UserUnsubscribedToFund fundUserUnsubscribed) {
    advisorToFunds.removeIf(fundWithAdviserAndInvestingMode -> hasFollowingAttributes(fundWithAdviserAndInvestingMode,
        fundUserUnsubscribed.getAdviserUsername(), fundUserUnsubscribed.getFundName(),
        fundUserUnsubscribed.getInvestingmode()));
  }

  @DynamoDBIgnore
  public boolean isSubscribed(String adviserName, String fundName, InvestingMode investingMode) {
    return advisorToFunds.stream()
        .anyMatch(fundWithAdviserAndInvestingMode -> hasFollowingAttributes(fundWithAdviserAndInvestingMode,
            adviserName, fundName, investingMode));
  }

  @DynamoDBIgnore
  public boolean isNotAutorized() {
    return this.role.equals(NTU_NA);
  }

  protected void setdefaultRegistrationParamsFromUserPool(UserRegisteredFromUserPool evt) {
    setdefaultRegistrationParams(new UserRegistered(evt.getUsername(), evt.getName().split(" ")[0],
        extractLastName(evt.getName()), evt.getUsername(), evt.getTimestamp(), Cognito, "", evt.getEmail()));
    phoneNumber = evt.getPhoneNumber();
    virtualWalletAmount = evt.getVirtualMoney();
    if (DoublesUtil.isZero(virtualWalletAmount)) {
      virtualWalletAmount = INITIAL_VIRTUAL_MONEY_AMOUNT;
    }

  }

  protected String extractLastName(String name) {
    String[] split = name.split(" ");
    String lastName = "";
    for (int i = 1; i < split.length; i++) {
      lastName += split[i] + " ";
    }
    return lastName.trim();
  }

  protected void setdefaultRegistrationParams(UserRegistered registerEvent) {
    firstName = registerEvent.getFirstName();
    lastName = registerEvent.getLastName();
    email = registerEvent.getEmail();
    username = registerEvent.getUsername();
    clientCode = registerEvent.getUsername();
    registeredTimestamp = registerEvent.getRegisterationTime();
    role = UserType.NTU;
    phoneNumber = registerEvent.getPhoneNumber();
    isWealthCode = registerEvent.getIsWealthCode();
    setBrokerCompany(MOCK.getBrokerName());
  }

  public RiskLevel decideRiskProfile(SubmitUserAnswers command) {
    String riskProfileQuestion = "How do you want your money to grow?";
    if (command.getAnswers().get(riskProfileQuestion) == null)
      return RiskLevel.MEDIUM;

    String riskProfileAnswer = command.getAnswers().get(riskProfileQuestion);
    if (riskProfileAnswer
        .equalsIgnoreCase("Slow and steady wins the race, I prefer safe investments like Fixed Deposits.")) {
      return RiskLevel.LOW;
    } else if (riskProfileAnswer
        .equalsIgnoreCase("I believe in moderation, I'm happy as long as I beat inflation like Mutual Funds.")) {
      return RiskLevel.MEDIUM;
    }
    return RiskLevel.HIGH;
  }

  @DynamoDBIgnore
  public boolean isWealthCode() {
    return this.isWealthCode.equalsIgnoreCase("true");
  }

  protected boolean hasFollowingAttributes(FundWithAdviserAndInvestingMode fundWithAdviserAndInvestingMode,
      String adviserName, String fundName, InvestingMode investingMode) {
    return fundWithAdviserAndInvestingMode.getAdviserUsername().equals(adviserName)
        && fundWithAdviserAndInvestingMode.getFund().equals(fundName)
        && fundWithAdviserAndInvestingMode.getInvestingMode().equals(investingMode.getInvestingMode());
  }
  

}