package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FundUnsubscription {
  private String adviserUsername;
  private String fundName;
  private InvestingMode investingMode;
  
  @JsonCreator
  public FundUnsubscription(@JsonProperty("adviser") String adviserUsername, @JsonProperty("fund") String fundName,
      @JsonProperty("investingMode") InvestingMode investingMode) {
    super();
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.investingMode = investingMode;
  }
}
