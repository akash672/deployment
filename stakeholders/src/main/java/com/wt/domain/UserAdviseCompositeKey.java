package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class UserAdviseCompositeKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @DynamoDBRangeKey
  private String adviseId;

  @DynamoDBHashKey
  private String usernameWithFundWithAdviser;

  public UserAdviseCompositeKey(String adviseId, String username, String fundName, String adviserUsername,
      InvestingMode investingMode ) {
    super();
    this.adviseId = adviseId;
    this.usernameWithFundWithAdviser = username + CompositeKeySeparator + fundName + CompositeKeySeparator
        + investingMode.name() + CompositeKeySeparator + adviserUsername;
  }

  @DynamoDBIgnore
  public String getUsername() {
    return usernameWithFundWithAdviser.split(CompositeKeySeparator)[0];
  }

  public String getAdviser() {
    return usernameWithFundWithAdviser.split(CompositeKeySeparator)[3];
  }
  
  public InvestingMode getInvestingMode() {
    return InvestingMode.valueOf(usernameWithFundWithAdviser.split(CompositeKeySeparator)[2]);
  }

  public String getFund() {
    return usernameWithFundWithAdviser.split(CompositeKeySeparator)[1];
  }

  public String persistenceId() {
    return adviseId + CompositeKeySeparator + usernameWithFundWithAdviser;
  }

}