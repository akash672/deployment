package com.wt.domain;

import static com.wt.utils.CommonConstants.HYPHEN;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wt.domain.write.commands.RegisterExistingUser;
import com.wt.domain.write.commands.UpdateKYCDetailsInRootActor;
import com.wt.domain.write.events.ExistingUserOnboarded;
import com.wt.domain.write.events.RootUserRegistered;
import com.wt.domain.write.events.RootUserRegisteredfromUserPool;

public class UserRootState implements Serializable {

  private static final long serialVersionUID = 1L;
  private Map<String, String> investorUsernames = new HashMap<>();
  private Map<String, String> investorUsernamesWithClientCode = new HashMap<>();
  private Map<String, Boolean> userIdentitiesWithOnboardingStatus = new HashMap<>();

  public UserRootState() {
    this(new HashMap<String, String>(), new HashMap<String, String>(), new HashMap<String, Boolean>());
  }

  private UserRootState(Map<String, String> investorUsernames, Map<String, String> investorUsernamesWithClientCode,
      Map<String, Boolean> userIdentitiesWithOnboardingStatus) {
    super();
    this.investorUsernames = investorUsernames;
    this.investorUsernamesWithClientCode = investorUsernamesWithClientCode;
    this.userIdentitiesWithOnboardingStatus = userIdentitiesWithOnboardingStatus;
  }

  public UserRootState copy() {
    return new UserRootState(investorUsernames, investorUsernamesWithClientCode, userIdentitiesWithOnboardingStatus);
  }

  public void update(Object event) {
    if (event instanceof RootUserRegisteredfromUserPool) {
      RootUserRegisteredfromUserPool registerExistingUser = (RootUserRegisteredfromUserPool) event;
      investorUsernames.put(registerExistingUser.getUsername(), registerExistingUser.getUsername());
    } else if (event instanceof RegisterExistingUser) {
      RegisterExistingUser registerExistingUser = (RegisterExistingUser) event;
      userIdentitiesWithOnboardingStatus.put(registerExistingUser.getUsername(), true);
    } else if (event instanceof RootUserRegistered) {
      RootUserRegistered rootUserRegistered = (RootUserRegistered) event;
      List<String> list = Arrays.asList("sukasa13@gmail.com", "dilipjadav2110@gmail.co", "pragneshpshah@bpwealth.com",
          "jayprasad834@gmai.com", "atul.vadya@rediffmail.com", "atul.vayda@gmail.com", "ankit@remail.com", "a@b.cim",
          "a@b.com");
      if (!list.contains(rootUserRegistered.getUserEmail())) {
        if (rootUserRegistered.getUserEmail().equals("rhunadkat@outlook.com")) {
          investorUsernames.put("rhunadkat1", rootUserRegistered.getUserEmail());
          userIdentitiesWithOnboardingStatus.put("rhunadkat1", rootUserRegistered.isOnboarded());
        } else {
          investorUsernames.put(rootUserRegistered.getUserEmail().split("@")[0], rootUserRegistered.getUserEmail());
          userIdentitiesWithOnboardingStatus.put(rootUserRegistered.getUserEmail().split("@")[0],
              rootUserRegistered.isOnboarded());
        }
      }

    } else if (event instanceof RootUserRegisteredfromUserPool) {
      RootUserRegisteredfromUserPool rootUserRegistered = (RootUserRegisteredfromUserPool) event;
      investorUsernames.put(rootUserRegistered.getUsername(), rootUserRegistered.getUsername());
    } else if (event instanceof UpdateKYCDetailsInRootActor) {
      UpdateKYCDetailsInRootActor updateKYCDetailsInRootActor = (UpdateKYCDetailsInRootActor) event;
      String username = userEmailFrom(getRequiredUsername(updateKYCDetailsInRootActor.getUsername()));
      investorUsernamesWithClientCode.put(username,
          updateKYCDetailsInRootActor.getClientCode() + HYPHEN + updateKYCDetailsInRootActor.getBrokerCompany());
    } else if (event instanceof ExistingUserOnboarded) {
      ExistingUserOnboarded existingUserOnboarded = (ExistingUserOnboarded) event;
      userIdentitiesWithOnboardingStatus.put(existingUserOnboarded.getUsername(),
          existingUserOnboarded.isOnboardingInProcess());
    }
  }

  public String userEmailFrom(String username) {
    return investorUsernames.get(username);
  }

  public String clientCodeFrom(String userName) {
    return investorUsernamesWithClientCode.get(userName);
  }

  public boolean containsClientCode(String clientCode) {
    return investorUsernamesWithClientCode.containsValue(clientCode);
  }

  public Boolean onboardingStatusFrom(String userName) {
    return userIdentitiesWithOnboardingStatus.get(userName);
  }

  public boolean isUserOnboarded(String username) {
    return userIdentitiesWithOnboardingStatus.containsKey(username)
        ? userIdentitiesWithOnboardingStatus.get(username)
        : false;
  }

  public boolean doesUserExist(String userEmail) {
    return investorUsernames.containsValue(userEmail);
  }

  public Collection<String> allUserEmails() {
    return investorUsernames.values();
  }

  private String getRequiredUsername(String username) {
    if (username.equals("rhunadkat@outlook.com"))
      return "rhunadkat1";
    return username.split("@")[0];
  }

  public String userNameFromEmail(String userEmail) {
    return investorUsernames.entrySet().stream().filter(e -> e.getValue().equals(userEmail)).map(Map.Entry::getKey)
        .findFirst().orElse(null);
  }
}
