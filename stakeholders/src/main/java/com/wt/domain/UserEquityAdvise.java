package com.wt.domain;

import static com.wt.domain.OrderSegment.Equity;
import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.OrderSide.SELL;
import static com.wt.domain.OrderType.MARKET;
import static com.wt.domain.Purpose.PARTIALWITHDRAWAL;
import static com.wt.domain.UserAcceptanceMode.Manual;
import static com.wt.domain.UserAdviseState.AdviseTokenPriceReceived;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlaced;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlacedWithBroker;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.ExpectingAdvisePrice;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderReceivedWithBroker;
import static com.wt.domain.UserAdviseState.PartialExitAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.UserAdviseIssued;
import static com.wt.domain.UserResponseType.Awaiting;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.DoublesUtil.validDouble;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Entity;

import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CreatedOrder.CreatedOrderBuilder;
import com.wt.domain.write.events.AdviseOrderCreated;
import com.wt.domain.write.events.AdviseOrderPlacedWithExchange;
import com.wt.domain.write.events.AdviseOrderReceivedWithBroker;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.AdviseTokenPriceReceived;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.DividendEventReceived;
import com.wt.domain.write.events.ExitAdviseReceived;
import com.wt.domain.write.events.FlushIssueAdviseTrades;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.OrderPlacedWithExchange;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.domain.write.events.UpdateCorporateEvent;
import com.wt.domain.write.events.UpdatingWdId;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceCalculated;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceWithDate;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserAdviseTransactionReceived;
import com.wt.domain.write.events.UserFundAdditionalSumAdvice;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.utils.akka.CancellableStream;

import akka.NotUsed;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString(callSuper=true, exclude= {"cumulativePaf","totalQuantityWithdrawn"})
@Entity
@EqualsAndHashCode(callSuper=true, of="userAdviseCompositeKey")
@DynamoDBTable(tableName = "UserEquityAdvise")
public class UserEquityAdvise extends UserAdvise implements Serializable {
  private static final long serialVersionUID = 1L;
  @Setter
  @DynamoDBAttribute
  protected long issueTime;
  @Setter
  @DynamoDBAttribute
  protected String description;
  @Setter
  @DynamoDBIgnore
  protected String clientCode;
  @Setter
  @DynamoDBIgnore
  protected BrokerName brokerName;
  @Setter
  @DynamoDBAttribute
  protected String token;
  @Setter
  @DynamoDBAttribute
  protected String symbol;
  @Setter
  @DynamoDBAttribute
  protected double issueAdviceAbsoluteAllocation;
  @Setter
  @DynamoDBAttribute
  protected double issueAdviseAllocationValue;
  @Setter
  @DynamoDBAttribute
  protected double closeAdviseAbsoluteAllocation;
  @Setter
  @DynamoDBAttribute
  protected double cumulativePaf = 1.0;
  @Setter
  @DynamoDBAttribute
  protected double cumulativeDividendsReceived ;
  @Setter
  @DynamoDBAttribute
  protected int totalQuantityWithdrawn;
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  protected Set<Order> issueAdviseOrder = new TreeSet<Order>();
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  protected Set<Order> closeAdviseOrders = new TreeSet<Order>();
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  protected Set<Order> corporateEventOrders = new TreeSet<Order>();
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = CumulativePafMapMarshaller.class)
  protected Map<Long, Double> dateToCumulativePafMap = new TreeMap<Long, Double>();
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = CumulativePafMapMarshaller.class)
  protected Map<Long, Double> dateToCumulativeDividendsReceived = new TreeMap<Long, Double>();
  @Setter
  @DynamoDBIgnore
  protected Map<String, Double> corporateEventDetailstoPaf = new HashMap<String, Double>();
  @Setter
  @DynamoDBAttribute
  protected PriceWithPaf averageEntryPrice;
  @Setter
  @DynamoDBAttribute
  protected PriceWithPaf averageExitPrice;
  @Setter
  @DynamoDBIgnore
  protected PriceWithPaf lastestCloseAdvisePrice;
  @Setter
  @DynamoDBIgnore
  protected int quantityInPendingCloseApproval;
  @Setter
  @DynamoDBIgnore
  protected String basketOrderCompositeId;
  @DynamoDBIgnore
  protected BrokerAuthInformation brokerAuthInformation;

  @DynamoDBRangeKey(attributeName = "adviseId")
  public String getAdviseId() {
    return userAdviseCompositeKey.getAdviseId();
  }

  public void setAdviseId(String adviseId) {
    userAdviseCompositeKey.setAdviseId(adviseId);
  }

  @DynamoDBHashKey(attributeName = "usernameWithFundWithAdviser")
  public String getUsernameWithFundWithAdviser() {
    return userAdviseCompositeKey.getUsernameWithFundWithAdviser();
  }

  public void setUsernameWithFundWithAdviser(String userWithFundWithAdviser) {
    userAdviseCompositeKey.setUsernameWithFundWithAdviser(userWithFundWithAdviser);
  }
  @Id
  @Setter
  @DynamoDBIgnore
  protected UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey();
  
  public UserEquityAdvise() {
    isA = "UserEquityAdvise";
  }

  public void update(UserAdviseEvent event) {
    if (event instanceof UserFundAdviceIssuedWithUserEmail) {
      UserFundAdviceIssuedWithUserEmail userFundAdviseIssued = (UserFundAdviceIssuedWithUserEmail) event;
      this.userAdviseCompositeKey = new UserAdviseCompositeKey( userFundAdviseIssued.getAdviseId(),
                                                                userFundAdviseIssued.getUsername(), 
                                                                userFundAdviseIssued.getFundName(),
                                                                userFundAdviseIssued.getAdviserUsername(), 
                                                                userFundAdviseIssued.getInvestingMode());
      this.issueTime = userFundAdviseIssued.getTimestamp();
      this.description = userFundAdviseIssued.getDescription();
      this.token = userFundAdviseIssued.getToken();
      this.symbol = userFundAdviseIssued.getSymbol();
      this.clientCode = userFundAdviseIssued.getClientCode();
      this.brokerName = userFundAdviseIssued.getBrokerName();
      this.issueAdviceAbsoluteAllocation = userFundAdviseIssued.getAbsoluteAllocation();
      this.issueAdviseAllocationValue = round(userFundAdviseIssued.getMonetaryAllocationValue());
      this.averageEntryPrice = new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.));
      this.averageExitPrice = new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.));
      this.quantityInPendingCloseApproval = 0;
      if(userFundAdviseIssued.getBasketOrderCompositeId() != null)
        this.basketOrderCompositeId = userFundAdviseIssued.getBasketOrderCompositeId();
      this.brokerAuthInformation = userFundAdviseIssued.getBrokerAuthInformation();
    } else if (event instanceof UserFundAdditionalSumAdvice) {
      UserFundAdditionalSumAdvice userFundAdviseIssued = (UserFundAdditionalSumAdvice) event;
      this.userAdviseCompositeKey = new UserAdviseCompositeKey( userFundAdviseIssued.getAdviseId(),
          userFundAdviseIssued.getUsername(), 
          userFundAdviseIssued.getFundName(),
          userFundAdviseIssued.getAdviserUsername(), 
          userFundAdviseIssued.getInvestingMode());
      this.issueTime = userFundAdviseIssued.getTimestamp();
      this.description = userFundAdviseIssued.getDescription();
      this.token = userFundAdviseIssued.getToken();
      this.symbol = userFundAdviseIssued.getSymbol();
      this.clientCode = userFundAdviseIssued.getClientCode();
      this.brokerName = userFundAdviseIssued.getBrokerName();
      this.issueAdviceAbsoluteAllocation = userFundAdviseIssued.getAbsoluteAllocation();
      this.issueAdviseAllocationValue = round(userFundAdviseIssued.getMonetaryAllocationValue());
      this.averageEntryPrice = averageEntryPrice == null ? new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.)) : averageEntryPrice;
      this.averageExitPrice = averageExitPrice == null ? new PriceWithPaf(new InstrumentPrice(0., issueTime, token), new Pafs(1., 0., 0., 1.)) : averageExitPrice;
      if(userFundAdviseIssued.getBasketOrderId() != null)
        this.basketOrderCompositeId = userFundAdviseIssued.getBasketOrderId();
      this.brokerAuthInformation = userFundAdviseIssued.getBrokerAuthInformation();
    } else if (event instanceof AdviseTokenPriceReceived) {
      AdviseTokenPriceReceived adviseTokenPriceReceivedEvent = (AdviseTokenPriceReceived) event;
      if (isIssuedAdvicePriceReceived(adviseTokenPriceReceivedEvent)) {
        int numberOfShares = calculateNumberOfShares(issueAdviseAllocationValue,
            adviseTokenPriceReceivedEvent.getRealTimePrice().getPrice().getPrice());
        AdviseOrderCreated adviseOrderCreated = createAdviseOrder(adviseTokenPriceReceivedEvent.getAdviseId(),
            adviseTokenPriceReceivedEvent.getLocalOrderId(),
            adviseTokenPriceReceivedEvent.getRealTimePrice().getPrice().getTime(),
            adviseTokenPriceReceivedEvent.getRealTimePrice().getPrice().getWdId(),
            clientCode, numberOfShares, BUY,
            adviseTokenPriceReceivedEvent.getInvestingMode(),
            adviseTokenPriceReceivedEvent.getPurpose(),
            adviseTokenPriceReceivedEvent.getPrice(), adviseTokenPriceReceivedEvent.getFundName());

        Order newAdviseOrder = new Order();
        newAdviseOrder.update(adviseOrderCreated);
        issueAdviseOrder.add(newAdviseOrder);
      }
    } else if (event instanceof AdviseOrderReceivedWithBroker) {
      AdviseOrderReceivedWithBroker adviseOrderReceivedWithBroker = (AdviseOrderReceivedWithBroker) event;
      OrderReceivedWithBroker orderReceivedWithBroker = new OrderReceivedWithBroker(new DestinationAddress(null),
          adviseOrderReceivedWithBroker.getLocalOrderId(), adviseOrderReceivedWithBroker.getBrokerOrderId());
      if (isIssuedAdviceOrderReceivedWithBroker(adviseOrderReceivedWithBroker)) {
        getLatestIssuedAdviseOrder().ifPresent(order -> order.update(orderReceivedWithBroker));
      } else if (isCorporateEventOrderReceivedWithBroker(adviseOrderReceivedWithBroker)) {
        getLatestCorpActionOrder().ifPresent(order -> order.update(orderReceivedWithBroker));
      }
      else {
        Order closeAdviseOrder = getLatestSellOrder().get();
        closeAdviseOrder.update(orderReceivedWithBroker);
       
      }
    } else if (event instanceof AdviseOrderPlacedWithExchange) {
      AdviseOrderPlacedWithExchange orderPlacedEvent = (AdviseOrderPlacedWithExchange) event;
      OrderPlacedWithExchange orderPlacedWithExchange = new OrderPlacedWithExchange(new DestinationAddress(null),orderPlacedEvent.getBrokerOrderId(),
          orderPlacedEvent.getExchangeOrderId(), orderPlacedEvent.getCtclId(), orderPlacedEvent.getSide(),
          orderPlacedEvent.getQuantity());
      if (isIssuedAdviceOrderPlacedWithExchange(orderPlacedEvent)) {
        getLatestIssuedAdviseOrder().ifPresent(order -> order.update(orderPlacedWithExchange));
      } else if(isCorporateEventOrderPlacedWithExchange(orderPlacedEvent)){
        getLatestCorpActionOrder().ifPresent(order -> order.update(orderPlacedWithExchange));
      } 
      else {
        getLatestSellOrder().ifPresent(order -> order.update(orderPlacedWithExchange));
      }
    } else if (event instanceof AdviseOrderTradeExecuted) {
      AdviseOrderTradeExecuted adviseOrderTradeExecuted = (AdviseOrderTradeExecuted) event;
      TradeExecuted tradeExecuted = new TradeExecuted.TradeExecutedBuilder(new DestinationAddress(null),
          adviseOrderTradeExecuted.getExchangeOrderId(), adviseOrderTradeExecuted.getTradeId(),
          adviseOrderTradeExecuted.getOrderSide(), adviseOrderTradeExecuted.getPrice(),
          adviseOrderTradeExecuted.getQuantity()).withLastTradedTime(adviseOrderTradeExecuted.getTimestamp()).build();
      if (isIssuedAdviceOrderTradeExecuted(adviseOrderTradeExecuted)) {
        getLatestIssuedAdviseOrder().ifPresent(order -> order.update(tradeExecuted));
      }
      else if(isCorporateEventOrderTradeExecuted(adviseOrderTradeExecuted)){
        getLatestCorpActionOrder().ifPresent(order -> order.update(tradeExecuted));
      }
      else {
        getLatestSellOrder().ifPresent(order -> order.update(tradeExecuted));
      }
      calculateAveragePrice(adviseOrderTradeExecuted, cumulativePaf);
    } else if (event instanceof ExitAdviseReceived) {
      ExitAdviseReceived exitAdviseReceived = (ExitAdviseReceived) event;
      if(exitAdviseReceived.getBasketOrderCompositeId() != null)
        this.basketOrderCompositeId = exitAdviseReceived.getBasketOrderCompositeId();
      if(exitAdviseReceived.getBrokerAuthInformation() != null)
        this.brokerAuthInformation = exitAdviseReceived.getBrokerAuthInformation();

      closeAdviseAbsoluteAllocation = exitAdviseReceived.getAbsoluteAllocation();
      int numberOfShares = 0;
      if(!exitAdviseReceived.isSpecialExit()){
        if (!isZero(closeAdviseAbsoluteAllocation - 100.))
          numberOfShares  = (int) (integerQuantityYetToBeExited() * closeAdviseAbsoluteAllocation / 100.);
        else
          numberOfShares = integerQuantityYetToBeExited();
      }else{
          numberOfShares = exitAdviseReceived.getSpecialExitshares();
      }
      AdviseOrderCreated adviseOrderCreated = createAdviseOrder(exitAdviseReceived.getAdviseId(),
          exitAdviseReceived.getLocalOrderId(), exitAdviseReceived.getExitDate(), token, exitAdviseReceived.getUsername(),
          numberOfShares, SELL, exitAdviseReceived.getInvestingMode(), exitAdviseReceived.getPurpose(), 
          exitAdviseReceived.getPrice(), exitAdviseReceived.getFundName());
     
      Order closeAdviseOrder = new Order();
      closeAdviseOrder.update(adviseOrderCreated);
      closeAdviseOrders.add(closeAdviseOrder);
      if (exitAdviseReceived.getPurpose().equals(PARTIALWITHDRAWAL))
        totalQuantityWithdrawn += closeAdviseOrder.getIntegerQuantity();
    } else if (event instanceof CurrentUserAdviseFlushedDueToExit) {
      issueAdviseOrder.stream().forEach(issueAdviseOrder -> issueAdviseOrder.update(new FlushIssueAdviseTrades()));
      closeAdviseOrders.stream().forEach(closeAdviseOrder -> closeAdviseOrder.update(new FlushIssueAdviseTrades()));
      corporateEventOrders.stream().forEach(corporateEventOrder -> corporateEventOrder.update(new FlushIssueAdviseTrades()));
      issueAdviseOrder = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      closeAdviseOrders = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      corporateEventOrders = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      issueAdviceAbsoluteAllocation = 0.;
      issueAdviseAllocationValue = 0.;
      closeAdviseAbsoluteAllocation = 0.;
      averageEntryPrice = PriceWithPaf.noPrice("");
      averageExitPrice = PriceWithPaf.noPrice("");
      cumulativeDividendsReceived =0.;
      cumulativePaf = 1.0;
      totalQuantityWithdrawn = 0;
      dateToCumulativePafMap = new TreeMap<Long, Double>();
      dateToCumulativeDividendsReceived = new TreeMap<Long, Double>();
    } else if(event instanceof UpdateCorporateEvent){
      UpdateCorporateEvent updateCorporateEvent = (UpdateCorporateEvent) event;
      int numberOfShares = 0;
      if(updateCorporateEvent.getAdjustPaf() > 1){
        numberOfShares = (int) (integerQuantityYetToBeExited() * updateCorporateEvent.getAdjustPaf()) - integerQuantityYetToBeExited();
      } else{
        numberOfShares = (int) (integerQuantityYetToBeExited() - integerQuantityYetToBeExited() * updateCorporateEvent.getAdjustPaf()) ;
      }
      AdviseOrderCreated adviseOrderCreated = createAdviseOrder(updateCorporateEvent.getAdviseId(),
          updateCorporateEvent.getLocalOrderId(), updateCorporateEvent.getExDate() , token, updateCorporateEvent.getUsername(),
          numberOfShares, checkCorporateEventOrderSide(updateCorporateEvent.getAdjustPaf()), updateCorporateEvent.getInvestingMode(),
          updateCorporateEvent.getPurpose(), updateCorporateEvent.getPrice(), updateCorporateEvent.getFundName());
      Order corporateEventOrder = new Order();
      corporateEventOrder.update(adviseOrderCreated);
      corporateEventOrders.add(corporateEventOrder);
      cumulativePaf *= updateCorporateEvent.getAdjustPaf();
      dateToCumulativePafMap.put(updateCorporateEvent.getExDate(), cumulativePaf);
      String corporateEventDetails = "" + updateCorporateEvent.getExDate()+ "_" +  updateCorporateEvent.getEventType();
      corporateEventDetailstoPaf.put(corporateEventDetails, updateCorporateEvent.getAdjustPaf());
    } else if(event instanceof DividendEventReceived){
      DividendEventReceived dividendEventReceived = (DividendEventReceived) event;
      cumulativeDividendsReceived += dividendEventReceived.getDividendValue()  * integerQuantityYetToBeExited();
      dateToCumulativeDividendsReceived.put(dividendEventReceived.getExDate(), cumulativeDividendsReceived);
      String corporateEventDetails = "" + dividendEventReceived.getExDate()+ "_" +  dividendEventReceived.getEventType();
      corporateEventDetailstoPaf.put(corporateEventDetails, dividendEventReceived.getDividendValue());
    } else if (event instanceof UserAdviseTransactionReceived) {
      UserAdviseTransactionReceived userAdviseTransactionReceived = (UserAdviseTransactionReceived) event;
      this.brokerAuthInformation = userAdviseTransactionReceived.getBrokerAuthInformation();
      lastestCloseAdvisePrice = userAdviseTransactionReceived.getExitPrice();
      if (checkResponseMode(userAdviseTransactionReceived)){
        quantityInPendingCloseApproval += calculateNumberOfCloseAdviseShares(integerQuantityYetToBeExited(), userAdviseTransactionReceived.getAbsoluteAllocation()) - quantityInPendingCloseApproval;
      }
      else {
        quantityInPendingCloseApproval = 0;
      }
    } else if (event instanceof UpdatingWdId) {
      UpdatingWdId updatingWdId = (UpdatingWdId) event;
      this.token = updatingWdId.getUpdatedWdId();

    }
  }

  private boolean checkResponseMode(UserAdviseTransactionReceived userAdviseTransactionReceived) {
    return userAdviseTransactionReceived.getUserAcceptanceMode().equals(Manual)
        && userAdviseTransactionReceived.getUserResponseType() != null
        && userAdviseTransactionReceived.getUserResponseType().equals(Awaiting);
  }

  @DynamoDBIgnore
  public Optional<Order> getLatestIssuedAdviseOrder(){
    return Optional.ofNullable(((TreeSet<Order>) issueAdviseOrder).last());
  }

  private boolean isIssuedAdvicePriceReceived(AdviseTokenPriceReceived adviseTokenPriceReceivedEvent) {
    return adviseTokenPriceReceivedEvent.getUserAdviseState().equals(ExpectingAdvisePrice)
        || adviseTokenPriceReceivedEvent.getUserAdviseState().equals(OpenAdviseOrderExecuted)
        || adviseTokenPriceReceivedEvent.getUserAdviseState().equals(PartialExitAdviseOrderExecuted)
        || adviseTokenPriceReceivedEvent.getUserAdviseState().equals(UserAdviseIssued);
      }

  private boolean isIssuedAdviceOrderPlacedWithExchange(AdviseOrderPlacedWithExchange orderPlacedEvent) {
    return (orderPlacedEvent.getUserAdviseState().equals(OpenAdviseOrderReceivedWithBroker));
  }

  private boolean isCorporateEventOrderReceivedWithBroker(
      AdviseOrderReceivedWithBroker adviseOrderReceivedWithBroker) {
    return (adviseOrderReceivedWithBroker.getUserAdviseState().equals(CorporateActionOrderPlaced));
        
  }

  private boolean isCorporateEventOrderPlacedWithExchange(AdviseOrderPlacedWithExchange orderPlacedEvent) {
    return (orderPlacedEvent.getUserAdviseState().equals(CorporateActionOrderPlacedWithBroker));
  }

  private boolean isCorporateEventOrderTradeExecuted(AdviseOrderTradeExecuted adviseOrderTradeExecuted) {
    return (adviseOrderTradeExecuted.getUserAdviseState().equals(CorporateActionOrderPlacedWithExchange));
  }
  
  @DynamoDBIgnore
  public CreatedOrderBuilder getCreatedBuyOrder() {
    Order latestIssueAdviseOrder = getLatestIssuedAdviseOrder().get();
    OrderExecutionMetaData orderExecutionMetadata = new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
        token,
        latestIssueAdviseOrder.getSide(),
        latestIssueAdviseOrder.getQuantity(), 
        MARKET, 
        Equity)
        .withSymbol(symbol)
        .withLimitPrice(latestIssueAdviseOrder.getPrice())
        .withbasketOrderCompositeId(basketOrderCompositeId)
        .withWdId(token).build();
    return new CreatedOrderBuilder(
        latestIssueAdviseOrder.getOrderId(), 
        userAdviseCompositeKey.getInvestingMode(), 
        userAdviseCompositeKey.getUsername(), 
        latestIssueAdviseOrder.getExchange())
        .withBrokerName(brokerName)
        .withClientCode(clientCode)
        .withOrderExecutionMetadata(orderExecutionMetadata)
        .withInvestingPurpose(latestIssueAdviseOrder.getPurpose())
        .withBrokerAuthentication(brokerAuthInformation);
  }
  
  @DynamoDBIgnore
  public Optional<Order> getLatestCorpActionOrder() {
    return Optional.ofNullable(((TreeSet<Order>) corporateEventOrders).last());
  }
  
  @DynamoDBIgnore
  public Optional<Order> getLatestSellOrder() {
    return Optional.ofNullable(((TreeSet<Order>) closeAdviseOrders).last());
  }
  
  @DynamoDBIgnore
  public CreatedOrderBuilder getCurrentCorpActionOrder() {
    Optional<Order> latestCorpActionOrder = Optional.ofNullable(((TreeSet<Order>) corporateEventOrders).last());
    
    OrderExecutionMetaData orderExecutionMetadata = new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
        token,
        latestCorpActionOrder.get().getSide(),
        latestCorpActionOrder.get().getQuantity(), 
        MARKET, 
        Equity)
        .withSymbol(symbol)
        .withLimitPrice(latestCorpActionOrder.get().getPrice())
        .withbasketOrderCompositeId(basketOrderCompositeId)
        .withWdId(token).build();
    
    return new CreatedOrderBuilder(
           latestCorpActionOrder.get().getOrderId(), 
           userAdviseCompositeKey.getInvestingMode(), 
           userAdviseCompositeKey.getUsername(), 
           latestCorpActionOrder.get().getExchange())
          .withOrderExecutionMetadata(orderExecutionMetadata);
  }
  
  @DynamoDBIgnore
  public CreatedOrderBuilder getCurrentSellOrderWith(String symbol) {
    Optional<Order> latestCloseAdviseOrder = Optional.of(((TreeSet<Order>) closeAdviseOrders).last());
    
    OrderExecutionMetaData orderExecutionMetadata = new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
        token,
        latestCloseAdviseOrder.get().getSide(),
        latestCloseAdviseOrder.get().getQuantity(), 
        MARKET, 
        Equity)
        .withSymbol(symbol)
        .withLimitPrice(latestCloseAdviseOrder.get().getPrice())
        .withbasketOrderCompositeId(basketOrderCompositeId)
        .withWdId(token).build();
    
    return new CreatedOrderBuilder(
           latestCloseAdviseOrder.get().getOrderId(), 
           userAdviseCompositeKey.getInvestingMode(), 
           userAdviseCompositeKey.getUsername(), 
           latestCloseAdviseOrder.get().getExchange())
          .withOrderExecutionMetadata(orderExecutionMetadata)
          .withBrokerAuthentication(brokerAuthInformation);
  }
  
  @DynamoDBIgnore
  public CreatedOrderBuilder getCurrentSellOrder() {
    Optional<Order> latestCloseAdviseOrder = Optional.of(((TreeSet<Order>) closeAdviseOrders).last());
    
    OrderExecutionMetaData orderExecutionMetadata = new OrderExecutionMetaData.OrderExecutionMetaDataBuilder(
        token,
        latestCloseAdviseOrder.get().getSide(),
        latestCloseAdviseOrder.get().getQuantity(), 
        MARKET, 
        Equity)
        .withSymbol(symbol)
        .withLimitPrice(latestCloseAdviseOrder.get().getPrice())
        .withbasketOrderCompositeId(basketOrderCompositeId)
        .withWdId(token).build();
    
    return new CreatedOrderBuilder(
           latestCloseAdviseOrder.get().getOrderId(), 
           userAdviseCompositeKey.getInvestingMode(), 
           userAdviseCompositeKey.getUsername(), 
           latestCloseAdviseOrder.get().getExchange())
          .withOrderExecutionMetadata(orderExecutionMetadata)
          .withBrokerName(brokerName)
          .withClientCode(clientCode)
          .withInvestingPurpose(latestCloseAdviseOrder.get().getPurpose());
  }

  public int calculateNumberOfIssueAdviseShares(double amountToInvest, double price) {
    if (price == 0) {
      return 0;
    }

    double fractionalShares = round(amountToInvest / price);
    if ((int) (fractionalShares) < 1)
      return 0;
    return (int) (fractionalShares);
  }
  
  public int calculateNumberOfCloseAdviseShares(int quantityToBeExited, double absoluteAllocation) {
    return (int) (quantityToBeExited * absoluteAllocation / 100.);
  }
  private boolean isIssuedAdviceOrderReceivedWithBroker(AdviseOrderReceivedWithBroker adviseOrderReceivedWithBroker) {
    return (adviseOrderReceivedWithBroker.getUserAdviseState().equals(AdviseTokenPriceReceived) ) ;
  }
  
  private boolean isIssuedAdviceOrderTradeExecuted(AdviseOrderTradeExecuted adviseOrderTradeExecuted) {
    return (adviseOrderTradeExecuted.getUserAdviseState().equals(OpenAdviseOrderPlacedWithExchange));
  }

  private OrderSide checkCorporateEventOrderSide(double adjustPaf){
    if(adjustPaf < 1){
      return SELL;
    }
    return BUY;
  }

  private AdviseOrderCreated createAdviseOrder(String adviseId, String localOrderId, long timestamp, String token,
      String clientCode, int numberOfShares, OrderSide orderSide, InvestingMode investingMode, Purpose purpose,
      double limitPrice, String fundName) {
    return new AdviseOrderCreated.AdviseOrderCreatedBuilder(getUsername(), localOrderId, timestamp, token, clientCode,
        orderSide, investingMode, fundName).withNumberOfShares(numberOfShares).withSymbol(symbol).withPurpose(purpose)
            .withPrice(limitPrice).build();
  }

  public int calculateNumberOfShares(double amountToInvest, double price) {
    if (price == 0) {
      return 0;
    }
    double fractionalShares = round(amountToInvest / price);
    if ((int) (fractionalShares) < 1)
      return 0;
    return (int) (fractionalShares);
  }

  private void calculateAveragePrice(AdviseOrderTradeExecuted tradeExecuted, double pafAtTrade) {
    long tradeTime = tradeExecuted.getTimestamp();
    if (isIssuedAdviceOrderTradeExecuted(tradeExecuted))
      averageEntryPrice = averageEntryPrice.withNewPriceTime(getUpdatedAveragePrice(issueAdviseOrder, true), tradeTime);
    else if (isCorporateEventOrderTradeExecuted(tradeExecuted)) {
      averageEntryPrice = averageEntryPrice.withNewPriceTime(getUpdatedAveragePrice(issueAdviseOrder, false), tradeTime);
      if (closeAdviseOrders.size() != 0)
        averageExitPrice = averageExitPrice.withNewPriceTime(getUpdatedAveragePrice(closeAdviseOrders, true), tradeTime);
    } else {
      averageExitPrice = averageExitPrice.withNewPriceTime(getUpdatedAveragePrice(closeAdviseOrders, true), tradeTime);
    }
  }

  private double getUpdatedAveragePrice(Set<Order> orderSet, boolean isNotCorporateEntry) {
    double normalizedQuantity = 0;
    double totalValue = 0;
    for (Order order : orderSet) {
      double pafAtOrder = getPafAtDate(order.getOrderTimestamp());
      List<Trade> trades = order.getTrades();
      for (Trade trade : trades) {
        totalValue += trade.getPrice() * (trade.getQuantity());
        if(isNotCorporateEntry)
          normalizedQuantity += (trade.getQuantity() / pafAtOrder);
        else
          normalizedQuantity += trade.getQuantity();
      }
    }
    return (validDouble(totalValue / (cumulativePaf * normalizedQuantity)));
  }
  
  public int tradedEntryQuantity() {
    return issueAdviseOrder.stream().mapToInt(issueOrder -> issueOrder.getTrades().stream().mapToInt(trade ->
      trade.getIntegerQuantity()).sum()).sum();
  }

  public int recentlyTradedEntryQuantity() {
    return getLatestIssuedAdviseOrder().get().getTrades().stream().mapToInt(trade -> trade.getIntegerQuantity()).sum();
  }

  public double averageEntryPrice() {
    return (getLatestIssuedAdviseOrder().get().calculateEquityAverageTradedPrice()/cumulativePaf);
  }

  public int currentCloseAdviseTradedQuantity(TradeExecuted tradeExecuted) {
    return currentCloseAdvise(tradeExecuted.getExchangeOrderId()).getIntegertotalTradedQuantity();
  }

  public int currentCloseAdviseOrderQuantity(TradeExecuted tradeExecuted) {
    return currentCloseAdvise(tradeExecuted.getExchangeOrderId()).getIntegerQuantity();
  }
  
  public double currentCloseAdviseAverageExitPrice(TradeExecuted tradeExecuted) {
    return currentCloseAdvise(tradeExecuted.getExchangeOrderId()).calculateEquityAverageTradedPrice();
  }
  
  public int currentCorporateEventTradedQuantity(TradeExecuted tradeExecuted) {
    return currentCorporateEvent(tradeExecuted.getExchangeOrderId()).getIntegertotalTradedQuantity();
    
  }

  public int currentCorporateEventOrderQuantity(TradeExecuted tradeExecuted) {
    return currentCorporateEvent(tradeExecuted.getExchangeOrderId()).getIntegerQuantity();
  }
  
  public double currentCorporateEventAverageExitPrice(TradeExecuted tradeExecuted) {
    return currentCorporateEvent(tradeExecuted.getExchangeOrderId()).calculateEquityAverageTradedPrice();
  }

  private Order currentCloseAdvise(String orderId) {
    return closeAdviseOrders.stream().filter(order -> order.getExchangeOrderId() != null && order.getExchangeOrderId().equals(orderId)).findFirst().get();
  }
  
  
  private Order currentCorporateEvent(String orderId) {
    return corporateEventOrders.stream().filter(order -> order.getExchangeOrderId().equals(orderId)).findFirst().get();
  }

  public CancellableStream<UserAdviseAbsolutePerformanceCalculated, NotUsed> realtimePerformanceUsing(
      CancellableStream<PriceWithPaf, NotUsed> subscription) {
    Source<PriceWithPaf, NotUsed> priceEvents = subscription.getSource();
    Source<UserAdviseAbsolutePerformanceCalculated, NotUsed> realtime = priceEvents
        .map(priceEvent -> performanceRealtime(priceEvent));

    return new AdviseStream<UserAdviseAbsolutePerformanceCalculated, NotUsed>(realtime, subscription.getActor(),
        subscription);
  }

  public List<UserAdviseAbsolutePerformanceWithDate> historicalPerformance(List<PriceWithPaf> listOfPriceEvent,
      Materializer materializer) throws InterruptedException, ExecutionException {

    Source<UserAdviseAbsolutePerformanceWithDate, NotUsed> map = Source.from(listOfPriceEvent)
        .map(priceEvent -> performanceForHistorical(priceEvent, priceEvent.getPrice().getTime()));
    List<UserAdviseAbsolutePerformanceWithDate> userAdvisePerformanceList = map.runWith(Sink.seq(), materializer).toCompletableFuture()
        .get();
    return userAdvisePerformanceList;
  }

  public int exitedQuantity() {
    Integer sum = soldTillDate();
    return (sum == null) ? 0 : (sum - totalQuantityWithdrawn);
  }

  public Integer soldTillDate() {
    Stream<Trade> trades = closeAdviseOrders.stream().flatMap(o -> o.getTrades().stream());
    Stream<Integer> quantities = trades.map(trade -> trade.getIntegerQuantity());
    Integer sum = quantities.collect(Collectors.summingInt(o -> o));
    return sum;
  }

  public UserAdviseAbsolutePerformanceCalculated performanceRealtime(PriceWithPaf price) {
    double realized = exitedQuantity()
        * (averageExitPrice.getPrice().getPrice() - averageEntryPrice.getPrice().getPrice()) + cumulativeDividendsReceived;
    double cashFromExit = exitedQuantity() * averageExitPrice.getPrice().getPrice();
    double currentValue = integerQuantityYetToBeExited() * price.getPrice().getPrice();
    double unrealized = currentValue - (integerQuantityYetToBeExited()
        * averageEntryPrice.getPrice().getPrice());
    double pnl = realized + unrealized;
    return new UserAdviseAbsolutePerformanceCalculated(getUsername(), userAdviseCompositeKey.getUsername(),
        userAdviseCompositeKey.getAdviser(), userAdviseCompositeKey.getFund(), userAdviseCompositeKey.getInvestingMode(), userAdviseCompositeKey.getAdviseId(),
        token, symbol, validDouble(realized), validDouble(unrealized),  validDouble(pnl), 
            pctReturn(price, currentTimeInMillis()), currentValue, integerQuantityYetToBeExited(), cashFromExit, getIssueType());
  }

  public UserAdviseAbsolutePerformanceWithDate performanceForHistorical(PriceWithPaf price, long date) {
    double realized = realizedTill(date);
    double unrealized = unrealizedTill(price, date);
    double currentValue = quantityYetToBeExited(date) * price.getPrice().getPrice();
    double cashFromExit = exitedEquityValue(date);
    double pnl = realized + unrealized;
    return new UserAdviseAbsolutePerformanceWithDate(getUsername(), userAdviseCompositeKey.getUsername(),
        userAdviseCompositeKey.getAdviser(), userAdviseCompositeKey.getFund(), userAdviseCompositeKey.getInvestingMode(), userAdviseCompositeKey.getAdviseId(),
        token, symbol, validDouble(realized), validDouble(unrealized),  validDouble(pnl), 
            pctReturn(price, currentTimeInMillis()), currentValue, integerQuantityYetToBeExited(), cashFromExit, date, getIssueType());
  }

  private double realizedTill(long date) {
    double averageEntryPrice = tradedEntryQuantity() == 0 ? 0.0 : getAverageEntryPriceTill(date);
    double adjustedQuantity = pafAdjustedExitedQuantityTill(date);
    double totalAdvicePerformance = getExitedValueTill(date) - (adjustedQuantity*averageEntryPrice) + getAccumulatedDividendsTillDate(date);
    return totalAdvicePerformance;
  }

  private double getExitedValueTill(long date) {
    double averagePrice = 0.;
    double totalValue = 0.;
    for (Order order : closeAdviseOrders) {
      if (order.getOrderKey().getOrderTimestamp() < date) {
        List<Trade> trades = order.getTrades();
        if (order.getIntegerQuantity() != 0)
          averagePrice = trades.stream().map(trade -> trade.getPrice() * trade.getIntegerQuantity())
              .collect(Collectors.summingDouble(o -> o)) / order.getIntegerQuantity();
        totalValue +=  round(order.getIntegerQuantity() *averagePrice);
      }
    }
    return totalValue;
  }

  private double pafAdjustedExitedQuantityTill(long date) {
    double normalizedQuantity = 0.;
    for (Order order : closeAdviseOrders) {
      double paf = 1.;
      if (order.getOrderKey().getOrderTimestamp() < date) {
        paf = getPafAtDate(order.getOrderKey().getOrderTimestamp());
        List<Trade> trades = order.getTrades();
        for (Trade trade : trades) {
          normalizedQuantity += (trade.getIntegerQuantity() / paf);
        }
      }
    }
    return normalizedQuantity;
  }

  private double getAverageEntryPriceTill(long date) {
    double averagePrice = 0.;
    for (Order order : issueAdviseOrder) {
      if (order.getOrderKey().getOrderTimestamp() < date) {
        List<Trade> trades = order.getTrades();
        if (order.getIntegerQuantity() != 0)
          averagePrice = trades.stream().map(trade -> trade.getPrice() * trade.getIntegerQuantity())
              .collect(Collectors.summingDouble(o -> o)) / order.getIntegerQuantity();
      }
    }
    return averagePrice;
  }

  private double unrealizedTill(PriceWithPaf price, long date) {
    int quantityRemaining = quantityYetToBeExited(date);
    double averageBuyPriceTill = getAverageBuyPriceAt(date);
    double totalUnrealizedPnl = quantityRemaining * (price.getPrice().getPrice() - averageBuyPriceTill);
    return totalUnrealizedPnl;
  }

  private double getAverageBuyPriceAt(long date) {
    double averageBuyPrice = getAverageEntryPriceTill(date);
    double pafAtDate = getPafAtDate(date);
    return (averageBuyPrice / pafAtDate);
  }

  private double getPafAtDate(long date) {
    double paf =1;
    for(Map.Entry<Long, Double> entry : dateToCumulativePafMap.entrySet()){
      long key = entry.getKey();
      double value = entry.getValue();
      if(date > key)
        paf = value;
    }
    return paf;
  }
  
  private double getAccumulatedDividendsTillDate(long date) {
    double dividends =0;
    for(Map.Entry<Long, Double> entry : dateToCumulativeDividendsReceived.entrySet()){
      long key = entry.getKey();
      double value = entry.getValue();
      if(date >= key)
        dividends = value;
    }
    return dividends;
  }

  @Override
  public int integerQuantityYetToBeExited() {
    return tradedEntryQuantity() + corporateEventQuantity() - exitedQuantity() - totalQuantityWithdrawn;
  }

  public int quantityInPendingPartialCloses() {
    return quantityInPendingCloseApproval;
  }

  @Override
  public double doubleQuantityYetToBeExited() {
    return 0.;
  }

  private int corporateEventQuantity() {
    Stream<Trade> tradesBuy = corporateEventOrders.stream().filter(order -> order.getSide() == BUY).
        flatMap(o -> o.getTrades().stream());
    Stream<Trade> tradesSell = corporateEventOrders.stream().filter(order -> order.getSide() == SELL).
        flatMap(o -> o.getTrades().stream());
    Stream<Integer> quantitiesBuy = tradesBuy.map(trade -> (int)trade.getQuantity());
    Stream<Integer> quantitiesSell = tradesSell.map(trade -> (int)trade.getQuantity());
    Integer sum = quantitiesBuy.collect(Collectors.summingInt(o -> o)) - quantitiesSell.collect(Collectors.summingInt(o -> o)) ;
    return (sum == null) ? 0 : sum;
  }

  private int quantityYetToBeExited(long date) {
    return tradedEntryQuantity() + corporateEventQuantityTill(date) - exitedQuantityTill(date);
    
  }

  private int corporateEventQuantityTill(long date) {
    Stream<Trade> tradesBuy = corporateEventOrders.stream().filter(order -> order.getOrderKey().getOrderTimestamp() <= date && order.getSide() == BUY)
        .flatMap(o -> o.getTrades().stream());
    Stream<Trade> tradesSell = corporateEventOrders.stream().filter(order -> order.getOrderKey().getOrderTimestamp() <= date && order.getSide() == SELL)
        .flatMap(o -> o.getTrades().stream());
    Stream<Integer> quantitiesBuy = tradesBuy.map(trade -> trade.getIntegerQuantity());
    Stream<Integer> quantitiesSell = tradesSell.map(trade -> trade.getIntegerQuantity());
    Integer sum = quantitiesBuy.collect(Collectors.summingInt(o -> o)) - quantitiesSell.collect(Collectors.summingInt(o -> o));
    return (sum == null) ? 0 : sum;
  }

  private int exitedQuantityTill(long date) {
    Stream<Trade> trades = closeAdviseOrders.stream().filter(order -> order.getOrderTimestamp() <= date)
        .flatMap(o -> o.getTrades().stream());
    Stream<Integer> quantities = trades.map(trade -> trade.getIntegerQuantity());
    Integer sum = quantities.collect(Collectors.summingInt(o -> o));
    return (sum == null) ? 0 : sum;
  }
  
  private double pctReturn(PriceWithPaf price, long date) {
    int remainingQuantity = quantityYetToBeExited(date);
    double exitedValue =  closeAdviseOrders.stream().filter(order -> order.getOrderTimestamp() <= date).map(order -> order.getTotalTradedQuantity() * order.getAverageTradedPrice())
                 .collect(Collectors.summingDouble(o -> o)) ; 
    double remainingValue = remainingQuantity * price.getPrice().getPrice();
    double entryValue = averageEntryPrice.getPrice().getPrice() * tradedEntryQuantity() * cumulativePaf;
    double pctReturn = (entryValue != 0.) ? round((100 * (exitedValue + remainingValue - entryValue)) / entryValue) : 0.;
    return pctReturn;
  }

  @DynamoDBIgnore
  public IssueType getIssueType() {
    return IssueType.Equity;
  }

  @Override
  @DynamoDBIgnore
  public String getUsername() {
    return userAdviseCompositeKey.getUsername();
  }

  @Override
  @DynamoDBIgnore
  @Deprecated
  public void setUsername(String username) {
    userAdviseCompositeKey.setUsernameWithFundWithAdviser(
        updateUsername(userAdviseCompositeKey.getUsernameWithFundWithAdviser(), username));
  }

  @DynamoDBIgnore
  private String updateUsername(String usernameWithFundWithAdviser, String username) {
    String[] strings = usernameWithFundWithAdviser.split(CompositeKeySeparator);
    strings[0] = username;
    return Arrays.stream(strings).collect(Collectors.joining(CompositeKeySeparator));
  }

}