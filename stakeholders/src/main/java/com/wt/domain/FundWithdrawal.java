package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class FundWithdrawal {

  private String fundName;
  private double withdrawalAmount;
  private String wealthCode;
  private String adviserUsername;
  private InvestingMode investingMode;

  @JsonCreator
  public FundWithdrawal(@JsonProperty("adviserUsername") String adviserUsername,
      @JsonProperty("fundName") String fundName, @JsonProperty("withdrawalAmount") double withdrawalAmount,
      @JsonProperty("wealthCode") String wealthCode, @JsonProperty("investingMode") InvestingMode investingMode) {
    super();
    this.adviserUsername = adviserUsername;
    this.fundName = fundName;
    this.withdrawalAmount = withdrawalAmount;
    this.wealthCode = wealthCode;
    this.investingMode = investingMode;
  }
}

