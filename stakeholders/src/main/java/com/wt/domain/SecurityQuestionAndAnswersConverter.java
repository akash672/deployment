package com.wt.domain;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class SecurityQuestionAndAnswersConverter implements DynamoDBTypeConverter<String, Map<String, String>> {

  @Override
  public String convert(Map<String, String> object) {
    return new Gson().toJson(object);
  }

  @Override
  public Map<String, String> unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Map<String, String>>() {
      private static final long serialVersionUID = 1L;
    }.getType());
  }
}
