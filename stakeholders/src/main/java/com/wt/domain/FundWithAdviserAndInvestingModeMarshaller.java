package com.wt.domain;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshaller;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@SuppressWarnings("deprecation")
public class FundWithAdviserAndInvestingModeMarshaller implements DynamoDBMarshaller<List<FundWithAdviserAndInvestingMode>> {
  @Override
  public String marshall(List<FundWithAdviserAndInvestingMode> objects) {
    return new Gson().toJson(objects);
  }

  @Override
  public List<FundWithAdviserAndInvestingMode> unmarshall(Class<List<FundWithAdviserAndInvestingMode>> clazz, String obj) {
    return new Gson().fromJson(obj, new TypeToken<List<FundWithAdviserAndInvestingMode>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }
}