package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class FundSubscription {
  private String fundName;
  private double lumpSumAmount;
  private double sipAmount;
  private String sipDate;
  private String wealthCode;
  private String adviserUsername;
  private InvestingMode investingMode;
  private Purpose investingPurpose;

  @JsonCreator
  public FundSubscription(@JsonProperty("adviserUsername") String adviserUsername,
      @JsonProperty("fundName") String fundName, @JsonProperty("lumpSumAmount") double lumpSumAmount,
      @JsonProperty("sipAmount") double sipAmount, @JsonProperty("date") String sipDate,
      @JsonProperty("wealthCode") String wealthCode, @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("investingPurpose") Purpose investingPurpose) {
	super();
	this.adviserUsername = adviserUsername;
	this.fundName = fundName;
	this.lumpSumAmount = lumpSumAmount;
	this.sipAmount = sipAmount;
	this.sipDate = sipDate;
	this.wealthCode = wealthCode;
	this.investingMode = investingMode;
	this.investingPurpose = investingPurpose;
  }
}
