package com.wt.domain;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.GsonBuilder;

public class UserAnswersConverter implements DynamoDBTypeConverter<String, Map<String, Map<String, String>>> {

  @Override
  public String convert(Map<String, Map<String, String>> object) {
    return new GsonBuilder().disableHtmlEscaping().create().toJson(object);
  }

  @Override
  public Map<String, Map<String, String>> unconvert(String object) {
    return new GsonBuilder().disableHtmlEscaping().create().fromJson(object,
        new TypeToken<Map<String, Map<String, String>>>() {
          private static final long serialVersionUID = 1L;
        }.getType());
  }

}
