package com.wt.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.domain.write.commands.GetMarginRequest;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.MarginRequested;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;

@Service("RealWallet")
public class RealWallet implements Wallet {

  private @Autowired WDActorSelections wdActorSelections;

  @Override
  public void requestWalletMoney(MarginRequested marginRequested) {
    if(marginRequested.getBrokerCompany().equals("MOSL"))
      wdActorSelections.userRootActor().tell(
          new MarginReceived(Double.MAX_VALUE, marginRequested.getUsername(), InvestingMode.REAL), ActorRef.noSender());
    else
      wdActorSelections.equityOrderRootActor().tell(new GetMarginRequest(marginRequested.getClientCode(),
          marginRequested.getUsername(), marginRequested.getBrokerCompany()), ActorRef.noSender());
  }

}
