package com.wt.domain;

import static com.wt.domain.AdviseType.Issue;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.UserResponseType.Awaiting;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
public class UserResponseParameters implements Serializable {
  
  private static final long serialVersionUID = 1L;
  private String adviseId;
  @Expose
  private AdviseType adviseType;
  @Expose
  private String username;
  @Expose
  private String fundName;
  @Expose
  private String adviserName;
  @Expose
  private String clientCode;
  @Expose
  private String brokerName;
  @Expose
  private String exchange;
  @Setter private String token;
  @Expose
  private String symbol;
  private InvestingMode investingMode;
  private UserResponseType userResponse;
  @Expose
  private double absoluteAllocation;
  @Expose
  private double allocationValue;
  @Expose
  private int numberOfShares;
  @Expose
  private long investmentDate;
  private String basketOrderCompositeId;
  @Setter
  private BrokerAuthInformation brokerAuthInformation;

  @JsonCreator
  public UserResponseParameters(@JsonProperty("adviseId") String adviseId, @JsonProperty("fundName") String fundName,
      @JsonProperty("clientCode") String clientCode, @JsonProperty("adviseType") AdviseType adviseType,
      @JsonProperty("username") String username, @JsonProperty("brokerName") String brokerName,
      @JsonProperty("exchange") String exchange, @JsonProperty("adviserName") String adviserName,
      @JsonProperty("token") String token, @JsonProperty("symbol") String symbol,
      @JsonProperty("investingMode") InvestingMode investingMode,
      @JsonProperty("userResponse") UserResponseType userResponse,
      @JsonProperty("absoluteAllocation") double absoluteAllocation,
      @JsonProperty("allocationValue") double allocationValue, @JsonProperty("numberOfShares") int numberOfShares,
      @JsonProperty("investmentDate") long investmentDate) {
    this.adviseId = adviseId;
    this.adviseType = adviseType;
    this.username = username;
    this.fundName = fundName;
    this.adviserName = adviserName;
    this.clientCode = clientCode;
    this.brokerName = brokerName;
    this.exchange = exchange;
    this.token = token;
    this.symbol = symbol;
    this.investingMode = investingMode;
    this.userResponse = userResponse;
    this.absoluteAllocation = absoluteAllocation;
    this.allocationValue = allocationValue;
    this.numberOfShares = numberOfShares;
    this.investmentDate = investmentDate;
  }

  public static class UserResponseParametersBuilder {
    private String adviseId;
    private AdviseType adviseType;
    private String userEmail;
    private String fundName;
    private String adviserName;
    private String clientCode;
    private String brokerName;
    private String exchange;
    private String token;
    private String symbol;
    private InvestingMode investingMode;
    private UserResponseType userResponse;
    private double absoluteAllocation;
    private double allocationValue;
    private int numberOfShares;
    private long investmentDate;
    private String basketOrderCompositeId;
    private BrokerAuthInformation brokerAuthInformation;


    public UserResponseParametersBuilder(String adviseId, AdviseType adviseType, String userEmail, String fundName, String adviserName,
        String clientCode, String brokerName, String exchange, String token, String symbol, InvestingMode investingMode,
        UserResponseType userResponse, long investmentDate) {
      super();
      this.adviseId = adviseId;
      this.adviseType = adviseType;
      this.userEmail = userEmail;
      this.fundName = fundName;
      this.adviserName = adviserName;
      this.clientCode = clientCode;
      this.brokerName = brokerName;
      this.exchange = exchange;
      this.token = token;
      this.symbol = symbol;
      this.investingMode = investingMode;
      this.userResponse = userResponse;
      this.investmentDate = investmentDate;
    }

    public UserResponseParametersBuilder withAllocationValue(double allocationValue) {
      this.allocationValue = allocationValue;
      return this;
    }

    public UserResponseParametersBuilder withAbsoluteAllocation(double absoluteAllocation) {
      this.absoluteAllocation = absoluteAllocation;
      return this;
    }
    
    public UserResponseParametersBuilder withBrokerAuthInformation(BrokerAuthInformation brokerAuthInformation) {
      this.brokerAuthInformation = brokerAuthInformation;
      return this;
    }

    public UserResponseParametersBuilder withNumberOfShares(int numberOfShares) {
      this.numberOfShares = numberOfShares;
      return this;
    }

    public UserResponseParametersBuilder withBasketOrderCompositeId(String basketOrderCompositeId) {
      this.basketOrderCompositeId = basketOrderCompositeId;
      return this;
    }

    public UserResponseParametersBuilder withAdviseType(AdviseType adviseType) {
      this.adviseType = adviseType;
      return this;
    }

    public UserResponseParameters build() {
      return new UserResponseParameters(this);
    }
  }

  private UserResponseParameters(UserResponseParametersBuilder builder) {
    this.adviseId = builder.adviseId;
    this.adviseType = builder.adviseType;
    this.username = builder.userEmail;
    this.fundName = builder.fundName;
    this.adviserName = builder.adviserName;
    this.clientCode = builder.clientCode;
    this.brokerName = builder.brokerName;
    this.exchange = builder.exchange;
    this.token = builder.token;
    this.symbol = builder.symbol;
    this.investingMode = builder.investingMode;
    this.userResponse = builder.userResponse;
    this.investmentDate = builder.investmentDate;
    this.absoluteAllocation = builder.absoluteAllocation;
    this.allocationValue = builder.allocationValue;
    this.numberOfShares = builder.numberOfShares;
    this.basketOrderCompositeId = builder.basketOrderCompositeId;
    this.brokerAuthInformation = builder.brokerAuthInformation;
  }

  public static UserResponseParameters withBlankResponse() {
    return new UserResponseParameters("", "", "", Issue, "", "", "", "", "", "", VIRTUAL, Awaiting, 0.0, 0.0, 0, 0L);
  }
}
