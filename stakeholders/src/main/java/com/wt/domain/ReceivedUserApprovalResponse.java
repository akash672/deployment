package com.wt.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.Getter;
import lombok.ToString;

@ToString
public class ReceivedUserApprovalResponse {
  @Getter private String allUserResponses;
  @Getter private String basketOrderId;
  @Getter private BrokerAuthInformation brokerAuthInformation;
  
  @JsonCreator
  public ReceivedUserApprovalResponse(@JsonProperty("userResponseParameters") String allUserResponses, @JsonProperty("basketOrderId") String basketOrderId,
      @JsonProperty("brokerAuthentication") BrokerAuthInformation brokerAuthInformation) {
    super();
    this.allUserResponses = allUserResponses;
    this.basketOrderId = basketOrderId;
    this.brokerAuthInformation = brokerAuthInformation;
  }
 
}
