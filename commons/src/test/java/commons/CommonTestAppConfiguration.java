package commons;

import java.io.IOException;
import java.util.Properties;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.amazonaws.services.sns.AmazonSNS;
import com.wt.config.utils.WDProperties;
import com.wt.utils.sms.factory.SMSFactory;

@Configuration
@PropertySource("classpath:test-environment.properties")
public class CommonTestAppConfiguration {

  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("test-environment.properties"));
    return properties;
  }

  @Bean
  public WDProperties wdproperties() {
    return new WDProperties();
  }

  @Bean(name = "SNSClient")
  public AmazonSNS snsClientCreation() {
    return Mockito.mock(AmazonSNS.class);
  }

  @Bean
  public SMSFactory smsFactory() throws Exception {
    return Mockito.mock(SMSFactory.class);
  }
}
