package commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.hibernate.validator.internal.constraintvalidators.hv.URLValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.wt.config.utils.WDProperties;
import com.wt.utils.aws.SESEmailUtil;
import com.wt.utils.email.templates.wealthdesk.KYCDoneEmail;
import com.wt.utils.email.templates.wealthdesk.UserSignedOnMailer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CommonTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class SESEmailUtilShould {

    private static final String INVESTOR_NAME = "UJJWAL TESTUSER";
    private static final String CLIENT_CODE = "TEST001";
    private static final String BROKER_COMPANY = "BP WEALTH";
    private static final String TEST_EMAIL_MESSAGE = "This Mail has been send fron JUnit Test Code";
    private static final String TEST_MESSAGE_TITLE = "Test Mail Being Send";
    private static final String TO_EMAIL = "app@wealthtech.in";
    private static final String FROM_EMAIL = "team@wealthtech";
    private SESEmailUtil emailer = Mockito.mock(SESEmailUtil.class);
    @Autowired
    private WDProperties properties;

    @Test
    public void send_an_email() {
        Mockito.doReturn(new SendEmailResult()).when(emailer).sendEmail(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString());
        SendEmailResult sendEmail = emailer.sendEmail(FROM_EMAIL, TO_EMAIL, TEST_MESSAGE_TITLE, TEST_EMAIL_MESSAGE);

        assertNotNull(sendEmail);
        assertEquals(true, (sendEmail instanceof SendEmailResult));
    }

    @Test
    public void check_preparation_of_the_KYC_email_being_sent() {
        KYCDoneEmail mailer = new KYCDoneEmail(TO_EMAIL, BROKER_COMPANY, CLIENT_CODE, INVESTOR_NAME, properties);
        Document html = Jsoup.parse(mailer.getHTMLTextformat());
        assertTrue(html.text().contains(BROKER_COMPANY + " Client Code is: " + CLIENT_CODE));
        assertTrue(html.text().contains("Congratulations " + INVESTOR_NAME + "!"));
        String emailerURLPlaceholder = html.getElementsContainingText("view in browser").attr("href");
        new URLValidator().isValid(emailerURLPlaceholder, null);
        assertTrue(emailerURLPlaceholder.contains("UserKYCCompleted"));
    }
    
    @Test
    public void check_preparation_of_the_user_sign_on_email_being_sent() throws IOException {
        UserSignedOnMailer mailer = new UserSignedOnMailer(TO_EMAIL, INVESTOR_NAME, properties);
        String htmlTextformat = mailer.getHTMLTextformat();
        Document html = Jsoup.parse(htmlTextformat);
        assertEquals("Your set username is: "+TO_EMAIL+" To get started, log onto your account on WealthDesk Mobile App.",
                html.selectFirst("body > table > tbody > tr:nth-child(3) > td > p:nth-child(2)").text());
        
        String emailerURLPlaceholder = html.getElementsContainingText("view in browser").attr("href");
        new URLValidator().isValid(emailerURLPlaceholder, null);
        assertTrue(emailerURLPlaceholder.contains("UserSignOn"));
    }
}
