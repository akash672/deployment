Create table benchmark(fund varchar(50),currentdate bigint,open double, high double,low double,close double,changes double, PRIMARY KEY(fund,currentdate));

Create table fund_performance(fund varchar(50),adviser_username varchar(50),date bigint,realized_mtm double,unrealized_mtm double,performance double,cash double, PRIMARY KEY(fund,adviser_username,date));

Create table fund_advise_performance(fund varchar(50),adviser_username varchar(50),advise_id varchar(50),realized_mtm double,unrealized_mtm double,performance double, PRIMARY KEY(fund,adviser_username,advise_id,date));

Create table prices(ticker varchar(50),price_time bigint,time bigint,unadjusted_price double,adjusted_price double,cumulative_paf double,dividend double,historical_paf double,nondividend_paf double, PRIMARY KEY(ticker,price_time));

Create table fund (fundNameWithAdviserUsername  VARCHAR(750) NOT NULL , startdate BIGINT, performancecalculatedTill BIGINT, enddate BIGINT, PRIMARY KEY(fundNameWithAdviserUsername));

CREATE TABLE adviseTransaction(adviseId VARCHAR(750),fundNameWithAdviserUsername VARCHAR(750),asOfDate BIGINT,wdId VARCHAR(200),allocation DOUBLE,orderSide VARCHAR(50),entryPrice DOUBLE,exitPrice DOUBLE,PRIMARY KEY(adviseId, fundNameWithAdviserUsername, asOfDate));

CREATE TABLE userFundTransaction(username VARCHAR(750) NOT NULL  ,fundNameWithAdviserUsername VARCHAR(750) NOT NULL  , investingMode VARCHAR(50)  NOT NULL , lastUpdatedTime BIGINT,fundCashFlow DOUBLE,PRIMARY KEY(username, fundNameWithAdviserUsername, investingMode, lastUpdatedTime),FOREIGN KEY(username, fundNameWithAdviserUsername, investingMode) REFERENCES userFund(username, fundNameWithAdviserUsername, investingMode));

CREATE TABLE userAdviseTransaction(adviseId VARCHAR(750) NOT NULL  ,fundNameWithAdviserUsername VARCHAR(750) NOT NULL , lastUpdatedTime BIGINT,investingMode VARCHAR(50),username VARCHAR(750),orderSide VARCHAR(30),amountBought DOUBLE,wdId VARCHAR(500),amountSold DOUBLE,numberOfSharesTransacted DOUBLE,PRIMARY KEY(username,adviseId, fundNameWithAdviserUsername, investingMode, lastUpdatedTime),FOREIGN KEY(username, adviseId, fundNameWithAdviserUsername, investingMode) REFERENCES userAdvise( username,adviseId, fundNameWithAdviserUsername, investingMode));


CREATE TABLE userAdvise(username VARCHAR(750) NOT NULL  ,adviseId VARCHAR(750)  NOT NULL  ,fundNameWithAdviserUsername VARCHAR(750) NOT NULL  , investingMode VARCHAR(50),adviseStartTime BIGINT,adviseEndTime BIGINT,PRIMARY KEY(username, adviseId, fundNameWithAdviserUsername, investingMode));

CREATE TABLE userFund(username VARCHAR(750) NOT NULL  ,investingMode VARCHAR(50)  NOT NULL  ,fundNameWithAdviserUsername VARCHAR(750) NOT NULL  , subscriptionTimeStamp BIGINT,unsubscriptionTimeStamp BIGINT,PRIMARY KEY(username, fundNameWithAdviserUsername, investingMode));

CREATE TABLE user(username VARCHAR(750) NOT NULL  ,investingMode VARCHAR(50) NOT NULL  , clientCode VARCHAR(750)  NOT NULL  ,onboardedTimeStamp BIGINT,  expiryTimeStamp BIGINT, PRIMARY KEY(username, investingMode));

CREATE TABLE userFundDividendTransaction(username VARCHAR(750) NOT NULL  ,fundNameWithAdviserUsername VARCHAR(750) NOT NULL  , investingMode VARCHAR(50)  NOT NULL  ,lastUpdatedTime BIGINT,dividendValue DOUBLE,PRIMARY KEY(username, fundNameWithAdviserUsername, investingMode, lastUpdatedTime),FOREIGN KEY(username, fundNameWithAdviserUsername, investingMode) REFERENCES userFund(username, fundNameWithAdviserUsername, investingMode));

CREATE TABLE userAdvisePerformance(adviseId VARCHAR(750), username VARCHAR(750), fundNameWithAdviserUsername VARCHAR(750), investingMode VARCHAR(50),	lastUpdatedTime BIGINT,	wdId VARCHAR(750),	amountWithdrawn DOUBLE,	currentValue DOUBLE,	amountInvested DOUBLE,	returnPct DOUBLE,	PRIMARY KEY(username, adviseId, fundNameWithAdviserUsername, investingMode, lastUpdatedTime),FOREIGN KEY(username, adviseId, fundNameWithAdviserUsername, investingMode) REFERENCES userAdvise( username, adviseId, fundNameWithAdviserUsername, investingMode));
	
CREATE TABLE userFundPerformance(username VARCHAR(750), fundNameWithAdviserUsername VARCHAR(750), investingMode VARCHAR(50), lastUpdatedTime BIGINT, pnlLtd DOUBLE, currentPnl DOUBLE, amountInvested DOUBLE, returnPct DOUBLE, irr DOUBLE, PRIMARY KEY(username, fundNameWithAdviserUsername, investingMode, lastUpdatedTime),FOREIGN KEY(username, fundNameWithAdviserUsername, investingMode) REFERENCES userFund(username, fundNameWithAdviserUsername, investingMode));

CREATE TABLE userPerformance(username VARCHAR(750), investingMode VARCHAR(50), lastUpdatedTime BIGINT, pnlLtd DOUBLE, currentPnl DOUBLE, amountInvested DOUBLE, returnPct DOUBLE, PRIMARY KEY(username, investingMode, lastUpdatedTime), FOREIGN KEY(username, investingMode) REFERENCES user(username,investingMode))
	
CREATE TABLE equityInstrument(wdId VARCHAR(750), symbol VARCHAR(1500),  PRIMARY KEY(wdId));

CREATE TABLE sequenceStore(eventType VARCHAR(750), seqNum BIGINT,  PRIMARY KEY(((eventType));