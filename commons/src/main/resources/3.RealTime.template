{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Cloudformation Template for Real Time Service. ",
  "Parameters": {
    "RealTimeServiceAZ1": {
      "Type": "AWS::EC2::AvailabilityZone::Name",
      "Description": "Select an AvailabilityZone for RealtimeService (Note: Selected AZ must be different from RealtimeService AZ2) "
    },
    "RealTimeServiceAZ2": {
      "Type": "AWS::EC2::AvailabilityZone::Name",
      "Description": "Select an AvailabilityZone for RealtimeService (Note: Selected AZ must be different from RealtimeService AZ1) "
    },
    "RealTimeServiceCIDR1": {
      "Type": "String",
      "Default": "10.10.5.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetA "
    },
    "RealTimeServiceCIDR2": {
      "Type": "String",
      "Default": "10.10.6.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetB"
    },
    "RealTimeServicePublicAZ1": {
      "Type": "AWS::EC2::AvailabilityZone::Name",
      "Description": "Select an AvailabilityZone for RealtimeService (Note: Selected AZ must be different from RealtimeService AZ2) "
    },
    "RealTimeServicePublicAZ2": {
      "Type": "AWS::EC2::AvailabilityZone::Name",
      "Description": "Select an AvailabilityZone for RealtimeService (Note: Selected AZ must be different from RealtimeService AZ1) "
    },
    "RealTimeServicePublicCIDR1": {
      "Type": "String",
      "Default": "10.10.3.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetA "
    },
    "RealTimeServicePublicCIDR2": {
      "Type": "String",
      "Default": "10.10.4.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetB"
    },
    "RealTimeServiceSubnetNameA": {
      "Type": "String",
      "Description": "Enter Subnet Name for RealtimeService\t(Note: Name is preceded by \"RealtimePrivate\")"
    },
    "RealTimeServiceSubnetNameB": {
      "Type": "String",
      "Description": "Enter Subnet Name for RealtimeService (Note: Name is preceded by \"RealtimePrivate\")"
    },
    "RealTimeServiceAMITag": {
      "Type": "String",
      "Description": "Enter the Environment Name (Note: name is preceded by \"RealTimeService\")"
    },
    "RealTimeServerVoulmeTermination": {
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "false",
        "true"
      ],
      "Description": "Select \"true\" if volume is to be retained post Server Deletion"
    },
    "RealTimeServerTerminationProtection": {
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "false",
        "true"
      ],
      "Description": "Selecting \"true\" will require to disable the termination protection manually from the console"
    },
    "RealTimeServiceELBTag": {
      "Type": "String",
      "Description": "Enter the Environment Name for ELB (Note: name is preceded by \"RealTime\".)"
    },
    "RealTimeServiceInstanceType": {
      "Type": "String",
      "Default": "t2.micro",
      "AllowedValues": [
        "t1.micro",
        "t2.nano",
        "t2.micro",
        "t2.small",
        "t2.medium",
        "t2.large",
        "m1.small",
        "m1.medium",
        "m1.large",
        "m1.xlarge",
        "m2.xlarge",
        "m2.2xlarge",
        "m2.4xlarge",
        "m3.medium",
        "m3.large",
        "m3.xlarge",
        "m3.2xlarge",
        "m4.large",
        "m4.xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.10xlarge",
        "c1.medium",
        "c1.xlarge",
        "c3.large",
        "c3.xlarge",
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "c4.large",
        "c4.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "g2.2xlarge",
        "g2.8xlarge",
        "r3.large",
        "r3.xlarge",
        "r3.2xlarge",
        "r3.4xlarge",
        "r3.8xlarge",
        "i2.xlarge",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "d2.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "hi1.4xlarge",
        "hs1.8xlarge",
        "cr1.8xlarge",
        "cc2.8xlarge",
        "cg1.4xlarge"
      ],
      "Description": "Select the instance type "
    },
    "RealTimeServerVolumeSize": {
      "Type": "Number",
      "Description": "EBS VolumeSize (GB) ( Note: Miniumum accepted 8 GB )",
      "Default": "10"
    },
    "RealTimeServiceKeyPair": {
      "Description": "Select the existing Key",
      "Type": "AWS::EC2::KeyPair::KeyName"
    },
    "RealTimeServiceHealthCheckPort": {
      "Type": "Number",
      "Description": "Enter port number where Elastic Load Balancer sends health check requests "
    },
    "AutoScalingDesiredCapacity": {
      "Type": "Number",
      "Default": "1",
      "Description": "Number of instances to launch in the Autoscaling Group."
    },
    "AutoScalingMaxInstanceNumber": {
      "Type": "Number",
      "Default": "1",
      "Description": "Maximum number of instances that can be launched in the AutoScaling Group."
    },
    "CerttificateARN": {
      "Default": "",
      "Description": "ARN for the certificate",
      "Type": "String"
    }
  },
  "Mappings": {
    "RegionMap": {
      "us-east-1": {
        "AMI": "ami-a4827dc9"
      },
      "us-west-1": {
        "AMI": "ami-11790371"
      },
      "us-west-2": {
        "AMI": "ami-f303fb93"
      },
      "ap-south-1": {
        "AMI": "ami-e2b6dc8d"
      },
      "ap-northeast-2": {
        "AMI": "ami-69e92207"
      },
      "ap-southeast-1": {
        "AMI": "ami-a2c111c1"
      },
      "ap-southeast-2": {
        "AMI": "ami-d9d7f9ba"
      },
      "ap-northeast-1": {
        "AMI": "ami-6154bb00"
      },
      "eu-central-1": {
        "AMI": "ami-7df01e12"
      },
      "eu-west-1": {
        "AMI": "ami-c39604b0"
      },
      "sa-east-1": {
        "AMI": "ami-106ee57c"
      }
    }
  },
  "Metadata": {
    "AWS::CloudFormation::Interface": {
      "ParameterGroups": [
        {
          "Label": {
            "default": "Public Subnet Details"
          },
          "Parameters": [
            "RealTimeServicePublicAZ1",
            "RealTimeServicePublicCIDR1",
            "RealTimeServicePublicAZ2",
            "RealTimeServicePublicCIDR2"
          ]
        },
        {
          "Label": {
            "default": "Private Subnet Details"
          },
          "Parameters": [
            "RealTimeServiceSubnetNameA",
            "RealTimeServiceAZ1",
            "RealTimeServiceCIDR1",
            "RealTimeServiceSubnetNameB",
            "RealTimeServiceAZ2",
            "RealTimeServiceCIDR2"
          ]
        },
        {
          "Label": {
            "default": "Real Time Server Details"
          },
          "Parameters": [
            "RealTimeServiceAMITag",
            "RealTimeServiceInstanceType",
            "RealTimeServerVolumeSize",
            "RealTimeServiceKeyPair",
            "RealTimeServerTerminationProtection",
            "RealTimeServerVoulmeTermination"
          ]
        },
        {
          "Label": {
            "default": "Load Balancer Details"
          },
          "Parameters": [
            "RealTimeServiceELBTag",
            "RealTimeServiceHealthCheckPort"
          ]
        },
        {
          "Label": {
            "default": "Auto Scaling Details for RealtimeService"
          },
          "Parameters": [
            "AutoScalingDesiredCapacity",
            "AutoScalingMaxInstanceNumber"
          ]
        },
        {
          "Label": {
            "default": "Certificate Details"
          },
          "Parameters": [
            "CerttificateARN"
          ]
        }
      ],
      "ParameterLabels": {
        "RealTimeServiceCIDR1": {
          "default": "Subnet A "
        },
        "RealTimeServiceCIDR2": {
          "default": "Subnet B "
        },
        "RealTimeServicePublicCIDR1": {
          "default": "Availability Zone A "
        },
        "RealTimeServicePublicCIDR2": {
          "default": "Availability Zone B "
        },
        "RealTimeServicePublicAZ1": {
          "default": "Subnet A "
        },
        "RealTimeServicePublicAZ2": {
          "default": "Subnet B "
        },
        "RealTimeServerVoulmeTermination": {
          "default": "Volume Termination "
        },
        "RealTimeServiceSubnetNameA": {
          "default": "Subnet Name A "
        },
        "RealTimeServiceSubnetNameB": {
          "default": "Subnet Name B "
        },
        "RealTimeServiceAZ1": {
          "default": "Availability Zone 1 "
        },
        "RealTimeServiceAZ2": {
          "default": "Availability Zone 2 "
        },
        "RealTimeServiceAMITag": {
          "default": "AMI Tag "
        },
        "RealTimeServiceInstanceType": {
          "default": "Instance Type "
        },
        "RealTimeServerVolumeSize": {
          "default": "Volume Size "
        },
        "RealTimeServiceKeyPair": {
          "default": "Key Pair "
        },
        "RealTimeServiceHealthCheckPort": {
          "default": "Health Check Port "
        },
        "AutoScalingDesiredCapacity": {
          "default": "Desired Capacity "
        },
        "AutoScalingMaxInstanceNumber": {
          "default": "Max Instance Number "
        },
        "CerttificateARN": {
          "default": "ACM Certificate "
        },
        "RealTimeServerTerminationProtection": {
          "default": "Termination Protection"
        }
      }
    }
  },
  "Resources": {
    "RealTimePublicSubnetA": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "RealTimeServicePublicCIDR1"
        },
        "AvailabilityZone": {
          "Ref": "RealTimeServicePublicAZ1"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": "RealPublicSubnetA"
          }
        ]
      }
    },
    "RealTimePublicSubnetB": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "RealTimeServicePublicCIDR2"
        },
        "AvailabilityZone": {
          "Ref": "RealTimeServicePublicAZ2"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": "RealPublicSubnetB"
          }
        ]
      }
    },
    "PublicSubnetAssociationA": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "RealTimePublicSubnetA"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PublicRouteTableID"
        }
      }
    },
    "PublicSubnetAssociationB": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "RealTimePublicSubnetB"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PublicRouteTableID"
        }
      }
    },
    "RealTimePrivateSubnetA": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "RealTimeServiceCIDR1"
        },
        "AvailabilityZone": {
          "Ref": "RealTimeServiceAZ1"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "RT-Private",
                  {
                    "Ref": "RealTimeServiceSubnetNameA"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "RealTimePrivateSubnetB": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "RealTimeServiceCIDR2"
        },
        "AvailabilityZone": {
          "Ref": "RealTimeServiceAZ2"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "RT-Private",
                  {
                    "Ref": "RealTimeServiceSubnetNameB"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "RealTimesubentassociation1": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "RealTimePrivateSubnetA"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PrivateRouteTableID"
        }
      }
    },
    "RealTimesubnetassociation2": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "RealTimePrivateSubnetB"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PrivateRouteTableID"
        }
      }
    },
    "RealTimeServer": {
      "Type": "AWS::EC2::Instance",
      "Properties": {
        "IamInstanceProfile": {
          "Fn::ImportValue": "Role-InstanceProfile"
        },
        "InstanceType": {
          "Ref": "RealTimeServiceInstanceType"
        },
        "DisableApiTermination": {
          "Ref": "RealTimeServerTerminationProtection"
        },
        "SubnetId": {
          "Ref": "RealTimePrivateSubnetA"
        },
        "ImageId": {
          "Fn::FindInMap": [
            "RegionMap",
            {
              "Ref": "AWS::Region"
            },
            "AMI"
          ]
        },
        "SecurityGroupIds": [
          {
            "Ref": "RealTimeSecurityGroup"
          }
        ],
        "UserData": {
          "Fn::Base64": {
            "Fn::Join": [
              "",
              [
                "#!/bin/bash\n",
                "export TZ=Asia/Kolkata\n",
                "sudo yum update -y\n",
                "sudo yum install java-1.8.0\n",
                "sudo yum remove java-1.7*\n",
                "sudo yum remove java-1.7.0-openjdk\n",
                "sudo yum install ruby -y\n",
                "sudo yum install wget -y \n",
                "cd /home/ec2-user\n",
                "wget https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install\n",
                "chmod +x ./install\n",
                "sudo ./install auto\n",
                "sudo service codedeploy-agent start"
              ]
            ]
          }
        },
        "BlockDeviceMappings": [
          {
            "DeviceName": "/dev/xvda",
            "Ebs": {
              "VolumeSize": {
                "Ref": "RealTimeServerVolumeSize"
              },
              "DeleteOnTermination": {
                "Ref": "RealTimeServerVoulmeTermination"
              },
              "VolumeType": "gp2"
            }
          }
        ],
        "KeyName": {
          "Ref": "RealTimeServiceKeyPair"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "RealTime",
                  {
                    "Ref": "RealTimeServiceAMITag"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "RealTimeSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupName": "RealTimeSecurityGroup",
        "GroupDescription": "SG for RealTime Service",
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "SecurityGroupIngress": [
          {
            "IpProtocol": "tcp",
            "FromPort": "2552",
            "ToPort": "2552",
            "CidrIp": {
              "Fn::ImportValue": "Network-VPCRange"
            },
            "Description": "Allowing Service"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "443",
            "ToPort": "443",
            "CidrIp": "0.0.0.0/0"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "80",
            "ToPort": "80",
            "CidrIp": "0.0.0.0/0"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "22",
            "ToPort": "22",
            "CidrIp": {
              "Fn::ImportValue": "Network-BastionEIP"
            },
            "Description" :"Allowing SSH to Bastion Host"
          }
        ],
        "Tags": [
          {
            "Key": "Name",
            "Value": "RealTimeSecurityGroup"
          }
        ]
      }
    },
    "LoadBalancer": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Subnets": [
          {
            "Ref": "RealTimePublicSubnetA"
          },
          {
            "Ref": "RealTimePublicSubnetB"
          }
        ],
        "Instances": [
          {
            "Ref": "RealTimeServer"
          }
        ],
        "Listeners": [
          {
            "LoadBalancerPort": "443",
            "InstancePort": "80",
            "Protocol": "SSL",
            "SSLCertificateId": {
              "Ref": "CerttificateARN"
            }
          }
        ],
        "SecurityGroups": [
          {
            "Ref": "RealTimeSecurityGroup"
          }
        ],
        "HealthCheck": {
          "Target": {
            "Fn::Join": [
              ":",
              [
                "TCP",
                {
                  "Ref": "RealTimeServiceHealthCheckPort"
                }
              ]
            ]
          },
          "HealthyThreshold": "3",
          "UnhealthyThreshold": "5",
          "Interval": "30",
          "Timeout": "5"
        },
        "LoadBalancerName": "RealTimeLoadBalancer",
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "RT",
                  {
                    "Ref": "RealTimeServiceELBTag"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "LaunchConfiguration": {
      "Type": "AWS::AutoScaling::LaunchConfiguration",
      "Properties": {
        "LaunchConfigurationName": "RealTime",
        "AssociatePublicIpAddress": false,
        "KeyName": {
          "Ref": "RealTimeServiceKeyPair"
        },
        "ImageId": {
          "Fn::FindInMap": [
            "RegionMap",
            {
              "Ref": "AWS::Region"
            },
            "AMI"
          ]
        },
        "IamInstanceProfile": {
          "Fn::ImportValue": "Role-InstanceProfile"
        },
        "InstanceType": {
          "Ref": "RealTimeServiceInstanceType"
        },
        "SecurityGroups": [
          {
            "Ref": "RealTimeSecurityGroup"
          }
        ],
        "BlockDeviceMappings": [
          {
            "DeviceName": "/dev/xvda",
            "Ebs": {
              "VolumeSize": {
                "Ref": "RealTimeServerVolumeSize"
              },
              "VolumeType": "gp2"
            }
          }
        ],
        "UserData": {
          "Fn::Base64": {
            "Fn::Join": [
              "",
              [
                "#!/bin/bash\n",
                "export TZ=Asia/Kolkata\n",
                "sudo yum update -y\n",
                "sudo yum install java-1.8.0\n",
                "sudo yum remove java-1.7*\n",
                "sudo yum remove java-1.7.0-openjdk\n",
                "sudo yum install ruby -y\n",
                "sudo yum install wget -y \n",
                "cd /home/ec2-user\n",
                "wget https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install\n",
                "chmod +x ./install\n",
                "sudo ./install auto\n",
                "sudo service codedeploy-agent start"
              ]
            ]
          }
        }
      }
    },
    "AutoScalingGroup": {
      "Type": "AWS::AutoScaling::AutoScalingGroup",
      "Properties": {
        "AutoScalingGroupName": "RealTimeService",
        "VPCZoneIdentifier": [
          {
            "Ref": "RealTimePrivateSubnetA"
          },
          {
            "Ref": "RealTimePrivateSubnetB"
          }
        ],
        "DesiredCapacity": {
          "Ref": "AutoScalingDesiredCapacity"
        },
        "LaunchConfigurationName": {
          "Ref": "LaunchConfiguration"
        },
        "MaxSize": {
          "Ref": "AutoScalingMaxInstanceNumber"
        },
        "MinSize": 0,
        "LoadBalancerNames": [
          {
            "Ref": "LoadBalancer"
          }
        ],
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "RealTimeAutoScaling",
                  {
                    "Ref": "RealTimeServiceAMITag"
                  }
                ]
              ]
            },
            "PropagateAtLaunch": true
          }
        ]
      }
    },
    "ScaleUpPolicy": {
      "Type": "AWS::AutoScaling::ScalingPolicy",
      "Properties": {
        "AdjustmentType": "ChangeInCapacity",
        "AutoScalingGroupName": {
          "Ref": "AutoScalingGroup"
        },
        "Cooldown": "1",
        "ScalingAdjustment": "-1"
      }
    },
    "HighCPUAlarm": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "EvaluationPeriods": "1",
        "Statistic": "Average",
        "Threshold": "90",
        "AlarmDescription": "Alarm if CPU too high",
        "Period": "60",
        "AlarmActions": [
          {
            "Ref": "ScaleUpPolicy"
          }
        ],
        "Namespace": "AWS/EC2",
        "Dimensions": [
          {
            "Name": "AutoScalingGroupName",
            "Value": {
              "Ref": "AutoScalingGroup"
            }
          }
        ],
        "ComparisonOperator": "GreaterThanThreshold",
        "MetricName": "CPUUtilization"
      }
    }
  },
  "Outputs": {
    "RealTimeSecurityGroup": {
      "Description": "Real Time Security Group",
      "Value": {
        "Ref": "RealTimeSecurityGroup"
      },
      "Export": {
        "Name": "RealTime-SecurityGroup"
      }
    },
    "RealTimeServiceELBDNSid": {
      "Description": "Real Time Service ELB",
      "Value": {
        "Fn::GetAtt": [
          "LoadBalancer",
          "CanonicalHostedZoneNameID"
        ]
      },
      "Export": {
        "Name": "RealTime-ELB-DNSid"
      }
    },
    "RealTimeServiceELBDNSname": {
      "Description": "Real Time Service ELB",
      "Value": {
        "Fn::GetAtt": [
          "LoadBalancer",
          "CanonicalHostedZoneName"
        ]
      },
      "Export": {
        "Name": "RealTime-ELB-DNSname"
      }
    }
  }
}
