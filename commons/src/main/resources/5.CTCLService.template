{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Cloudformation Template for Equity CTCL Service. It will create Subnets, EC2 Machines, ELB and Security Group",
  "Parameters": {
    "CTCLPrivateSubnetAZ1": {
      "Type": "AWS::EC2::AvailabilityZone::Name",
      "Description": "Select an AvailabilityZone for CTCL Service (Note: Selected AZ must be different from CTCL Subnet AZ2)"
    },
    "CTCLPrivateSubnetAZ2": {
      "Type": "AWS::EC2::AvailabilityZone::Name",
      "Description": "Select an AvailabilityZone for CTCL Service (Note: Selected AZ must be different from CTCL Subnet AZ1)"
    },
    "CTCLPrivateSubnet1": {
      "Type": "String",
      "Description": "Enter Subnet Name (Note: Name is preceded by \"CTCL-Private\")"
    },
    "CTCLPrivateSubnet2": {
      "Type": "String",
      "Description": "Enter Subnet Name (Note: Name is preceded by \"CTCL-Private\")"
    },
    "CTCLeventSubnetName1": {
      "Type": "String",
      "Description": "Enter Subnet Name (Note: Name is preceded by \"CTCLeventstore\")"
    },
    "CTCLeventSubnetName2": {
      "Type": "String",
      "Description": "Enter Subnet Name (Note: Name is preceded by \"CTCLeventstore\")"
    },
    "EventStorePrivateAZ1": {
      "Type": "AWS::EC2::AvailabilityZone::Name"
    },
    "EventStorePrivateAZ2": {
      "Type": "AWS::EC2::AvailabilityZone::Name"
    },
    "CTCLprivatesubnet1aCIDR": {
      "Type": "String",
      "Default": "10.10.25.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetA"
    },
    "CTCLPrivateSubnet1bCIDR": {
      "Type": "String",
      "Default": "10.10.26.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetB"
    },
    "privatesubnet1aCIDR": {
      "Type": "String",
      "Default": "10.10.27.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetA"
    },
    "PrivateSubnet1bCIDR": {
      "Type": "String",
      "Default": "10.10.28.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the SubnetB"
    },
    "EventStorePublicAZ1": {
      "Type": "AWS::EC2::AvailabilityZone::Name"
    },
    "EventStorePublicAZ2": {
      "Type": "AWS::EC2::AvailabilityZone::Name"
    },
    "PublicSubnetEventStoreCIDR1": {
      "Type": "String",
      "Default": "10.10.29.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the EventStore Subnet"
    },
    "PublicSubnetEventStoreCIDR2": {
      "Type": "String",
      "Default": "10.10.30.0/24",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description": "Enter the CIDR range for the EventStore Subnet"
    },
    "CTCLec2Tag": {
      "Type": "String",
      "Description": "Enter the Environment Name (Note: name is preceded by \"CTCL\")"
    },
    "CTCLelbTag": {
      "Type": "String",
      "Description": "Enter the Environment/Tag Name for ELB (Note: Name is preceded by \"CTCL\")"
    },
    "CTCLvolumetermination": {
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "false",
        "true"
      ],
      "Description": "Select \"true\" if volume is to be retained post Server Deletion"
    },
    "CTCLEC2termination": {
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "false",
        "true"
      ],
      "Description": "Selecting \"true\" will require to disable the termination protection manually from the console"
    },
    "InstanceType": {
      "Type": "String",
      "Default": "t2.micro",
      "AllowedValues": [
        "t1.micro",
        "t2.nano",
        "t2.micro",
        "t2.small",
        "t2.medium",
        "t2.large",
        "m1.small",
        "m1.medium",
        "m1.large",
        "m1.xlarge",
        "m2.xlarge",
        "m2.2xlarge",
        "m2.4xlarge",
        "m3.medium",
        "m3.large",
        "m3.xlarge",
        "m3.2xlarge",
        "m4.large",
        "m4.xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.10xlarge",
        "c1.medium",
        "c1.xlarge",
        "c3.large",
        "c3.xlarge",
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "c4.large",
        "c4.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "g2.2xlarge",
        "g2.8xlarge",
        "r3.large",
        "r3.xlarge",
        "r3.2xlarge",
        "r3.4xlarge",
        "r3.8xlarge",
        "i2.xlarge",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "d2.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "hi1.4xlarge",
        "hs1.8xlarge",
        "cr1.8xlarge",
        "cc2.8xlarge",
        "cg1.4xlarge"
      ],
      "Description": "Select the instance type "
    },
    "CTCLVolumeSize": {
      "Type": "Number",
      "Description": "EBS VolumeSize (GB)",
      "Default": "10"
    },
    "CTCLKeyPair": {
      "Description": "Select the exsisting Key",
      "Type": "AWS::EC2::KeyPair::KeyName"
    },
    "HealthCheckPort": {
      "Type": "Number",
      "Description": "Enter port number where Elastic Load Balancer sends health check requests "
    },
    "CTCLSGname": {
      "Description": "Enter name for Security Group. (Note: SG name is preceded by \"CTCL\")",
      "Type": "String"
    },
    "CTCLSGDestination": {
      "Description": "Enter Destination Address for External 1115 Port",
      "Type": "String",
      "Default": "0.0.0.0/0",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    },
    "CTCLEventstoreEC2Tag": {
      "Type": "String",
      "Description": "Environment Name (Note: Tag name is preceded by \"CTCL-EventStore\")"
    },
    "CTCLeventstoreVolumeTermination": {
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "false",
        "true"
      ],
      "Description": "Select \"true\" if volume is to be retained post Server Deletion"
    },
    "CTCLserverTermination": {
      "Type": "String",
      "Default": "false",
      "AllowedValues": [
        "false",
        "true"
      ],
      "Description": "Selecting \"true\" will require to disable the termination protection manually from the console"
    },
    "InstanceTypeEvent": {
      "Type": "String",
      "Default": "t2.micro",
      "AllowedValues": [
        "t1.micro",
        "t2.nano",
        "t2.micro",
        "t2.small",
        "t2.medium",
        "t2.large",
        "m1.small",
        "m1.medium",
        "m1.large",
        "m1.xlarge",
        "m2.xlarge",
        "m2.2xlarge",
        "m2.4xlarge",
        "m3.medium",
        "m3.large",
        "m3.xlarge",
        "m3.2xlarge",
        "m4.large",
        "m4.xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.10xlarge",
        "c1.medium",
        "c1.xlarge",
        "c3.large",
        "c3.xlarge",
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "c4.large",
        "c4.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "g2.2xlarge",
        "g2.8xlarge",
        "r3.large",
        "r3.xlarge",
        "r3.2xlarge",
        "r3.4xlarge",
        "r3.8xlarge",
        "i2.xlarge",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "d2.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "hi1.4xlarge",
        "hs1.8xlarge",
        "cr1.8xlarge",
        "cc2.8xlarge",
        "cg1.4xlarge"
      ],
      "Description": "Select the Instance type"
    },
    "EventVolumeSize": {
      "Type": "Number",
      "Description": "EBS VolumeSize (GB)",
      "Default": "30"
    },
    "EventKeyPair": {
      "Description": "Select Existing Key for EventSore Servers",
      "Type": "AWS::EC2::KeyPair::KeyName"
    },
    "eventSG": {
      "Description": "Enter name for EventStore. (Note: SG name is preceded by \"CTCL-EventStore\")",
      "Type": "String"
    },
    "CTCLServiceSGopenPort": {
      "Description": "Enter the Port for the Broadcast Service",
      "Type": "String",
      "Default": "0000"
    },
    "CTCLSGDestinationAddressWithOpenService": {
      "Description": "Enter the IP to be whitelisted for Broadcast Service",
      "Type": "String",
      "Default": "0.0.0.0/0",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    }
  },
  "Mappings": {
    "RegionMap": {
      "us-east-1": {
        "AMI": "ami-a4827dc9"
      },
      "us-west-1": {
        "AMI": "ami-11790371"
      },
      "us-west-2": {
        "AMI": "ami-f303fb93"
      },
      "ap-south-1": {
        "AMI": "ami-e2b6dc8d"
      },
      "ap-northeast-2": {
        "AMI": "ami-69e92207"
      },
      "ap-southeast-1": {
        "AMI": "ami-a2c111c1"
      },
      "ap-southeast-2": {
        "AMI": "ami-d9d7f9ba"
      },
      "ap-northeast-1": {
        "AMI": "ami-6154bb00"
      },
      "eu-central-1": {
        "AMI": "ami-7df01e12"
      },
      "eu-west-1": {
        "AMI": "ami-c39604b0"
      },
      "sa-east-1": {
        "AMI": "ami-106ee57c"
      }
    }
  },
  "Metadata": {
    "AWS::CloudFormation::Interface": {
      "ParameterGroups": [
        {
          "Label": {
            "default": "CTCL Subnet Details"
          },
          "Parameters": [
            "CTCLPrivateSubnet1",
            "CTCLPrivateSubnetAZ1",
            "CTCLprivatesubnet1aCIDR",
            "CTCLPrivateSubnet2",
            "CTCLPrivateSubnetAZ2",
            "CTCLPrivateSubnet1bCIDR"
          ]
        },
        {
          "Label": {
            "default": "EventStore Subnet Details"
          },
          "Parameters": [
            "EventStorePrivateAZ2",
            "EventStorePrivateAZ1",
            "privatesubnet1aCIDR",
            "PrivateSubnet1bCIDR",
            "CTCLeventSubnetName1",
            "CTCLeventSubnetName2"
          ]
        },
        {
          "Label": {
            "default": "EventStore ELB Public Subnet Details"
          },
          "Parameters": [
            "EventStorePublicAZ1",
            "EventStorePublicAZ2",
            "PublicSubnetEventStoreCIDR1",
            "PublicSubnetEventStoreCIDR2"
          ]
        },
        {
          "Label": {
            "default": "Equity CTCL Server Details"
          },
          "Parameters": [
            "CTCLec2Tag",
            "CTCLelbTag",
            "InstanceType",
            "CTCLvolumetermination",
            "CTCLVolumeSize",
            "CTCLEC2termination",
            "CTCLKeyPair"
          ]
        },
        {
          "Label": {
            "default": "Security Group Details"
          },
          "Parameters": [
            "CTCLSGname",
            "CTCLSGDestination",
            "CTCLServiceSGopenPort",
            "CTCLSGDestinationAddressWithOpenService",
            "eventSG"
          ]
        },
        {
          "Label": {
            "default": "EventStore Server Details"
          },
          "Parameters": [
            "CTCLEventstoreEC2Tag",
            "InstanceTypeEvent",
            "CTCLeventstoreVolumeTermination",
            "EventVolumeSize",
            "EventKeyPair",
            "CTCLserverTermination"
          ]
        },
        {
          "Label": {
            "default": "Load Balancer Details"
          },
          "Parameters": [
            "HealthCheckPort"
          ]
        }
      ],
      "ParameterLabels": {
        "CTCLPrivateSubnet1": {
          "default": "Subnet Name A "
        },
        "CTCLPrivateSubnet2": {
          "default": "Subnet Name B "
        },
        "CTCLeventSubnetName1": {
          "default": "Subnet Name "
        },
        "CTCLeventSubnetName2": {
          "default": "Subnet Name "
        },
        "privatesubnet1aCIDR": {
          "default": "Subnet Range A"
        },
        "PrivateSubnet1bCIDR": {
          "default": "Subnet Range B"
        },
        "CTCLprivatesubnet1aCIDR": {
          "default": "Subnet Range "
        },
        "CTCLPrivateSubnet1bCIDR": {
          "default": "Subnet Range "
        },
        "PublicSubnetEventStoreCIDR1": {
          "default": "Subnet Range "
        },
        "PublicSubnetEventStoreCIDR2": {
          "default": "Subnet Range "
        },
        "CTCLPrivateSubnetAZ1": {
          "default": "Select Availability Zone AZ1"
        },
        "CTCLPrivateSubnetAZ2": {
          "default": "Select Availability Zone AZ2"
        },
        "EventStorePrivateAZ1": {
          "default": "Select Availability Zone AZ1"
        },
        "EventStorePrivateAZ2": {
          "default": "Select Availability Zone AZ1"
        },
        "EventStorePublicAZ1": {
          "default": "Select Availability Zone AZ1"
        },
        "EventStorePublicAZ2": {
          "default": "Select Availability Zone AZ1"
        },
        "CTCLvolumetermination": {
          "default": "Volume Termination "
        },
        "CTCLEC2termination": {
          "default": "EC2 Termination Protection "
        },
        "CTCLeventstoreVolumeTermination": {
          "default": "Volume Termination "
        },
        "CTCLserverTermination": {
          "default": "EC2 Termination Protection "
        },
        "CTCLec2Tag": {
          "default": "CTCL Servers Tag "
        },
        "CTCLelbTag": {
          "default": "Load Balancer Tag "
        },
        "InstanceType": {
          "default": "Instance Type "
        },
        "CTCLVolumeSize": {
          "default": "Volume Size "
        },
        "CTCLKeyPair": {
          "default": "Key Pair "
        },
        "HealthCheckPort": {
          "default": "Health Check Port "
        },
        "CTCLSGDestination": {
          "default": "Source IP for CTCL Security Group"
        },
        "CTCLSGname": {
          "default": "Security Group Name"
        },
        "CTCLEventstoreEC2Tag": {
          "default": "Servers Environment/Tag"
        },
        "InstanceTypeEvent": {
          "default": "Instance Type "
        },
        "EventVolumeSize": {
          "default": "Volume Size "
        },
        "EventKeyPair": {
          "default": "Key Pair "
        },
        "eventSGdestination": {
          "default": "Source IP for EventStore Security Group "
        },
        "eventSG": {
          "default": "Security Group Name "
        },
        "CTCLServiceSGopenPort": {
          "default": "Broadcast Port "
        },
        "CTCLSGDestinationAddressWithOpenService": {
          "default": "Source IP for Broadcast Service"
        }
      }
    }
  },
  "Resources": {
    "LoadBalancer": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Subnets": [
          {
            "Ref": "EventStorepublicSubnet1"
          },
          {
            "Ref": "EventStorepublicSubnet2"
          }
        ],
        "Instances": [
          {
            "Ref": "CTCLServer1"
          }
        ],
        "Listeners": [
          {
            "LoadBalancerPort": "80",
            "InstancePort": "80",
            "Protocol": "TCP"
          }
        ],
        "SecurityGroups": [
          {
            "Ref": "CTCLSG"
          }
        ],
        "HealthCheck": {
          "Target": {
            "Fn::Join": [
              ":",
              [
                "TCP",
                {
                  "Ref": "HealthCheckPort"
                }
              ]
            ]
          },
          "HealthyThreshold": "3",
          "UnhealthyThreshold": "5",
          "Interval": "30",
          "Timeout": "5"
        },
        "LoadBalancerName": "CTCL",
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCL",
                  {
                    "Ref": "CTCLelbTag"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "CTCLprivatesubnet01": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "CTCLprivatesubnet1aCIDR"
        },
        "AvailabilityZone": {
          "Ref": "CTCLPrivateSubnetAZ1"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCLPrivate",
                  {
                    "Ref": "CTCLPrivateSubnet1"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "CTCLprivatesubnet02": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "CTCLPrivateSubnet1bCIDR"
        },
        "AvailabilityZone": {
          "Ref": "CTCLPrivateSubnetAZ2"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCLPrivate",
                  {
                    "Ref": "CTCLPrivateSubnet2"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "EventStoreprivateSubnet1": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "privatesubnet1aCIDR"
        },
        "AvailabilityZone": {
          "Ref": "EventStorePrivateAZ1"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCLeventstore",
                  {
                    "Ref": "CTCLeventSubnetName1"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "EventStoreprivateSubnet2": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "PrivateSubnet1bCIDR"
        },
        "AvailabilityZone": {
          "Ref": "EventStorePrivateAZ2"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCLeventstore",
                  {
                    "Ref": "CTCLeventSubnetName2"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "EventStorepublicSubnet1": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "PublicSubnetEventStoreCIDR1"
        },
        "AvailabilityZone": {
          "Ref": "EventStorePublicAZ1"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": "Eventstorepublic1"
          }
        ]
      }
    },
    "EventStorepublicSubnet2": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "CidrBlock": {
          "Ref": "PublicSubnetEventStoreCIDR2"
        },
        "AvailabilityZone": {
          "Ref": "EventStorePublicAZ2"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": "Eventstorepublic2"
          }
        ]
      }
    },
    "subnetassociation01": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "CTCLprivatesubnet01"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PrivateRouteTableID"
        }
      }
    },
    "subentassociation02": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "CTCLprivatesubnet02"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PrivateRouteTableID"
        }
      }
    },
    "subnetassociation03": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "EventStoreprivateSubnet1"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PrivateRouteTableID"
        }
      }
    },
    "subnetassociation04": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "EventStoreprivateSubnet2"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PrivateRouteTableID"
        }
      }
    },
    "subnetassociation05": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "EventStorepublicSubnet1"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PublicRouteTableID"
        }
      }
    },
    "subnetassociation06": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": {
          "Ref": "EventStorepublicSubnet1"
        },
        "RouteTableId": {
          "Fn::ImportValue": "Network-PublicRouteTableID"
        }
      }
    },
    "CTCLServer1": {
      "Type": "AWS::EC2::Instance",
      "Properties": {
        "IamInstanceProfile": {
          "Fn::ImportValue": "Role-InstanceProfile"
        },
        "InstanceType": {
          "Ref": "InstanceType"
        },
        "DisableApiTermination": {
          "Ref": "CTCLEC2termination"
        },
        "SubnetId": {
          "Ref": "CTCLprivatesubnet01"
        },
        "ImageId": {
          "Fn::FindInMap": [
            "RegionMap",
            {
              "Ref": "AWS::Region"
            },
            "AMI"
          ]
        },
        "SecurityGroupIds": [
          {
            "Ref": "CTCLSG"
          }
        ],
        "UserData": {
          "Fn::Base64": {
            "Fn::Join": [
              "",
              [
                "#!/bin/bash\n",
                "export TZ=Asia/Kolkata\n",
                "sudo yum update -y\n",
                "sudo yum install java-1.8.0\n",
                "sudo yum remove java-1.7*\n",
                "sudo yum remove java-1.7.0-openjdk\n",
                "sudo yum install ruby -y\n",
                "sudo yum install wget -y \n",
                "cd /home/ec2-user\n",
                "wget https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install\n",
                "chmod +x ./install\n",
                "sudo ./install auto\n",
                "sudo service codedeploy-agent start"
              ]
            ]
          }
        },
        "BlockDeviceMappings": [
          {
            "DeviceName": "/dev/xvda",
            "Ebs": {
              "VolumeSize": {
                "Ref": "CTCLVolumeSize"
              },
              "DeleteOnTermination": {
                "Ref": "CTCLvolumetermination"
              },
              "VolumeType": "gp2"
            }
          }
        ],
        "KeyName": {
          "Ref": "CTCLKeyPair"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCL",
                  {
                    "Ref": "CTCLec2Tag"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "EventStoreServer": {
      "Type": "AWS::EC2::Instance",
      "Properties": {
        "IamInstanceProfile": {
          "Fn::ImportValue": "Role-InstanceProfile"
        },
        "InstanceType": {
          "Ref": "InstanceTypeEvent"
        },
        "DisableApiTermination": {
          "Ref": "CTCLserverTermination"
        },
        "SubnetId": {
          "Ref": "EventStoreprivateSubnet1"
        },
        "ImageId": {
          "Fn::FindInMap": [
            "RegionMap",
            {
              "Ref": "AWS::Region"
            },
            "AMI"
          ]
        },
        "SecurityGroupIds": [
          {
            "Ref": "CTCLEventStoreSG"
          }
        ],
        "UserData": {
          "Fn::Base64": {
            "Fn::Join": [
              "",
              [
                "#!/bin/bash\n",
                "export TZ=Asia/Kolkata\n",
                "yum update -y\n",
                "cd /opt/\n",
                "wget https://eventstore.org/downloads/EventStore-OSS-Ubuntu-14.04-v4.0.3.tar.gz\n",
                "tar -xzf EventStore-OSS-Ubuntu-14.04-v4.0.3.tar.gz\n",
                "mv EventStore-OSS-Ubuntu-14.04-v4.0.3/* /home/ec2-user/\n",
                "cd EventStore-OSS-Ubuntu-14.04-v4.0.3"
              ]
            ]
          }
        },
        "BlockDeviceMappings": [
          {
            "DeviceName": "/dev/xvda",
            "Ebs": {
              "VolumeSize": {
                "Ref": "EventVolumeSize"
              },
              "DeleteOnTermination": {
                "Ref": "CTCLeventstoreVolumeTermination"
              },
              "VolumeType": "gp2"
            }
          }
        ],
        "KeyName": {
          "Ref": "EventKeyPair"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCL-EventStore",
                  {
                    "Ref": "CTCLEventstoreEC2Tag"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "CTCLSG": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupName": "CTCLSecGroup",
        "GroupDescription": "SG open for 46026,51525,1115",
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "SecurityGroupIngress": [
          {
            "IpProtocol": "tcp",
            "FromPort": {
              "Ref": "CTCLServiceSGopenPort"
            },
            "ToPort": {
              "Ref": "CTCLServiceSGopenPort"
            },
            "CidrIp": {
              "Ref": "CTCLSGDestinationAddressWithOpenService"
            },
            "Description": "For Broadcast Service"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "22",
            "ToPort": "22",
            "CidrIp": {
              "Fn::ImportValue": "Network-BastionEIP"
            },
            "Description": "SSH allowing to Bastion Host"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "2552",
            "ToPort": "2552",
            "CidrIp": {
              "Fn::ImportValue": "Network-VPCRange"
            },
            "Description": "Allowing Service"
          }
        ],
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCL",
                  {
                    "Ref": "CTCLSGname"
                  }
                ]
              ]
            }
          }
        ]
      }
    },
    "CTCLEventStoreSG": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupName": "CTCLEventSecGroup",
        "GroupDescription": "SG for CTCL EventStore",
        "VpcId": {
          "Fn::ImportValue": "Network-VPCID"
        },
        "SecurityGroupIngress": [
          {
            "IpProtocol": "tcp",
            "FromPort": "5114",
            "ToPort": "5114",
            "SourceSecurityGroupId": {
              "Ref": "CTCLSG"
            },
            "Description": "Allowing CTCL serivce"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "6114",
            "ToPort": "6114",
            "SourceSecurityGroupId": {
              "Ref": "CTCLSG"
            },
            "Description": "Allowing CTCL serivce"
          },
          {
            "IpProtocol": "tcp",
            "FromPort": "22",
            "ToPort": "22",
            "CidrIp": {
              "Fn::ImportValue": "Network-BastionEIP"
            },
            "Description": "SSH allowing to Bastion Host"
          }
        ],
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                "-",
                [
                  "CTCL-EventStore",
                  {
                    "Ref": "eventSG"
                  }
                ]
              ]
            }
          }
        ]
      }
    }
  },
  "Outputs": {
    "CTCLSG": {
      "Description": "CTCL Service Security Group",
      "Value": {
        "Ref": "CTCLSG"
      },
      "Export": {
        "Name": "CTCL-SecurityGroup"
      }
    }
  }
}
