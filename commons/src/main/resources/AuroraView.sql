CREATE VIEW lastPriceTimestamps
AS
SELECT ticker, MAX(price_time) AS maxPriceTime FROM prices pr GROUP BY pr.ticker

CREATE VIEW lastPrices
AS
SELECT pr.ticker, pr.price_time, pr.unadjusted_price
FROM prices AS pr
INNER JOIN lastPriceTimestamps AS lpto ON (lpto.ticker=pr.ticker AND lpto.maxPriceTime = pr.price_time)

CREATE VIEW activeHoldings AS
SELECT uat.adviseId, uat.wdId, eq.symbol, u.clientCode, uat.fundNameWithAdviserUsername, uat.username, uat.investingMode,
      SUM(CASE  orderside WHEN 'BUY'
             THEN  numberofsharesTransacted
          WHEN 'SELL' THEN -1 * numberofsharesTransacted
		END) AS quantity,
		SUM(CASE  orderside WHEN 'BUY'
			 THEN  amountBought
		  WHEN 'SELL' THEN -1 * amountSold
		  END) AS buyValue,
		  lp.unadjusted_price AS currentPrice
FROM userAdviseTransaction  uat
JOIN userAdvise ua
ON (uat.adviseId = ua.adviseId AND uat.fundNameWithAdviserUsername = ua.fundNameWithAdviserUsername AND ua.investingMode='REAL' AND uat.username = ua.username AND ua.adviseEndTime = 0 AND uat.lastUpdatedTime >= ua.adviseStartTime)
JOIN user u
 ON (uat.username = u.username AND u.investingMode = 'REAL' )
JOIN equityInstrument eq
 ON (uat.wdId = eq.wdId) 
 JOIN lastPrices AS lp
 ON (lp.ticker=uat.wdId)
GROUP BY uat.adviseId, uat.username