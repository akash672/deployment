package com.wt.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class TradingHolidaysConfig {
  private static List<String> tradingHolidays = new ArrayList<String>();
  
  public static List<String> tradingHolidaysList() {
    if (TradingHolidaysConfig.tradingHolidays.isEmpty()) {
      String key = "holidays";
      String holidays = "";
      try {
        holidays = property(key);
      } catch (IOException e) {
        e.printStackTrace();
      }
      tradingHolidays = new ArrayList<String>(Arrays.asList(holidays.split(",")));
    }

    return tradingHolidays;
  }
  
  private static String property(String key) throws IOException {
    String value = System.getProperty(key);
    if (value != null)
      return value;
    Properties properties = new Properties();
    properties.load(TradingHolidaysConfig.class.getClassLoader().getResourceAsStream("trading-holidays.config"));

    return properties.getProperty(key).toString();
  }

}