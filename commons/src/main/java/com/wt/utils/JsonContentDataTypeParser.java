package com.wt.utils;

public class JsonContentDataTypeParser {

  public static String getPackagename(String fullMetaData)
  {
    String packageName=null;
    int indexLast = fullMetaData.indexOf(',');
    int indexFirst = fullMetaData.indexOf('(')+1;
    packageName = fullMetaData.substring(indexFirst, indexLast);
 
    return packageName;
  }
  
  public static byte[] getClassdata(String fullData)
  {
    String packageName=null;
    int indexLast = fullData.lastIndexOf(',');
    int indexFirst = fullData.indexOf('{');
    packageName = fullData.substring(indexFirst, indexLast);

    return packageName.getBytes();
  }

}
