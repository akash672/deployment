package com.wt.utils;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;

import com.wt.domain.serializers.JsonSerializer;
import com.wt.domain.write.commands.FinishedProcessingCurrentStream;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.japi.function.Predicate;
import akka.persistence.query.EventEnvelope;
import akka.persistence.query.Offset;
import akka.stream.javadsl.Source;
import eventstore.Event;
import eventstore.EventNumber;
import eventstore.IndexedEvent;
import eventstore.SubscriptionObserver;
import lombok.extern.log4j.Log4j;

@Log4j
public class JournalProviderProduction extends JournalProvider {
  private static final String WEALTH_DESK_EVENT_PREFIX = "com.wt";

  private @Autowired EventStoreConnector connect;

  public JournalProviderProduction(ActorSystem system) {
    super(system);
  }

  @Override
  @SafeVarargs
  public final Source<String, NotUsed> allPersistenceIds(Predicate<String>... filters) {
    Set<String> uniqueIds = new HashSet<String>();
    Publisher<IndexedEvent> allStreamsPublisher = connect.getConnection().
        allStreamsPublisher(null, false, null, true);
    Source<String, NotUsed> allPersistenceIds = Source
        .fromPublisher(allStreamsPublisher)
            .filter(event -> event.event().streamId() instanceof eventstore.EventStream.Plain)
            .filter(uniqueEvent -> uniqueIds.add(uniqueEvent.event().streamId().streamId()))
            .map(streamIds -> streamIds.event().streamId().streamId());
               
   for (Predicate<String> filter : filters)
      allPersistenceIds = allPersistenceIds.filter(filter);
    return allPersistenceIds;
  }

  @Override
  public final Source<EventEnvelope, NotUsed> allEventsSourceForPersistenceId(String id, long from, long to) {
    EventNumber eventNumber = (from==0) ? null : new EventNumber.Exact((int)from);
    Publisher<Event> streamPublisher = connect.getConnection().streamPublisher(id, eventNumber, false, null, true);
    Source<EventEnvelope, NotUsed> allEvents = Source
                                              .fromPublisher(streamPublisher)
                                              .filter(wtevent -> wtevent.record().data().eventType().startsWith(WEALTH_DESK_EVENT_PREFIX))
                                              .map(event -> convertToEventEnvelope(event.data().data().toString(), event.streamId().streamId(), event.data().metadata().toString(), event.number().value()));
    return allEvents;
  }
  
  public void getSubscribedEvents(String typeOfEvent, long from, ActorRef destinationActor) {
    final Closeable closeable = connect.getConnection().subscribeToStreamFrom(typeOfEvent,
        new SubscriptionObserver<Event>() {
          @Override
          public void onLiveProcessingStart(Closeable subscription) {
            try {
              destinationActor.tell(new FinishedProcessingCurrentStream(), ActorRef.noSender());
              subscription.close();
            } catch (IOException e) {
              log.error("Error in closing subscription " + e);
              
            }
          }

          @Override
          public void onEvent(Event event, Closeable subscription) {

            try {
              EventEnvelope eventEnvelope = convertToEventEnvelope(event.data().data().toString(),
                  event.data().metadata().toString(), event.data().metadata().toString(),
                  event.record().record().number().value());
             destinationActor.tell(eventEnvelope, ActorRef.noSender());

            } catch (ClassNotFoundException e1) {
              e1.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            log.error("Error in reading event stream for eventType " + typeOfEvent +" . Exception encountered is " + e);
          }

          @Override
          public void onClose() {
           log.info("Closing event stream for eventType " + typeOfEvent );
          }
        }, from == 0 ? null : from-1, true, null);
  }

  private EventEnvelope convertToEventEnvelope(String data, String streamId, String metadata, long eventNumber) throws ClassNotFoundException {
    JsonSerializer jsonSerializer =new JsonSerializer();
    Object deserializedEvent = jsonSerializer.fromBinary(JsonContentDataTypeParser.getClassdata((data)),
        Class.forName(JsonContentDataTypeParser.getPackagename(metadata)));
    
     EventEnvelope eventEnvelope = new EventEnvelope(Offset.sequence(eventNumber), streamId, eventNumber, deserializedEvent);
     return eventEnvelope;
  }
  
}
