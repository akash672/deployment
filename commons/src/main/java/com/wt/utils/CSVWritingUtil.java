package com.wt.utils;


import static com.wt.utils.CommonConstants.DEFAULT_CHAR_CSV_SEPARATOR;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class CSVWritingUtil {

  public static void writeLine(Writer w, List<String> values) throws IOException {
      writeLine(w, values, DEFAULT_CHAR_CSV_SEPARATOR, ' ');
  }

  public static void writeLine(Writer w, List<String> values, char separators) throws IOException {
      writeLine(w, values, separators, ' ');
  }

  private static String followCVSformat(String value) {

      String result = value;
      if (result.contains("\"")) {
          result = result.replace("\"", "\"\"");
      }
      return result;

  }

  public static void writeLine(Writer w, List<String> values, char separators, char customQuote) throws IOException {

      boolean first = true;

      if (separators == ' ') {
          separators = DEFAULT_CHAR_CSV_SEPARATOR;
      }

      StringBuilder sb = new StringBuilder();
      for (String value : values) {
          if (!first) {
              sb.append(separators);
          }
          if (customQuote == ' ') {
              sb.append(followCVSformat(value));
          } else {
              sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
          }

          first = false;
      }
      sb.append("\n");
      w.append(sb.toString());


  }
}