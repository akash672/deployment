 package com.wt.utils;

public class CommonConstants {
   public static final String CompositeKeySeparator = "__";
   public static final String HYPHEN = "-";
   public static final String COLON = ":";
   public static final String DOT = ".";
   public static final String CommaSeparated = ",";
   public static final char DEFAULT_CHAR_CSV_SEPARATOR = ',';
   public static final String FolderOrDateSeparator = "/";
   public static final String PipeSeparated = "\\|";
   public static final String EntryOrderIdPrefix = "EntryWdOrdId";
   public static final String ExitOrderIdPrefix = "ExitWdOrdId";
   public static final String CorporateEventIdPrefix = "CorporateEventId";
   public static final String PHONENUMBERPREFIX = "+";
}
