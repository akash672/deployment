package com.wt.utils.aws;

import static org.apache.commons.io.FileUtils.readFileToString;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;
import com.wt.config.utils.WDProperties;
import com.wt.utils.email.factory.BaseEmailTemplate;
import com.wt.utils.email.factory.EmailFactory;
import com.wt.utils.email.factory.EmailTemplateFactory;

import lombok.extern.log4j.Log4j;

@Service("SESEmailUtil")
@Log4j
public class SESEmailUtil {
  private AmazonSimpleEmailService sesClient;
  private WDProperties properties;
  @Value("${amazon.aws.ses.accesskey}")
  private String SesAWSAccessKeyId;
  @Value("${amazon.aws.ses.secretkey}")
  private String SesAWSSecretKey;

  @Autowired
  public SESEmailUtil(@Qualifier("sesClient") AmazonSimpleEmailService sesClient, WDProperties properties) {
    this.sesClient = sesClient;
    this.properties = properties;
  }

  public SendEmailResult sendEmail(BaseEmailTemplate template) {
    try {
      SendEmailResult sendEmail = sesClient.sendEmail(getEmailRequest(properties.OUTBOUND_TRANSACTIONAL_EMAIL(),
          template.sendTo(), template.getSubjectLine(), template.getPlainTextFormat(), template.getHTMLTextformat()));
      log.info("Email sent to " + template.sendTo());
      return sendEmail;
    } catch (Exception ex) {
      log.error("The email was not sent to " + template.sendTo(), ex);
      return null;
    }
  }

  public SendEmailResult sendEmail(String from, String to, String mailSubject, String mailBody) {
    try {
      SendEmailResult sendEmail = sesClient.sendEmail(getEmailRequest(from, to, mailSubject, mailBody));
      log.info("Email sent to " + to);
      return sendEmail;
    } catch (Exception ex) {
      log.error("The email was not sent to " + to, ex);
      return null;
    }
  }

  public SendRawEmailResult sendRawEmail(String from, String to, String cc, String mailSubject, String mailBody,
      File attachmentFile) {
    try {
      Session session = Session.getInstance(emailSpecificProperties());
      MimeMessage mimeMessage = prepareMimeMessage(from, to, cc, mailSubject, session);
      BodyPart body = prepareBody(mailBody);
      BodyPart attachment = prepareAttachment(attachmentFile);
      MimeMultipart multiPartMessage = constructMimeMessage(body, attachment);
      mimeMessage.setContent(multiPartMessage);
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      mimeMessage.writeTo(outputStream);
      RawMessage rawMessage = prepareRawMessage(outputStream);
      SendRawEmailRequest sendRawEmailRequest = new SendRawEmailRequest(rawMessage);
      SendRawEmailResult sendEmail = sesClient.sendRawEmail(sendRawEmailRequest);
      log.debug("Email sent to " + to);
      return sendEmail;
    } catch (Exception ex) {
      log.error("The email was not sent to " + to, ex);
      return new SendRawEmailResult();
    }
  }

  private Properties emailSpecificProperties() {
    Properties emailSpecificProperties = new Properties();
    emailSpecificProperties.setProperty("mail.transport.protocol", "aws");
    emailSpecificProperties.setProperty("mail.aws.user", SesAWSAccessKeyId);
    emailSpecificProperties.setProperty("mail.aws.password", SesAWSSecretKey);
    return emailSpecificProperties;
  }

  private RawMessage prepareRawMessage(ByteArrayOutputStream outputStream) {
    RawMessage rawMessage = new RawMessage();
    rawMessage.setData(ByteBuffer.wrap(outputStream.toString().getBytes()));
    return rawMessage;
  }

  private MimeMultipart constructMimeMessage(BodyPart body, BodyPart attachment) throws MessagingException {
    MimeMultipart multiPartMessage = new MimeMultipart();
    multiPartMessage.addBodyPart(body);
    multiPartMessage.addBodyPart(attachment);
    return multiPartMessage;
  }

  private BodyPart prepareAttachment(File attachmentFile) throws MessagingException, IOException {
    BodyPart attachment = new MimeBodyPart();
    attachment.setDataHandler(new DataHandler(new FileDataSource(attachmentFile)));
    attachment.setText(readFileToString(attachmentFile));
    attachment.setFileName(attachmentFile.getName());
    return attachment;
  }

  private BodyPart prepareBody(String mailBody) throws MessagingException {
    BodyPart body = new MimeBodyPart();
    body.setContent(mailBody, "text/html");
    return body;
  }

  private MimeMessage prepareMimeMessage(String from, String to, String cc, String mailSubject, Session session)
      throws MessagingException, AddressException {
    MimeMessage mimeMessage = new MimeMessage(session);
    mimeMessage.setFrom(new InternetAddress(from));
    mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(to));
    mimeMessage.addRecipients(RecipientType.CC, InternetAddress.parse(cc));
    mimeMessage.setSubject(mailSubject);
    return mimeMessage;
  }

  private SendEmailRequest getEmailRequest(String from, String to, String mailSubject, String mailBody) {
    String[] addresses = getListOfEmails(to);
    Destination destination = new Destination().withToAddresses(addresses);
    Content subject = new Content().withData(mailSubject);
    Content textBody = new Content().withData(mailBody);
    Body body = new Body().withText(textBody);
    Body htmlBody = new Body().withHtml(textBody);
    Message message = new Message().withSubject(subject).withBody(body).withBody(htmlBody);

    SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination)
        .withMessage(message);
    return request;
  }

  private SendEmailRequest getEmailRequest(String from, String to, String mailSubject, String plainTextBody,
      String htmlFormattedBody) {
    String[] addresses = getListOfEmails(to);
    Destination destination = new Destination().withToAddresses(addresses);

    Content subject = new Content().withData(mailSubject);
    Content textBody = new Content().withData(plainTextBody);
    Content htmlTextBody = new Content().withData(htmlFormattedBody);
    Body body = new Body().withText(textBody).withHtml(htmlTextBody);
    Message message = new Message().withSubject(subject).withBody(body);

    SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination)
        .withMessage(message);
    return request;
  }

  private String[] getListOfEmails(String to) {
    String[] emails = to.split("(;)|(,)");
    return emails;
  }

  public SendEmailResult prepareAndSendEmail(Class<?> emailerName, Object... args) {
    EmailFactory factory = EmailTemplateFactory.factory(properties.environment() != null ? properties.environment() : "");
    BaseEmailTemplate baseEmailTemplate = factory.getEmail(emailerName.getSimpleName(), args);
    if (baseEmailTemplate != null)
      return sendEmail(baseEmailTemplate);
    return new SendEmailResult().withMessageId("");
  }

}