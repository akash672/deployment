package com.wt.utils.sms.templates.wealthdesk;

import com.wt.utils.sms.factory.BaseSMSTemplate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AlertForUserApprovalSMS implements BaseSMSTemplate {

  private String fundName;

  @Override
  public String getPlainTextFormat() {
    String message = "Your Order for " + getFundName()
        + " basket is generated. Kindly approve from the WealthDesk App/Website.";

    return message;
  }

  private String getFundName() {
    if (this.fundName.length() > 15)
      return this.fundName.substring(0, 14);
    else
      return this.fundName;
  }
}
