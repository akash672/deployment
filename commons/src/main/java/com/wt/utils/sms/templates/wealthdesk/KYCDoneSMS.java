package com.wt.utils.sms.templates.wealthdesk;

import com.wt.utils.sms.factory.BaseSMSTemplate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class KYCDoneSMS implements BaseSMSTemplate {

  private String brokerCompany;
  private String clientCode;
  private String firstName;

  @Override
  public String getPlainTextFormat() {
    return "Hi " + getFirstName() + ", your KYC is updated with Client Code: " + getClientCode() + " @ "
        + getBrokerCompany();
  }

  private String getFirstName() {
    if (firstName.length() > 21)
      return this.firstName.substring(0, 20);
    else
      return this.firstName;
  }

}
