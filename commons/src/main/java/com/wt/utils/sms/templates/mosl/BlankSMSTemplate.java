package com.wt.utils.sms.templates.mosl;

import com.wt.utils.sms.factory.BaseSMSTemplate;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BlankSMSTemplate implements BaseSMSTemplate{
  
  @Override
  public String getPlainTextFormat() {
    return "";
  }

}
