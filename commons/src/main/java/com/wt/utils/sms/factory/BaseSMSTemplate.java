package com.wt.utils.sms.factory;

public interface BaseSMSTemplate {
    public String getPlainTextFormat();
}
