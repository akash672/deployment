package com.wt.utils.sms.factory;

import com.wt.utils.sms.templates.mosl.AlertForUserApprovalSMSForMOSL;
import com.wt.utils.sms.templates.mosl.BlankSMSTemplate;
import com.wt.utils.sms.templates.wealthdesk.AlertForUserApprovalSMS;
import com.wt.utils.sms.templates.wealthdesk.KYCDoneSMS;
import com.wt.utils.sms.templates.wealthdesk.OTPSMS;
import com.wt.utils.sms.templates.wealthdesk.UserAppliedForKYCSMS;
import com.wt.utils.sms.templates.wealthdesk.UserSignedOnSMS;

public class MOSLSMSFactory implements SMSFactory {

  @Override
  public BaseSMSTemplate getSMS(String contentClass, Object... args) {
    if (contentClass.equals(AlertForUserApprovalSMS.class.getSimpleName())) {
      return new AlertForUserApprovalSMSForMOSL((String) args[0]);
    } else if (contentClass.equals(KYCDoneSMS.class.getSimpleName())) {
      return new BlankSMSTemplate();
    } else if (contentClass.equals(UserSignedOnSMS.class.getSimpleName())) {
      return new BlankSMSTemplate();
    } else if (contentClass.equals(OTPSMS.class.getSimpleName())) {
      return new BlankSMSTemplate();
    } else if (contentClass.equals(UserAppliedForKYCSMS.class.getSimpleName())) {
      return new BlankSMSTemplate();
    }
    return null;
  }

}
