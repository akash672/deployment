package com.wt.utils.sms.templates.wealthdesk;

import com.wt.utils.sms.factory.BaseSMSTemplate;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OTPSMS implements BaseSMSTemplate{
  
  String otp;

  @Override
  public String getPlainTextFormat() {
    return otp + " is your One Time Password for WealthDesk";
  }
}
