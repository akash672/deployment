package com.wt.utils.sms.factory;

public interface SMSFactory {
  public BaseSMSTemplate getSMS(String contentClass, Object... args);
}
