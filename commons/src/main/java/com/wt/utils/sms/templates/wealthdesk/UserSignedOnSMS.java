package com.wt.utils.sms.templates.wealthdesk;

import com.wt.utils.sms.factory.BaseSMSTemplate;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class UserSignedOnSMS implements BaseSMSTemplate {

  @Override
  public String getPlainTextFormat() {
    return "Registration completed. Welcome to WealthDesk, you can now subscribe to baskets with virtual money.";
  }

}
