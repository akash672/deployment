package com.wt.utils.sms.templates.mosl;

import com.wt.utils.sms.factory.BaseSMSTemplate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AlertForUserApprovalSMSForMOSL implements BaseSMSTemplate {

  private String fundName;

  @Override
  public String getPlainTextFormat() {
    String message = "Your Order for " + getFundName()
        + " is generated. The portfolio details have been sent on your registered email.";
    return message;
  }

  private String getFundName() {
    if (this.fundName.length() > 15)
      return this.fundName.substring(0, 14);
    else
      return this.fundName;
  }

}
