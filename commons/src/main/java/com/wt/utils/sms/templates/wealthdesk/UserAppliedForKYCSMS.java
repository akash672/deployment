package com.wt.utils.sms.templates.wealthdesk;

import com.wt.utils.sms.factory.BaseSMSTemplate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserAppliedForKYCSMS implements BaseSMSTemplate {

  private String userName;
  private String email;
  private String phoneNumber;

  @Override
  public String getPlainTextFormat() {
    return "New user has requested for KYC. Username: " + getUserName() + ", Email: " + getEmail() + ", PhoneNumber: +"
        + getPhoneNumber();
  }

  private String getEmail() {
    if (this.email.isEmpty())
      return "NA";
    else
      return this.email;
  }
}