package com.wt.utils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.google.common.reflect.ClassPath.ClassInfo;
import com.wt.config.utils.ReflectionUtils;
import com.wt.config.utils.WDProperties;

import lombok.extern.log4j.Log4j;

@Log4j
@Scope("singleton")
@Service
public class PrefixTableNameAnnotationsWithEnvironment implements ApplicationListener<ContextRefreshedEvent> {

  private ReflectionUtils reflectionUtils;
  private Map<String, String> originalTableNames = new ConcurrentHashMap<>();
  private WDProperties properties;
  
  @Autowired
  public PrefixTableNameAnnotationsWithEnvironment(ReflectionUtils reflectionUtils, WDProperties properties) {
    this.reflectionUtils = reflectionUtils;
    this.properties = properties;
  }

  public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
    try {
      List<ClassInfo> classOfPackage = reflectionUtils.classOfPackage("com.wt.domain");
      for (ClassInfo classInfo : classOfPackage) {
        Class<?> clazz = Class.forName(classInfo.getName());
        DynamoDBTable tableNameAnnotation = clazz.getDeclaredAnnotation(DynamoDBTable.class);
        if (tableNameAnnotation == null)
          continue;

        changeAnnotation(clazz, tableNameAnnotation);

      }
    } catch (IOException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException | NoSuchFieldException e) {
      log.error(e);
    }

  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  private void changeAnnotation(Class clazz, final DynamoDBTable oldAnnotation)
      throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, NoSuchFieldException {
    Annotation newAnnotation = newAnnotation(clazz, oldAnnotation);

    Method method = Class.class.getDeclaredMethod("annotationData");
    method.setAccessible(true);
    Object annotationData = method.invoke(clazz);
    Field declaredAnnotations = annotationData.getClass().getDeclaredField("declaredAnnotations");
    declaredAnnotations.setAccessible(true);
    Map<Class<? extends Annotation>, Annotation> annotations = (Map<Class<? extends Annotation>, Annotation>) declaredAnnotations
        .get(annotationData);
    annotations.put(DynamoDBTable.class, newAnnotation);
  }

  @SuppressWarnings("rawtypes")
  private Annotation newAnnotation(Class clazz, final DynamoDBTable oldAnnotation) {
    Annotation newAnnotation = new DynamoDBTable() {

      @Override
      public String tableName() {
        String oldTableName = oldAnnotation.tableName();
          oldTableName = oldTableName.replaceFirst( properties.environment() + "_", "");

          String newName = properties.environment() + "_" + oldTableName;
          
          originalTableNames.putIfAbsent(clazz.getName(), newName);
          return originalTableNames.get(clazz.getName());
        
      }

      @Override
      public Class<? extends Annotation> annotationType() {
        return oldAnnotation.annotationType();
      }
    };
    return newAnnotation;
  }

}