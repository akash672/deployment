package com.wt.utils;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.japi.function.Predicate;
import akka.persistence.query.EventEnvelope;
import akka.persistence.query.javadsl.PersistenceIdsQuery;
import akka.persistence.query.javadsl.EventsByPersistenceIdQuery;
import akka.stream.javadsl.Source;

public class JournalProviderTest extends JournalProvider { 
  private ActorSystem system;

  public JournalProviderTest(ActorSystem system) {
    super(system);
    this.system = system;
  }

  @Override
  @SafeVarargs
  public final Source<String, NotUsed> allPersistenceIds(Predicate<String>... filters) {
    Source<String, NotUsed> allPersistenceIds = ((PersistenceIdsQuery) journal(system)).persistenceIds();

    for (Predicate<String> filter : filters)
      allPersistenceIds = allPersistenceIds.filter(filter);

    return allPersistenceIds;
  }

  @Override
  public final Source<EventEnvelope, NotUsed> allEventsSourceForPersistenceId(String id, long from, long to) {
    return ((EventsByPersistenceIdQuery) journal(system)).eventsByPersistenceId(id, from, to);
  }
  
  @Override
  public void getSubscribedEvents(String typeOfEvent, long from, ActorRef destinationActor) {
  }
}
