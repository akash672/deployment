package com.wt.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import com.google.common.collect.Maps;

import lombok.extern.log4j.Log4j;

@Log4j
public class DateUtils {

  private static final String DATE_FORMAT = "dd/MM/yyyy";
  private static final long MILLIS_TILL_END_OF_DAY = (long) (15.5 * 60 * 60 * 1000L);
  private static final TimeZone INDIA_TIME_ZONE = TimeZone.getTimeZone("Asia/Kolkata");
  private static final long MILLIS_TILL_BEGINNING_OF_DAY = (long) (9 * 60 * 60 * 1000L);
  private static final long MILLIS_TILL_EXECUTION_EOD = (long) (15.41 * 60 * 60 * 1000L);
  private static final long MILLIS_TILL_MF_EXECUTION_EOD = (long) (13.5 * 60 * 60 * 1000L);
  private static final long MILLIS_TILL_EXECUTION_BOD = (long) (9.25 * 60 * 60 * 1000L);
  private static final long MILLIS_TILL_TASKS_BOD = (long) (7 * 60 * 60 * 1000L);
  private static final long MILLIS_TO_EXTEND_TRADE_EXECUTION_WINDOW = (long) (35 * 60  * 1000L);
  private static final Map<String, String> monthConverter = Maps.newHashMap();
  static {
    monthConverter.put("Jan", "01");
    monthConverter.put("Feb", "02");
    monthConverter.put("Mar", "03");
    monthConverter.put("Apr", "04");
    monthConverter.put("May", "05");
    monthConverter.put("Jun", "06");
    monthConverter.put("Jul", "07");
    monthConverter.put("Aug", "08");
    monthConverter.put("Sep", "09");
    monthConverter.put("Oct", "10");
    monthConverter.put("Nov", "11");
    monthConverter.put("Dec", "12");
    monthConverter.put("JAN", "01");
    monthConverter.put("FEB", "02");
    monthConverter.put("MAR", "03");
    monthConverter.put("APR", "04");
    monthConverter.put("MAY", "05");
    monthConverter.put("JUN", "06");
    monthConverter.put("JUL", "07");
    monthConverter.put("AUG", "08");
    monthConverter.put("SEP", "09");
    monthConverter.put("OCT", "10");
    monthConverter.put("NOV", "11");
    monthConverter.put("DEC", "12");
  }

  public static long eodDateFromExcelDate(String dateInString, String format) throws ParseException {
    return eodDateFromExcelDate(dateInString, format, "/");
  }

  public static long bodDateFromExcelDate(String dateInString, String format) throws ParseException {
    return bodDateFromExcelDate(dateInString, format, "/");
  }

  public static long bodTaskDateFromExcelDate(String dateInString, String format) throws ParseException {
    return bodTaskDateFromExcelDate(dateInString, format, "/");
  }

  public static long executionEodDateFromExcelDate(String dateInString, String format) throws ParseException {
    return executionEodDateFromExcelDate(dateInString, format, "/");
  }

  public static long executionBodDateFromExcelDate(String dateInString, String format) throws ParseException {
    return executionBodDateFromExcelDate(dateInString, format, "/");
  }

  public static long eodDateFromExcelDate(String dateInString, String format, String splitter) throws ParseException {
    String[] parts = dateInString.split(splitter);
    String correctFormat = correctFormat(parts, "-");
    return eodUnixTimeFromString(correctFormat, "dd-MM-yyyy");
  }

  public static long bodDateFromExcelDate(String dateInString, String format, String splitter) throws ParseException {
    String[] parts = dateInString.split(splitter);
    String correctFormat = correctFormat(parts, splitter);
    return bodUnixTimeFromString(correctFormat, format);
  }

  public static long bodTaskDateFromExcelDate(String dateInString, String format, String splitter) throws ParseException {
    String[] parts = dateInString.split(splitter);
    String correctFormat = correctFormat(parts, splitter);
    return bodTaskUnixTimeFromString(correctFormat, format);
  }

  public static long executionEodDateFromExcelDate(String dateInString, String format, String splitter) throws ParseException {
    String[] parts = dateInString.split(splitter);
    String correctFormat = correctFormat(parts, "-");
    return executionEodUnixTimeFromString(correctFormat, "dd-MM-yyyy");
  }

  public static long executionBodDateFromExcelDate(String dateInString, String format, String splitter) throws ParseException {
    String[] parts = dateInString.split(splitter);
    String correctFormat = correctFormat(parts, splitter);
    return executionBodUnixTimeFromString(correctFormat, format);
  }

  public static long dateFromExcelDate(String dateInString, String format, String splitter) throws ParseException {
    String[] parts = splitDate(dateInString, splitter);
    String correctFormat = correctFormat(parts, splitter);
    return eodUnixTimeFromString(correctFormat, format);
  }

  public static String[] splitDate(String dateInString, String splitter) {
    return dateInString.split(splitter);
  }

  public static long eodUnixTimeFromString(String date, String format) throws ParseException {
    DateFormat indiaFormat = new SimpleDateFormat(format);
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);
    long time = indiaFormat.parse(date).getTime();

    return extractEodDateInfoFrom(time);
  }

  public static long bodUnixTimeFromString(String date, String format) throws ParseException {
    DateFormat indiaFormat = new SimpleDateFormat(format);
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);
    long time = indiaFormat.parse(date).getTime();
    return extractBodDateInfoFrom(time);
  }

  public static long broadcastUnixTimeFromString(String date, String format) throws ParseException {
    DateFormat indiaFormat = new SimpleDateFormat(format);
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);
    long time = indiaFormat.parse(date).getTime();
    return extractBroadcastDateInfoFrom(time);
  }

  public static long bodTaskUnixTimeFromString(String date, String format) throws ParseException {
    DateFormat indiaFormat = new SimpleDateFormat(format);
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);
    long time = indiaFormat.parse(date).getTime();

    return extractBodTaskDateInfoFrom(time);
  }

  public static long executionEodUnixTimeFromString(String date, String format) throws ParseException {
    DateFormat indiaFormat = new SimpleDateFormat(format);
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);
    long time = indiaFormat.parse(date).getTime();

    return extractExecutionEodDateInfoFrom(time);
  }

  public static long mfExecutionEodUnixTimeFromString(String date) throws ParseException {
    DateFormat indiaFormat = new SimpleDateFormat("dd/MM/yyyy");
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);
    long time = indiaFormat.parse(date).getTime();

    return extractMFExecutionEodDateInfoFrom(time);
  }
  
  public static long executionBodUnixTimeFromString(String date, String format) throws ParseException {
    DateFormat indiaFormat = new SimpleDateFormat(format);
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);
    long time = indiaFormat.parse(date).getTime();

    return extractExecutionBodDateInfoFrom(time);
  }

  private static long extractEodDateInfoFrom(long timeStamp) {
    return timeStamp + MILLIS_TILL_END_OF_DAY;
  }

  private static long extractBodDateInfoFrom(long timeStamp) {
    return timeStamp + MILLIS_TILL_BEGINNING_OF_DAY;
  }

  private static long extractBroadcastDateInfoFrom(long timeStamp) {
    return timeStamp + MILLIS_TILL_EXECUTION_BOD;
  }

  private static long extractBodTaskDateInfoFrom(long timeStamp) {
    return timeStamp + MILLIS_TILL_TASKS_BOD;
  }

  private static long extractExecutionEodDateInfoFrom(long timeStamp) {
    return timeStamp + MILLIS_TILL_EXECUTION_EOD;
  }

  private static long extractMFExecutionEodDateInfoFrom(long timeStamp) {
    return timeStamp + MILLIS_TILL_MF_EXECUTION_EOD;
  }
  
  private static long extractExecutionBodDateInfoFrom(long timeStamp) {
    return timeStamp + MILLIS_TILL_EXECUTION_BOD;
  }

  public static long bodDateFromExcelDate(String dateInString) throws ParseException {
    return bodDateFromExcelDate(dateInString, DATE_FORMAT);
  }

  public static long bodTaskDateFromExcelDate(String dateInString) throws ParseException {
    return bodTaskDateFromExcelDate(dateInString, DATE_FORMAT);
  }

  public static long eodDateFromExcelDate(String dateInString) throws ParseException {
    return eodDateFromExcelDate(dateInString, DATE_FORMAT);
  }

  public static long executionBodDateFromExcelDate(String dateInString) throws ParseException {
    return executionBodDateFromExcelDate(dateInString, DATE_FORMAT);
  }

  public static long executionEodDateFromExcelDate(String dateInString) throws ParseException {
    return executionEodDateFromExcelDate(dateInString, DATE_FORMAT);
  }

  public static boolean isTodayEOD(String date) throws ParseException {
    try {
      long todayEOD = todayEOD();
      return todayEOD == eodDateFromExcelDate(date);
    } catch (Exception e) {
      return false;
    }
  }

  public static boolean isTodayBOD(String date) throws ParseException {
    try {
      long todayBOD = todayTaskBOD();
      return todayBOD == bodTaskDateFromExcelDate(date);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return false;
    }
  }

  public static long todayEOD() throws ParseException {
    String prettyTime = eodDateToday();

    return eodDateFromExcelDate(prettyTime);
  }

  public static long todayExecutionEOD() throws ParseException {
    String prettyTime = todayPrettyTime();

    return executionEodDateFromExcelDate(prettyTime);
  }
  
  public static long todayTradeExecutionEOD() throws ParseException {

    return (todayExecutionEOD() + MILLIS_TO_EXTEND_TRADE_EXECUTION_WINDOW);
  }
  
  public static long todayTradeExecutionBOD() throws ParseException {

    return todayExecutionBOD();
  }

  public static long lastDayEOD(long timestamp) {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    calendar.setTimeInMillis(timestamp);
    calendar.add(Calendar.DATE, -1);
    long lastWorkingDate = requiredTradingDay(calendar, -1);
    calendar.setTimeInMillis(lastWorkingDate);
    calendar.set(Calendar.HOUR_OF_DAY, 15);
    calendar.set(Calendar.MINUTE, 30);
    calendar.set(Calendar.SECOND, 00);
    calendar.set(Calendar.MILLISECOND, 00);
    return calendar.getTimeInMillis();
  }

  public static long nextBusinessDay(long timestamp) {
    Calendar calendar = nextBusinessDate(timestamp);
    return calendar.getTimeInMillis();
  }

  public static Calendar nextBusinessDate(long timestamp) {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    calendar.setTimeInMillis(timestamp);
    calendar.add(Calendar.DATE, 1);
    long nextTradingDay = requiredTradingDay(calendar, 1);
    calendar.setTimeInMillis(nextTradingDay);
    calendar.set(Calendar.HOUR_OF_DAY, 9);
    calendar.set(Calendar.MINUTE, 30);
    calendar.set(Calendar.SECOND, 00);
    calendar.set(Calendar.MILLISECOND, 00);
    return calendar;
  }
  
  public static long requiredTradingDay(Calendar currentDate, int offset) {
    long currentTime = currentDate.getTimeInMillis();
    while(isTradingHoliday(prettyTime(currentTime, DATE_FORMAT)) || isWeekend(currentDate)) {
      currentDate.add(Calendar.DATE, offset);
      currentTime = currentDate.getTimeInMillis();
    }
    
    return currentTime;
    
  }
  
  public static long nextTradingWeekday(long timestamp) {
    if (isTradingHoliday(prettyDate(timestamp)) || isWeekend(unixToCalendarDate(timestamp)))
      timestamp = nextBusinessDay(timestamp);
    return timestamp;
  }

  public static boolean isTradingHoliday(String isItHoliday) {
    if (TradingHolidaysConfig.tradingHolidaysList().contains(isItHoliday))
      return true;
    return false;
  }

  public static long todayBOD() throws ParseException {
    String prettyTime = todayPrettyTime();

    return bodDateFromExcelDate(prettyTime);
  }

  public static long getBODFromDate(long milliseconds) throws ParseException {
    String prettyTime = prettyTime(milliseconds);

    return bodDateFromExcelDate(prettyTime);
  }
  
  public static long getEODFromDate(long milliseconds) throws ParseException {
    String prettyTime = prettyTime(milliseconds);

    return eodDateFromExcelDate(prettyTime);
  }
  
  public static long todayTaskBOD() throws ParseException {
    String prettyTime = todayPrettyTime();

    return bodTaskDateFromExcelDate(prettyTime);
  }

  public static long todayExecutionBOD() throws ParseException {
    String prettyTime = todayPrettyTime();

    return executionBodDateFromExcelDate(prettyTime);
  }

  public static String todayPrettyTime() {
    Calendar instance = Calendar.getInstance(INDIA_TIME_ZONE);
    return prettyTime(instance.getTimeInMillis(), DATE_FORMAT);
  }

  public static String year() {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    return String.valueOf(calendar.get(Calendar.YEAR));
  }
  
  public static String month() {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    return String.valueOf(calendar.get(Calendar.MONTH) + 1);
  }
  
  public static String date() {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    return String.valueOf(calendar.get(Calendar.DATE));
  }
  
  public static String eodDateToday() {
    if (System.getenv("WT_EOD_DATE") != null) {
      log.info("Today's EOD(dd/MM/yyyy) date received is" + System.getenv("WT_EOD_DATE"));
      return System.getenv("WT_EOD_DATE");
    } else {
      Calendar instance = Calendar.getInstance(INDIA_TIME_ZONE);
      return prettyTime(instance.getTimeInMillis(), DATE_FORMAT);
    }
  }

  private static String correctFormat(String[] parts, String splitter) {
    return correctDate(parts[0]) + splitter + correctMonth(parts[1]) + splitter + correctYear(parts[2]);
  }

  public static String correctDate(String part) {
    return correctTwoDigitDateFormat(part);
  }

  public static String correctMonth(String part) {
    return correctTwoDigitDateFormat(part);
  }

  public static String formatMonth(String part) throws ParseException {
    SimpleDateFormat monthParse = new SimpleDateFormat("MM");
    SimpleDateFormat monthDisplay = new SimpleDateFormat("MMM");
    return monthDisplay.format(monthParse.parse(part));
  }

  public static String correctYear(String dateInInCorrectFormat) {
    if (dateInInCorrectFormat.length() == 2)
      return "20" + dateInInCorrectFormat;

    return dateInInCorrectFormat;
  }

  private static String correctTwoDigitDateFormat(String incorrectFormaStr) {
    if (incorrectFormaStr.length() == 1)
      return "0" + incorrectFormaStr;

    if (incorrectFormaStr.length() == 3)
      return monthConverter.get(incorrectFormaStr);

    return incorrectFormaStr;
  }

  public static String prettyTime(long time) {
    return prettyTime(time, "dd/MM/yyyy hh:mm a");
  }

  public static String prettyTime(long time, String format) {
    Date date = new Date(time);
    SimpleDateFormat indiaFormat = new SimpleDateFormat(format);
    indiaFormat.setTimeZone(INDIA_TIME_ZONE);

    return indiaFormat.format(date);
  }

  public static String prettyDate(long time) {
    Date date = new Date(time);
    SimpleDateFormat indiaFormat = new SimpleDateFormat("dd/MM/yyyy");
    indiaFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
    return indiaFormat.format(date);
  }

  public static long minusMonths(long time, int months) {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    calendar.setTimeInMillis(time);
    calendar.add(Calendar.MONTH, -1 * months);

    return calendar.getTimeInMillis();
  }

  public static long currentTimeInMillis() {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    return calendar.getTimeInMillis();
  }

  public static int millisecondsToDays(long milliseconds) {
    return (int) (milliseconds / (1000 * 60 * 60 * 24));
  }

  public static boolean isWeekend(Calendar date) {
    return date.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || date.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
  }

  public static boolean isMarketOpen() throws ParseException {
    if (System.getenv("WT_MARKET_OPEN") != null) {
      if (System.getenv("WT_MARKET_OPEN").toString().equalsIgnoreCase("true"))
        return true;
      if (System.getenv("WT_MARKET_OPEN").toString().equalsIgnoreCase("false"))
        return false;
    }
    if (isWeekend(Calendar.getInstance(INDIA_TIME_ZONE)))
      return false;
    if(isTradingHoliday())
      return false;
    if (todayBOD() < DateUtils.currentTimeInMillis() && todayEOD() > DateUtils.currentTimeInMillis())
      return true;
    else
      return false;
  }

  public static boolean isExecutionWindowOpen() {
    try {
      if (System.getenv("WT_MARKET_OPEN") != null) {
        if (System.getenv("WT_MARKET_OPEN").toString().equalsIgnoreCase("true"))
          return true;
        if (System.getenv("WT_MARKET_OPEN").toString().equalsIgnoreCase("false"))
          return false;
      }
      if (isWeekend(Calendar.getInstance(INDIA_TIME_ZONE)))
        return false;
      if (isTradingHoliday())
        return false;
      if (todayExecutionBOD() < DateUtils.currentTimeInMillis()
          && todayExecutionEOD() > DateUtils.currentTimeInMillis())
        return true;
      else
        return false;
    } catch (ParseException e) {
      e.printStackTrace();
      return true;
    }
  }
  
  public static boolean isTradeExecutionWindowOpen() {
    //TODO : add here
    try {
      if (System.getenv("WT_MARKET_OPEN") != null) {
        if (System.getenv("WT_MARKET_OPEN").toString().equalsIgnoreCase("true"))
          return true;
        if (System.getenv("WT_MARKET_OPEN").toString().equalsIgnoreCase("false"))
          return false;
      }
      if (isWeekend(Calendar.getInstance(INDIA_TIME_ZONE)))
        return false;
      if (isTradingHoliday())
        return false;
      if (todayTradeExecutionBOD() < DateUtils.currentTimeInMillis()
          && todayTradeExecutionEOD() > DateUtils.currentTimeInMillis())
        return true;
      else
        return false;
    } catch (ParseException e) {
      e.printStackTrace();
      return true;
    }
  }
  
  public static boolean isBroadcastWindowOpen() throws ParseException {
    if (System.getenv("WT_BROADCAST_OPEN") != null) {
      if (System.getenv("WT_BROADCAST_OPEN").toString().equalsIgnoreCase("true"))
        return true;
      if (System.getenv("WT_BROADCAST_OPEN").toString().equalsIgnoreCase("false"))
        return false;
    }
    if (isWeekend(Calendar.getInstance(INDIA_TIME_ZONE)))
      return false;
    if(isTradingHoliday())
      return false;
    if (marketOpenTime() < DateUtils.currentTimeInMillis() && todayExecutionEOD() > DateUtils.currentTimeInMillis())
      return true;
    else
      return false;
  }
  
  public static long marketOpenTime() throws ParseException {
    String todayDate = prettyDate(currentTimeInMillis());
    return  executionBodUnixTimeFromString(todayDate, "dd/MM/yyyy");
  }
  
  public static long marketCloseTime() throws ParseException {
    String todayDate = prettyDate(currentTimeInMillis());
    return  eodUnixTimeFromString(todayDate, "dd/MM/yyyy");
  }

  public static boolean isTradingHoliday() {
    return TradingHolidaysConfig.tradingHolidaysList().contains(eodDateToday());
  }

  public static long minusTime(int duration, int noOfUnits) {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    calendar.add(duration, noOfUnits * -1);
    return calendar.getTimeInMillis();
  }
  
  public static Calendar unixToCalendarDate(long timeInMillis) {
    Calendar calendar = Calendar.getInstance(INDIA_TIME_ZONE);
    calendar.setTimeInMillis(timeInMillis);
    return calendar;
  }
  
}