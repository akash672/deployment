package com.wt.utils;

import static java.lang.Double.isInfinite;
import static java.lang.Double.isNaN;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DoublesUtil {
  private static double THRESHOLD = 0.001;

    public static double round(double value) {
      return round(value, 2);
    }
    
    public static double roundToFourDigits(double value) {
      return round(value, 4);
    }

    public static double round(double value, int roundUpto) {
      BigDecimal bd = new BigDecimal(String.valueOf(value));
      bd = bd.setScale(roundUpto, RoundingMode.HALF_EVEN);
      return bd.doubleValue();
    }

    public static double exchangeParseDouble(String value) {
      if(value.equals(null) || value.equals("") || value.equals(CommonConstants.HYPHEN)) 
        return 0.;

      return Double.parseDouble(value);
    }

    public static boolean isZero(double value) {
      return value >= -THRESHOLD && value <= THRESHOLD;
    }
    
    public static double validDouble(double number) {
      return (!isNaN(number) || isInfinite(number)) ? number : 0D;
    }
    
    public static boolean isANonZeroValidDouble(double number) {
      return (!isNaN(number) || isInfinite(number)) && !isZero(number);
    }
}