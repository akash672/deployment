package com.wt.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
  public static String getFile(String fileName, Class<?> clazz) throws IOException {
    StringBuilder result = new StringBuilder("");
    InputStream inputStream = clazz.getClassLoader().getResourceAsStream(fileName);
    Scanner scanner = new Scanner(inputStream);
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      result.append(line).append("\n");
    }
    scanner.close();
    return result.toString();
  }

  public static String replacePlaceholders(String html, Map<String, String> placeHolderMap) {
    for (Map.Entry<String, String> entry : placeHolderMap.entrySet()) {
      html = html.replaceAll(entry.getKey(), entry.getValue());
    }
    return html;
  }

  public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
      Pattern.CASE_INSENSITIVE);

  public static boolean isValidEmail(String emailStr) {
    Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
    return matcher.find();
  }
}
