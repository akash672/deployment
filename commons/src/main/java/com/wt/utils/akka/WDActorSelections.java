package com.wt.utils.akka;

import akka.actor.ActorSelection;

public interface WDActorSelections {
  ActorSelection advisersRootActor();

  ActorSelection universeRootActor();

  ActorSelection userRootActor();
  
  ActorSelection equityOrderRootActor();

  ActorSelection mforderRootActor();
  
  ActorSelection realtimeAdviserFundPerformanceRootActor();
  
  ActorSelection realtimeInvestorPerformanceRootActor();
  
  ActorSelection realtimeMarketPriceRootActor();

  ActorSelection activeInstrumentTracker();
  
  ActorSelection rdsFacadeRootActor();
}
