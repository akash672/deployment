package com.wt.utils.akka;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.stream.javadsl.Source;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CancellableStream<Out, Mat> {
  private final Source<Out, Mat> source;
  protected final ActorRef actor;
  
  public void cancel() {
    actor.tell(PoisonPill.getInstance(), ActorRef.noSender());
  }

}
