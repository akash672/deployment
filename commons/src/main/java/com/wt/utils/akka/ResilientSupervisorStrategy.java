package com.wt.utils.akka;

import static akka.actor.SupervisorStrategy.resume;
import static java.util.concurrent.TimeUnit.MINUTES;

import akka.actor.OneForOneStrategy;
import akka.actor.SupervisorStrategy;
import akka.actor.SupervisorStrategyConfigurator;
import scala.concurrent.duration.Duration;

public class ResilientSupervisorStrategy implements SupervisorStrategyConfigurator {

  @Override
  public SupervisorStrategy create() {
    return strategy;
  }

  private static SupervisorStrategy strategy = new OneForOneStrategy(10, Duration.create(1, MINUTES), 
      exception -> {
          return resume();
      });

}
