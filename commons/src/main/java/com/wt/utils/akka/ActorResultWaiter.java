package com.wt.utils.akka;

import static akka.pattern.Patterns.ask;
import static java.util.concurrent.TimeUnit.SECONDS;
import static scala.concurrent.Await.result;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import lombok.experimental.UtilityClass;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@UtilityClass
public class ActorResultWaiter {
	public static final Duration TIMEOUT = Duration.create(60D, SECONDS);

	public static Object askAndWaitForResult(final ActorRef actor, Object command) throws Exception {
		Future<Object> maybeResult = ask(actor, command, 60 * 1000);
		return result(maybeResult, TIMEOUT);
	}
	
	public static Object askAndWaitForResult(final ActorSelection actor, Object command) throws Exception {
      Future<Object> maybeResult = ask(actor, command, 60 * 1000);
      return result(maybeResult, TIMEOUT);
  }
}
