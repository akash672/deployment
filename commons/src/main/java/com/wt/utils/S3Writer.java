package com.wt.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public interface S3Writer {

  public default void createBucket(AmazonS3 amazonS3Client, String bucketName) {
    if (!amazonS3Client.doesBucketExistV2(bucketName)) {
      amazonS3Client.createBucket(bucketName);
    }
  }

  public default String s3PathFor(AmazonS3 amazonS3Client, String bucketName) {
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
    String year = String.valueOf(calendar.get(Calendar.YEAR));
    List<String> keys = getObjectslistFromFolder(amazonS3Client, bucketName, year);
    if (!checkWheterFolderIsPresent(keys, year))
      createFolder(amazonS3Client, bucketName, year);
    String month = String.valueOf((calendar.get(Calendar.MONTH) + 1));
    if (!checkWheterFolderIsPresent(keys, month))
      createFolder(amazonS3Client, bucketName, year + "/" + month);
    String day = String.valueOf(calendar.get(Calendar.DATE));
    createFolder(amazonS3Client, bucketName, year + "/" + month + "/" + day);
    return year + "/" + month + "/" + day + "/";
  }

  public default boolean checkWheterFolderIsPresent(List<String> keys, String searchKey) {
    for (String key : keys) {
      if (key.contains(searchKey))
        return true;
    }
    return false;
  }

  public default List<String> getObjectslistFromFolder(AmazonS3 amazonS3Client, String bucketName, String folderKey) {

    ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName)
        .withPrefix(folderKey + "/");

    List<String> keys = new ArrayList<>();

    ObjectListing objects = amazonS3Client.listObjects(listObjectsRequest);
    for (;;) {
      List<S3ObjectSummary> summaries = objects.getObjectSummaries();
      if (summaries.size() < 1) {
        break;
      }
      summaries.forEach(s -> keys.add(s.getKey()));
      objects = amazonS3Client.listNextBatchOfObjects(objects);
    }

    return keys;
  }

  public default void createFolder(AmazonS3 amazonS3Client, String bucketName, String keyName) {
    ObjectMetadata metadata = new ObjectMetadata();
    metadata.setContentLength(0);
    InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, keyName + "/", emptyContent, metadata);
    amazonS3Client.putObject(putObjectRequest);
  }

  public default void putDataInS3(AmazonS3 amazonS3Client, String bucketName, String fileName, String s3Path,
      String directoryPath) {
    try {
      File file = new File(directoryPath + "/" + fileName);
      s3Path += fileName;
      putObjectInS3Request(amazonS3Client, bucketName, s3Path, file);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public default void putObjectInS3Request(AmazonS3 amazonS3Client, String bucketName, String s3Path, File file) {
    try {
      amazonS3Client.putObject(bucketName, s3Path, file);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void putObjectInS3AsObjectMetadata(String bucketName, String s3Path, InputStream orderLines);
}