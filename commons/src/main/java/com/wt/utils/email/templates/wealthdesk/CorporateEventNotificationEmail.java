package com.wt.utils.email.templates.wealthdesk;

import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.Getter;

@Getter
public class CorporateEventNotificationEmail implements BaseEmailTemplate{
  private final String sendTo;
  private String symbol;
  private String environment;

  @Override
  public String getSubjectLine() {
    return "CORPORATE EVENT ALERT";
  }

  @Override
  public String getPlainTextFormat() {
    return "Scheme of Arrangement declared for " + symbol + " " + environment + " " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME");
  }

  @Override
  public String getHTMLTextformat() {
    return "<p>Scheme of Arrangement declared for " + symbol + environment + " " + " " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME") + "</p>";
  }

  @Override
  public String sendTo() {
    return getSendTo();
  }

  public CorporateEventNotificationEmail(String sendTo ,String symbol, String environment) {
    super();
    this.sendTo = sendTo;
    this.environment = environment;
    this.symbol = symbol;
  }
}
