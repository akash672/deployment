package com.wt.utils.email.factory;

import com.wt.domain.write.commands.RebalanceFundAlertData;
import com.wt.utils.email.templates.mosl.AlertForUserApprovalEmailForMOSL;
import com.wt.utils.email.templates.wealthdesk.AlertForUserApprovalEmail;

public class MOSLEmailer implements EmailFactory {

  @Override
  public BaseEmailTemplate getEmail(String emailer, Object... args) {
    if (emailer.equals(AlertForUserApprovalEmail.class.getSimpleName())) {
      return new AlertForUserApprovalEmailForMOSL((RebalanceFundAlertData) args[0]);
    } 
    
    return null;
  }

}
