package com.wt.utils.email.templates.mosl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.wt.config.utils.WDProperties;
import com.wt.domain.write.commands.RebalanceFundAlertData;
import com.wt.utils.CommonUtils;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;
import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Getter
@Log4j
public class AlertForUserApprovalEmailForMOSL implements BaseEmailTemplate {

  private String userEmail;
  private String userName;
  private String symbol;
  private double allocationValue;
  private double numberOfShares;
  private String fundName;
  private String adviseType;
  private double price;
  private double allocation;
  private WDProperties properties;
  private long datetime;

  public AlertForUserApprovalEmailForMOSL(RebalanceFundAlertData data) {
    this.userEmail = data.getEmail();
    this.userName = data.getUserName();
    this.symbol = data.getSymbol();
    this.allocationValue = data.getAllocationValue() > 0 ? data.getAllocationValue() : calculateValue(data);
    this.numberOfShares = data.getNumberOfShares() > 0 ? data.getNumberOfShares() : calculateQty(data);
    this.allocation = data.getAllocation();
    this.fundName = data.getFundName();
    this.adviseType = data.getAdviseType();
    this.properties = data.getProperties();
    this.datetime = data.getInvestmentDate();
    this.price = data.getPrice();
  }

  private int calculateQty(RebalanceFundAlertData data) {
    if (data.getAllocationValue() > 0 && data.getPrice() > 0) {
      return (int) (data.getAllocationValue() / data.getPrice());
    }
    return 0;
  }

  @Override
  public String getSubjectLine() {
    return "Action Required - Rebalancing update  in " + getFundName();
  }

  @Override
  public String getPlainTextFormat() {
    String message = "Dear " + userName + ",\n" + "\n" + "Your portfolio is re-balanced for " + fundName
        + ", Please approve the portfolio by clicking on\n"
        + "the link below or copy & paste the link in a new window of your browser.\n"
        + "Visit \"https://www.motilaloswal.com/online-trade-login.aspx\" to login and go to Advisory Products --> "
        + fundName + " --> Order Execution";
    return message;
  }

  @Override
  public String getHTMLTextformat() {
    try {
      String html;
      html = CommonUtils.getFile("emailers/mosl/rebalance-fund.template", getClass());
      return prepareHtml(html);
    } catch (Exception e) {
      log.error("HTML Email could not be send due to error: " + e.getMessage(), e);
      return getPlainTextFormat();
    }
  }

  private String prepareHtml(String html) throws IOException {
    Map<String, String> placeHolderMap = new HashMap<String, String>();
    placeHolderMap.put("_CLIENTNAME_", getUserName());
    placeHolderMap.put("_DETAILSDATA_", getHTMLDataTable());
    placeHolderMap.put("_FUNDNAME_", getFundName());
    placeHolderMap.put("_DATETIME_", DateUtils.prettyTime(datetime));
    return CommonUtils.replacePlaceholders(html, placeHolderMap);
  }

  private String getHTMLDataTable() {
    // @formatter:off
    return "<tr>" + "<td>" + getSymbol() + "</td>" + "<td>"
        + (getAdviseType().equalsIgnoreCase("issue") ? "Buy" : "Sell") + "</td>" + "<td>"
        + (getNumberOfShares() > 0 ? getNumberOfShares() : "NA") + "</td>" + "<td>"
        + (getPrice() > 0 ? getPrice() : "NA") + "</td>" + "<td>"
        + (getAllocationValue() > 0 ? getAllocationValue() : "NA") + "</td>" + "<td>"
        + (getAllocation() > 0 ? getAllocation() : "NA") + "</td>" + "</tr>";
    // @formatter:on
  }

  private double getAllocation() {
    return DoublesUtil.round(allocation, 2);
  }

  private double getNumberOfShares() {
    return DoublesUtil.round(numberOfShares, 0);
  }

  private double getAllocationValue() {
    return DoublesUtil.round(allocationValue, 2);
  }

  private double getPrice() {
    return DoublesUtil.round(price, 2);
  }

  private double calculateValue(RebalanceFundAlertData data) {
    if (data.getPrice() > 0 && data.getNumberOfShares() > 0)
      return DoublesUtil.round(data.getPrice() * data.getNumberOfShares(), 2);
    return 0;
  }

  @Override
  public String sendTo() {
    return getUserEmail();
  }

}
