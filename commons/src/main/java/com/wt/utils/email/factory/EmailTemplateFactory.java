package com.wt.utils.email.factory;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class EmailTemplateFactory {

  private static final WealthDeskEmailer WEALTH_DESK_EMAILER = new WealthDeskEmailer();
  private static final MOSLEmailer MOSL_EMAILER = new MOSLEmailer();

  public static EmailFactory factory(String environment) {
    if (environment.equals("mosl") || environment.equals("mosluat"))
      return MOSL_EMAILER;
    else
      return WEALTH_DESK_EMAILER;
  }
}
