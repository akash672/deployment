package com.wt.utils.email.templates.wealthdesk;

import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OTPEmail implements BaseEmailTemplate{
  
  String otp;
  String email;

  @Override
  public String getSubjectLine() {
    return "OTP Generated | WealthDesk";
  }

  @Override
  public String getPlainTextFormat() {
    return otp + " is your One Time Password for WealthDesk";
  }

  @Override
  public String getHTMLTextformat() {
    return getPlainTextFormat();
  }

  @Override
  public String sendTo() {
    return email;
  }
}
