package com.wt.utils.email.factory;

public interface EmailFactory {
  public BaseEmailTemplate getEmail(String emailerName, Object... args);
}
