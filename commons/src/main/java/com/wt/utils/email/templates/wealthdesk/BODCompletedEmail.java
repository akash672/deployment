package com.wt.utils.email.templates.wealthdesk;

import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.Getter;

@Getter
public class BODCompletedEmail implements BaseEmailTemplate {
  private final String sendTo;
  private Object environment;

  @Override
  public String getSubjectLine() {
    return "BOD Tasks Completed";
  }

  @Override
  public String getPlainTextFormat() {
    return "BOD Tasks have successfully completed on the Server " + environment + " " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME");
  }

  @Override
  public String getHTMLTextformat() {
    return "<p>BOD Tasks have successfully completed on the Server " + environment + " " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME") + "</p>";
  }

  @Override
  public String sendTo() {
    return getSendTo();
  }

  public BODCompletedEmail(String sendTo, String environment) {
    super();
    this.sendTo = sendTo;
    this.environment = environment;
  }

}
