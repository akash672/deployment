package com.wt.utils.email.templates.wealthdesk;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.wt.config.utils.WDProperties;
import com.wt.domain.write.commands.RebalanceFundAlertData;
import com.wt.utils.CommonUtils;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;
import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Getter
@Log4j
public class AlertForUserApprovalEmail implements BaseEmailTemplate {

  private String symbol;
  private double allocationValue;
  private String exchangeName;
  private String clientCode;
  private String userEmail;
  private double numberOfShares;
  private String fundName;
  private String adviseType;
  private String adviserName;
  private String brokerName;
  private WDProperties properties;
  private long datetime;
  private String userName;
  private double price;
  private double allocation;

  @Override
  public String getSubjectLine() {
    return "Action Required - Rebalancing update  in " + getFundName();
  }

  @Override
  public String getPlainTextFormat() {
    String message = "Hi " + getUserName() + ", A change needs to be approved in your subscribed fund: " + getFundName()
        + "." + "\n";
    if (getAdviseType().contains("Issue"))
      message += "A New Investment in " + getSymbol() + " is being made with an approximately allocated value of Rs. "
          + getAllocationValue() + " on " + getExchangeName() + " for your client code " + getClientCode() + " with "
          + getBrokerName() + "Broker ." + "\n";

    if (getAdviseType().contains("Close"))
      message += "An existing stock: " + getSymbol() + " is being exited with number of shares " + getNumberOfShares()
          + " on " + getExchangeName() + " for your client code " + getClientCode() + " with " + getBrokerName()
          + " Broker." + "\n" + "" + "\n";

    message += "***Please approve by going into Wealthdesk Mobile App \"My Investments\" (Section) > " + getFundName()
        + " > \"See All Changes\" ***" + "\n";
    message += "\n" + "Regards, " + "\n" + getAdviserName() + "(" + getFundName() + ")" + "\n" + " "
        + DateUtils.prettyTime(datetime);

    return message;
  }

  @Override
  public String getHTMLTextformat() {
    try {
      String html;
      if (getAdviseType().contains("Issue"))
        html = CommonUtils.getFile("emailers/wealthdesk/alert-new-advise-user-approval-email.template", getClass());
      else if (getAdviseType().contains("Close"))
        html = CommonUtils.getFile("emailers/wealthdesk/alert-close-advise-user-approval-email.template", getClass());
      else
        throw new IOException("No Emailer related found for Advise type: " + getAdviseType());
      return prepareHtml(html);
    } catch (Exception e) {
      log.error("HTML Email could not be send due to error: " + e.getMessage(), e);
      return getPlainTextFormat();
    }
  }

  private String prepareHtml(String html) throws IOException {
    Map<String, String> placeHolderMap = new HashMap<String, String>();
    placeHolderMap.put("_ADVISERNAME_", getAdviserName());
    placeHolderMap.put("_USERNAME_", getUserName());
    placeHolderMap.put("_SYMBOL_", getSymbol());
    placeHolderMap.put("_ALLOCATIONVALUE_", getAllocationValue() + "");
    placeHolderMap.put("_PRICE_", getPrice() + "");
    placeHolderMap.put("_ALLOCATION_", getAllocation() + "");
    placeHolderMap.put("_EXCHANGENAME_", getExchangeName());
    placeHolderMap.put("_CLIENTCODE_", getClientCode());
    placeHolderMap.put("_NUMBEROFSHARES_", getNumberOfShares() + "");
    placeHolderMap.put("_FUNDNAME_", getFundName());
    placeHolderMap.put("_BROKERNAME_", getBrokerName());
    placeHolderMap.put("_EMAILERURL_", getS3URLLink());
    placeHolderMap.put("_DATETIME_", DateUtils.prettyTime(datetime));
    return CommonUtils.replacePlaceholders(html, placeHolderMap);
  }

  private double getAllocation() {
    return DoublesUtil.round(allocation, 2);
  }

  private double getPrice() {
    return DoublesUtil.round(price, 2);
  }

  private double getNumberOfShares() {
    return DoublesUtil.round(numberOfShares, 0);
  }

  private double getAllocationValue() {
    return DoublesUtil.round(allocationValue, 2);
  }

  private String getS3URLLink() throws IOException {
    if (getAdviseType().contains("Issue"))
      return getProperties().EMAIL_ASSETS_FOLDER_NAME() + "/NewAdviseAlert";
    else if (getAdviseType().contains("Close"))
      return getProperties().EMAIL_ASSETS_FOLDER_NAME() + "/CloseAdviseAlert";
    else
      throw new IOException("No Emailer related found for Advise type: " + getAdviseType());
  }

  @Override
  public String sendTo() {
    return getUserEmail();
  }

  public AlertForUserApprovalEmail(RebalanceFundAlertData data) {
    super();
    this.symbol = data.getSymbol();
    this.allocationValue = data.getAllocationValue() > 0 ? data.getAllocationValue() : calculateValue(data);
    this.exchangeName = data.getExchange();
    this.clientCode = data.getClientCode();
    this.userEmail = data.getEmail();
    this.numberOfShares = data.getNumberOfShares() > 0 ? data.getNumberOfShares() : calculateQty(data);
    this.fundName = data.getFundName();
    this.adviseType = data.getAdviseType();
    this.adviserName = data.getAdviserName();
    this.brokerName = data.getBrokerName();
    this.properties = data.getProperties();
    this.datetime = data.getInvestmentDate();
    this.userName = data.getUserName();
    this.allocation = data.getAllocation();
    this.price = data.getPrice();
  }

  private double calculateValue(RebalanceFundAlertData data) {
    if (data.getNumberOfShares() > 0 && data.getPrice() > 0)
      return DoublesUtil.round(data.getNumberOfShares() * data.getPrice(), 2);
    return 0;
  }

  private int calculateQty(RebalanceFundAlertData data) {
    if (data.getAllocationValue() > 0 && data.getPrice() > 0) {
      return (int) (data.getAllocationValue() / data.getPrice());
    }
    return 0;
  }
}
