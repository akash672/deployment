package com.wt.utils.email.templates.wealthdesk;

import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.Getter;

@Getter
public class WrongDealerPasswordEmail implements BaseEmailTemplate {
  private final String sendTo;
  private Object environment;

  @Override
  public String getSubjectLine() {
    return "WRONG PASSWORD FOR DEALER!";
  }

  @Override
  public String getPlainTextFormat() {
    return "Wrong Dealer Password on the Server " + environment + " " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME");
  }

  @Override
  public String getHTMLTextformat() {
    return "<p>Wrong Dealer Password on the Server " + environment + " " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME") + "</p>";
  }

  @Override
  public String sendTo() {
    return getSendTo();
  }

  public WrongDealerPasswordEmail(String sendTo, String environment) {
    super();
    this.sendTo = sendTo;
    this.environment = environment;
  }

}
