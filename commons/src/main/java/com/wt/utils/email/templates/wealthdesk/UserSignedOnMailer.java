package com.wt.utils.email.templates.wealthdesk;

import java.util.HashMap;
import java.util.Map;

import com.wt.config.utils.WDProperties;
import com.wt.utils.CommonUtils;
import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@Getter
@Log4j
public class UserSignedOnMailer implements BaseEmailTemplate {
	
	private String userEmail;
	private String investorName;
	@Getter(AccessLevel.PRIVATE)
	private WDProperties properties;

	@Override
	public String getSubjectLine() {
		return "Welcome Aboard! | WealthDesk";
	}

	@Override
	public String getPlainTextFormat() {
		return "We are glad to have you onboard! Your set username is: "+getInvestorName()+"\r\n" + 
				"To get started, log onto your account on wealthdesk mobile app using this username and your set password.\r\n"
				+ "You can start with a virtual money, invest in different funds and track your portfolio and your adviser portfolio realtime";
	}

	@Override
	public String getHTMLTextformat() {
      String html;
      try {
        html = CommonUtils.getFile("emailers/wealthdesk/user-sign-on.template", getClass());
        return prepareHtml(html);
      } catch (Exception e) {
        log.error("Could not send HTML email due to error: "+e.getMessage(), e);
        return getPlainTextFormat();
      }
	}

	private String prepareHtml(String html) {
		Map<String, String> placeHolderMap = new HashMap<String,String>();
		placeHolderMap.put("_INVESTORNAME_", getInvestorName());
		placeHolderMap.put("_USERNAME_", getUserEmail());
		placeHolderMap.put("_EMAILERURL_", getS3URLLink());
		return CommonUtils.replacePlaceholders(html, placeHolderMap);
	}

	private String getS3URLLink() {
		return getProperties().EMAIL_ASSETS_FOLDER_NAME()+"/UserSignOn";
	}

	@Override
	public String sendTo() {
		return getUserEmail();
	}

}
