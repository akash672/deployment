package com.wt.utils.email.templates.wealthdesk;

import com.wt.config.utils.WDProperties;
import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserAppliedForKYC implements BaseEmailTemplate {
  
  private String userName;
  private String email;
  private String phoneNumber;
  @Getter(AccessLevel.PRIVATE)
  private WDProperties properties; 

  @Override
  public String getSubjectLine() {
    return "New User Applied for KYC";
  }

  @Override
  public String getPlainTextFormat() {
    return "Hi, A new user have requested for KYC. Please do the requisite to initiate KYC process for: \n Name: " + getUserName() + ",\n Email: " + getEmail() + ",\n Phone Number: +" + getPhoneNumber();
  }

  @Override
  public String getHTMLTextformat() {
    return "Hi, A new user have requested for KYC. Please do the requisite to initiate KYC process for: \n Name: " + getUserName() + "\n Email: " + getEmail() + "\n Phone Number: +" + getPhoneNumber();
  }

  @Override
  public String sendTo() {
    return properties.BROKER_SALES_TEAM_EMAILS();
  }

  private String getEmail() {
    if (this.email.isEmpty())
      return "NA";
    else
      return this.email;
  }
}
