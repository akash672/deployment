package com.wt.utils.email.factory;

import com.wt.config.utils.WDProperties;
import com.wt.domain.write.commands.RebalanceFundAlertData;
import com.wt.utils.email.templates.wealthdesk.AlertForUserApprovalEmail;
import com.wt.utils.email.templates.wealthdesk.KYCDoneEmail;
import com.wt.utils.email.templates.wealthdesk.OTPEmail;
import com.wt.utils.email.templates.wealthdesk.UserSignedOnMailer;

public class WealthDeskEmailer implements EmailFactory {

  @Override
  public BaseEmailTemplate getEmail(String emailer, Object... args) {
    if (emailer.equals(KYCDoneEmail.class.getSimpleName())) {
      return new KYCDoneEmail((String) args[0], (String) args[1], (String) args[2], (String) args[3],
          (WDProperties) args[4]);
    } else if (emailer.equals(UserSignedOnMailer.class.getSimpleName())) {
      return new UserSignedOnMailer((String) args[0], (String) args[1], (WDProperties) args[2]);
    } else if (emailer.equals(AlertForUserApprovalEmail.class.getSimpleName())) {
      return new AlertForUserApprovalEmail((RebalanceFundAlertData) args[0]);
    } else if (emailer.equals(OTPEmail.class.getSimpleName())) {
      return new OTPEmail((String) args[0], (String) args[1]);
    }
    return null;
  }

}
