package com.wt.utils.email.templates.wealthdesk;

import java.util.HashMap;
import java.util.Map;

import com.wt.config.utils.WDProperties;
import com.wt.utils.CommonUtils;
import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Getter
@Log4j
@AllArgsConstructor
public class KYCDoneEmail implements BaseEmailTemplate {

  private String email;
  private String brokerCompany;
  private String clientCode;
  private String firstName;
  
  @Getter(value=AccessLevel.PRIVATE)
  private WDProperties properties;

  @Override
  public String getSubjectLine() {
    return "Congratulations! Your KYC is Completed with " + getBrokerCompany();
  }

  @Override
  public String getPlainTextFormat() {
    return "Hi " + getFirstName() + ", We are glad to inform you that your KYC Process is completed with "
        + getBrokerCompany() + "and your generated client code is " + getClientCode()
        + ". We are excited to see you progress in achieving your investment goals by investing your savings through WealthDesk.";
  }

  @Override
  public String getHTMLTextformat() {
    String htmlText;
    try {
      htmlText = CommonUtils.getFile("emailers/wealthdesk/user-kyc-completed.template", getClass());
      return prepareHtmlText(htmlText);
    } catch (Exception e) {
      log.error("Could not send HTML email due to error: " + e.getMessage(), e);
      return getPlainTextFormat();
    }
  }

  @Override
  public String sendTo() {
    return getEmail();
  }

  private String prepareHtmlText(String html) {

    Map<String, String> placeHolderMap = new HashMap<>();
    placeHolderMap.put("_INVESTORNAME_", getFirstName());
    placeHolderMap.put("_BROKERCOMPANY_", getBrokerCompany());
    placeHolderMap.put("_CLIENTCODE_", getClientCode());
    placeHolderMap.put("_EMAILERURL_", getEmailerURL());

    return CommonUtils.replacePlaceholders(html, placeHolderMap);
  }

  private String getEmailerURL() {
    return properties.EMAIL_ASSETS_FOLDER_NAME() + "/UserKYCCompleted";
  }

}
