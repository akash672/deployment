package com.wt.utils.email.templates.wealthdesk;

import com.wt.utils.email.factory.BaseEmailTemplate;

import lombok.Getter;

@Getter
public class CTCLConnectionFailureEmail implements BaseEmailTemplate {
  private final String sendTo;

  @Override
  public String getSubjectLine() {
    return "CTCL Connection FAILURE!";
  }

  @Override
  public String getPlainTextFormat() {
    return "Kindly Check the CTCL services provided for " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME");
  }

  @Override
  public String getHTMLTextformat() {
    return "<p>Kindly Check the CTCL services provided for " + System.getProperty("os.name") + "-"
        + System.getenv("HOSTNAME") + "</p>";
  }

  @Override
  public String sendTo() {
    return getSendTo();
  }

  public CTCLConnectionFailureEmail(String sendTo) {
    super();
    this.sendTo = sendTo;
  }

}
