package com.wt.utils.email.factory;

public interface BaseEmailTemplate {
    public String getSubjectLine();
    public String getPlainTextFormat();
    public String getHTMLTextformat();
    public String sendTo();
}
