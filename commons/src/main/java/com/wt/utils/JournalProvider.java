package com.wt.utils;

import static akka.stream.ActorMaterializer.create;
import static java.util.concurrent.CompletableFuture.allOf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import akka.Done;
import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.japi.function.Predicate;
import akka.japi.function.Procedure;
import akka.persistence.query.EventEnvelope;
import akka.persistence.query.PersistenceQuery;
import akka.persistence.query.javadsl.CurrentEventsByPersistenceIdQuery;
import akka.persistence.query.javadsl.CurrentPersistenceIdsQuery;
import akka.persistence.query.javadsl.ReadJournal;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Log4j
public abstract class JournalProvider {
  
  private ReadJournal readJournal;
  private ActorSystem system;
  
  public JournalProvider(ActorSystem system) {
    this.system = system;
  }
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public ReadJournal journal(ActorSystem system) {
    if (readJournal == null) {
      String queryJournalClass = system.settings().config().getString("queryJournalClass");
      String queryIdentifier = system.settings().config().getString("queryIdentifier");

      if (queryJournalClass == null || queryIdentifier == null) {
        throw new RuntimeException(
            "Please set queryIdentifier and queryJournalClass variables in application.conf or reference.conf");
      }

      try {
        Class clasz = Class.forName(queryJournalClass);
        readJournal = PersistenceQuery.get(system).getReadJournalFor(clasz, queryIdentifier);
      } catch (ClassNotFoundException e) {
        throw new RuntimeException("Caught exception : " + e);
      }
    }

    return readJournal;
  }
  public CompletableFuture<Void> runForEachId(Procedure<EventEnvelope> function,
      Map<String, Long> idsWithStartSequenceNr) {
    List<CompletableFuture<Done>> allFutures = new ArrayList<>();

    for (String id : idsWithStartSequenceNr.keySet()) {
      Long fromSequenceNr = idsWithStartSequenceNr.get(id);

      CompletionStage<Done> mapPreparedCompletionStage = runForEachEvent(id, fromSequenceNr, function);
      allFutures.add(mapPreparedCompletionStage.toCompletableFuture());
    }
    CompletableFuture<Void> combinedFuture = allOf(allFutures.toArray(new CompletableFuture[0]));
    return combinedFuture;
  }

  public CompletionStage<Done> runForEachEvent(String id, long sequenceNr, Procedure<EventEnvelope> function) {
    ActorMaterializer materializer = ActorMaterializer.create(system);
    Source<EventEnvelope, NotUsed> eventsForId = ((CurrentEventsByPersistenceIdQuery) journal(system))
        .currentEventsByPersistenceId(id, sequenceNr, Long.MAX_VALUE);
    return eventsForId.runForeach(function, materializer);
  }

  public final List<Object> fetchFilteredEventsByPersistenceId(String id, Predicate<EventEnvelope> filter) {
    List<Object> allEvents = new ArrayList<>();
    try {
      ((CurrentEventsByPersistenceIdQuery) journal(system)).currentEventsByPersistenceId(id, 0, Long.MAX_VALUE)
          .filter(filter).runForeach((event) -> allEvents.add(event.event()), create(system)).toCompletableFuture()
          .get();
    } catch (InterruptedException | ExecutionException e) {
      log.error(" Error while getting currentEventsForPersistenceId for id " + id, e);
    }
    return allEvents;
  }

  public List<Object> fetchEventsByPersistenceId(String id) {
    List<Object> allEvents = new ArrayList<>();
    try {
      ((CurrentEventsByPersistenceIdQuery) journal(system)).currentEventsByPersistenceId(id, 0, Long.MAX_VALUE)
          .runForeach((event) -> allEvents.add(event.event()), create(system)).toCompletableFuture()
          .get();
    } catch (InterruptedException | ExecutionException e) {
      log.error(" Error while getting currentEventsForPersistenceId for id " + id, e);
    }
    return allEvents;
  }

  
  @SafeVarargs
  public final List<String> currentPersistenceIds(Materializer materializer, Predicate<String>... filters)
      throws InterruptedException, ExecutionException {
    Source<String, NotUsed> currentPersistenceIds = ((CurrentPersistenceIdsQuery) journal(system))
        .currentPersistenceIds();

    for (Predicate<String> filter : filters)
      currentPersistenceIds = currentPersistenceIds.filter(filter);

    List<String> allIds = new ArrayList<String>();
    CompletionStage<Done> allIdCompletionStage = currentPersistenceIds.runForeach(id -> allIds.add(id), materializer);
    allIdCompletionStage.toCompletableFuture().get();
    return allIds;
  }
  
  public final Source<EventEnvelope, NotUsed> currentEventsSourceForPersistenceId(String id) {
    return ((CurrentEventsByPersistenceIdQuery) journal(system)).currentEventsByPersistenceId(id, 0, Long.MAX_VALUE);
  }
  
  public final Source<EventEnvelope, NotUsed> allEventsSourceForPersistenceId(String id) {
    return allEventsSourceForPersistenceId(id, 0, Long.MAX_VALUE);
  }

  public abstract Source<EventEnvelope, NotUsed> allEventsSourceForPersistenceId(String id, long from, long to);
  
  public abstract Source<String, NotUsed> allPersistenceIds(@SuppressWarnings("unchecked") Predicate<String>... filters);
  
  public abstract void getSubscribedEvents(String typeOfEvent,long from, ActorRef destinationActor);


}
