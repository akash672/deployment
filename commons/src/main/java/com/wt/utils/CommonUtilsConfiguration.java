package com.wt.utils;

import static com.amazonaws.regions.Regions.fromName;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.wt.config.utils.WDProperties;

import lombok.extern.log4j.Log4j;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan(basePackages = {"com.wt.config.utils","com.wt.utils.aws"})
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:environment.properties")
@Log4j

public class CommonUtilsConfiguration {
  
  @Autowired
  protected WDProperties wdProperties;

  @Value("${amazon.dynamodb.endpoint}")
  private String amazonDynamoDBEndpoint;

  @Value("${amazon.aws.accesskey}")
  private String amazonAWSAccessKey;

  @Value("${amazon.aws.secretkey}")
  private String amazonAWSSecretKey;

  @Value("${amazon.aws.ses.accesskey}")
  private String amazonSESAccessKey;

  @Value("${amazon.aws.ses.secretkey}")
  private String amazonSESSecretKey;

  @Value("${amazon.aws.region}")
  private String amazonAWSRegion;

  @Bean
  public Executor taskExecutor() {
    return new SimpleAsyncTaskExecutor();
  }

  @Bean(name = "amazonDynamoDB")
  public AmazonDynamoDB amazonDynamoDB() {
    log.debug("Creating dynamodb connections");
    ClientConfiguration config = new ClientConfiguration();
    config.setMaxConnections(20);
    AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
        .withCredentials(amazonAWSCredentialsProvider())
        .withEndpointConfiguration(new EndpointConfiguration(amazonDynamoDBEndpoint, amazonAWSRegion))
        .build();
    log.debug("Created dynamodb connections");
    return amazonDynamoDB;
  }

  @Primary
  @Bean(name = "AWSCredentials")
  public AWSCredentials amazonAWSCredentials() {
    return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
  }
  
  @Primary
  @Bean(name="AWSCredentialsProvider")
  public AWSCredentialsProvider amazonAWSCredentialsProvider() {
    return new AWSStaticCredentialsProvider(amazonAWSCredentials());
  }
  
  @Bean(name= "AmazonS3")
  public AmazonS3 amazonS3() {
    return AmazonS3ClientBuilder.standard()
        .withCredentials(amazonAWSCredentialsProvider())
        .withRegion(fromName(amazonAWSRegion))
        .build();
  }

  @Bean(name = "SESCredentials")
  public AWSCredentials amazonSESCredentials() {
    return new BasicAWSCredentials(amazonSESAccessKey, amazonSESSecretKey);
  }
  
  @Bean(name="SESCredentialsProvider")
  public AWSCredentialsProvider amazonSESCredentialsProvider() {
    return new AWSStaticCredentialsProvider(amazonSESCredentials());
  }
  
  @Bean(name = "sesClient")
  public AmazonSimpleEmailService prepareSESClient() {
    return AmazonSimpleEmailServiceClientBuilder.standard().withRegion(usRegionAsMumbaiDoesNotHaveSnS().getName())
        .withCredentials(amazonSESCredentialsProvider()).build();
  }

  private Region usRegionAsMumbaiDoesNotHaveSnS() {
    return Region.getRegion(Regions.US_EAST_1);
  }

  public void setSystemProperties() {
    // TODO find a better way to do so.
    System.setProperty("environment", wdProperties.environment());
  }
  
  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
    return properties;
  }
  
}
