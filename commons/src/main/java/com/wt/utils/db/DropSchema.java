package com.wt.utils.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.model.DeleteTableRequest;
import com.amazonaws.services.dynamodbv2.model.DeleteTableResult;
import com.google.common.reflect.ClassPath.ClassInfo;
import com.wt.config.utils.ReflectionUtils;
import com.wt.utils.CommonAppConfiguration;

import lombok.extern.log4j.Log4j;

@Log4j
@Component
public class DropSchema {
  private AmazonDynamoDB dynamoDB;
  private ReflectionUtils reflectionUtils;

  @Autowired
  public DropSchema(AmazonDynamoDB dynamoDB, ReflectionUtils reflectionUtils) {
    super();
    this.dynamoDB = dynamoDB;
    this.reflectionUtils = reflectionUtils;
  }

  public static void main(String[] args) throws Exception {

    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CommonAppConfiguration.class)) {
      DropSchema executeSqlSchemaBean = ctx.getBean(DropSchema.class);
      executeSqlSchemaBean.dropDB();
    }
  }

  public void dropDB() throws Exception {
    log.debug("Finding classes in domain ...");
    dropTablesForClassesInPackage("com.wt.domain");
  }

  public void dropTablesForClassesInPackage(String packageName) throws Exception {
    for (ClassInfo classInfo : reflectionUtils.classOfPackage(packageName)) {
      try {
        log.debug("Found class: " + classInfo.getName());
        deleteTableForClass(classInfo.getName());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void deleteTableForClass(String name) throws ClassNotFoundException {
    Class<?> clazz = Class.forName(name);
    DynamoDBTable tableNameAnnotation = clazz.getDeclaredAnnotation(DynamoDBTable.class);
    if (tableNameAnnotation == null)
      return;
    String tableName = tableNameAnnotation.tableName();
    if (tableName == null || tableName.isEmpty())
      return;
    DeleteTableRequest deleteTableRequest = new DeleteTableRequest().withTableName(tableName);
    log.debug("Deleting " + deleteTableRequest);
    DeleteTableResult deleteTable = dynamoDB.deleteTable(deleteTableRequest);
    log.debug("CreateTableResult" + deleteTable);
  }

}