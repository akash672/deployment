package com.wt.utils;

import static com.wt.utils.CommonConstants.COLON;
import static java.lang.Integer.parseInt;
import static scala.collection.JavaConversions.asScalaBuffer;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import akka.actor.ActorSystem;
import eventstore.HttpSettings;
import eventstore.Settings;
import eventstore.cluster.ClusterSettings;
import eventstore.j.ClusterSettingsBuilder;
import eventstore.j.EsConnection;
import eventstore.j.EsConnectionFactory;
import eventstore.j.SettingsBuilder;

@Service
public class EventStoreConnector {

  private EsConnection connection;

  @Autowired
  public EventStoreConnector(ActorSystem system) {
    String ip = system.settings().config().getString("eventstore.address.host");
    int port = parseInt(system.settings().config().getString("eventstore.address.port"));
    String userName = system.settings().config().getString("eventstore.credentials.login");
    String password = system.settings().config().getString("eventstore.credentials.password");
    List<String> clusterGossipIps = system.settings().config().getStringList("eventstore.cluster.gossip-seeds");
    final Settings settings = getSettings(system, ip, port, userName, password, clusterGossipIps);
    connection = EsConnectionFactory.create(system, settings);
  }

  private Settings getSettings(ActorSystem actorSystem, String ip, int port, String userName, String password, List<String> clusterGossipIps) {
    List<InetSocketAddress> clusterGossipInets = new ArrayList<InetSocketAddress>();
    if (!clusterGossipIps.isEmpty()) {
      clusterGossipIps.stream().forEach(clusterNode -> clusterGossipInets
          .add(new InetSocketAddress(clusterNode.split(COLON)[0], parseInt(clusterNode.split(COLON)[1]))));
      ClusterSettings clusterSettings = new ClusterSettingsBuilder()
          .gossipSeeds(asScalaBuffer(clusterGossipInets))
          .build();
      return new SettingsBuilder()
          .address(new InetSocketAddress(ip, port))
          .http(HttpSettings.apply(actorSystem.settings().config().getConfig("eventstore")))
          .defaultCredentials(userName, password)
          .cluster(clusterSettings)
          .build();
    } else
      return new SettingsBuilder()
          .address(new InetSocketAddress(ip, port))
          .http(HttpSettings.apply(actorSystem.settings().config().getConfig("eventstore")))
          .defaultCredentials(userName, password)
          .build();
  }

  public EsConnection getConnection() {
    return connection;
  }

}
