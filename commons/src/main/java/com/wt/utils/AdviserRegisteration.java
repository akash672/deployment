package com.wt.utils;

import com.wt.aws.cognito.configuration.CredentialsProviderType;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AdviserRegisteration {
  @NonNull
  private String adviserUsername;
  @NonNull
  private String adviserName;
  @NonNull
  private String email;
  @NonNull
  private String publicId;
  private CredentialsProviderType type = CredentialsProviderType.Cognito;
  private long timestamp = DateUtils.currentTimeInMillis();
  private String password;

  public AdviserRegisteration(String adviserUsername, String adviserName, String email, String password, String publicId) {
    super();
    this.adviserUsername = adviserUsername;
    this.adviserName = adviserName;
    this.email = email;
    this.password = password;
    this.publicId = publicId;
  }

}
