package com.wt.utils;

import static com.wt.utils.DoublesUtil.round;

import java.util.function.ToDoubleFunction;
import java.util.stream.Collector;

public class NumberUtils {

  public static int getFirstHalf(int number) {
    return (int) round(number / 2., 0);
  }

  public static int getSecondHalf(int number) {
    return number - getFirstHalf(number);
  }

  public static <T> Collector<T, ?, Double> averagingWeighted(ToDoubleFunction<T> valueFunction,
      ToDoubleFunction<T> weightFunction) {
    class Box {
      double num = 0;
      double denom = 0;
    }
    return Collector.of(Box::new, (b, e) -> {
      b.num += valueFunction.applyAsDouble(e) * weightFunction.applyAsDouble(e);
      b.denom += weightFunction.applyAsDouble(e);
    }, (b1, b2) -> {
      b1.num += b2.num;
      b1.denom += b2.denom;
      return b1;
    }, b -> b.num / b.denom);
  }

  public static boolean isNumeric(String str) {
    return str.matches("-?\\d+(\\.\\d+)?");
  }

  public static boolean containsNumbers(String str) {
    return str.matches(".*\\d+.*");
  }

}
