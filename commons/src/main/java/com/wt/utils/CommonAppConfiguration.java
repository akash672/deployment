package com.wt.utils;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.logs.AWSLogsAsyncClientBuilder;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.google.common.collect.ImmutableMap;
import com.wt.config.utils.WDProperties;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.sms.factory.DefaultSMSFactory;
import com.wt.utils.sms.factory.MOSLSMSFactory;
import com.wt.utils.sms.factory.SMSFactory;
import com.wt.utils.sms.factory.WealthDeskSMSFactory;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

@EnableAsync
@Configuration
@ComponentScan(basePackages = "com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:environment.properties")
@Log4j
public class CommonAppConfiguration {

  private static final Integer INCEPTION_MONTHS = 40 * 12;

  @Autowired
  protected WDProperties wdProperties;

  @Autowired
  @Lazy
  protected WDActorSelections wdActorSelections;

  @Value("${amazon.dynamodb.endpoint}")
  private String amazonDynamoDBEndpoint;

  @Value("${amazon.aws.accesskey}")
  private String amazonAWSAccessKey;

  @Value("${amazon.aws.secretkey}")
  private String amazonAWSSecretKey;

  @Value("${amazon.aws.ses.accesskey}")
  private String amazonSESAccessKey;

  @Value("${amazon.aws.ses.secretkey}")
  private String amazonSESSecretKey;

  @Getter
  @Value("${amazon.aws.region}")
  private String amazonAWSRegion;

  @Getter
  @Value("${amazon.aws.sms.region}")
  private String amazonSMSAWSRegion;
  
  
  @Bean
  public Executor taskExecutor() {
    return new SimpleAsyncTaskExecutor();
  }

  @Bean(name = "amazonDynamoDB")
  public AmazonDynamoDB amazonDynamoDB() {
    log.debug("Creating dynamodb connections");
    ClientConfiguration config = new ClientConfiguration();
    config.setMaxConnections(20);
    AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().
        withEndpointConfiguration(new EndpointConfiguration(amazonDynamoDBEndpoint, amazonAWSRegion)).
        withCredentials(amazonAWSCredentialsProvider()).build();
    
    log.debug("Created dynamodb connections");
    return amazonDynamoDB;
  }
  
  @Bean(name = "drawDownConfig")
  public Map<String, Integer> drawDownConfig() {
    return new ImmutableMap.Builder<String, Integer>().put("INCEPTION", INCEPTION_MONTHS).put("1M", 1).put("3M", 3)
        .put("6M", 6).put("9M", 9).put("1Y", 12).build();
  }

  @Primary
  @Bean(name = "AWSCredentials")
  public AWSCredentials amazonAWSCredentials() {
    return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
  }
  
  @Primary
  @Bean(name="AWSCredentialsProvider")
  public AWSCredentialsProvider amazonAWSCredentialsProvider() {
    return new AWSStaticCredentialsProvider(amazonAWSCredentials());
  }

  @Bean(name = "SESCredentials")
  public AWSCredentials amazonSESCredentials() {
    return new BasicAWSCredentials(amazonSESAccessKey, amazonSESSecretKey);
  }
  
  @Bean(name="SESCredentialsProvider")
  public AWSCredentialsProvider amazonSESCredentialsProvider() {
    return new AWSStaticCredentialsProvider(amazonSESCredentials());
  }
  
  @Bean(name = "sesClient")
  public AmazonSimpleEmailService prepareSESClient() {
    return AmazonSimpleEmailServiceClientBuilder.standard().withRegion(usRegionAsMumbaiDoesNotHaveSnS().getName())
        .withCredentials(amazonSESCredentialsProvider()).build();
  }

  private Region usRegionAsMumbaiDoesNotHaveSnS() {
    return Region.getRegion(Regions.US_EAST_1);
  }

  public void setSystemProperties() {
    // TODO find a better way to do so.
    System.setProperty("environment", wdProperties.environment());
  }

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
    return properties;
  }

  @Bean
  public SMSFactory smsFactory() throws Exception {
    if (wdProperties.ACTIVE_BROKERS().get(0).equals("BPWEALTH"))
      return new WealthDeskSMSFactory();
    else if (wdProperties.ACTIVE_BROKERS().get(0).equals("MOSL"))
      return new MOSLSMSFactory();
    else
      return new DefaultSMSFactory();
  }

  @Bean
  public AWSLogs awsLogs() {
    return AWSLogsAsyncClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider())
        .withRegion(amazonAWSRegion).build();
  }

  @Bean
  public AmazonCloudWatch amazonCloudWatch() {
    return AmazonCloudWatchAsyncClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider())
        .withRegion(amazonAWSRegion).build();
  }
}