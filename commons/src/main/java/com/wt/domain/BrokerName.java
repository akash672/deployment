package com.wt.domain;

import static com.google.common.collect.Maps.uniqueIndex;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static java.util.Arrays.asList;

import java.util.Map;

import javax.validation.constraints.NotNull;

public enum BrokerName {
  BPWEALTH("BPWEALTH"),
  SYKES("SYKES"),
  MOSL("MOSL"),
  MOCK("MOCK"),
  NOBROKER("NOBROKER");

  private static final Map<String, BrokerName> LOOKUP = uniqueIndex(asList(BrokerName.values()),
      BrokerName::getBrokerName);

  private final String brokerName;

  BrokerName(String brokerName) {
    this.brokerName = brokerName;
  }

  public String getBrokerName() {
    return brokerName;
  }

  @NotNull
  public static BrokerName fromBrokerName(String brokerName, InvestingMode investingMode) {
    if (investingMode.equals(VIRTUAL))
      return LOOKUP.get("MOCK");
    return LOOKUP.get(brokerName);
  }

}