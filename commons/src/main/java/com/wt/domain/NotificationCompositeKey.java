package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class NotificationCompositeKey implements Serializable {
  private static final long serialVersionUID = 1L;
  @DynamoDBHashKey(attributeName = "username")
  private String username;
  @DynamoDBRangeKey(attributeName = "notificationTimestamp")
  private long timestamp;
  
}
