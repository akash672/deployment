package com.wt.domain;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.google.common.collect.Maps;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@DynamoDBTable(tableName = "Notification")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Notification implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @DynamoDBIgnore
  private NotificationCompositeKey notificationCompositeKey = new NotificationCompositeKey();

  @Getter
  @Setter
  @DynamoDBAttribute
  private Map<String,String> jsonMessage = Maps.newHashMap(); 

  @DynamoDBHashKey(attributeName = "username")
  public String getUsername() {
    return notificationCompositeKey.getUsername();
  }

  public void setUsername(String username) {
    notificationCompositeKey.setUsername(username);
  }

  public void setTimestamp(long timestamp) {
    notificationCompositeKey.setTimestamp(timestamp);
  }

  @DynamoDBRangeKey(attributeName = "notificationTimestamp")
  public long getTimestamp() {
    return notificationCompositeKey.getTimestamp();
  }

}
