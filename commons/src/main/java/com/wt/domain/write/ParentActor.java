package com.wt.domain.write;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import akka.actor.ActorContext;
import akka.actor.ActorRef;
import scala.Option;

public interface ParentActor<T> {
  static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParentActor.class);

  public default void forwardToChildren(ActorContext context, Object command, String... children) {

    for (String child : children) {
      //log.info("forward command " + command + " to " + child);
      ActorRef childActor = findChild(context, child);
      childActor.forward(command, context);
    }
  }

  public default void tellChildren(ActorContext context, ActorRef sender, Object command, String... children) {
    for (String child : children) {
      //log.info("tell command " + command + " to " + child);
      ActorRef childActor = findChild(context, child);
      childActor.tell(command, sender);
    }
  }

  public default ActorRef findChild(ActorContext context, String name) {
    String childName = getName() + "-" + name;
    Option<ActorRef> maybeActor = context.child(childName);
    if (maybeActor.isEmpty()) {
      //log.info("maybeActor is empty! Creating " + childName);
      return context.actorOf(SpringExtProvider.get(context.system()).props(getChildBeanName(), name), childName);
    } else {
      //log.info("maybeActor is not empty! Using " + childName);
      return maybeActor.get();
    }
  }

  public default Object askChild(ActorContext context, Object command, String child) throws Exception {
    ActorRef childActor = findChild(context, child);
    return askAndWaitForResult(childActor, command);
  }
  
  public String getName();

  public String getChildBeanName();

}