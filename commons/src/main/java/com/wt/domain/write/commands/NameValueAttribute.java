package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NameValueAttribute {
  private String name;
  private String value;
}
