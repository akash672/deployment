package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import akka.http.javadsl.model.StatusCodes;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class Exists extends Failed {
  private static final long serialVersionUID = 1L;

  @JsonCreator
  public static Exists exists(@JsonProperty("name") String name) {
    return new Exists(name, getResult());
  }

  protected Exists(String id, String reason) {
    super(id, reason, StatusCodes.NOT_ACCEPTABLE);
  }

  public static String getResult() {
    return "Already exists";
  }
}
