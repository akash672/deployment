package com.wt.domain.write.events;

import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import akka.http.javadsl.model.StatusCode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class PreConditionFailed implements Result {

  @Getter
  private String id;
  @Getter
  private String message;
  
  @JsonCreator
  public PreConditionFailed(@JsonProperty("id") String id, @JsonProperty("message") String message) {
    super();
    this.id = id;
    this.message = message;
  }
  
  @Override
  public StatusCode getStatusCode() {
    return PRECONDITION_FAILED;
  }

}
