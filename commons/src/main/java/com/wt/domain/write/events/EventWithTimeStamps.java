package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wt.utils.DateUtils;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
public class EventWithTimeStamps {
  @JsonIgnore @Getter private long timestamp;
  public EventWithTimeStamps() {
    this(DateUtils.currentTimeInMillis());
  }
}
