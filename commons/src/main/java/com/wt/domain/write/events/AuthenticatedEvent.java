package com.wt.domain.write.events;

public class AuthenticatedEvent extends EventWithTimeStamps {
  private String identityId;
  private String username;

  public AuthenticatedEvent(String username) {
    super();
    this.identityId = username;
    this.username = username;
  }
  
  public AuthenticatedEvent(String identityId, String username) {
    super();
    this.identityId = identityId;
    this.username = username;
  }
  
  public String getUsername() {
    return username != null ? username : identityId;
  }
}
