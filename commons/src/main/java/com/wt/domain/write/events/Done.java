package com.wt.domain.write.events;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import akka.http.javadsl.model.StatusCode;
import akka.http.javadsl.model.StatusCodes;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
@ToString
@EqualsAndHashCode
public class Done implements Result, Serializable {

  
  private static final long serialVersionUID = 1L;
  @Getter
  private String id;
  @Getter
  private String message;

  @JsonCreator
  public Done(@JsonProperty("id") String id, @JsonProperty("message") String message) {
    super();
    this.id = id;
    this.message = message;
  }

  @Override
  public StatusCode getStatusCode() {
    return StatusCodes.OK;
  }

}
