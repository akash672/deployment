package com.wt.domain.write;

import static akka.stream.ThrottleMode.shaping;
import static com.wt.domain.write.events.Failed.failed;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.wt.config.utils.WDProperties;
import com.wt.domain.write.events.Result;

import akka.actor.ActorRef;
import akka.http.javadsl.model.StatusCodes;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Log4j
public class ChildrenResultsAggregator {
  private final ActorRef self;

  private boolean calculating = false;
  private ActorRef originalSender;
  private int resultsAwaited = 0;
  private int totalResult = 0;

  private List<Result> results = new ArrayList<>();
  private Result emptyResultForNoChildren;
  private ActorMaterializer materializer;

  public ChildrenResultsAggregator(ActorRef self, Result emptyResultForNoChildren, ActorMaterializer materializer) {
    this.self = self;
    this.emptyResultForNoChildren = emptyResultForNoChildren;
    this .materializer = materializer;
  }

  public void tellAllChildrenToCalculate(ActorRef sender, Collection<String> children,
      Consumer<String> tellChildFunction, WDProperties wdProperties) {
    if (calculating) {
      log.info(self + " already calculating ");
      sender.tell(failed("" + self.path(), "Already calculating", StatusCodes.TOO_MANY_REQUESTS), self);
      return;
    }

    if (children.isEmpty()) {
      log.info(emptyResultForNoChildren + " emptyResultForNoChildren ");
      sender.tell(emptyResultForNoChildren, self);
      return;
    }

    tellChildrenAndSetState(sender, children, tellChildFunction, wdProperties);
  }

  private void tellChildrenAndSetState(ActorRef sender, Collection<String> children,
      Consumer<String> tellChildFunction, WDProperties wdProperties) {
    calculating = true;
    originalSender = sender;
    totalResult = children.size();
    
    Source.from(children)
    .throttle(wdProperties.NUMBER_OF_CHILDREN_TO_RECOVER(), Duration.ofSeconds(wdProperties.CHILD_RECOVERY_FREQUENCY()), wdProperties.CHILD_RECOVERY_MAXIMUM_BURST(), shaping())
    .runForeach(child -> {
      tellChildFunction.accept(child);
      resultsAwaited++;
    }, materializer);

    log.info(self + " awaiting " + children.size() + " results ");
  }

  public void onResultReceived(Result result, Consumer<Pair<ActorRef, List<Result>>> consumer) {
    results.add(result);
    log.debug(self.path().name() + " awaiting " + resultsAwaited + " results. Received " + results.size());

    if (allResultsReceived()) 
      aggregateResultsAndTellSender(consumer);
  }
  
  private boolean allResultsReceived() {
    return results.size() == totalResult;
  }
  
  private void aggregateResultsAndTellSender(Consumer<Pair<ActorRef, List<Result>>> consumer) {
    log.debug(self + " received all " + resultsAwaited);

    consumer.accept(new ImmutablePair<ActorRef, List<Result>>(originalSender, results));
    calculating = false;
    resultsAwaited = 0;
    totalResult = 0;
    results.clear();
  }

}
