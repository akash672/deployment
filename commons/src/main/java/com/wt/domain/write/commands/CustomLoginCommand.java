package com.wt.domain.write.commands;

public interface CustomLoginCommand {
  String getPassword();
}
