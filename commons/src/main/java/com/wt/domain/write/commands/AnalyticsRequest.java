package com.wt.domain.write.commands;

import scala.Option;

public interface AnalyticsRequest {
    public String getName();
    public Option<String> getCondition();
}
