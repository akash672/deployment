package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonIgnore;

import akka.http.javadsl.model.StatusCode;

public interface Result {
  public String getId();
  public String getMessage();
  @JsonIgnore
  public StatusCode getStatusCode();
}
