package com.wt.domain.write;

import akka.stream.Attributes;
import akka.stream.FlowShape;
import akka.stream.Inlet;
import akka.stream.Outlet;
import akka.stream.stage.AbstractInHandler;
import akka.stream.stage.AbstractOutHandler;
import akka.stream.stage.GraphStage;
import akka.stream.stage.GraphStageLogic;

public class SendLastReceived<T> extends GraphStage<FlowShape<T, T>> {
  public Inlet<T> fromProducer = Inlet.<T> create("SendLastReceived.in");
  public Outlet<T> toConsumer = Outlet.<T> create("SendLastReceived.out");
  private FlowShape<T, T> flowShape = FlowShape.of(fromProducer, toConsumer);

  @Override
  public FlowShape<T, T> shape() {
    return flowShape;
  }

  @Override
  public GraphStageLogic createLogic(Attributes inheritedAttributes) {
    return new GraphStageLogic(flowShape) {
      private T currentValue = null;
      private boolean waitingForFirstValue = true;

      {
        setHandler(fromProducer, new AbstractInHandler() {
          @Override
          public void onPush() throws Exception {
            currentValue = grab(fromProducer);
            if (waitingForFirstValue) {
              waitingForFirstValue = false;
              if (isAvailable(toConsumer))
                push(toConsumer, currentValue);
            }
            pull(fromProducer);
          }
        });
        setHandler(toConsumer, new AbstractOutHandler() {
          @Override
          public void onPull() throws Exception {
            if (!waitingForFirstValue) 
              push(toConsumer, currentValue);
          }
        });
      }

      @Override
      public void preStart() {
        pull(fromProducer);
      }

    };
  }
}
