package com.wt.domain.write.events;

import static akka.http.javadsl.model.StatusCodes.FORBIDDEN;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import akka.http.javadsl.model.StatusCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Forbidden extends Failed {

  private static final long serialVersionUID = 1L;

  @JsonCreator
  public static Forbidden forbidden(String id, @JsonProperty("reason") String reason) {
    return new Forbidden(id, reason);
  }

  protected Forbidden(String id, String reason) {
    super(id, reason, FORBIDDEN);
  }
  
  @JsonIgnore
  @Override
  public StatusCode getStatusCode() {
    return FORBIDDEN;
  }
}
