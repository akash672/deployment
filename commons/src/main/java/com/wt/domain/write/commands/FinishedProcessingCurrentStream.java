package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FinishedProcessingCurrentStream implements Serializable{
  private static final long serialVersionUID = 1L;

}
