package com.wt.domain.write.events;

import java.io.Serializable;

import com.wt.utils.DateUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ActiveInstrument implements Serializable {

  private static final long serialVersionUID = 1L;
  private String ticker;
  private long timeStamp;

  public ActiveInstrument(String ticker) {
    super();
    this.ticker = ticker;
    this.timeStamp = DateUtils.currentTimeInMillis();
  }

}
