package com.wt.domain.write.events;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import akka.http.javadsl.model.StatusCode;
import akka.http.javadsl.model.StatusCodes;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class Failed implements Result, Serializable {
  private static final long serialVersionUID = 1L;
  @Getter
  private String id;
  @Getter
  private String message;
  private StatusCode statusCode;

  public Failed(String id, String message) {
    super();
    this.id = id;
    this.message = message;
    this.statusCode = StatusCodes.BAD_REQUEST;
  }

  @JsonCreator
  public Failed(@JsonProperty("id") String id, @JsonProperty("message") String message,
      @JsonProperty("statusCode") StatusCode statusCode) {
    super();
    this.id = id;
    this.message = message;
    this.statusCode = statusCode;
  }

  @Override
  public StatusCode getStatusCode() {
    return this.statusCode;
  }

  public static Failed failed(String id, String message, StatusCode statusCode) {
    if (statusCode != null)
      return new Failed(id, message, statusCode);
    else
      return new Failed(id, message, StatusCodes.BAD_REQUEST);
  }
}
