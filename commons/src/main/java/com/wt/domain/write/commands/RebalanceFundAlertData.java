package com.wt.domain.write.commands;

import com.wt.config.utils.WDProperties;

import lombok.Getter;

@Getter
public class RebalanceFundAlertData {

  private String symbol;
  private double allocationValue;
  private String exchange;
  private String clientCode;
  private String email;
  private String phoneNumber;
  private int numberOfShares;
  private String fundName;
  private String adviseType;
  private String adviserName;
  private String brokerName;
  private WDProperties properties;
  private long investmentDate;
  private String userName;
  private double price;
  private double allocation;

  public RebalanceFundAlertData(String symbol, double round, String exchange, String clientCode, String email,
      String phoneNumber, int numberOfShares, String fundName, String adviseType, String adviserName, String brokerName,
      WDProperties properties, long investmentDate, String userName, double currentPrice, double allocation) {
    this.symbol = symbol;
    this.allocationValue = round;
    this.exchange = exchange;
    this.clientCode = clientCode;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.numberOfShares = numberOfShares;
    this.fundName = fundName;
    this.adviseType = adviseType;
    this.adviserName = adviserName;
    this.brokerName = brokerName;
    this.properties = properties;
    this.investmentDate = investmentDate;
    this.userName = userName;
    this.price = currentPrice;
    this.allocation = allocation;
  }

}
