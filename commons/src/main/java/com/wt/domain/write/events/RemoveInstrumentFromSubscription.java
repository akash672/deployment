package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
@Setter
public class RemoveInstrumentFromSubscription implements Serializable {

  private static final long serialVersionUID = 1L;
  private String ticker;

}
