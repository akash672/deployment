package com.wt.domain;

import java.nio.ByteBuffer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@JsonIgnoreProperties({ "identityID", "salt", "password", "timestamp" })
public class WDCredentials {
  private String identityID;
  private String accessKey;
  private String secretKey;
  private String sessionToken;
  private long expiration;
  private ByteBuffer salt;
  private ByteBuffer password;
  private long timestamp;

  public static WDCredentials forCognitoAdviser(String identityID, String accessKey, String secretKey,
      String sessionToken, long expiration, ByteBuffer salt, ByteBuffer password, long timestamp) {
    return new WDCredentials(identityID, accessKey, secretKey, sessionToken, expiration, salt, password, timestamp);
  }

  public static WDCredentials forCognitoUser(String identityID, String accessKey, String secretKey, String sessionToken,
      long expiration, ByteBuffer salt, ByteBuffer password, long timestamp) {
    return new WDCredentials(identityID, accessKey, secretKey, sessionToken, expiration, salt, password, timestamp);
  }

  public static WDCredentials forFederatedUser(String identityID, String accessKey, String secretKey,
      String sessionToken, long expiration, long timestamp) {
    return new WDCredentials(identityID, accessKey, secretKey, sessionToken, expiration, null, null, timestamp);
  }

  @JsonCreator
  public WDCredentials(@JsonProperty("identityID") String identityID, @JsonProperty("accessKey") String accessKey,
      @JsonProperty("secretKey") String secretKey, @JsonProperty("sessionToken") String sessionToken,
      @JsonProperty("expiration") long expiration, @JsonProperty("salt") ByteBuffer salt,
      @JsonProperty("password") ByteBuffer password, @JsonProperty("timestamp") long timestamp) {
    super();
    this.identityID = identityID;
    this.accessKey = accessKey;
    this.secretKey = secretKey;
    this.sessionToken = sessionToken;
    this.expiration = expiration;
    this.salt = salt;
    this.password = password;
    this.timestamp = timestamp;
  }
}
