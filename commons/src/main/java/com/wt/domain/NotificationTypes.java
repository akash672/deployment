package com.wt.domain;

public enum NotificationTypes {
  Adviser,
  Investment,
  ChangeInvestment,
  Exchange,
  Account,
  Kyc,
  Actionable,
  Fund
}
