package com.wt.domain;

import lombok.Getter;

public enum InvestingMode {
  VIRTUAL("VIRTUAL"), REAL("REAL");

  @Getter
  private final String investingMode;

  private InvestingMode(String investingMode) {
    this.investingMode = investingMode;
  }
}
