package com.wt.domain.repo;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wt.domain.Notification;
import com.wt.domain.NotificationCompositeKey;

@EnableScan
public interface NotificationRepository extends CrudRepository<Notification, NotificationCompositeKey> {
  @EnableScan
  public List<Notification> findByUsername(@Param("username") String username);
}