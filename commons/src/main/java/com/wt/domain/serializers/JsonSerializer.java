package com.wt.domain.serializers;

import static akka.actor.ActorRef.noSender;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import akka.persistence.PersistentImpl;
import akka.persistence.PersistentRepr;
import akka.persistence.eventstore.EventStoreSerializer;
import eventstore.Event;
import eventstore.EventData;
import eventstore.j.EventDataBuilder;
import scala.Option;

public class JsonSerializer implements EventStoreSerializer {
  private Gson GSON;

  @SuppressWarnings("rawtypes")
  public JsonSerializer() { 
    GsonBuilder gsonBuilder = new GsonBuilder(); 
    gsonBuilder.registerTypeAdapter(scala.Option.class, new OptionDeserializer()); 
    GSON = gsonBuilder.create();
  }

  @Override
  public int identifier() {
    return 10023;
  }

  @Override 
  public byte[] toBinary(Object o) { 
    return GSON.toJson(o).getBytes();
  } 

  @Override 
  public boolean includeManifest() {
    return true;
  }

  @Override 
  public Object fromBinary(byte[] bytes, Option<Class<?>> classOption) { 
    try { 
      return fromBinary(bytes, classOption.get()); 
    } catch (Exception e) { 
      e.printStackTrace(); 
      return null; 
    }
  }

  @Override 
  public Object fromBinary(byte[] bytes) { 
    return null; 
  }

  @Override 
  public Object fromBinary(byte[] bytes, Class<?> aClass) {
    try {
      return GSON.fromJson(new String(bytes), aClass);
    } catch (Exception e) { 
      e.printStackTrace();
      return null;
    } 
  }

  @Override 
  public EventData toEvent(Object o) { 
    PersistentImpl obj = (PersistentImpl) o; 
    EventDataBuilder eventDataBuilder = new EventDataBuilder(obj.payload().getClass().getName()); 
    eventDataBuilder.jsonData(new String(toBinary(obj.payload())));
    eventDataBuilder.jsonMetadata(obj.payload().getClass().getName()); 
    return eventDataBuilder.build(); 
  }

  @Override
  public Object fromEvent(Event event, Class<?> aClass) {
    Object result = null; 
    try {
      result = fromBinary(event.data().data().value().toArray(), Class.forName(event.data().eventType())); 
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }

    PersistentRepr persistentRepr = new PersistentImpl(result,
        event.number().value(),
        event.streamId().value(),
        "MANIFEST", // TODO check this
        false,
        noSender(), 
        "WriterUUID"// TODO check this
        );

    return persistentRepr;
  }
}

class OptionDeserializer<T> implements JsonDeserializer<Option<T>> {
  public Option<T> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException { 
    if (json.isJsonNull()) { 
      return Option.empty(); 
    } else if (json.isJsonObject() && json.getAsJsonObject().entrySet().size() == 0) { 
      return Option.empty(); 
    } else { 
      String className = json.getAsJsonObject().get("class").getAsString(); 

      try { 
        return Option.apply(context.deserialize(json.getAsJsonObject().get("value"), Class.forName(className))); 
      } 
      catch (ClassNotFoundException e) { 
        return Option.empty(); 
      }
    } 
  } 
}