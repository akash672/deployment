package com.wt.config.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;

@Service
public class ReflectionUtils {
  private final static Set<Class<?>> NUMBER_REFLECTED_PRIMITIVES;
  static {
    Set<Class<?>> s = new HashSet<>();
    s.add(byte.class);
    s.add(short.class);
    s.add(int.class);
    s.add(long.class);
    s.add(float.class);
    s.add(double.class);
    NUMBER_REFLECTED_PRIMITIVES = s;
  }

  public List<ClassInfo> classOfPackage(String packageName) throws IOException {
    List<ClassPath.ClassInfo> infos = new ArrayList<>();
    final ClassLoader loader = Thread.currentThread().getContextClassLoader();

    for (final ClassPath.ClassInfo info : ClassPath.from(loader).getAllClasses()) {
      if (info.getPackageName().equals(packageName))
        infos.add(info);
    }

    return infos;
  }

  public boolean isReflectedAsNumber(Class<?> type) {
    return Number.class.isAssignableFrom(type) || NUMBER_REFLECTED_PRIMITIVES.contains(type);
  }
}
