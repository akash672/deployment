package com.wt.config.utils;

import static com.wt.utils.CommonConstants.CommaSeparated;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.GsonBuilder;

import lombok.extern.log4j.Log4j;

@Service
@Log4j
public class WDProperties {

  public static String DUMMY_HASHCODE_FOR_USER_ACTIVATION = "sdgfjhgikasi@!233";

  @Autowired
  private Properties properties;

  public String version() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("version.properties"));

    return properties.getProperty("version");
  }

  public String keystoreFileName() {
    return property("wd.keystore.filename");
  }

  public String keystorePassword() {
    return property("wd.keystore.password");
  }

  public String url() {
    String key = "wd.url";
    return property(key);
  }

  public String realtimeStreamUrl() throws NumberFormatException, IOException {
    String key = "wd.realtime.performance.url";
    return property(key);
  }

  public Integer port() throws NumberFormatException, IOException {
    String key = "wd.port";
    return new Integer(property(key));
  }

  public Integer realtimeStreamPort() throws NumberFormatException, IOException {
    String key = "wd.realtime.performance.port";
    return new Integer(property(key));
  }

  
  public String bpRegisteredName() {
    String key = "broker.bp.registeredname";
    return property(key);
  }

  public String urlSSL() {
    String key = "wd.url.ssl";
    return property(key);
  }

  public Integer portSSL() throws NumberFormatException {
    String key = "wd.port.ssl";
    return new Integer(property(key));
  }

  public int maxPricesToSNSBatchSizeToRDS() throws NumberFormatException {
    String key = "max.prices.to.sns.batch.size.to.rds";
    return Integer.valueOf(property(key));
  }

  public int maxPricesToSNSBatchSizeToDDB() throws NumberFormatException {
    String key = "max.prices.to.sns.batch.size.to.ddb";
    return Integer.valueOf(property(key));
  }

  public String realtimeStreamUrlSSL() {
    String key = "wd.realtime.performance.url.ssl";
    return property(key);
  }

  public Integer realtimeStreamPortSSL() throws NumberFormatException {
    String key = "wd.realtime.performance.port.ssl";
    return new Integer(property(key));
  }

  public String environment() {
    return property("environment");
  }

  public String FCM_API_KEY() {
    return property("fcm_api_key");
  }

  public String CUSTOM_DOMAIN_NAME() {
    return property("wd.custom.domain.name");
  }

  public String SNS_APPLICATION_ARN() {
    return property("sns_application_arn");
  }

  public long OTP_EXPIRING_MINUTES() {
    if (property("expire.otp.minutes") == null) {
      return (10 * 60000);
    } else if (property("expire.otp.minutes").isEmpty()) {
      return (10 * 60000);
    }
    return Integer.parseInt(property("expire.otp.minutes")) * 60000;
  }

  public String AWS_ACCOUNT_ID() {
    return property("aws.account.id");
  }

  public String MUTUAL_FUND_ORDER_SENDER_EMAIL_ID() {
    return property("mutual.fund.order.sender.email.id");
  }

  public String SYKES_MUTUAL_FUND_ORDER_RECIPIENT_EMAIL_ID() {
    return property("sykes.mutual.fund.order.recipient.email.id");
  }

  public String BPW_MUTUAL_FUND_ORDER_RECIPIENT_EMAIL_ID() {
    return property("bpw.mutual.fund.order.recipient.email.id");
  }

  public String SYKES_MUTUAL_FUND_DIRECTORY_NAME() {
    return property("sykes.mutual.fund.directory.name");
  }

  public String BPWEALTH_MUTUAL_FUND_DIRECTORY_NAME() {
    return property("bpwealth.mutual.fund.directory.name");
  }

  public List<String> ACTIVE_BROKERS() throws Exception {
    return Arrays.asList(property("active.brokers").split(CommaSeparated));
  }
  
  public List<String> BOD_Taks() throws Exception {
    return Arrays.asList(property("bodTasksToRun").split(CommaSeparated));
  }

  public String BPW_XNEUTRINO_INTERACTIVE_IP() {
    return property("bpw.xneutrino.interactive.ip");
  }

  public int BPW_XNEUTRINO_INTERACTIVE_PORT() {
    return Integer.parseInt((String) property("bpw.xneutrino.interactive.port"));
  }

  public String SYKES_XNEUTRINO_INTERACTIVE_IP() {
    return property("sykes.xneutrino.interactive.ip");
  }

  public int SYKES_XNEUTRINO_INTERACTIVE_PORT() {
    return Integer.parseInt((String) property("sykes.xneutrino.interactive.port"));
  }

  public String XNEUTRINO_BROADCAST_IP() {
    return property("xneutrino.broadcast.ip");
  }

  public int XNEUTRINO_BROADCAST_PORT() {
    return Integer.parseInt(property("xneutrino.broadcast.port"));
  }

  public int maxNumberOfWritesToDBInMinute() {
    return Integer.valueOf(property("maxNumberOfWritesToDBInMinute"));
  }
  
  public int maxNumberOfWritesToRdsInABatch() {
    return Integer.valueOf(property("maxNumberOfBatchWritesToRDS"));
  }

  public double INITIAL_VIRTUAL_MONEY_AMOUNT() throws NumberFormatException, IOException {
    return Double.parseDouble((String) property("virtual_money_initial"));
  }

  public boolean MARKET_OFFLINE_ENABLED() {
    return Boolean.parseBoolean((String) property("market.offline.enabled"));
  }

  public boolean forceSnapshotAtEOD() {
    return Boolean.parseBoolean((String) property("forceSnapshotAtEOD"));
  }

  public boolean disableRecoveryFromSnapshot() {
    return Boolean.parseBoolean((String) property("disableRecoveryFromSnapshot"));
  }

  public boolean USER_SNS_ENABLED() {
    return Boolean.parseBoolean((String) property("user.sns.enabled"));
  }

  public boolean scheduleViewpopWrites() {
    String condition = (String) property("scheduleViewpopWrites");
    if (condition == null) {
      return false;
    }
    return property("scheduleViewpopWrites").equals("true") ? true : false;
  }

  public boolean scheduleTicks() {
    String condition = (String) property("scheduleTicks");
    if (condition == null) {
      return false;
    }
    return property("scheduleTicks").equals("true") ? true : false;
  }

  public long STREAM_PRICE_TICK_INTERVAL() {
    long streamPriceTickInterval = 3;
    try {
      streamPriceTickInterval = Long.parseLong(property("stream.price.tick.interval"));
    } catch (Exception e) {
      log.error("Cannot get the eod price tick interval due to " + e.getMessage());
      streamPriceTickInterval = 3;
    }
    return streamPriceTickInterval;
  }

  public int STREAM_PRICE_TICK_FREQUENCY() {
    int streamPriceTickFrequency = 1;
    try {
      streamPriceTickFrequency = Integer.parseInt(property("stream.price.tick.frequency"));
    } catch (Exception e) {
      log.error("Cannot get the eod price tick frequency due to " + e.getMessage());
      streamPriceTickFrequency = 1;
    }
    return streamPriceTickFrequency;
  }

  public int STREAM_PRICE_TICK_MAXIMUM_BURST() {
    int streamPriceTickMaximumBurst = 1;
    try {
      streamPriceTickMaximumBurst = Integer.parseInt(property("stream.price.tick.maximum.burst"));
    } catch (Exception e) {
      log.error("Cannot get the eod price tick maximum burst due to " + e.getMessage());
      streamPriceTickMaximumBurst = 1;
    }
    return streamPriceTickMaximumBurst;
  }

  public int NUMBER_OF_CHILDREN_TO_RECOVER() {
    int numberOfChildrenToRecover = 1;
    try {
      numberOfChildrenToRecover = Integer.parseInt(property("number.of.children.to.recover"));
    } catch (Exception e) {
      log.error("Cannot get the number of users to recover due to " + e.getMessage());
      numberOfChildrenToRecover = 1;
    }
    return numberOfChildrenToRecover;
  }

  public int CHILD_RECOVERY_FREQUENCY() {
    int childRecoveryFrequency = 2;
    try {
      childRecoveryFrequency = Integer.parseInt(property("child.recovery.frequency"));
    } catch (Exception e) {
      log.error("Cannot get the user recovery frequency due to " + e.getMessage());
      childRecoveryFrequency = 2;
    }
    return childRecoveryFrequency;
  }

  public int CHILD_RECOVERY_MAXIMUM_BURST() {
    int childRecoveryMaximumBurst = 5;
    try {
      childRecoveryMaximumBurst = Integer.parseInt(property("child.recovery.maximum.burst"));
    } catch (Exception e) {
      log.error("Cannot get the user maximum burst due to " + e.getMessage());
      childRecoveryMaximumBurst = 5;
    }
    return childRecoveryMaximumBurst;
  }

  public String WD_ACTOR_SYSTEM_ADVISER_ROOT_ACTOR() {
    return property("wdActorSystem.advisersRootActor");
  }

  public String WD_ACTOR_SYSTEM_USER_ROOT_ACTOR() {
    return property("wdActorSystem.userRootActor");
  }

  public String EQUITY_CTCL_ACTOR_SYSTEM_ORDER_ROOT_ACTOR() {
    return property("equityCtclActorSystem.equityOrderRootActor");
  }

  public String MF_EXECUTION_ACTOR_SYSTEM_MF_ORDER_ROOT_ACTOR() {
    return "" + property("mfExecutionActorSystem.mfOrderRootActor");
  }

  public String UNIVERSE_ACTOR_SYSTEM_UNIVERSE_ROOT_ACTOR() {
    return property("universeActorSystem.universeRootActor");
  }

  public String WD_ACTIVE_INSTRUMENT_TRACKER_ACTOR() {
    return property("wdActorSystem.activeInstrumentTracker");
  }

  public String USER_REALTIME_PERFORMANCE_ROOT_ACTOR() {
    return property("wdActorSystem.realtimeInvestorPerformanceRoot");
  }

  public String REALTIME_MARKET_PRICE_ROOT_ACTOR() {
    return property("wdActorSystem.realtimeMarketPriceRoot");
  }

  public String ADVISER_REALTIME_PERFORMANCE_ROOT_ACTOR() {
    return property("wdActorSystem.realtimeFundPerformanceRoot");
  }
  
  public String RDS_FACADE_ROOT_ACTOR() {
    return property("wdActorSystem.rdsFacadeRootActor");
  }

  public int SNAPSHOT_INTERVAL() {
    int snapShotInterval = 50;
    try {
      snapShotInterval = Integer.parseInt(property("snapshot.interval"));
    } catch (Exception e) {
      log.error("Cannot get the snapshot interval due to " + e.getMessage());
      snapShotInterval = 50;
    }
    return snapShotInterval;
  }
  
  public String OUTBOUND_TRANSACTIONAL_EMAIL() throws IOException {
    return property("outbound.transaction.email");
  }

  public boolean useSSL() {
    return property("useSSL").equals("true") ? true : false;
  }

  public boolean exitViewPopulaterOnDBUploadFailure() {
    return property("exitViewPopulaterOnDBUploadFailure").equals("true") ? true : false;
  }

  public int USER_TRIAL_PERIOD() {
    return Integer.parseInt(property("user.trial.period"));
  }

  public int EVENT_SEQUENCE_INDEX() {
    return Integer.parseInt(property("eventSequence.index"));
  }

  private String property(String key) {
    String value = System.getProperty(key);
    if (value != null)
      return value;

    return properties.getProperty(key);
  }

  public String awsRegion() {
    return property("amazon.aws.region");
  }

  public String DEV_TEAM_EMAILIDS() {
    String emails = property("dev.team.emails");
    return emails.isEmpty() ? "team@wealthtech.in" : emails;
  }

  public boolean isSendingUserEmailUpdatesAllowed() {
    String condition = (String) property("sendUserEmailUpdates");
    return condition.equals("true") ? true : false;
  }

  public String MUTUAL_FUND_OFFLINE_ORDER_FILE() {
    return "" + property("mf_offline_order_file");
  }

  public String SNS_TOPIC_ARN_TRIGGERING_FOR_DYNAMODB_DUMP() {
    return property("amazon.aws.sns.topicArnTriggeringDynamoDbDumpLambda");
  }

  public String SNS_TOPIC_ARN_TRIGGERING_FOR_WRITE_TO_RDS() {
    return property("amazon.aws.sns.topicArnTriggeringWriteToRDSLambda");
  }

  public String SNS_TOPIC_ARN_TRIGGERING_FOR_WRITE_TO_DDB() {
    return property("amazon.aws.sns.topicArnTriggeringWriteToDDBLambda");
  }

  public String EMAIL_ASSETS_FOLDER_NAME() {
    return property("email-assets-S3-bucketname");
  }

  public String BROKER_SALES_TEAM_EMAILS() {
    return property("broker.sales.email") + ",team@wealthtech.in";
  }
 
  public String BROKER_SALES_TEAM_PHONE_NUMBER() {
    return property("broker.sales.phone.numbers");
  }

  public String apiAuthLambdaARN() {
    return property("lambda.apiauthentication.apigateway");
  }
  
  public String userAdviseTransactionLambdaARN() throws IOException {
    return property("lambda.userAdviseTransaction.apigateway");
  }
  
  public String adviserAnalyticsLambdaARN() throws IOException {
    return property("lambda.adviser.analtyics");
  }

  public String investorFundPerformanceSnapshotLambda() throws IOException {
    return property("lambda.investor.fund.performance.snapshot");
  }
  
  public String dynamoDBPerformanceProcessor() throws IOException {
    return property("lambda.dynamoDBPerformanceProcessor.apigateway");
  }
  
  public String investorUserPooLARN() {
    return property("cognito.arn.investor");
  }
  
  public String adviserUserPoolARN() {
    return property("cognito.arn.adviser");
  }

  public String SMS_SENDER_NAME() {
    return property("sms.sender.name") != null ? property("sms.sender.name") : "WDESK";
  }
  
  public String appDebugFlag() {
    return property("app.properties.debug.flag") != null ? property("app.properties.debug.flag") : "false" ;
  }

  public String appApplicationId() {
    return property("app.properties.application.id");
  }

  public String appBuiltType() {
    return property("app.properties.built.type");
  }

  public String appFlavor() {
    return property("app.properties.flavor");
  }

  public String appVersionCode() {
    return property("app.properties.version.code");
  }

  public String appVersionName() {
    return property("app.properties.version.name");
  }

  public String byPassFlowFlag() {
    return property("app.properties.passflow.flag") != null ? property("app.properties.passflow.flag") : "false";
  }

  public String hotLineAppId() {
    return property("app.properties.hotline.id");
  }

  public String hotLineKey() {
    return property("app.properties.hotline.key");
  }

  public String isFabricOpen() {
    return property("app.properties.fabric.open") != null ? property("app.properties.fabric.open") : "true";
  }

  public String webSocketUrl() {
    return property("app.properties.websocket.url");
  }
  
  public String rdsDriverClass() {
    return property("rds.datasource.driver-class");
  }
  
  public String rdsPassword() {
    return property("rds.datasource.password");
  }
  
  public String rdsUsername() {
    return property("rds.datasource.username");
  }
  
  public String rdsUrl() {
    return property("rds.datasource.url");
  }
  
  public String moslBroadcastURL() {
    String wsLink = property("mosl.broadcast.websocket.url");
    if(wsLink == null)
      wsLink = "";
    return wsLink;
  }

  public String sortedFundArray() {
    return new GsonBuilder().disableHtmlEscaping().create()
        .toJson(Arrays.stream(property("app.properties.sorted.fund.arrays").split(","))
        .collect(Collectors.toList()));
  }

  public String INVESTOR_USER_POOL_ID() {
    return property("cognito.userpool.investor.userpool.id");
  }
  
  public int ASK_FOR_PRICES_FROM_TRACKER_INTERVAL(){
    return Integer.parseInt((String) property("get.prices.from.tracker.interval"));
  }
  
  public List<String> tablesToEnableStreamsFor() {
    return Arrays.stream(property("domain.tables.to.enable.streams.for").split(","))
        .collect(Collectors.toList());
  }

  public boolean shallMonitorService() {
    return property("service.monitoring.flag").equalsIgnoreCase("true") ? true : false;
  }

  public String devOpsNotifierSnsArn() {
    return property("devops.notifier.sns.arn");
  }

}

