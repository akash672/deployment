package com.wt.config.utils;

import static com.amazonaws.services.dynamodbv2.model.StreamViewType.NEW_AND_OLD_IMAGES;
import static com.amazonaws.services.dynamodbv2.transactions.TransactionManager.verifyOrCreateTransactionImagesTable;
import static com.amazonaws.services.dynamodbv2.transactions.TransactionManager.verifyOrCreateTransactionTable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.StreamSpecification;
import com.google.common.reflect.ClassPath.ClassInfo;

@Component
public class ExecuteSqlSchema {

  private AmazonDynamoDB dynamoDB;
  private ReflectionUtils reflectionUtils;
  private WDProperties properties;

  @Autowired
  public ExecuteSqlSchema(AmazonDynamoDB dynamoDB, ReflectionUtils reflectionUtils, WDProperties properties)
      throws Exception {
    super();
    this.dynamoDB = dynamoDB;
    this.reflectionUtils = reflectionUtils;
    this.properties = properties;
  }

  public void setupDB() throws Exception {
    verifyOrCreateTransactionTable(dynamoDB, properties.environment() + "_Transactions", 7, 7, null);
    verifyOrCreateTransactionImagesTable(dynamoDB, properties.environment() + "_TransactionImages", 7, 7, null);

    createTablesForClassesInPackage("com.wt.domain");
  }

  public void createTablesForClassesInPackage(String packageName) throws Exception {
    for (ClassInfo classInfo : reflectionUtils.classOfPackage(packageName)) {
      try {
        createTableForClass(classInfo.getName());
      } catch (Exception e) {
        System.out.println("Table creation failed for " + classInfo.getName());
        e.printStackTrace();
      }
    }
  }

  private void createTableForClass(String name) throws ClassNotFoundException {
    Class<?> clazz = Class.forName(name);
    DynamoDBTable tableNameAnnotation = clazz.getDeclaredAnnotation(DynamoDBTable.class);
    if (tableNameAnnotation == null)
      return;
    String tableName = tableNameAnnotation.tableName();
    if (tableName == null || tableName.isEmpty())
      return;
    CreateTableRequest createTableRequest = new CreateTableRequest()
        .withTableName(tableName)
        .withAttributeDefinitions(attributeDefinitionsFor(clazz))
        .withKeySchema(keySchemasFor(clazz))
        .withProvisionedThroughput(new ProvisionedThroughput()
        .withReadCapacityUnits(1L)
        .withWriteCapacityUnits(1L));
    
    String environmentAgnosticTableName = tableName.split("_")[1];

    if (properties.tablesToEnableStreamsFor().contains(environmentAgnosticTableName)) {
      StreamSpecification streamSpecification = new StreamSpecification();
        streamSpecification.withStreamEnabled(true)
        .withStreamViewType(NEW_AND_OLD_IMAGES);
      createTableRequest.withStreamSpecification(streamSpecification);
    }
    dynamoDB.createTable(createTableRequest);
  }

  private List<AttributeDefinition> attributeDefinitionsFor(Class<?> clazz) {
    for (Field field : clazz.getDeclaredFields()) {
      Id annotation = field.getDeclaredAnnotation(Id.class);
      if (annotation != null) {
        return attributeDefinitionsFromKeyClass(field.getType());
      }
    }

    return attributeDefinitionsFromKeyClass(clazz);
  }

  private List<AttributeDefinition> attributeDefinitionsFromKeyClass(Class<?> clazz) {
    List<AttributeDefinition> defs = new ArrayList<>();
    for (Field field : clazz.getDeclaredFields()) {
      DynamoDBHashKey annotation = field.getDeclaredAnnotation(DynamoDBHashKey.class);
      if (annotation == null)
        continue;
      AttributeDefinition attributeDefinition = new AttributeDefinition();
      attributeDefinition.withAttributeName(attributeName(field, annotation));
      attributeDefinition.withAttributeType(type(field));
      defs.add(attributeDefinition);
    }
    for (Field field : clazz.getDeclaredFields()) {
      DynamoDBRangeKey annotation = field.getDeclaredAnnotation(DynamoDBRangeKey.class);
      if (annotation == null)
        continue;
      AttributeDefinition attributeDefinition = new AttributeDefinition();
      attributeDefinition.withAttributeName(attributeName(field, annotation));
      attributeDefinition.withAttributeType(type(field));
      defs.add(attributeDefinition);
    }
    return defs;
  }

  private String attributeName(Field field, DynamoDBHashKey annotation) {
    return annotation.attributeName() == null || annotation.attributeName().isEmpty() ? field.getName()
        : annotation.attributeName();
  }

  private String attributeName(Field field, DynamoDBRangeKey annotation) {
    return annotation.attributeName() == null || annotation.attributeName().isEmpty() ? field.getName()
        : annotation.attributeName();
  }

  private List<KeySchemaElement> keySchemasFor(Class<?> clazz) {
    for (Field field : clazz.getDeclaredFields()) {
      Id annotation = field.getDeclaredAnnotation(Id.class);
      if (annotation != null) {
        return keySchema(field.getType());
      }
    }

    return keySchema(clazz);
  }

  private List<KeySchemaElement> keySchema(Class<?> clazz) {
    List<KeySchemaElement> keytSchemas = new ArrayList<>();
    for (Field field : clazz.getDeclaredFields()) {
      DynamoDBHashKey annotation = field.getDeclaredAnnotation(DynamoDBHashKey.class);
      if (annotation == null)
        continue;
      keytSchemas.add(new KeySchemaElement(attributeName(field, annotation), KeyType.HASH));
    }
    for (Field field : clazz.getDeclaredFields()) {
      DynamoDBRangeKey annotation = field.getDeclaredAnnotation(DynamoDBRangeKey.class);
      if (annotation == null)
        continue;
      keytSchemas.add(new KeySchemaElement(attributeName(field, annotation), KeyType.RANGE));
    }
    return keytSchemas;
  }

  private ScalarAttributeType type(Field field) {
    Class<?> type = field.getType();
    if (reflectionUtils.isReflectedAsNumber(type)) {
      return ScalarAttributeType.N;
    }
    if (type.isAssignableFrom(String.class)) {
      return ScalarAttributeType.S;
    }
    return ScalarAttributeType.B;
  }

}