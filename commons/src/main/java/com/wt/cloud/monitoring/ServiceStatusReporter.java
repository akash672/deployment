package com.wt.cloud.monitoring;

import static com.amazonaws.services.cloudwatch.model.ComparisonOperator.LessThanThreshold;
import static com.amazonaws.services.cloudwatch.model.Statistic.Average;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.PutMetricAlarmRequest;
import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.logs.model.CreateLogGroupRequest;
import com.amazonaws.services.logs.model.CreateLogStreamRequest;
import com.amazonaws.services.logs.model.DescribeLogGroupsRequest;
import com.amazonaws.services.logs.model.DescribeLogGroupsResult;
import com.amazonaws.services.logs.model.InputLogEvent;
import com.amazonaws.services.logs.model.InvalidSequenceTokenException;
import com.amazonaws.services.logs.model.MetricTransformation;
import com.amazonaws.services.logs.model.OperationAbortedException;
import com.amazonaws.services.logs.model.PutLogEventsRequest;
import com.amazonaws.services.logs.model.PutLogEventsResult;
import com.amazonaws.services.logs.model.PutMetricFilterRequest;
import com.amazonaws.services.logs.model.ResourceAlreadyExistsException;
import com.wt.utils.DateUtils;

import lombok.extern.log4j.Log4j;

@Service
@Log4j
@Scope("prototype")
public class ServiceStatusReporter {

  private static final String NAMESPACE = "LogMetrics";
  private static final Random RANDOM = new Random();
  private AWSLogs awsLogs;
  @Value("${environment:dev}")
  private String environment;
  @Value("${devops.notifier.sns.arn}")
  private String devOpsNotifierSnsArn;
  private boolean setupDoneStatus = false;
  private String logGroupName;
  private String sequenceToken = "NA";
  private AmazonCloudWatch amazonCloudWatch;

  @Autowired
  public ServiceStatusReporter(AWSLogs awsLogs, AmazonCloudWatch amazonCloudWatch) {
    this.awsLogs = awsLogs;
    this.amazonCloudWatch = amazonCloudWatch;
  }

  private void initializeLogGroupName(String serviceName) {
    this.logGroupName = "WT_" + this.environment.toUpperCase() + "_" + serviceName;
  }

  public void setupCloudWatchMetric(String serviceName) {
    createLogGroup(serviceName);
    createLogStream(serviceName);
    createMetricFilter(serviceName);
    setupDoneStatus = true;
  }

  private void createMetricFilter(String serviceName) {
    try {
      awsLogs.putMetricFilter(new PutMetricFilterRequest(logGroupName, serviceName + "_" + this.environment + "_filter",
          "", getMetricTransformations(serviceName)));
      amazonCloudWatch.putMetricAlarm(getMetricAlarmRequest(serviceName));
    } catch (OperationAbortedException e) {
      log.info(e.getMessage());
    } catch (Exception e) {
      log.error("CLOUDWATCH METRIC FILTER: " + e.getMessage(), e);
    }
  }

  private PutMetricAlarmRequest getMetricAlarmRequest(String serviceName) {
    //@formatter:off
    return new PutMetricAlarmRequest()
        .withAlarmName(serviceName + "_" + this.environment + "_Alarm")
        .withAlarmDescription("Service Check alarm for " +  this.environment + " -  " + serviceName + " service")
        .withMetricName(serviceName + "_" + this.environment + "_Metric")
        .withPeriod(60)
        .withStatistic(Average)
        .withDatapointsToAlarm(3)
        .withComparisonOperator(LessThanThreshold)
        .withEvaluationPeriods(3)
        .withNamespace(NAMESPACE)
        .withTreatMissingData("breaching")
        .withAlarmActions(devOpsNotifierSnsArn)
        .withThreshold(1d);
  //@formatter:on
  }

  private void createLogStream(String serviceName) {
    try {
      awsLogs.createLogStream(new CreateLogStreamRequest(logGroupName, serviceName));
    } catch (ResourceAlreadyExistsException e) {    } 
      catch (Exception e) {
      log.error("CLOUDWATCH LOG STREAM: " + e.getMessage(), e);
    }
  }

  private void createLogGroup(String serviceName) {
    try {
      awsLogs.createLogGroup(new CreateLogGroupRequest(logGroupName));
    } catch (ResourceAlreadyExistsException e) {    } 
      catch (Exception e) {
      log.error("CLOUDWATCH LOG GROUP: " + e.getMessage(), e);
    }
  }

  private List<MetricTransformation> getMetricTransformations(String serviceName) {
    return Arrays.asList(
        new MetricTransformation().withDefaultValue(1d).withMetricName(serviceName + "_" + this.environment + "_Metric")
            .withMetricNamespace(NAMESPACE).withMetricValue("1"));
  }

  public void sendStatusUpdate(String serviceName) {
    initializeLogGroupName(serviceName);
    if (!setupDoneStatus)
      setupCloudWatchMetric(serviceName);
    try {
      PutLogEventsResult putLogEventsResult = awsLogs
          .putLogEvents(new PutLogEventsRequest(logGroupName, serviceName, getEventsToSent(serviceName))
              .withSequenceToken(getRequiredSequenceNumber()));
      updateSequenceNumber(putLogEventsResult);
    } catch (InvalidSequenceTokenException e) {
      sequenceToken = e.getExpectedSequenceToken();
      sendStatusUpdate(serviceName);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

  private void updateSequenceNumber(PutLogEventsResult putLogEventsResult) {
    if (putLogEventsResult.getNextSequenceToken() != null)
      sequenceToken = putLogEventsResult.getNextSequenceToken();
  }

  private String getRequiredSequenceNumber() {
    if (sequenceToken.equals("NA")) {
      return requestCloudWatchForNextSequencNumber();
    }
    return sequenceToken;
  }

  private String requestCloudWatchForNextSequencNumber() {
    try {
      DescribeLogGroupsResult describeLogGroups = awsLogs
          .describeLogGroups(new DescribeLogGroupsRequest().withLogGroupNamePrefix(logGroupName));
      return describeLogGroups.getNextToken();
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return RANDOM.nextLong() + "";
    }
  }

  private List<InputLogEvent> getEventsToSent(String serviceName) {
    return Arrays.asList(new InputLogEvent().withMessage(serviceName + " Random|" + RANDOM.nextInt(Integer.MAX_VALUE))
        .withTimestamp(DateUtils.currentTimeInMillis()));
  }

}
