package com.wt.aws.cognito.exception;

public class AuthorizationException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public AuthorizationException(String s, RuntimeException e) {
    super(s, e);
  }

  public AuthorizationException(String s) {
    super(s);
  }
}
