package com.wt.aws.cognito.exception;

public class InternalErrorException extends RuntimeException {
  private static final long serialVersionUID = 1L;
  private static final String PREFIX = "INT_ERROR: ";

  public InternalErrorException(String s, RuntimeException e) {
    super(PREFIX + s, e);
  }

  public InternalErrorException(String s) {
    super(PREFIX + s);
  }
}
