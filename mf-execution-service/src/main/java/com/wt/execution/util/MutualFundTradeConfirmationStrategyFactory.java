package com.wt.execution.util;

public class MutualFundTradeConfirmationStrategyFactory {
  private MutualFundTradeConfirmationHandlingStrategy mutualFundAllotmentConfirmationStrategy = new MutualFundOrderAllotmentConfirmationStrategy();
  private MutualFundTradeConfirmationHandlingStrategy mutualFundAllotmentRejectionStrategy = new MutualFundOrderAllotmentRejectionStrategy();
  private MutualFundTradeConfirmationHandlingStrategy mutualFundRedemptionConfirmationStrategy = new MutualFundOrderRedemptionConfirmationStrategy();
  private MutualFundTradeConfirmationHandlingStrategy mutualFundRedemptionRejectionStrategy = new MutualFundOrderRedemptionRejectionStrategy();

  public MutualFundTradeConfirmationHandlingStrategy getMutualFundTradeConfirmationHandlingStrategyFrom(String[] confirmationFields) {
    if (isAllotmentConfirmation(confirmationFields))
      return mutualFundAllotmentConfirmationStrategy;
    else if (isAllotmentRejection(confirmationFields)) 
      return mutualFundAllotmentRejectionStrategy;
    else if (isRedemptionConfirmation(confirmationFields))
      return mutualFundRedemptionConfirmationStrategy;
    else if (isRedemptionRejection(confirmationFields))
      return mutualFundRedemptionRejectionStrategy;
    return null;
  }
  
  private boolean isAllotmentConfirmation(String[] confirmationFields) {
    return confirmationFields.length == 34 && (confirmationFields[21]).equals("Y");
  }
  
  private boolean isAllotmentRejection(String[] confirmationFields) {
    return confirmationFields.length == 34 && (confirmationFields[21]).equals("N");
  }
  
  private boolean isRedemptionConfirmation(String[] confirmationFields) {
    return confirmationFields.length != 34 && (confirmationFields[21]).equals("Y");
  }
  
  private boolean isRedemptionRejection(String[] confirmationFields) {
    return confirmationFields.length != 34 && (confirmationFields[21]).equals("N");
  }
  
}
