package com.wt.execution.util;

import java.text.ParseException;

import com.wt.utils.akka.WDActorSelections;

public interface MutualFundTradeConfirmationHandlingStrategy {
  public void extractSettlementDetailsFrom(String[] confirmationFields, WDActorSelections wdActorSelections) throws NumberFormatException, ParseException;
}
