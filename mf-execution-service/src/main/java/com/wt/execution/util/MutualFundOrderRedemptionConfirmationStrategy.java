package com.wt.execution.util;

import static com.wt.domain.write.commands.MutualFundTradeConfirmation.redemptionConfirmation;
import static com.wt.utils.DateUtils.bodTaskUnixTimeFromString;
import static java.lang.Double.parseDouble;

import java.text.ParseException;

import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;

public class MutualFundOrderRedemptionConfirmationStrategy implements MutualFundTradeConfirmationHandlingStrategy {

  @Override
  public void extractSettlementDetailsFrom(String[] confirmationFields, WDActorSelections wdActorSelections)
      throws NumberFormatException, ParseException {
    wdActorSelections.mforderRootActor()
        .tell(redemptionConfirmation(confirmationFields[1], confirmationFields[12], confirmationFields[6],
            bodTaskUnixTimeFromString(confirmationFields[0], "YYYY-MM-DD"), confirmationFields[15],
            confirmationFields[5], parseDouble(confirmationFields[18]), parseDouble(confirmationFields[19]),
            confirmationFields[22]), ActorRef.noSender());
  }

}
