package com.wt.ctcl;

import static com.wt.domain.BrokerName.BPWEALTH;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.date;
import static com.wt.utils.DateUtils.isTradingHoliday;
import static com.wt.utils.DateUtils.prettyDate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.wt.config.utils.WDProperties;
import com.wt.domain.BrokerType;
import com.wt.utils.S3Writer;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;

import lombok.extern.log4j.Log4j;

@Log4j
@Service("BPWRealMutualFundInteractive")
public class BPWRealMutualFundInteractive extends RealMutualFundInteractive implements S3Writer {

  @Autowired
  public BPWRealMutualFundInteractive(WDActorSelections actorSelections, InteractiveManager interactiveManager,
      WDProperties wdProperties, SESEmailUtil sesEmailUtil, AmazonS3 amazonS3Client) {
    super(actorSelections, interactiveManager, wdProperties, sesEmailUtil, amazonS3Client);
  }

  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(MUTUALFUNDSUBSCRIPTION, REAL, BPWEALTH);
  }

  @Override
  public void createDirectoryStructure(String date) throws IOException {
    File mutualFundOrderDirectory = new File(
        directoryStructure(wdProperties.BPWEALTH_MUTUAL_FUND_DIRECTORY_NAME(), date));
    if (!mutualFundOrderDirectory.exists()) {
      mutualFundOrderDirectory.mkdirs();
    }
  }

  @Override
  public File mutualFundOrderFile(String date) throws IOException {
    File mutualFundOrderFile = new File(directoryStructure(wdProperties.BPWEALTH_MUTUAL_FUND_DIRECTORY_NAME(), date),
        wdProperties.MUTUAL_FUND_OFFLINE_ORDER_FILE());
    if (!mutualFundOrderFile.exists())
      mutualFundOrderFile.getAbsoluteFile().createNewFile();
    return mutualFundOrderFile;
  }

  @Async
  @Scheduled(cron = "${mf.execution.s3.folder.creation.schedule}", zone = "Asia/Kolkata")
  @Override
  public void scheduleS3FolderCreation() {
    if (!isTradingHoliday()) {
      createBucket(amazonS3Client, BPWEALTH.getBrokerName().toLowerCase());
      String s3Path = s3PathFor(amazonS3Client, BPWEALTH.getBrokerName().toLowerCase());
      log.debug("S3 Path for mutual fund executions file of " + BPWEALTH + " is " + s3Path);
    }
  }

  @Async
  @Scheduled(cron = "${mf.order.file.sending.schedule}", zone = "Asia/Kolkata")
  @Override
  public void scheduleEmailingMutualFundOrderFile() {
    try {
      String mailSubject = "Mutual Fund Orders for " + prettyDate(currentTimeInMillis());
      String mailBody = "The attachment contains the mutual fund orders for " + prettyDate(currentTimeInMillis());
      sesEmailUtil.sendRawEmail(wdProperties.MUTUAL_FUND_ORDER_SENDER_EMAIL_ID(),
          wdProperties.BPW_MUTUAL_FUND_ORDER_RECIPIENT_EMAIL_ID(), wdProperties.DEV_TEAM_EMAILIDS(), mailSubject,
          mailBody, mutualFundOrderFile(date()));
    } catch (IOException e) {
      log.error("Could not send email due to " + e);
    }
  }

  @Async
  @Scheduled(cron = "${mf.execution.file.processing.schedule}", zone = "Asia/Kolkata")
  @Override
  public void scheduleS3ExecutionFileProcessing() {
    try {
      s3ObjectStream(BPWEALTH.getBrokerName().toLowerCase())
          .forEach(filteredFile -> processSettlementsFrom(BPWEALTH.getBrokerName().toLowerCase(), filteredFile));
    } catch (NullPointerException e) {
      log.error("Unable to create s3 object stream due to " + e.getMessage());
    }
  }

  @Override
  public void putObjectInS3AsObjectMetadata(String bucketName, String s3Path, InputStream orderLines) {

  }

}
