package com.wt.ctcl;

import static com.wt.utils.CommonConstants.PipeSeparated;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.date;
import static com.wt.utils.DateUtils.mfExecutionEodUnixTimeFromString;
import static com.wt.utils.DateUtils.month;
import static com.wt.utils.DateUtils.nextBusinessDate;
import static com.wt.utils.DateUtils.prettyDate;
import static com.wt.utils.DateUtils.year;
import static java.io.File.separator;
import static java.util.Arrays.asList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;
import java.util.stream.Stream;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.ctcl.xneutrino.interactive.MarginRequest;
import com.ctcl.xneutrino.interactive.MarginResponse;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.CtclOrder;
import com.wt.domain.MarginRequestMessage;
import com.wt.domain.write.commands.ReceivedByDealer;
import com.wt.execution.util.MutualFundTradeConfirmationStrategyFactory;
import com.wt.utils.CSVWritingUtil;
import com.wt.utils.S3Writer;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;

import akka.actor.ActorRef;
import lombok.extern.log4j.Log4j;

@Log4j
public abstract class RealMutualFundInteractive implements MutualFundInteractive, S3Writer, Observer {

  protected WDActorSelections actorSelections;
  protected InteractiveManager interactiveManager;
  protected WDProperties wdProperties;
  protected SESEmailUtil sesEmailUtil;
  protected AmazonS3 amazonS3Client;
  protected MutualFundTradeConfirmationStrategyFactory mutualFundTradeConfirmationStrategyFactory;

  public RealMutualFundInteractive(WDActorSelections actorSelections, InteractiveManager interactiveManager,
      WDProperties wdProperties, SESEmailUtil sesEmailUtil, AmazonS3 amazonS3Client) {
    super();
    this.actorSelections = actorSelections;
    this.interactiveManager = interactiveManager;
    this.wdProperties = wdProperties;
    this.sesEmailUtil = sesEmailUtil;
    this.amazonS3Client = amazonS3Client;
    this.mutualFundTradeConfirmationStrategyFactory = new MutualFundTradeConfirmationStrategyFactory();
  }

  @Override
  public void update(Observable o, Object arg) {
    if (arg instanceof MarginResponse) {
      MarginResponse marginResponse = (MarginResponse) arg;
      if (marginResponse != null) {
        MarginRequestMessage marginRequestMessage = new MarginRequestMessage(marginResponse.getClientCode(),
            (marginResponse.getAvaliableMargin() - marginResponse.getPDHV()) / 100.0);
        actorSelections.mforderRootActor().tell(marginRequestMessage, ActorRef.noSender());
      }
    }
  }

  @Override
  public void sendMarginRequest(String clientCode) throws Exception {
    MarginRequest marginRequest = new MarginRequest();
    marginRequest.setClientCode(clientCode);
    interactiveManager.sendRequest(marginRequest.getStruct(), false);
  }
  
  @Override
  public void sendMutualFundOrder(CtclOrder ctclOrder) {
    long exchOrderID = UUID.randomUUID().getMostSignificantBits();

    actorSelections.mforderRootActor().tell(new ReceivedByDealer(ctclOrder.getTxCode(), ctclOrder.getLocalOrderId(),
        exchOrderID, 12345, "12345", ctclOrder.getClientCode(), "Order Successfully placed", "1"), ActorRef.noSender());
    try {
      createOrderFileForOfflineExecution(ctclOrder);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void createOrderFileForOfflineExecution(CtclOrder order) throws IOException {
    String folioNumber = (order.getFolioNumber() == null) ? "0" : order.getFolioNumber();
    String currentDate = date();
    if (shouldBusinessDayBeSwitched())
      currentDate = String.valueOf(nextBusinessDate(currentTimeInMillis()).get(Calendar.DATE));
    createDirectoryStructure(currentDate);
    FileWriter writer = new FileWriter(mutualFundOrderFile(currentDate).getAbsolutePath(), true);
    CSVWritingUtil.writeLine(writer,
        asList(order.getTxCode(), order.getLocalOrderId(), "", order.getClientCode(),
            String.valueOf(order.getSchemeCode()), String.valueOf(order.getSide()), order.getOrderSideType(), "C",
            String.valueOf(order.getAllocatedAmount()), String.valueOf(order.getAllocatedAmount()),
            order.getAllRedeem(), folioNumber, "Y", order.getLocalOrderId(), "Y", "PASSWORD", "UniquePassKey"));
    writer.flush();
    writer.close();
  }

  private boolean shouldBusinessDayBeSwitched() {
    try {
      return currentTimeInMillis() > mfExecutionEodUnixTimeFromString(prettyDate(currentTimeInMillis()));
    } catch (ParseException e) {
      log.error("Could not parse the date due to " + e);
    }
    return false;
  }

  public abstract void createDirectoryStructure(String date) throws IOException;

  public abstract File mutualFundOrderFile(String date) throws IOException;

  protected String directoryStructure(String rootFolderName, String date) {
    return rootFolderName.concat(separator).concat(year()).concat(separator).concat(month()).concat(separator)
        .concat(date);
  }

  private Stream<String> getFileContentsFrom(String bucketName, String fullyQualifiedFileName) {
    S3Object s3Object = amazonS3Client.getObject(new GetObjectRequest(bucketName, fullyQualifiedFileName));
    InputStream fileContents = s3Object.getObjectContent();
    return new BufferedReader(new InputStreamReader(fileContents)).lines();
  }

  protected void processSettlementsFrom(String bucketName, String fullyQualifiedFileName) {
    Stream<String> allConfirmations = getFileContentsFrom(bucketName, fullyQualifiedFileName);
    allConfirmations.forEach(confirmation -> {
      try {
        extractSettlementDetailsFrom(confirmation);
      } catch (NumberFormatException | ParseException e) {
        log.error("Could not extract details from the execution file due to " + e.getMessage()
            + "Kindly fix the file and re-run BOD.");
      }
    });
  }

  public void extractSettlementDetailsFrom(String confirmation) throws NumberFormatException, ParseException {
    String[] confirmationFields = confirmation.split(PipeSeparated);
    mutualFundTradeConfirmationStrategyFactory.getMutualFundTradeConfirmationHandlingStrategyFrom(confirmationFields)
        .extractSettlementDetailsFrom(confirmationFields, actorSelections);
  }

  protected Stream<String> s3ObjectStream(String bucketName) {
    return getObjectslistFromFolder(bucketName, s3PathFor(amazonS3Client, bucketName)).stream();
  }

  private List<String> getObjectslistFromFolder(String bucketName, String folderKey) throws NullPointerException {

    ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix(folderKey);

    List<String> keys = new ArrayList<>();

    ObjectListing objects = amazonS3Client.listObjects(listObjectsRequest);
    List<S3ObjectSummary> summaries = objects.getObjectSummaries();
    if (summaries.size() < 1) {
      throw new NullPointerException("No new objects in the S3 directory !");
    }
    summaries.forEach(s -> keys.add(s.getKey()));
    return keys;
  }

}