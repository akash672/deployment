package com.wt.universe.util;

import static com.google.common.collect.Lists.newArrayList;
import static com.typesafe.config.ConfigFactory.load;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.typesafe.config.Config;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.BPWRealMutualFundInteractive;
import com.wt.ctcl.SYKESRealMutualFundInteractive;
import com.wt.ctcl.VirtualMutualFundInteractive;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerType;
import com.wt.utils.CommonUtilsConfiguration;
import com.wt.utils.akka.SpringExtension;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;

import akka.actor.ActorSystem;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan(basePackages = "com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:environment.properties")
public class MFExecutionServiceAppConfiguration extends CommonUtilsConfiguration {
  private ApplicationContext applicationContext;

  @Autowired
  public MFExecutionServiceAppConfiguration(ApplicationContext applicationContext) {
    super();
    this.applicationContext = applicationContext;
  }
  
  @Autowired
  public SESEmailUtil emailerService;
  
  @Autowired
  @Lazy
  protected WDActorSelections wdActorSelections;
  
  @Autowired
  protected WDProperties wdProperties;


  public void setSystemProperties() {
    System.setProperty("environment", wdProperties.environment());
  }

  
  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
    return properties;
  }

  @Bean(name = "BPWInteractiveManager")
  public InteractiveManager bpwInteractiveManager() throws IOException {
    InteractiveManager interactiveManager = new InteractiveManager(wdProperties.BPW_XNEUTRINO_INTERACTIVE_IP(),
        wdProperties.BPW_XNEUTRINO_INTERACTIVE_PORT());
    return interactiveManager;
  }
  
  @Bean(name = "BPWRealMutualFundInteractive")
  public MutualFundInteractive bpwRealMutualFundInteractive() throws IOException {
    return new BPWRealMutualFundInteractive(wdActorSelections, bpwInteractiveManager(), wdProperties, emailerService, amazonS3());
  }

  @Bean(name = "SYKESInteractiveManager")
  public InteractiveManager sykesInteractiveManager() throws IOException {
    InteractiveManager interactiveManager = new InteractiveManager(wdProperties.SYKES_XNEUTRINO_INTERACTIVE_IP(),
        wdProperties.SYKES_XNEUTRINO_INTERACTIVE_PORT());
    return interactiveManager;
  }
  
  @Bean(name = "SYKESRealMutualFundInteractive")
  public MutualFundInteractive sykesRealMutualFundInteractive() throws IOException {
    return new SYKESRealMutualFundInteractive(wdActorSelections, sykesInteractiveManager(), wdProperties, emailerService, amazonS3());
  }
  
  @Bean( name = "VirtualMutualFundInteractive")
  public MutualFundInteractive virtualMutualFundInteractive(){
    return new VirtualMutualFundInteractive(wdActorSelections);
  }
  
  @Bean
  public List<MutualFundInteractive> listOfMFCTCLs() throws Exception {
    return newArrayList(bpwRealMutualFundInteractive(), sykesRealMutualFundInteractive(), virtualMutualFundInteractive());
  }
  
  @Bean
  public Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation() throws Exception {
    Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation = new HashMap<BrokerType, MutualFundInteractive>();
    listOfMFCTCLs()
    .stream()
    .forEach(ctcl -> brokerTypeWithMFCTCLImplementation.put(ctcl.getBrokerType(), ctcl));
    return brokerTypeWithMFCTCLImplementation;
  }
  
  @Bean
  public Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation() throws Exception {
    return new HashMap<BrokerType, CtclInteractive>();
  }

  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() throws IOException {
    setSystemProperties();
    String config = "MFExecution-application.conf";
    String actorName = "MFExecution";
    if (System.getProperty("appConfig") != null) {
      config = System.getProperty("appConfig");
      actorName = System.getProperty("appConfig").split("-")[0];
    }
    Config conf = load(config);
    ActorSystem system = ActorSystem.create(actorName, conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }
}
