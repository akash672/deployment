package com.wt.domain.write;


import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.Runtime.getRuntime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wt.cloud.monitoring.ServiceStatusReporter;
import com.wt.universe.util.MFExecutionServiceAppConfiguration;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class MFExecutionService {
  private static AnnotationConfigApplicationContext ctx;

  private ActorSystem mfExecutionSystem;
  @SuppressWarnings("unused")
  private ActorRef mfOrderRootActor;
  private ServiceStatusReporter serviceStatusReporter;
  @Value("${service.monitoring.flag}")
  private String serviceMonitoringFlag;

  @Autowired
  public MFExecutionService(ActorSystem ctclSystem, ServiceStatusReporter serviceStatusReporter) {
    super();
    this.mfExecutionSystem = ctclSystem;
    this.serviceStatusReporter = serviceStatusReporter;
  }

  public static void main(String[] args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(MFExecutionServiceAppConfiguration.class);
    MFExecutionService service = ctx.getBean(MFExecutionService.class);
    service.start();
    
  }

  private void start() throws Exception {
   log.info("=======================================");
   log.info("| Starting MF Execution Service       |");
   log.info("=======================================");
    createActors();

    getRuntime().addShutdownHook(new Thread(() -> {
      ctx.close();
    })); 
  }

  public void createActors() {
    mfOrderRootActor = mfExecutionSystem.actorOf(SpringExtProvider.get(mfExecutionSystem).props("MFOrderRootActor"), "mf-order-aggregator");
  }

  @Scheduled(cron = "${service.heartbeat.schedule}", zone = "Asia/Kolkata")
  public void sendServiceStatusHeartBeats() {
    if (serviceMonitoringFlag.equalsIgnoreCase("true"))
      serviceStatusReporter.sendStatusUpdate(this.getClass().getSimpleName());
  }
}
