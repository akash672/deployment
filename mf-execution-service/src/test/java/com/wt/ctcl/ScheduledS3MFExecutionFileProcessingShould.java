package com.wt.ctcl;

import static org.mockito.Mockito.doReturn;

import java.io.File;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.wt.config.utils.WDProperties;
import com.wt.domain.write.commands.MutualFundTradeConfirmation;
import com.wt.utils.S3Writer;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.testkit.javadsl.TestKit;
import io.findify.s3mock.S3Mock;

@RunWith(MockitoJUnitRunner.class)
public class ScheduledS3MFExecutionFileProcessingShould implements S3Writer {
  private static final String MF_EXECUTION_FILE_BUCKET = "bpwealth";
  private static final File settlementFile = new File("mf_executions.txt");
  private AmazonS3 amazonS3;
  private S3Mock s3Mock;
  @Mock
  private WDActorSelections actorSelections;
  @Mock
  private WDProperties wdProperties;
  @Mock
  private SESEmailUtil sesEmailUtil;
  @Mock
  private InteractiveManager interactiveManager;
  private ActorSystem actorSystem = ActorSystem.create("TestSystem");
  private Props props = Props.create(TestableMFOrderRootActor.class, () -> new TestableMFOrderRootActor());
  private ActorRef mfOrderRootActor = actorSystem.actorOf(props);
  private BPWRealMutualFundInteractive bpwMutualFundInteractive;

  @Before
  public void setup() throws Exception {
    amazonS3 = amazonS3();
    createBucket(amazonS3, MF_EXECUTION_FILE_BUCKET);
    String s3Path = s3PathFor(amazonS3, MF_EXECUTION_FILE_BUCKET);
    putDataInS3(amazonS3, MF_EXECUTION_FILE_BUCKET, settlementFile.getName(), s3Path, settlementFile.getAbsoluteFile().getParent());
    doReturn(actorSystem.actorSelection(mfOrderRootActor.path())).when(actorSelections).mforderRootActor();
    bpwMutualFundInteractive = new BPWRealMutualFundInteractive(actorSelections, interactiveManager, wdProperties, sesEmailUtil, amazonS3);
    bpwMutualFundInteractive.scheduleS3ExecutionFileProcessing();
  }

  @Test
  public void send_mf_order_root_actor_trade_details() throws Exception {
    new TestKit(actorSystem) {
      {
        mfOrderRootActor.tell(new GetMutualFundTradeConfirmation(), getRef());
        expectMsgClass(MutualFundTradeConfirmation.class);
      }
    };
  }

  @After
  public void terminate() throws Exception {
    s3Mock.stop();
    actorSystem.terminate();
  }

  private AmazonS3 amazonS3() {
    s3Mock = new S3Mock.Builder()
    		.withPort(8001)
    		.withInMemoryBackend()
    		.build();
    s3Mock.start();
    EndpointConfiguration endpoint = new EndpointConfiguration("http://localhost:8001", "us-east-1");
    return AmazonS3ClientBuilder.standard()
        .withPathStyleAccessEnabled(true)
        .disableChunkedEncoding()
        .withEndpointConfiguration(endpoint)
        .withCredentials(new AWSStaticCredentialsProvider(new AnonymousAWSCredentials())).build();
  }

  static class TestableMFOrderRootActor extends UntypedAbstractActor {
    private MutualFundTradeConfirmation mutualFundTradeConfirmation;

    @Override
    public void onReceive(Object msg) throws Exception {
      if (msg instanceof MutualFundTradeConfirmation) {
        this.mutualFundTradeConfirmation = (MutualFundTradeConfirmation) msg;
      } else if (msg instanceof GetMutualFundTradeConfirmation) {
        sender().tell(mutualFundTradeConfirmation, self());
      }
    }

  }

  static class GetMutualFundTradeConfirmation {

  }

  @Override
  public void putObjectInS3AsObjectMetadata(String bucketName, String s3Path, InputStream orderLines) {
    
  }

}
