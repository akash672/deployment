package com.wt.ctcl;

import static com.wt.domain.BrokerName.BPWEALTH;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.Purpose.CORPORATEACTION;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CorporateEventCTCLInteractive;
import com.wt.domain.BrokerType;
import com.wt.utils.akka.WDActorSelections;

@Service("BPWCorporateEventCTCLReal")
public class BPWCorporateEventCTCLReal extends CorporateEventCTCLInteractive {

  @Autowired
  public BPWCorporateEventCTCLReal(WDActorSelections actorSelections) {
    super(actorSelections);
  }
  
  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(CORPORATEACTION, REAL, BPWEALTH);
  }
  
}
