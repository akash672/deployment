package com.wt.ctcl;

import static com.wt.domain.BrokerName.MOSL;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.Purpose.CORPORATEACTION;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CorporateEventCTCLInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.CtclOrder;
import com.wt.domain.write.commands.BrokerOrderAccepted;
import com.wt.domain.write.commands.ExchangeOrderAccepted;
import com.wt.domain.write.commands.TradeOrderConfirmation;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;

@Service("MOSLCorporateEventCTCLReal")
public class MOSLCorporateEventCTCLReal extends CorporateEventCTCLInteractive {
  
  private WDActorSelections actorSelections;
  
  @Autowired
  public MOSLCorporateEventCTCLReal(WDActorSelections actorSelections) {
    super(actorSelections);
    this.actorSelections = actorSelections;
  }
  
  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(CORPORATEACTION, REAL, MOSL);
  }
  
  @Override
  public void sendMarketCashOrder(CtclOrder order) throws Exception {
    String localOrderId = (order.getLocalOrderId());
    long brokerOrderID = UUID.randomUUID().getMostSignificantBits();
    long exchOrderID = UUID.randomUUID().getMostSignificantBits();
    long tradeOrderID = UUID.randomUUID().getMostSignificantBits();
    long exchOrderTime = DateUtils.currentTimeInMillis();
    actorSelections.equityOrderRootActor().tell(new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
    actorSelections.equityOrderRootActor().tell(new ExchangeOrderAccepted(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime, order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
    actorSelections.equityOrderRootActor().tell(new TradeOrderConfirmation(String.valueOf(exchOrderID), String.valueOf(brokerOrderID), String.valueOf(tradeOrderID) , (float) order.getLimitPrice() ,
        (float) (order.getIntegerQuantity()), exchOrderTime, order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
     
    
  }
  
}
