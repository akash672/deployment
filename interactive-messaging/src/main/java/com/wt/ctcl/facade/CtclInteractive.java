package com.wt.ctcl.facade;

import com.wt.domain.BrokerType;
import com.wt.domain.CancelOrder;
import com.wt.domain.CtclOrder;

public interface CtclInteractive {

  public void sendMarketCashOrder(CtclOrder order) throws Exception;
  
  public void sendLimitCashOrder(CtclOrder order) throws Exception;
  
  public BrokerType getBrokerType();

  public void sendTradeBook(String clientCode, String localOrderId) throws Exception;
  
  public void sendOrderBook(String clientCode, String localOrderID) throws Exception;
  
  public void sendMarginRequest(String clientCode) throws Exception;
  
  public void sendCancelLimitCashOrder(CancelOrder order) throws Exception;
     
  public void heartBeatCheck() throws Exception;
  
 
}
