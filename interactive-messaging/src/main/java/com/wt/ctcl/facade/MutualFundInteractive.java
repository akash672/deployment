package com.wt.ctcl.facade;

import com.wt.domain.BrokerType;
import com.wt.domain.CtclOrder;

public interface MutualFundInteractive {
  
  public void sendMutualFundOrder(CtclOrder ctclOrder);
  
  public void sendMarginRequest(String clientCode) throws Exception;
  
  public BrokerType getBrokerType();
  
  public void scheduleEmailingMutualFundOrderFile();

  public void scheduleS3FolderCreation();

  public void scheduleS3ExecutionFileProcessing();
}
