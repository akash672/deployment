package com.wt.ctcl.facade;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.UUID;

import com.wt.domain.CancelOrder;
import com.wt.domain.CtclOrder;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.BrokerOrderAccepted;
import com.wt.domain.write.commands.ExchangeOrderAccepted;
import com.wt.domain.write.commands.GetPriceForSymbolAndClientCode;
import com.wt.domain.write.commands.TradeOrderConfirmation;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;

public abstract class OnBoardingCTCLInteractive implements CtclInteractive {
  
  private WDActorSelections actorSelections;
  
  public OnBoardingCTCLInteractive(WDActorSelections actorSelections) {
    super();
    this.actorSelections = actorSelections;
  }

  @Override
  public void sendMarketCashOrder(CtclOrder order) throws Exception {
    short localOrderId = new Short(order.getLocalOrderId());
    long brokerOrderID = UUID.randomUUID().getMostSignificantBits();
    long exchOrderID = UUID.randomUUID().getMostSignificantBits();
    long tradeOrderID = UUID.randomUUID().getMostSignificantBits();
    long exchOrderTime = DateUtils.currentTimeInMillis();
    actorSelections.equityOrderRootActor().tell(new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
    actorSelections.equityOrderRootActor().tell(new ExchangeOrderAccepted(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime, order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
    PriceWithPaf priceWithPaf = (PriceWithPaf) askAndWaitForResult(actorSelections.userRootActor(), new GetPriceForSymbolAndClientCode(order.getSymbol(), order.getClientCode()));
    actorSelections.equityOrderRootActor().tell(new TradeOrderConfirmation(String.valueOf(exchOrderID), String.valueOf(brokerOrderID), String.valueOf(tradeOrderID) ,(float) priceWithPaf.getPrice().getPrice(),
        (float) (order.getIntegerQuantity()), (int) exchOrderTime, order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
     
  }

  @Override
  public void heartBeatCheck() {
    
  }

  @Override
  public void sendTradeBook(String clientCode, String localOrderId) throws Exception {
    
  }

  @Override
  public void sendOrderBook(String clientCode, String localOrderId) throws Exception {
    
  }

  @Override
  public void sendLimitCashOrder(CtclOrder order) throws Exception {
    
  }


  @Override
  public void sendMarginRequest(String clientCode) throws Exception {
    
  }

  @Override
  public void sendCancelLimitCashOrder(CancelOrder order) throws Exception {
    
  }

}
