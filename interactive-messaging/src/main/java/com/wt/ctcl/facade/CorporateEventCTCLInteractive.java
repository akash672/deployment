package com.wt.ctcl.facade;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Qualifier;

import com.wt.domain.CancelOrder;
import com.wt.domain.CtclOrder;
import com.wt.domain.write.commands.BrokerOrderAccepted;
import com.wt.domain.write.commands.ExchangeOrderAccepted;
import com.wt.domain.write.commands.TradeOrderConfirmation;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;

public abstract class CorporateEventCTCLInteractive implements CtclInteractive {
  
  
  private WDActorSelections actorSelections;
  
  public CorporateEventCTCLInteractive(@Qualifier("WDActors") WDActorSelections actorSelections) {
    super();
    this.actorSelections = actorSelections;
  }
  
  
  @Override
  public void sendMarketCashOrder(CtclOrder order) throws Exception {
    short localOrderId = new Short(order.getLocalOrderId());
    long brokerOrderID = UUID.randomUUID().getMostSignificantBits();
    long exchOrderID = UUID.randomUUID().getMostSignificantBits();
    long tradeOrderID = UUID.randomUUID().getMostSignificantBits();
    long exchOrderTime = DateUtils.currentTimeInMillis();
    actorSelections.equityOrderRootActor().tell(new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
    actorSelections.equityOrderRootActor().tell(new ExchangeOrderAccepted(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime, order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
    actorSelections.equityOrderRootActor().tell(new TradeOrderConfirmation(String.valueOf(exchOrderID), String.valueOf(brokerOrderID), String.valueOf(tradeOrderID) , (float) order.getLimitPrice() ,
        (float) (order.getIntegerQuantity()), exchOrderTime, order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()), ActorRef.noSender());
     
    
  }

  @Override
  public void sendLimitCashOrder(CtclOrder order) throws Exception {
  }

  @Override
  public void sendTradeBook(String clientCode, String localOrderId) throws Exception {
  }

  @Override
  public void sendOrderBook(String clientCode, String localOrderId) throws Exception {
  }

  @Override
  public void sendMarginRequest(String clientCode) throws Exception {
  }

  @Override
  public void sendCancelLimitCashOrder(CancelOrder order) throws Exception {
  }

  @Override
  public void heartBeatCheck() throws Exception {
    
  }

}
