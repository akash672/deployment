package com.wt.ctcl;

import static com.wt.domain.BrokerName.SYKES;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.Purpose.ONBOARDING;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.OnBoardingCTCLInteractive;
import com.wt.domain.BrokerType;
import com.wt.utils.akka.WDActorSelections;

@Service("SYKESOnboardingCTCLInteractive")
public class SYKESOnboardingCTCLInteractive extends OnBoardingCTCLInteractive {

  @Autowired
  public SYKESOnboardingCTCLInteractive(@Qualifier("WDActors") WDActorSelections actorSelections) {
    super(actorSelections);
  }

  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(ONBOARDING, REAL, SYKES);
  }
}
