package com.wt.ctcl;

import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.utils.NumberUtils.getFirstHalf;
import static com.wt.utils.NumberUtils.getSecondHalf;

import java.io.IOException;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CtclInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.CancelOrder;
import com.wt.domain.CtclOrder;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Trade;
import com.wt.domain.write.commands.BrokerOrderAccepted;
import com.wt.domain.write.commands.BrokerOrderRejected;
import com.wt.domain.write.commands.ExchangeOrderAccepted;
import com.wt.domain.write.commands.ExchangeOrderRejected;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.SendTradeOrderConfirmation;
import com.wt.domain.write.commands.StopLivePriceStream;
import com.wt.domain.write.commands.TradeOrderConfirmation;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.ActorResultWaiter;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import lombok.extern.log4j.Log4j;

@Service("CTCLMock")
@Log4j
public class CTCLMock implements CtclInteractive {

  private WDActorSelections wdActorSelections;
  private Props props = Props.create(PriceGetter.class, () -> new PriceGetter());
  private ActorRef priceGetterActor;
  private int counter = 1;

  @Autowired
  public CTCLMock(ActorSystem system, WDActorSelections wdActorSelections) {
    this.wdActorSelections = wdActorSelections;
    priceGetterActor = system.actorOf(props);
  }

  @Override
  public void sendMarketCashOrder(CtclOrder order) {
    Random random = new Random();
    short localOrderId = new Short(order.getLocalOrderId());
    long brokerOrderID = random.nextLong();
    long exchOrderID = random.nextLong();
    long exchOrderTime = DateUtils.currentTimeInMillis();
    String message = UUID.randomUUID().toString();
    if (order.getSymbol() == "XYZ") {
      wdActorSelections.equityOrderRootActor().tell(
          new BrokerOrderRejected(String.valueOf(localOrderId), String.valueOf(brokerOrderID), message, order.getClientCode(),
              order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
          ActorRef.noSender());
      return;
    } else if (order.getSymbol() == "ABC") {
      wdActorSelections.equityOrderRootActor().tell(
          new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(),
              order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
          ActorRef.noSender());

      wdActorSelections.equityOrderRootActor().tell(
          new ExchangeOrderRejected(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime,
              order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide(), message),
          ActorRef.noSender());
      return;
    } else {
      if (order.getSymbol() == "PQR") {
        wdActorSelections.equityOrderRootActor().tell(
            new ExchangeOrderAccepted(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime,
                order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
            ActorRef.noSender());
        priceGetterActor.tell(new SendTradeOrderConfirmation(order, brokerOrderID, exchOrderID), ActorRef.noSender());
      } else if (order.getSymbol() == "UVW") {
        priceGetterActor.tell(new SendTradeOrderConfirmation(order, brokerOrderID, exchOrderID), ActorRef.noSender());
      } else if (order.getSymbol() == "DEF") {
        wdActorSelections.equityOrderRootActor().tell(
            new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(),
                order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
            ActorRef.noSender());
        priceGetterActor.tell(new SendTradeOrderConfirmation(order, brokerOrderID, exchOrderID), ActorRef.noSender());
      } else if (order.getSymbol() == "LMN") {
        wdActorSelections.equityOrderRootActor().tell(
            new ExchangeOrderRejected(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime,
                order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide(), message),
            ActorRef.noSender());
      } else if (order.getSymbol() == "HIJ") {
        doNothing();
      } else if (order.getSymbol() == "KLM") {
        if (order.getSide() == BUY) {
          wdActorSelections.equityOrderRootActor().tell(
              new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(),
                  order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
              ActorRef.noSender());

          wdActorSelections.equityOrderRootActor().tell(
              new ExchangeOrderAccepted(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime,
                  order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
              ActorRef.noSender());
          wdActorSelections.equityOrderRootActor().tell(
              new TradeOrderConfirmation(String.valueOf(exchOrderID), String.valueOf(brokerOrderID), String.valueOf(random.nextLong()), (float) 300.0,
                  (float) order.getIntegerQuantity(), exchOrderTime, order.getClientCode(),
                  order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
              ActorRef.noSender());
        } else {
          wdActorSelections.equityOrderRootActor().tell(
              new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(),
                  order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
              ActorRef.noSender());

          wdActorSelections.equityOrderRootActor().tell(
              new ExchangeOrderAccepted(order.getClientCode(), String.valueOf(brokerOrderID), String.valueOf(exchOrderID), exchOrderTime,
                  order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
              ActorRef.noSender());
          if (counter == 1) {
            wdActorSelections.equityOrderRootActor().tell(
                new TradeOrderConfirmation(String.valueOf(exchOrderID), String.valueOf(brokerOrderID), String.valueOf(random.nextLong()), (float) 320.0,
                    (float) order.getIntegerQuantity(), exchOrderTime, order.getClientCode(),
                    order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
                ActorRef.noSender());
            counter += 1;
          } else {
            wdActorSelections.equityOrderRootActor().tell(
                new TradeOrderConfirmation(String.valueOf(exchOrderID), String.valueOf(brokerOrderID), String.valueOf(random.nextLong()), (float) 170.0,
                    (float) order.getIntegerQuantity(), exchOrderTime, order.getClientCode(),
                    order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
                ActorRef.noSender());
            counter = 1;
          }
        }
      } else {
        wdActorSelections.equityOrderRootActor().tell(
            new BrokerOrderAccepted(String.valueOf(localOrderId), String.valueOf(brokerOrderID), order.getClientCode(),
                order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(), order.getSide()),
            ActorRef.noSender());
       
        priceGetterActor.tell(new SendTradeOrderConfirmation(order, brokerOrderID, exchOrderID), ActorRef.noSender());
      }
    }
  }

  public class PriceGetter extends UntypedAbstractActor {

    @Override
    public void onReceive(Object message) throws Throwable {
      if (message instanceof SendTradeOrderConfirmation) {
        SendTradeOrderConfirmation confirmation = (SendTradeOrderConfirmation) message;
        
        String ticker = confirmation.getOrder().getOrderExecutionMetaData().getToken() + "-"
            + confirmation.getOrder().getExchange() + "-" + "EQ";
        
        String wdId = confirmation.getOrder().getOrderExecutionMetaData().getWdId();
         
        double executionPrice = confirmation.getOrder().getOrderExecutionMetaData().getLimitPrice();
        if(executionPrice != 0){
          PriceWithPaf price = new PriceWithPaf(new InstrumentPrice(executionPrice ,
              DateUtils.currentTimeInMillis(), ticker),Pafs.withCumulativePaf(1.));
          
          wdActorSelections.equityOrderRootActor().tell(
              new ExchangeOrderAccepted(confirmation.getOrder().getClientCode(), String.valueOf(confirmation.getBrokerOrderId()), String.valueOf(confirmation.getExchangeOrderId()), DateUtils.currentTimeInMillis(),
                  confirmation.getOrder().getOrderExecutionMetaData().getToken(), confirmation.getOrder().getIntegerQuantity(), confirmation.getOrder().getSide()),
              ActorRef.noSender());
         
          sendTradeExecutionMessages(price, confirmation.getExchangeOrderId(), confirmation.getBrokerOrderId(),
              confirmation.getOrder(), wdActorSelections);
        }else{
          
          try {
          PriceWithPaf priceWithPaf = (PriceWithPaf) ActorResultWaiter.askAndWaitForResult(wdActorSelections.universeRootActor(),
                new GetLivePriceStream(wdId));
          
            
            wdActorSelections.equityOrderRootActor().tell(
                new ExchangeOrderAccepted(confirmation.getOrder().getClientCode(), String.valueOf(confirmation.getBrokerOrderId()), String.valueOf(confirmation.getExchangeOrderId()), DateUtils.currentTimeInMillis(),
                    confirmation.getOrder().getOrderExecutionMetaData().getToken(), confirmation.getOrder().getIntegerQuantity(), confirmation.getOrder().getSide()),
                ActorRef.noSender());
            
            sendTradeExecutionMessages(priceWithPaf, confirmation.getExchangeOrderId(), confirmation.getBrokerOrderId(),
                confirmation.getOrder(), wdActorSelections);
            
          } catch (Exception e) {
            log.error("Price not received " + e.getMessage());
            
            wdActorSelections.equityOrderRootActor().tell(new ExchangeOrderRejected(confirmation.getOrder().getClientCode(), String.valueOf(confirmation.getBrokerOrderId()), String.valueOf(confirmation.getExchangeOrderId()), DateUtils.currentTimeInMillis(),
                confirmation.getOrder().getOrderExecutionMetaData().getToken(), confirmation.getOrder().getIntegerQuantity(), confirmation.getOrder().getSide(),
                "The advise could not be honored as the system failed to fetch its current market price"), ActorRef.noSender());
          }
          
          wdActorSelections.universeRootActor()
              .tell(new StopLivePriceStream(confirmation.getOrder().getOrderExecutionMetaData().getToken()), self());
        }
       
      }
    }

  }

  public void sendTradeExecutionMessages(PriceWithPaf price, Long exchangeOrderId, Long brokerOrderId, CtclOrder order,
      WDActorSelections wdActorSelections) {
    Random random = new Random();
    if (order.getIntegerQuantity() != 1) {
      PriceWithPaf price2 = new PriceWithPaf(new InstrumentPrice(price.getPrice().getPrice() + .25,
          price.getPrice().getTime(), price.getPrice().getWdId()), Pafs.withCumulativePaf(price.getCumulativePaf()));

      Trade trade1 = new Trade(price.getPrice().getPrice(), order.getSide(), getFirstHalf(order.getIntegerQuantity()),
          Long.toString(exchangeOrderId), UUID.randomUUID().toString());

      Trade trade2 = new Trade(price2.getPrice().getPrice(), order.getSide(), getSecondHalf(order.getIntegerQuantity()),
          Long.toString(exchangeOrderId), UUID.randomUUID().toString());
      wdActorSelections.equityOrderRootActor()
          .tell(new TradeOrderConfirmation(String.valueOf(exchangeOrderId), String.valueOf(brokerOrderId), String.valueOf(random.nextLong()),
              (float) price.getPrice().getPrice(), (float) trade1.getQuantity(), DateUtils.currentTimeInMillis(),
              order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(),
              order.getSide()), ActorRef.noSender());
      wdActorSelections.equityOrderRootActor()
          .tell(new TradeOrderConfirmation(String.valueOf(exchangeOrderId), String.valueOf(brokerOrderId), String.valueOf(random.nextLong()),
              (float) price2.getPrice().getPrice(), (float) trade2.getQuantity(), random.nextInt(),
              order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(),
              order.getSide()), ActorRef.noSender());
    } else {
      Trade trade1 = new Trade(price.getPrice().getPrice(), order.getSide(), 1, Long.toString(exchangeOrderId),
          UUID.randomUUID().toString());

      wdActorSelections.equityOrderRootActor()
          .tell(new TradeOrderConfirmation(String.valueOf(exchangeOrderId), String.valueOf(brokerOrderId), String.valueOf(random.nextLong()),
              (float) price.getPrice().getPrice(), (float) trade1.getQuantity(), random.nextInt(),
              order.getClientCode(), order.getOrderExecutionMetaData().getToken(), order.getIntegerQuantity(),
              order.getSide()), ActorRef.noSender());
    }

  }

  private void doNothing() {

  }

  @Override
  public void sendTradeBook(String clientCode, String localOrderId) throws IOException {

  }

  @Override
  public void sendOrderBook(String clientCode, String localOrderId) throws IOException {

  }

  @Override
  public void heartBeatCheck() {

  }

  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(EQUITYSUBSCRIPTION, VIRTUAL, MOCK);
  }

  @Override
  public void sendLimitCashOrder(CtclOrder order) throws Exception {
  }

  @Override
  public void sendMarginRequest(String clientCode) throws Exception {
  }

  @Override
  public void sendCancelLimitCashOrder(CancelOrder order) throws Exception {
  }

}
