package com.wt.ctcl;

import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static com.wt.domain.write.commands.MutualFundTradeConfirmation.allotmentConfirmation;
import static com.wt.domain.write.commands.MutualFundTradeConfirmation.allotmentRejection;
import static com.wt.domain.write.commands.MutualFundTradeConfirmation.redemptionConfirmation;
import static com.wt.domain.write.commands.MutualFundTradeConfirmation.redemptionRejection;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.CtclOrder;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.GetPrice;
import com.wt.domain.write.commands.ReceivedByDealer;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;

@Service("VirtualMutualFundInteractive")
public class VirtualMutualFundInteractive implements MutualFundInteractive {

  private WDActorSelections actorSelections;

  @Autowired
  public VirtualMutualFundInteractive(WDActorSelections actorSelections) {
    super();
    this.actorSelections = actorSelections;
  }

  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(MUTUALFUNDSUBSCRIPTION, VIRTUAL, MOCK);
  }

  @Override
  public void sendMutualFundOrder(CtclOrder order) {
    long exchangeOrderID = UUID.randomUUID().getMostSignificantBits();
    long folioNumber = (order.getFolioNumber().equals("0")) ? UUID.randomUUID().getMostSignificantBits()
        : Long.valueOf(order.getFolioNumber());
    long exchOrderTime = DateUtils.currentTimeInMillis();
    String isin = UUID.randomUUID().toString();
    String uniqueReferenceNumber = order.getLocalOrderId();

    actorSelections.mforderRootActor().tell(new ReceivedByDealer(order.getTxCode(), order.getLocalOrderId(),
        exchangeOrderID, 12345, "12345", order.getClientCode(), "OrderSuccessfully placed", "1"), ActorRef.noSender());
    PriceWithPaf priceWithPaf;
    try {
      priceWithPaf = (PriceWithPaf) askAndWaitForResult(actorSelections.universeRootActor(),
          new GetPrice((order.getToken() + "-MF")));
      if(order.getSide() == BUY) {
      double quantity = (order.getAllocatedAmount() / priceWithPaf.getPrice().getPrice());
      quantity = DoublesUtil.round(quantity, 4);
      if (order.getSchemeCode().equals("AXIS-DP"))
        actorSelections.mforderRootActor().tell(allotmentRejection(uniqueReferenceNumber, String.valueOf(folioNumber), isin, exchOrderTime,
            order.getClientCode(), order.getSchemeCode(), priceWithPaf.getPrice().getPrice(), quantity,
            "Order rejected"), ActorRef.noSender());
      else {
        actorSelections.mforderRootActor()
            .tell(allotmentConfirmation(uniqueReferenceNumber, String.valueOf(folioNumber), isin, exchOrderTime,
                order.getClientCode(), order.getSchemeCode(), priceWithPaf.getPrice().getPrice(), quantity,
                "Order confirmed"), ActorRef.noSender());
      }
      }else {
        double quantity = order.getQuantity();
        if (order.getSchemeCode().equals("AXIS-DP"))
          actorSelections.mforderRootActor().tell(redemptionRejection(uniqueReferenceNumber, String.valueOf(folioNumber), isin, exchOrderTime,
              order.getClientCode(), order.getSchemeCode(), (float)0.0, (0.0 * quantity),
              "Order rejected"), ActorRef.noSender());
        else 
          actorSelections.mforderRootActor()
              .tell(redemptionConfirmation(uniqueReferenceNumber, String.valueOf(folioNumber), isin, exchOrderTime,
                  order.getClientCode(), order.getSchemeCode(),priceWithPaf.getPrice().getPrice(), (priceWithPaf.getPrice().getPrice() * quantity),
                  "Order confirmed"), ActorRef.noSender());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void sendMarginRequest(String clientCode) throws Exception {

  }

  @Override
  public void scheduleEmailingMutualFundOrderFile() {
    
  }

  @Override
  public void scheduleS3FolderCreation() {
    
  }

  @Override
  public void scheduleS3ExecutionFileProcessing() {
    
  }

}