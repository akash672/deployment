package com.wt.ctcl;

import static com.wt.domain.BrokerName.SYKES;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.CORPORATEACTION;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CorporateEventCTCLInteractive;
import com.wt.domain.BrokerType;
import com.wt.utils.akka.WDActorSelections;

@Service("SYKESCorporateEventCTCLInteractive")
public class SYKESCorporateEventCTCLInteractive extends CorporateEventCTCLInteractive {

  @Autowired
  public SYKESCorporateEventCTCLInteractive(WDActorSelections actorSelections) {
    super(actorSelections);
  }
  
  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(CORPORATEACTION, VIRTUAL, SYKES);
  }
  
}
