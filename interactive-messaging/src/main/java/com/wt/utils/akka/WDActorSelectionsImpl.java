package com.wt.utils.akka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;

import akka.actor.ActorSelection;
import akka.actor.ActorSystem;

@Service("WDActors")
public class WDActorSelectionsImpl implements WDActorSelections {
  private ActorSystem system;
  private WDProperties wdProperties;

  @Autowired
  public WDActorSelectionsImpl(ActorSystem system, WDProperties wdProperties) {
    this.system = system;
    this.wdProperties = wdProperties;
  }

  @Override
  public ActorSelection advisersRootActor() {
    return actorRef(wdProperties.WD_ACTOR_SYSTEM_ADVISER_ROOT_ACTOR());
  }

  @Override
  public ActorSelection universeRootActor()  {
    return actorRef(wdProperties.UNIVERSE_ACTOR_SYSTEM_UNIVERSE_ROOT_ACTOR());
  }

  @Override
  public ActorSelection userRootActor()  {
    return actorRef(wdProperties.WD_ACTOR_SYSTEM_USER_ROOT_ACTOR());
  }

  @Override
  public ActorSelection equityOrderRootActor() {
    return actorRef(wdProperties.EQUITY_CTCL_ACTOR_SYSTEM_ORDER_ROOT_ACTOR());
  }
  
  @Override
  public ActorSelection mforderRootActor() {
    return actorRef(wdProperties.MF_EXECUTION_ACTOR_SYSTEM_MF_ORDER_ROOT_ACTOR());
  }
  
  private ActorSelection actorRef(String selection) {
    return system.actorSelection(selection);
  }

  @Override
  public ActorSelection realtimeAdviserFundPerformanceRootActor() {
    return actorRef(wdProperties.ADVISER_REALTIME_PERFORMANCE_ROOT_ACTOR());
  }

  @Override
  public ActorSelection realtimeInvestorPerformanceRootActor() {
    return actorRef(wdProperties.USER_REALTIME_PERFORMANCE_ROOT_ACTOR());
  }

  @Override
  public ActorSelection realtimeMarketPriceRootActor() {
    return actorRef(wdProperties.REALTIME_MARKET_PRICE_ROOT_ACTOR());
  }
  
  @Override
  public ActorSelection activeInstrumentTracker() {
    return actorRef(wdProperties.WD_ACTIVE_INSTRUMENT_TRACKER_ACTOR());
  }
  
  @Override
  public ActorSelection rdsFacadeRootActor() {
    return actorRef(wdProperties.RDS_FACADE_ROOT_ACTOR());
  }

}

