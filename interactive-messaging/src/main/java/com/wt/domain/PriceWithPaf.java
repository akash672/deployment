package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.write.events.AssetClassEvent;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.DateUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper=false)
@ToString
@Setter
@DynamoDBDocument
public class PriceWithPaf extends AssetClassEvent implements Serializable, Cloneable {
  
  private static final long serialVersionUID = 1L;
  private InstrumentPrice price;
  @Getter private Pafs pafs;
  
  @JsonCreator
  public PriceWithPaf(@JsonProperty("price") InstrumentPrice price, @JsonProperty("pafs") Pafs pafs) {
    this.price = price;
    this.pafs = pafs;
  }
  
  public PriceWithPaf() {
    super();
    this.pafs = Pafs.withCumulativePaf(1.);
  }
  
  public static PriceWithPaf noPrice(String token) {
    return new PriceWithPaf(new InstrumentPrice(0, 0, token), Pafs.withCumulativePaf(1.));
  }

  @Override
  public PriceWithPaf clone() {
    InstrumentPrice clonePrice = price == null ? null : price.clone();
    return new PriceWithPaf(clonePrice, pafs.clone());
  }

  @JsonIgnore
  public PriceWithPaf withPaf(Pafs paf) {
    return new PriceWithPaf(price, paf);
  }

  public PriceWithPaf withNewPrice(double price) {
    return new PriceWithPaf(this.price.withPrice(price), pafs);
  }
  
  public PriceWithPaf withNewPriceTime(double price, long time) {
    return new PriceWithPaf(this.price.withPriceTime(price, time), pafs);
  }

  public static PriceWithPaf withPrice(double price, String wdId, Pafs pafs) {
    return new PriceWithPaf(new InstrumentPrice(price, DateUtils.currentTimeInMillis(), wdId),
        pafs);
  }
  
  @JsonIgnore
  public double performanceWith(PriceWithPaf exit) {
    return (exit.getAdjusted() - getAdjusted()) / getAdjusted();
  }
  @JsonIgnore @DynamoDBIgnore
  public double getAdjusted() {
    if (pafs != null)
      return price.getPrice() * pafs.getCumulativePaf();
    throw new IllegalStateException("Paf is not known");
  }

  public void validate() {
    if (price.getPrice() <= 0.) {
      throw new IllegalArgumentException("Price should be non zero only");
    }
  }

  @JsonIgnore @DynamoDBIgnore
  public double getCumulativePaf() {
    return pafs.getCumulativePaf();
  }
  
  @JsonIgnore @DynamoDBIgnore
  public double getDividend() {
    return pafs.getDividend();
  }

  @JsonIgnore @DynamoDBIgnore
  public double getNonDividendPaf() {
    return pafs.getNonDividendPaf();
  }
  
  @DynamoDBAttribute
  public InstrumentPrice getPrice() {
    return price;
  }

  @JsonIgnore @DynamoDBIgnore
  public double getHistoricalPaf() {
    return pafs.getHistoricalPaf();
  }

}
