package com.wt.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.wt.domain.write.events.InstrumentPrice;

public class InstrumentPriceConverter implements DynamoDBTypeConverter<String, InstrumentPrice> {
  @Override
  public String convert(InstrumentPrice object) {
    return new Gson().toJson(object);
  }

  @Override
  public InstrumentPrice unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<InstrumentPrice>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }

}
