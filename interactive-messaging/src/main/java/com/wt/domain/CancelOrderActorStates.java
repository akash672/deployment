package com.wt.domain;

import akka.persistence.fsm.PersistentFSM.FSMState;
import lombok.Getter;

public enum CancelOrderActorStates implements FSMState {

  CancelOrderBeingGenerated("CancelOrderBeingGenerated"),
  CancelOrderGenerated("CancelOrderGenerated"),
  CancelOrderReceivedbyBroker("CancelOrderReceivedbyBroker"), 
  CancellationConfirmed("CancellationConfirmed"), 
  CancelOrderRejectedByBroker("CancelOrderRejectedByBroker"), 
  CancelOrderRejectedByExchange("CancelOrderRejectedByExchange"); 

  @Getter
  private final String stateIdentifier;

  private CancelOrderActorStates(String stateIdentifier) {
    this.stateIdentifier = stateIdentifier;
  }

  @Override
  public String identifier() {
    return stateIdentifier;
  }
}