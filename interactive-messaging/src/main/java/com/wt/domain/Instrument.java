package com.wt.domain;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString(of = "wdId")
@EqualsAndHashCode(of="wdId")
public class Instrument implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter 
  @Setter
  private transient String wdId;
  @Getter
  @Setter
  private transient String segment;
  @Getter
  @Setter
  private transient String token;
  @Getter
  @Setter
  private transient String symbol;
}
