package com.wt.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class DestinationAddress implements Serializable {

  private static final long serialVersionUID = 1L;
  private String senderPath;

  public DestinationAddress(String senderPath) {
    super();
    this.senderPath = senderPath;
  }

}
