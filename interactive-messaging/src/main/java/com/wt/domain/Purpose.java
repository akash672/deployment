package com.wt.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Purpose {
  NONE("NONE"),
  EQUITYSUBSCRIPTION("EQUITYSUBSCRIPTION"),
  PARTIALWITHDRAWAL("PARTIALWITHDRAWAL"),
  UNSUBSCRIPTION("UNSUBSCRIPTION"),
  ONBOARDING("ONBOARDING"),
  CORPORATEACTION("CORPORATEACTION"),
  MUTUALFUNDSUBSCRIPTION("MUTUALFUNDSUBSCRIPTION");
  
  @Getter private final String purpose;
}
