package com.wt.domain;

import akka.persistence.fsm.PersistentFSM.FSMState;
import lombok.Getter;

public enum OrderActorStates implements FSMState {

  OrderBeingGenerated("OrderBeingGenerated"),
  OrderGenerated("OrderGenerated"),
  OrderReceivedbyBroker("OrderReceivedbyBroker"), 
  OrderSenttoExchange("OrderSenttoExchange"), 
  OrderRejectedByBroker("OrderRejectedByBroker"), 
  OrderRejectedByExchange("OrderRejectedByExchange"), 
  PartiallyExecuted("PartiallyExecuted"), 
  FullyExecuted("FullyExecuted"), 
  CancellationOrderPlaced("CancellationOrderPlaced"),
  Cancelled("Cancelled");

  @Getter
  private final String stateIdentifier;

  private OrderActorStates(String stateIdentifier) {
    this.stateIdentifier = stateIdentifier;
  }

  @Override
  public String identifier() {
    return stateIdentifier;
  }
}