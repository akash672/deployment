package com.wt.domain;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.wt.ctcl.facade.CtclInteractive;

@Component
public class CTCLInteractiveFactory {

  @Resource
  private Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation;

  public CtclInteractive getCTCL(BrokerType brokerType) throws IOException {
    return brokerTypeWithCTCLImplementation.get(brokerType);
  }

}
