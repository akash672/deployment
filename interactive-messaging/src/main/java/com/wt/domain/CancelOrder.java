package com.wt.domain;

import java.io.Serializable;

import javax.persistence.Entity;

import com.wt.domain.write.events.CancelOrderEvent;
import com.wt.domain.write.events.CancelOrderGenerationInProcess;
import com.wt.domain.write.events.CancelOrderReceivedByBroker;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode
public class CancelOrder implements Serializable{

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String localOrderId;
  private String exchangeOrderId;
  private OrderExecutionMetaData orderExecutionMetaData;
  private String brokerOrderId;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private String exchange;
  
  
  public void update(CancelOrderEvent event) {
    
    if(event instanceof CancelOrderGenerationInProcess){
      CancelOrderGenerationInProcess cancelOrderGenerationInProcess = (CancelOrderGenerationInProcess) event;
      this.clientCode = cancelOrderGenerationInProcess.getClientCode();
      this.localOrderId = cancelOrderGenerationInProcess.getLocalOrderId();
      this.exchangeOrderId = cancelOrderGenerationInProcess.getExchangeOrderId();
      this.orderExecutionMetaData = cancelOrderGenerationInProcess.getOrderExecutionMetaData();
      this.brokerName = cancelOrderGenerationInProcess.getBrokerName();
      this.purpose = cancelOrderGenerationInProcess.getPurpose();
      this.investingMode = cancelOrderGenerationInProcess.getInvestingMode();
      this.exchange = cancelOrderGenerationInProcess.getExchange();
    }
    
    if(event instanceof CancelOrderReceivedByBroker){
      CancelOrderReceivedByBroker cancelOrderReceivedByBroker = (CancelOrderReceivedByBroker) event;
      this.brokerOrderId = cancelOrderReceivedByBroker.getBrokerOrderId();
    }
    
  }
  
  
  
}
