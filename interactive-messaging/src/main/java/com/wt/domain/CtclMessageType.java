package com.wt.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class CtclMessageType extends MessageType {

  private static final long serialVersionUID = 1L;
  private String localOrderID;
  private String brokerOrderID;
  private String token;
  private int quantity;
  private OrderSide orderSide;
  
  public CtclMessageType(String localOrderID, String brokerOrderID, String clientCode, String token,
      int quantity, OrderSide orderSide) {
    super(clientCode);
    this.localOrderID = localOrderID;
    this.brokerOrderID = brokerOrderID;
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
  }
  
}
