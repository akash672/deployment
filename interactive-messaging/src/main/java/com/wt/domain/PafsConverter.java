package com.wt.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.wt.domain.write.events.Pafs;

public class PafsConverter  implements DynamoDBTypeConverter<String, Pafs> {
  @Override
  public String convert(Pafs object) {
    return new Gson().toJson(object);
  }

  @Override
  public Pafs unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Pafs>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }

}
