package com.wt.domain;

import com.wt.ctcl.facade.CtclInteractive;

public enum CtclOrderType{
  STOP_LIMIT((short) 3) {
    @Override
    public void sendOrder(CtclInteractive ctclInteractive, CtclOrder orderData) {
      
    }

    @Override
    public void sendCancelOrder(CtclInteractive ctclInteractive, CancelOrder orderData) throws Exception {
      
    }
  }, 
  LIMIT((short) 2) {
    @Override
    public void sendOrder(CtclInteractive ctclInteractive, CtclOrder orderData) throws Exception {
      ctclInteractive.sendLimitCashOrder(orderData);
    }

    @Override
    public void sendCancelOrder(CtclInteractive ctclInteractive, CancelOrder orderData) throws Exception {
      
    }
  },
  CANCEL((short) 2) {
    @Override
    public void sendCancelOrder(CtclInteractive ctclInteractive, CancelOrder orderData) throws Exception {
      ctclInteractive.sendCancelLimitCashOrder(orderData);
    }

    @Override
    public void sendOrder(CtclInteractive ctclInteractive, CtclOrder orderData) throws Exception {
      
    }
  }, 
  MARKET((short) 1) {
    @Override
    public void sendOrder(CtclInteractive ctclInteractive, CtclOrder orderData) throws Exception {
      ctclInteractive.sendMarketCashOrder(orderData);
    }

    @Override
    public void sendCancelOrder(CtclInteractive ctclInteractive, CancelOrder orderData) throws Exception {
      
    }
  }, 
  SMART((short) 4) {
    @Override
    public void sendOrder(CtclInteractive ctclInteractive, CtclOrder orderData) throws Exception {
    }

    @Override
    public void sendCancelOrder(CtclInteractive ctclInteractive, CancelOrder orderData) throws Exception {
      
    }
  };

  private short id;

  CtclOrderType(short id) {
    this.id = id;
  }

  public short getID() {
    return id;
  }
  
  public abstract void sendOrder(CtclInteractive ctclInteractive, CtclOrder orderData) throws Exception;
  public abstract void sendCancelOrder(CtclInteractive ctclInteractive, CancelOrder orderData) throws Exception;


}
