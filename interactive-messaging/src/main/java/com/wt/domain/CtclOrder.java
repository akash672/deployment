package com.wt.domain;

public abstract class CtclOrder {
  public abstract String getFolioNumber();
  public abstract String getLocalOrderId();
  public abstract void setLocalOrderId(String localOrderId);
  public abstract BrokerName getBrokerName();
  public abstract String getClientCode();
  public abstract String getSymbol();
  public abstract String getToken();
  public abstract String getSchemeName();
  public abstract String getSchemeType();
  public abstract String getTxCode();
  public abstract double getQuantity();
  public abstract int getIntegerQuantity();
  public abstract void setIntegerQuantity(int quantity);
  public abstract OrderExecutionMetaData getOrderExecutionMetaData();
  public abstract OrderSide getSide();
  public abstract String getOrderSideType();
  public abstract String getSchemeCode();
  public abstract double getAllocatedAmount();
  public abstract String getAllRedeem();
  public abstract String getExchange();
  public abstract double getLimitPrice();
  public abstract String getClientOrderId();
  public abstract void setOrderExecutionMetaData(OrderExecutionMetaData orderInstrument);
  public abstract void setSide(OrderSide buy);
  public abstract void setClientCode(String string);
}