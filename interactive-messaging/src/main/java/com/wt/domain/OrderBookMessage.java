package com.wt.domain;

import lombok.Getter;

@Getter
public class OrderBookMessage extends MessageType {
  
  private static final long serialVersionUID = 1L;
  private short localOrderId;
  private long brokerOrderId;
  private long exchangeOrderId;
  private long exchangeOrderTime;
  private String token;
  private int quantity;
  private OrderSide orderSide;
  private String message;
  
  
  public OrderBookMessage(String clientCode, short localOrderId, long brokerOrderId, 
      long exchangeOrderId, long exchangeOrderTime,
      String token, int quantity,
      OrderSide orderSide, String message) {
    super(clientCode);
    this.localOrderId = localOrderId;
    this.brokerOrderId = brokerOrderId;
    this.exchangeOrderId = exchangeOrderId;
    this.exchangeOrderTime = exchangeOrderTime; 
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
    this.message = message;
  }
  
  
}
