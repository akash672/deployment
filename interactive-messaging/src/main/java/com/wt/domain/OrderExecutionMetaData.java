package com.wt.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OrderExecutionMetaData implements Serializable {

  private static final long serialVersionUID = 1L;
  private String symbol;
  private String token;
  private String wdId;
  private OrderSide orderSide;
  private double quantity;
  private double limitPrice;
  private double triggerPrice;
  private OrderType orderType;
  private OrderSegment orderSegment;
  private String basketOrderCompositeId;

  public static class OrderExecutionMetaDataBuilder {
    private String symbol;
    private String token;
    private String wdId;;
    private OrderSide orderSide;
    private double quantity;
    private double limitPrice;
    private double triggerPrice;
    private OrderType orderType;
    private OrderSegment orderSegment;
    private String basketOrderCompositeId;

    public OrderExecutionMetaDataBuilder(String token, OrderSide orderSide, double quantity, OrderType orderType, OrderSegment orderSegment) {
      super();
      this.token = token;
      this.orderSide = orderSide;
      this.quantity = quantity;
      this.orderType = orderType;
      this.orderSegment = orderSegment;
    }

    public OrderExecutionMetaDataBuilder withLimitPrice(double limitPrice) {
      this.limitPrice = limitPrice;
      return this;
    }

    public OrderExecutionMetaDataBuilder withTriggerPrice(double triggerPrice) {
      this.triggerPrice = triggerPrice;
      return this;
    }
    
    public OrderExecutionMetaDataBuilder withSymbol(String symbol) {
      this.symbol = symbol;
      return this;
    }

    public OrderExecutionMetaDataBuilder withbasketOrderCompositeId(String basketOrderCompositeId) {
      this.basketOrderCompositeId = basketOrderCompositeId;
      return this;
    }
    
    public OrderExecutionMetaDataBuilder withWdId(String wdId) {
      this.wdId = wdId;
      return this;
    }

    public OrderExecutionMetaData build() {
      return new OrderExecutionMetaData(this);
    }
  }

  private OrderExecutionMetaData(OrderExecutionMetaDataBuilder builder) {
    this.symbol = builder.symbol;
    this.token = builder.token;
    this.wdId = builder.wdId;
    this.quantity = builder.quantity;
    this.orderSide = builder.orderSide;
    this.orderType = builder.orderType;
    this.orderSegment = builder.orderSegment;
    this.limitPrice = builder.limitPrice;
    this.triggerPrice = builder.triggerPrice;
    this.basketOrderCompositeId = builder.basketOrderCompositeId;
  }
  
  public int getIntegerQuantity() {
    return (int)getQuantity();
  }
  
}
