package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class HasCorporateActionOrderBeenReceivedQuery extends OrderRootCommand {

  private static final long serialVersionUID = 1L;
  @Getter private String clientOrderId;
}
