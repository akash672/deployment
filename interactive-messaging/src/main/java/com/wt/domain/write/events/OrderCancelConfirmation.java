package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderCancelConfirmation extends OrderEvent{
 
  private static final long serialVersionUID = 1L;

}
