package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=true, of = {"exchangeOrderId"})
@Getter
public class OrderRejectedByBSEStar extends ExchangeOrderUpdateEvent {
  
  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  private String message;
  public OrderRejectedByBSEStar(DestinationAddress destinationAddress, String exchangeOrderId, String message) {
    super(destinationAddress);
    this.exchangeOrderId = exchangeOrderId;
    this.message = message;
  }

}
