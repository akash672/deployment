package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CancelConfirmed extends OrderEvent{
  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  private String brokerOrderId;
  
}
