package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class MessageReceptionConfirmation implements Serializable {

  private static final long serialVersionUID = 1L;
  private long deliveryId;

}
