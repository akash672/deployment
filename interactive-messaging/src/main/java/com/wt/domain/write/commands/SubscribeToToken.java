package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class SubscribeToToken extends AssetClassCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  @Getter private String ticker;
  @Getter
  @Setter
  private String token;
  
  public SubscribeToToken(String ticker) {
    super();
    this.ticker = ticker;
  }
  
  
}
