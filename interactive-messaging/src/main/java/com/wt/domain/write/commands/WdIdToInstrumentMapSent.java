package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.Map;

import com.wt.domain.EquityInstrument;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class WdIdToInstrumentMapSent implements Serializable {

  private static final long serialVersionUID = 1L;
  private Map<String,EquityInstrument> WdIdtoInstruments;
  
  public WdIdToInstrumentMapSent(Map<String, EquityInstrument> wdIdtoInstruments) {
    super();
    WdIdtoInstruments = wdIdtoInstruments;
  }  
  
  
}
