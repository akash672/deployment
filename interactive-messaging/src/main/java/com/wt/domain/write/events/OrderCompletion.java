package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderCompletion extends OrderRootEvent {

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String exchangeOrderID;
  private String brokerOrderID;

  public OrderCompletion(String wdOrderId, String clientCode, String exchangeOrderID, String brokerOrderID) {
    super(wdOrderId);
    this.clientCode = clientCode;
    this.exchangeOrderID = exchangeOrderID;
    this.brokerOrderID = brokerOrderID;

  }

}
