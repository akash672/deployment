package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class MutualFundOrderRejectedByExchange extends OrderEvent {

	private static final long serialVersionUID = 1L;
	private String exchangeOrderId;
	private String message;

}
