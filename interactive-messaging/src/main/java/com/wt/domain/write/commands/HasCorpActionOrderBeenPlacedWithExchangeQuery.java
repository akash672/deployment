package com.wt.domain.write.commands;

import com.wt.domain.OrderSide;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class HasCorpActionOrderBeenPlacedWithExchangeQuery extends OrderRootCommand {

  private static final long serialVersionUID = 1L;
  @Getter private String clientOrderId;
  @Getter private String brokerOrderId;
  @Getter private OrderSide side;
  @Getter private int quantity;
}
