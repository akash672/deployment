package com.wt.domain.write.events;

import static java.lang.String.valueOf;

import java.io.Serializable;

import lombok.Getter;

public class EquityInstrumentDescriptionUpdated implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private String token;
  @Getter private String symbol;
  @Getter private String description;
  @Getter private String exchange;
  @Getter private String wdId;
  
  public EquityInstrumentDescriptionUpdated(String token, String symbol, String description, String exchange, String wdId) {
    super();
    this.token = token;
    this.symbol = symbol;
    this.description = description;
    this.exchange = exchange;
    this.wdId = wdId;
  }
  
  public EquityInstrumentDescriptionUpdated(int token, String symbol, String description, String exchange, String wdId) {
    super();
    this.token = valueOf(token);
    this.symbol = symbol;
    this.description = description;
    this.exchange = exchange;
    this.wdId = wdId;
  }
  
  
}
