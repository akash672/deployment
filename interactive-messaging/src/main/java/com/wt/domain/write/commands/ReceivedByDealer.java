package com.wt.domain.write.commands;

import com.wt.domain.write.events.OrderEvent;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class ReceivedByDealer extends OrderEvent {
  private static final long serialVersionUID = 1L;
  private String txCode;
  private String localOrderID;
  private long exchangeOrderId;
  private long userId;
  private String memberId;
  private String clientCode;
  private String bseRemark;
  private String orderSuccessFlag;

}