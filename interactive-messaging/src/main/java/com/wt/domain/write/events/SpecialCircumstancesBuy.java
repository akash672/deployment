package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SpecialCircumstancesBuy extends MergerDemergerEvent {

  private static final long serialVersionUID = 1L;
  private String token;
  private String symbol;
  private double price;
  private int quantity;
  private String investorUsername;
  private String adviserUsername;
  private String fund;
  private String adviseId;
  private String clientCode;
  private BrokerName brokerName;
  private InvestingMode investingMode;

  public SpecialCircumstancesBuy(@JsonProperty("token") String token, @JsonProperty("symbol") String symbol,
      @JsonProperty("price") double price, @JsonProperty("quantity") int quantity,
      @JsonProperty("investorUsername") String investorUsername,
      @JsonProperty("adviserUsername") String adviserUsername, @JsonProperty("fund") String fund,
      @JsonProperty("adviseId") String adviseId, @JsonProperty("clientCode") String clientCode,
      @JsonProperty("brokerName") BrokerName brokerName, @JsonProperty("investingMode") InvestingMode investingMode) {
    this.token = token;
    this.symbol = symbol;
    this.price = price;
    this.quantity = quantity;
    this.investorUsername = investorUsername;
    this.adviserUsername = adviserUsername;
    this.fund = fund;
    this.adviseId = adviseId;
    this.clientCode = clientCode;
    this.brokerName = brokerName;
    this.investingMode = investingMode;
  }
}
