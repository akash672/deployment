package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderRejectedByBroker extends OrderRejectedEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private String localOrderId;

  public OrderRejectedByBroker(DestinationAddress destinationAddress, String localOrderId, String msg) {
    super(destinationAddress, msg);
    this.localOrderId = localOrderId;
  }

}