package com.wt.domain.write.commands;

import com.wt.domain.OrderSide;

import lombok.Getter;
import lombok.ToString;


@Getter
@ToString
public class SentToExchange extends OrderCommand {

  private static final long serialVersionUID = 1L;
  private String brokerOrderId;
  private String exchangeOrderId;
  private long exchangeOrderTime;
  private String clientCode;
  private String token;
  private int quantity;
  private OrderSide orderSide;

  public SentToExchange(String brokerOrderId, String exchangeOrderId, long exchangeOrderTime,
      String clientCode, String token, int quantity, OrderSide orderSide) {
    super();
    this.brokerOrderId = brokerOrderId;
    this.exchangeOrderId = exchangeOrderId;
    this.exchangeOrderTime = exchangeOrderTime;
    this.clientCode = clientCode;
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
  }

}
