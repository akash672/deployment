package com.wt.domain.write.events;

import com.wt.domain.OrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderTraded extends OrderEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private OrderActorStates orderActorStates;
  @Getter
  private String exchangeOrderId;
  @Getter
  private long orderConfirmationTime;
  @Getter
  private float price;
  @Getter
  private int qty;
  @Getter
  private String tradeId;
  @Getter
  private String brokerOrderId;

  public OrderTraded(OrderActorStates orderActorStates, String exchangeOrderId, long orderConfirmationTime, float price,
      int qty, String tradeId, String brokerOrderID) {
    super();
    this.orderActorStates = orderActorStates;
    this.exchangeOrderId = exchangeOrderId;
    this.orderConfirmationTime = orderConfirmationTime;
    this.price = price;
    this.qty = qty;
    this.tradeId = tradeId;
    this.brokerOrderId = brokerOrderID;
  }

}