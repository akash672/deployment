package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class StopLivePriceStream extends AssetClassCommand implements Serializable {
  
  private static final long serialVersionUID = 1L;
  String wdId;
}
