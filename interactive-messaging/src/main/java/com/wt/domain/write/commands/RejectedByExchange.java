package com.wt.domain.write.commands;

import com.wt.domain.OrderSide;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class RejectedByExchange extends OrderCommand {

  private static final long serialVersionUID = 1L;
  private String brokerOrderId;
  private String message;
  private String clientCode;
  private String token;
  private int quantity;
  private OrderSide orderSide;
  

  public RejectedByExchange(String brokerOrderId, String message, String clientCode,
       String token, int quantity, OrderSide orderSide) {
    super();
    this.brokerOrderId = brokerOrderId;
    this.message = message;
    this.clientCode = clientCode;
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
  }

}
