package com.wt.domain.write.events;

import com.wt.domain.CancelOrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class CancelOrderReceivedByBroker extends CancelOrderEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private CancelOrderActorStates cancelOrderActorStates;
  @Getter
  private String localOrderId;
  @Getter
  private String brokerOrderId;

  public CancelOrderReceivedByBroker(CancelOrderActorStates cancelOrderActorStates, String localOrderId, String brokerOrderId) {
    super();
    this.cancelOrderActorStates = cancelOrderActorStates;
    this.localOrderId = localOrderId;
    this.brokerOrderId = brokerOrderId;
  }
}