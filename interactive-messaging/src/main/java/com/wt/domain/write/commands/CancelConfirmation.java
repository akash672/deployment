package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CancelConfirmation extends OrderCommand{

  
  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  private String brokerOrderId;
  
}
