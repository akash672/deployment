package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class OrderRejectionCtcl extends OrderRootEvent {

  private static final long serialVersionUID = 1L;
  private String localOrderId;
  private String brokerOrderId;
  private String message;

  public OrderRejectionCtcl(String WdOrderId, String localOrderId, String brokerOrderId, String message) {
    super(WdOrderId);
    this.localOrderId = localOrderId;
    this.brokerOrderId = brokerOrderId;
    this.message = message;
  }

}
