package com.wt.domain.write.events;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.Purpose;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderActorCreated extends OrderRootEvent {

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String userEmail;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private OrderExecutionMetaData orderExecutionMetaData;
  private String senderPath;
  private String localOrderId;
  private String exchange;
  private BrokerAuthInformation brokerAuthInformation;
  
  public OrderActorCreated(String clientCode, String userEmail, BrokerName brokerName, 
      Purpose purpose, InvestingMode investingMode,
      OrderExecutionMetaData orderExecutionMetaData, String senderPath, String localOrderId, 
      String clientOrderId, String exchange, BrokerAuthInformation brokerAuthInformation) {
      
      super(clientOrderId);
      this.clientCode = clientCode;
      this.userEmail = userEmail;
      this.brokerName = brokerName;
      this.purpose = purpose;
      this.investingMode = investingMode;
      this.orderExecutionMetaData = orderExecutionMetaData;
      this.senderPath = senderPath;
      this.localOrderId = localOrderId;
      this.exchange = exchange;
      this.brokerAuthInformation = brokerAuthInformation;
    
  }
  
  
 
}
