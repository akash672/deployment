package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.HashMap;

import com.wt.domain.PriceWithPaf;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class PriceSnapshotsFromPriceSource extends AssetClassCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  HashMap<String, PriceWithPaf> tickerToPriceWithPafMap;

}
