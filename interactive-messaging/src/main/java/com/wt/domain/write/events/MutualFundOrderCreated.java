package com.wt.domain.write.events;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderSide;
import com.wt.domain.Purpose;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class MutualFundOrderCreated extends OrderRootEvent {

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String userEmail;
  private BrokerName brokerName;
  private String localOrderId;
  private Purpose purpose;
  private InvestingMode investingMode;
  private String token;
  private String symbol;
  private double quantity;
  private OrderSide orderSide;
  private String senderPath;
  private double allocatedAmount;
  private String schemeName;
  private String schemeCode;
  private String schemeType;
  private String folioNumber;
  private String txCode;
  private String dpcTxn;
  private String allRedeem;
  private String minRedeem;
  private String dpcFlag;

  public MutualFundOrderCreated(String wdOrderId, String clientCode, String userEmail, BrokerName brokerName, String localOrderId,
      Purpose purpose, InvestingMode investingMode, String token, String symbol, double quantity, OrderSide orderSide, String senderPath,
      double orderValue, String schemeName, String schemeCode, String schemeType, String folioNumber,  String txCode, String dpcTxn,
      String allRedeem, String minRedeem, String dpcFlag) {
    super(wdOrderId);
    this.clientCode = clientCode;
    this.userEmail = userEmail;
    this.brokerName = brokerName;
    this.localOrderId = localOrderId;
    this.purpose = purpose;
    this.investingMode = investingMode;
    this.token = token;
    this.symbol = symbol;
    this.quantity = quantity;
    this.orderSide = orderSide;
    this.senderPath = senderPath;
    this.allocatedAmount = orderValue;
    this.schemeName = schemeName;
    this.schemeCode =schemeCode;
    this.schemeType = schemeType;
    this.folioNumber = folioNumber;
    this.txCode = txCode;
    this.dpcTxn = dpcTxn;
    this.allRedeem = allRedeem;
    this.minRedeem = minRedeem;
    this.dpcFlag = dpcFlag;
        
  }

}