package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class OrderCancellationRejected {
  private String clientOrderId;
  private String clientCode;
  private String localOrderIdCancellation;
  private String brokerOrderIdCancellation;
}
