
package com.wt.domain.write.commands;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class MFOrderFullyTraded {
  private String wdOrderId;
  private String clientCode;
  private long exchangeOrderID;
  

  public MFOrderFullyTraded(String wdOrderId, String clientCode, long exchangeOrderID) {
    super();
    this.wdOrderId = wdOrderId;
    this.clientCode = clientCode;
    this.exchangeOrderID = exchangeOrderID;
  }

}

