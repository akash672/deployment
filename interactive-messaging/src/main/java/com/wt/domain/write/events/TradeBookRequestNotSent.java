package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class TradeBookRequestNotSent extends OrderEvent {

  private static final long serialVersionUID = 1L;
  private String message;
}
