package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class OrderRootEvent implements Serializable {

  private static final long serialVersionUID = 1L;
  private String clientOrderId;

  public OrderRootEvent(String clientOrderId) {
    super();
    this.clientOrderId = clientOrderId;
  }

}
