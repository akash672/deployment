package com.wt.domain.write.events;

import java.io.Serializable;

import com.wt.domain.DestinationAddress;

import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public abstract class ExchangeOrderUpdateEvent extends EventWithTimeStamps implements Serializable {
  private static final long serialVersionUID = 1L;
  private final DestinationAddress destinationAddress;
  
  public String senderPath() {
    return destinationAddress.getSenderPath();
  }
}
