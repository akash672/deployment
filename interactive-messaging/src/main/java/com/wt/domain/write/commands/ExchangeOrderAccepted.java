package com.wt.domain.write.commands;

import com.wt.domain.ExchangeOrderMessageType;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(callSuper=true)
@ToString
public class ExchangeOrderAccepted extends ExchangeOrderMessageType {

  private static final long serialVersionUID = 1L;

  public ExchangeOrderAccepted(String clientCode, String brokerOrderId, String exchOrderID, long exchOrderTime,
      String token, int quantity, OrderSide orderSide) {
    super(clientCode, brokerOrderId, exchOrderID, exchOrderTime, token, quantity, orderSide);
  }

}
