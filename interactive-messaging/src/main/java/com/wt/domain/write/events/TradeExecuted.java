package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;
import com.wt.domain.OrderSide;
import com.wt.domain.PriceWithPaf;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true, of = {"tradeOrderId"})
public class TradeExecuted extends ExchangeOrderUpdateEvent {
  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  private String tradeOrderId;
  private OrderSide orderSide;
  private PriceWithPaf price;
  private double quantity;
  private String folioNumber;
  private long lastTradedTime;

  public TradeExecuted(DestinationAddress destinationAddress, String exchangeOrderId, String tradeOrderId,
      OrderSide orderSide, PriceWithPaf price, double quantity) {
    super(destinationAddress);
    this.exchangeOrderId = exchangeOrderId;
    this.tradeOrderId = tradeOrderId;
    this.orderSide = orderSide;
    this.price = price;
    this.quantity = quantity;
  }
  
  public static class  TradeExecutedBuilder extends ExchangeOrderUpdateEvent {
    private static final long serialVersionUID = 1L;
    private String exchangeOrderId;
    private String tradeOrderId;
    private OrderSide orderSide;
    private PriceWithPaf price;
    private double quantity;
    private String folioNumber;
    private long lastTradedTime;
    
    public TradeExecutedBuilder(DestinationAddress destinationAddress, String exchangeOrderId, String tradeOrderId,
        OrderSide orderSide, PriceWithPaf price, double quantity) {
      super(destinationAddress);
      this.exchangeOrderId = exchangeOrderId;
      this.tradeOrderId = tradeOrderId;
      this.orderSide = orderSide;
      this.price = price;
      this.quantity = quantity;
    }
    
    public TradeExecutedBuilder withFolioNumber(String folioNumber) {
      this.folioNumber = folioNumber;
       return this;
    }
    
    public TradeExecutedBuilder withLastTradedTime(long lastTradedTime) {
      this.lastTradedTime = lastTradedTime;
      return this;
    }

    public TradeExecuted build() {
      return new TradeExecuted(this);
    }
    
  }
  
  public TradeExecuted(TradeExecutedBuilder builder) {
    super(builder.getDestinationAddress());
    this.exchangeOrderId = builder.exchangeOrderId;
    this.tradeOrderId = builder.tradeOrderId;
    this.orderSide = builder.orderSide;
    this.price = builder.price;
    this.quantity = builder.quantity;
    this.folioNumber = builder.folioNumber;
    this.lastTradedTime = builder.lastTradedTime;
  }
  
  
  
  
  public int getIntegerQuantity()
  {
    return (int)getQuantity();
  }

}