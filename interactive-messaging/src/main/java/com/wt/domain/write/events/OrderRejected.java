package com.wt.domain.write.events;

import com.wt.domain.OrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderRejected extends OrderEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private OrderActorStates orderActorStates;
  @Getter
  private String localOrderId;
  @Getter
  String msg;

  public OrderRejected(OrderActorStates orderActorStates, String localOrderId, String msg) {
    super();
    this.orderActorStates = orderActorStates;
    this.localOrderId = localOrderId;
    this.msg = msg;
  }

}