package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class ChildShutDownAtEOD extends OrderRootEvent {

  private static final long serialVersionUID = 1L;
  private String clientCode;

  public ChildShutDownAtEOD(String WdOrderId, String clientCode) {
    super(WdOrderId);
    this.clientCode = clientCode;
  }

}
