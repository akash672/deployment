package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class OrderRejectionExchange extends OrderRootEvent {

  private static final long serialVersionUID = 1L;

  private String exchOrderID;
  private String brokerOrderId;
  private String message;

  public OrderRejectionExchange(String WdOrderId,String exchOrderID, String brokerOrderId, String message) {
    super(WdOrderId);
    this.exchOrderID = exchOrderID;
    this.brokerOrderId = brokerOrderId;
    this.message = message;
   
  }

}
