package com.wt.domain.write;

import static com.wt.domain.BrokerName.fromBrokerName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerName;
import com.wt.domain.BrokerType;
import com.wt.domain.InvestingMode;
import com.wt.domain.MarginRequestMessage;
import com.wt.domain.MutualFundInteractiveFactory;
import com.wt.domain.Purpose;
import com.wt.domain.write.commands.CreateMutualFundOrder;
import com.wt.domain.write.commands.ExchangeOrderUpdateEventWithDeliveryId;
import com.wt.domain.write.commands.GetMarginRequest;
import com.wt.domain.write.commands.MFOrderFullyTraded;
import com.wt.domain.write.commands.MessageReceptionConfirmation;
import com.wt.domain.write.commands.MutualFundOrderTradeExecuted;
import com.wt.domain.write.commands.MutualFundTradeConfirmation;
import com.wt.domain.write.commands.ReceivedByDealer;
import com.wt.domain.write.commands.SubscribeToExchangeUpdateEventStream;
import com.wt.domain.write.events.ExchangeOrderUpdateEvent;
import com.wt.domain.write.events.MFOrderReceivedByDealer;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.MessageDeliveredSuccessfully;
import com.wt.domain.write.events.MutualFundOrderCreated;
import com.wt.domain.write.events.MutualFundOrderRejectedByExchange;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.persistence.AbstractPersistentActorWithAtLeastOnceDelivery;
import akka.persistence.RecoveryCompleted;
import scala.util.Random;

@Service("MFOrderRootActor")
@Scope("prototype")
public class MFOrderRootActor extends AbstractPersistentActorWithAtLeastOnceDelivery implements ParentActor<MFOrderActor> {

  private Map<String, String> localOrderIdToClientOrderId = new HashMap<String, String>();
  private Map<String, List<String>> clientCodeToClientOrderId = new HashMap<String, List<String>>();
  private Map<String, String> clientCodeUserEmailMap = new HashMap<>();
  @Autowired
  private WDActorSelections wdActorSelections;
  @Autowired
  private MutualFundInteractiveFactory mfCtclFactory;
  private long counter=0;

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(CreateMutualFundOrder.class, command -> {
      createMutualFundOrder(command);
    }).match(GetMarginRequest.class, evt -> {
      handleGetMarginRequest(evt);
   }).match(MarginRequestMessage.class, evt -> {
      handleMarginRequestMessage(evt);
   }).match(ExchangeOrderUpdateEvent.class, evt -> {
          handleExchangeOrderUpdateEvent(evt);
    }).match(MessageReceptionConfirmation.class, evt -> {
      handleMessageReceptionConfirmation(evt);
    }).match(ReceivedByDealer.class, command -> {
          handleOrderReceivedByDealer(command);
    }).match(MutualFundTradeConfirmation.class, command -> {
          handleTradeConfirmation(command);
    }).match(MFOrderFullyTraded.class, command -> {
          handleOrderFullyTraded(command);
    }).match(MutualFundOrderRejectedByExchange.class, command -> {
          handleOrderRejectedByExchange(command);
    }).build();
    }

  private void handleGetMarginRequest(GetMarginRequest getMarginRequest) {
    MutualFundInteractive mfCtcl;
    BrokerName brokerName = fromBrokerName(getMarginRequest.getBrokerCompany(), InvestingMode.REAL);
    try {
      mfCtcl = mfCtclFactory.getCTCL(new BrokerType(Purpose.MUTUALFUNDSUBSCRIPTION, InvestingMode.REAL, brokerName));
      mfCtcl.sendMarginRequest(getMarginRequest.getClientCode());
    } catch (Exception e) {
      log.error("Error while getting margin request " + e.getMessage());
    }
    clientCodeUserEmailMap.put(getMarginRequest.getClientCode(),getMarginRequest.getEmailId());
  }
  
  private void handleMarginRequestMessage(MarginRequestMessage marginRequestMessage) {
    String emailId = clientCodeUserEmailMap.get(marginRequestMessage.getClientCode());
    wdActorSelections.userRootActor().tell(new MarginReceived(marginRequestMessage.getMarginAmount(),
        emailId, InvestingMode.REAL), ActorRef.noSender());
    clientCodeUserEmailMap.remove(marginRequestMessage.getClientCode());
  }
  
  private void handleMessageReceptionConfirmation(MessageReceptionConfirmation messageReceptionConfirmation) {
    persist(new MessageDeliveredSuccessfully(messageReceptionConfirmation.getDeliveryId()), 
        messageDelivered -> confirmDelivery(messageDelivered.getDeliveryId()));
  }

  private void handleExchangeOrderUpdateEvent(ExchangeOrderUpdateEvent exchangeOrderUpdateEvent) {
    persist(exchangeOrderUpdateEvent, 
        (exchangeOrderUpdateReceived) -> deliver(wdActorSelections.userRootActor(), 
            deliveryId -> new ExchangeOrderUpdateEventWithDeliveryId(exchangeOrderUpdateReceived, deliveryId, "MutualFund")));
  }

  private void handleOrderRejectedByExchange(MutualFundOrderRejectedByExchange command) {
    persist(command , event -> {
      tellChildren(context(), self(), event,
          localOrderIdToClientOrderId.get(String.valueOf(command.getExchangeOrderId())));
    });
    
  }

  private void handleOrderFullyTraded(MFOrderFullyTraded command) {
    localOrderIdToClientOrderId.remove(String.valueOf(command.getExchangeOrderID()));
  }

  private void handleTradeConfirmation(MutualFundTradeConfirmation command) {
    MutualFundOrderTradeExecuted mutualFundOrderTradeExecuted = MutualFundOrderTradeExecuted.builder()
        .exchangeOrderId(command.getExchangeOrderId())
        .isin(command.getIsin())
        .side(command.getOrderSide())
        .quantity(command.getAllottedUnit())
        .price(command.getAllottedNav())
        .clientCode(command.getClientCode())
        .folioNumber(command.getFolioNumber())
        .build();
    persist(mutualFundOrderTradeExecuted, event -> {
      if(isOrderSuccessfullyPlaced(command.getValidFlag())) {
        tellChildren(context(), self(), event,
            localOrderIdToClientOrderId.get(String.valueOf(command.getExchangeOrderId())));
      } else {
        self().tell(new MutualFundOrderRejectedByExchange(command.getExchangeOrderId(), command.getBseRemarks()) ,self());
      }
    });      
    
  }

  private void handleOrderReceivedByDealer(ReceivedByDealer command) {
      MFOrderReceivedByDealer mfOrderReceivedByDealer = new MFOrderReceivedByDealer(localOrderIdToClientOrderId.get(command.getLocalOrderID()), command.getLocalOrderID());
      persist(mfOrderReceivedByDealer, event -> {
        tellChildren(context(), self(), command,  localOrderIdToClientOrderId.get(String.valueOf(command.getLocalOrderID())));
      });
  }



  private void createMutualFundOrder(CreateMutualFundOrder command) {
    
    addToClientToWDOrderIdMap(command.getClientCode(), command.getClientOrderId());
    String localOrderId = createLocalOrderId();
    MutualFundOrderCreated mutualFundOrderCreated = new MutualFundOrderCreated(command.getClientOrderId(),
        command.getClientCode(), command.getUserEmail(), command.getBrokerName(), localOrderId, command.getPurpose(),
        command.getInvestingMode(), command.getOrderExecutionMetaData().getToken(), command.getOrderExecutionMetaData().getSymbol(), command.getOrderExecutionMetaData().getQuantity(), command.getOrderExecutionMetaData().getOrderSide(), command.getSenderPath(),
        command.getAllocatedAmount(), command.getSchemeName(), command.getSchemeCode(), command.getSchemeType(), command.getFolioNumber(), command.getTxCode(), command.getDpcTxn(), command.getAllRedeem(), command.getMinRedeem(), command.getDpcFlag());
    persist(mutualFundOrderCreated, event -> {
      localOrderIdToClientOrderId.put(localOrderId, command.getClientOrderId()); 
      tellChildren(context(), self(), mutualFundOrderCreated, event.getClientOrderId());
    });
  }
  

  @Override
  @SuppressWarnings("unused")
  public Receive createReceiveRecover() {
    return receiveBuilder().match(MutualFundOrderCreated.class, event -> {
      MutualFundOrderCreated mutualFundOrderCreated = (MutualFundOrderCreated)event;
      localOrderIdToClientOrderId.put(mutualFundOrderCreated.getLocalOrderId(), mutualFundOrderCreated.getClientOrderId());
    }).match(MFOrderReceivedByDealer.class, event  -> {
      MFOrderReceivedByDealer mfOrderReceivedByDealer = (MFOrderReceivedByDealer)event;
    }).match(MutualFundOrderRejectedByExchange.class, event -> {
      MutualFundOrderRejectedByExchange mutualFundOrderRejectedByDealer = (MutualFundOrderRejectedByExchange)event;
    }).match(ExchangeOrderUpdateEvent.class, evt -> {
      deliver(wdActorSelections.userRootActor(), 
          deliveryId -> new ExchangeOrderUpdateEventWithDeliveryId(evt, deliveryId, "MutualFund"));
    }).match(MessageDeliveredSuccessfully.class, evt -> {
      confirmDelivery(evt.getDeliveryId());
    }).match(MutualFundOrderTradeExecuted.class, event -> {
      MutualFundOrderTradeExecuted mutualFundOrderTradeExecuted = (MutualFundOrderTradeExecuted)event;
    }).match(RecoveryCompleted.class, evt -> {
      recoverActiveOrders();
    }).build();
  }

  private void recoverActiveOrders() {
    clientCodeToClientOrderId.values().stream().forEach(activeOrderIds -> {
      for (String activeOrderId : activeOrderIds) {
        forwardToChildren(context(), new SubscribeToExchangeUpdateEventStream(), activeOrderId);
      }
    });

  }

  private void addToClientToWDOrderIdMap(String clientCode, String wdOrderId) {
    if (clientCodeToClientOrderId.containsKey(clientCode)) {
      List<String> wdIdsSet = clientCodeToClientOrderId.get(clientCode);
      wdIdsSet.add(wdOrderId);
    } else {
      List<String> wdIdsSet = new ArrayList<String>();
      wdIdsSet.add(wdOrderId);
      clientCodeToClientOrderId.put(clientCode, wdIdsSet);
    }
  }
  
  private boolean isOrderSuccessfullyPlaced(String orderSuccessFlag) {
    return orderSuccessFlag.equals("Y");
  }
  
  public String createLocalOrderId() {
    if(counter == 0) {
      Random r = new Random();
      counter = r.nextInt(1000000000);
    }
     String localOrderId =  DateUtils.prettyTime(DateUtils.currentTimeInMillis(),"yyyyMMdd") + counter;
     counter++;
     return localOrderId;
  }

  @Override
  public String persistenceId() {
    return "MFOrderRootActor";
  }

  @Override
  public String getName() {
    return "MFOrderRoot";
  }

  @Override
  public String getChildBeanName() {
    return "MFOrderActor";
  }

}