package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper=true)
@Getter
public class OrderReceivedByDealer extends ExchangeOrderUpdateEvent {

  private static final long serialVersionUID = 1L;
  private String wdOrderId;
  private String dealerOrderId;
  
  public OrderReceivedByDealer(DestinationAddress destinationAddress, String wdOrderId, String dealerOrderId) {
    super(destinationAddress);
    this.wdOrderId = wdOrderId;
    this.dealerOrderId = dealerOrderId;
    
  }
  
  

}
