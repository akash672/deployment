package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class CancellationRejected  extends OrderRootEvent {

  private static final long serialVersionUID = 1L;
  private String clientOrderIDd;
  private String clientCode;
  private String localOrderIdCancellation;
  private String brokerOrderIdCancellation;

  public CancellationRejected(String clientOrderId,String clientCode,String localOrderIdCancellation,String brokerOrderIDCancellation) {
    super(clientOrderId);
    this.clientCode = clientCode;
    this.localOrderIdCancellation = localOrderIdCancellation;
    this.brokerOrderIdCancellation = brokerOrderIDCancellation;
  }

}
