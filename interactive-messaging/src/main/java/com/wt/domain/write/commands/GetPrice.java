package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class GetPrice extends AssetClassCommand implements Serializable {
  private static final long serialVersionUID = 1L;
  private String wdId;
}
