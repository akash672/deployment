package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true, of = {"exchangeOrderId"})
public class OrderPlacedWithExchange extends ExchangeOrderUpdateEvent {
  private static final long serialVersionUID = 1L;
  private String brokerOrderId;
  private String exchangeOrderId;
  private String ctclId;
  private OrderSide side;
  private int quantity;

  public OrderPlacedWithExchange(DestinationAddress destinationAddress, String brokerOrderId, String exchangeOrderId,
      String ctclId, OrderSide side, int quantity) {
    super(destinationAddress);
    this.brokerOrderId = brokerOrderId;
    this.exchangeOrderId = exchangeOrderId;
    this.ctclId = ctclId;
    this.side = side;
    this.quantity = quantity;
  }
}
