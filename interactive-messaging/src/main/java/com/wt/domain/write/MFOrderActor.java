package com.wt.domain.write;

import static com.wt.domain.MFOrderActorStates.FullyExecuted;
import static com.wt.domain.MFOrderActorStates.OrderBeingGenerated;
import static com.wt.domain.MFOrderActorStates.OrderGenerated;
import static com.wt.domain.MFOrderActorStates.OrderReceivedbyDealer;
import static com.wt.domain.MFOrderActorStates.OrderRejectedbyExchange;
import static com.wt.domain.PriceWithPaf.withPrice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.CtclMFOrder;
import com.wt.domain.DestinationAddress;
import com.wt.domain.MFOrderActorStates;
import com.wt.domain.MutualFundInteractiveFactory;
import com.wt.domain.write.commands.MFOrderFullyTraded;
import com.wt.domain.write.commands.MutualFundOrderTradeExecuted;
import com.wt.domain.write.commands.ReceivedByDealer;
import com.wt.domain.write.commands.StateMachineTerminationCommand;
import com.wt.domain.write.events.MutualFundOrderCreated;
import com.wt.domain.write.events.MutualFundOrderGenerationInProcess;
import com.wt.domain.write.events.MutualFundOrderReceivedByDealer;
import com.wt.domain.write.events.MutualFundOrderRejectedByExchange;
import com.wt.domain.write.events.MutualFundTradeExecuted;
import com.wt.domain.write.events.OrderEvent;
import com.wt.domain.write.events.OrderReceivedByDealer;
import com.wt.domain.write.events.OrderRejectedByBSEStar;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.utils.DoublesUtil;

import akka.actor.ActorRef;
import akka.persistence.fsm.AbstractPersistentFSM;
import lombok.extern.log4j.Log4j;

@Log4j
@Service("MFOrderActor")
@Scope("prototype")
public class MFOrderActor extends AbstractPersistentFSM<MFOrderActorStates, CtclMFOrder, OrderEvent> {
  private MutualFundInteractiveFactory mfCtclFactory;
  private String persistenceId;

  @Autowired
  public void setCTCLFactory(MutualFundInteractiveFactory mfCtclFactory) {
    this.mfCtclFactory = mfCtclFactory;
  }

  public MFOrderActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
    startWith(OrderBeingGenerated, new CtclMFOrder());
    
    when(OrderBeingGenerated,
        matchEvent(MutualFundOrderCreated.class, 
            (mfOrderCreated, orderData) -> {
              return goTo(OrderGenerated).
                  applying(MutualFundOrderGenerationInProcess.builder()
                      .txCode(mfOrderCreated.getTxCode())
                      .localOrderId( mfOrderCreated.getLocalOrderId())
                      .clientCode( mfOrderCreated.getClientCode())
                      .schemeCode(mfOrderCreated.getSchemeCode())
                      .side(mfOrderCreated.getOrderSide())
                      .quantity(mfOrderCreated.getQuantity())
                      .dpTxn(mfOrderCreated.getDpcTxn())
                      .allocatedAmount(mfOrderCreated.getAllocatedAmount())
                      .allRedeem(mfOrderCreated.getAllRedeem())
                      .folioNumber(mfOrderCreated.getFolioNumber())
                      .minRedeem(mfOrderCreated.getMinRedeem())
                      .dpcFlag(mfOrderCreated.getDpcFlag())
                      .destinationAddress(new DestinationAddress(mfOrderCreated.getSenderPath()))
                      .clientOrderId(mfOrderCreated.getClientOrderId())
                      .symbol(mfOrderCreated.getSymbol())
                      .brokerName(mfOrderCreated.getBrokerName())
                      .purpose(mfOrderCreated.getPurpose())
                      .investingMode(mfOrderCreated.getInvestingMode())
                      .schemeName(mfOrderCreated.getSchemeName())
                      .schemeType(mfOrderCreated.getSchemeType())
                      .token(mfOrderCreated.getToken())
                      .orderActorStates(stateName())
                      .build()).
                  andThen(exec(data -> {
                    MutualFundInteractive mfCtcl;
                    try {
                      mfCtcl = mfCtclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                      mfCtcl.sendMutualFundOrder(orderData);
                    } catch (Exception e) {
                      log.error("Error while sending order to BSE Star: "+e.getMessage());
                    }
                  }));
     }));
    
    when(OrderGenerated, 
        matchEvent(ReceivedByDealer.class, 
        (receivedByDealer, orderData) -> {
          return goTo(OrderReceivedbyDealer).
              applying(new MutualFundOrderReceivedByDealer(
                  receivedByDealer.getLocalOrderID(), 
                  String.valueOf(receivedByDealer.getExchangeOrderId())))
              .andThen(exec(data -> {
                context().parent().tell(new OrderReceivedByDealer(
                    orderData.getDestinationAddress(),
                    orderData.getClientOrderId(),
                   String.valueOf(receivedByDealer.getLocalOrderID())), self());
              }));
        }));

    when(OrderReceivedbyDealer,
        matchEvent(MutualFundOrderTradeExecuted.class, 
        (mutualFundTradeExecuted, orderData) -> {
          double quantity;
          if( DoublesUtil.isZero(mutualFundTradeExecuted.getQuantity())) {
            quantity = orderData.getQuantity();
          }
          else
            quantity = mutualFundTradeExecuted.getQuantity();
      return goTo(FullyExecuted)
          .applying(new MutualFundTradeExecuted(
              String.valueOf(mutualFundTradeExecuted.getExchangeOrderId()),
              mutualFundTradeExecuted.getSide(),
              quantity,
              mutualFundTradeExecuted.getPrice()))
          .andThen(exec(data -> {
            context().parent().tell(new TradeExecuted.TradeExecutedBuilder(
                orderData.getDestinationAddress(),
                String.valueOf(mutualFundTradeExecuted.getExchangeOrderId()),
                String.valueOf(mutualFundTradeExecuted.getExchangeOrderId()),
                mutualFundTradeExecuted.getSide(),
                withPrice(mutualFundTradeExecuted.getPrice(), orderData.getToken(), Pafs.withCumulativePaf(1.)),
                quantity).withFolioNumber( String.valueOf(mutualFundTradeExecuted.getFolioNumber())).build(), self());
            context().parent().tell(new MFOrderFullyTraded(orderData.getClientOrderId(),
                                    orderData.getClientCode(),
                                    Long.parseLong(orderData.getExchangeOrderId())
                                    ), ActorRef.noSender());
          }));
        })
           .event(MutualFundOrderRejectedByExchange.class,
                  (orderRejected, orderData) -> {
                return goTo(OrderRejectedbyExchange)
                    .applying(orderRejected)
                    .andThen(exec( data -> {
                      context().parent()
                      .tell(new OrderRejectedByBSEStar(
                          orderData.getDestinationAddress(),
                          orderRejected.getExchangeOrderId(), 
                          orderRejected.getMessage()), self());
                    }));
                  }));
    
    when(OrderRejectedbyExchange, 
        matchEvent(StateMachineTerminationCommand.class, 
            (stateMachineTerminated, orderData) -> {
      return stay();
        }));
  
    when(FullyExecuted, 
        matchEvent(StateMachineTerminationCommand.class, 
            (stateMachineTerminated, orderData) -> {
      return stay();
        }));
    }


  @Override
  public String persistenceId() {
    return persistenceId;
  }

  @Override
  public Class<OrderEvent> domainEventClass() {
    return OrderEvent.class;
  }
  
  @Override
  public CtclMFOrder applyEvent(OrderEvent event, CtclMFOrder data) {
    data.update(event);
    return data;
  }

}