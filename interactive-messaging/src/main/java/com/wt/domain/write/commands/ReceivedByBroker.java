package com.wt.domain.write.commands;

import lombok.Getter;
import lombok.ToString;

@ToString
public class ReceivedByBroker extends OrderCommand {

  private static final long serialVersionUID = 1L;
  @Getter
  private final String localOrderId;
  @Getter
  private final String brokerOrderId;

  public ReceivedByBroker(String localOrderId, String brokerOrderId) {
    super();
    this.localOrderId = localOrderId;
    this.brokerOrderId = brokerOrderId;
  }

}
