package com.wt.domain.write.events;

import com.wt.domain.BrokerName;
import com.wt.domain.DestinationAddress;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderActorStates;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.Purpose;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderGenerationInProcess extends OrderEvent {

  private static final long serialVersionUID = 1L;
  private OrderActorStates orderActorStates;
  private String clientCode;
  private OrderExecutionMetaData orderExecutionMetaData;
  private DestinationAddress destinationAddress;
  private String localOrderId;
  private String clientOrderId;
  private BrokerName brokerName;
  private String exchange;
  private Purpose purpose;
  private InvestingMode investingMode;
  private BrokerAuthInformation brokerAuthInformation;
  
  public OrderGenerationInProcess(OrderActorStates orderActorStates, String clientCode, 
      OrderExecutionMetaData orderExecutionMetaData, DestinationAddress destinationAddress, 
      String localOrderId, String clientOrderId, BrokerName brokerName, String exchange,
      Purpose purpose, InvestingMode investingMode, BrokerAuthInformation brokerAuthInformation) {
    super();
    this.orderActorStates = orderActorStates;
    this.clientCode = clientCode;
    this.orderExecutionMetaData = orderExecutionMetaData;
    this.destinationAddress = destinationAddress;
    this.localOrderId = localOrderId;
    this.clientOrderId = clientOrderId;
    this.brokerName = brokerName;
    this.exchange = exchange;
    this.purpose = purpose;
    this.investingMode = investingMode;
    this.brokerAuthInformation = brokerAuthInformation;
  }
  
  public static OrderGenerationInProcess marketOrderGenerationInProcess(OrderActorStates orderActorStates, String clientCode,
      OrderExecutionMetaData orderExecutionMetaData, DestinationAddress destinationAddress, String localOrderId,
      String clientOrderId, BrokerName brokerName, String exchange, Purpose purpose, InvestingMode investingMode,
      BrokerAuthInformation brokerAuthInformation) {
    return new OrderGenerationInProcess(orderActorStates, clientCode, orderExecutionMetaData, destinationAddress, localOrderId, clientOrderId, brokerName,
        exchange, purpose, investingMode, brokerAuthInformation);
  }

}
