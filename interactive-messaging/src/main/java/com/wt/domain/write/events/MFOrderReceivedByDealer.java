package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MFOrderReceivedByDealer extends OrderEvent {

  private static final long serialVersionUID = 1L;
  private String clientOrderId;
  private String localOrderId;

}
