package com.wt.domain.write.events;

import com.wt.domain.OrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderSentToExchange extends OrderEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private String clientCode;
  @Getter
  private OrderActorStates orderActorStates;
  @Getter
  private String brokerOrderId;
  @Getter
  private String exchangeOrderId;
  @Getter
  private long exchangeOrderTime;

  public OrderSentToExchange(OrderActorStates orderActorStates, String clientCode, String brokerOrderId, String exchangeOrderId,
      long exchangeOrderTime) {
    super();
    this.orderActorStates = orderActorStates;
    this.clientCode = clientCode;
    this.brokerOrderId = brokerOrderId;
    this.exchangeOrderId = exchangeOrderId;
    this.exchangeOrderTime = exchangeOrderTime;
  }
}