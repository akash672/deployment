package com.wt.domain.write.events;

import com.wt.domain.OrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderReceivedByBroker extends OrderEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private OrderActorStates orderActorStates;
  @Getter
  private String localOrderId;
  @Getter
  private String brokerOrderId;

  public OrderReceivedByBroker(OrderActorStates orderActorStates, String localOrderId, String brokerOrderId) {
    super();
    this.orderActorStates = orderActorStates;
    this.localOrderId = localOrderId;
    this.brokerOrderId = brokerOrderId;
  }

}