package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class MutualFundOrderReceivedByDealer extends OrderEvent {
	private static final long serialVersionUID = 1L;
	private String localOrderId;
	private String exchangeOrderId;

}