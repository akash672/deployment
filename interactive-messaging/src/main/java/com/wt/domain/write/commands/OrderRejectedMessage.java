package com.wt.domain.write.commands;

import lombok.Getter;

@Getter
public class OrderRejectedMessage {
  private String wdOrderId;
  private String clientCode;

  public OrderRejectedMessage(String wdOrderId, String clientCode) {
    super();
    this.wdOrderId = wdOrderId;
    this.clientCode = clientCode;
  }

}
