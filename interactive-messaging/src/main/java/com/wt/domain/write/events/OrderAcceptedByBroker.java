package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class OrderAcceptedByBroker extends OrderRootEvent {
  private static final long serialVersionUID = 1L;
  private String localOrderID;
  private String brokerOrderID;

  public OrderAcceptedByBroker(String wdOrderId,String localOrderID, String brokerOrderID) {
    super(wdOrderId);
    this.localOrderID = localOrderID;
    this.brokerOrderID = brokerOrderID;
  }

}
