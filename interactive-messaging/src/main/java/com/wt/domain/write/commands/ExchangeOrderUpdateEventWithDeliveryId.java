package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.write.events.ExchangeOrderUpdateEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class ExchangeOrderUpdateEventWithDeliveryId implements Serializable {

  private static final long serialVersionUID = 1L;
  private ExchangeOrderUpdateEvent exchangeOrderUpdateEvent;
  private long deliveryId;
  private String securityType;

  public String senderPath() {
    return exchangeOrderUpdateEvent.senderPath();
  }
}
