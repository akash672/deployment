package com.wt.domain.write.events;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class RetryRejectedAdvise implements Serializable {

  private static final long serialVersionUID = 1L;
  private String rejectedAdviseDetails;
  private String basketOrderId;
  private BrokerAuthInformation brokerAuthInformation;

  @JsonCreator
  public RetryRejectedAdvise(@JsonProperty("rejectedAdviseDetails") String rejectedAdviseDetails,
      @JsonProperty("basketOrderId") String basketOrderId,
      @JsonProperty("brokerAuthInformation") BrokerAuthInformation brokerAuthInformation) {
    this.rejectedAdviseDetails = rejectedAdviseDetails;
    this.basketOrderId = basketOrderId;
    this.brokerAuthInformation = brokerAuthInformation;
  }
}