package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderCancellationRequest extends OrderRootEvent{
  
  private static final long serialVersionUID = 1L;
  private short localOrderID;

  public OrderCancellationRequest(String clientOrderId, short localOrderID) {
    super(clientOrderId);
    this.localOrderID = localOrderID;
  }
  
  

}
