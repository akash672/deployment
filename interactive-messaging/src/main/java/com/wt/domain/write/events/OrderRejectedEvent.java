package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderRejectedEvent extends ExchangeOrderUpdateEvent {
  
  private static final long serialVersionUID = 1L;
  @Getter private String rejectionMessage;
  
  public OrderRejectedEvent(DestinationAddress destinationAddress, String rejectionMessage) {
    super(destinationAddress);
    this.rejectionMessage = rejectionMessage;
  }

}
