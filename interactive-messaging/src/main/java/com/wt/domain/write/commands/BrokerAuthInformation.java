package com.wt.domain.write.commands;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class BrokerAuthInformation implements Serializable {

  private static final long serialVersionUID = 1L;
  @Expose
  private String sessionId;

  @JsonCreator
  public BrokerAuthInformation(@JsonProperty("sessionId") String sessionId) {
    super();
    this.sessionId = sessionId;
  }

  public static BrokerAuthInformation withNoBrokerAuth() {
   return new BrokerAuthInformation("");
  }
  
  public static BrokerAuthInformation withBrokerAuth(String sessionId) {
    return new BrokerAuthInformation(sessionId);
   }

}
