package com.wt.domain.write.commands;

import com.wt.domain.CtclMessageType;
import com.wt.domain.OrderSide;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class BrokerOrderAccepted extends CtclMessageType {

  private static final long serialVersionUID = 1L;

  public BrokerOrderAccepted(String localOrderID, String brokerOrderID, String clientCode, String token,
      int quantity, OrderSide orderSide) {
    super(localOrderID, brokerOrderID, clientCode, token, quantity, orderSide);
  }

}
