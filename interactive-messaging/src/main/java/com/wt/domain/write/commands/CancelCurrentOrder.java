package com.wt.domain.write.commands;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderActorStates;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.Purpose;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode()
public class CancelCurrentOrder {
  
  private OrderActorStates orderActorStates;
  private String ClientCode;
  private String localOrderId;
  private OrderExecutionMetaData orderExecutionMetaData;
  private String exchangeOrderId;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private String exchange;
}
