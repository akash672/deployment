package com.wt.domain.write.events;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderActorStates;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.Purpose;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class CancelOrderGenerationInProcess extends CancelOrderEvent {

  private static final long serialVersionUID = 1L;
  private OrderActorStates orderActorStates;
  private String clientCode;
  private String localOrderId;
  private OrderExecutionMetaData orderExecutionMetaData;
  private String exchangeOrderId;;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private String exchange;

  public CancelOrderGenerationInProcess(OrderActorStates orderActorStates, String clientCode, 
      String localOrderId, OrderExecutionMetaData orderExecutionMetaData,
      String exchangeOrderId, BrokerName brokerName, Purpose purpose, 
      InvestingMode investingMode, String exchange) {
    super();
    this.orderActorStates = orderActorStates;
    this.clientCode = clientCode;
    this.localOrderId = localOrderId;
    this.orderExecutionMetaData = orderExecutionMetaData;
    this.exchangeOrderId = exchangeOrderId;
    this.brokerName = brokerName;
    this.purpose = purpose;
    this.investingMode = investingMode;
    this.exchange = exchange;
  }

}
