package com.wt.domain.write.events;

import com.wt.domain.OrderSide;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@Getter
public class MutualFundTradeExecuted extends OrderEvent {
  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  private OrderSide side;
  private double quantity;
  private double nav;
}