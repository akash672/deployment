package com.wt.domain.write.events;

import com.wt.domain.OrderActorStates;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class ActorTerminatedOnStateTimeOut extends OrderEvent {

  private static final long serialVersionUID = 1L;
  private OrderActorStates orderState;
  
}
