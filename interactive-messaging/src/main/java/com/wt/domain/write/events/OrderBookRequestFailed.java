package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class OrderBookRequestFailed extends OrderEvent {

  private static final long serialVersionUID = 1L;

}
