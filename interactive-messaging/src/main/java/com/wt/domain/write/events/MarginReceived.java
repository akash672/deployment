package com.wt.domain.write.events;

import com.wt.domain.InvestingMode;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class MarginReceived extends UserEvent {

  private static final long serialVersionUID = 1L;
  private double marginAmount;
  private InvestingMode investingMode;
  
  public MarginReceived(double marginAmount, String username, InvestingMode investingMode) {
    super(username);
    this.marginAmount = marginAmount;
    this.investingMode = investingMode;
  }

}
