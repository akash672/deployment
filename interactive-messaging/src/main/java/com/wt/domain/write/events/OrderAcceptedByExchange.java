package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class OrderAcceptedByExchange extends OrderRootEvent {

  private static final long serialVersionUID = 1L;
  private String brokerOrderId;
  private String exchOrderId;
  

  public OrderAcceptedByExchange(String wdOrderID,String brokerOrderId, String exchOrderId) {
    super(wdOrderID);
    this.brokerOrderId = brokerOrderId;
    this.exchOrderId = exchOrderId;
  }

}
