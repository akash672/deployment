package com.wt.domain.write;

import static com.wt.domain.CancelOrderActorStates.CancelOrderBeingGenerated;
import static com.wt.domain.CancelOrderActorStates.CancelOrderGenerated;
import static com.wt.domain.CancelOrderActorStates.CancelOrderReceivedbyBroker;
import static com.wt.domain.CancelOrderActorStates.CancelOrderRejectedByBroker;
import static com.wt.domain.CancelOrderActorStates.CancelOrderRejectedByExchange;
import static com.wt.domain.CancelOrderActorStates.CancellationConfirmed;
import static java.util.concurrent.TimeUnit.SECONDS;
import static scala.concurrent.duration.Duration.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CtclInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.CTCLInteractiveFactory;
import com.wt.domain.CancelOrder;
import com.wt.domain.CancelOrderActorStates;
import com.wt.domain.CtclOrderType;
import com.wt.domain.OrderBookMessage;
import com.wt.domain.write.commands.CancelConfirmation;
import com.wt.domain.write.commands.CancelCurrentOrder;
import com.wt.domain.write.commands.CancelRejected;
import com.wt.domain.write.commands.ReceivedByBroker;
import com.wt.domain.write.commands.RejectedByBroker;
import com.wt.domain.write.commands.RejectedByExchange;
import com.wt.domain.write.commands.SentToExchange;
import com.wt.domain.write.commands.StateMachineTerminationCommand;
import com.wt.domain.write.commands.SubscribeToExchangeUpdateEventStream;
import com.wt.domain.write.events.CancelOrderBookRequestFailed;
import com.wt.domain.write.events.CancelOrderConfirmation;
import com.wt.domain.write.events.CancelOrderEvent;
import com.wt.domain.write.events.CancelOrderGenerationInProcess;
import com.wt.domain.write.events.CancelOrderNotSentToBroker;
import com.wt.domain.write.events.CancelOrderReceivedByBroker;
import com.wt.domain.write.events.CancelOrderRejected;
import com.wt.domain.write.events.CancelOrderStateTimedOut;
import com.wt.domain.write.events.OrderBookRequestNotSent;
import com.wt.domain.write.events.OrderNotSent;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.persistence.fsm.AbstractPersistentFSM;
import lombok.Getter;
import lombok.extern.log4j.Log4j;



@Log4j
@Service("CancelOrderActor")
@Scope("prototype")
public class CancelOrderActor  extends AbstractPersistentFSM<CancelOrderActorStates, CancelOrder, CancelOrderEvent> {

  private CTCLInteractiveFactory ctclFactory;
  @Getter
  private String persistenceId;

  @Autowired
  public void setCTCLFactory(CTCLInteractiveFactory ctclFactory) {
    this.ctclFactory = ctclFactory;
  }
  
  public CancelOrderActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
    
    startWith(CancelOrderBeingGenerated, new CancelOrder());
   
    when(CancelOrderBeingGenerated, 
        matchEvent(CancelCurrentOrder.class, 
            (cancelCurrentOrder, orderData) -> {
        return goTo(CancelOrderGenerated).
           applying(new CancelOrderGenerationInProcess(
              cancelCurrentOrder.getOrderActorStates(), 
              cancelCurrentOrder.getClientCode(),
              cancelCurrentOrder.getLocalOrderId(), 
              cancelCurrentOrder.getOrderExecutionMetaData(),
              cancelCurrentOrder.getExchangeOrderId(),
              cancelCurrentOrder.getBrokerName(),
              cancelCurrentOrder.getPurpose(),
              cancelCurrentOrder.getInvestingMode(),
              cancelCurrentOrder.getExchange())
             ).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              CtclOrderType.CANCEL.sendCancelOrder(ctcl, orderData);
            } catch (Exception e) {
              self().tell(new OrderNotSent(data.getExchangeOrderId()), self());
            }
          }));

    }));

    
    when(CancelOrderGenerated, create(60, "seconds"), 
        matchEvent(ReceivedByBroker.class, 
            (receivedByBroker, orderData) -> {
        return goTo(CancelOrderReceivedbyBroker).
           applying(new CancelOrderReceivedByBroker(
              stateName(),
              receivedByBroker.getLocalOrderId(), 
              receivedByBroker.getBrokerOrderId())).
           andThen(exec(data -> {
            
          }));

    }).
        event(RejectedByBroker.class, 
            (rejectedByBroker, orderData) -> {
        return goTo(CancelOrderRejectedByBroker).
           applying(new CancelOrderRejected(
             stateName(),
             rejectedByBroker.getLocalOrderId(), 
             rejectedByBroker.getMessage())).
           andThen(exec(data -> {
        
             context().parent().tell(new CancelRejected(rejectedByBroker.getMessage(), orderData.getLocalOrderId(), String.valueOf(rejectedByBroker.getLocalOrderId())),self());
             self().tell(PoisonPill.getInstance(), self());
         }));

  }). 
        event(OrderNotSent.class, 
            (orderNotSent, orderData) -> {
        return stay().
            applying(new CancelOrderNotSentToBroker()).
            andThen(exec(data -> {
              CtclInteractive ctcl;
              try {
                ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                CtclOrderType.CANCEL.sendCancelOrder(ctcl, orderData);
              } catch (Exception e) {
                context().system().scheduler().scheduleOnce(
                create(20, SECONDS), 
                self(),
                new OrderNotSent(data.getExchangeOrderId()),
                context().system().dispatcher(), ActorRef.noSender());
              }
         }));
  }).
        event(OrderBookRequestNotSent.class, 
            (orderNotSent, orderData) -> {
        return stay().
            applying(new CancelOrderBookRequestFailed()).
            andThen(exec(data -> {
              CtclInteractive ctcl;
              try {
                ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                log.debug("Asking for order book from " + ctcl);
                ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
              } catch (Exception e) {
                context().system().scheduler().scheduleOnce(
                create(20, SECONDS), 
                self(),
                new OrderBookRequestNotSent(data.getExchangeOrderId()),
                context().system().dispatcher(), ActorRef.noSender());
               }
        }));
 }).
       eventEquals(StateTimeout(), 
            (stateTimeout, orderData) -> {
       return stay().
           applying(new CancelOrderStateTimedOut()).
           andThen(exec(data -> {
             CtclInteractive ctcl;
             try {
               ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
               log.debug("Asking for order book from " + ctcl);
               ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
             } catch (Exception e) {
               self().tell(new OrderBookRequestNotSent(data.getExchangeOrderId()), self());
            }
       }));
 })
     );
    
    
    when(CancelOrderReceivedbyBroker, create(60, "seconds"), 
         matchEvent(SentToExchange.class, 
             (sentToExchange, orderData) -> {
         return goTo(CancellationConfirmed).
            applying(new CancelOrderConfirmation()).
            andThen(exec(data -> {
            
            context().parent().tell(new CancelConfirmation(sentToExchange.getExchangeOrderId(),sentToExchange.getBrokerOrderId()), self());
            
            self().tell(PoisonPill.getInstance(), self());
            
       }));

  }).
       event(RejectedByExchange.class, 
            (rejectedByExchange, orderData) -> {
       return goTo(CancelOrderRejectedByExchange).
          applying(new CancelOrderRejected(
                  stateName(), 
                  rejectedByExchange.getBrokerOrderId(), 
                  rejectedByExchange.getMessage())).
          andThen(exec(data -> {
                context().parent().tell(new CancelRejected(rejectedByExchange.getMessage(), orderData.getLocalOrderId(), String.valueOf(rejectedByExchange.getBrokerOrderId())),self());
                self().tell(PoisonPill.getInstance(), self());   
              
      }));
 }). 
        
       event(OrderBookRequestNotSent.class, 
            (orderNotSent, orderData) -> {
       return stay().
          applying(new CancelOrderBookRequestFailed()).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              log.debug("Asking for order book from " + ctcl);
              ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
            } catch (Exception e) {
              context().system().scheduler().scheduleOnce(
              create(20, SECONDS), 
              self(),
              new OrderBookRequestNotSent(data.getExchangeOrderId()),
              context().system().dispatcher(), ActorRef.noSender());
            }
     }));
  }).
       eventEquals(StateTimeout(), 
            (stateTimeout, orderData) -> {
       return stay().
          applying(new CancelOrderStateTimedOut()).
          andThen(exec(data -> {
            
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              log.debug("Asking for order book from " + ctcl);
              ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
            } catch (Exception e) {
              self().tell(new OrderBookRequestNotSent(data.getExchangeOrderId()), self());
            }
     }));
  })
        );
        
        
    when(CancellationConfirmed, matchEvent(StateMachineTerminationCommand.class, (stateExceptionCommand, orderData) -> {
      return stay();
    }));
    
    when(CancelOrderRejectedByExchange, matchEvent(StateMachineTerminationCommand.class, (stateExceptionCommand, orderData) -> {
      return stay();
    }));

    when(CancelOrderRejectedByBroker, matchEvent(StateMachineTerminationCommand.class, (stateExceptionCommand, orderData) -> {
      return stay();
    }));

        
    whenUnhandled(
        matchEvent(SubscribeToExchangeUpdateEventStream.class, 
            (event, orderData) -> {
            log.debug(self() + " is subscribed to exchange update stream in state " + stateName());
            return stay();
        }).
        event(SentToExchange.class,
            (sentToExchange,orderData) -> { 
         return (stateName().equals(CancelOrderGenerated)) ? true : false;
            }, 
         (sentToExchange,orderData) -> {
          return goTo(CancellationConfirmed)
          .applying(new CancelOrderConfirmation())
          .andThen(exec(data -> {
            
            context().parent().tell(new CancelConfirmation(sentToExchange.getExchangeOrderId(), sentToExchange.getBrokerOrderId()), self());
            
            self().tell(PoisonPill.getInstance(), self());
            
          }));

    }).
        event(RejectedByExchange.class,
            (rejectedByExchange, orderData) -> {
         return (stateName().equals(CancelOrderGenerated)) ? true : false;
            }, 
         (rejectedByExchange, orderData) -> {
           return goTo(CancelOrderRejectedByExchange)
               .applying(new CancelOrderRejected(
                   stateName(), 
                   rejectedByExchange.getBrokerOrderId(), 
                   rejectedByExchange.getMessage()))
               .andThen(exec(data -> {
                 
                 context().parent().tell(new CancelRejected(rejectedByExchange.getMessage(), orderData.getLocalOrderId(), String.valueOf(rejectedByExchange.getBrokerOrderId())),self());
                 self().tell(PoisonPill.getInstance(), self());   
               
               }));
        })
        .event(OrderBookMessage.class,
            (orderBookMessage,orderData) -> {
            return (stateName().equals(CancelOrderGenerated)  
                && orderBookMessage.getMessage().equals("Cancelled")) ? true : false; 
                                                                                    }, 
            (orderBookMessage,orderData) -> {
              return goTo(CancellationConfirmed).
                  applying(new CancelOrderConfirmation()).
                  andThen(exec(data -> {
                    
                    context().parent().tell(new CancelConfirmation(String.valueOf(orderBookMessage.getExchangeOrderId()), String.valueOf(orderBookMessage.getBrokerOrderId())), self());
                    
                    self().tell(PoisonPill.getInstance(), self());
                    
                  }));
        })
        .event(OrderBookMessage.class,
            (orderBookMessage,orderData) -> {
            return (stateName().equals(CancelOrderGenerated)  
                && !orderBookMessage.getMessage().equals("Cancelled")) ? true : false; 
                                                                                    }, 
            (orderBookMessage,orderData) -> {
              return goTo(CancelOrderRejectedByExchange)
                  .applying(new CancelOrderRejected(
                      stateName(), 
                      String.valueOf(orderBookMessage.getBrokerOrderId()), 
                      orderBookMessage.getMessage()))
                  .andThen(exec(data -> {
                    
                    context().parent().tell(new CancelRejected(orderBookMessage.getMessage(), orderData.getLocalOrderId(), String.valueOf(orderBookMessage.getBrokerOrderId())),self());
                    self().tell(PoisonPill.getInstance(), self());   
                  
                  }));
        }).
          event(OrderBookMessage.class,
            (orderBookMessage,orderData) -> {
            return (stateName().equals(CancelOrderReceivedbyBroker)  
                && !orderBookMessage.getMessage().equals("Cancelled")) ? true : false; 
                                                                                    }, 
            (orderBookMessage,orderData) -> {
              return goTo(CancelOrderRejectedByExchange)
                  .applying(new CancelOrderRejected(
                      stateName(), 
                      String.valueOf(orderBookMessage.getBrokerOrderId()), 
                      orderBookMessage.getMessage()))
                  .andThen(exec(data -> {
                    context().parent().tell(new CancelRejected(orderBookMessage.getMessage(), orderData.getLocalOrderId(), String.valueOf(orderBookMessage.getBrokerOrderId())),self());
                    self().tell(PoisonPill.getInstance(), self());   
                  
                  }));
        }).
          event(OrderBookMessage.class,
              (orderBookMessage,orderData) -> {
              return (stateName().equals(CancelOrderReceivedbyBroker)  
                  && orderBookMessage.getMessage().equals("Cancelled")) ? true : false; 
                                                                                      }, 
              (orderBookMessage,orderData) -> {
                return goTo(CancellationConfirmed).
                    applying(new CancelOrderConfirmation()).
                    andThen(exec(data -> {
                      
                      context().parent().tell(new CancelConfirmation(String.valueOf(orderBookMessage.getExchangeOrderId()), String.valueOf(orderBookMessage.getBrokerOrderId())), self());
                      
                      self().tell(PoisonPill.getInstance(), self());
                      
                    }));
          })
         );
        
  }
  
  public CancelOrder applyEvent(CancelOrderEvent event, CancelOrder data) {
    data.update(event);
    return data;
  }
  @Override
  public String persistenceId() {
    return persistenceId();
  }

  @Override
  public Class<CancelOrderEvent> domainEventClass() {
    return CancelOrderEvent.class;
  }

}
