package com.wt.domain.write.commands;

import com.wt.domain.OrderSide;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class HasCorpActionOrderBeenExecutedQuery extends OrderRootCommand {

  private static final long serialVersionUID = 1L;
  @Getter private String clientOrderId;
  @Getter private String wdId;
  @Getter private String exchangeOrderId;
  @Getter private OrderSide side;
  @Getter private int quantity;
}
