package com.wt.domain.write.commands;

import com.wt.domain.OrderExecutionMetaData;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class OrderSuccessfullySentToCTCL extends OrderCommand {

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String localOrderId;
  private OrderExecutionMetaData orderExecutionMetadata;

}
