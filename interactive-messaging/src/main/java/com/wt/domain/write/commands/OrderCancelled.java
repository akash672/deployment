package com.wt.domain.write.commands;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class OrderCancelled {
  private String clientOrderId;
  private String clientCode;
  private String exchangeOrderId;
  private String brokerOrderId;
  

  public OrderCancelled(String wdOrderId, String clientCode, String exchangeOrderId, String brokerOrderId) {
    super();
    this.clientOrderId = wdOrderId;
    this.clientCode = clientCode;
    this.exchangeOrderId = exchangeOrderId;
    this.brokerOrderId = brokerOrderId;
  }

}
