package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class EquityInstrumentSegmentUpdated implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private String wdId;
  @Getter private String symbol;
  @Getter private String exchange;
  @Getter private String newSegment;
  
   
}
