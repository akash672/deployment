package com.wt.domain.write;

import static com.wt.domain.OrderActorStates.CancellationOrderPlaced;
import static com.wt.domain.OrderActorStates.Cancelled;
import static com.wt.domain.OrderActorStates.FullyExecuted;
import static com.wt.domain.OrderActorStates.OrderBeingGenerated;
import static com.wt.domain.OrderActorStates.OrderGenerated;
import static com.wt.domain.OrderActorStates.OrderReceivedbyBroker;
import static com.wt.domain.OrderActorStates.OrderRejectedByBroker;
import static com.wt.domain.OrderActorStates.OrderRejectedByExchange;
import static com.wt.domain.OrderActorStates.OrderSenttoExchange;
import static com.wt.domain.OrderActorStates.PartiallyExecuted;
import static java.util.concurrent.TimeUnit.SECONDS;
import static scala.concurrent.duration.Duration.create;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CtclInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.CTCLInteractiveFactory;
import com.wt.domain.CtclEquityOrder;
import com.wt.domain.DestinationAddress;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderActorStates;
import com.wt.domain.OrderBookMessage;
import com.wt.domain.OrderSpecification;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Trade;
import com.wt.domain.write.commands.CancelConfirmation;
import com.wt.domain.write.commands.CancelCurrentOrder;
import com.wt.domain.write.commands.CancelRejected;
import com.wt.domain.write.commands.OrderCancellationRejected;
import com.wt.domain.write.commands.OrderCancelled;
import com.wt.domain.write.commands.OrderFullyTraded;
import com.wt.domain.write.commands.OrderRejectedMessage;
import com.wt.domain.write.commands.OrderSuccessfullySentToCTCL;
import com.wt.domain.write.commands.OrderTradeConfirmation;
import com.wt.domain.write.commands.ProvideOrderBook;
import com.wt.domain.write.commands.ProvideTradeBook;
import com.wt.domain.write.commands.ReceivedByBroker;
import com.wt.domain.write.commands.RejectedByBroker;
import com.wt.domain.write.commands.RejectedByExchange;
import com.wt.domain.write.commands.SentToExchange;
import com.wt.domain.write.commands.ShuttingDownActorAtEOD;
import com.wt.domain.write.commands.StateMachineTerminationCommand;
import com.wt.domain.write.commands.SubscribeToExchangeUpdateEventStream;
import com.wt.domain.write.events.ActorTerminatedOnStateTimeOut;
import com.wt.domain.write.events.CancelConfirmed;
import com.wt.domain.write.events.CancelRejectedEvent;
import com.wt.domain.write.events.ForwardingToChild;
import com.wt.domain.write.events.OrderActorCreated;
import com.wt.domain.write.events.OrderBookRequestFailed;
import com.wt.domain.write.events.OrderBookRequestNotSent;
import com.wt.domain.write.events.OrderCancellationInProcess;
import com.wt.domain.write.events.OrderCancellationRequest;
import com.wt.domain.write.events.OrderEvent;
import com.wt.domain.write.events.OrderGenerationInProcess;
import com.wt.domain.write.events.OrderHasBeenSuccessfullySentToCTCL;
import com.wt.domain.write.events.OrderNotSent;
import com.wt.domain.write.events.OrderNotSentToBroker;
import com.wt.domain.write.events.OrderPlacedWithExchange;
import com.wt.domain.write.events.OrderReceivedByBroker;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.OrderRejected;
import com.wt.domain.write.events.OrderRejectedByBroker;
import com.wt.domain.write.events.OrderRejectedByExchange;
import com.wt.domain.write.events.OrderSentToExchange;
import com.wt.domain.write.events.OrderStateTimedOut;
import com.wt.domain.write.events.OrderTerminatedAtEOD;
import com.wt.domain.write.events.OrderTraded;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.ProvidingOrderBook;
import com.wt.domain.write.events.ProvidingTradeBook;
import com.wt.domain.write.events.ReceivedRequestToProvideOrderBook;
import com.wt.domain.write.events.ReceivedRequestToProvideTradeBook;
import com.wt.domain.write.events.TradeBookRequestFailed;
import com.wt.domain.write.events.TradeBookRequestNotSent;
import com.wt.domain.write.events.TradeCancelled;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.utils.DateUtils;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.persistence.fsm.AbstractPersistentFSM;
import lombok.extern.log4j.Log4j;

@Log4j
@Service("EquityOrderActor")
@Scope("prototype")
public class EquityOrderActor extends AbstractPersistentFSM<OrderActorStates, CtclEquityOrder, OrderEvent>  implements ParentActor<CancelCurrentOrder> {
  private CTCLInteractiveFactory ctclFactory;

  @Autowired
  public void setCTCLFactory(CTCLInteractiveFactory ctclFactory) {
    this.ctclFactory = ctclFactory;
  }
  
  private String persistenceId;
  
  public EquityOrderActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
    startWith(OrderBeingGenerated, new CtclEquityOrder());

    when(OrderBeingGenerated, 
        matchEvent(OrderActorCreated.class, 
            (orderActorCreated, orderData) -> {
      return goTo(OrderGenerated).
          applying(new OrderGenerationInProcess(
              stateName(), 
              orderActorCreated.getClientCode(),
              orderActorCreated.getOrderExecutionMetaData(), 
              new DestinationAddress(orderActorCreated.getSenderPath()),
              String.valueOf(orderActorCreated.getLocalOrderId()),
              orderActorCreated.getClientOrderId(),
              orderActorCreated.getBrokerName(),
              orderActorCreated.getExchange(),
              orderActorCreated.getPurpose(), 
              orderActorCreated.getInvestingMode(),
              orderActorCreated.getBrokerAuthInformation())).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              orderData.getOrderType().sendOrder(ctcl, orderData);
              self().tell(new OrderSuccessfullySentToCTCL(
                  orderActorCreated.getClientCode(),  
                  String.valueOf(orderActorCreated.getLocalOrderId()), 
                  orderActorCreated.getOrderExecutionMetaData()), self());
              
            } catch (Exception e) {
              log.error("Error while sending order to CTCL: "+e.getMessage());
              self().tell(new OrderNotSent(data.getClientOrderId()), self());
            }
          }));
    }));
    
    when(OrderGenerated, create(60, "seconds"), 
        matchEvent(OrderSuccessfullySentToCTCL.class, 
            (orderSuccessfullySent, orderData) -> {
      return stay().
          applying(new OrderHasBeenSuccessfullySentToCTCL(
              orderSuccessfullySent.getClientCode(), 
              orderSuccessfullySent.getLocalOrderId(), 
              orderSuccessfullySent.getOrderExecutionMetadata()));

    }).
        event(ReceivedByBroker.class, 
            (receivedByBroker, orderData) -> {
      return goTo(OrderReceivedbyBroker).
          applying(new OrderReceivedByBroker(
              stateName(),
              receivedByBroker.getLocalOrderId(), 
              receivedByBroker.getBrokerOrderId())).
          andThen(exec(data -> {
            context().parent().tell(new OrderReceivedWithBroker
                (orderData.getDestinationAddress(),
                orderData.getClientOrderId(), 
                receivedByBroker.getBrokerOrderId()), self());
          }));

    }).
        event(OrderNotSent.class, 
        (orderNotSent, orderData) -> {
        return stay()
               .applying(new OrderNotSentToBroker())
               .andThen(exec(data -> {
                  CtclInteractive ctcl;
                  try {
                    ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                    orderData.getOrderType().sendOrder(ctcl, orderData);
                  } catch (Exception e) {
                    context().system().scheduler().scheduleOnce(
                        create(20, SECONDS), 
                        self(),
                        new OrderNotSent(data.getClientOrderId()),
                        context().system().dispatcher(), ActorRef.noSender());
                  }
                }));
    }).
        event(RejectedByBroker.class, 
            (rejectedByBroker, orderData) -> {
      return goTo(OrderRejectedByBroker)
          .applying(new OrderRejected(
              stateName(),
              rejectedByBroker.getLocalOrderId(), 
              rejectedByBroker.getMessage()))
          .andThen(exec(data -> {
           
            context().parent().tell(new OrderRejectedByBroker(
                orderData.getDestinationAddress(),
                rejectedByBroker.getLocalOrderId(), 
                rejectedByBroker.getMessage()), self());
            
            context().parent()
            .tell(new OrderRejectedMessage(
                orderData.getClientOrderId(), orderData.getClientCode()), self());
            
            self().tell(PoisonPill.getInstance(), self());

          }));

    }).
        event(OrderBookRequestNotSent.class, 
        (orderNotSent, orderData) -> {
        return stay()
               .applying(new OrderBookRequestFailed())
               .andThen(exec(data -> {
                  CtclInteractive ctcl;
                  try {
                    ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                    log.debug("Asking for order book from " + ctcl + " for client order id " + orderData.getClientOrderId() + " with exchange order " + orderData.getExchangeOrderId());
                    ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
                  } catch (Exception e) {
                    context().system().scheduler().scheduleOnce(
                        create(20, SECONDS), 
                        self(),
                        new OrderBookRequestNotSent(data.getClientOrderId()),
                        context().system().dispatcher(), ActorRef.noSender());
                  }
                }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return orderData.isCtclOrderSentSuccessfully() && DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new OrderStateTimedOut()).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              log.debug("Asking for order book from " + ctcl + " for client order id " + orderData.getClientOrderId() + " with exchange order " + orderData.getExchangeOrderId());
              ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
            } catch (Exception e) {
              self().tell(new OrderBookRequestNotSent(data.getClientOrderId()), self());
            }
          }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return !orderData.isCtclOrderSentSuccessfully() && DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new OrderStateTimedOut()).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              orderData.getOrderType().sendOrder(ctcl, orderData);
            } catch (Exception e) {
              log.error("Error while sending order to CTCL: "+ e.getMessage());
              self().tell(new OrderNotSent(data.getClientOrderId()), self());
            }
          }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return !DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new ActorTerminatedOnStateTimeOut(stateName())).
          andThen(exec(data -> {
            
            context().parent().tell(new OrderRejectedByBroker(
                orderData.getDestinationAddress(),
                orderData.getLocalOrderId(), 
                "Order rejected as the broker failed to provide a response during market hours"), self());
            
            context().parent()
            .tell(new OrderRejectedMessage(
                orderData.getClientOrderId(), orderData.getClientCode()), self());
            
            context().parent()
            .tell(new ShuttingDownActorAtEOD(
               stateName(), orderData.getClientCode(), orderData.getClientOrderId()), self());
            
            self().tell(PoisonPill.getInstance(), self());
            
          }));
    }));
        
    when(OrderReceivedbyBroker, create(60, "seconds"), 
        matchEvent(SentToExchange.class, 
            (sentToExchange, orderData) -> {
      OrderSpecification orderSpecification = new OrderSpecification(sentToExchange.getClientCode(),
                                                                     sentToExchange.getToken(), 
                                                                     sentToExchange.getQuantity(), 
                                                                     sentToExchange.getOrderSide());
      return (orderSpecification.equals(orderData.getOrderSpecification())) ? true : false; 
                                           },
            (sentToExchange, orderData) -> {
      return goTo(OrderSenttoExchange).
          applying(new OrderSentToExchange(
              stateName(), 
              orderData.getClientCode(),
              sentToExchange.getBrokerOrderId(),
              sentToExchange.getExchangeOrderId(), 
              sentToExchange.getExchangeOrderTime())).
          andThen(exec(data -> {
            
            context().parent()
                .tell(new OrderPlacedWithExchange(
                    orderData.getDestinationAddress(), 
                    sentToExchange.getBrokerOrderId(),
                    sentToExchange.getExchangeOrderId(), 
                    sentToExchange.getBrokerOrderId(), 
                    orderData.getSide(),
                    orderData.getIntegerQuantity()), self());
          }));
    }).
        event(SentToExchange.class, 
            (sentToExchange, orderData) -> {
      OrderSpecification orderSpecification = new OrderSpecification(sentToExchange.getClientCode(),
                                                                     sentToExchange.getToken(), 
                                                                     sentToExchange.getQuantity(), 
                                                                     sentToExchange.getOrderSide());
      return (orderSpecification.equals(orderData.getOrderSpecification())) ? false : true; 
                                           },
            (sentToExchange, orderData) -> {
      return stay();
    }).
        event(RejectedByExchange.class, 
        (rejectedByExchange, orderData) -> {
      OrderSpecification orderSpecification = new OrderSpecification(rejectedByExchange.getClientCode(), 
                                                                     rejectedByExchange.getToken(), 
                                                                     rejectedByExchange.getQuantity(), 
                                                                     rejectedByExchange.getOrderSide());
      return (orderSpecification.equals(orderData.getOrderSpecification())) ? true : false; 
                                           },
        (rejectedByExchange, orderData) -> {
      return goTo(OrderRejectedByExchange)
          .applying(new OrderRejected(
              stateName(), 
              rejectedByExchange.getBrokerOrderId(), 
              rejectedByExchange.getMessage()))
          .andThen(exec(data -> {
            
            context().parent()
            .tell(new OrderRejectedByExchange(
                orderData.getDestinationAddress(),
                rejectedByExchange.getBrokerOrderId(), 
                rejectedByExchange.getMessage()), self());
            
            context().parent()
            .tell(new OrderRejectedMessage(
                orderData.getClientOrderId(), orderData.getClientCode()), self());
            
            self().tell(PoisonPill.getInstance(), self());
          }));
    }). 
        event(RejectedByExchange.class, 
            (rejectedByExchange, orderData) -> {
          OrderSpecification orderSpecification = new OrderSpecification(rejectedByExchange.getClientCode(), 
                                                                         rejectedByExchange.getToken(), 
                                                                         rejectedByExchange.getQuantity(), 
                                                                         rejectedByExchange.getOrderSide());
          return (orderSpecification.equals(orderData.getOrderSpecification())) ? false : true; 
                                               },
            (rejectedByExchange, orderData) -> {
          return stay();
    }).
        event(OrderBookRequestNotSent.class, 
        (orderNotSent, orderData) -> {
        return stay()
               .applying(new OrderBookRequestFailed())
               .andThen(exec(data -> {
                  CtclInteractive ctcl;
                  try {
                    ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                    log.debug("Asking for order book from " + ctcl + " for client order id " + orderData.getClientOrderId() + " with exchange order " + orderData.getExchangeOrderId());
                    ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
                  } catch (Exception e) {
                    context().system().scheduler().scheduleOnce(
                        create(20, SECONDS), 
                        self(),
                        new OrderBookRequestNotSent(data.getClientOrderId()),
                        context().system().dispatcher(), ActorRef.noSender());
                  }
                }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new OrderStateTimedOut()).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              log.debug("Asking for order book from " + ctcl + " for client order id " + orderData.getClientOrderId() + " with exchange order " + orderData.getExchangeOrderId());
              ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
            } catch (Exception e) {
              self().tell(new OrderBookRequestNotSent(data.getClientOrderId()), self());
            }
          }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return !DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new ActorTerminatedOnStateTimeOut(stateName())).
          andThen(exec(data -> {
            context().parent().tell(new OrderRejectedByExchange(
                orderData.getDestinationAddress(),
                orderData.getLocalOrderId(), 
                "Order rejected as the exchange failed to provide a response for the order during market hours"), self());
            
            context().parent()
            .tell(new OrderRejectedMessage(
                orderData.getClientOrderId(), orderData.getClientCode()), self());
            
            context().parent()
            .tell(new ShuttingDownActorAtEOD(
               stateName(), orderData.getClientCode(), orderData.getClientOrderId()), self());
            
            self().tell(PoisonPill.getInstance(), self());
            
          }));
    }));

    when(OrderSenttoExchange, create(60, "seconds"),
        matchEvent(OrderTradeConfirmation.class, 
            (orderTradeConfirmation, orderData) -> {
              
      int quantityRemaining = orderData.getIntegerQuantity() - orderTradeConfirmation.getQty();
      return (quantityRemaining != 0 
              && orderTradeConfirmation.getExchangeOrderId().equals(orderData.getExchangeOrderId())) ? true : false;
    }, (orderTradeConfirmation, orderData) -> {
      return goTo(PartiallyExecuted).
          applying(new OrderTraded(
              stateName(), 
              orderTradeConfirmation.getExchangeOrderId(),
              orderTradeConfirmation.getOrderConfirmationTime(), 
              orderTradeConfirmation.getPrice(),
              orderTradeConfirmation.getQty(), 
              orderTradeConfirmation.getTradeOrderId(),
              orderTradeConfirmation.getBrokerOrderId())).
          andThen(exec(data -> {
            context().parent()
            .tell(new TradeExecuted(
                orderData.getDestinationAddress(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getTradeOrderId(), 
                orderData.getSide(),
                PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                    data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                    Pafs.withCumulativePaf(1.)),
                orderTradeConfirmation.getQty()), self());

          }));

    }).
        event(OrderTradeConfirmation.class, 
            (orderTradeConfirmation, orderData) -> {
      int quantityRemaining = orderData.getIntegerQuantity() - orderTradeConfirmation.getQty();
      return (quantityRemaining == 0
              && orderTradeConfirmation.getExchangeOrderId().equals(orderData.getExchangeOrderId())) ? true : false;
    }, (orderTradeConfirmation, orderData) -> {
      return goTo(FullyExecuted).
          applying(new OrderTraded(
              stateName(), 
              orderTradeConfirmation.getExchangeOrderId(),
              orderTradeConfirmation.getOrderConfirmationTime(), 
              orderTradeConfirmation.getPrice(),
              orderTradeConfirmation.getQty(), 
              orderTradeConfirmation.getTradeOrderId(),
              orderTradeConfirmation.getBrokerOrderId())).
          andThen(exec(data -> {
            context().parent()
            .tell(new TradeExecuted(
                orderData.getDestinationAddress(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getTradeOrderId(), 
                orderData.getSide(),
                PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                    data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                    Pafs.withCumulativePaf(1.)),
                orderTradeConfirmation.getQty()), self());
            
            context().parent()
            .tell(new OrderFullyTraded(
                orderData.getClientOrderId(),
                orderData.getClientCode(),
                String.valueOf(orderData.getExchangeOrderId()),
                String.valueOf(orderData.getBrokerOrderId())), self());
            self().tell(PoisonPill.getInstance(), self());
          }));

    }).
        event(OrderCancellationRequest.class, 
            (orderCancellationRequest, orderData) -> {
      return goTo(CancellationOrderPlaced).
          applying(new OrderCancellationInProcess(
              stateName(),
              String.valueOf(orderCancellationRequest.getLocalOrderID()))). 
          andThen(exec(data -> {
            CancelCurrentOrder cancelCurrentOrder = new CancelCurrentOrder(stateName(),
                orderData.getClientCode(),
                String.valueOf(orderCancellationRequest.getLocalOrderID()),
                orderData.getOrderExecutionMetaData(),
                orderData.getExchangeOrderId(),
                orderData.getBrokerName(),
                orderData.getPurpose(),
                orderData.getInvestingMode(),
                orderData.getExchange());

            tellChildren(context(), self(),cancelCurrentOrder , "Cancel"+orderData.getClientOrderId());
            }));

    }).
        event(TradeBookRequestNotSent.class, 
        (orderNotSent, orderData) -> {
        return stay()
               .applying(new TradeBookRequestFailed())
               .andThen(exec(data -> {
                  CtclInteractive ctcl;
                  try {
                    ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                    log.debug("Asking for order book from " + ctcl);
                    ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
                  } catch (Exception e) {
                    context().system().scheduler().scheduleOnce(
                        create(20, SECONDS), 
                        self(),
                        new TradeBookRequestNotSent(data.getClientOrderId()),
                        context().system().dispatcher(), ActorRef.noSender());
                  }
                }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new OrderStateTimedOut()).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              log.debug("Asking for trade book from " + ctcl);
              ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
            } catch (Exception e) {
              self().tell(new TradeBookRequestNotSent(data.getClientOrderId()), self());
            }
          }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return !DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new ActorTerminatedOnStateTimeOut(stateName())).
          andThen(exec(data -> {
         
            context().parent()
            .tell(new OrderTerminatedAtEOD(
                orderData.getDestinationAddress(),
               stateName(), data.getSide(), data.getExchangeOrderId()), self());
            
            context().parent()
            .tell(new ShuttingDownActorAtEOD(
               stateName(), orderData.getClientCode(), orderData.getClientOrderId()), self());
            
            self().tell(PoisonPill.getInstance(), self());
            
          }));
    }));

    when(PartiallyExecuted, create(60, "seconds"),
        matchEvent(OrderTradeConfirmation.class, 
            (orderTradeConfirmation, orderData) -> {
      int quantityRemaining = orderData.getQuantityRemaining() - orderTradeConfirmation.getQty();
      return (quantityRemaining != 0
              && orderTradeConfirmation.getExchangeOrderId().equals(orderData.getExchangeOrderId())
              && !orderData.getTradeOrderIdsList().contains(orderTradeConfirmation.getTradeOrderId())) ? true : false;
    }, (orderTradeConfirmation, orderData) -> {
      return stay().
          applying(new OrderTraded(
              stateName(), 
              orderTradeConfirmation.getExchangeOrderId(),
              orderTradeConfirmation.getOrderConfirmationTime(),
              orderTradeConfirmation.getPrice(),
              orderTradeConfirmation.getQty(), 
              orderTradeConfirmation.getTradeOrderId(),
              orderTradeConfirmation.getBrokerOrderId())).
          andThen(exec(data -> {
            
            context().parent()
            .tell(new TradeExecuted(
                orderData.getDestinationAddress(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getTradeOrderId(), 
                orderData.getSide(),
                PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                    data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                    Pafs.withCumulativePaf(1.)),
                orderTradeConfirmation.getQty()), self());
          }));

    }).
        event(OrderTradeConfirmation.class, 
            (orderTradeConfirmation, orderData) -> {
      int quantityRemaining = orderData.getQuantityRemaining() - orderTradeConfirmation.getQty();
      return (quantityRemaining == 0
          && orderTradeConfirmation.getExchangeOrderId().equals(orderData.getExchangeOrderId())
          && !orderData.getTradeOrderIdsList().contains(orderTradeConfirmation.getTradeOrderId())) ? true : false;
    }, (orderTradeConfirmation, orderData) -> {
      return goTo(FullyExecuted).
          applying(new OrderTraded(
              stateName(),
              orderTradeConfirmation.getExchangeOrderId(),
              orderTradeConfirmation.getOrderConfirmationTime(), 
              orderTradeConfirmation.getPrice(),
              orderTradeConfirmation.getQty(), 
              orderTradeConfirmation.getTradeOrderId(),
              orderTradeConfirmation.getBrokerOrderId())).
          andThen(exec(data -> {
            
            context().parent()
            .tell(new TradeExecuted(
                orderData.getDestinationAddress(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getTradeOrderId(), 
                orderData.getSide(),
                PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                    data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                    Pafs.withCumulativePaf(1.)),
                orderTradeConfirmation.getQty()), self());
            
            context().parent()
            .tell(new OrderFullyTraded(
                orderData.getClientOrderId(),
                orderData.getClientCode(),
                String.valueOf(orderData.getExchangeOrderId()),
                String.valueOf(orderData.getBrokerOrderId())), self());
            self().tell(PoisonPill.getInstance(), self());

          }));

    }).
        event(OrderCancellationRequest.class, 
        (orderCancellationRequest, orderData) -> {
          return goTo(CancellationOrderPlaced).
              applying(new OrderCancellationInProcess(
              stateName(),
              String.valueOf(orderCancellationRequest.getLocalOrderID()))). 
          andThen(exec(data -> {
            CancelCurrentOrder cancelCurrentOrder = new CancelCurrentOrder(stateName(),
                orderData.getClientCode(),
                String.valueOf(orderCancellationRequest.getLocalOrderID()),
                orderData.getOrderExecutionMetaData(),
                orderData.getExchangeOrderId(),
                orderData.getBrokerName(),
                orderData.getPurpose(),
                orderData.getInvestingMode(),
                orderData.getExchange());

            tellChildren(context(), self(),cancelCurrentOrder , "Cancel"+orderData.getClientOrderId());
            }));

        }).
        event(TradeBookRequestNotSent.class, 
        (orderNotSent, orderData) -> {
        return stay()
               .applying(new TradeBookRequestFailed())
               .andThen(exec(data -> {
                  CtclInteractive ctcl;
                  try {
                    ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                    log.debug("Asking for order book from " + ctcl);
                    ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
                  } catch (Exception e) {
                    context().system().scheduler().scheduleOnce(
                        create(20, SECONDS), 
                        self(),
                        new TradeBookRequestNotSent(data.getClientOrderId()),
                        context().system().dispatcher(), ActorRef.noSender());
                  }
                }));
    }).
        event(StateTimeout$.class, 
            (stateTimeout, orderData) -> {
              return DateUtils.isTradeExecutionWindowOpen() ? true : false;
            },
            (stateTimeout, orderData) -> {
          return stay().
          applying(new OrderStateTimedOut()).
          forMax(create(60, "seconds")).
          andThen(exec(data -> {
            CtclInteractive ctcl;
            try {
              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
              log.debug("Asking for trade book from " + ctcl);
              ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
            } catch (Exception e) {
              self().tell(new TradeBookRequestNotSent(data.getClientOrderId()), self());
            }
          }));
    }).event(StateTimeout$.class, 
        (stateTimeout, orderData) -> {
          return !DateUtils.isTradeExecutionWindowOpen() ? true : false;
        },
        (stateTimeout, orderData) -> {
      return stay().
      applying(new ActorTerminatedOnStateTimeOut(stateName())).
      andThen(exec(data -> {
        context().parent()
        .tell(new OrderTerminatedAtEOD(
            orderData.getDestinationAddress(),
           stateName(), data.getSide(), data.getExchangeOrderId()), self());
        
        context().parent()
        .tell(new ShuttingDownActorAtEOD(
           stateName(), orderData.getClientCode(), orderData.getClientOrderId()), self());
        
        self().tell(PoisonPill.getInstance(), self());
        
         }));
     }));
       
    
    when(CancellationOrderPlaced,
        matchEvent(OrderTradeConfirmation.class, 
            (orderTradeConfirmation, orderData) -> {
      int quantityRemaining = orderData.getQuantityRemaining() - orderTradeConfirmation.getQty();
      return (quantityRemaining != 0
              && orderTradeConfirmation.getExchangeOrderId().equals(orderData.getExchangeOrderId())
              && !orderData.getTradeOrderIdsList().contains(orderTradeConfirmation.getTradeOrderId())) ? true : false;
    }, (orderTradeConfirmation, orderData) -> {
      return stay().
          applying(new OrderTraded(
              stateName(), 
              orderTradeConfirmation.getExchangeOrderId(),
              orderTradeConfirmation.getOrderConfirmationTime(),
              orderTradeConfirmation.getPrice(),
              orderTradeConfirmation.getQty(), 
              orderTradeConfirmation.getTradeOrderId(),
              orderTradeConfirmation.getBrokerOrderId())).
          andThen(exec(data -> {

            context().parent()
            .tell(new TradeExecuted(
                orderData.getDestinationAddress(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getTradeOrderId(), 
                orderData.getSide(),
                PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                    data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                    Pafs.withCumulativePaf(1.)),
                orderTradeConfirmation.getQty()), self());
          }));

    }).
        event(OrderTradeConfirmation.class, 
            (orderTradeConfirmation, orderData) -> {
      int quantityRemaining = orderData.getQuantityRemaining() - orderTradeConfirmation.getQty();
      return (quantityRemaining == 0
          && orderTradeConfirmation.getExchangeOrderId().equals(orderData.getExchangeOrderId())
          && !orderData.getTradeOrderIdsList().contains(orderTradeConfirmation.getTradeOrderId())) ? true : false;
    }, (orderTradeConfirmation, orderData) -> {
      return goTo(FullyExecuted).
          applying(new OrderTraded(
              stateName(),
              orderTradeConfirmation.getExchangeOrderId(),
              orderTradeConfirmation.getOrderConfirmationTime(), 
              orderTradeConfirmation.getPrice(),
              orderTradeConfirmation.getQty(), 
              orderTradeConfirmation.getTradeOrderId(),
              orderTradeConfirmation.getBrokerOrderId())).
          andThen(exec(data -> {
            
            context().parent()
                .tell(new TradeExecuted(
                    orderData.getDestinationAddress(), 
                    orderTradeConfirmation.getExchangeOrderId(),
                    orderTradeConfirmation.getTradeOrderId(), 
                    orderData.getSide(),
                    PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                        data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                        Pafs.withCumulativePaf(1.)),
                    orderTradeConfirmation.getQty()), self());
            
            context().parent()
            .tell(new OrderFullyTraded(
                orderData.getClientOrderId(),
                orderData.getClientCode(),
                String.valueOf(orderData.getExchangeOrderId()),
                String.valueOf(orderData.getBrokerOrderId())), self());
           
            self().tell(PoisonPill.getInstance(), self());

          }));

    }).
        event(ReceivedByBroker.class, 
            (receivedByBroker, orderData) -> {
        return stay().
          applying(new ForwardingToChild()).
          andThen(exec(data -> {
          
            tellChildren(context(), self(),receivedByBroker , "Cancel"+orderData.getClientOrderId());
          }));
    }).
        event(RejectedByBroker.class, 
            (rejectedByBroker, orderData) -> {
        return stay()
            .applying(new ForwardingToChild())
            .andThen(exec(data -> {
              
              tellChildren(context(), self(),rejectedByBroker , "Cancel"+orderData.getClientOrderId()); 
            
          }));

   }). 
        event(RejectedByExchange.class, 
            (rejectedByExchange, orderData) -> {
          OrderSpecification orderSpecification = new OrderSpecification(rejectedByExchange.getClientCode(), 
                                                                         rejectedByExchange.getToken(), 
                                                                         rejectedByExchange.getQuantity(), 
                                                                         rejectedByExchange.getOrderSide());
          return (orderSpecification.equals(orderData.getOrderSpecification()) ) ? true : false;
                                               },
            (rejectedByExchange, orderData) -> {
          return stay()
              .applying(new ForwardingToChild())
              .andThen(exec(data -> {

                tellChildren(context(), self(),rejectedByExchange , "Cancel"+orderData.getClientOrderId());  
              
              }));
     }).
        event(SentToExchange.class, 
            (sentToExchange, orderData) -> {
      OrderSpecification orderSpecification = new OrderSpecification(sentToExchange.getClientCode(),
                                                                     sentToExchange.getToken(), 
                                                                     sentToExchange.getQuantity(), 
                                                                     sentToExchange.getOrderSide());
      return (orderSpecification.equals(orderData.getOrderSpecification())) ? true : false; 
                                           },
            (sentToExchange, orderData) -> {
      return stay().
          applying(new ForwardingToChild()).
          andThen(exec(data -> {
            
            tellChildren(context(), self(),sentToExchange, "Cancel"+orderData.getClientOrderId());  
            
          }));
    }).
        event(OrderBookMessage.class, 
            (orderBookMessage, orderData) -> {
      OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(),
                                                                     orderBookMessage.getToken(), 
                                                                     orderBookMessage.getQuantity(), 
                                                                     orderBookMessage.getOrderSide());
      return (orderSpecification.equals(orderData.getOrderSpecification())) ? true : false; 
                                           },
            (orderBookMessage, orderData) -> {
      return stay().
          applying(new ForwardingToChild()).
          andThen(exec(data -> {
            
            tellChildren(context(), self(),orderBookMessage, "Cancel"+orderData.getClientOrderId());  
            
          }));
    }).
        event(CancelRejected.class,
            (cancelRejected, orderData) -> {
       return(orderData.getQuantityTraded()!=0)? true:false;
                                           },
            (cancelRejected, orderData) -> {
        return goTo(PartiallyExecuted).
            applying(new CancelRejectedEvent(cancelRejected.getMessage(), cancelRejected.getLocalOrderIdCancellation(), cancelRejected.getBrokerOrderIdCancellation())).
            andThen(exec(data -> {
              context().parent().tell(new OrderCancellationRejected(
                  orderData.getClientOrderId(),
                  orderData.getClientCode(),
                  cancelRejected.getLocalOrderIdCancellation(),
                  cancelRejected.getBrokerOrderIdCancellation()), self());  
              
            }));
    }). 
        event(CancelRejected.class,
            (cancelRejected, orderData) -> {
       return(orderData.getQuantityTraded()==0)? true:false;
                                           },
            (cancelRejected, orderData) -> {
        return goTo(OrderSenttoExchange).
            applying(new CancelRejectedEvent(cancelRejected.getMessage(), cancelRejected.getLocalOrderIdCancellation(), cancelRejected.getBrokerOrderIdCancellation())).
            andThen(exec(data -> {
              context().parent().tell(new OrderCancellationRejected(
                  orderData.getClientOrderId(),
                  orderData.getClientCode(),
                  //TODO: here + cancelRejected + cancel Confirmation + remove CancellationOrderRejected
                  cancelRejected.getLocalOrderIdCancellation(),
                  cancelRejected.getBrokerOrderIdCancellation()), self());  
              
              
            }));
    }).
        event(CancelConfirmation.class,
            (cancelConfirmation, orderData) -> {
        return goTo(Cancelled).
            applying(new CancelConfirmed(cancelConfirmation.getExchangeOrderId(), cancelConfirmation.getBrokerOrderId())).
            andThen(exec(data -> {
              context().parent()
              .tell(new TradeCancelled(
                  orderData.getDestinationAddress(), 
                  orderData.getExchangeOrderId()), self());

              context().parent()
              .tell(new OrderCancelled(
                  orderData.getClientOrderId(),
                  orderData.getClientCode(),
                  cancelConfirmation.getExchangeOrderId(),
                  cancelConfirmation.getBrokerOrderId()), self());
                  
              
              self().tell(PoisonPill.getInstance(), self());

            }));
    })
               );
    

    when(FullyExecuted, matchEvent(StateMachineTerminationCommand.class, (stateExceptionCommand, orderData) -> {
      return stay();
    }) 
    .event(ProvideOrderBook.class, 
        (provideOrderBook, orderData) -> {
          return stay()
              .applying(new ReceivedRequestToProvideOrderBook(provideOrderBook.getClientOrderId()))
              .andThen(exec(data -> {
                CtclInteractive ctcl;
                try {
                  if(orderData.getInvestingMode() == InvestingMode.REAL){
                  ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                  log.debug("Asking for order book from " + ctcl);
                  ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
                  }else{
                    sendVirtualOrderBook(orderData);
                  }
                } catch (Exception e) {
                  self().tell(new OrderBookRequestNotSent(data.getClientOrderId()), self());
                }
          }));
        })
    .event(ProvideTradeBook.class, 
            (provideTradeBook, orderData) -> {
              return stay()
                  .applying(new ReceivedRequestToProvideTradeBook(provideTradeBook.getClientOrderId()))
                  .andThen(exec(data -> {
                    CtclInteractive ctcl;
                    try {
                      if(orderData.getInvestingMode() == InvestingMode.REAL){
                      ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                      log.debug("Asking for order book from " + ctcl);
                      ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
                      }else{
                        sendVirtualTradeBook(orderData);
                      }
                    } catch (Exception e) {
                      self().tell(new TradeBookRequestNotSent(data.getClientOrderId()), self());
                    }
                  }));
        }));
    when(OrderRejectedByExchange, matchEvent(StateMachineTerminationCommand.class, (stateExceptionCommand, orderData) -> {
      return stay();
    })
    .event(ProvideOrderBook.class, 
        (provideOrderBook, orderData) -> {
          return stay()
              .applying(new ReceivedRequestToProvideOrderBook(provideOrderBook.getClientOrderId()))
              .andThen(exec(data -> {
                CtclInteractive ctcl;
                try {
                  ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                  log.debug("Asking for order book from " + ctcl);
                  ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
                } catch (Exception e) {
                  self().tell(new OrderBookRequestNotSent(data.getClientOrderId()), self());
                }
          }));
        })
    .event(ProvideTradeBook.class, 
            (provideTradeBook, orderData) -> {
              return stay()
                  .applying(new ReceivedRequestToProvideTradeBook(provideTradeBook.getClientOrderId()))
                  .andThen(exec(data -> {
                    CtclInteractive ctcl;
                    try {
                      ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                      log.debug("Asking for order book from " + ctcl);
                      ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
                    } catch (Exception e) {
                      self().tell(new TradeBookRequestNotSent(data.getClientOrderId()), self());
                    }
                  }));
        }));

    when(OrderRejectedByBroker, matchEvent(StateMachineTerminationCommand.class, (stateExceptionCommand, orderData) -> {
      return stay();
    })
    .event(ProvideOrderBook.class, 
        (provideOrderBook, orderData) -> {
          return stay()
              .applying(new ReceivedRequestToProvideOrderBook(provideOrderBook.getClientOrderId()))
              .andThen(exec(data -> {
                CtclInteractive ctcl;
                try {
                  ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                  log.debug("Asking for order book from " + ctcl);
                  ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
                } catch (Exception e) {
                  self().tell(new OrderBookRequestNotSent(data.getClientOrderId()), self());
                }
          }));
        })
    .event(ProvideTradeBook.class, 
            (provideTradeBook, orderData) -> {
              return stay()
                  .applying(new ReceivedRequestToProvideTradeBook(provideTradeBook.getClientOrderId()))
                  .andThen(exec(data -> {
                    CtclInteractive ctcl;
                    try {
                      ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                      log.debug("Asking for order book from " + ctcl);
                      ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
                    } catch (Exception e) {
                      self().tell(new TradeBookRequestNotSent(data.getClientOrderId()), self());
                    }
                  }));
        }));
    
    when(Cancelled, matchEvent(StateMachineTerminationCommand.class, (stateExceptionCommand, orderData) -> {
      return stay();
    })
    .event(ProvideOrderBook.class, 
        (provideOrderBook, orderData) -> {
          return stay()
              .applying(new ReceivedRequestToProvideOrderBook(provideOrderBook.getClientOrderId()))
              .andThen(exec(data -> {
                CtclInteractive ctcl;
                try {
                  ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                  log.debug("Asking for order book from " + ctcl);
                  ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
                } catch (Exception e) {
                  self().tell(new OrderBookRequestNotSent(data.getClientOrderId()), self());
                }
          }));
        })
    .event(ProvideTradeBook.class, 
            (provideTradeBook, orderData) -> {
              return stay()
                  .applying(new ReceivedRequestToProvideTradeBook(provideTradeBook.getClientOrderId()))
                  .andThen(exec(data -> {
                    CtclInteractive ctcl;
                    try {
                      ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                      log.debug("Asking for order book from " + ctcl);
                      ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
                    } catch (Exception e) {
                      self().tell(new TradeBookRequestNotSent(data.getClientOrderId()), self());
                    }
                  }));
        }));

    whenUnhandled(
        matchEvent(SubscribeToExchangeUpdateEventStream.class, 
        (event, orderData) -> {
        log.debug(self() + " is subscribed to exchange update stream in state " + stateName());
        return stay();
    })
        .event(SentToExchange.class,
            (sentToExchange,orderData) -> {
              OrderSpecification orderSpecification = new OrderSpecification(
                  sentToExchange.getClientCode(), 
                  sentToExchange.getToken(), 
                  sentToExchange.getQuantity(), 
                  sentToExchange.getOrderSide());
            return (stateName().equals(OrderGenerated) && orderSpecification.equals(orderData.getOrderSpecification())) ? true : false;
                                          }, 
            (sentToExchange,orderData) -> {
              OrderSentToExchange orderSentToExchange = new OrderSentToExchange(
                  stateName(), 
                  orderData.getClientCode(),
                  sentToExchange.getBrokerOrderId(),
                  sentToExchange.getExchangeOrderId(), 
                  sentToExchange.getExchangeOrderTime());
              return goTo(OrderSenttoExchange)
              .applying(orderSentToExchange)
              .andThen(exec(data -> {
                context().parent().tell(orderSentToExchange, self());
               
                context().parent().tell(new OrderReceivedWithBroker
                    (orderData.getDestinationAddress(),
                    orderData.getClientOrderId(), 
                    sentToExchange.getBrokerOrderId()), self());
                
                context().parent()
                    .tell(new OrderPlacedWithExchange(
                        orderData.getDestinationAddress(), 
                        sentToExchange.getBrokerOrderId(),
                        sentToExchange.getExchangeOrderId(), 
                        sentToExchange.getBrokerOrderId(), 
                        orderData.getSide(),
                        orderData.getIntegerQuantity()), self());
              }));
    })
        .event(SentToExchange.class,
            (sentToExchange,orderData) -> {
          OrderSpecification orderSpecification = new OrderSpecification(
              sentToExchange.getClientCode(), 
              sentToExchange.getToken(), 
              sentToExchange.getQuantity(), 
              sentToExchange.getOrderSide());
        return (stateName().equals(OrderGenerated) && orderSpecification.equals(orderData.getOrderSpecification())) ? false : true;
                                          }, 
            (sentToExchange,orderData) -> {
          return stay();
    })
        .event(RejectedByExchange.class,
            (rejectedByExchange, orderData) -> {
        OrderSpecification orderSpecification = new OrderSpecification(
            rejectedByExchange.getClientCode(), 
            rejectedByExchange.getToken(), 
            rejectedByExchange.getQuantity(), 
            rejectedByExchange.getOrderSide());
        return (stateName().equals(OrderGenerated) && orderSpecification.equals(orderData.getOrderSpecification())) ? true : false;
                                           }, 
            (rejectedByExchange, orderData) -> {
          return goTo(OrderRejectedByExchange).
              applying(new OrderRejected(
                  stateName(), 
                  rejectedByExchange.getBrokerOrderId(), 
                  rejectedByExchange.getMessage()))
              .andThen(exec(data -> {
                
                context().parent().tell(new OrderReceivedWithBroker
                    (orderData.getDestinationAddress(),
                    orderData.getClientOrderId(), 
                    rejectedByExchange.getBrokerOrderId()), self());
                
                context().parent()
                .tell(new OrderRejectedByExchange(
                    orderData.getDestinationAddress(),
                    rejectedByExchange.getBrokerOrderId(), 
                    rejectedByExchange.getMessage()), self());
                
                context().parent()
                .tell(new OrderRejectedMessage(
                    orderData.getClientOrderId(), orderData.getClientCode()), self());
                
                self().tell(PoisonPill.getInstance(), self());
              }));
    })
        .event(RejectedByExchange.class,
            (rejectedByExchange, orderData) -> {
          OrderSpecification orderSpecification = new OrderSpecification(
              rejectedByExchange.getClientCode(), 
              rejectedByExchange.getToken(), 
              rejectedByExchange.getQuantity(), 
              rejectedByExchange.getOrderSide());
          return (stateName().equals(OrderGenerated) && orderSpecification.equals(orderData.getOrderSpecification())) ? false : true;
                                               }, 
            (rejectedByExchange, orderData) -> {
            return stay();
      })
        .event(OrderTradeConfirmation.class,(orderTradeConfirmation,orderData) -> {
        OrderSpecification orderSpecification = new OrderSpecification(orderTradeConfirmation.getClientCode(), 
                                                                       orderTradeConfirmation.getToken(), 
                                                                       orderTradeConfirmation.getQuantity(), 
                                                                       orderTradeConfirmation.getOrderSide());
        int quantityRemaining = orderData.getIntegerQuantity() - orderTradeConfirmation.getQty();
        return (stateName().equals(OrderGenerated) 
                && orderSpecification.equals(orderData.getOrderSpecification()) 
                &&   quantityRemaining != 0) ? true : false;
                                                  }, 
            (orderTradeConfirmation,orderData) -> {
          OrderSentToExchange orderSentToExchange = new OrderSentToExchange(
              stateName(), 
              orderData.getClientCode(),
              orderTradeConfirmation.getBrokerOrderId(),
              orderTradeConfirmation.getExchangeOrderId(), 
              orderTradeConfirmation.getOrderConfirmationTime());
          return goTo(PartiallyExecuted).
              applying(new OrderTraded(
                  stateName(), 
                  orderTradeConfirmation.getExchangeOrderId(),
                  orderTradeConfirmation.getOrderConfirmationTime(), 
                  orderTradeConfirmation.getPrice(),
                  orderTradeConfirmation.getQty(), 
                  orderTradeConfirmation.getTradeOrderId(),
                  orderTradeConfirmation.getBrokerOrderId())).
              andThen(exec(data -> {
                context().parent().tell(orderSentToExchange, self());
                
                context().parent().tell(new OrderReceivedWithBroker
                    (orderData.getDestinationAddress(),
                    orderData.getClientOrderId(), 
                    orderTradeConfirmation.getBrokerOrderId()), self());
                
                context().parent()
                    .tell(new OrderPlacedWithExchange(
                        orderData.getDestinationAddress(), 
                        orderTradeConfirmation.getBrokerOrderId(),
                        orderTradeConfirmation.getExchangeOrderId(), 
                        orderTradeConfirmation.getBrokerOrderId(), 
                        orderData.getSide(),
                        orderData.getIntegerQuantity()), self());
                
                context().parent()
                    .tell(new TradeExecuted(
                        orderData.getDestinationAddress(), 
                        orderTradeConfirmation.getExchangeOrderId(),
                        orderTradeConfirmation.getTradeOrderId(), 
                        orderData.getSide(),
                        PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                            data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                            Pafs.withCumulativePaf(1.)),
                        orderTradeConfirmation.getQty()), self());
  
              }));
      })
        .event(OrderTradeConfirmation.class,
            (orderTradeConfirmation,orderData) -> {
          OrderSpecification orderSpecification = new OrderSpecification(orderTradeConfirmation.getClientCode(), 
              orderTradeConfirmation.getToken(), 
              orderTradeConfirmation.getQuantity(), 
              orderTradeConfirmation.getOrderSide());
          int quantityRemaining = orderData.getIntegerQuantity() - orderTradeConfirmation.getQty();
          return (stateName().equals(OrderGenerated) && 
                  orderSpecification.equals(orderData.getOrderSpecification()) && 
                  quantityRemaining == 0) ? true : false; 
                                                  }, 
            (orderTradeConfirmation,orderData) -> {
        return goTo(FullyExecuted).
            applying(new OrderTraded(
                stateName(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getOrderConfirmationTime(), 
                orderTradeConfirmation.getPrice(),
                orderTradeConfirmation.getQty(), 
                orderTradeConfirmation.getTradeOrderId(),
                orderTradeConfirmation.getBrokerOrderId())).
            andThen(exec(data -> {
              
              context().parent().tell(new OrderReceivedWithBroker
                  (orderData.getDestinationAddress(),
                  orderData.getClientOrderId(), 
                  orderTradeConfirmation.getBrokerOrderId()), self());
              
              context().parent()
                  .tell(new OrderPlacedWithExchange(
                      orderData.getDestinationAddress(), 
                      orderTradeConfirmation.getBrokerOrderId(),
                      orderTradeConfirmation.getExchangeOrderId(), 
                      orderTradeConfirmation.getBrokerOrderId(), 
                      orderData.getSide(),
                      orderData.getIntegerQuantity()), self());
              
              context().parent()
              .tell(new TradeExecuted(
                  orderData.getDestinationAddress(), 
                  orderTradeConfirmation.getExchangeOrderId(),
                  orderTradeConfirmation.getTradeOrderId(), 
                  orderData.getSide(),
                  PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                      data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                      Pafs.withCumulativePaf(1.)),
                  orderTradeConfirmation.getQty()), self());
              
              context().parent()
              .tell(new OrderFullyTraded(
                  orderData.getClientOrderId(),
                  orderData.getClientCode(),
                  String.valueOf(orderData.getExchangeOrderId()),
                  String.valueOf(orderData.getBrokerOrderId())), self());
              self().tell(PoisonPill.getInstance(), self());

            }));
      })
        .event(OrderTradeConfirmation.class,
            (orderTradeConfirmation, orderData) -> {
          OrderSpecification orderSpecification = new OrderSpecification(orderTradeConfirmation.getClientCode(), 
              orderTradeConfirmation.getToken(), 
              orderTradeConfirmation.getQuantity(), 
              orderTradeConfirmation.getOrderSide());
          int quantityRemaining = orderData.getIntegerQuantity() - orderTradeConfirmation.getQty();
         return (stateName().equals(OrderReceivedbyBroker) && 
                 orderSpecification.equals(orderData.getOrderSpecification()) &&  
                 quantityRemaining != 0) ? true : false; 
                                                   }, 
            (orderTradeConfirmation, orderData) -> {
              OrderSentToExchange orderSentToExchange = new OrderSentToExchange(
                  stateName(), 
                  orderData.getClientCode(),
                  orderTradeConfirmation.getBrokerOrderId(),
                  orderTradeConfirmation.getExchangeOrderId(), 
                  orderTradeConfirmation.getOrderConfirmationTime());
        return goTo(PartiallyExecuted).
            applying(new OrderTraded(
                stateName(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getOrderConfirmationTime(), 
                orderTradeConfirmation.getPrice(),
                orderTradeConfirmation.getQty(), 
                orderTradeConfirmation.getTradeOrderId(),
                orderTradeConfirmation.getBrokerOrderId())).
            andThen(exec(data -> {
              context().parent().tell(orderSentToExchange, self());
          
              context().parent()
                  .tell(new OrderPlacedWithExchange(
                      orderData.getDestinationAddress(), 
                      orderTradeConfirmation.getBrokerOrderId(),
                      orderTradeConfirmation.getExchangeOrderId(), 
                      orderTradeConfirmation.getBrokerOrderId(), 
                      orderData.getSide(),
                      orderData.getIntegerQuantity()), self());
              
              context().parent()
              .tell(new TradeExecuted(
                  orderData.getDestinationAddress(), 
                  orderTradeConfirmation.getExchangeOrderId(),
                  orderTradeConfirmation.getTradeOrderId(), 
                  orderData.getSide(),
                  PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                      data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                      Pafs.withCumulativePaf(1.)),
                  orderTradeConfirmation.getQty()), self());

            }));
      })
        .event(OrderTradeConfirmation.class,
            (orderTradeConfirmation, orderData) -> {
          OrderSpecification orderSpecification = new OrderSpecification(orderTradeConfirmation.getClientCode(), 
              orderTradeConfirmation.getToken(), 
              orderTradeConfirmation.getQuantity(), 
              orderTradeConfirmation.getOrderSide());
          int quantityRemaining = orderData.getIntegerQuantity() - orderTradeConfirmation.getQty();
        return (stateName().equals(OrderReceivedbyBroker) && 
              orderSpecification.equals(orderData.getOrderSpecification()) &&
              quantityRemaining == 0) ? true : false; 
                                                   }, 
            (orderTradeConfirmation, orderData) -> {
        return goTo(FullyExecuted).
            applying(new OrderTraded(
                stateName(), 
                orderTradeConfirmation.getExchangeOrderId(),
                orderTradeConfirmation.getOrderConfirmationTime(), 
                orderTradeConfirmation.getPrice(),
                orderTradeConfirmation.getQty(), 
                orderTradeConfirmation.getTradeOrderId(),
                orderTradeConfirmation.getBrokerOrderId())).
            andThen(exec(data -> {
              
              context().parent()
                  .tell(new OrderPlacedWithExchange(
                      orderData.getDestinationAddress(), 
                      orderTradeConfirmation.getBrokerOrderId(),
                      orderTradeConfirmation.getExchangeOrderId(), 
                      orderTradeConfirmation.getBrokerOrderId(), 
                      orderData.getSide(),
                      orderData.getIntegerQuantity()), self());
 
              context().parent()
              .tell(new TradeExecuted(
                  orderData.getDestinationAddress(), 
                  orderTradeConfirmation.getExchangeOrderId(),
                  orderTradeConfirmation.getTradeOrderId(), 
                  orderData.getSide(),
                  PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                      data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                      Pafs.withCumulativePaf(1.)),
                  orderTradeConfirmation.getQty()), self());
              
              context().parent()
              .tell(new OrderFullyTraded(
                  orderData.getClientOrderId(),
                  orderData.getClientCode(),
                  String.valueOf(orderData.getExchangeOrderId()),
                  String.valueOf(orderData.getBrokerOrderId())), self());
              self().tell(PoisonPill.getInstance(), self());

            }));
      })
        .event(OrderTradeConfirmation.class,
            (orderTradeConfirmation, orderData) -> {
        return (stateName().equals(FullyExecuted) && 
            orderTradeConfirmation.getExchangeOrderId().equals(orderData.getExchangeOrderId())) ? true : false;
                
                                                   }, 
            (orderTradeConfirmation, orderData) -> {
        return stay().
            applying(new ProvidingTradeBook()).
            andThen(exec(data -> {
      
          
              context().parent()
              .tell(new TradeExecuted(
                  orderData.getDestinationAddress(), 
                  orderTradeConfirmation.getExchangeOrderId(),
                  orderTradeConfirmation.getTradeOrderId(), 
                  orderData.getSide(),
                  PriceWithPaf.withPrice(Double.parseDouble(String.valueOf(orderTradeConfirmation.getPrice())),
                      data.getOrderExecutionMetaData().getToken() + "-" + data.getExchange() + "-" + "EQ", 
                      Pafs.withCumulativePaf(1.)),
                  orderTradeConfirmation.getQty()), self());
              
         
              self().tell(PoisonPill.getInstance(), self());

            }));
      })
        .event(OrderTradeConfirmation.class,
            (orderTradeConfirmation, orderData) -> {
              return !(stateName().equals(OrderReceivedbyBroker) || stateName().equals(OrderGenerated) || stateName().equals(FullyExecuted)) ? true : false; 
                                                   }, 
            (orderTradeConfirmation, orderData) -> {
                return stay();
      })
        .event(OrderBookMessage.class,
            (orderBookMessage,orderData) -> {
              OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(), 
                                                                     orderBookMessage.getToken(), 
                                                                     orderBookMessage.getQuantity(), 
                                                                     orderBookMessage.getOrderSide());
              return (stateName().equals(OrderGenerated) 
                  && orderSpecification.equals(orderData.getOrderSpecification()) 
                  && !(orderBookMessage.getMessage().contains("Placed") || orderBookMessage.getMessage().contains("Cancelled")) 
                  && orderBookMessage.getExchangeOrderId() == 0) ? true : false; 
                                            }, 
            (orderBookMessage,orderData) -> {
             return goTo(OrderRejectedByBroker).
                 applying(new OrderRejected(
                          stateName(),
                          String.valueOf(orderBookMessage.getLocalOrderId()), 
                          orderBookMessage.getMessage())).
                 andThen(exec(data -> {
                         context().parent().tell(new OrderRejectedByBroker(
                              orderData.getDestinationAddress(),
                              String.valueOf(orderBookMessage.getLocalOrderId()), 
                              orderBookMessage.getMessage()), self());
                          
                         context().parent()
                          .tell(new OrderRejectedMessage(
                              orderData.getClientOrderId(), orderData.getClientCode()), self());
                          
                          self().tell(PoisonPill.getInstance(), self());
                         
                        }));
      })
        .event(OrderBookMessage.class,
            (orderBookMessage,orderData) -> {
              OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(), 
                                                                     orderBookMessage.getToken(), 
                                                                     orderBookMessage.getQuantity(), 
                                                                     orderBookMessage.getOrderSide());
              return (stateName().equals(OrderGenerated) 
                  && orderSpecification.equals(orderData.getOrderSpecification()) 
                  && !(orderBookMessage.getMessage().contains("Placed") || orderBookMessage.getMessage().contains("Cancelled")) 
                  && orderBookMessage.getExchangeOrderId() != 0) ? true : false; 
                                            }, 
            (orderBookMessage,orderData) -> {
              return goTo(OrderRejectedByExchange).
                  applying(new OrderRejected(
                      stateName(),
                      String.valueOf(orderBookMessage.getLocalOrderId()), 
                      orderBookMessage.getMessage())).
               andThen(exec(data -> {
                        
                     context().parent().tell(new OrderReceivedWithBroker
                       (orderData.getDestinationAddress(),
                        orderData.getClientOrderId(), 
                        String.valueOf(orderBookMessage.getBrokerOrderId())), self());
                         
                     context().parent().tell(new OrderRejectedByExchange(
                        orderData.getDestinationAddress(),
                        String.valueOf(orderBookMessage.getBrokerOrderId()), 
                        orderBookMessage.getMessage()), self());
                          
                     context().parent().tell(new OrderRejectedMessage(
                        orderData.getClientOrderId(), orderData.getClientCode()), self());
                          
                     self().tell(PoisonPill.getInstance(), self());
                         
                        }));
       })
        .event(OrderBookMessage.class,
            (orderBookMessage,orderData) -> {
              OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(), 
                                                                     orderBookMessage.getToken(), 
                                                                     orderBookMessage.getQuantity(), 
                                                                     orderBookMessage.getOrderSide());
              return (stateName().equals(OrderGenerated) 
                  && orderSpecification.equals(orderData.getOrderSpecification()) 
                  && orderBookMessage.getMessage().contains("Placed")
                  && orderBookMessage.getExchangeOrderId() != 0) ? true : false; 
                                            }, 
            (orderBookMessage,orderData) -> {
              return goTo(OrderSenttoExchange).
                  applying(new OrderSentToExchange(
                      stateName(), 
                      orderData.getClientCode(),
                      String.valueOf(orderBookMessage.getBrokerOrderId()),
                      String.valueOf(orderBookMessage.getExchangeOrderId()), 
                      orderBookMessage.getExchangeOrderTime())).
                  andThen(exec(data -> {
                    
                    context().parent().tell(new OrderReceivedWithBroker
                        (orderData.getDestinationAddress(),
                        orderData.getClientOrderId(), 
                        String.valueOf(orderBookMessage.getBrokerOrderId())), self());
                    
                    context().parent()
                        .tell(new OrderPlacedWithExchange(
                            orderData.getDestinationAddress(), 
                            String.valueOf(orderBookMessage.getBrokerOrderId()),
                            String.valueOf(orderBookMessage.getExchangeOrderId()), 
                            String.valueOf(orderBookMessage.getBrokerOrderId()), 
                            orderData.getSide(),
                            orderData.getIntegerQuantity()), self());
                  }));
              
            })
            .event(OrderBookMessage.class,
                (orderBookMessage,orderData) -> {
                  OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(), 
                                                                     orderBookMessage.getToken(), 
                                                                     orderBookMessage.getQuantity(), 
                                                                     orderBookMessage.getOrderSide());
                  return (stateName().equals(OrderGenerated) 
                      && orderSpecification.equals(orderData.getOrderSpecification()) 
                      && orderBookMessage.getMessage().contains("Placed")
                      && orderBookMessage.getExchangeOrderId() == 0) ? true : false; 
                                                }, 
                (orderBookMessage,orderData) -> {
              return goTo(OrderReceivedbyBroker).
                  applying(new OrderReceivedByBroker(
                      stateName(),
                      String.valueOf(orderBookMessage.getLocalOrderId()), 
                      String.valueOf(orderBookMessage.getBrokerOrderId()))).
                  andThen(exec(data -> {
                    context().parent().tell(new OrderReceivedWithBroker
                        (orderData.getDestinationAddress(),
                        orderData.getClientOrderId(), 
                        String.valueOf(orderBookMessage.getBrokerOrderId())), self());
                  }));
              
            })
            .event(OrderBookMessage.class,
                (orderBookMessage,orderData) -> {
                  OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(), 
                                                                     orderBookMessage.getToken(), 
                                                                     orderBookMessage.getQuantity(), 
                                                                     orderBookMessage.getOrderSide());
                   return (stateName().equals(OrderReceivedbyBroker) 
                       && orderSpecification.equals(orderData.getOrderSpecification())
                       && !(orderBookMessage.getMessage().contains("Placed") || orderBookMessage.getMessage().contains("Cancelled"))) ? true : false;
                                                        }, 
                        (orderBookMessage,orderData) -> {
                    return goTo(OrderRejectedByExchange)
                        .applying(new OrderRejected(
                            stateName(), 
                            String.valueOf(orderBookMessage.getBrokerOrderId()), 
                            orderBookMessage.getMessage()))
                        .andThen(exec(data -> {
                          
                          context().parent()
                          .tell(new OrderRejectedByExchange(
                              orderData.getDestinationAddress(),
                              String.valueOf(orderBookMessage.getBrokerOrderId()), 
                              orderBookMessage.getMessage()), self());
                          
                          context().parent()
                          .tell(new OrderRejectedMessage(
                              orderData.getClientOrderId(), orderData.getClientCode()), self());
                          
                          self().tell(PoisonPill.getInstance(), self());
                }));
          })
            .event(OrderBookMessage.class,
                (orderBookMessage,orderData) -> {
                  OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(), 
                                                                     orderBookMessage.getToken(), 
                                                                     orderBookMessage.getQuantity(), 
                                                                             orderBookMessage.getOrderSide());
                   return (stateName().equals(OrderReceivedbyBroker) 
                       && orderSpecification.equals(orderData.getOrderSpecification())
                       && orderBookMessage.getMessage().contains("Placed")) ? true : false; 
                                                        }, 
                        (orderBookMessage,orderData) -> {
                    return goTo(OrderSenttoExchange).
                        applying(new OrderSentToExchange(
                            stateName(), 
                            orderData.getClientCode(),
                            String.valueOf(orderBookMessage.getBrokerOrderId()),
                            String.valueOf(orderBookMessage.getExchangeOrderId()), 
                            orderBookMessage.getExchangeOrderTime())).
                        andThen(exec(data -> {
                          
                          context().parent()
                              .tell(new OrderPlacedWithExchange(
                                  orderData.getDestinationAddress(), 
                                  String.valueOf(orderBookMessage.getBrokerOrderId()),
                                  String.valueOf(orderBookMessage.getExchangeOrderId()), 
                                  String.valueOf(orderBookMessage.getBrokerOrderId()), 
                                  orderData.getSide(),
                                  orderData.getIntegerQuantity()), self());
                }));
          })
            .event(OrderBookMessage.class,
                (orderBookMessage,orderData) -> {
                  OrderSpecification orderSpecification = new OrderSpecification(orderBookMessage.getClientCode(), 
                                                                         orderBookMessage.getToken(), 
                                                                         orderBookMessage.getQuantity(), 
                                                                         orderBookMessage.getOrderSide());
                  return (stateName().equals(FullyExecuted) 
                      && orderSpecification.equals(orderData.getOrderSpecification()) 
                      && orderBookMessage.getMessage().contains("Placed")
                      && orderBookMessage.getExchangeOrderId() != 0) ? true : false; 
                                                }, 
                (orderBookMessage,orderData) -> {
                  return stay().
                      applying(new ProvidingOrderBook()).
                      andThen(exec(data -> {
                        
                        context().parent().tell(new OrderReceivedWithBroker
                            (orderData.getDestinationAddress(),
                            orderData.getClientOrderId(), 
                            String.valueOf(orderBookMessage.getBrokerOrderId())), self());
                        
                        context().parent()
                            .tell(new OrderPlacedWithExchange(
                                orderData.getDestinationAddress(), 
                                String.valueOf(orderBookMessage.getBrokerOrderId()),
                                String.valueOf(orderBookMessage.getExchangeOrderId()), 
                                String.valueOf(orderBookMessage.getBrokerOrderId()), 
                                orderData.getSide(),
                                orderData.getIntegerQuantity()), self());
                      }));
                  
                })
            .event(OrderBookMessage.class,
                (orderTradeConfirmation, orderData) -> {
                  return !(stateName().equals(OrderReceivedbyBroker) || stateName().equals(OrderGenerated) || stateName().equals(FullyExecuted)) ? true : false; 
                                                       }, 
                (orderTradeConfirmation, orderData) -> {
                    return stay();
          })
            .event(ProvideOrderBook.class, 
                (provideOrderBook, orderData) -> {
                  return stay()
                      .applying(new ReceivedRequestToProvideOrderBook(provideOrderBook.getClientOrderId()))
                      .andThen(exec(data -> {
                        CtclInteractive ctcl;
                        try {
                          ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                          log.debug("Asking for order book from " + ctcl);
                          ctcl.sendOrderBook(orderData.getClientCode(), orderData.getLocalOrderId());
                        } catch (Exception e) {
                          self().tell(new OrderBookRequestNotSent(data.getClientOrderId()), self());
                        }
                      }));
                })
            .event(ProvideTradeBook.class, 
                    (provideTradeBook, orderData) -> {
                      return stay()
                          .applying(new ReceivedRequestToProvideTradeBook(provideTradeBook.getClientOrderId()))
                          .andThen(exec(data -> {
                            CtclInteractive ctcl;
                            try {
                              ctcl = ctclFactory.getCTCL(new BrokerType(data.getPurpose(), data.getInvestingMode(), data.getBrokerName()));
                              log.debug("Asking for order book from " + ctcl);
                              ctcl.sendTradeBook(orderData.getClientCode(), orderData.getLocalOrderId());
                            } catch (Exception e) {
                              self().tell(new TradeBookRequestNotSent(data.getClientOrderId()), self());
                            }
                          }));
                    }
                    ));
    
    onTermination(
        matchStop(Shutdown(), 
            (orderState, ctclOrder) -> {
              log.warn("Shutting down " + self() + " in state " + stateName());
    }));
  }


  private void sendVirtualTradeBook(CtclEquityOrder orderData) {
      Set<Trade> trades = orderData.getTrades();
      
      for(Trade trade : trades){
        context().parent()
        .tell(new TradeExecuted(
            orderData.getDestinationAddress(), 
            trade.getExchOrderId(),
            trade.getOrderTradeId(), 
            orderData.getSide(),
            PriceWithPaf.withPrice(trade.getPrice(),
                orderData.getOrderExecutionMetaData().getToken() + "-" + orderData.getExchange() + "-" + "EQ", 
                Pafs.withCumulativePaf(1.)),
            trade.getIntegerQuantity()), self());
      }
      
  }

  private void sendVirtualOrderBook(CtclEquityOrder orderData) {
    
    context().parent().tell(new OrderReceivedWithBroker
        (orderData.getDestinationAddress(),
        orderData.getClientOrderId(), 
        orderData.getBrokerOrderId()), self());
    
    context().parent()
        .tell(new OrderPlacedWithExchange(
            orderData.getDestinationAddress(), 
            orderData.getBrokerOrderId(),
            orderData.getExchangeOrderId(), 
            orderData.getBrokerOrderId(), 
            orderData.getSide(),
            orderData.getIntegerQuantity()), self());
    
  }

  @Override
  public CtclEquityOrder applyEvent(OrderEvent event, CtclEquityOrder data) {
    data.update(event);
    return data;
  }

  @Override
  public String persistenceId() {
    return persistenceId;
  }
  
  @Override
  public Class<OrderEvent> domainEventClass() {
    return OrderEvent.class;
  }

  @Override
  public String getName() {
    return "OrderActor";
  }

  @Override
  public String getChildBeanName() {
    return "CancelOrderActor";
  }



}