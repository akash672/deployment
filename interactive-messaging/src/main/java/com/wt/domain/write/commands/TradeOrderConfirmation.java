package com.wt.domain.write.commands;

import com.wt.domain.ExchangeTradeMessageType;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(callSuper=true)
@ToString
public class TradeOrderConfirmation extends  ExchangeTradeMessageType {

  private static final long serialVersionUID = 1L;
  private String brokerOrderId;
  private String exchangeTradeId;
  private float tradePrice;
  private float tradeQty;
  private long exchangeTradeTime;
  private String token;
  private double quantity;
  private OrderSide orderSide;

  public TradeOrderConfirmation(String exchangeOrderID, String brokerOrderID, String exchangeTradeId, float tradePrice, float tradeQty,
      long exchangeTradeTime, String clientCode, String token, double quantity,
      OrderSide orderSide) {
    super(clientCode, exchangeOrderID);
    this.brokerOrderId = brokerOrderID;
    this.exchangeTradeId = exchangeTradeId;
    this.tradePrice = tradePrice;
    this.tradeQty = tradeQty;
    this.exchangeTradeTime = exchangeTradeTime;
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
  }

}
