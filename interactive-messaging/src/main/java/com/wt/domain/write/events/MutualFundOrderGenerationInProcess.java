package com.wt.domain.write.events;

import com.wt.domain.BrokerName;
import com.wt.domain.DestinationAddress;
import com.wt.domain.InvestingMode;
import com.wt.domain.MFOrderActorStates;
import com.wt.domain.OrderSide;
import com.wt.domain.Purpose;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@Builder
public class MutualFundOrderGenerationInProcess extends OrderEvent {

  private static final long serialVersionUID = 1L;
  private String txCode;
  private String localOrderId;
  private String clientCode;
  private String schemeCode;
  private OrderSide side;
  private String dpTxn;
  private double allocatedAmount;
  private double quantity;
  private String allRedeem;
  private String folioNumber;
  private String minRedeem;
  private String dpcFlag;
  private DestinationAddress destinationAddress;
  private String clientOrderId;
  private String symbol;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private String schemeName;
  private String schemeType;
  private String token;
  private MFOrderActorStates orderActorStates;
  
}