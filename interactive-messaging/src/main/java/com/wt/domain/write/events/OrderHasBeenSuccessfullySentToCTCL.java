package com.wt.domain.write.events;

import com.wt.domain.OrderExecutionMetaData;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class OrderHasBeenSuccessfullySentToCTCL extends OrderEvent {

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String localOrderId;
  private OrderExecutionMetaData orderExecutionMetadata;

}
