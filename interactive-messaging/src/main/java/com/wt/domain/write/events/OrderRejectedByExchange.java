package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderRejectedByExchange extends OrderRejectedEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private String brokerOrderId;

  public OrderRejectedByExchange(DestinationAddress destinationAddress, String brokerOrderId, String msg) {
    super(destinationAddress, msg);
    this.brokerOrderId = brokerOrderId;
  }

}
