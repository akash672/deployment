package com.wt.domain.write.commands;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.Purpose;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=false)
public class CreateMutualFundOrder extends CreatedOrder {
  private static final long serialVersionUID = 1L;
  private double allocatedAmount;
  private String schemeType;
  private String schemeName;
  private String schemeCode;
  private String folioNumber;
  private String txCode;
  private String dpcTxn;
  private String allRedeem;
  private String minRedeem;
  private String dpcFlag;
  
  public CreateMutualFundOrder(String senderPath, String wdOrderId, BrokerName brokerName, Purpose purpose,
      InvestingMode investingMode, String userEmail, String clientCode, OrderExecutionMetaData orderExecutionMetaData,
      String exchange, double allocatedAmount, String schemeType, String schemeName, String schemeCode, String folioNumber, String txCode, String dpcTxn,
      String allRedeem, String minRedeem, String dpcFlag, BrokerAuthInformation brokerAuthInformation) {
    super(senderPath, wdOrderId, brokerName, purpose, investingMode, userEmail, clientCode, orderExecutionMetaData,
        exchange, brokerAuthInformation);
    this.allocatedAmount = allocatedAmount;
    this.schemeType = schemeType;
    this.schemeName = schemeName;
    this.schemeCode = schemeCode;
    this.folioNumber = folioNumber;
    this.txCode = txCode;
    this.dpcTxn = dpcTxn;
    this.allRedeem = allRedeem;
    this.minRedeem = minRedeem;
    this.dpcFlag = dpcFlag;
  }
  
  public static CreateMutualFundOrder orderPlacementForMutualFundBuy(String senderPath, String wdOrderId, BrokerName brokerName, Purpose purpose,
      InvestingMode investingMode, String userEmail, String clientCode, OrderExecutionMetaData orderExecutionMetaData,
      double allocatedAmount, String schemeType, String schemeName, String schemeCode, String folioNumber, BrokerAuthInformation brokerAuthInformation) {
    return new CreateMutualFundOrder(senderPath, wdOrderId, brokerName, purpose, investingMode, userEmail, clientCode, orderExecutionMetaData, "", allocatedAmount, schemeType, schemeName, schemeCode, folioNumber,
        "NEW", "N", "N", "N", "N", brokerAuthInformation);
  }
  
  public static CreateMutualFundOrder orderPlacementForMutualFundSell(String senderPath, String wdOrderId, BrokerName brokerName, Purpose purpose,
      InvestingMode investingMode, String userEmail, String clientCode, OrderExecutionMetaData orderExecutionMetaData,
      double allocatedAmount, String schemeType, String schemeName, String schemeCode, String folioNumber, BrokerAuthInformation brokerAuthInformation) {
    return new CreateMutualFundOrder(senderPath, wdOrderId, brokerName, purpose, investingMode, userEmail, clientCode, orderExecutionMetaData, "", allocatedAmount, schemeType, schemeName, schemeCode, folioNumber,
        "NEW", "N", "Y", "N", "N", brokerAuthInformation);
  }
  
}

