package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class CancellationOrderRejected extends OrderRejectedEvent {

  private static final long serialVersionUID = 1L;

  public CancellationOrderRejected(DestinationAddress destinationAddress, String msg) {
    super(destinationAddress, msg);
  }

}