package com.wt.domain.write.commands;

import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.OrderSide.SELL;

import com.wt.domain.ExchangeTradeMessageType;
import com.wt.domain.OrderSide;

import lombok.Getter;
import lombok.ToString;

@ToString(callSuper=true)
@Getter
public class MutualFundTradeConfirmation extends ExchangeTradeMessageType {

  private static final long serialVersionUID = 1L;
  private long exchangeTradeTime;
  private String exchangeTradeId;
  private String clientCode;
  private String schemeCode;
  private String isin;
  private String folioNumber;
  private OrderSide orderSide;
  private String validFlag;
  private double allottedNav;
  private double allottedUnit;
  private double allottedAmount;
  private String bseRemarks;

  public MutualFundTradeConfirmation(String exchangeOrderID, String folioNumber, String isin, 
      long exchangeTradeTime, String clientCode, OrderSide orderSide, String validFlag, String schemeCode, double allottedNav,
      double allottedUnit, double allottedAmount, String bseRemarks) {
    super(clientCode, exchangeOrderID);
    this.exchangeTradeId = exchangeOrderID;
    this.clientCode = clientCode;
    this.schemeCode = schemeCode;
    this.exchangeTradeTime = exchangeTradeTime;
    this.folioNumber = folioNumber;
    this.isin = isin;
    this.orderSide = orderSide;
    this.validFlag = validFlag;
    this.allottedAmount = allottedAmount;
    this.allottedNav = allottedNav;
    this.allottedUnit = allottedUnit;
    this.bseRemarks = bseRemarks;

  }
  
  public static MutualFundTradeConfirmation allotmentConfirmation(String exchangeOrderID, String folioNumber, String isin,
      long exchangeTradeTime, String clientCode, String schemeCode, double allottedNav,
      double allottedUnit, String bseRemarks) {
    return new MutualFundTradeConfirmation(exchangeOrderID, folioNumber, isin, exchangeTradeTime, clientCode, BUY, "Y", schemeCode, allottedNav, allottedUnit, 0., bseRemarks);
  }
  
  public static MutualFundTradeConfirmation redemptionConfirmation(String exchangeOrderID, String folioNumber, String isin,
      long exchangeTradeTime, String clientCode, String schemeCode, double allottedNav,
      double allottedAmount, String bseRemarks) {
    return new MutualFundTradeConfirmation(exchangeOrderID, folioNumber, isin, exchangeTradeTime, clientCode, SELL, "Y", schemeCode, allottedNav, 0., allottedAmount, bseRemarks);
  }
  
  public static MutualFundTradeConfirmation allotmentRejection(String exchangeOrderID, String folioNumber, String isin,
      long exchangeTradeTime, String clientCode, String schemeCode, double allottedNav,
      double allottedUnit, String bseRemarks) {
    return new MutualFundTradeConfirmation(exchangeOrderID, folioNumber, isin, exchangeTradeTime, clientCode, BUY, "N", schemeCode, allottedNav, allottedUnit, 0., bseRemarks);
  }
  
  public static MutualFundTradeConfirmation redemptionRejection(String exchangeOrderID, String folioNumber, String isin,
      long exchangeTradeTime, String clientCode, String schemeCode, double allottedNav,
      double allottedAmount, String bseRemarks) {
    return new MutualFundTradeConfirmation(exchangeOrderID, folioNumber, isin, exchangeTradeTime, clientCode, SELL, "N", schemeCode, allottedNav, 0., allottedAmount, bseRemarks);
  }

}