package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;
import com.wt.domain.OrderActorStates;
import com.wt.domain.OrderSide;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper=true)
public class OrderTerminatedAtEOD extends ExchangeOrderUpdateEvent {
  
  private static final long serialVersionUID = 1L;
  private OrderActorStates orderState;
  private OrderSide orderSide;
  private String exchangeOrderId;

  public OrderTerminatedAtEOD(DestinationAddress destinationAddress, OrderActorStates orderState,
                              OrderSide orderSide, String exchangeOrderId) {
    super(destinationAddress);
    this.orderState = orderState;
    this.orderSide = orderSide;
    this.exchangeOrderId = exchangeOrderId;
  }
  
  

}
