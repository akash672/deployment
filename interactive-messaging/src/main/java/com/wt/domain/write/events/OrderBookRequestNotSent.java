package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderBookRequestNotSent extends OrderEvent {

  private static final long serialVersionUID = 1L;
  @Getter private String message;
  
}
