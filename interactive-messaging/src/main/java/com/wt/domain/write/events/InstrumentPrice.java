package com.wt.domain.write.events;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@DynamoDBDocument
@NoArgsConstructor
@ToString(callSuper=false)
@EqualsAndHashCode(callSuper=false, exclude={"time"})
public class InstrumentPrice extends AssetClassEvent implements Serializable,Cloneable{
  private static final long serialVersionUID = 1L;
  @Getter @DynamoDBAttribute private double price;
  @Getter @DynamoDBAttribute private long time;
  @Getter @DynamoDBAttribute private String wdId;
  
  @JsonCreator
  public InstrumentPrice(@JsonProperty("price") double price, @JsonProperty("time")  long time, @JsonProperty("wdId") String token)
  {
    this.price = price;
    this.time = time;
    this.wdId = token;
  }
  
  public InstrumentPrice withPrice(double price) {
    return new InstrumentPrice(price, time, wdId);
  }

  public InstrumentPrice withPriceTime(double price, long time) {
    return new InstrumentPrice(price, time, wdId);
  }

  @Override
  public InstrumentPrice clone() {
    return new InstrumentPrice(price, time, wdId);
  }
}
