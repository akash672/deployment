package com.wt.domain.write.commands;

import com.wt.domain.BrokerName;
import com.wt.domain.InvestingMode;
import com.wt.domain.OrderExecutionMetaData;
import com.wt.domain.Purpose;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
public class CreatedOrder extends OrderRootCommand {
  private static final long serialVersionUID = 1L;
  private String senderPath;
  private String clientOrderId;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private String userEmail;
  private String clientCode;
  private OrderExecutionMetaData orderExecutionMetaData;
  private String exchange;
  private BrokerAuthInformation brokerAuthInformation;

  public CreatedOrder(String senderPath, String clientOrderId, BrokerName brokerName, Purpose purpose,
      InvestingMode investingMode, String userEmail, String clientCode, OrderExecutionMetaData orderExecutionMetaData,
      String exchange, BrokerAuthInformation brokerAuthInformation) {
    super();
    this.senderPath = senderPath;
    this.clientOrderId = clientOrderId;
    this.brokerName = brokerName;
    this.purpose = purpose;
    this.investingMode = investingMode;
    this.userEmail = userEmail;
    this.clientCode = clientCode;
    this.orderExecutionMetaData = orderExecutionMetaData;
    this.exchange = exchange;
    this.brokerAuthInformation = brokerAuthInformation;
  }

  @Getter
  public static class CreatedOrderBuilder {
    private String senderPath;
    private String clientOrderId;
    private BrokerName brokerName;
    private Purpose purpose;
    private InvestingMode investingMode;
    private String userEmail;
    private String clientCode;
    private OrderExecutionMetaData orderExecutionMetaData;
    private String exchange;
    private BrokerAuthInformation brokerAuthInformation;

    public CreatedOrderBuilder(String clientOrderId, InvestingMode investingMode, String userEmail, String exchange) {
      super();
      this.clientOrderId = clientOrderId;
      this.investingMode = investingMode;
      this.userEmail = userEmail;
      this.exchange = exchange;
    }

    public CreatedOrderBuilder withOrderExecutionMetadata(OrderExecutionMetaData orderExecutionMetadata) {
      this.orderExecutionMetaData = orderExecutionMetadata;
      return this;
    }

    public CreatedOrderBuilder withSenderPath(String senderPath) {
      this.senderPath = senderPath;
      return this;
    }

    public CreatedOrderBuilder withInvestingPurpose(Purpose purpose) {
      this.purpose = purpose;
      return this;
    }
    
    public CreatedOrderBuilder withClientCode(String clientCode) {
      this.clientCode = clientCode;
      return this;
    }
    
    public CreatedOrderBuilder withBrokerName(BrokerName brokerName) {
      this.brokerName = brokerName;
      return this;
    }
    
    public CreatedOrderBuilder withBrokerAuthentication(BrokerAuthInformation brokerAuthInformation) {
      this.brokerAuthInformation = brokerAuthInformation;
      return this;
    }

    public CreatedOrder build() {
      return new CreatedOrder(senderPath, clientOrderId, brokerName, purpose, investingMode, userEmail, clientCode,
          orderExecutionMetaData, exchange,brokerAuthInformation);
    }
  }
}