package com.wt.domain.write.events;

import com.wt.domain.CancelOrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class CancelOrderRejected extends CancelOrderEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private CancelOrderActorStates cancelOrderActorStates;
  @Getter
  private String localOrderId;
  @Getter
  String msg;

  public CancelOrderRejected(CancelOrderActorStates cancelOrderActorStates, String localOrderId, String msg) {
    super();
    this.cancelOrderActorStates = cancelOrderActorStates;
    this.localOrderId = localOrderId;
    this.msg = msg;
  }

}