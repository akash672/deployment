package com.wt.domain.write.commands;

import com.wt.domain.OrderSide;
import com.wt.domain.write.events.OrderEvent;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@Builder
public class MutualFundOrderTradeExecuted extends OrderEvent {
  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  private String isin;
  private OrderSide side;
  private double quantity;
  private double price;
  private String clientCode;
  private String folioNumber;
  private double allottedNav;
  private double allottedUnit;
  private double allottedAmount;
}
