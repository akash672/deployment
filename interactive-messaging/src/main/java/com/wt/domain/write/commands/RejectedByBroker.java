package com.wt.domain.write.commands;

import lombok.Getter;

public class RejectedByBroker extends OrderCommand {

  private static final long serialVersionUID = 1L;
  @Getter
  private String localOrderId;
  @Getter
  private String message;

  public RejectedByBroker(String localOrderId, String message) {
    super();
    this.localOrderId = localOrderId;
    this.message = message;
  }

}
