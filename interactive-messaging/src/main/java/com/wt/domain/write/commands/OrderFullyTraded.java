package com.wt.domain.write.commands;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class OrderFullyTraded {
  private String wdOrderId;
  private String clientCode;
  private String exchangeOrderID;
  private String brokerOrderID;
  

  public OrderFullyTraded(String wdOrderId, String clientCode, String exchangeOrderID, String brokerOrderID) {
    super();
    this.wdOrderId = wdOrderId;
    this.clientCode = clientCode;
    this.exchangeOrderID = exchangeOrderID;
    this.brokerOrderID = brokerOrderID;
  }

}
