package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode(callSuper=true)
@ToString
public class ExchangeOrderUpdateAcknowledged extends Done {

  private static final long serialVersionUID = 1L;
  @Getter private ExchangeOrderUpdateEvent exchangeOrderUpdate;
  @Getter private String issueType;
  
  public ExchangeOrderUpdateAcknowledged(String id, String message, ExchangeOrderUpdateEvent exchangeOrderUpdateEvent, String issueType) {
    super(id, message);
    this.exchangeOrderUpdate = exchangeOrderUpdateEvent;
    this.issueType = issueType;
  }

}
