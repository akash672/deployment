package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class GetPriceBasketFromTracker extends AssetClassCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  Collection<String> collectionOfWdId;

}
