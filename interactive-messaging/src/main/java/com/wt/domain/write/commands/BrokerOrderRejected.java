package com.wt.domain.write.commands;

import com.wt.domain.CtclMessageType;
import com.wt.domain.OrderSide;

import lombok.Getter;

@Getter
public class BrokerOrderRejected extends CtclMessageType {

  private static final long serialVersionUID = 1L;
  private String message;

  public BrokerOrderRejected(String localOrderID, String brokerOrderID, String message, String clientCode, 
      String token, int quantity, OrderSide orderSide) {
    super(localOrderID, brokerOrderID, clientCode, token, quantity, orderSide);
    this.message = message;
  }

}
