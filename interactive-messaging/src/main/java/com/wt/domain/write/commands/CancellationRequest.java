package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class CancellationRequest extends OrderRootCommand{

  private static final long serialVersionUID = 1L;
  private String clientOrderId;

  
}
