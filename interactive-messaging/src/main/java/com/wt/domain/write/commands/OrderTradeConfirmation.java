package com.wt.domain.write.commands;

import com.wt.domain.OrderSide;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class OrderTradeConfirmation extends OrderCommand {

  private static final long serialVersionUID = 1L;

  private String brokerOrderId;
  private String exchangeOrderId;
  private long orderConfirmationTime;
  private float price;
  private int qty;
  private String tradeOrderId;
  private String clientCode;
  private String token;
  private int quantity;
  private OrderSide orderSide;
  

  public OrderTradeConfirmation(String brokerOrderId, String exchangeOrderId, long orderConfirmationTime, float price, int qty,
      String tradeOrderId, String clientCode, String token, int quantity, OrderSide orderSide) {
    this.brokerOrderId = brokerOrderId;
    this.exchangeOrderId = exchangeOrderId;
    this.orderConfirmationTime = orderConfirmationTime;
    this.price = price;
    this.qty = qty;
    this.tradeOrderId = tradeOrderId;
    this.clientCode = clientCode;
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
  }

}