package com.wt.domain.write.commands;

import java.io.Serializable;

import com.wt.domain.CtclOrder;

import lombok.Getter;
import lombok.ToString;

@ToString
public class SendTradeOrderConfirmation implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private CtclOrder order;
  @Getter private Long brokerOrderId;
  @Getter private Long exchangeOrderId;
  
  public SendTradeOrderConfirmation(CtclOrder order, Long brokerOrderId, Long exchangeOrderId) {
    super();
    this.order = order;
    this.brokerOrderId = brokerOrderId;
    this.exchangeOrderId = exchangeOrderId;
  }
}
