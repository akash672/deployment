package com.wt.domain.write;

import static com.wt.domain.BrokerName.MOSL;
import static com.wt.domain.BrokerName.fromBrokerName;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CtclInteractive;
import com.wt.domain.BrokerName;
import com.wt.domain.BrokerType;
import com.wt.domain.CTCLInteractiveFactory;
import com.wt.domain.DestinationAddress;
import com.wt.domain.EquityInstrument;
import com.wt.domain.InvestingMode;
import com.wt.domain.MarginRequestMessage;
import com.wt.domain.OrderBookMessage;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.write.commands.BrokerOrderAccepted;
import com.wt.domain.write.commands.BrokerOrderRejected;
import com.wt.domain.write.commands.CancellationRequest;
import com.wt.domain.write.commands.CreatedOrder;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.ExchangeOrderAccepted;
import com.wt.domain.write.commands.ExchangeOrderRejected;
import com.wt.domain.write.commands.ExchangeOrderUpdateEventWithDeliveryId;
import com.wt.domain.write.commands.GetMarginRequest;
import com.wt.domain.write.commands.GetWdIdToInstrumentMap;
import com.wt.domain.write.commands.HasCorpActionOrderBeenExecutedQuery;
import com.wt.domain.write.commands.HasCorpActionOrderBeenPlacedWithExchangeQuery;
import com.wt.domain.write.commands.HasCorporateActionOrderBeenReceivedQuery;
import com.wt.domain.write.commands.HasCreatedOrderBeenReceived;
import com.wt.domain.write.commands.MessageReceptionConfirmation;
import com.wt.domain.write.commands.OrderCancellationRejected;
import com.wt.domain.write.commands.OrderCancelled;
import com.wt.domain.write.commands.OrderFullyTraded;
import com.wt.domain.write.commands.OrderPlacementUpdate;
import com.wt.domain.write.commands.OrderRejectedMessage;
import com.wt.domain.write.commands.OrderTradeConfirmation;
import com.wt.domain.write.commands.ProvideOrderBook;
import com.wt.domain.write.commands.ProvideTradeBook;
import com.wt.domain.write.commands.ReceivedByBroker;
import com.wt.domain.write.commands.RejectedByBroker;
import com.wt.domain.write.commands.RejectedByExchange;
import com.wt.domain.write.commands.SentToExchange;
import com.wt.domain.write.commands.ShuttingDownActorAtEOD;
import com.wt.domain.write.commands.SubscribeToExchangeUpdateEventStream;
import com.wt.domain.write.commands.TradeOrderConfirmation;
import com.wt.domain.write.commands.WdIdToInstrumentMapSent;
import com.wt.domain.write.events.CancellationRejected;
import com.wt.domain.write.events.ChildShutDownAtEOD;
import com.wt.domain.write.events.ExchangeOrderUpdateEvent;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.MessageDeliveredSuccessfully;
import com.wt.domain.write.events.OrderAcceptedByBroker;
import com.wt.domain.write.events.OrderAcceptedByExchange;
import com.wt.domain.write.events.OrderActorCreated;
import com.wt.domain.write.events.OrderCancellation;
import com.wt.domain.write.events.OrderCancellationRequest;
import com.wt.domain.write.events.OrderCompletion;
import com.wt.domain.write.events.OrderHasBeenReceived;
import com.wt.domain.write.events.OrderHasNotBeenReceived;
import com.wt.domain.write.events.OrderNotSent;
import com.wt.domain.write.events.OrderPlacedWithExchange;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.OrderRejectionCtcl;
import com.wt.domain.write.events.OrderRejectionExchange;
import com.wt.domain.write.events.OrderRejectionMessage;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.persistence.AbstractPersistentActorWithAtLeastOnceDelivery;
import akka.persistence.RecoveryCompleted;

@Service("EquityOrderRootActor")
@Scope("prototype")
public class EquityOrderRootActor extends AbstractPersistentActorWithAtLeastOnceDelivery implements ParentActor<EquityOrderActor> {

  private short LOCAL_ORDER_ID = 0;
  @Autowired
  private CTCLInteractiveFactory ctclFactory;
  @Autowired
  private WDActorSelections wdActorSelections;
  private Map<String, String> localOrderIdToWDOrderIdMap = new HashMap<String, String>();
  private Map<String, String> brokerIdToWDOrderIdMap = new HashMap<String, String>();
  private Map<String, String> exchangeIdToWDOrderIdMap = new HashMap<String, String>();
  private Map<String, List<String>> clientCodeToWDOrderIDsMap = new HashMap<String, List<String>>();
  private Set<String> allReceivedWdOrderIds = new java.util.HashSet<String>();
  private Map<String, String> clientCodeUserEmailMap = new HashMap<>();
  private Map<String, Double> corporateActionWdIdsToPrice = new HashMap<>();
  private Map<String, EquityInstrument> wdIdToEquityInstrumentMap = new HashMap<>();
  
  @Override
  public Receive createReceive() {
    return receiveBuilder().match(CreatedOrder.class, evt -> {
      createOrderActor(evt);
    }).match(ExchangeOrderUpdateEvent.class, evt -> {
      handleExchangeOrderUpdateEvent(evt);
    }).match(MessageReceptionConfirmation.class, evt -> {
      handleMessageReceptionConfirmation(evt);
    }).match(BrokerOrderAccepted.class, evt -> {
      handleBrokerOrderAccepted(evt);
    }).match(ExchangeOrderAccepted.class, evt -> {
      handleExchangeOrderAccepted(evt);
    }).match(TradeOrderConfirmation.class, evt -> {
      handleTradeOrderConfirmation(evt);
    }).match(OrderFullyTraded.class, evt -> {
      handleOrderFullyTraded(evt);
    }).match(OrderCancelled.class, evt -> {
      handleOrderCancelled(evt);
    }).match(BrokerOrderRejected.class, evt -> {
      handleBrokerOrderRejected(evt);
    }).match(ExchangeOrderRejected.class, evt -> {
      handleExchangeOrderRejected(evt);
    }).match(OrderRejectedMessage.class, evt -> {
      handleOrderRejectedMessage(evt);
    }).match(ShuttingDownActorAtEOD.class, evt -> {
      handleActorShutDownAtEOD(evt);
    }).match(OrderBookMessage.class, evt -> {
      handleOrderBookMessage(evt);
    }).match(CancellationRequest.class, evt -> {
      handleCancellationRequest(evt);
    }).match(OrderCancellationRejected.class, evt -> {
      handleCancellationOrderRejected(evt);
    }).match(GetMarginRequest.class, evt -> {
       handleGetMarginRequest(evt);
    }).match(MarginRequestMessage.class, evt -> {
       handleMarginRequestMessage(evt);
    }).match(HasCreatedOrderBeenReceived.class, evt -> {
       handleCreatedOrderQueryRequest(evt);
   }).match(ProvideOrderBook.class, evt -> {
       handleProvideOrderBookRequest(evt);
   }).match(ProvideTradeBook.class, evt -> {
       handleProvideTradeBookRequest(evt);
   }).match(HasCorporateActionOrderBeenReceivedQuery.class, evt -> {
     handleCreatedCorpActionOrderBeenReceivedQuery(evt);
   }).match(HasCorpActionOrderBeenPlacedWithExchangeQuery.class, evt -> {
     handleHasCorpActionOrderBeenPlacedWithExchangeQuery(evt);
   }).match(HasCorpActionOrderBeenExecutedQuery.class, evt -> {
     handleCorpActionOrderBeenExecutedQuery(evt);
   }).match(EODCompleted.class, command -> {
     log.info("Received command " + command);
     updateWdIdtoInstrumentsMap();
   }).match(OrderPlacementUpdate.class, evt -> {
     forwardOrderPlacementFailure(evt);
   }).build();
  }

  private void forwardOrderPlacementFailure(OrderPlacementUpdate evt) {
    tellChildren(context(), self(), new OrderNotSent(evt.getOrderResponse()), localOrderIdToWDOrderIdMap.get(evt.getLocalOrderId())); 
  }

  private void handleMessageReceptionConfirmation(MessageReceptionConfirmation messageReceptionConfirmation) {
    persist(new MessageDeliveredSuccessfully(messageReceptionConfirmation.getDeliveryId()), 
        messageDelivered -> confirmDelivery(messageDelivered.getDeliveryId()));
  }

  private void handleExchangeOrderUpdateEvent(ExchangeOrderUpdateEvent exchangeOrderUpdateEvent) {
    persist(exchangeOrderUpdateEvent, 
        (exchangeOrderUpdateReceived) -> deliver(wdActorSelections.userRootActor(), 
            deliveryId -> new ExchangeOrderUpdateEventWithDeliveryId(exchangeOrderUpdateReceived, deliveryId, "Equity")));
  }

  private void handleCorpActionOrderBeenExecutedQuery(HasCorpActionOrderBeenExecutedQuery hasCorpActionOrderBeenExecuted) {
    if(corporateActionWdIdsToPrice.containsKey(hasCorpActionOrderBeenExecuted.getClientOrderId()))
      sender().tell(new TradeExecuted(new DestinationAddress(self().toString()), 
          hasCorpActionOrderBeenExecuted.getExchangeOrderId(),
          UUID.randomUUID().toString(),
          hasCorpActionOrderBeenExecuted.getSide(),
          PriceWithPaf.withPrice(corporateActionWdIdsToPrice.get(hasCorpActionOrderBeenExecuted.getClientOrderId()),
              hasCorpActionOrderBeenExecuted.getWdId(), 
              Pafs.withCumulativePaf(1.)),
          hasCorpActionOrderBeenExecuted.getQuantity()), self());
  }

  private void handleHasCorpActionOrderBeenPlacedWithExchangeQuery(HasCorpActionOrderBeenPlacedWithExchangeQuery isCorpActionOrderPlacedWithExchange) {
    if(corporateActionWdIdsToPrice.containsKey(isCorpActionOrderPlacedWithExchange.getClientOrderId()))
        sender().tell(new OrderPlacedWithExchange(new DestinationAddress(self().toString()), 
            isCorpActionOrderPlacedWithExchange.getBrokerOrderId(),
            UUID.randomUUID().toString(), isCorpActionOrderPlacedWithExchange.getBrokerOrderId(),
            isCorpActionOrderPlacedWithExchange.getSide(),
            isCorpActionOrderPlacedWithExchange.getQuantity()), self());
  }

  private void handleCreatedCorpActionOrderBeenReceivedQuery(HasCorporateActionOrderBeenReceivedQuery hasCorporateActionOrderBeenReceived) {
    if(corporateActionWdIdsToPrice.containsKey(hasCorporateActionOrderBeenReceived.getClientOrderId()))
      sender().tell(new OrderReceivedWithBroker(new DestinationAddress(self().toString()), hasCorporateActionOrderBeenReceived.getClientOrderId(),
          UUID.randomUUID().toString()), self());
    else
          sender().tell(new OrderHasNotBeenReceived(hasCorporateActionOrderBeenReceived.getClientOrderId()), self());
  }

  private void createOrderActor(CreatedOrder createOrder) {
    String clientOrderId = createOrder.getClientOrderId();
    String localOrderId = generateLocalOrderId(createOrder);
    addToClientToWDOrderIdMap(createOrder.getClientCode(), clientOrderId);
    allReceivedWdOrderIds.add(clientOrderId);
    localOrderIdToWDOrderIdMap.put(clientOrderId, clientOrderId);
    localOrderIdToWDOrderIdMap.put(String.valueOf(localOrderId), clientOrderId);
    if(createOrder.getPurpose() == Purpose.CORPORATEACTION){
      corporateActionWdIdsToPrice.put(createOrder.getClientOrderId(),createOrder.getOrderExecutionMetaData().getLimitPrice());
    }
    String orderWdId =  createOrder.getOrderExecutionMetaData().getToken();
    updateWdIdtoInstrumentsMapIfWdIdNotFound(orderWdId);
    if(!wdIdToEquityInstrumentMap.containsKey(orderWdId))
      return;
    String orderToken =  wdIdToEquityInstrumentMap.get(orderWdId).getToken();
    String orderSymbol =  wdIdToEquityInstrumentMap.get(orderWdId).getSymbol();
    createOrder.getOrderExecutionMetaData().setToken(orderToken);
    createOrder.getOrderExecutionMetaData().setSymbol(orderSymbol);;
    OrderActorCreated orderActorCreated = new OrderActorCreated(
        createOrder.getClientCode(),
        createOrder.getUserEmail(), 
        createOrder.getBrokerName(), 
        createOrder.getPurpose(),
        createOrder.getInvestingMode(),
        createOrder.getOrderExecutionMetaData(),
        createOrder.getSenderPath(),
        localOrderId, 
        clientOrderId,
        createOrder.getExchange(),
        createOrder.getBrokerAuthInformation());
    persist(orderActorCreated, event -> {
      tellChildren(context(), self(), orderActorCreated, event.getClientOrderId()); 
    });
  }

  private void updateWdIdtoInstrumentsMapIfWdIdNotFound(String wdId) {
    if(!wdIdToEquityInstrumentMap.containsKey(wdId))
      updateWdIdtoInstrumentsMap();
  }

  private void updateWdIdtoInstrumentsMap() {

    WdIdToInstrumentMapSent wdIdToInstrumentMapSent = null;
    boolean instrumentsMapReceived = false;
    int retryCounter = 1;
    while (!instrumentsMapReceived && retryCounter <= 2) {
      try {
        Object result = askAndWaitForResult(wdActorSelections.universeRootActor(), new GetWdIdToInstrumentMap());

        if (result instanceof Failed) {
          retryCounter++;
          log.warn("EquityOrderRootActor :Failed to get instruments by wdId from EquityActor. Retrying " + result);
          continue;
        }
        wdIdToInstrumentMapSent = (WdIdToInstrumentMapSent) result;
        wdIdToEquityInstrumentMap = wdIdToInstrumentMapSent.getWdIdtoInstruments();
        instrumentsMapReceived = true;
        log.info("EquityOrderRootActor : wdIdToInstrument Map updated");
      } catch (Exception e) {
        log.warn("EquityOrderRootActor :Exception while getting instruments by wdId from EquityActor. Retrying ", e);
        retryCounter++;
      }
    }
    if(retryCounter >2 )
      log.error("EquityOrderRootActor : Failed to get instruments map for universe actor after multiple attempts.");
  }

  private void handleBrokerOrderAccepted(BrokerOrderAccepted ctclOrderAccepted) {
    if(localOrderIdToWDOrderIdMap.containsKey(String.valueOf(ctclOrderAccepted.getLocalOrderID()))){
    persist(
        new OrderAcceptedByBroker(localOrderIdToWDOrderIdMap.get(String.valueOf(ctclOrderAccepted.getLocalOrderID())),
            String.valueOf(ctclOrderAccepted.getLocalOrderID()), ctclOrderAccepted.getBrokerOrderID()),
        event -> {
          brokerIdToWDOrderIdMap.put(ctclOrderAccepted.getBrokerOrderID(),
              localOrderIdToWDOrderIdMap.get(String.valueOf(ctclOrderAccepted.getLocalOrderID())));
          forwardToChildren(context(),
              new ReceivedByBroker((ctclOrderAccepted.getLocalOrderID()),
                  ctclOrderAccepted.getBrokerOrderID()),
              localOrderIdToWDOrderIdMap.get(String.valueOf(ctclOrderAccepted.getLocalOrderID())));
          String clientOrderId = localOrderIdToWDOrderIdMap.get(String.valueOf(ctclOrderAccepted.getLocalOrderID()));
          localOrderIdToWDOrderIdMap.remove(clientOrderId);
          localOrderIdToWDOrderIdMap.remove(String.valueOf(ctclOrderAccepted.getLocalOrderID()));
        });
    }
  }

  private void handleExchangeOrderAccepted(ExchangeOrderAccepted exchangeOrderAccepted) {
    if(brokerIdToWDOrderIdMap.containsKey(exchangeOrderAccepted.getBrokerOrderId())){
      persist(new OrderAcceptedByExchange(brokerIdToWDOrderIdMap.get((exchangeOrderAccepted.getBrokerOrderId())),
          exchangeOrderAccepted.getBrokerOrderId(), exchangeOrderAccepted.getExchOrderID()), event -> {
                exchangeIdToWDOrderIdMap.put(exchangeOrderAccepted.getExchOrderID(),
                    brokerIdToWDOrderIdMap.get(exchangeOrderAccepted.getBrokerOrderId()));
                forwardToChildren(context(),
                    new SentToExchange(exchangeOrderAccepted.getBrokerOrderId(),
                        exchangeOrderAccepted.getExchOrderID(), exchangeOrderAccepted.getExchOrderTime(),
                        exchangeOrderAccepted.getClientCode(), exchangeOrderAccepted.getToken(), 
                        exchangeOrderAccepted.getQuantity(), exchangeOrderAccepted.getOrderSide()),
                    brokerIdToWDOrderIdMap.get(exchangeOrderAccepted.getBrokerOrderId()));
               
    });
    }else {
      forwardToChildren(context(),
                    new SentToExchange(exchangeOrderAccepted.getBrokerOrderId(),
                        exchangeOrderAccepted.getExchOrderID(), exchangeOrderAccepted.getExchOrderTime(),
                        exchangeOrderAccepted.getClientCode(), exchangeOrderAccepted.getToken(), 
                        exchangeOrderAccepted.getQuantity(), exchangeOrderAccepted.getOrderSide()),
                    clientCodeToWDOrderIDsMap.get(exchangeOrderAccepted.getClientCode()).toArray(new String[0]));
              }
        

  }
  
  private void handleTradeOrderConfirmation(TradeOrderConfirmation tradeOrderConfirmation) {
    if(exchangeIdToWDOrderIdMap.containsKey(tradeOrderConfirmation.getExchangeOrderId())){
      forwardToChildren(context(),
            new OrderTradeConfirmation(
                tradeOrderConfirmation.getBrokerOrderId(), 
                tradeOrderConfirmation.getExchangeOrderId(),
                tradeOrderConfirmation.getExchangeTradeTime(), 
                tradeOrderConfirmation.getTradePrice(),
                (int) tradeOrderConfirmation.getTradeQty(), 
                tradeOrderConfirmation.getExchangeTradeId(),
                tradeOrderConfirmation.getClientCode(), 
                tradeOrderConfirmation.getToken(),
                (int)tradeOrderConfirmation.getQuantity(), 
                tradeOrderConfirmation.getOrderSide()),
            exchangeIdToWDOrderIdMap.get(tradeOrderConfirmation.getExchangeOrderId()));
        } else {
          forwardToChildren(context(),
              new OrderTradeConfirmation(tradeOrderConfirmation.getBrokerOrderId(), tradeOrderConfirmation.getExchangeOrderId(),
                  tradeOrderConfirmation.getExchangeTradeTime(), tradeOrderConfirmation.getTradePrice(),
                  (int) tradeOrderConfirmation.getTradeQty(), tradeOrderConfirmation.getExchangeTradeId(),
                  tradeOrderConfirmation.getClientCode(), tradeOrderConfirmation.getToken(),
                  (int)tradeOrderConfirmation.getQuantity(), tradeOrderConfirmation.getOrderSide()),
              clientCodeToWDOrderIDsMap.get(tradeOrderConfirmation.getClientCode()).toArray(new String[0]));
        }

  }

  private void handleOrderFullyTraded(OrderFullyTraded orderFullyTraded) {
    persist(new OrderCompletion(orderFullyTraded.getWdOrderId(), orderFullyTraded.getClientCode(),
        orderFullyTraded.getExchangeOrderID(), orderFullyTraded.getBrokerOrderID()), event -> {
            brokerIdToWDOrderIdMap.remove(orderFullyTraded.getBrokerOrderID());
            exchangeIdToWDOrderIdMap.remove(orderFullyTraded.getExchangeOrderID());
        });

  }

  private void handleBrokerOrderRejected(BrokerOrderRejected ctclOrderRejected) {
    persist(new OrderRejectionCtcl(localOrderIdToWDOrderIdMap.get(String.valueOf(ctclOrderRejected.getLocalOrderID())),
        ctclOrderRejected.getLocalOrderID(), ctclOrderRejected.getBrokerOrderID(), ctclOrderRejected.getMessage()) , event -> {
        forwardToChildren(context(),
            new RejectedByBroker((ctclOrderRejected.getLocalOrderID()), ctclOrderRejected.getMessage()),
            localOrderIdToWDOrderIdMap.get(String.valueOf(ctclOrderRejected.getLocalOrderID())));
        localOrderIdToWDOrderIdMap.remove(String.valueOf(ctclOrderRejected.getLocalOrderID()));
        
        });
  }

  private void handleExchangeOrderRejected(ExchangeOrderRejected exchangeOrderRejected) {
    if(brokerIdToWDOrderIdMap.containsKey(exchangeOrderRejected.getBrokerOrderId())){
        persist(new OrderRejectionExchange(brokerIdToWDOrderIdMap.get(exchangeOrderRejected.getBrokerOrderId()),
            exchangeOrderRejected.getExchOrderID(), exchangeOrderRejected.getBrokerOrderId(),exchangeOrderRejected.getMessage()), event -> {
              forwardToChildren(context(),
                  new RejectedByExchange(exchangeOrderRejected.getBrokerOrderId(),
                      exchangeOrderRejected.getMessage(), exchangeOrderRejected.getClientCode(), exchangeOrderRejected.getToken(),
                      exchangeOrderRejected.getQuantity(), exchangeOrderRejected.getOrderSide()),
                  brokerIdToWDOrderIdMap.get(exchangeOrderRejected.getBrokerOrderId()));
              brokerIdToWDOrderIdMap.remove(exchangeOrderRejected.getBrokerOrderId());
            });
      } else {
        forwardToChildren(context(),
            new RejectedByExchange(exchangeOrderRejected.getBrokerOrderId(),
                exchangeOrderRejected.getMessage(), exchangeOrderRejected.getClientCode(),
                exchangeOrderRejected.getToken(), exchangeOrderRejected.getQuantity(),
                exchangeOrderRejected.getOrderSide()),
            clientCodeToWDOrderIDsMap.get(exchangeOrderRejected.getClientCode()).toArray(new String[0]));
        
        
      }

  }
  
  private void handleOrderRejectedMessage(OrderRejectedMessage orderRejectedMessage){
   persist(new OrderRejectionMessage(orderRejectedMessage.getWdOrderId(), orderRejectedMessage.getClientCode()) , event -> {
   });
    
  }
  
  private void handleActorShutDownAtEOD(ShuttingDownActorAtEOD shuttingDownActorAtEOD){
    persist(new ChildShutDownAtEOD(shuttingDownActorAtEOD.getClientOrderId(), shuttingDownActorAtEOD.getClientCode()) , event -> {
    });
     
   }
  
  private void handleOrderBookMessage(OrderBookMessage orderBookMessage) {
    forwardToChildren(context(), 
        orderBookMessage, 
        clientCodeToWDOrderIDsMap.get(orderBookMessage.getClientCode()).toArray(new String[0]));
  }
  
  private void handleOrderCancelled(OrderCancelled orderCancelled) {
    persist(new OrderCancellation(orderCancelled.getClientOrderId(), orderCancelled.getClientCode(),
        orderCancelled.getExchangeOrderId(), orderCancelled.getBrokerOrderId()), event -> {
            brokerIdToWDOrderIdMap.remove(Long.parseLong(orderCancelled.getBrokerOrderId()));
            exchangeIdToWDOrderIdMap.remove(Long.parseLong(orderCancelled.getExchangeOrderId()));
        });
  }

  private void handleCancellationRequest(CancellationRequest cancellationRequest) {
    Short localOrderId = getlocalID();
    String clientOrderId = cancellationRequest.getClientOrderId();
    localOrderIdToWDOrderIdMap.put(String.valueOf(localOrderId), clientOrderId);
    OrderCancellationRequest orderCancellationRequest = new OrderCancellationRequest(cancellationRequest.getClientOrderId(), 
                                                                                    localOrderId);
    persist(
        orderCancellationRequest , event ->{
            forwardToChildren(context(), orderCancellationRequest , orderCancellationRequest.getClientOrderId());
        });
   }
  
  private void handleMarginRequestMessage(MarginRequestMessage marginRequestMessage) {
    if (clientCodeUserEmailMap.containsKey(marginRequestMessage.getClientCode())) {
      String emailId = clientCodeUserEmailMap.get(marginRequestMessage.getClientCode());
      wdActorSelections.userRootActor().tell(
          new MarginReceived(marginRequestMessage.getMarginAmount(), emailId, InvestingMode.REAL), ActorRef.noSender());
      clientCodeUserEmailMap.remove(marginRequestMessage.getClientCode());
    }
  }

  private void handleCreatedOrderQueryRequest(HasCreatedOrderBeenReceived hasCreatedOrderBeenReceived) {
    if (allReceivedWdOrderIds.stream().anyMatch(wdOrderId -> hasCreatedOrderBeenReceived.getClientOrderId().equals(wdOrderId)))
        sender().tell(new OrderHasBeenReceived(hasCreatedOrderBeenReceived.getClientOrderId()), self());
    else
        sender().tell(new OrderHasNotBeenReceived(hasCreatedOrderBeenReceived.getClientOrderId()), self());
  }
  
  private void handleProvideOrderBookRequest(ProvideOrderBook provideOrderBook) {
      forwardToChildren(context(), provideOrderBook, provideOrderBook.getClientOrderId());
     }
  
  private void handleProvideTradeBookRequest(ProvideTradeBook provideTradeBook) {
    forwardToChildren(context(), provideTradeBook, provideTradeBook.getClientOrderId());
  }
  
  private void handleGetMarginRequest(GetMarginRequest getMarginRequest) {
    log.debug("Received Margin Request: "+getMarginRequest);
    CtclInteractive ctcl;
    BrokerName brokerName = fromBrokerName(getMarginRequest.getBrokerCompany(), InvestingMode.REAL);
    try {
      ctcl = ctclFactory.getCTCL(new BrokerType(Purpose.EQUITYSUBSCRIPTION, InvestingMode.REAL, brokerName));
      ctcl.sendMarginRequest(getMarginRequest.getClientCode());
    } catch (Exception e) {
      log.error("Error while getting margin request " + e.getMessage());
    }
    clientCodeUserEmailMap.put(getMarginRequest.getClientCode(),getMarginRequest.getEmailId());
  }

  private void handleCancellationOrderRejected(OrderCancellationRejected orderCancellationRejected) {
    persist(new CancellationRejected(orderCancellationRejected.getClientOrderId(), orderCancellationRejected.getClientCode(),
        orderCancellationRejected.getLocalOrderIdCancellation(), orderCancellationRejected.getBrokerOrderIdCancellation()), event -> {
            brokerIdToWDOrderIdMap.remove(orderCancellationRejected.getBrokerOrderIdCancellation());
            localOrderIdToWDOrderIdMap.remove(orderCancellationRejected.getLocalOrderIdCancellation());
        });
  }
  
  

  @Override
  public Receive createReceiveRecover() {

    return receiveBuilder().match(OrderActorCreated.class, evt -> {
      OrderActorCreated orderActorCreated = (OrderActorCreated) evt ;
      localOrderIdToWDOrderIdMap.put(String.valueOf(orderActorCreated.getLocalOrderId()),orderActorCreated.getClientOrderId());
      addToClientToWDOrderIdMap(orderActorCreated.getClientCode(), orderActorCreated.getClientOrderId());
      allReceivedWdOrderIds.add(orderActorCreated.getClientOrderId());
      if(orderActorCreated.getPurpose() == Purpose.CORPORATEACTION){
        corporateActionWdIdsToPrice.put(orderActorCreated.getClientOrderId(),orderActorCreated.getOrderExecutionMetaData().getLimitPrice());
      }
    }).match(OrderAcceptedByBroker.class, evt -> {
      OrderAcceptedByBroker orderAcceptedByBroker = (OrderAcceptedByBroker) evt;
      brokerIdToWDOrderIdMap.put((orderAcceptedByBroker).getBrokerOrderID(),
          (orderAcceptedByBroker).getClientOrderId());
      localOrderIdToWDOrderIdMap.remove(String.valueOf(orderAcceptedByBroker.getLocalOrderID()));
    }).match(OrderAcceptedByExchange.class, evt -> {
      exchangeIdToWDOrderIdMap.put(((OrderAcceptedByExchange) evt).getExchOrderId(),
          ((OrderAcceptedByExchange) evt).getClientOrderId());
    }).match(OrderRejectionExchange.class, evt -> {
      brokerIdToWDOrderIdMap.remove(((OrderRejectionExchange) evt).getBrokerOrderId());
    }).match(ChildShutDownAtEOD.class, evt -> {
      ChildShutDownAtEOD childShutDownAtEOD = (ChildShutDownAtEOD) evt;
      removeFromClientToWDOrderIdMap(childShutDownAtEOD.getClientCode(), childShutDownAtEOD.getClientOrderId());
    }).match(OrderCompletion.class, evt -> {
      brokerIdToWDOrderIdMap.remove(((OrderCompletion) evt).getBrokerOrderID());
      exchangeIdToWDOrderIdMap.remove(((OrderCompletion) evt).getExchangeOrderID());
    }).match(OrderRejectionMessage.class, evt -> {
      OrderRejectionMessage orderRejectionMessage = (OrderRejectionMessage) evt;
      brokerIdToWDOrderIdMap.values().remove(orderRejectionMessage.getClientOrderId());
      exchangeIdToWDOrderIdMap.values().remove(orderRejectionMessage.getClientOrderId());
    }).match(OrderCancellationRequest.class, evt -> {
      OrderCancellationRequest orderCancellationRequest = (OrderCancellationRequest) evt;
      localOrderIdToWDOrderIdMap.put(String.valueOf(orderCancellationRequest.getLocalOrderID()),orderCancellationRequest.getClientOrderId());
    }).match(OrderCancelled.class, evt -> {
      OrderCancelled orderCancelled = (OrderCancelled) evt;
      brokerIdToWDOrderIdMap.remove(Long.parseLong(orderCancelled.getBrokerOrderId()));
      exchangeIdToWDOrderIdMap.remove(Long.parseLong(orderCancelled.getExchangeOrderId()));
    }).match(ExchangeOrderUpdateEvent.class, evt -> {
      deliver(wdActorSelections.userRootActor(), 
          deliveryId -> new ExchangeOrderUpdateEventWithDeliveryId(evt, deliveryId, "Equity"));
    }).match(MessageDeliveredSuccessfully.class, evt -> {
      confirmDelivery(evt.getDeliveryId());
    }).match(RecoveryCompleted.class, evt -> {
      updateWdIdtoInstrumentsMap();
      recoverActiveOrders();
    }).build();
      
  }
  
  private void recoverActiveOrders() {
    clientCodeToWDOrderIDsMap
    .values()
    .stream()
    .forEach(activeOrderIds -> {
      for (String activeOrderId : activeOrderIds) {
        forwardToChildren(context(), new SubscribeToExchangeUpdateEventStream(), activeOrderId);
      }
    });
    
  }

  private void addToClientToWDOrderIdMap(String clientCode, String wdOrderId){
    if(clientCodeToWDOrderIDsMap.containsKey(clientCode)){
      List<String> wdIdsSet = clientCodeToWDOrderIDsMap.get(clientCode);
      wdIdsSet.add(wdOrderId);
    } else{
      List<String> wdIdsSet = new ArrayList<String>();
      wdIdsSet.add(wdOrderId);
      clientCodeToWDOrderIDsMap.put(clientCode, wdIdsSet);
    }
  }
  
  private void removeFromClientToWDOrderIdMap(String clientCode, String wdOrderId){
      List<String> wdIdsSet = clientCodeToWDOrderIDsMap.get(clientCode);
      wdIdsSet.remove(wdOrderId);

  }
  
  private short getlocalID() {
    LOCAL_ORDER_ID = (short) ((LOCAL_ORDER_ID % Short.MAX_VALUE) + 1);
    return LOCAL_ORDER_ID;
  }
  
  private String generateLocalOrderId(CreatedOrder createdOrder) {
    if(createdOrder.getBrokerName().equals(MOSL)){
      return parseBasketIdAndReturnLocalOrderId(createdOrder.getOrderExecutionMetaData().getBasketOrderCompositeId());
    }else
     LOCAL_ORDER_ID = (short) ((LOCAL_ORDER_ID % Short.MAX_VALUE) + 1);
    return String.valueOf(LOCAL_ORDER_ID);
  }
  
  private String parseBasketIdAndReturnLocalOrderId(String basketOrderCompositeId) {
    String[] parts = basketOrderCompositeId.split("__");
    return parts[0] + "_" + parts[1] ;
  }

  @Override
  public String getChildBeanName() {
    return "EquityOrderActor";
  }

  @Override
  public String getName() {
    return "EquityOrderRoot";
  }

  @Override
  public String persistenceId() {
    return "EquityOrderRootActor";
  }

}
