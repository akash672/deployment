package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class OrderHasNotBeenReceived implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private String clientOrderId;
}
