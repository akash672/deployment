package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.Map;

import com.wt.domain.EquityInstrument;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SymbolToInstrumentMapSent implements Serializable{

  private static final long serialVersionUID = 1L;
  private Map<String,EquityInstrument> symbolToInstruments;
  
  
  public SymbolToInstrumentMapSent(Map<String, EquityInstrument> symbolToInstruments) {
    super();
    this.symbolToInstruments = symbolToInstruments;
  }
}