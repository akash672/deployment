package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper=true)
public class OrderReceivedWithBroker extends ExchangeOrderUpdateEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private final String localOrderId;
  @Getter
  private final String brokerOrderId;
  
  public OrderReceivedWithBroker(DestinationAddress destinationAddress, String localOrderId, String brokerOrderId) {
    super(destinationAddress);
    this.localOrderId = localOrderId;
    this.brokerOrderId = brokerOrderId;
  }

}
