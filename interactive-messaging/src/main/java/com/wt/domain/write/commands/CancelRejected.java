package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CancelRejected extends OrderCommand {

  private static final long serialVersionUID = 1L;
  private String message;
  private String localOrderIdCancellation;
  private String brokerOrderIdCancellation;
  
}
