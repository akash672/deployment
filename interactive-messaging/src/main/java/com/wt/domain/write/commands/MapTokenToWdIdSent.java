package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.Map;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class MapTokenToWdIdSent  implements Serializable{
  
  private static final long serialVersionUID = 1L;
  private Map<String,String> tokenToWdId;
  public MapTokenToWdIdSent(Map<String, String> tokenToWdId) {
    super();
    this.tokenToWdId = tokenToWdId;
  }
  
  
  
}
