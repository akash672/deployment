package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
public class SubscribeAndGetLivePriceStream extends AssetClassCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  String wdId;
  @Setter private String token;
  
  public SubscribeAndGetLivePriceStream(String wdId) {
    super();
    this.wdId = wdId;
  }
  

}
