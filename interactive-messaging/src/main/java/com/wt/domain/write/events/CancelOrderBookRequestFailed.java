package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class CancelOrderBookRequestFailed extends CancelOrderEvent {

  private static final long serialVersionUID = 1L;

}
