package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@ToString
public class EquityInstrumentTokenUpdated implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter private String wdId;
  @Getter private String oldToken;
  @Getter private String newToken;
  @Getter private String symbol;
  @Getter private String exchange;
  
  public EquityInstrumentTokenUpdated(String wdId, String oldToken, String newToken, String symbol, String exchange) {
    super();
    this.wdId = wdId;
    this.oldToken = oldToken;
    this.newToken = newToken;
    this.symbol = symbol;
    this.exchange = exchange;
  }
}
