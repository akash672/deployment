package com.wt.domain.write.commands;


import java.io.Serializable;

import com.wt.domain.OrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class ShuttingDownActorAtEOD implements Serializable {
  
  private static final long serialVersionUID = 1L;
  private OrderActorStates orderState;
  private String clientCode;
  private String clientOrderId;

  public ShuttingDownActorAtEOD(OrderActorStates orderState,
      String clientCode, String clientOrderId) {
    this.orderState = orderState;
    this.clientCode = clientCode;
    this.clientOrderId = clientOrderId;
  }
  
  

}
