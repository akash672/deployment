package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class ProvidingTradeBook extends OrderEvent {

  private static final long serialVersionUID = 1L;

}
