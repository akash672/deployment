package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.ToString;

@ToString
public abstract class UserEvent extends EventWithTimeStamps implements Serializable {
  private static final long serialVersionUID = 1L;
  protected String email;
  private String username;

  public String getUsername() {
    return username != null ? username : email;
  }

  public UserEvent(String username) {
    this.username = username;
    this.email = username;
  }

  public UserEvent(String username, String email) {
    this.username = username;
    this.email = email;
  }
  
  @Deprecated
  public String getEmail() {
    return email;
  }
}
