package com.wt.domain.write.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class ReceivedRequestToProvideOrderBook extends OrderEvent {

  private static final long serialVersionUID = 1L;
  @Getter private String clientOrderId;

}
