package com.wt.domain.write.events;

import com.wt.domain.OrderActorStates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class OrderCancellationInProcess extends OrderEvent {

  private static final long serialVersionUID = 1L;
  private OrderActorStates orderActorStates;
  private String localOrderIdCancellation;
  
  public OrderCancellationInProcess(OrderActorStates orderActorStates, String localOrderIdCancellation){
    this.orderActorStates = orderActorStates;
    this.localOrderIdCancellation = localOrderIdCancellation;
  }
}
