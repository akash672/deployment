package com.wt.domain.write.events;

import com.wt.domain.DestinationAddress;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class TradeCancelled extends ExchangeOrderUpdateEvent{

  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  
  public TradeCancelled(DestinationAddress destinationAddress, String exchangeOrderId) {
    super(destinationAddress);
    this.exchangeOrderId = exchangeOrderId;
  }

}
