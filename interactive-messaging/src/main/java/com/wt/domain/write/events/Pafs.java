package com.wt.domain.write.events;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@DynamoDBDocument
@NoArgsConstructor
@EqualsAndHashCode
@ToString(exclude={"nonDividendPaf", "dividend", "historicalPaf"})
public class Pafs implements Serializable, Cloneable {
  private static final long serialVersionUID = 1L;

  @DynamoDBAttribute private double cumulativePaf = 0.;
  @DynamoDBAttribute private double nonDividendPaf = 0.;
  @DynamoDBAttribute private double dividend = 0.;
  @DynamoDBAttribute private double historicalPaf = 0.;
  
  @JsonIgnore
  public static Pafs withCumulativePaf(double historicalPaf) {
    return new Pafs(historicalPaf, 1., 0., historicalPaf);
  }

  @JsonCreator
  public Pafs(@JsonProperty("cumulativePaf") double cumulativePaf, @JsonProperty("nonDividendPaf")  double nonDividendPaf, 
      @JsonProperty("dividend") double dividend, @JsonProperty("historicalPaf") double historicalPaf) {
    super();
    this.cumulativePaf = cumulativePaf;
    this.nonDividendPaf = nonDividendPaf;
    this.dividend = dividend;
    this.historicalPaf = historicalPaf;
  }
  
  @Override
  public Pafs clone() {
    return new Pafs(cumulativePaf, nonDividendPaf, dividend, historicalPaf);
  }
}
