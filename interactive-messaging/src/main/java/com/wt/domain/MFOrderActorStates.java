package com.wt.domain;

import akka.persistence.fsm.PersistentFSM.FSMState;
import lombok.Getter;

public enum MFOrderActorStates implements FSMState {

  OrderBeingGenerated("OrderBeingGenerated"),
  OrderGenerated("OrderGenerated"),
  OrderReceivedbyDealer("OrderReceivedbyBroker"),
  OrderRejectedbyExchange("OrderRejectedbyExchange"),
  FullyExecuted("FullyExecuted"); 

  @Getter
  private final String stateIdentifier;

  private MFOrderActorStates(String stateIdentifier) {
    this.stateIdentifier = stateIdentifier;
  }

  @Override
  public String identifier() {
    return stateIdentifier;
  }
}