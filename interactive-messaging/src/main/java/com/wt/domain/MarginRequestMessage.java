package com.wt.domain;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class MarginRequestMessage extends MessageType {
  
  private static final long serialVersionUID = 1L;
  private double marginAmount;

  public MarginRequestMessage(String clientCode, double marginAmount) {
    super(clientCode);
    this.marginAmount = marginAmount;
  }
  

}
