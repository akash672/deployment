package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wt.domain.write.events.EquityInstrumentDescriptionUpdated;
import com.wt.domain.write.events.EquityInstrumentSegmentUpdated;
import com.wt.domain.write.events.EquityInstrumentTokenUpdated;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString(of = "wdId")
@DynamoDBTable(tableName = "Equity_Instrument")
@EqualsAndHashCode(callSuper=true, of="wdId")
public class EquityInstrument extends Instrument implements Serializable {
  private static final long serialVersionUID = 1L;

  @DynamoDBHashKey
  @Getter 
  @Setter
  private String wdId;
  @DynamoDBAttribute
  @Getter
  @Setter
  private String description;
  @DynamoDBAttribute
  @Getter
  @Setter
  private String symbol;
  @DynamoDBAttribute
  @Setter
  private String token;
  @Getter
  @Setter
  private String exchange;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String segment;

  public EquityInstrument(String wdId, String token, String symbol, String description, String segment) {
    this.wdId = wdId;
    this.token = token;
    this.symbol= symbol;
    this.description = description;
    this.segment = segment;
    
    String[] parts = wdId.split("-");
    exchange = parts[1];
    
  }
  
  public EquityInstrument(String wdId){
    this.wdId = wdId;
    String[] parts = wdId.split("-");
    exchange = parts[1];
  }
  
  
  public String getToken() {
    return token;
  }
  
  public void update(Object evt) {
    if (evt instanceof EquityInstrumentDescriptionUpdated) {
      EquityInstrumentDescriptionUpdated event = (EquityInstrumentDescriptionUpdated) evt;
      description = event.getDescription();
    }
    if (evt instanceof EquityInstrumentTokenUpdated) {
      EquityInstrumentTokenUpdated event = (EquityInstrumentTokenUpdated) evt;
      token = event.getNewToken();
    }
    if (evt instanceof EquityInstrumentSegmentUpdated) {
      EquityInstrumentSegmentUpdated event = (EquityInstrumentSegmentUpdated) evt;
      segment = event.getNewSegment();
    }
    
  }
}
