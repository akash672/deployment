package com.wt.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@EqualsAndHashCode
public abstract class MessageType implements Serializable {

  private static final long serialVersionUID = 1L;
  @Getter private String clientCode;

}
