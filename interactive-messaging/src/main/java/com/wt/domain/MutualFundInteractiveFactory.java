package com.wt.domain;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.wt.ctcl.facade.MutualFundInteractive;

@Component
public class MutualFundInteractiveFactory {

  @Resource(name="brokerTypeWithMFCTCLImplementation")
  private Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation;

  public MutualFundInteractive getCTCL(BrokerType brokerType) throws IOException {
    return brokerTypeWithMFCTCLImplementation.get(brokerType);
  }

}
