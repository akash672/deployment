package com.wt.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderSide {
  BUY(1), SELL(-1);
  private int value;
}
