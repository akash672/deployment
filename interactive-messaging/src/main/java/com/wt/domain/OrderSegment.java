package com.wt.domain;

public enum OrderSegment {

  Equity("Equity"),
  MutualFund("MutualFund"),
  FuturesOptions("FuturesOptions");
  
  private String id;
  
  OrderSegment(String id) {
    this.id = id;
  }

  public String getID() {
    return id;
  }
}
