package com.wt.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(callSuper=true)
@ToString
public class ExchangeOrderMessageType extends MessageType {

  private static final long serialVersionUID = 1L;
  private String brokerOrderId;
  private String exchOrderID;
  private long exchOrderTime;
  private String token;
  private int quantity;
  private OrderSide orderSide;
  
  public ExchangeOrderMessageType(String clientCode, String brokerOrderId, String exchOrderID, long exchOrderTime,
      String token, int quantity, OrderSide orderSide) {
    super(clientCode);
    this.brokerOrderId = brokerOrderId;
    this.exchOrderID = exchOrderID;
    this.exchOrderTime = exchOrderTime;
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
  }
  
  
}
