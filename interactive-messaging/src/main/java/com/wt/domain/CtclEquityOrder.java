package com.wt.domain;

import static com.wt.utils.NumberUtils.averagingWeighted;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;

import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.events.OrderCancellationInProcess;
import com.wt.domain.write.events.OrderEvent;
import com.wt.domain.write.events.OrderGenerationInProcess;
import com.wt.domain.write.events.OrderHasBeenSuccessfullySentToCTCL;
import com.wt.domain.write.events.OrderNotSentToBroker;
import com.wt.domain.write.events.OrderReceivedByBroker;
import com.wt.domain.write.events.OrderSentToExchange;
import com.wt.domain.write.events.OrderTraded;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class CtclEquityOrder extends CtclOrder implements Serializable {

  private static final long serialVersionUID = 1L;
  private OrderSpecification orderSpecification;
  private String clientCode;
  private String clientOrderId;
  private String localOrderId;
  private String localOrderIdCancellation;
  private String brokerOrderIdCancellation;
  private String brokerOrderId;
  private String exchangeOrderId;
  private OrderExecutionMetaData orderExecutionMetaData;
  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private int quantity;
  private int quantityTraded;
  private double limitPrice;
  private double triggerPrice;
  private OrderSide side;
  private OrderType orderType;
  private Set<Trade> trades = new HashSet<>();
  private List<String> tradeOrderIdsList = new ArrayList<String>();
  private DestinationAddress destinationAddress;
  private String exchange;
  private String symbol;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private boolean isCtclOrderSentSuccessfully = false;
  private BrokerAuthInformation brokerAuthInformation;

  public void update(OrderEvent event) {

    if (event instanceof OrderGenerationInProcess) {
      OrderGenerationInProcess orderGenerationInProcess = (OrderGenerationInProcess) event;
      this.clientCode = orderGenerationInProcess.getClientCode();
      this.symbol = orderGenerationInProcess.getOrderExecutionMetaData().getSymbol();
      this.orderExecutionMetaData = orderGenerationInProcess.getOrderExecutionMetaData();
      this.quantity = orderGenerationInProcess.getOrderExecutionMetaData().getIntegerQuantity();
      this.orderType = (orderGenerationInProcess.getOrderExecutionMetaData().getOrderType());
      this.side = orderGenerationInProcess.getOrderExecutionMetaData().getOrderSide();
      this.destinationAddress = orderGenerationInProcess.getDestinationAddress();
      this.localOrderId = orderGenerationInProcess.getLocalOrderId();
      this.clientOrderId = orderGenerationInProcess.getClientOrderId();
      this.orderSpecification = new OrderSpecification(clientCode, this.orderExecutionMetaData.getToken(), quantity,
          side);
      this.brokerName = orderGenerationInProcess.getBrokerName();
      this.exchange = orderGenerationInProcess.getExchange();
      this.purpose = orderGenerationInProcess.getPurpose();
      this.investingMode = orderGenerationInProcess.getInvestingMode();
      this.symbol = orderGenerationInProcess.getOrderExecutionMetaData().getSymbol();
      this.limitPrice = orderGenerationInProcess.getOrderExecutionMetaData().getLimitPrice();
    } else if (event instanceof OrderHasBeenSuccessfullySentToCTCL) {
      this.isCtclOrderSentSuccessfully = true;
    } else if (event instanceof OrderReceivedByBroker) {
      OrderReceivedByBroker orderReceivedBroker = (OrderReceivedByBroker) event;
      this.brokerOrderId = orderReceivedBroker.getBrokerOrderId();
    } else if (event instanceof OrderSentToExchange) {
      OrderSentToExchange orderPlacedExchange = (OrderSentToExchange) event;
      this.exchangeOrderId = orderPlacedExchange.getExchangeOrderId();
      this.brokerOrderId = orderPlacedExchange.getBrokerOrderId();
    } else if (event instanceof OrderTraded) {
      OrderTraded order = (OrderTraded) event;
      tradeOrderIdsList.add(order.getTradeId());
      this.exchangeOrderId = order.getExchangeOrderId();
      this.brokerOrderId = order.getBrokerOrderId();
      quantityTraded += order.getQty();
      trades.add(new Trade(order.getPrice(), side, order.getQty(), order.getExchangeOrderId(), order.getTradeId()));
    } else if (event instanceof OrderCancellationInProcess) {
      OrderCancellationInProcess orderCancellationInProcess = (OrderCancellationInProcess) event;
      this.localOrderIdCancellation = orderCancellationInProcess.getLocalOrderIdCancellation();
    } else if (event instanceof OrderNotSentToBroker) {
      this.isCtclOrderSentSuccessfully = false;
    }

  }

  public int getQuantityRemaining() {
    return (quantity - quantityTraded);
  }

  public double getAverageTradedPrice() {
    return trades.stream().collect(averagingWeighted(Trade::getPrice, Trade::getIntegerQuantity));
  }

  @Override
  public String getFolioNumber() {
    return null;
  }

  @Override
  public double getQuantity() {
    return 0;
  }

  @Override
  public int getIntegerQuantity() {
    return quantity;
  }

  @Override
  public void setIntegerQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Override
  public String getTxCode() {
    return null;
  }

  @Override
  public String getOrderSideType() {
    return null;
  }

  @Override
  public String getSchemeCode() {
    return null;
  }

  @Override
  public double getAllocatedAmount() {
    return 0;
  }

  @Override
  public String getAllRedeem() {
    return null;
  }

  @Override
  public String getToken() {
    return null;
  }

  @Override
  public String getSchemeName() {
    return null;
  }

  @Override
  public String getSchemeType() {
    return null;
  }
}