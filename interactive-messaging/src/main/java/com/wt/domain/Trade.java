package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude={"orderSide","price","quantity"})
@ToString
@DynamoDBDocument
public class Trade implements Serializable {
  private static final long serialVersionUID = 1L;
  @DynamoDBAttribute
  private double price;
  @DynamoDBAttribute
  @DynamoDBTypeConvertedEnum
  private OrderSide orderSide;
  @DynamoDBAttribute
  private double quantity;
  @DynamoDBAttribute
  private String exchOrderId;
  @DynamoDBAttribute
  private String orderTradeId;
  @DynamoDBIgnore
  public int getIntegerQuantity() {
    return (int) quantity;
  }
}