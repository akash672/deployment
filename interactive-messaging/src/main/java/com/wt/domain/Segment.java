package com.wt.domain;

public enum Segment {
  NSE_EQ(0), BSE_EQ(1), NSE_FNO(2);
  private final int value;

  public static String getStringValue(int Segment) {
    switch (Segment) {
    case 0:
      return "NSE-EQ";
    case 1:
      return "BSE-EQ";
    case 2:
      return "NSE-FNO";
    }
    return null;
  }

  private Segment(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
