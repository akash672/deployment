package com.wt.domain;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class OrderSpecification {
  private String clientCode;
  private String token;
  private int quantity;
  private OrderSide orderSide;
  
  public OrderSpecification(String clientCode, String token, int quantity, OrderSide orderSide) {
    this.clientCode = clientCode;
    this.token = token;
    this.quantity = quantity;
    this.orderSide = orderSide;
  }
  
  public boolean equals(Object o){
    if(o instanceof OrderSpecification){
        OrderSpecification orderSpecification = (OrderSpecification) o ;
        return (getClientCode().equals(orderSpecification.getClientCode()) &&
                getToken().equals(orderSpecification.getToken())&&
                getQuantity()== orderSpecification.getQuantity() &&
                getOrderSide().equals(orderSpecification.getOrderSide())) ;
    } else{
      return false;
    }
    
  }

}
