package com.wt.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class BrokerType {
  private Purpose purpose;
  private InvestingMode investingMode;
  private BrokerName brokerName;
}
