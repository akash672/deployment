package com.wt.domain;

import java.io.Serializable;

import javax.persistence.Entity;

import com.wt.domain.write.commands.ReceivedByDealer;
import com.wt.domain.write.events.MutualFundOrderGenerationInProcess;
import com.wt.domain.write.events.MutualFundTradeExecuted;
import com.wt.domain.write.events.OrderEvent;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper=true)
public class CtclMFOrder extends CtclOrder implements Serializable {

  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String symbol;
  private OrderSide side;
  private String token;
  private String clientOrderId;
  private String localOrderId;
  private DestinationAddress destinationAddress;
  private BrokerName brokerName;
  private Purpose purpose;
  private InvestingMode investingMode;
  private double allocatedAmount;
  private String schemeName;
  private String schemeType;
  private String folioNumber;
  private double quantity;
  private double nav;
  private String schemeCode;
  private String exchangeOrderId;
  private boolean isOrderPlaced = false;
  private String OrderSideType;
  private String txCode;
  private String memberId;
  private String allRedeem;
  private String kycStatus;
  private String dpcFlag;
  private String euin;
  private char euinFlag;

  public void update(OrderEvent event) {

    if (event instanceof MutualFundOrderGenerationInProcess) {
      MutualFundOrderGenerationInProcess mutualFundOrderGenerationInProcess = (MutualFundOrderGenerationInProcess) event;
      this.clientCode = mutualFundOrderGenerationInProcess.getClientCode();
      this.symbol = mutualFundOrderGenerationInProcess.getSymbol();
      this.token = mutualFundOrderGenerationInProcess.getToken();
      this.side = mutualFundOrderGenerationInProcess.getSide();
      this.quantity = mutualFundOrderGenerationInProcess.getQuantity();
      this.clientOrderId = mutualFundOrderGenerationInProcess.getClientOrderId();
      this.localOrderId = mutualFundOrderGenerationInProcess.getLocalOrderId();
      this.destinationAddress = mutualFundOrderGenerationInProcess.getDestinationAddress();
      this.brokerName = mutualFundOrderGenerationInProcess.getBrokerName();
      this.purpose = mutualFundOrderGenerationInProcess.getPurpose();
      this.investingMode = mutualFundOrderGenerationInProcess.getInvestingMode();
      this.allocatedAmount = mutualFundOrderGenerationInProcess.getAllocatedAmount();
      this.schemeName = mutualFundOrderGenerationInProcess.getSchemeName();
      this.schemeCode = mutualFundOrderGenerationInProcess.getSchemeCode();
      this.schemeType = mutualFundOrderGenerationInProcess.getSchemeType();
      this.folioNumber = mutualFundOrderGenerationInProcess.getFolioNumber();
      this.OrderSideType ="Fresh";
      this.txCode =mutualFundOrderGenerationInProcess.getTxCode();
      this.allRedeem = mutualFundOrderGenerationInProcess.getAllRedeem();
    }

    if (event instanceof ReceivedByDealer) {
      ReceivedByDealer receivedByDealer = (ReceivedByDealer) event;
      this.exchangeOrderId = String.valueOf(receivedByDealer.getExchangeOrderId());
    }

    if (event instanceof MutualFundTradeExecuted) {
      MutualFundTradeExecuted mutualFundTradeExecuted = (MutualFundTradeExecuted) event;
      this.quantity = mutualFundTradeExecuted.getQuantity();
      this.nav = mutualFundTradeExecuted.getNav();
      this.exchangeOrderId = mutualFundTradeExecuted.getExchangeOrderId();
      isOrderPlaced = true;
    }

  }

  @Override
  public int getIntegerQuantity() {
    return 0;
  }

  @Override
  public void setIntegerQuantity(int quantity) {
    
  }
  
  @Override
  public OrderExecutionMetaData getOrderExecutionMetaData() {
    return null;
  }

  @Override
  public String getExchange() {
    return "";
  }

  @Override
  public double getLimitPrice() {
    return 0;
  }

  @Override
  public void setOrderExecutionMetaData(OrderExecutionMetaData orderInstrument) {
    
  }

}
