package com.wt.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(callSuper=true)
@ToString
public class ExchangeTradeMessageType extends MessageType {

  private static final long serialVersionUID = 1L;
  private String exchangeOrderId;
  
  public ExchangeTradeMessageType(String clientCode, String exchangeOrderId) {
    super(clientCode);
    this.exchangeOrderId = exchangeOrderId;
  }
  
  

}
