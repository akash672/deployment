package com.wt.mosl.write.commands;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class MOSLTradeUpdateStatus implements Serializable{

  private static final long serialVersionUID = 1L;
  @JsonProperty("Clientcode")
  private String clientcode;
  @JsonProperty("BatchSequenceId")
  private String batchSequenceId;
  @JsonProperty("TradeQty")
  private String tradeQty;
  @JsonProperty("TradePrice")
  private String tradePrice;
  @JsonProperty("ScripName")
  private String scripName;
  @JsonProperty("OrderStatus")
  private String orderStatus;
  @JsonProperty("UniqueOrderId")
  private String uniqueOrderId;
  @JsonProperty("OrderNumber")
  private String tradeOrderId;
  @JsonProperty("BuySell")
  private String orderSide;
  @JsonProperty("ExchangeCode")
  private String exchangeCode;
 
  private String token;
  
  public void setToken(String token){
    this.token = token;
  }
  
}
