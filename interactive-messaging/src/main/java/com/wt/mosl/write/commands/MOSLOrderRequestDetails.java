package com.wt.mosl.write.commands;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class MOSLOrderRequestDetails implements Serializable{
   
  private static final long serialVersionUID = 1L;
  private String clientCode;
  private String batchId;
  
  
  @JsonCreator
  public MOSLOrderRequestDetails(@JsonProperty("clientCode") String clientCode,
      @JsonProperty("clientCode") String batchId) {
    super();
    this.clientCode = clientCode;
    this.batchId = batchId;
  }

}
