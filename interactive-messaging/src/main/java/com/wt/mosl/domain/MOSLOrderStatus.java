package com.wt.mosl.domain;

import lombok.Getter;

public enum MOSLOrderStatus {
  Cancel("Cancel"), PENDING("PENDING"), Partial("Partial"), Confirm("Confirm"), Error("Error"), Rejected(
      "Rejected"), Traded("Traded");

  @Getter
  private final String orderStatus;

  private MOSLOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

}
