package com.wt.config.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import lombok.extern.log4j.Log4j;

@Service
@Log4j
public class BackUpEventStore {

  private String path;
  private String folderName;
  private String bucketName;
  private String s3Path;
  private boolean scheduleBackupProcess;
  private @Autowired AmazonS3 amazonS3Client;
  
  public BackUpEventStore(@Value("${scheduleBackupProcess}") boolean flag) {
    this.scheduleBackupProcess = flag;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public void setFolderName(String folderName) {
    this.folderName = folderName;
  }

  public void setBucketName(String bucketName) {
    this.bucketName = bucketName;
  }

  @Async
  @Scheduled(cron = "${backupprocess.schedule}", zone = "Asia/Kolkata")
  public void startBackup() {
    if(!scheduleBackupProcess) {
      return;
    }
    
    createFoldersInS3();
    if (folderName.contains("ctcl"))
      ctclBackUp();
    if (folderName.contains("wd"))
      wdBackUp();
    if (folderName.contains("universe"))
      universeBackUp();

  }

  public void createFoldersInS3() {
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
    String year = String.valueOf(calendar.get(Calendar.YEAR));
    List<String> keys = getObjectslistFromFolder(bucketName, year);
    if (!checkWheterFolderIsPresent(keys, year))
      createFolder(bucketName, year);
    String month = String.valueOf((calendar.get(Calendar.MONTH) + 1));
    if (!checkWheterFolderIsPresent(keys, month))
      createFolder(bucketName, year + "/" + month);
    String day = String.valueOf(calendar.get(Calendar.DATE));
    createFolder(bucketName, year + "/" + month + "/" + day);
    s3Path = year + "/" + month + "/" + day + "/";
    log.info("Files will saved in"+s3Path);
  }

  private boolean checkWheterFolderIsPresent(List<String> keys, String searchKey) {
    for (String key : keys) {
      if (key.contains(searchKey))
        return true;
    }
    return false;
  }

  public List<String> getObjectslistFromFolder(String bucketName, String folderKey) {

    ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName)
        .withPrefix(folderKey + "/");

    List<String> keys = new ArrayList<>();

    ObjectListing objects = amazonS3Client.listObjects(listObjectsRequest);
    for (;;) {
      List<S3ObjectSummary> summaries = objects.getObjectSummaries();
      if (summaries.size() < 1) {
        break;
      }
      summaries.forEach(s -> keys.add(s.getKey()));
      objects = amazonS3Client.listNextBatchOfObjects(objects);
    }

    return keys;
  }

  public void createFolder(String bucketName, String keyName) {
    ObjectMetadata metadata = new ObjectMetadata();
    metadata.setContentLength(0);
    InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, keyName + "/", emptyContent, metadata);
    amazonS3Client.putObject(putObjectRequest);
  }

  private void universeBackUp() {
    hotBackUp();
  }

  private void wdBackUp() {
    hotBackUp();
  }

  private void ctclBackUp() {

    try {
      killProcess();
      Process processCommand = Runtime.getRuntime()
          .exec("./ctcl_eventstore_script.sh " + path + " " + folderName + " " + s3Path);
      processCommand.waitFor();
      log.info("CTCL EventStore service has resumed");
      String backupFileName = folderName + ".zip";
      log.info("Backing up File" + backupFileName);
      putDataIns3(backupFileName);
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
      log.debug("some issue with execution of Shell script"+e);
    }
  }

  private void killProcess() throws IOException {
    String line;
    String pid = "";
    Process processCommand = Runtime.getRuntime().exec("ps -aef");
    BufferedReader allProcessInput = new BufferedReader(new InputStreamReader(processCommand.getInputStream()));
    while ((line = allProcessInput.readLine()) != null) {
      if (line.contains(folderName)) {
        for (int i = 0; i < line.length(); i++) {
          char c = line.charAt(i);
          if (Character.isDigit(c)) {
            String extractPid = line.substring(i);
            pid = extractPid.substring(0, extractPid.indexOf(" "));
            break;
          }
        }
        break;
      }
    }
    processCommand = Runtime.getRuntime().exec("kill -term " + pid);
    log.info("ctcl EventStore is down");
  }

  private void hotBackUp() {
    Process processCommand;
    try {
      processCommand = Runtime.getRuntime().exec("./hotbackup_eventstore_script.sh " + path + " " + folderName);
      processCommand.waitFor();
      String backupFileName = folderName + ".zip";
      log.info("Backing up File" + backupFileName);
      putDataIns3(backupFileName);
    } catch (IOException |InterruptedException e) {
      e.printStackTrace();
      log.debug("some issue with execution of Shell script"+e);
    } 
  }

  public void putDataIns3(String fileName) {
    try {
    File file = new File(path + "/" + fileName);
    s3Path += fileName;
    amazonS3Client.putObject(new PutObjectRequest(bucketName, s3Path, file));
    log.info("File has been saved to Bucket name "+bucketName+" folder is saved in "+s3Path);
    }
    catch (Exception e) {
      log.debug("Some Exception while saving the file to s3"+e);
    }
  }
}
