package com.wt.config.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.wt.utils.CommonUtilsConfiguration;

@Component
public class BackUpMonitor {
  
  private BackUpEventStore backUpEventStore;
  private static AnnotationConfigApplicationContext ctx;

  
  @Autowired
  public BackUpMonitor(BackUpEventStore backUpEventStore)
  {
    this.backUpEventStore = backUpEventStore;
  }
  
  public static void main(String args[])
  {
    ctx = new AnnotationConfigApplicationContext(CommonUtilsConfiguration.class);
    BackUpMonitor backUpMonitor = ctx.getBean(BackUpMonitor.class);
    backUpMonitor.start(args);
  }

  private void start(String[] args) {
    backUpEventStore.setPath(args[0]);
    backUpEventStore.setFolderName(args[1]);
    backUpEventStore.setBucketName(args[2]);
    while(true);
    
  }

}
