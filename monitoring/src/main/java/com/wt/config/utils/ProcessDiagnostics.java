package com.wt.config.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wt.utils.aws.SESEmailUtil;

@Service("ProcessDiagnostics")
public class ProcessDiagnostics {

  private @Autowired SESEmailUtil emailerService;
  private @Autowired WDProperties properties;
  private String serviceName[];
  private boolean scheduleMonitoringProcess;
  
  public ProcessDiagnostics(@Value("${scheduleMonitoringProcess}") boolean flag) {
    this.scheduleMonitoringProcess = flag;
  }

  public void setServiceName(String[] services) {
    serviceName = services;
  }

  @Async
  @Scheduled(cron = "${processCheck.schedule}", zone = "Asia/Kolkata")
  public void runningDiagnostics() {
    String emailerBody = "";
    if(!scheduleMonitoringProcess) {
      return;
    }
    for (int i = 0; i < serviceName.length; i++)
      if (!isRunning(serviceName[i]))
        emailerBody += serviceName[i] + " service is Down." + '\n';

    if (!emailerBody.isEmpty()) {
      String emailBody = "Please check\n" + emailerBody;
      try {
        sendStatusEmailToDevTeam(emailBody);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  public boolean isRunning(String processName) {
    String line;
    boolean checkService = false;
    Process processCommand;
    try {
      processCommand = Runtime.getRuntime().exec("ps -aef");
      BufferedReader allProcessInput = new BufferedReader(new InputStreamReader(processCommand.getInputStream()));
      while ((line = allProcessInput.readLine()) != null) {
        if (line.contains("ServiceMonitor"))
          continue;
        if (line.contains(processName)) {
          checkService = true;
          break;
        }
      }
      allProcessInput = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return checkService;
  }

  private void sendStatusEmailToDevTeam(String emailBody) throws IOException {

    emailerService.sendEmail(properties.OUTBOUND_TRANSACTIONAL_EMAIL(), properties.DEV_TEAM_EMAILIDS(), "Process Monitoring ALERT", emailBody
        + '\n' + System.getProperty("os.name") + "-" + properties.environment() + "-" + System.getenv("HOSTNAME"));
  }

}
