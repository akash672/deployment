package com.wt.config.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.wt.utils.CommonUtilsConfiguration;

@Component
public class ServiceMonitor {
  
  private static AnnotationConfigApplicationContext ctx;
  private  ProcessDiagnostics process;
  
  public static void main(String[] args) {
    ctx = new AnnotationConfigApplicationContext(CommonUtilsConfiguration.class);
    ServiceMonitor serviceMonitor = ctx.getBean(ServiceMonitor.class);
     serviceMonitor.start(args);
  }
  

  private void start(String[] serviceName) {
    process.setServiceName(serviceName);
    while(true);
  }

  @Autowired
  public ServiceMonitor(ProcessDiagnostics process)
  {
    this.process = process;
  }

}
