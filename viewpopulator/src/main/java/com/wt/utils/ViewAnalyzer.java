package com.wt.utils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import com.wt.api.CommonHandlerRetrier;
import com.wt.domain.UserFund;
import com.wt.domain.repo.SequenceStoreRepository;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundMaxDrawdownCalculated;
import com.wt.domain.write.events.UserFundPerformanceCalculated;

import akka.actor.ActorSystem;
import akka.persistence.fsm.PersistentFSM.StateChangeEvent;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;

@Service("ViewAnalyzer")
public class ViewAnalyzer {

  @SuppressWarnings("unused")
  private CommonHandlerRetrier handler;
  @SuppressWarnings("unused")
  private SequenceStoreRepository seqStoreRepository;
  private JournalProvider journalProvider;

  @Autowired
  public ViewAnalyzer(CommonHandlerRetrier handler, SequenceStoreRepository seqStoreRepository,
      JournalProvider journalProvider) {
    super();
    this.handler = handler;
    this.seqStoreRepository = seqStoreRepository;
    this.journalProvider = journalProvider;
  }

  @SuppressWarnings("resource")
  public static void main(String[] args) throws Exception {
    AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ViewPopulatorAppConfiguration.class);
    final ActorSystem system = (ActorSystem) ctx.getBean("actorSystem");
    ViewAnalyzer viewPopulater = (ViewAnalyzer) ctx.getBean("ViewAnalyzer");
    ActorMaterializer materializer = ActorMaterializer.create(system);

    viewPopulater.populateCurrentView(system, materializer);
  }

  private void populateCurrentView(ActorSystem system, Materializer materializer) throws Exception {

    List<String> allIds = ImmutableList.of("nimit_nimit@yahoo.co.in__TechnoFunda__VIRTUAL__hansrajmodi");
    handleAllEvents(system, materializer, allIds);
  }

  protected void handleAllEvents(ActorSystem system, Materializer materializer, List<String> allIds) throws Exception {

      UserFund userFund = new UserFund();
    for (String id : allIds) {      
        journalProvider.allEventsSourceForPersistenceId(id)
        .filterNot(event -> event.event() instanceof StateChangeEvent || event.event() instanceof UserFundPerformanceCalculated || event.event() instanceof UserFundMaxDrawdownCalculated)
        .map(event -> (UserFundEvent) event.event())
        .runForeach(userFundEvent -> {
            userFund.update(userFundEvent);
        }, materializer);
    }

  }

}