package com.wt.utils;

import static java.lang.Runtime.getRuntime;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import eventstore.ProjectionDetails;
import eventstore.ProjectionsClient;
import eventstore.ProjectionsClient.ProjectionMode;
import lombok.extern.log4j.Log4j;
import scala.Option;
import scala.concurrent.Future;

@Component
@Log4j
public class ProjectionCreator {

  private ProjectionsClient projectionClient;
  private final static char extensionPoint = '.';
  private final static char lineDifrrentiator = '\n';

  @Autowired
  public ProjectionCreator(@Qualifier("WDProjections") ProjectionsClient projectionClient) {
    this.projectionClient = projectionClient;
  }

  public static void main(String args[]) throws Exception {
    AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
        ViewPopulatorAppConfiguration.class);
    ProjectionCreator projectionCreator = ctx.getBean(ProjectionCreator.class);
    if(args[0].contains("eventstore/"))
       projectionCreator.getNameOfProjections();
    else
      projectionCreator.startCreatingProjection(args);
    getRuntime().addShutdownHook(new Thread(() -> {
      ctx.close();
    }));
  }

  private void getNameOfProjections() throws Exception {
    List<String> projectionNamesList = new ArrayList<String>();
    InputStream in = ClassLoader.getSystemResourceAsStream("eventstore/");
    InputStreamReader inReader = new InputStreamReader(in);
    Scanner scan = new Scanner(inReader);
    while (scan.hasNext()) {
      String projectionName = scan.next();
      projectionNamesList.add(projectionName);
    }
    scan.close();
    String[] projectionNames = new String[projectionNamesList.size()];
    for(int i = 0; i < projectionNamesList.size(); i++)
      projectionNames[i] = projectionNamesList.get(i);
    startCreatingProjection(projectionNames);
  }

  public void startCreatingProjection(String[] projectionNames) throws Exception {
    
      for(int i = 0;i < projectionNames.length; i++ ) {
      String projectionName = projectionNames[i];
      log.debug("Creating Projection for " + projectionName);
      InputStream inputStream = ClassLoader.getSystemResourceAsStream("eventstore/" + projectionName);
      InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
      BufferedReader reader = new BufferedReader(inputStreamReader);
      String eachProjectionCodeLine;
      String projectionCode = "";
      while ((eachProjectionCodeLine = reader.readLine()) != null) {
        projectionCode += eachProjectionCodeLine + lineDifrrentiator;
      }
      int endIndex = projectionName.indexOf(extensionPoint);
      createProjection(projectionName.substring(0, endIndex), projectionCode);
      log.debug("projection done for " + projectionName.substring(0, endIndex));
      reader.close();
    }
      System.exit(0);
  }

  public void createProjection(String projectionName, String projectionCode) throws InterruptedException {
    Future<Option<ProjectionDetails>> fetchProjectionDetails = projectionClient.fetchProjectionDetails(projectionName);
    Thread.sleep(2000);
    if(!fetchProjectionDetails.toString().contains("None")) {
      log.info(projectionName + "already exsist");
      return;
    }
    ProjectionMode projectionMode = projectionClient.createProjection$default$3();
    projectionClient.createProjection(projectionName, projectionCode, projectionMode, true);
    Thread.sleep(2000);
  }
}
