package com.wt.utils.db;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.wt.utils.CommonAppConfiguration;

public class SchemaDropper {
  public static void main(String[] args) throws Exception {
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CommonAppConfiguration.class)) {
      DropSchema dropSchema = ctx.getBean(DropSchema.class);
      dropSchema.dropDB();
    }
  }
}
