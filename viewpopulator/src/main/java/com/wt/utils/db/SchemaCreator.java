package com.wt.utils.db;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.wt.config.utils.ExecuteSqlSchema;
import com.wt.utils.CoreAppConfiguration;

/**
 * Class used to create tables in DynamoDB
 */
public class SchemaCreator {
  public static void main(String[] args) throws Exception {
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CoreAppConfiguration.class)) {
      ExecuteSqlSchema executeSqlSchemaBean = ctx.getBean(ExecuteSqlSchema.class);
      executeSqlSchemaBean.setupDB();
    }
  }
}
