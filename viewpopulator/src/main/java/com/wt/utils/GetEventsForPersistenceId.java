package com.wt.utils;

import java.io.Serializable;

public class GetEventsForPersistenceId implements Serializable {

  private static final long serialVersionUID = 1L;
  String persistenceId;

  public GetEventsForPersistenceId(String persistenceId) {
    this.persistenceId = persistenceId;
  }

  public String getPersistenceId() {
    return persistenceId;
  }

}
