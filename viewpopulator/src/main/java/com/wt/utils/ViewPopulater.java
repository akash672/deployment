package com.wt.utils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.transactions.Transaction;
import com.amazonaws.services.dynamodbv2.transactions.TransactionManager;
import com.wt.api.CommonHandlerRetrier;
import com.wt.config.utils.ExecuteSqlSchema;
import com.wt.domain.LastViewPopulatedSequenceNumber;
import com.wt.domain.repo.SequenceStoreRepository;

import akka.Done;
import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Source;
import akka.stream.scaladsl.Sink;

@Service("ViewPopulator")
public class ViewPopulater {

  private CommonHandlerRetrier handler;
  private SequenceStoreRepository seqStoreRepository;
  private JournalProvider journalProvider;

  @Autowired
  public ViewPopulater(CommonHandlerRetrier handler, SequenceStoreRepository seqStoreRepository,
      JournalProvider journalProvider, ExecuteSqlSchema schema) {
    super();
    this.handler = handler;
    this.seqStoreRepository = seqStoreRepository;
    this.journalProvider = journalProvider;
  }

  @SuppressWarnings("unused")
  public static void main(String[] args) throws Exception {
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
        ViewPopulatorAppConfiguration.class)) {
      final ActorSystem system = (ActorSystem) ctx.getBean("actorSystem");

      ViewPopulater viewPopulater = (ViewPopulater) ctx.getBean("ViewPopulator");
      viewPopulater.populateViewsToLocalDynamoDb(system, viewPopulater);
      @SuppressWarnings("rawtypes")
      Map<String, CrudRepository> repositories = ctx.getBeansOfType(CrudRepository.class);

      // viewPopulater.populateFinalViewToRealDynamoDb(repositories,
      // viewPopulater);
    }
  }

  private void populateViewsToLocalDynamoDb(final ActorSystem system, ViewPopulater viewPopulater) throws Exception {
    ActorMaterializer materializer = ActorMaterializer.create(system);
    //schema.setupDB();
    
    viewPopulater.blockingPopulateView(system, materializer);
  }

  @SuppressWarnings("unused")
  private void populateFinalViewToRealDynamoDb(@SuppressWarnings("rawtypes") Map<String, CrudRepository> repositories,
      ViewPopulater viewPopulater) {
    try (AnnotationConfigApplicationContext realContext = new AnnotationConfigApplicationContext(
        ViewPopulatorAppConfiguration.class)) {
      for (@SuppressWarnings("rawtypes")
      CrudRepository localRepos : repositories.values()) {
        TransactionManager txManager = new TransactionManager(realContext.getBean(AmazonDynamoDB.class), "Transactions",
            "TransactionImages");
        for (Object entity : localRepos.findAll()) {
          Transaction transaction = txManager.newTransaction();
          transaction.save(entity);
          transaction.commit();
        }
      }
    }
  }

  public void blockingPopulateView(ActorSystem system, Materializer materializer) throws Exception {
    Thread.sleep(2 * 1000);
    populateCurrentView(system, materializer);
  }

  public void clearCacheToCheckRecovery() {
    handler.clearAllCache();
  }

  private void populateCurrentView(ActorSystem system, Materializer materializer) throws Exception {
    List<String> allIds = journalProvider.currentPersistenceIds(materializer, id -> validatePersistenceId(id));
    for (String id : allIds) {
      CompletionStage<Done> completionStageForId = journalProvider.runForEachEvent(id, startSeqNumber(id),
          event -> handler.handle(event));
      completionStageForId.toCompletableFuture().get();
    }
  }

  private boolean validatePersistenceId(String id) {
    if(id.equals("active-instrument-tracker"))
      return false;
    if((!(id.endsWith("-EQ") || id.endsWith("-MF") ) && (!id.isEmpty())))
      return true;
    return false;
  }

  private long startSeqNumber(String id) {
    LastViewPopulatedSequenceNumber maybeSeqNumber = seqStoreRepository.findById(id).orElse(null);
    if (maybeSeqNumber == null)
      return 0L;

    return maybeSeqNumber.getSeqNum() + 1;
  }
  public void startPopulatingView(ActorRef viewPopulatorRoot, Materializer materializer) throws Exception {
    Source<String, NotUsed> filteredPersistenceIds = journalProvider.
        allPersistenceIds(id -> (!(id.contains("projection")) && (!id.isEmpty()) && !id.endsWith("-snapshots")));
      
        filteredPersistenceIds
        .map(id -> new GetEventsForPersistenceId(id))
        .runWith(Sink.actorRef(viewPopulatorRoot, "Done"), materializer);
  }

  public void perf() {

  }
}