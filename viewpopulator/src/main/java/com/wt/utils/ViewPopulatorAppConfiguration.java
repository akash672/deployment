package com.wt.utils;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:environment.properties")
public class ViewPopulatorAppConfiguration extends CoreAppConfiguration {
  
  @Autowired
  public ViewPopulatorAppConfiguration(ApplicationContext applicationContext) {
    super(applicationContext);
  }
  
}