package com.wt.api.write;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.Runtime.getRuntime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wt.cloud.monitoring.ServiceStatusReporter;
import com.wt.config.utils.WDProperties;
import com.wt.domain.write.commands.UpdateInstrumentsMap;
import com.wt.utils.ViewPopulater;
import com.wt.utils.ViewPopulatorAppConfiguration;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class ViewPopulatorService {
  private static AnnotationConfigApplicationContext ctx;

  private ActorSystem system;
  private ViewPopulater viewPopulater;
  private WDProperties properties;
  private ActorRef viewPopulatorRoot;

  private ServiceStatusReporter serviceStatusReporter;

  @Autowired
  public ViewPopulatorService(ActorSystem system, ViewPopulater viewPopulater, WDProperties properties,
      ServiceStatusReporter serviceStatusReporter) throws Exception {
    super();
    this.system = system;
    this.viewPopulater = viewPopulater;
    this.properties = properties;
    this.serviceStatusReporter = serviceStatusReporter;
  }

  public static void main(String[] args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(ViewPopulatorAppConfiguration.class);
    ViewPopulatorService service = ctx.getBean(ViewPopulatorService.class);
    service.start();
  }

  private void start() throws Exception {
    log.info("=======================================");
    log.info("| Starting ViewPopulator Service       |");
    log.info("=======================================");

    Materializer materializer = ActorMaterializer.create(system);
    viewPopulatorRoot = system.actorOf(SpringExtProvider.get(system).props("ViewPopulatorRoot"),
        "view-populator-root");
    viewPopulatorRoot.tell(new UpdateInstrumentsMap(), ActorRef.noSender());
    viewPopulater.startPopulatingView(viewPopulatorRoot, materializer);

    log.info("View Populator service started.");

    getRuntime().addShutdownHook(new Thread(() -> {
      ctx.close();
    }));
  }
  
  @Scheduled(cron = "${service.heartbeat.schedule}", zone = "Asia/Kolkata")
  public void sendServiceStatusHeartBeats() {
    if (properties.shallMonitorService())
      serviceStatusReporter.sendStatusUpdate(this.getClass().getSimpleName());
  }
}
