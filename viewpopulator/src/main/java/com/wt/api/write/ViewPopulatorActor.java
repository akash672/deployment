package com.wt.api.write;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.io.IOException;
import java.text.ParseException;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.wt.api.CommonHandler;
import com.wt.aws.cognito.exception.InternalErrorException;
import com.wt.domain.LastViewPopulatedSequenceNumber;
import com.wt.domain.repo.SequenceStoreRepository;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.UserFundAdditionalSumAdvice;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.utils.GetEventsForPersistenceId;
import com.wt.utils.JournalProvider;
import com.wt.utils.RetryingFailedException;

import akka.NotUsed;
import akka.actor.UntypedAbstractActor;
import akka.persistence.query.EventEnvelope;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Service("ViewPopulatorActor")
@Scope("prototype")
@Log4j
public class ViewPopulatorActor extends UntypedAbstractActor {

  private String persistenceId;
  private @Autowired CommonHandler handler;
  private @Autowired JournalProvider journalProvider;
  private @Autowired ActorMaterializer materializer;
  private @Autowired SequenceStoreRepository seqStoreRepository;


  public ViewPopulatorActor(String persistencId) {
    this.persistenceId = persistencId;
  }

  @Override
  public void postRestart(Throwable reason) throws Exception {
    log.debug("Restarting ViewPopulatorActor with PersistenceId " + persistenceId, reason);
    super.postRestart(reason);
  }
  
  @Override
  public void postStop() throws Exception {
    log.debug("Stopped ViewPopulatorActor with PersistenceId " + persistenceId);
  }
  
  private long startSeqNumber(String id) {
    Optional<LastViewPopulatedSequenceNumber> maybeSeqNumber = seqStoreRepository.findById(id);
    if (!maybeSeqNumber.isPresent())
      return 0L;

    long seqNumber = 0L;
    try {
      seqNumber = maybeSeqNumber.get().getSeqNum();
    } catch (NoSuchElementException ne) {
      log.error("Got NoSuchElementException while retrieving start sequence number for stream id " + id, ne);
    }
    return seqNumber;
  }


  @Override
  public void onReceive(Object cmd) throws Exception {

    if (cmd instanceof GetEventsForPersistenceId) {
      String streamId = ((GetEventsForPersistenceId) cmd).getPersistenceId();
      long seqNumber = startSeqNumber(streamId);
      log.debug("Last seen sequence number for streamId " + streamId + " is " + seqNumber);
      Source<EventEnvelope, NotUsed> allEventsOfId = journalProvider.allEventsSourceForPersistenceId(streamId,
          seqNumber, Integer.MAX_VALUE);

      allEventsOfId.runWith(Sink.actorRef(self(), "Done"), materializer);
    } else if (cmd instanceof EventEnvelope) {
      int retryCounter = 1;
      EventEnvelope eventEnvelope = (EventEnvelope) cmd;
      eventEnvelope = updateWithSymbol(eventEnvelope);
      for (int tryCount = 1; tryCount <= 5; tryCount++) {
        try {
          handler.tryToHandle(eventEnvelope);
          break;
        } catch (IOException | AmazonServiceException | IllegalArgumentException | InternalErrorException
            | ParseException e) {
          log.debug("Retrying failed event with persistence Id: " + eventEnvelope.persistenceId() + ", event number : " + 
               eventEnvelope.sequenceNr() + ", event type : " + eventEnvelope.event().getClass() + ", Retry Count: " + retryCounter, e);
          retryCounter++;
        } catch (Exception e) {
          log.debug("Failed handling event with persistence Id: " + eventEnvelope.persistenceId() + ", event number : " + 
              eventEnvelope.sequenceNr() + ", event type : " + eventEnvelope.event().getClass() , e);
          
          throw e;
        }
      }
      if (retryCounter >= 5) {
        throw new RetryingFailedException(eventEnvelope.persistenceId());
      }
    }
  }
  
  private EventEnvelope updateWithSymbol(EventEnvelope eventEnvelope) {
    Object event = eventEnvelope.event();
    if(event instanceof UserFundAdviceIssuedWithUserEmail){
      UserFundAdviceIssuedWithUserEmail userFundAdviseIssued = (UserFundAdviceIssuedWithUserEmail) event;
      String wdId = userFundAdviseIssued.getToken();
      String symbol = getSymbolFor(wdId);
      userFundAdviseIssued.setSymbol(symbol);
    } else if(event instanceof UserFundAdditionalSumAdvice){
      UserFundAdditionalSumAdvice userFundAdviseIssued = (UserFundAdditionalSumAdvice) event;
      String wdId = userFundAdviseIssued.getToken();
      String symbol = getSymbolFor(wdId);
      userFundAdviseIssued.setSymbol(symbol);
    } else if(event instanceof FundAdviceIssued){
      FundAdviceIssued fundAdviseIssued = (FundAdviceIssued) event;
      String wdId = fundAdviseIssued.getToken();
      String symbol = getSymbolFor(wdId);
      fundAdviseIssued.setSymbol(symbol);
    }
    return eventEnvelope;
  }
  
  private String getSymbolFor(String ticker) {
    try {
      Object result = askAndWaitForResult(
          context().parent(), new GetSymbolForWdId(ticker));
      if (result instanceof Failed) {
        log.error("Error while getting symbol for wdId " + ticker);
        return ticker;
      }
      
      SymbolSentForWdId symbolReceived = (SymbolSentForWdId) result;
      return symbolReceived.getSymbol();
    } catch (Exception e) {
      log.error("UserActor: failed to get symbol from UserRootActor for ticker " + ticker);
      return ticker;
    }
  }

}
