package com.wt.api.write;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static java.util.concurrent.TimeUnit.SECONDS;
import static scala.concurrent.duration.Duration.create;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.EquityInstrument;
import com.wt.domain.write.ParentActor;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.GetTokenToWdIdMap;
import com.wt.domain.write.commands.GetWdIdToInstrumentMap;
import com.wt.domain.write.commands.MapTokenToWdIdSent;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.commands.UpdateInstrumentsMap;
import com.wt.domain.write.commands.WdIdToInstrumentMapSent;
import com.wt.domain.write.events.Failed;
import com.wt.utils.CommonConstants;
import com.wt.utils.GetEventsForPersistenceId;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.OneForOneStrategy;
import akka.actor.SupervisorStrategy;
import akka.actor.SupervisorStrategy.Directive;
import akka.actor.Terminated;
import akka.actor.UntypedAbstractActor;
import akka.japi.Function;

@Scope("prototype")
@Service("ViewPopulatorRoot")
public class ViewPopulatorRoot extends UntypedAbstractActor implements ParentActor<ViewPopulatorActor> {
  
  private static final int MAX_ACTOR_RESTART_COUNTER = 5;

  private Map<String, EquityInstrument> wdIdToEquityInstrumentMap = new HashMap<>();
  private Map<String, String> tokenToWdIdMap = new HashMap<>();
  private WDActorSelections actorSelections;
  private final Map<String, Integer> actorRetryCounterMap = new HashMap<>();
  

  @Autowired
  public void setActorSelections(WDActorSelections actorSelections) {
    this.actorSelections = actorSelections;
  }
  
  @Override
  public void onReceive(Object cmd) throws Throwable {
    if (cmd instanceof GetEventsForPersistenceId) {
      String children = ((GetEventsForPersistenceId) cmd).getPersistenceId();
      ActorRef childActor = findChild(context(), children);
      getContext().watch(childActor);
      childActor.tell(cmd, sender());
    } else if(cmd instanceof UpdateInstrumentsMap){
      updateWdIdtoInstrumentsMap();
      updateTokenToWdIdMap();
    } else if(cmd instanceof GetSymbolForWdId){
      sendSymbolForWdId(cmd);
    }
    else if(cmd instanceof Terminated){
      Terminated terminated = ((Terminated) cmd);
      ActorRef childActor = terminated.actor();
      String childName = childActor.path().name();
      int parentNameIndex = childName.indexOf("-");
      String persistenceId = childName.substring(parentNameIndex+1);
      int counter = 1;
      if (actorRetryCounterMap.containsKey(persistenceId)) {
        counter = actorRetryCounterMap.get(persistenceId) + 1;
      }
      
      if (counter <= MAX_ACTOR_RESTART_COUNTER) {
        self().tell(new GetEventsForPersistenceId(persistenceId), Actor.noSender());
        actorRetryCounterMap.put(persistenceId, counter);
      }
    }
  }

  private void sendSymbolForWdId(Object cmd) {
    GetSymbolForWdId getSymbolForWdId = (GetSymbolForWdId) cmd;
    String isin = getSymbolForWdId.getWdId().split("-")[0];
    if (!wdIdToEquityInstrumentMap.containsKey(getSymbolForWdId.getWdId())){
      if(!isin.matches("[0-9]+")){
        updateWdIdtoInstrumentsMap();
        sender().tell(new SymbolSentForWdId(getSymbolForWdId.getWdId(), getSymbolForWdId.getWdId()), self());
        return;
      } else{
        getWdIdForTokenAndSendSymbol(getSymbolForWdId.getWdId());
        return;
      }
    }if (wdIdToEquityInstrumentMap.containsKey(getSymbolForWdId.getWdId())) {
      String symbol = wdIdToEquityInstrumentMap.get(getSymbolForWdId.getWdId()).getSymbol();
      sender().tell(new SymbolSentForWdId(getSymbolForWdId.getWdId(), symbol), self());
      return;
    } 
  }

  private void getWdIdForTokenAndSendSymbol(String wdId) {
    String token = wdId.split("-")[0];
    String exchange = wdId.split("-")[1];
    String id = tokenToWdIdMap.get(token + CommonConstants.HYPHEN + exchange);
    if(id!=null && wdIdToEquityInstrumentMap.containsKey(id)){
      String symbol = wdIdToEquityInstrumentMap.get(id).getSymbol();
      sender().tell(new SymbolSentForWdId(wdId, symbol), self());
    }else{
      sender().tell(new SymbolSentForWdId(wdId, wdId), self());
    }
  }


  private SupervisorStrategy strategy = new OneForOneStrategy(5, create(120, SECONDS),
      new Function<Throwable, Directive>() {
        @Override
        public Directive apply(Throwable thrown) {
          return SupervisorStrategy.stop();
        }
      });

  private void updateWdIdtoInstrumentsMap() {

    WdIdToInstrumentMapSent wdIdToInstrumentMapSent = null;
    boolean instrumentsMapReceived = false;
    int retryCounter = 1;
    while (!instrumentsMapReceived && retryCounter <= 5) {
      try {
        Object result = askAndWaitForResult(actorSelections.universeRootActor(), new GetWdIdToInstrumentMap());

        if (result instanceof Failed) {
          retryCounter++;
          log.warn("ViewPopulatorRoot :Failed to get instruments by wdId from EquityActor. Retrying " + result);
          continue;
        }
        wdIdToInstrumentMapSent = (WdIdToInstrumentMapSent) result;
        wdIdToEquityInstrumentMap = wdIdToInstrumentMapSent.getWdIdtoInstruments();
        instrumentsMapReceived = true;
        log.info("ViewPopulatorRoot : wdIdToInstrument Map updated");
      } catch (Exception e) {
        log.warn("ViewPopulatorRoot :Exception while getting instruments by wdId from EquityActor. Retrying ", e);
        retryCounter++;
      }
    }
    if(retryCounter >5 )
      log.error("ViewPopulatorRoot : Failed to get instruments map for universe actor after multiple attempts.");
  }
  
  private void updateTokenToWdIdMap() {
    MapTokenToWdIdSent mapTokenToWdIdSent = null;
    boolean mapReceived = false;
    int retryCounter = 1;
    while (!mapReceived && retryCounter <= 5) {
      try {
        Object result = askAndWaitForResult(actorSelections.universeRootActor(), new GetTokenToWdIdMap());

        if (result instanceof Failed) {
          retryCounter++;
          log.warn("ViewPopulatorRoot :Failed to get tokens to WdId map from EquityActor. Retrying " + result);
          continue;
        }
        mapTokenToWdIdSent = (MapTokenToWdIdSent) result;
        tokenToWdIdMap = mapTokenToWdIdSent.getTokenToWdId();
        mapReceived = true;
        log.info("ViewPopulatorRoot : tokenToWdIdMap Map updated");
      } catch (Exception e) {
        log.warn("ViewPopulatorRoot :Exception while getting token to WdId map from EquityActor. Retrying ", e);
        retryCounter++;
      }
    }
    if(retryCounter >5 )
      log.error("ViewPopulatorRoot : Failed to get token to WdId map for universe actor after multiple attempts.");
  }
  
  @Override
  public SupervisorStrategy supervisorStrategy() {
    return strategy;
  }

  @Override
  public String getName() {
    return "ViewPopulatorRoot";
  }

  @Override
  public String getChildBeanName() {
    return "ViewPopulatorActor";
  }

}
