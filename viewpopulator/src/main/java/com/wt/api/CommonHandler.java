package com.wt.api;

import static com.wt.domain.UserAdviseState.AdditionalLumpSumToMF;
import static com.wt.domain.UserAdviseState.AdviseTokenPriceReceived;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlaced;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlacedWithBroker;
import static com.wt.domain.UserAdviseState.CorporateActionOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderPlacedWithExchange;
import static com.wt.domain.UserAdviseState.OpenAdviseOrderReceivedWithBroker;
import static com.wt.domain.UserAdviseState.UserMFAdviseIssued;
import static com.wt.domain.UserAdviseState.UserMFOpenAdviseOrderExecuted;
import static com.wt.domain.UserAdviseState.UserMFOpenAdviseOrderPlacedWithDealer;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.DoublesUtil.round;

import java.io.IOException;
import java.text.ParseException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.amazonaws.SdkBaseException;
import com.amazonaws.retry.RetryUtils;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.FailedBatch;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.BatchWriteRetryStrategy;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.wt.aws.cognito.exception.InternalErrorException;
import com.wt.config.utils.WDProperties;
import com.wt.domain.Advise;
import com.wt.domain.CurrentAdviserDb;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.CurrentUserDb;
import com.wt.domain.CurrentUserFundDb;
import com.wt.domain.LastViewPopulatedSequenceNumber;
import com.wt.domain.Order;
import com.wt.domain.Trade;
import com.wt.domain.UserAdvise;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.UserFundPerformance;
import com.wt.domain.UserPerformance;
import com.wt.domain.repo.AdviserRepository;
import com.wt.domain.repo.FundAdviseRepository;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.repo.UserEquityAdviseRepository;
import com.wt.domain.repo.UserFundRepository;
import com.wt.domain.repo.UserMFAdviseRepository;
import com.wt.domain.repo.UserRepository;
import com.wt.domain.write.WithdrawalMadefromFund;
import com.wt.domain.write.events.AdditionalSumAddedToUserFund;
import com.wt.domain.write.events.AdjustFundCashForInsufficientCapitalAdvise;
import com.wt.domain.write.events.AdvicesIssuedPreviously;
import com.wt.domain.write.events.AdviseOrderPlacedWithExchange;
import com.wt.domain.write.events.AdviseOrderReceivedWithBroker;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.AdvisePerformanceCalculated;
import com.wt.domain.write.events.AdviseTokenPriceReceived;
import com.wt.domain.write.events.AdviserFundClosed;
import com.wt.domain.write.events.AdviserFundFloated;
import com.wt.domain.write.events.AdviserRegisteredfromUserPool;
import com.wt.domain.write.events.CognitoAdviserRegistered;
import com.wt.domain.write.events.CorporateActionSellDetails;
import com.wt.domain.write.events.CorporateMergerPrimaryBuyEvent;
import com.wt.domain.write.events.CorporateMergerSecondaryBuyEvent;
import com.wt.domain.write.events.CurrentFundPortfolioValue;
import com.wt.domain.write.events.DividendEventReceived;
import com.wt.domain.write.events.ExecutedAdviseDetailsUpdated;
import com.wt.domain.write.events.ExitAdviseReceived;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.FundAdviceTradeExecutedForWithdrawal;
import com.wt.domain.write.events.FundAdviceTradeRejectedForWithdrawal;
import com.wt.domain.write.events.FundAttributeUpdate;
import com.wt.domain.write.events.FundClosed;
import com.wt.domain.write.events.FundFloated;
import com.wt.domain.write.events.FundLumpSumAdded;
import com.wt.domain.write.events.FundSubscribed;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.FundUserSubscribed;
import com.wt.domain.write.events.GenerateClientOrderId;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.KycApprovedEvent;
import com.wt.domain.write.events.MaxDrawdownCalculated;
import com.wt.domain.write.events.PhoneNumberUpdated;
import com.wt.domain.write.events.RecommendedEntryPriceUpdated;
import com.wt.domain.write.events.SpecialCircumstancesBuyEvent;
import com.wt.domain.write.events.UpdateCorporateEvent;
import com.wt.domain.write.events.UpdateKYCDetails;
import com.wt.domain.write.events.UpdatedWdIdsForAdvisesReceived;
import com.wt.domain.write.events.UpdatingAdviseWdIds;
import com.wt.domain.write.events.UpdatingUserFundCash;
import com.wt.domain.write.events.UpdatingWdId;
import com.wt.domain.write.events.UserAcceptanceModeUpdateReceived;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserFundAdditionalSumAdvice;
import com.wt.domain.write.events.UserFundAdviceClosed;
import com.wt.domain.write.events.UserFundAdviceIssued;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.domain.write.events.UserFundDividendReceived;
import com.wt.domain.write.events.UserFundWithdrawalDoneSuccessfully;
import com.wt.domain.write.events.UserLumpSumAddedToFund;
import com.wt.domain.write.events.UserOpenAdviseOrderRejected;
import com.wt.domain.write.events.UserRegistered;
import com.wt.domain.write.events.UserRegisteredFromUserPool;
import com.wt.domain.write.events.UserResponseReceived;
import com.wt.domain.write.events.UserRiskProfileIdentified;
import com.wt.domain.write.events.UserSubscribedToFund;
import com.wt.domain.write.events.UserUnsubscribedToFund;
import com.wt.domain.write.events.WealthCodeUpdated;
import com.wt.domain.write.events.WithdrawSumFromUserFundAttempted;

import akka.persistence.query.EventEnvelope;
import lombok.extern.log4j.Log4j;

@Service("CommonHandler")
@Log4j
public class CommonHandler {

  private FundAdviseRepository adviceRepository;
  private AdviserRepository adviserRepository;
  private UserRepository userRepository;
  private FundRepository fundRepository;
  private UserFundRepository userFundRepository;
  private UserEquityAdviseRepository userEquityAdviseRepository;
  private UserMFAdviseRepository userMFAdviseRepository;

  private Map<String, Advise> advices = new HashMap<String, Advise>();
  private Map<String, CurrentAdviserDb> advisers = new HashMap<String, CurrentAdviserDb>();
  private Map<String, CurrentUserDb> users = new HashMap<String, CurrentUserDb>();
  private Map<String, CurrentFundDb> funds = new HashMap<String, CurrentFundDb>();
  private Map<UserFundCompositeKey, CurrentUserFundDb> userFunds = new HashMap<UserFundCompositeKey, CurrentUserFundDb>();
  private Map<UserAdviseCompositeKey, UserAdvise> userAdvises = new HashMap<UserAdviseCompositeKey, UserAdvise>();

  private LinkedList<DbObject> modifiedObjects = new LinkedList<DbObject>();
  private LinkedList<DbObject> retryObjects = new LinkedList<DbObject>();

  DynamoDBMapper dbMapper;
  WDProperties properties;

  @Autowired
  public CommonHandler(FundAdviseRepository adviceRepository, UserRepository userRepository, AmazonDynamoDB dynamoDB,
      FundRepository fundRepository, UserFundRepository userFundRepository,
      UserEquityAdviseRepository userEquityAdviseRepository, UserMFAdviseRepository userMFAdviseRepository,
      AdviserRepository adviserRepository, WDProperties properties) throws InterruptedException {
    super();
    this.adviceRepository = adviceRepository;
    this.userRepository = userRepository;
    this.fundRepository = fundRepository;
    this.userEquityAdviseRepository = userEquityAdviseRepository;
    this.userMFAdviseRepository = userMFAdviseRepository;
    this.userFundRepository = userFundRepository;
    this.adviserRepository = adviserRepository;

    DynamoDBMapperConfig config = new DynamoDBMapperConfig.Builder()
        .withBatchWriteRetryStrategy(new BatchWriteRetryStrategy() {
          @Override
          public int getMaxRetryOnUnprocessedItems(Map<String, List<WriteRequest>> batchWriteItemInput) {
            return 5;
          }

          @Override
          public long getDelayBeforeRetryUnprocessedItems(Map<String, List<WriteRequest>> unprocessedItems,
              int retriesAttempted) {
            return 1000;
          }
        }).build();
    

    dbMapper = new DynamoDBMapper(dynamoDB, config);
    this.properties = properties;
  }

  private CurrentFundDb findFund(String fundname) {
    CurrentFundDb fund = funds.get(fundname);
    if (fund == null) {
      Optional<CurrentFundDb> result = fundRepository.findById(fundname);
      if (result.isPresent()) {
        fund = result.get();
        funds.put(fundname, fund);
      }
    }
    return fund;
  }

  private CurrentUserDb findUser(String username) {
    CurrentUserDb user = users.get(username);
    if (user == null) {
      Optional<CurrentUserDb> result = userRepository.findById(username);
      if (result.isPresent()) {
        user = result.get();
        users.put(username, user);
      }
    }
    return user;
  }

  private CurrentAdviserDb findAdviser(String adviserUsername, boolean createOneIfNotPresent) {
    CurrentAdviserDb adviser = advisers.get(adviserUsername);
    if (adviser == null) {
      Optional<CurrentAdviserDb> result = adviserRepository.findById(adviserUsername);
      if (result.isPresent()) {
        adviser = result.get();
        advisers.put(adviserUsername, adviser);
      } else if (createOneIfNotPresent) {
        adviser = new CurrentAdviserDb();
        advisers.put(adviserUsername, adviser);

      }
    }
    return adviser;
  }

  private Advise findAdvice(String key) {
    Advise advise = advices.get(key);
    if (advise == null) {
      Optional<Advise> result = adviceRepository.findById(key);
      if (result.isPresent()) {
        advise = result.get();
        advices.put(key, advise);
      }
    }
    return advise;
  }

  private CurrentUserFundDb findUserFund(UserFundCompositeKey key) {
    CurrentUserFundDb userFund = userFunds.get(key);
    if (userFund == null) {
      Optional<CurrentUserFundDb> result = userFundRepository.findById(key);
      if (result.isPresent()) {
        userFund = result.get();
        userFunds.put(key, userFund);
      }
    }
    return userFund;
  }

  class DbObject {
    private final List<Entry<Object, Object>> data;
    private final LastViewPopulatedSequenceNumber sequenceNumber;
    private final boolean isDeleteObject;


    public DbObject(List<Entry<Object, Object>> data, LastViewPopulatedSequenceNumber sequenceNumber, boolean isDeleteObject) {
      super();
      this.data = data;
      this.sequenceNumber = sequenceNumber;
      this.isDeleteObject = isDeleteObject;
    }
  }
  
  public void tryToHandle(EventEnvelope eventEnvelope) throws IOException, IllegalArgumentException, InternalErrorException, ParseException {
    Object event = eventEnvelope.event();
    log.debug("Handling on common handler: " + eventEnvelope.persistenceId() + ", event number : "
        + eventEnvelope.sequenceNr() + ", event type : " + event.getClass());
    List<Entry<Object, Object>> dbDataToBeAdded = new ArrayList<Entry<Object, Object>>();
    List<Entry<Object, Object>> dbDataToBeRemoved = new ArrayList<Entry<Object, Object>>();
    boolean isEventHandled = true;

    if (event instanceof CognitoAdviserRegistered) {
      CognitoAdviserRegistered registeredAdviser = (CognitoAdviserRegistered) event;
      CurrentAdviserDb adviser = findAdviser(registeredAdviser.getAdviserUsername(), true);
      adviser.update(registeredAdviser);
      advisers.put(registeredAdviser.getAdviserUsername(), adviser);
      addDbEntry(dbDataToBeAdded, registeredAdviser.getAdviserUsername(), adviser);
    } else if (event instanceof AdviserRegisteredfromUserPool) {
      AdviserRegisteredfromUserPool registeredAdviser = (AdviserRegisteredfromUserPool) event;
      CurrentAdviserDb adviser = findAdviser(registeredAdviser.getAdviserUsername(), true);
      adviser.update(registeredAdviser);
      advisers.put(registeredAdviser.getAdviserUsername(), adviser);
      addDbEntry(dbDataToBeAdded, registeredAdviser.getAdviserUsername(), adviser);
    } else if (event instanceof AdviserFundFloated) {
      AdviserFundFloated adviserFundFloated = (AdviserFundFloated) event;
      CurrentAdviserDb adviser = findAdviser(adviserFundFloated.getAdviserUsername(), false);
      adviser.update(adviserFundFloated);
      addDbEntry(dbDataToBeAdded, adviserFundFloated.getAdviserUsername(), adviser);
    } else if (event instanceof AdviserFundClosed) {
      AdviserFundClosed adviserFundClosed = (AdviserFundClosed) event;
      CurrentAdviserDb adviser = findAdviser(adviserFundClosed.getAdviserUsername(), false);
      adviser.update(adviserFundClosed);
      advisers.put(adviser.getAdviserUsername(), adviser);
      addDbEntry(dbDataToBeAdded, adviser.getAdviserUsername(), adviser);
    } else if (event instanceof FundFloated) {
      FundFloated floatedFund = (FundFloated) event;
      CurrentFundDb fund = findFund(floatedFund.getFundName());

      if (fund == null) {
        fund = new CurrentFundDb(floatedFund.getAdviserUsername(), floatedFund.getAdviserName(), floatedFund.getFundName(),
            floatedFund.getStartDate(), floatedFund.getEndDate(), floatedFund.getDescription(), floatedFund.getMinSIP(),
            floatedFund.getMinLumpSum(), floatedFund.getRiskLevel());
      }
      fund.update(floatedFund);

      funds.put(fund.getFundName(), fund);
      addDbEntry(dbDataToBeAdded, fund.getFundName(), fund);
    } else if (event instanceof FundClosed) {
      FundClosed fundClosed = (FundClosed) event;

      CurrentFundDb fund = findFund(fundClosed.getFundName());
      fund.update(fundClosed);
      funds.remove(fund.getFundName());
      removeDbEntry(dbDataToBeRemoved, fund.getFundName(), fund);
    } else if (event instanceof FundAttributeUpdate) {
      FundAttributeUpdate floatedFund = (FundAttributeUpdate) event;

      CurrentFundDb fund = findFund(floatedFund.getFundName());

      fund.update(floatedFund);
      addDbEntry(dbDataToBeAdded, fund.getFundName(), fund);
    }else if (event instanceof FundAdviceIssued) {
      FundAdviceIssued fundAdviceIssued = (FundAdviceIssued) event;
      CurrentFundDb fund = findFund(fundAdviceIssued.getFundName());
      fund.update(fundAdviceIssued);
      Advise issuedAdvise = findAdvice(fundAdviceIssued.getAdviseId());
      if (issuedAdvise == null) {
        issuedAdvise = new Advise();
        issuedAdvise.update(fundAdviceIssued);
      } else {
        issuedAdvise.updateExistingAdvise(fundAdviceIssued);
      }
      advices.put(fundAdviceIssued.getAdviseId(), issuedAdvise);

      addDbEntry(dbDataToBeAdded, fundAdviceIssued.getAdviseId(), issuedAdvise);
      addDbEntry(dbDataToBeAdded, fundAdviceIssued.getFundName(), fund);
    } else if (event instanceof UpdatingAdviseWdIds) {
      UpdatingAdviseWdIds updatingAdviseWdIds = (UpdatingAdviseWdIds) event;
      CurrentFundDb fund = findFund(updatingAdviseWdIds.getFundName());
      fund.update(updatingAdviseWdIds);

      addDbEntry(dbDataToBeAdded, updatingAdviseWdIds.getFundName(), fund);
    } else if (event instanceof FundAdviceClosed) {
      FundAdviceClosed closedAdvise = (FundAdviceClosed) event;
      CurrentFundDb fund = findFund(closedAdvise.getFundName());
      fund.update(closedAdvise);
      Advise origAdvise = findAdvice(closedAdvise.getAdviseId());
      origAdvise.update(closedAdvise);

      addDbEntry(dbDataToBeAdded, closedAdvise.getAdviseId(), origAdvise);
      addDbEntry(dbDataToBeAdded, closedAdvise.getFundName(), fund);
    } else if (event instanceof AdvisePerformanceCalculated) {
      AdvisePerformanceCalculated performance = (AdvisePerformanceCalculated) event;
      Advise advise = findAdvice(performance.getAdviseId());
      advise.update(performance);
      addDbEntry(dbDataToBeAdded, advise.getId(), advise);
    } else if (event instanceof UserSubscribedToFund) {
      UserSubscribedToFund fundUserSubscribedEvent = (UserSubscribedToFund) event;
      CurrentUserDb user = findUser(getRequiredUsername(fundUserSubscribedEvent.getUsername()));

      if (user == null) {
        user = new CurrentUserDb();
        user.setEmail(fundUserSubscribedEvent.getUsername());
      }
      user.update(fundUserSubscribedEvent);
      users.put(getRequiredUsername(user.getUsername()), user);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
    }  else if (event instanceof UpdateKYCDetails) {
      UpdateKYCDetails updateKycEvent = (UpdateKYCDetails) event;
      CurrentUserDb user = findUser(getRequiredUsername(updateKycEvent.getUsername()));
      user.update(updateKycEvent);
      users.put(getRequiredUsername(user.getUsername()), user);

      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
    }else if (event instanceof AdditionalSumAddedToUserFund) {
      AdditionalSumAddedToUserFund fundUserAddingLumpsum = (AdditionalSumAddedToUserFund) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(fundUserAddingLumpsum.getUsername()),
          fundUserAddingLumpsum.getFundName(), fundUserAddingLumpsum.getAdviserUsername(),fundUserAddingLumpsum.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(fundUserAddingLumpsum);
      userFundCompositeKey.setUsername(getRequiredUsername(userFundCompositeKey.getUsername()));
      userFunds.put(userFundCompositeKey, userFund);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if (event instanceof FundUserSubscribed) {
      FundUserSubscribed fundUserSubscribing = (FundUserSubscribed) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(fundUserSubscribing.getUsername()),
          fundUserSubscribing.getFundName(), fundUserSubscribing.getAdviserUsername(),
          fundUserSubscribing.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      if (userFund == null) {
        userFund = new CurrentUserFundDb();
      }
      userFund.update(fundUserSubscribing);
      userFunds.put(userFundCompositeKey, userFund);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if (event instanceof FundSubscribed) {
      FundSubscribed fundSubscribed = (FundSubscribed) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(fundSubscribed.getUsername()),
          fundSubscribed.getFundName(), fundSubscribed.getAdviserUsername(), fundSubscribed.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(fundSubscribed);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if (event instanceof FundLumpSumAdded) {
      FundLumpSumAdded fundLumpSumAdded = (FundLumpSumAdded) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(fundLumpSumAdded.getUsername()),
          fundLumpSumAdded.getFundName(), fundLumpSumAdded.getAdviserUsername(), fundLumpSumAdded.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(fundLumpSumAdded);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if(event instanceof UserAcceptanceModeUpdateReceived){
      UserAcceptanceModeUpdateReceived userAcceptanceModeUpdateReceived = (UserAcceptanceModeUpdateReceived)event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(userAcceptanceModeUpdateReceived.getUsername()),
          userAcceptanceModeUpdateReceived.getFundName(), userAcceptanceModeUpdateReceived.getAdviserUsername(),userAcceptanceModeUpdateReceived.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);

      userFund.update(userAcceptanceModeUpdateReceived);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if(event instanceof UserResponseReceived){
      UserResponseReceived userResponseReceived = (UserResponseReceived) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(userResponseReceived.getUsername()),
          userResponseReceived.getFundName(), userResponseReceived.getAdviserUsername(),userResponseReceived.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);

      userFund.update(userResponseReceived);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if (event instanceof FundSuccessfullyUnsubscribed) {
      FundSuccessfullyUnsubscribed fundSuccessfullyUnsubscribed = (FundSuccessfullyUnsubscribed) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(fundSuccessfullyUnsubscribed.getUsername()),
          fundSuccessfullyUnsubscribed.getFundName(), fundSuccessfullyUnsubscribed.getAdviserUsername(),fundSuccessfullyUnsubscribed.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);

      userFund.update(fundSuccessfullyUnsubscribed);
      removeDbEntry(dbDataToBeRemoved, userFund.getUserFundCompositeKey(), userFund);
      userFunds.remove(userFundCompositeKey);

    } else if (event instanceof UserFundDividendReceived) {
      UserFundDividendReceived userFundDividendReceived = (UserFundDividendReceived) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(userFundDividendReceived.getUsername()),
          userFundDividendReceived.getFundName(), userFundDividendReceived.getAdviserUsername(),userFundDividendReceived.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);

      userFund.update(userFundDividendReceived);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof UpdatingUserFundCash) {
      UpdatingUserFundCash updatingUserFundCash = (UpdatingUserFundCash) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(updatingUserFundCash.getUsername()),
          updatingUserFundCash.getFundName(), updatingUserFundCash.getAdviserUsername(),updatingUserFundCash.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);

      userFund.update(updatingUserFundCash);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    }else if (event instanceof UpdatedWdIdsForAdvisesReceived) {
      UpdatedWdIdsForAdvisesReceived updatedWdIdsForAdvisesReceived = (UpdatedWdIdsForAdvisesReceived) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(updatedWdIdsForAdvisesReceived.getUsername()),
          updatedWdIdsForAdvisesReceived.getFundName(), updatedWdIdsForAdvisesReceived.getAdviserUsername(),updatedWdIdsForAdvisesReceived.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);

      userFund.update(updatedWdIdsForAdvisesReceived);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof AdvicesIssuedPreviously) {
      AdvicesIssuedPreviously previouslyIssuedAdvices = (AdvicesIssuedPreviously) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(previouslyIssuedAdvices.getUsername()),
          previouslyIssuedAdvices.getFundName(), previouslyIssuedAdvices.getAdviserUsername(),previouslyIssuedAdvices.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(previouslyIssuedAdvices);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof AdjustFundCashForInsufficientCapitalAdvise) {
      AdjustFundCashForInsufficientCapitalAdvise adjustFundCashForInsufficientCapitalAdvise = (AdjustFundCashForInsufficientCapitalAdvise) event;

      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(adjustFundCashForInsufficientCapitalAdvise.getUsername()),
          adjustFundCashForInsufficientCapitalAdvise.getFundName(),
          adjustFundCashForInsufficientCapitalAdvise.getAdviserUsername(),
          adjustFundCashForInsufficientCapitalAdvise.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(adjustFundCashForInsufficientCapitalAdvise);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof UserFundAdviceIssued) {
      UserFundAdviceIssued fundAdviceIssuedWithUserEmail = (UserFundAdviceIssued) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(fundAdviceIssuedWithUserEmail.getUsername()),
          fundAdviceIssuedWithUserEmail.getFundName(), fundAdviceIssuedWithUserEmail.getAdviserUsername(), fundAdviceIssuedWithUserEmail.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(fundAdviceIssuedWithUserEmail);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof CorporateMergerPrimaryBuyEvent) {
      CorporateMergerPrimaryBuyEvent corporateMergerPrimaryBuyEvent = (CorporateMergerPrimaryBuyEvent) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(corporateMergerPrimaryBuyEvent.getUsername()),
          corporateMergerPrimaryBuyEvent.getFundName(), corporateMergerPrimaryBuyEvent.getAdviserUsername(), corporateMergerPrimaryBuyEvent.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(corporateMergerPrimaryBuyEvent);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof CorporateMergerSecondaryBuyEvent) {
      CorporateMergerSecondaryBuyEvent corporateMergerSecondaryBuyEvent = (CorporateMergerSecondaryBuyEvent) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(corporateMergerSecondaryBuyEvent.getUsername()),
          corporateMergerSecondaryBuyEvent.getFundName(), corporateMergerSecondaryBuyEvent.getAdviserUsername(), corporateMergerSecondaryBuyEvent.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(corporateMergerSecondaryBuyEvent);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof CorporateActionSellDetails) {
      CorporateActionSellDetails corporateActionSellDetails = (CorporateActionSellDetails) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(corporateActionSellDetails.getUsername()),
          corporateActionSellDetails.getFundName(), corporateActionSellDetails.getAdviserUsername(), corporateActionSellDetails.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(corporateActionSellDetails);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof UserFundAdviceClosed) {
      UserFundAdviceClosed adviseClosed = (UserFundAdviceClosed) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(adviseClosed.getUsername()),
          adviseClosed.getFundName(), adviseClosed.getAdviserUsername(), adviseClosed.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(adviseClosed);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof SpecialCircumstancesBuyEvent) {
      SpecialCircumstancesBuyEvent specialCircumstancesBuyEvent = (SpecialCircumstancesBuyEvent) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(specialCircumstancesBuyEvent.getUsername()),
          specialCircumstancesBuyEvent.getFundName(), specialCircumstancesBuyEvent.getAdviserUsername(), specialCircumstancesBuyEvent.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(specialCircumstancesBuyEvent);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof FundAdviceTradeExecuted) {
      FundAdviceTradeExecuted fundAdviseTradeExecuted = (FundAdviceTradeExecuted) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(fundAdviseTradeExecuted.getUsername()),
          fundAdviseTradeExecuted.getFundName(), fundAdviseTradeExecuted.getAdviserUsername(), fundAdviseTradeExecuted.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(fundAdviseTradeExecuted);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof ExecutedAdviseDetailsUpdated) {
      ExecutedAdviseDetailsUpdated executedAdviseDetailsUpdated = (ExecutedAdviseDetailsUpdated) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(executedAdviseDetailsUpdated.getUsername()),
          executedAdviseDetailsUpdated.getFundName(), executedAdviseDetailsUpdated.getAdviserUsername(), executedAdviseDetailsUpdated.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);
      userFund.update(executedAdviseDetailsUpdated);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof UserOpenAdviseOrderRejected) {
      UserOpenAdviseOrderRejected userOpenAdviseOrderRejected = (UserOpenAdviseOrderRejected) event;

      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(getRequiredUsername(userOpenAdviseOrderRejected.getUsername()),
          userOpenAdviseOrderRejected.getFundName(), userOpenAdviseOrderRejected.getAdviserUsername(), userOpenAdviseOrderRejected.getInvestingMode());
      CurrentUserFundDb userFund = getUserFund(userFundCompositeKey);

      userFund.update(userOpenAdviseOrderRejected);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);

    } else if (event instanceof UserFundAdviceIssuedWithUserEmail) {
      UserFundAdviceIssuedWithUserEmail userFundAdviseIssued = (UserFundAdviceIssuedWithUserEmail) event;
      IssueType issueType = (userFundAdviseIssued.getIssueType() == null) ? IssueType.Equity : userFundAdviseIssued.getIssueType();
      UserAdvise userAdvise = issueType.userAdvise();
      userAdvise.update(userFundAdviseIssued);
      updateUserAdvises(userAdvise);
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
    } else if (event instanceof DividendEventReceived) {
      DividendEventReceived dividendEventReceived = (DividendEventReceived) event;
      UserAdvise userAdvise = userAdvise(dividendEventReceived);
      userAdvise.update(dividendEventReceived);
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
    } else if (event instanceof UpdatingWdId) {
      UpdatingWdId updatingWdId = (UpdatingWdId) event;
      UserAdvise userAdvise = userAdvise(updatingWdId);
      userAdvise.update(updatingWdId);
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
    } else if (event instanceof UserFundAdditionalSumAdvice) {
      UserFundAdditionalSumAdvice userFundAdviseIssued = (UserFundAdditionalSumAdvice) event;
      UserAdvise userAdvise = userAdvise(userFundAdviseIssued);
      if (userAdvise == null) {
        IssueType issueType = (userFundAdviseIssued.getIssueType() == null) ? IssueType.Equity : userFundAdviseIssued.getIssueType();
        userAdvise = issueType.userAdvise();
      }
      userAdvise.update(userFundAdviseIssued);
      updateUserAdvises(userAdvise);
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
    } else if (event instanceof UpdateCorporateEvent) {
      UpdateCorporateEvent updateCorporateEvent = (UpdateCorporateEvent) event;
      UserAdvise userAdvise = userAdvise(updateCorporateEvent);
      if (userAdvise == null) {
        IssueType issueType = (updateCorporateEvent.getIssueType() == null) ? IssueType.Equity : updateCorporateEvent.getIssueType();
        userAdvise = issueType.userAdvise();
        updateUserAdvises(userAdvise);
      }
      userAdvise.update(updateCorporateEvent);
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
    } else if (event instanceof GenerateClientOrderId) {
      GenerateClientOrderId generateClientOrderId = (GenerateClientOrderId) event;
      UserAdvise userAdvise = userAdvise(generateClientOrderId);
      userAdvise.update(generateClientOrderId);
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
    } else if (event instanceof ExitAdviseReceived) {
      ExitAdviseReceived exitAdviseReceived = (ExitAdviseReceived) event;
      UserAdvise userAdvise = userAdvise(exitAdviseReceived);
      userAdvise.update(exitAdviseReceived);
      Order closeAdviseOrder = userAdvise.getLatestSellOrder().get();
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
      if(closeAdviseOrder != null)
        addDbEntry(dbDataToBeAdded, closeAdviseOrder.getOrderId(), closeAdviseOrder);
    } else if (event instanceof AdviseTokenPriceReceived) {
      AdviseTokenPriceReceived adviseTokenPriceReceived = (AdviseTokenPriceReceived) event;
      UserAdvise userAdvise = userAdvise(adviseTokenPriceReceived);
      userAdvise.update(adviseTokenPriceReceived);
      Order issuedAdviseOrder = userAdvise.getLatestIssuedAdviseOrder().get();
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
      if(issuedAdviseOrder.getOrderId() != null)
        addDbEntry(dbDataToBeAdded, issuedAdviseOrder.getOrderId(), issuedAdviseOrder);
    } else if (event instanceof AdviseOrderReceivedWithBroker) {
      AdviseOrderReceivedWithBroker adviseOrderReceivedWithBroker = (AdviseOrderReceivedWithBroker) event;
      UserAdvise userAdvise = userAdvise(adviseOrderReceivedWithBroker);
      userAdvise.update(adviseOrderReceivedWithBroker);
            Order adviseOrder = null;
      if (adviseOrderReceivedWithBroker.getUserAdviseState().equals(CorporateActionOrderPlaced)) {
          adviseOrder = userAdvise.getLatestCorpActionOrder().get();
      } else if (adviseOrderReceivedWithBroker.getUserAdviseState().equals(AdviseTokenPriceReceived)
          || adviseOrderReceivedWithBroker.getUserAdviseState().equals(UserMFAdviseIssued)
          || adviseOrderReceivedWithBroker.getUserAdviseState().equals(UserMFOpenAdviseOrderExecuted)
          || adviseOrderReceivedWithBroker.getUserAdviseState().equals(AdditionalLumpSumToMF)) {
          adviseOrder = userAdvise.getLatestIssuedAdviseOrder().get();
      } else {
          adviseOrder = userAdvise.getLatestSellOrder().get();
      }
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
      if(adviseOrder != null)
        addDbEntry(dbDataToBeAdded, adviseOrder.getOrderId(), adviseOrder);
    } else if (event instanceof AdviseOrderPlacedWithExchange) {
      AdviseOrderPlacedWithExchange adviseOrderPlacedWithExchange = (AdviseOrderPlacedWithExchange) event;
      UserAdvise userAdvise = userAdvise(adviseOrderPlacedWithExchange);
      userAdvise.update(adviseOrderPlacedWithExchange);
      Order adviseOrder = null;
      if (adviseOrderPlacedWithExchange.getUserAdviseState().equals(CorporateActionOrderPlacedWithBroker)) {
        adviseOrder = userAdvise.getLatestCorpActionOrder().get();
      } else if (adviseOrderPlacedWithExchange.getUserAdviseState().equals(OpenAdviseOrderReceivedWithBroker)) {
        adviseOrder = userAdvise.getLatestIssuedAdviseOrder().get();
      } else {
          adviseOrder = userAdvise.getLatestSellOrder().get();
      }
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
      if(adviseOrder != null)
        addDbEntry(dbDataToBeAdded, adviseOrder.getOrderId(), adviseOrder);
    } else if (event instanceof AdviseOrderTradeExecuted) {
      AdviseOrderTradeExecuted adviseTradeExecuted = (AdviseOrderTradeExecuted) event;
      UserAdvise userAdvise = userAdvise(adviseTradeExecuted);
      userAdvise.update(adviseTradeExecuted);
      Order adviseOrder = null;
      if (!adviseTradeExecuted.getUserAdviseState().equals(CorporateActionOrderPlacedWithExchange)) {
        if (adviseTradeExecuted.getUserAdviseState().equals(OpenAdviseOrderPlacedWithExchange)
            || adviseTradeExecuted.getUserAdviseState().equals(UserMFOpenAdviseOrderExecuted)
            || adviseTradeExecuted.getUserAdviseState().equals(UserMFOpenAdviseOrderPlacedWithDealer)) {
          adviseOrder = userAdvise.getLatestIssuedAdviseOrder().get();
          List<Trade> trades = adviseOrder.getTrades();
          trades.stream().forEach(trade -> trade.setPrice(round(trade.getPrice())));
        } else {
          adviseOrder = userAdvise.getLatestSellOrder().get();
          if (adviseOrder != null)
            adviseOrder.getTrades().stream().forEach(trade -> trade.setPrice(round(trade.getPrice())));
        }
      } else {
        adviseOrder = userAdvise.getLatestCorpActionOrder().get();
        List<Trade> trades = adviseOrder.getTrades();
        trades.stream().forEach(trade -> trade.setPrice(round(trade.getPrice())));
      }
      addDbEntry(dbDataToBeAdded, userAdvise.getUserAdviseCompositeKey(), userAdvise);
      if (adviseOrder != null)
        addDbEntry(dbDataToBeAdded, adviseOrder.getOrderId(), adviseOrder);
    } else if (event instanceof WithdrawSumFromUserFundAttempted) {
      WithdrawSumFromUserFundAttempted evt = (WithdrawSumFromUserFundAttempted)event;
      CurrentUserFundDb userFund = findUserFund(new UserFundCompositeKey(evt.getUsername().split("@")[0], 
          evt.getFundName(), evt.getAdviserUsername(), evt.getInvestingMode()));
      userFund.update(evt);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if(event instanceof CurrentFundPortfolioValue) {
      CurrentFundPortfolioValue evt = (CurrentFundPortfolioValue)event;
      CurrentUserFundDb userFund = findUserFund(new UserFundCompositeKey(evt.getUsername().split("@")[0], 
          evt.getFundName(), evt.getAdviserUsername(), evt.getInvestingMode()));
      userFund.update(evt);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if (event instanceof FundAdviceTradeExecutedForWithdrawal) {
      FundAdviceTradeExecutedForWithdrawal evt = (FundAdviceTradeExecutedForWithdrawal) event;
      CurrentUserFundDb userFund = findUserFund(new UserFundCompositeKey(evt.getUsername().split("@")[0], 
          evt.getFundName(), evt.getAdviserUsername(), evt.getInvestingMode()));
      userFund.update(evt);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    }else if (event instanceof FundAdviceTradeRejectedForWithdrawal) {
      FundAdviceTradeRejectedForWithdrawal evt = (FundAdviceTradeRejectedForWithdrawal) event;
      CurrentUserFundDb userFund = findUserFund(new UserFundCompositeKey(evt.getUsername().split("@")[0], 
          evt.getFundName(), evt.getAdviserUsername(), evt.getInvestingMode()));
      userFund.update(evt);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    }else if( event instanceof UserFundWithdrawalDoneSuccessfully) {
      UserFundWithdrawalDoneSuccessfully evt = (UserFundWithdrawalDoneSuccessfully)event;
      CurrentUserFundDb userFund = findUserFund(new UserFundCompositeKey(evt.getUsername().split("@")[0], 
          evt.getFundName(), evt.getAdviserUsername(), evt.getInvestingMode()));
      userFund.update(evt);
      addDbEntry(dbDataToBeAdded, userFund.getUserFundCompositeKey(), userFund);
    } else if (event instanceof UserUnsubscribedToFund) {
      UserUnsubscribedToFund fundUserUnsubscribedEvent = (UserUnsubscribedToFund) event;
      CurrentUserDb user = findUser(getRequiredUsername(fundUserUnsubscribedEvent.getUsername()));
      user.update(fundUserUnsubscribedEvent);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
    } else if (event instanceof MaxDrawdownCalculated) {
      MaxDrawdownCalculated evt = (MaxDrawdownCalculated) event;
      CurrentFundDb fund = findFund(evt.getFundName());
      fund.setDrawDowns(evt.getMaxDrawDowns());
      addDbEntry(dbDataToBeAdded, fund.getFundName(), fund);
    } else if (event instanceof UserRegistered) {
      UserRegistered registerUserEvent = (UserRegistered) event;
      CurrentUserDb user = new CurrentUserDb();
      user.update(registerUserEvent);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
      users.put(getRequiredUsername(user.getUsername()), user);
    } else if (event instanceof UserRegisteredFromUserPool) {
      UserRegisteredFromUserPool registerUserEvent = (UserRegisteredFromUserPool) event;
      CurrentUserDb user = new CurrentUserDb();
      user.update(registerUserEvent);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
      users.put(getRequiredUsername(user.getUsername()), user);
    } else if (event instanceof UserRiskProfileIdentified) {
      UserRiskProfileIdentified userRiskProfileIdentified = (UserRiskProfileIdentified) event;
      CurrentUserDb user = findUser(getRequiredUsername(userRiskProfileIdentified.getUsername()));
      user.update(userRiskProfileIdentified);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
    } else if (event instanceof PhoneNumberUpdated) {
      PhoneNumberUpdated evt = (PhoneNumberUpdated) event;
      CurrentUserDb user = findUser(getRequiredUsername(evt.getUsername()));
      user.update(evt);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
    } else if (event instanceof RecommendedEntryPriceUpdated) {
      RecommendedEntryPriceUpdated recommendedPriceUpdated = (RecommendedEntryPriceUpdated) event;
      Advise advise = findAdvice(recommendedPriceUpdated.getAdviseId());
      advise.update(recommendedPriceUpdated);
      addDbEntry(dbDataToBeAdded, advise.getId(), advise);
    } else if (event instanceof KycApprovedEvent) {
      KycApprovedEvent evt = (KycApprovedEvent) event;
      CurrentUserDb user = findUser(getRequiredUsername(evt.getUsername()));
      user.update(evt);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
    } else if (event instanceof WealthCodeUpdated) {
      WealthCodeUpdated evt = (WealthCodeUpdated) event;
      CurrentUserDb user = findUser(getRequiredUsername(evt.getUsername()));
      user.update(evt);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
    } else if (event instanceof UserLumpSumAddedToFund) {
      UserLumpSumAddedToFund evt = (UserLumpSumAddedToFund)event;
      CurrentUserDb user = findUser(getRequiredUsername(evt.getUsername()));
      user.update(evt);
      addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
  } else if (event instanceof WithdrawalMadefromFund) {
      WithdrawalMadefromFund evt = (WithdrawalMadefromFund)event;
      CurrentUserDb user = findUser(getRequiredUsername(evt.getUsername()));
      if(user!= null) {
        user.update(evt);
        addDbEntry(dbDataToBeAdded, getRequiredUsername(user.getUsername()), user);
      }
  } else {
    isEventHandled = false;
    }

    LastViewPopulatedSequenceNumber sequenceNumber = new LastViewPopulatedSequenceNumber(eventEnvelope.persistenceId(),
        eventEnvelope.sequenceNr());
    if (!isEventHandled) {
      modifiedObjects.add(new DbObject(Collections.emptyList(), sequenceNumber, false));
    }

    if (!dbDataToBeAdded.isEmpty()) {
      modifiedObjects.add(new DbObject(dbDataToBeAdded, sequenceNumber, false));
    }
    if (!dbDataToBeRemoved.isEmpty()) {
      modifiedObjects.add(new DbObject(dbDataToBeRemoved, sequenceNumber, true));
    }
  }

  private String getRequiredUsername(String username) {
    if (username.equals("rhunadkat@outlook.com"))
      return "rhunadkat1";
    return username.split("@")[0];
  }

  private void updateUserAdvises(UserAdvise userAdvise) {
    UserAdviseCompositeKey userAdviseCompositeKey = userAdvise.getUserAdviseCompositeKey();
    userAdviseCompositeKey.setUsernameWithFundWithAdviser(
        getUpdatedUsernameInKey(userAdviseCompositeKey.getUsernameWithFundWithAdviser(), 0));
    userAdvise.setUserAdviseCompositeKey(userAdviseCompositeKey);
    userAdvises.put(userAdviseCompositeKey, userAdvise);
  }

  private CurrentUserFundDb getUserFund(UserFundCompositeKey userFundCompositeKey) {
    userFundCompositeKey.setUsername(getRequiredUsername(userFundCompositeKey.getUsername()));
    return findUserFund(userFundCompositeKey);
  }

  private void addDbEntry(List<Entry<Object, Object>> dbData, Object key, Object value) {
    if (value instanceof UserAdvise) {
      ((UserAdvise) value)
          .setUsernameWithFundWithAdviser(getUpdatedUsernameInKey(((UserAdvise) value).getUsernameWithFundWithAdviser(), 0));
      ((UserAdviseCompositeKey) key).setUsernameWithFundWithAdviser(
          getUpdatedUsernameInKey(((UserAdviseCompositeKey) key).getUsernameWithFundWithAdviser(), 0));
    } else if (value instanceof Order) {
      ((Order) value).setUsernameWithOrderId(getUpdatedUsernameInKey(((Order) value).getUsernameWithOrderId(), 0));
    } else if (value instanceof CurrentUserFundDb) {
      ((CurrentUserFundDb) value).setUsername(getRequiredUsername(((CurrentUserFundDb) value).getUsername()));
      ((UserFundCompositeKey) key).setUsername(getRequiredUsername(((UserFundCompositeKey) key).getUsername()));
    } else if (value instanceof UserFundPerformance) {
      ((UserFundPerformance) value).setUsernameFundWithAdviseUsername(
          getUpdatedUsernameInKey(((UserFundPerformance) value).getUsernameFundWithAdviseUsername(), 0));
      key = getUpdatedUsernameInKey((String) key, 0);
    } else if(value instanceof UserPerformance) {
      ((UserPerformance) value).setUsernameWithInvestingMode(
          getUpdatedUsernameInKey(((UserPerformance) value).getUsernameWithInvestingMode(), 0));
      key = getUpdatedUsernameInKey((String) key, 0);
    } else if(value instanceof CurrentUserDb) {
      ((CurrentUserDb) value).setUsername(getRequiredUsername(((CurrentUserDb) value).getUsername()));
      key = getRequiredUsername(((String) key));
    }
    
    dbData.add(new SimpleEntry<Object, Object>(key, value));
  }

  private void removeDbEntry(List<Entry<Object, Object>> dbDataToBeRemoved, Object key, Object value) {
    if (value instanceof UserAdvise) {
      ((UserAdvise) value)
          .setUsernameWithFundWithAdviser(getUpdatedUsernameInKey(((UserAdvise) value).getUsernameWithFundWithAdviser(), 0));
      ((UserAdviseCompositeKey) key).setUsernameWithFundWithAdviser(
          getUpdatedUsernameInKey(((UserAdviseCompositeKey) key).getUsernameWithFundWithAdviser(), 0));
    } else if (value instanceof CurrentUserFundDb) {
      ((CurrentUserFundDb) value).setUsername(getRequiredUsername(((CurrentUserFundDb) value).getUsername()));
      ((UserFundCompositeKey) key).setUsername(getRequiredUsername(((UserFundCompositeKey) key).getUsername()));
    } 

    dbDataToBeRemoved.add(new SimpleEntry<Object, Object>(key, value));
  }
  
  private String getUpdatedUsernameInKey(String usernameOrEmail, int usernamePosition) {
    String[] splitKeys = usernameOrEmail.split(CompositeKeySeparator);
    splitKeys[usernamePosition] = getRequiredUsername(splitKeys[usernamePosition]);
    return Arrays.stream(splitKeys).collect(Collectors.joining(CompositeKeySeparator));
  }

  @Scheduled(fixedDelayString = "${dbUploadRate.in.milliseconds}")
  public void uploadDataToDB() {
    if (!properties.scheduleViewpopWrites()) {
      return;
    }

    if (modifiedObjects.peek() == null && retryObjects.peek() == null) {
      return;
    }
    
    log.debug("Number of Pending entries before uploading to Database, New Entries: " + modifiedObjects.size()
        + " Retry Entries: " + retryObjects.size());
    Map<Object, Object> objectsToWrite = new HashMap<>();
    Map<Object, Object> objectsToDelete = new HashMap<>();
    LinkedList<DbObject> uploadedObjects = new LinkedList<DbObject>();
    LinkedList<DbObject> dbDataList = retryObjects.peek() == null ? modifiedObjects : retryObjects;

    while ((objectsToWrite.size() + objectsToDelete.size()) < properties.maxNumberOfWritesToDBInMinute()) {
      DbObject ob = dbDataList.pollFirst();
      if (ob == null) {
        break;
      }
      List<Entry<java.lang.Object, java.lang.Object>> dataList = ob.data;
      if (ob.isDeleteObject) {
        for (Entry<Object, Object> e : dataList) {
          objectsToDelete.put(e.getKey(), e.getValue());
          objectsToWrite.remove(e.getKey());
        }
      } else {
        for (Entry<Object, Object> e : dataList) {
          objectsToWrite.put(e.getKey(), e.getValue());
          objectsToDelete.remove(e.getKey());
        }
      }
      uploadedObjects.add(ob);
    }
 log.debug("Number of entries in the current batch Modified Entries: " + objectsToWrite.size()
    + " Deleted Entries: " + objectsToDelete.size());

    List<FailedBatch> failedBatches = dbMapper.batchWrite(objectsToWrite.values(), objectsToDelete.values());
    if (failedBatches.isEmpty()) {
      Map<Object, Object> sequenceNumbers = new HashMap<>();
       for (DbObject ob : uploadedObjects) {
         sequenceNumbers.put("SequenceNumber:" + ob.sequenceNumber.getPersistenceId(), ob.sequenceNumber);
         
       }
       dbMapper.batchSave(sequenceNumbers.values());
    }
    else {
      boolean canRetryAllException = true;
      
      for (FailedBatch b : failedBatches) {
        Exception exception = b.getException();
        log.debug("Upload to DB for following entries failed with exception " , exception);
        
        for (Entry<String, List<WriteRequest>> c : b.getUnprocessedItems().entrySet()) {
          log.debug("Failed entries in the table " + c.getKey());
          for (WriteRequest req : c.getValue()) {
            log.debug("Entry, PutRequest: " + req.getPutRequest() + " DeleteRequest: " + req.getDeleteRequest());
          }
        }

        canRetryAllException = (exception == null || (exception instanceof SdkBaseException
            && RetryUtils.isThrottlingException((SdkBaseException) exception)));
      }

      if (canRetryAllException) {
        retryObjects.addAll(uploadedObjects);
      }
      else if (properties.exitViewPopulaterOnDBUploadFailure()) {
        log.error(
            "Upload to DynamoDB failed. Please fix the issue and re-start or disable the property \'exitViewPopulaterOnDBUploadFailure\'");
        System.exit(1);
      }
    }


    log.debug("Number of Pending entries after uploading to Database, New Entries: " + modifiedObjects.size() + " Retry Entries: " + retryObjects.size());
  }

  protected void clearAllCache() {
    advices.clear();
    advisers.clear();
    users.clear();
    funds.clear();
    userFunds.clear();
    userAdvises.clear();
  }

  private UserAdvise userAdvise(UserAdviseEvent userAdviseEvent) {
    UserAdviseCompositeKey compositeKey = new UserAdviseCompositeKey(userAdviseEvent.getAdviseId(), getRequiredUsername(userAdviseEvent.getUsername()),
            userAdviseEvent.getFundName(), userAdviseEvent.getAdviserUsername(), userAdviseEvent.getInvestingMode());
        UserAdvise userAdvise = userAdvises.get(compositeKey);
        
        if (userAdvise != null) {
          return userAdvise;
        }
        
        IssueType issueType = userAdviseEvent.getIssueType();
        Optional<? extends UserAdvise> result;
        if (issueType != null && issueType.equals(IssueType.MutualFund)) {
          result = userMFAdviseRepository.findById(compositeKey);
        }
        else {
          result = userEquityAdviseRepository.findById(compositeKey);
        }
        
        if (result.isPresent()) {
          userAdvise = result.get();
          userAdvises.put(compositeKey, userAdvise);
          return userAdvise;
        }
         
        return null;
       
  }
}