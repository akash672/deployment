package com.wt.api;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonServiceException;
import com.wt.utils.notifications.ErrorNotifier;

import akka.persistence.query.EventEnvelope;

@Component
public class CommonHandlerRetrier {

  private ErrorNotifier errorNotifier;
  private CommonHandler commonHandler;

  @Autowired
  public CommonHandlerRetrier(ErrorNotifier errorNotifier, CommonHandler commonHandler) {
    super();
    this.errorNotifier = errorNotifier;
    this.commonHandler = commonHandler;
  }

  public void handle(EventEnvelope eventEnvelope) throws AmazonServiceException {
    for (int tryCount = 1; tryCount <= 5; tryCount++) {
      try {
        commonHandler.tryToHandle(eventEnvelope);
        commonHandler.uploadDataToDB();
        break;
      } catch (Exception e) {
        errorNotifier.notify(e);
      }
    }
  }

  public void uploadDataToDB() throws IOException {
    commonHandler.uploadDataToDB();
  }
  
  public void clearAllCache() {
    commonHandler.clearAllCache();
  }

}