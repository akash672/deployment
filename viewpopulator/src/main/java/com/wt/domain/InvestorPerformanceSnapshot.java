package com.wt.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@DynamoDBTable(tableName = "InvestorPerformanceSnapshot")
@ToString
public class InvestorPerformanceSnapshot implements Serializable {

  private static final long serialVersionUID = 1L;
  @org.springframework.data.annotation.Id
  @DynamoDBIgnore
  private InvestorPerformanceSnapshotKey investorPerformanceSnapshotKey = new InvestorPerformanceSnapshotKey();
  @DynamoDBAttribute
  private long lastReadSequenceNumber;
  @DynamoDBAttribute
  private PerformanceNumbers userPerformanceNumbers;
  @DynamoDBAttribute
  private Map<String, PerformanceNumbers> userFundToPerformance;
  @DynamoDBAttribute
  private Map<String, PerformanceNumbers> userAdviseToPerformance;
  @DynamoDBAttribute
  private Map<String, Long> userFundToLastReadSequenceNumber;
  @DynamoDBAttribute
  private Map<String, Long> userAdviseToLastReadSequenceNumber;
  
  @DynamoDBHashKey(attributeName = "username")
  public String getUsername() {
    return investorPerformanceSnapshotKey.getUsername();
  }
  
  public void setUsername(String username) {
    investorPerformanceSnapshotKey.setUsername(username);
  }

  @DynamoDBRangeKey(attributeName = "investingMode")
  public String getInvestingMode() {
    return investorPerformanceSnapshotKey.getInvestingMode();
  }

  public void setInvestingMode(String investingMode) {
    investorPerformanceSnapshotKey.setInvestingMode(investingMode);
  }
  
  public InvestorPerformanceSnapshot() {
    super();
    this.lastReadSequenceNumber = 0L;
    this.userPerformanceNumbers = PerformanceNumbers.empty();
    this.userFundToPerformance = new HashMap<String, PerformanceNumbers>();
    this.userAdviseToPerformance = new HashMap<String, PerformanceNumbers>();
    this.userFundToLastReadSequenceNumber = new HashMap<String, Long>();
    this.userAdviseToLastReadSequenceNumber = new HashMap<String, Long>();
  }

  @Builder
  private InvestorPerformanceSnapshot(InvestorPerformanceSnapshotKey investorPerformanceSnapshotKey,
      long lastReadSequenceNumber, PerformanceNumbers userPerformanceNumbers,
      Map<String, PerformanceNumbers> userFundToPerformance, Map<String, PerformanceNumbers> userAdviseToPerformance,
      Map<String, Long> userFundToLastReadSequenceNumber, Map<String, Long> userAdviseToLastReadSequenceNumber) {
    super();
    this.investorPerformanceSnapshotKey = investorPerformanceSnapshotKey;
    this.lastReadSequenceNumber = lastReadSequenceNumber;
    this.userPerformanceNumbers = userPerformanceNumbers;
    this.userFundToPerformance = userFundToPerformance;
    this.userAdviseToPerformance = userAdviseToPerformance;
    this.userFundToLastReadSequenceNumber = userFundToLastReadSequenceNumber;
    this.userAdviseToLastReadSequenceNumber = userAdviseToLastReadSequenceNumber;
  }
  
  

}
