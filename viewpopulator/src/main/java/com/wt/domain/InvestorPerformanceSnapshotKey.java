package com.wt.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InvestorPerformanceSnapshotKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @Getter
  @Setter
  @DynamoDBHashKey(attributeName = "username")
  private String username;
  @Getter
  @Setter
  @DynamoDBRangeKey(attributeName = "investingMode")
  private String investingMode;
  
}
