package com.wt.domain;



import static com.wt.domain.write.events.IssueType.Default;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@DynamoDBDocument
@ToString
@EqualsAndHashCode
public class PerformanceNumbers implements Serializable {

  private static final long serialVersionUID = 1L;
  private double currentValue;
  private double returnPct;
  private double pnl;
  private double realizedPnL;
  private double unrealizedPnL;
  private double cashFromExit;
  private double totalInvestment;
  private double withdrawal;
  private double cashComponent;
  private double totalDividendsReceived;
  private double nbShares;
  private double lastPerformancePrice;
  private double averageEntryPrice;
  private double averageExitPrice;
  private double remainingQuantity;
  private double exitedQuantity;
  private double boughtValue;
  private double exitedValue;
  private double cumulativePaf;
  private double allocation;
  private String wdId;
  private String symbol;
  private String issueType;
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  private Set<Order> issueAdviseOrders;
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  private Set<Order> closeAdviseOrders;
  @DynamoDBTypeConverted(converter = OrderListMarshaller.class)
  private Set<Order> corporateEventOrders;
  @DynamoDBTypeConverted(converter = CumulativePafMapMarshaller.class)
  private Map<Long, Double> dateToCumulativePafMap;

  public static PerformanceNumbers empty() {
    return new PerformanceNumbersBuilder().build();
  }
  
  public PerformanceNumbers(PerformanceNumbers updatedPerformanceNumbers) {
    currentValue = updatedPerformanceNumbers.getCurrentValue(); 
    returnPct = updatedPerformanceNumbers.getReturnPct(); 
    pnl = updatedPerformanceNumbers.getPnl();
    realizedPnL = updatedPerformanceNumbers.getRealizedPnL(); 
    unrealizedPnL =updatedPerformanceNumbers.getUnrealizedPnL();
    cashFromExit = updatedPerformanceNumbers.getCashFromExit();
    totalInvestment = updatedPerformanceNumbers.getTotalInvestment(); 
    withdrawal = updatedPerformanceNumbers.getWithdrawal();
    cashComponent = updatedPerformanceNumbers.getCashComponent(); 
    totalDividendsReceived = updatedPerformanceNumbers.getTotalDividendsReceived();
    nbShares = updatedPerformanceNumbers.getNbShares();
    lastPerformancePrice = updatedPerformanceNumbers.getLastPerformancePrice();
    averageEntryPrice = updatedPerformanceNumbers.getAverageEntryPrice(); 
    averageExitPrice = updatedPerformanceNumbers.getAverageExitPrice();
    remainingQuantity = updatedPerformanceNumbers.getRemainingQuantity();
    exitedQuantity = updatedPerformanceNumbers.getExitedQuantity();
    exitedValue = updatedPerformanceNumbers.getExitedValue();
    boughtValue = updatedPerformanceNumbers.getBoughtValue();
    cumulativePaf = updatedPerformanceNumbers.getCumulativePaf(); 
    allocation = updatedPerformanceNumbers.getAllocation(); 
    wdId = updatedPerformanceNumbers.getWdId();
    symbol = updatedPerformanceNumbers.getSymbol();
    issueType = updatedPerformanceNumbers.getIssueType();
    issueAdviseOrders = updatedPerformanceNumbers.getIssueAdviseOrders();
    closeAdviseOrders = updatedPerformanceNumbers.getCloseAdviseOrders();
    dateToCumulativePafMap = updatedPerformanceNumbers.getDateToCumulativePafMap();
  }
  
  @DynamoDBDocument
  @ToString
  @Getter
  public static class PerformanceNumbersBuilder {
    private double currentValue;
    private double returnPct;
    private double pnl;
    private double realizedPnL;
    private double unrealizedPnL;
    private double cashFromExit;
    private double totalInvestment;
    private double withdrawal;
    private double cashComponent;
    double totalDividendsReceived;
    private double nbShares;
    private double lastPerformancePrice;
    private double averageEntryPrice;
    private double averageExitPrice;
    private double remainingQuantity;
    private double exitedQuantity;
    private double exitedValue;
    private double boughtValue;
    private double cumulativePaf;
    private double allocation;
    private String wdId;
    private String symbol;
    private String issueType;
    private Set<Order> issueAdviseOrders;
    private Set<Order> closeAdviseOrders;
    private Set<Order> corporateEventOrders;
    private Map<Long, Double> dateToCumulativePafMap;

    public PerformanceNumbersBuilder(PerformanceNumbers performanceNumbers) {
      super();
      this.currentValue = performanceNumbers.getCurrentValue();
      this.returnPct = performanceNumbers.getReturnPct();
      this.pnl = performanceNumbers.getPnl();
      this.realizedPnL = performanceNumbers.getRealizedPnL();
      this.unrealizedPnL = performanceNumbers.getUnrealizedPnL();
      this.cashFromExit = performanceNumbers.getCashFromExit();
      this.totalInvestment = performanceNumbers.getTotalInvestment();
      this.totalDividendsReceived = performanceNumbers.getTotalDividendsReceived();
      this.nbShares = performanceNumbers.getNbShares();
      this.averageEntryPrice = performanceNumbers.getAverageEntryPrice();
      this.averageExitPrice = performanceNumbers.getAverageExitPrice();
      this.wdId = performanceNumbers.getWdId();
      this.symbol = performanceNumbers.getSymbol();
      this.issueType = performanceNumbers.getIssueType();
      this.issueAdviseOrders = performanceNumbers.getIssueAdviseOrders();
      this.closeAdviseOrders = performanceNumbers.getCloseAdviseOrders();
      this.corporateEventOrders = performanceNumbers.getCorporateEventOrders();
      this.dateToCumulativePafMap = performanceNumbers.getDateToCumulativePafMap();
    }
    
    public PerformanceNumbersBuilder(double currentValue, double returnPct, double pnl, double realizedPnL,
        double unrealizedPnL) {
      super();
      this.currentValue = currentValue;
      this.returnPct = returnPct;
      this.pnl = pnl;
      this.realizedPnL = realizedPnL;
      this.unrealizedPnL = unrealizedPnL;
      this.cashFromExit = 0.;
      this.totalInvestment = 0.;
      this.totalDividendsReceived = 0.;
      this.nbShares = 0.;
      this.averageEntryPrice = 0.;
      this.averageExitPrice = 0.;
      this.wdId = "";
      this.symbol = "";
      this.issueType = Default.getIssueType();
      this.issueAdviseOrders = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      this.closeAdviseOrders = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      this.corporateEventOrders = new CopyOnWriteArraySet<Order>();
      this.dateToCumulativePafMap = new TreeMap<Long, Double>();
    }
    
    public PerformanceNumbersBuilder() {
      super();
      this.currentValue = 0D;
      this.returnPct = 0D;
      this.pnl = 0D;
      this.realizedPnL = 0D;
      this.unrealizedPnL = 0D;
      this.cashFromExit = 0D;
      this.totalInvestment = 0.;
      this.withdrawal = 0.;
      this.totalDividendsReceived = 0.;
      this.cashComponent = 0.;
      this.nbShares = 0.;
      this.averageEntryPrice = 0.;
      this.averageExitPrice = 0.;
      this.allocation = 0.;
      this.wdId = "";
      this.symbol = "";
      this.issueType = Default.getIssueType();
      this.issueAdviseOrders = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      this.closeAdviseOrders = new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp));
      this.corporateEventOrders = new CopyOnWriteArraySet<Order>();
      this.dateToCumulativePafMap = new TreeMap<Long, Double>();
    }
    
    public PerformanceNumbersBuilder withIssueAdviseOrders(Set<Order> orders) {
      this.issueAdviseOrders = orders;
      return this;
    }
    
    public PerformanceNumbersBuilder withCloseAdviseOrders(Set<Order> orders) {
      this.closeAdviseOrders = orders;
      return this;
    }
    
    public PerformanceNumbersBuilder withCorporateEventOrders(Set<Order> orders) {
      this.corporateEventOrders = orders;
      return this;
    }
    
    public PerformanceNumbersBuilder withDateToCumulativePafMap(Map<Long, Double> dateToCumulativePafMap) {
      this.dateToCumulativePafMap = dateToCumulativePafMap;
      return this;
    }
    
    public PerformanceNumbersBuilder withCashFromExit(double cashFromExit) {
      this.cashFromExit = cashFromExit;
      return this;
    }

    public PerformanceNumbersBuilder withCurrentValue(double currentValue) {
      this.currentValue = currentValue;
      return this;
    }
    
    public PerformanceNumbersBuilder withTotalInvestment(double totalInvestment) {
      this.totalInvestment = totalInvestment;
      return this;
    }

    public PerformanceNumbersBuilder withTotalDividendsReceived(double totalDividendsReceived) {
      this.totalDividendsReceived = totalDividendsReceived;
      return this;
    }
    
    public PerformanceNumbersBuilder withWithdrawal(double withdrawal) {
      this.withdrawal = withdrawal;
      return this;
    }

    public PerformanceNumbersBuilder withCashComponent(double cashComponent) {
      this.cashComponent = cashComponent;
      return this;
    }

    public PerformanceNumbersBuilder withLastPerformancePrice(double lastPerformancePrice) {
      this.lastPerformancePrice = lastPerformancePrice;
      return this;
    }
    
    public PerformanceNumbersBuilder withNbShares(double nbShares) {
      this.nbShares = nbShares;
      return this;
    }

    public PerformanceNumbersBuilder withAverageEntryPrice(double averageEntryPrice) {
      this.averageEntryPrice = averageEntryPrice;
      return this;
    }
    
    public PerformanceNumbersBuilder withAverageExitPrice(double averageExitPrice) {
      this.averageExitPrice = averageExitPrice;
      return this;
    }
    
    public PerformanceNumbersBuilder withRemainingQuantity(double remainingQuantity) {
      this.remainingQuantity = remainingQuantity;
      return this;
    }

    public PerformanceNumbersBuilder withExitedValue(double exitedValue) {
      this.exitedValue = exitedValue;
      return this;
    }
    
    public PerformanceNumbersBuilder withExitedQuantity(double exitedQuantity) {
      this.exitedQuantity = exitedQuantity;
      return this;
    }
    
    public PerformanceNumbersBuilder withBoughtValue(double boughtValue) {
      this.boughtValue = boughtValue;
      return this;
    }
    
    public PerformanceNumbersBuilder withCumulativePaf(double cumulativePaf) {
      this.cumulativePaf = cumulativePaf;
      return this;
    }

    public PerformanceNumbersBuilder withAllocation(double allocation) {
      this.allocation = allocation;
      return this;
    }

    public PerformanceNumbersBuilder withWdId(String wdId) {
      this.wdId = wdId;
      return this;
    }

    public PerformanceNumbersBuilder withSymbol(String symbol) {
      this.symbol = symbol;
      return this;
    }

    public PerformanceNumbersBuilder withIssueType(String issueType) {
      this.issueType = issueType;
      return this;
    }

    public PerformanceNumbers build() {
      return new PerformanceNumbers(currentValue, returnPct, pnl, realizedPnL, unrealizedPnL, cashFromExit,
          totalInvestment, withdrawal, cashComponent, totalDividendsReceived, nbShares, lastPerformancePrice, averageEntryPrice, averageExitPrice, 
          remainingQuantity, exitedQuantity, boughtValue, exitedValue, cumulativePaf, allocation, wdId, symbol, issueType, issueAdviseOrders, 
          closeAdviseOrders, corporateEventOrders, dateToCumulativePafMap);
    }
  }

}
