package com.not.wt.pkg.to.override.main.config;

import static com.google.common.collect.Lists.newArrayList;
import static com.typesafe.config.ConfigFactory.load;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.mockito.Mockito;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import com.amazonaws.services.dynamodbv2.local.shared.access.AmazonDynamoDBLocal;
import com.amazonaws.services.sns.AmazonSNS;
import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.typesafe.config.Config;
import com.wt.benchmarks.BenchmarkSource;
import com.wt.benchmarks.NseBenchmarkSource;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.repo.EquityInstrumentRepository;
import com.wt.instruments.BSEUniverseSource;
import com.wt.instruments.EquityUniverseSource;
import com.wt.instruments.NSEUniverseSource;
import com.wt.utils.EventStoreConnector;
import com.wt.utils.JournalProvider;
import com.wt.utils.JournalProviderTest;
import com.wt.utils.NotificationFacade;
import com.wt.utils.OTPUtils;
import com.wt.utils.akka.SpringExtension;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorSystem;
import eventstore.ProjectionsClient;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan("com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:test-environment.properties")
public class TestAppConfiguration {
  
  @Autowired
  private ApplicationContext applicationContext;
  @SuppressWarnings("unused")
  @Autowired
  private  WDActorSelections actorSelections;
  
  @Autowired
  public WDProperties wdProperties;
  
  @Lazy
  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() {
    Config conf = load("test-application.conf");
    ActorSystem system = ActorSystem.create("WD", conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }
  
  @Bean
  public List<EquityUniverseSource> securitiesUniverseSources() {
    return newArrayList(nse());
  }
  
  @Bean
  public AWSCredentials amazonAWSCredentials() {
    return Mockito.mock(AWSCredentials.class);
  }
  
  @Bean
  public EquityUniverseSource nse() {
    return new NSEUniverseSource("file://src/test/resources/security.txt");
  }

  @Bean
  public EquityUniverseSource bse() {
    return new BSEUniverseSource("file://src/test/resources/scrip.zip");
  }

  @Bean
  public List<BenchmarkSource> benchmarkSources() {
    return newArrayList(nseBenchmarks());
  }

  @Bean
  public BenchmarkSource nseBenchmarks() {
    return new NseBenchmarkSource("https://www.nseindia.com/content/indices/");
  }

  @Bean
  public EquityInstrumentRepository equityInstrumentRepository() {
    EquityInstrumentRepository equityInstrumentRepository = applicationContext.getBean(EquityInstrumentRepository.class);
    EquityInstrumentRepository mockEquityInstrumentRepository = spy(equityInstrumentRepository);
    doNothing().when(mockEquityInstrumentRepository).deleteAll();

    return mockEquityInstrumentRepository;
  }

  public AmazonDynamoDBLocal amazonDynamoDBLocal() {
    return DynamoDBEmbedded.create();
  }

  @Bean
  public AmazonDynamoDB amazonDynamoDB() {
    return amazonDynamoDBLocal().amazonDynamoDB();
  }

  @Bean
  public NotificationFacade NotificationFacade() {
    return mock(NotificationFacade.class);
  }

  @Bean 
  public BroadCastManager broadCastManager() throws NumberFormatException, IOException {
    return mock(BroadCastManager.class);
  }

  @Bean
  public CtclBroadcast ctclBroadcast() {
    return mock(CtclBroadcast.class);
  }
  
  @Bean(name = "InteractiveManager")
  @Lazy
  public InteractiveManager interactiveManager() throws IOException {
    return Mockito.mock(InteractiveManager.class);
  }
  
  @Bean
  public OTPUtils OTPUtils() {
    return mock(OTPUtils.class);
  }

  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("test-environment.properties"));
    return properties;
  }
  
  @Bean(name="JournalProvider")
  public JournalProvider journalProvider()
  {
    JournalProviderTest journalProviderProduction = new JournalProviderTest(actorSystem());
    return journalProviderProduction;
  }

  @Bean(name="EventStoreConnector")
  public EventStoreConnector eventStoreConnector()
  {
    return mock(EventStoreConnector.class);
  }
  
  @Bean(name="WDProjections")
  public ProjectionsClient projectionFromWDService() throws IOException{
      return Mockito.mock(ProjectionsClient.class);
  }
  
  @Bean(name = "dataSource")
  public DataSource dataSource() throws IOException {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    
    dataSource.setDriverClassName(wdProperties.rdsDriverClass());
    dataSource.setUsername(wdProperties.rdsUsername());
    dataSource.setPassword(wdProperties.rdsPassword());
    dataSource.setUrl(wdProperties.rdsUrl());
    return dataSource;
  }

  @Bean(name = "SNSClient")
  public AmazonSNS snsClientCreation() {
    return Mockito.mock(AmazonSNS.class);
  }

  @Bean(name = "SNSClientForSMS")
  public AmazonSNS snsClientSMSRegionCreation() {
    return Mockito.mock(AmazonSNS.class);
  }
}