package com.wt.utils;

import static com.google.common.collect.Sets.newHashSet;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.approvaltests.Approvals;
import org.assertj.core.util.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.domain.Adviser;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.EquityInstrument;
import com.wt.domain.repo.FundAdviseRepository;
import com.wt.domain.repo.FundAdviserRepository;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.write.commands.UpdateEQInstruments;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.stream.ActorMaterializer;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class AdviseUploaderShould {
  @Autowired
  private FundAdviseRepository adviseRepository;
  @Autowired
  private FundAdviserRepository adviserRepository;
  @Autowired
  private FundRepository fundRepository;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private ActorSystem system;
  @Autowired
  private ViewPopulater viewPopulater;

  @Before
  public void setupDB() throws Exception {
    Files.delete(new File("build/journal"));
    Files.delete(new File("build/snapshots"));
    dbTest.createSchema();
  }

  @Test
  public void populate_all_adviser_views() throws Exception {
    AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TestAppConfiguration.class);
    final ActorRef adviserManagerActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"),
        "adviser-aggregator");
    final ActorRef universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    ActorMaterializer materializer = ActorMaterializer.create(system);
    final WDActorSelections actorSelection = (WDActorSelections) ctx.getBean(WDActorSelections.class);
    
    setupUniverse(universeRootActor);

    AdviseUploader adviseUploader = new AdviseUploader();
    adviseUploader.upload(adviserManagerActor,
        "Swapnil_Swapnil-Joshi_Swapnil@bpwealth.com_TechnoFunda_27-12-2013_31-12-2079_fundDescription_7500_21500_HIGH.xlsx",
        "swapnil", actorSelection.universeRootActor());

    viewPopulater.blockingPopulateView(system, materializer);
    Iterable<Adviser> advisers = adviserRepository.findAll();
    assertThat(advisers).hasSize(1);
    assertThat(advisers.iterator().next().getAdviserUsername()).isEqualTo("swapnil");
    Iterable<CurrentFundDb> funds = fundRepository.findAll();
    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo("TechnoFunda");

    StringBuilder approvedOutput = new StringBuilder();
    adviseRepository.findAll().forEach(advise -> approvedOutput.append(advise + "\n"));
    Approvals.verify(approvedOutput);
    ctx.close();
  }

  @SuppressWarnings("unused")
  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    Terminated result = terminate.result(Duration.Inf(), new CanAwait() {
    });
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void setupUniverse(ActorRef universeRootActor) throws Exception {
    Set<EquityInstrument> today = newHashSet(today());
    askAndWaitForResult(universeRootActor, new UpdateEQInstruments(today));
  }

  private List<EquityInstrument> today() {
    // TODO Auto-generated method stub
    List<EquityInstrument> allEQInstruments = new ArrayList<EquityInstrument>();
    allEQInstruments.add(new EquityInstrument("ISIN1-NSE-EQ", "14732", "BAJAJ-AUTO", "Bajaj","EQ"));
    allEQInstruments.add(new EquityInstrument("ISIN2-NSE-EQ", "13986", "BHARTIARTL", "Bharti Airtel", "EQ"));
    allEQInstruments.add(new EquityInstrument("ISIN3-NSE-EQ", "3045", "CAIRN", "Cairn India", "EQ"));
    allEQInstruments.add(new EquityInstrument("ISIN4-NSE-EQ", "1660", "DLF", "DLF India", "EQ"));
    allEQInstruments.add(new EquityInstrument("ISIN5-NSE-EQ", "16669", "ITC", "ITC" ,"EQ"));
    allEQInstruments.add(new EquityInstrument("ISIN6-NSE-EQ", "3351", "SBIN", "SBI", "EQ"));
    allEQInstruments.add(new EquityInstrument("ISIN7-NSE-EQ", "10604", "SUNPHARMA", "Sun Pharma", "EQ"));

    return allEQInstruments;
  }


}
