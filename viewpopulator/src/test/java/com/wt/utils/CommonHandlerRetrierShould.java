package com.wt.utils;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.api.CommonHandler;
import com.wt.api.CommonHandlerRetrier;
import com.wt.utils.notifications.ErrorNotifier;

import akka.persistence.query.EventEnvelope;
import akka.persistence.query.Offset;

@RunWith(MockitoJUnitRunner.class)
public class CommonHandlerRetrierShould {
  @Mock
  private CommonHandler commonHandler;
  @Mock
  private ErrorNotifier errorNotifier;
  
  @InjectMocks
  private CommonHandlerRetrier commonHandlerRetrier = new CommonHandlerRetrier(errorNotifier, commonHandler);

  @Ignore
  @Test public void 
  notify_error_notifier_on_exception() throws Exception {
    EventEnvelope dummyEventEnvelope = new EventEnvelope(Offset.sequence(1), "dummy_id", 0, "dummy_object");

    RuntimeException dummyException = new RuntimeException("dummy exception");
    doThrow(dummyException).when(commonHandler).tryToHandle(dummyEventEnvelope);

    commonHandlerRetrier.handle(dummyEventEnvelope);

    verify(errorNotifier, times(3)).notify(dummyException);
  }
  
  @Test public void 
  does_not_notify_error_notifier_on_no_exception() throws Exception {
    EventEnvelope dummyEventEnvelope = new EventEnvelope(Offset.sequence(1), "dummy_id", 0, "dummy_object");

    RuntimeException dummyException = new RuntimeException("dummy exception");
    doNothing().when(commonHandler).tryToHandle(dummyEventEnvelope);

    commonHandlerRetrier.handle(dummyEventEnvelope);

    verify(commonHandler, times(1)).tryToHandle(dummyEventEnvelope);;
    verify(errorNotifier, never()).notify(dummyException);
  }
}
