package com.wt.utils;

import static akka.pattern.Patterns.ask;
import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.util.Collections.sort;
import static org.assertj.core.api.Assertions.assertThat;
import static scala.concurrent.Await.result;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.domain.Adviser;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.FundPerformance;
import com.wt.domain.repo.FundAdviserRepository;
import com.wt.domain.repo.FundPerformanceRepository;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.write.commands.CalculateHistoricalFundPerformance;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.utils.akka.ActorResultWaiter;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.stream.ActorMaterializer;
import commons.Retry;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@Ignore
public class WDPartialExitPerformanceShould {
  private static final String ADVISER_IDENTITY_ID = "swapnil";
  private static final String TICKER1 = "123-NSE-EQ";
  private static final int TOKEN_1 = 123;
  private static final String SYMBOL_1 = "RIL";
  private static final String TICKER2 = "456-NSE-EQ";
  private static final int TOKEN_2 = 456;
  private static final String SYMBOL_2 = "TATA";
  private static final String TICKER3 = "789-NSE-EQ";
  private static final int TOKEN_3 = 789;
  private static final String SYMBOL_3 = "INFY";
  @Autowired
  private FundAdviserRepository adviserRepository;
  @Autowired
  private FundPerformanceRepository fundPerformanceRepository;
  @Autowired
  private FundRepository fundRepository;
  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private ActorSystem system;
  
  private ActorRef universeRootActor;

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Before
  public void setupDB() throws Exception {
    Files.delete(new File("build/journal"));
    Files.delete(new File("build/snapshots"));
    dbTest.createSchema();
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());
  }

  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      FileUtils.deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Rule
  public Retry retry = new Retry(5);
  
  @Test
  public void calculate_fund_performance() throws Exception {
    ActorMaterializer materializer = ActorMaterializer.create(system);

    RegisterAdviserFromUserPool adviser = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi", "swapnil@bpwealth.com",
        "32publicId", "Fort Capital", "");
    playAllEvents(system, materializer, adviser, "WeeklyPicks");

    viewPopulater.blockingPopulateView(system, materializer);
    Iterable<Adviser> advisers = adviserRepository.findAll();
    assertThat(advisers).hasSize(1);
    assertThat(advisers.iterator().next().getAdviserUsername()).isEqualTo("swapnil");
    Iterable<CurrentFundDb> funds = fundRepository.findAll();
    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo("WeeklyPicks");

    List<FundPerformance> allPerformance = new ArrayList<>();
    StringBuilder output = new StringBuilder();
    Iterable<FundPerformance> findAll = fundPerformanceRepository.findAll();
    findAll.spliterator().forEachRemaining(fundPerformance -> allPerformance.add(fundPerformance));

    sort(allPerformance, (o1, o2) -> Long.compare(o1.getDate(), o2.getDate()));

    for (FundPerformance fundPerformance2 : allPerformance) {
      output.append(fundPerformance2 + "\n");
    }
//    verify(output);
  }

  public void playAllEvents(ActorSystem system, ActorMaterializer materializer, RegisterAdviserFromUserPool adviser,
      String fundName) throws Exception {
    final ActorRef adviserManagerActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"),
        "adviser-aggregator");

    sendPriceUpdate(universeRootActor, new InstrumentPrice(102, eodDateFromExcelDate("06/01/2015"), "123-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(210, eodDateFromExcelDate("06/01/2015"), "456-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(155, eodDateFromExcelDate("06/01/2015"), "789-NSE-EQ"), fundName);


    sendPriceUpdate(universeRootActor, new InstrumentPrice(105, eodDateFromExcelDate("03/01/2015"), "123-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(210, eodDateFromExcelDate("03/01/2015"), "456-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(150, eodDateFromExcelDate("03/01/2015"), "789-NSE-EQ"), fundName);

    sendPriceUpdate(universeRootActor, new InstrumentPrice(102, eodDateFromExcelDate("04/01/2015"), "123-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(210, eodDateFromExcelDate("04/01/2015"), "456-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(150, eodDateFromExcelDate("04/01/2015"), "789-NSE-EQ"), fundName);

    sendPriceUpdate(universeRootActor, new InstrumentPrice(102, eodDateFromExcelDate("05/01/2015"), "123-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(210, eodDateFromExcelDate("05/01/2015"), "456-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(155, eodDateFromExcelDate("05/01/2015"), "789-NSE-EQ"), fundName);
    
    sendPriceUpdate(universeRootActor, new InstrumentPrice(105, eodDateFromExcelDate("02/01/2015"), "123-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(200, eodDateFromExcelDate("02/01/2015"), "456-NSE-EQ"), fundName);
    sendPriceUpdate(universeRootActor, new InstrumentPrice(150, eodDateFromExcelDate("02/01/2015"), "789-NSE-EQ"), fundName);


    askAndWaitForResult(adviserManagerActor,
        new RegisterAdviserFromUserPool(adviser.getAdviserUsername(), adviser.getName(), adviser.getEmail(),
            adviser.getPublicARN(), adviser.getCompany(), adviser.getPhoneNumber()));
    askAndWaitForResult(adviserManagerActor,
        new FloatFund(ADVISER_IDENTITY_ID, fundName, eodDateFromExcelDate("01/01/2015"),
            eodDateFromExcelDate("31/12/2079"), fundName + "-Description", "Theme", 6500, 20500, "Medium"));

    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "1", "123-NSE-EQ", 10., 100.0,
            eodDateFromExcelDate("01/01/2015"), 120.0, eodDateFromExcelDate("20/01/2015"),
            eodDateFromExcelDate("01/01/2015")));
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "2", "456-NSE-EQ", 40., 200.0,
            eodDateFromExcelDate("02/01/2015"), 220.0, eodDateFromExcelDate("20/01/2015"),
            eodDateFromExcelDate("02/01/2015")));
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "3", "789-NSE-EQ", 25., 150.0,
            eodDateFromExcelDate("03/01/2015"), 200.0, eodDateFromExcelDate("20/01/2015"),
            eodDateFromExcelDate("03/01/2015")));

    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "1", 10., eodDateFromExcelDate("06/01/2015"), 103));
    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "2", 40., eodDateFromExcelDate("05/01/2015"), 205));
    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "3", 10., eodDateFromExcelDate("06/01/2015"), 160));
    askAndWaitForResult(adviserManagerActor, new CalculateHistoricalFundPerformance());
  }

  protected void sendPriceUpdate(final ActorRef to, InstrumentPrice event, String fund) throws Exception {
    Future<Object> maybeRegistered = ask(to, event, 10 * 1000L);
    result(maybeRegistered, ActorResultWaiter.TIMEOUT);
  }

}