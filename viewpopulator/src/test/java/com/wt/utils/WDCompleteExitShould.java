package com.wt.utils;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.util.Collections.sort;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Files.currentFolder;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.domain.Adviser;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.EquityInstrument;
import com.wt.domain.FundPerformance;
import com.wt.domain.repo.FundAdviserRepository;
import com.wt.domain.repo.FundPerformanceRepository;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.write.commands.CalculateHistoricalFundPerformance;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.UpdateEQInstruments;
import com.wt.universe.util.PriceExtractor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.stream.ActorMaterializer;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@Ignore
public class WDCompleteExitShould {
  private static final String ADVISER_IDENTITY_ID = "swapnil";
  @Autowired
  private FundAdviserRepository adviserRepository;
  @Autowired
  private FundPerformanceRepository fundPerformanceRepository;
  @Autowired
  private FundRepository fundRepository;
  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private ActorSystem system;

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Before
  public void setupDB() throws Exception {
    dbTest.createSchema();
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      FileUtils.deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @SuppressWarnings("unused")
  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    Terminated result = terminate.result(Duration.Inf(), new CanAwait() {
    });
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      FileUtils.deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void calculate_complete_exit_fund_performance() throws Exception {
    ActorMaterializer materializer = ActorMaterializer.create(system);
    RegisterAdviserFromUserPool adviser = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi", "swapnil@bpwealth.com", 
        "32publicId", "Fort Capital", "");
    playAllEvents(system, materializer, adviser, "WeeklyPicks", currentFolder() + "/src/test/resources/BhaavCopy");

    viewPopulater.blockingPopulateView(system, materializer);
    Iterable<Adviser> advisers = adviserRepository.findAll();
    assertThat(advisers).hasSize(1);
    assertThat(advisers.iterator().next().getAdviserUsername()).isEqualTo("swapnil");
    Iterable<CurrentFundDb> funds = fundRepository.findAll();
    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo("WeeklyPicks");

    List<FundPerformance> allPerformance = new ArrayList<>();
    StringBuilder output = new StringBuilder();
    Iterable<FundPerformance> findAll = fundPerformanceRepository.findAll();
    findAll.spliterator().forEachRemaining(fundPerformance -> allPerformance.add(fundPerformance));
    sort(allPerformance, (o1, o2) -> Long.compare(o1.getDate(), o2.getDate()));
    for (FundPerformance fundPerformance2 : allPerformance) {
      output.append(fundPerformance2 + System.lineSeparator());
    }
//    Approvals.verify(output);
  }

  public void playAllEvents(ActorSystem system, ActorMaterializer materializer, RegisterAdviserFromUserPool adviser,
      String fundName, String historicalFolderName) throws Exception {
    final ActorRef adviserManagerActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"),
        "adviser-aggregator");
    final ActorRef universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    HashSet<EquityInstrument> equityInstruments = new HashSet<EquityInstrument>();
    equityInstruments.add(new EquityInstrument("ISIN1-NSE-EQ", "11", "SBIN", "State Bank of India", "EQ"));
    equityInstruments.add(new EquityInstrument("ISIN2-NSE-EQ", "22", "BHARTIARTL", "Bharti Airtel", "EQ"));
    equityInstruments.add(new EquityInstrument("ISIN3-NSE-EQ", "33", "DLF", "DLF India", "EQ"));
    askAndWaitForResult(universeRootActor, new UpdateEQInstruments(equityInstruments));

    PriceExtractor extract = new PriceExtractor();
    DateFormat indiaFormat = new SimpleDateFormat("dd-MM-yyyy");
    indiaFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
    Date startDate = indiaFormat.parse("01-01-2014");
    Date endDate = indiaFormat.parse("09-01-2014");
    extract.readFilesFromFolder(historicalFolderName, startDate, endDate, "NSE", universeRootActor);
    environmentVariables.set("WT_MARKET_OPEN", "true");
    askAndWaitForResult(adviserManagerActor, registerAdviser(adviser));
    askAndWaitForResult(adviserManagerActor,
        new FloatFund(ADVISER_IDENTITY_ID, fundName, eodDateFromExcelDate("28/02/2013"),
            eodDateFromExcelDate("31/12/2079"), fundName + "-Description", "Theme", 6500, 20500, "Medium"));
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "1", "11-NSE-EQ", 10., 100.0, eodDateFromExcelDate("01/01/2014"),
            103.0, eodDateFromExcelDate("02/01/2014"), eodDateFromExcelDate("01/01/2014")));
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "2", "22-NSE-EQ", 40., 200.0, eodDateFromExcelDate("03/01/2014"),
            205.0, eodDateFromExcelDate("06/01/2014"), eodDateFromExcelDate("03/01/2014")));
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "3", "33-NSE-EQ", 25., 150.0, eodDateFromExcelDate("06/01/2014"),
            160.0, eodDateFromExcelDate("07/01/2014"), eodDateFromExcelDate("06/01/2014")));
    viewPopulater.blockingPopulateView(system, materializer);
    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "1", 10., eodDateFromExcelDate("02/01/2014"), 103.));
    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "2", 40., eodDateFromExcelDate("06/01/2014"), 205.));
    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "3", 25., eodDateFromExcelDate("07/01/2014"), 160.));
    askAndWaitForResult(adviserManagerActor, new CalculateHistoricalFundPerformance());
  }

  private RegisterAdviserFromUserPool registerAdviser(RegisterAdviserFromUserPool adviser) {
    return new RegisterAdviserFromUserPool(adviser.getAdviserUsername(), adviser.getName(), adviser.getEmail(),
        adviser.getPublicARN(), adviser.getCompany(), adviser.getPhoneNumber());
  }
}
