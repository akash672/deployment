package com.wt.utils;

import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.UNSUBSCRIPTION;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import java.io.File;
import java.util.Optional;

import org.assertj.core.util.Files;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.BrokerName;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.CurrentUserDb;
import com.wt.domain.InvestingMode;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.RiskLevel;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.repo.UserEquityAdviseRepository;
import com.wt.domain.repo.UserFundRepository;
import com.wt.domain.repo.UserRepository;
import com.wt.domain.write.RealTimeInstrumentPrice;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.CloseFund;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.FundActionApproved;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.SubscribeToFund;
import com.wt.domain.write.commands.UnsubscribeToFund;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateWealthCode;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.Result;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import lombok.extern.log4j.Log4j;

@Log4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(properties="dbUploadRate.in.milliseconds=1")
public class CommonHandlerShould {
  private static final RegisterAdviserFromUserPool ADVISER = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi",
      "swapnil@bpwealth.com", "123publicId", "Wealth Tech", "");
  private static final String ADVISER_IDENTITY_ID = "swapnil";
  private static final String FUND_NAME = "WeeklyPicks";
  private static final String TICKER1 = "IN1-NSE-EQ";
  private static final int TOKEN_1 = 123;
  private static final String SYMBOL_1 = "ACC";
  private static final String TICKER2 = "IN2-NSE-EQ";
  private static final int TOKEN_2 = 456;
  private static final String SYMBOL_2 = "SBIN";
  private static final String TICKER3 = "IN3-NSE-EQ";
  private static final int TOKEN_3 = 789;
  private static final String SYMBOL_3 = "ICICIBANK";
  private static final double CURRENT_MARKET_PRICE = 1040.;
  private static final long STARTDATE = 0;
  private static final long ENDDATE = 0;
  private static final String FUND_DESCRIPTION = "";
  private static final String FUND_THEME = "Actively Managed";
  private static final double FUND_MIN_SIP = 1000;
  private static final double FUND_MIN_LUMPSUM = 20000;
  private static final String FUND_RISK_LEVEL = RiskLevel.LOW.name();
  private static final String ADVISEID1 = "1a";
  private static final double ALLOCATION1 = 10;
  private static final double ENTRYPRICE = 50.;
  private static final long ENTRYTIME = 1L;
  private static final double EXITPRICE = 70.;
  private static final long ISSUETIME1 = 1L;
  private static final long EXITTIME = 0L;
  private static final String INVESTOR_NAME = "Investor Name";
  private static final String SUBSCRIPTION_USER_EMAIL = "email@gmail.com";
  private static final String LUMPSUMAMOUNT = "20000";
  private static final String SIPAMOUNT = "10";
  private static final String USERNAME = "subscription_user_name";
  private static final String INVESTINGMODE = "VIRTUAL";
  private static final String SIPDATE = "6L";
  private static final String SUBSCRIPTION_WEALTHCODE = "1234";
  private static final String BASKETORDERID_FOR_TEST = "baseketOrderIs12345";
  private static final Object COUNTOFPERSISTEDUSERFUNDAFTERFUNDEXIT = 0L;
  private static final long COUNTOFPERSISTEDUSERFUNDS = 1L;
  private static final long COUNTOFPERSISTEDUSERADVISES = 1L;
  private static final long COUNTOFPERSISTEDFUNDS = 0;
  private static final String CLIENT_CODE = "EMP919191";
  private static final String PANCARD = "ARNPC9761A";
  private final static BrokerName BROKER_COMPANY_NAME = BrokerName.BPWEALTH;
  private static final String USER_PHONENUMBER = "8080621831";

  @Autowired
  private FundRepository fundRepository;
  @Autowired
  private UserFundRepository userFundRepository;
  @Autowired
  private UserEquityAdviseRepository userEquityAdviseRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private ActorSystem system;
  @Autowired
  private BroadCastManager broadcastManager;
  @Autowired
  private CtclBroadcast ctclBroadcast;
  private ActorRef adviserManagerActor;
  private ActorRef userRootActor;
  private ActorRef universeRootActor;
  @SuppressWarnings("unused")
  private ActorRef activeInstrumentTrackerActor;
  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Before
  public void setupDB() throws Exception {
    Files.delete(new File("build/journal"));
    Files.delete(new File("build/snapshots"));
    log.info("Creating schema for Common Handler ...");
    dbTest.createSchema();
    doNothing()
    .when(ctclBroadcast)
    .addSubscription(Mockito.anyString(), Mockito.anyString());
    
    doNothing()
    .when(ctclBroadcast)
    .removeSubscription(Mockito.anyString(), Mockito.anyString());
    
    doReturn(true)
    .when(broadcastManager)
    .isConnected();
    environmentVariables.set("WT_MARKET_OPEN", "true");
    environmentVariables.set("WT_BROADCAST_OPEN", "true");
    userRootActor = system.actorOf(SpringExtProvider.get(system).props("UserRootActor"), "user-aggregator");
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    adviserManagerActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"),
        "adviser-aggregator");
    activeInstrumentTrackerActor = system.actorOf(SpringExtProvider.get(system).props("ActiveInstrumentTrackerActor"),
        "active-instrument-tracker");
    system.actorOf(SpringExtProvider.get(system).props("EquityOrderRootActor"), "equity-order-aggregator");
    
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(100d, DateUtils.currentTimeInMillis(), "IN1-NSE-EQ"), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(100d, DateUtils.currentTimeInMillis(), "IN2-NSE-EQ"), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(100d, DateUtils.currentTimeInMillis(), "IN3-NSE-EQ"), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());

  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());
  }

  @Test
  public void testAllocationLogic() throws Exception {
    ActorMaterializer materializer = ActorMaterializer.create(system);
    RegisterAdviserFromUserPool adviser = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi", "swapnil@bpwealth.com", "32publicId", "fort Capital", "");
    playAllEvents(system, materializer, adviser, "WeeklyPicks");
    viewPopulater.blockingPopulateView(system, materializer);

    Iterable<CurrentFundDb> funds = fundRepository.findAll();

    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo("WeeklyPicks");
    Thread.sleep(2000);

    CurrentFundDb fund = funds.iterator().next();
    assertThat(isZero(fund.getTickersWithComposition().size() - 3));
    assertThat(fund.getTickersWithComposition().get("1").getFundWithAdviser().equals("WeeklyPicks__swapnil"));
    assertThat(fund.getTickersWithComposition().get("2").getFundWithAdviser().equals("WeeklyPicks__swapnil"));
    assertThat(fund.getTickersWithComposition().get("3").getFundWithAdviser().equals("WeeklyPicks__swapnil"));
    assertThat(fund.getTickersWithComposition().get("1").getTicker().equals("IN1-NSE-EQ"));
    assertThat(fund.getTickersWithComposition().get("2").getFundWithAdviser().equals("IN2-NSE-EQ"));
    assertThat(fund.getTickersWithComposition().get("3").getFundWithAdviser().equals("IN3-NSE-EQ"));
    assertThat(isZero(fund.getTickersWithComposition().get("1").getAllocation() - 15.));
    assertThat(isZero(fund.getTickersWithComposition().get("2").getAllocation() - 15.));
    assertThat(isZero(fund.getTickersWithComposition().get("3").getAllocation() - 15.));
  }

  public void playAllEvents(ActorSystem system, ActorMaterializer materializer, RegisterAdviserFromUserPool adviser,
      String fundName) throws Exception {
    askAndWaitForResult(adviserManagerActor, new RegisterAdviserFromUserPool(adviser.getAdviserUsername(),
        adviser.getName(), adviser.getEmail(), adviser.getPublicARN(),adviser.getCompany(), adviser.getPhoneNumber()));
    askAndWaitForResult(adviserManagerActor, new FloatFund(ADVISER_IDENTITY_ID, fundName, eodDateFromExcelDate("28/02/2013"),
        eodDateFromExcelDate("31/12/2079"), fundName + "-Description", "Theme", 6500.00, 20500, "MEDIUM"));
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "1", "IN1-NSE-EQ", 20., 99.0, eodDateFromExcelDate("28/02/2013"), 103.0,
            eodDateFromExcelDate("28/02/2013"), eodDateFromExcelDate("28/02/2013")));
    Thread.sleep(2000);
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "2", "IN2-NSE-EQ", 20., 99.0, eodDateFromExcelDate("28/02/2013"), 205.0,
            eodDateFromExcelDate("28/02/2013"), eodDateFromExcelDate("28/02/2013")));
    Thread.sleep(2000);
    askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fundName, "3", "IN3-NSE-EQ", 20., 99.0, eodDateFromExcelDate("28/02/2013"), 160.0,
            eodDateFromExcelDate("28/02/2013"), eodDateFromExcelDate("28/02/2013")));
    Thread.sleep(2000);

    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "1", 5., eodDateFromExcelDate("28/02/2013"), 103.));
    Thread.sleep(2000);
    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "2", 5., eodDateFromExcelDate("28/02/2013"), 103.));
    Thread.sleep(2000);
    askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fundName, "3", 5., eodDateFromExcelDate("28/02/2013"), 103.));
    Thread.sleep(2000);

  }
  
  @Test public void 
  remove_user_fund_db_entry_after_unsubscribing() throws Exception {
    ActorMaterializer materializer = ActorMaterializer.create(system);
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    Result responseEntity;
    registerAdviserProcess();

    FloatFund floatFund = new FloatFund(ADVISER_IDENTITY_ID, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL);
    responseEntity = (Result) askAndWaitForResult(adviserManagerActor, floatFund);
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    responseEntity = (Result) askAndWaitForResult(adviserManagerActor, firstAdvice);
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(3);

    registerUserAndActivate();
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);

    subscriptionProcess();
    
    populateView(materializer);

    assertThat(userEquityAdviseRepository.count()).isEqualTo(COUNTOFPERSISTEDUSERADVISES);
    assertThat(userFundRepository.count()).isEqualTo(COUNTOFPERSISTEDUSERFUNDS);
    
    unSubscriptionProcess();
    populateView(materializer);
    waitForProcessToComplete(3);
    assertThat(userFundRepository.count()).isEqualTo(COUNTOFPERSISTEDUSERFUNDAFTERFUNDEXIT);
}

  @Test public void 
  remove_fund_entry_on_fund_closed() throws Exception {
    ActorMaterializer materializer = ActorMaterializer.create(system);
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    Result responseEntity;
    registerAdviserProcess();

    FloatFund floatFund = new FloatFund(ADVISER_IDENTITY_ID, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL);
    responseEntity = (Result) askAndWaitForResult(adviserManagerActor, floatFund);
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    waitForProcessToComplete(3);

    CloseFund closeFund = new CloseFund(ADVISER_IDENTITY_ID, FUND_NAME, 10);
    responseEntity = (Result) askAndWaitForResult(adviserManagerActor, closeFund);
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo(" fund closed.");

    populateView(materializer);
    waitForProcessToComplete(3);
    assertThat(fundRepository.count()).isEqualTo(COUNTOFPERSISTEDFUNDS);
  }

  @Test
  public void checkRecoveryForUser() throws Exception {
    registerUserAndActivate();
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);

    changeWealthCode();
    viewPopulater.clearCacheToCheckRecovery();
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);
    
    Optional<CurrentUserDb> user = userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL);
    assertThat(user.get().getIsWealthCode()).isEqualTo("true");
  }

  private void subscriptionProcess() throws Exception {
    waitForProcessToComplete(2);
    SubscribeToFund subscribeToFund = new SubscribeToFund(ADVISER_IDENTITY_ID, FUND_NAME, USERNAME, Double.parseDouble(LUMPSUMAMOUNT), Double.parseDouble(SIPAMOUNT),
        SIPDATE, InvestingMode.valueOf(INVESTINGMODE), Purpose.EQUITYSUBSCRIPTION);
    
    Result responseEntity = (Result) askAndWaitForResult(userRootActor, subscribeToFund);

    
    FundActionApproved fundActionApproved = new FundActionApproved(USERNAME, FUND_NAME, ADVISER_IDENTITY_ID, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth());
    
    responseEntity = (Result) askAndWaitForResult(userRootActor, fundActionApproved);
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage())
          .isEqualTo("Subscription in process. You will receive a notification once its done.");
  }
  
  private void unSubscriptionProcess() throws Exception {
    waitForProcessToComplete(2);
    UnsubscribeToFund unSubscribeToFund = new UnsubscribeToFund(ADVISER_IDENTITY_ID, FUND_NAME, USERNAME, InvestingMode.valueOf(INVESTINGMODE));
    
    Result responseEntity = (Result) askAndWaitForResult(userRootActor, unSubscribeToFund);

    
    FundActionApproved fundActionApproved = new FundActionApproved(USERNAME, FUND_NAME, ADVISER_IDENTITY_ID, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, UNSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth());
    
    responseEntity = (Result) askAndWaitForResult(userRootActor, fundActionApproved);
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage())
          .isEqualTo("UnSubscribe in process. You will receive a notification once its done.");
  }
  
  private void registerAdviserProcess() throws Exception {
    Result responseEntity = (Result) askAndWaitForResult(adviserManagerActor, ADVISER);
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo("Adviser Registered");
    Thread.sleep(5*1000);
  }
  
  private void registerUserAndActivate() throws Exception {
    RegisterInvestorFromUserPool registerInvestorFromUserPool = new RegisterInvestorFromUserPool(USERNAME, INVESTOR_NAME, "", SUBSCRIPTION_USER_EMAIL);
    Result responseEntity = (Result) askAndWaitForResult(userRootActor, registerInvestorFromUserPool);
    assertThat(responseEntity).isInstanceOf(Done.class);
    Thread.sleep(5*1000);
  }
  
  private void changeWealthCode() throws Exception {
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, SUBSCRIPTION_USER_EMAIL);
    Result responseEntity = (Result) askAndWaitForResult(userRootActor, kycApproveCommand);
    assertThat(responseEntity).isInstanceOf(Done.class);
    Thread.sleep(5*1000);

    UpdateWealthCode updateWealthCode = new UpdateWealthCode(SUBSCRIPTION_WEALTHCODE, USERNAME);
    responseEntity = (Result) askAndWaitForResult(userRootActor, updateWealthCode);
    assertThat(responseEntity).isInstanceOf(Done.class);
    Thread.sleep(5*1000);
  }

  private void sendLivePriceToEquityActor(double currentMarketPrice, String token) {
    universeRootActor.tell(new RealTimeInstrumentPrice(new InstrumentPrice(currentMarketPrice, currentTimeInMillis(), token)), ActorRef.noSender());
  }
  
  private void populateView(ActorMaterializer materializer) throws Exception {
    viewPopulater.blockingPopulateView(system, materializer);
  }
  
  private void waitForProcessToComplete(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000);
  }
  
  private void sendEODPriceToEquityActor(double currentMarketPrice, String token, long time) {
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(currentMarketPrice, time, token), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }
  
}