package com.wt.utils;

import static akka.actor.ActorRef.noSender;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.FundConstituent;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.stream.ActorMaterializer;
import commons.Retry;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class PortfolioCompositionShould {
  private static final RegisterAdviserFromUserPool ADVISER = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi",
      "swapnil@bpwealth.com", "32publicId", "Fort Capital", "");
  private static final String ADVISER_IDENTITY = "swapnil";
  private static final String FUND_NAME = "WeeklyPicks";
  private static final long STARTDATE = 1388082600000L;
  private static final long ENDDATE = 3471186600000L;
  private static final String FUND_DESCRIPTION = FUND_NAME + "Description";
  private static final String FUND_THEME = "Theme";
  private static final double FUND_MIN_SIP = 20000.;
  private static final double FUND_MIN_LUMPSUM = 200000.;
  private static final String FUND_RISK_LEVEL = "MEDIUM";
  private static final double CURRENT_MARKET_PRICE = 1040d;
  private static final String ADVISE_ID_1 = "1a";
  private static final String TICKER1 = "IN1-NSE-EQ";
  private static final double ALLOCATION1 = 20.0;
  private static final double ENTRYPRICE = 1030.;
  private static final long ENTRYTIME = 0;
  private static final double EXITPRICE = 1090.;
  private static final long EXITTIME = 1;
  private static final long ISSUETIME1 = currentTimeInMillis();
  private static final double PARTIAL_CLOSING_ALLOCATION = 5.;
  private static final double COMPLETE_CLOSING_ALLOCATION = 20.;
  private static final long ACTUAL_EXITDATE = currentTimeInMillis();
  private static final double ACTUAL_EXITPRICE = 1090.;
  private static final int TOKEN_1 = 123;
  private static final String SYMBOL_1 = "ACC";
  private static final String TICKER2 = "IN2-NSE-EQ";
  private static final int TOKEN_2 = 456;
  private static final String SYMBOL_2 = "SBIN";
  private static final String TICKER3 = "IN3-NSE-EQ";
  private static final int TOKEN_3 = 789;
  private static final String SYMBOL_3 = "ICICIBANK";

  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private ActorSystem system;
  @Autowired
  private ActorMaterializer materializer;
  @Autowired
  private FundRepository fundRepository;
  
  private ActorRef universeRootActor;
  private ActorRef adviserRootActor;
  @SuppressWarnings("unused")
  private ActorRef activeInstrumentTrackerActor;
  @SuppressWarnings("unused")
  private ActorRef adviserActor;
  @SuppressWarnings("unused")
  private ActorRef fundActor;

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Before public void 
  setupDB() throws Exception {
    dbTest.createSchema();
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    adviserRootActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"), "adviser-aggregator");
    adviserActor = system.actorOf(SpringExtProvider.get(system).props("AdviserActor", ADVISER_IDENTITY),
        adviserRootActor.path().name() + "-" + ADVISER_IDENTITY);
    activeInstrumentTrackerActor = system.actorOf(SpringExtProvider.get(system).props("ActiveInstrumentTrackerActor"),
        "active-instrument-tracker");
    fundActor = system.actorOf(SpringExtProvider.get(system).props("FundActor", FUND_NAME),
        adviserRootActor.path().name() + "-" + FUND_NAME);
    environmentVariables.set("WT_MARKET_OPEN", "true");
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());
  }

  @Rule
  public Retry retry = new Retry(5);
  
  private void getSnapshot(double currentMarketPrice, String token) {
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(currentMarketPrice, currentTimeInMillis(), token), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }
  
  @Test public void 
  update_fund_composition_on_issue_advise() throws Exception {
    getSnapshot(CURRENT_MARKET_PRICE, TICKER1);
    adviserRootActor.tell(ADVISER, noSender());
    adviserRootActor.tell(new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP,
        FUND_MIN_LUMPSUM, FUND_RISK_LEVEL), ActorRef.noSender());
    adviserRootActor.tell(new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, ADVISE_ID_1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1), noSender());
    Thread.sleep(2000);
    
    waitForViewToPopulate(10);
    Iterable<CurrentFundDb> funds = fundRepository.findAll();

    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo(FUND_NAME);
    

    CurrentFundDb fund = funds.iterator().next();
    FundConstituent fundConstituent = fund.getTickersWithComposition().get(ADVISE_ID_1);
    assertThat(fundConstituent.getAllocation()).isEqualTo(20.0);
    
  }
  
  @Test public void 
  update_fund_composition_on_close_advise() throws Exception {
    getSnapshot(CURRENT_MARKET_PRICE, TICKER1);
    adviserRootActor.tell(ADVISER, noSender());
    adviserRootActor.tell(new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP,
        FUND_MIN_LUMPSUM, FUND_RISK_LEVEL), ActorRef.noSender());
    adviserRootActor.tell(new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, ADVISE_ID_1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1), noSender());
    Thread.sleep(2000);
    adviserRootActor.tell(new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, ADVISE_ID_1,
        PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE), noSender());
    Thread.sleep(2000);
    waitForViewToPopulate(5);
    Iterable<CurrentFundDb> funds = fundRepository.findAll();

    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo(FUND_NAME);
    

    CurrentFundDb fund = funds.iterator().next();
    FundConstituent fundConstituent = fund.getTickersWithComposition().get(ADVISE_ID_1);
    assertThat(fundConstituent.getAllocation()).isEqualTo(15);
    
  }
  
  @Test public void 
  update_fund_composition_post_partial_close() throws Exception {
    getSnapshot(CURRENT_MARKET_PRICE, TICKER1);
    adviserRootActor.tell(ADVISER, noSender());
    adviserRootActor.tell(new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP,
        FUND_MIN_LUMPSUM, FUND_RISK_LEVEL), ActorRef.noSender());
    adviserRootActor.tell(new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, ADVISE_ID_1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1), noSender());
    Thread.sleep(2000);
    adviserRootActor.tell(new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, ADVISE_ID_1,
        PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE), noSender());
    Thread.sleep(2000);
    waitForViewToPopulate(5);
    Iterable<CurrentFundDb> funds = fundRepository.findAll();

    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo(FUND_NAME);
    

    CurrentFundDb fund = funds.iterator().next();
    FundConstituent fundConstituent = fund.getTickersWithComposition().get(ADVISE_ID_1);
    assertThat(fundConstituent.getAllocation()).isEqualTo(ALLOCATION1 - PARTIAL_CLOSING_ALLOCATION);
    
    adviserRootActor.tell(new EODCompleted(), noSender());
    
    waitForViewToPopulate(5);
    
    fundConstituent = fund.getTickersWithComposition().get(ADVISE_ID_1);
    assertThat(fundConstituent.getAllocation()).isEqualTo(15.0);
    
  }

  @Test public void 
  remove_fund_constituent_on_complete_exit() throws Exception {
    getSnapshot(CURRENT_MARKET_PRICE, TICKER1);

    adviserRootActor.tell(ADVISER, noSender());
    adviserRootActor.tell(new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP,
        FUND_MIN_LUMPSUM, FUND_RISK_LEVEL), ActorRef.noSender());
    adviserRootActor.tell(new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, ADVISE_ID_1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1), noSender());
    Thread.sleep(2000);
    adviserRootActor.tell(new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, ADVISE_ID_1,
        COMPLETE_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE), noSender());
    Thread.sleep(2000);
    waitForViewToPopulate(5);
    Iterable<CurrentFundDb> funds = fundRepository.findAll();

    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getFundName()).isEqualTo(FUND_NAME);
    

    CurrentFundDb fund = funds.iterator().next();
    FundConstituent fundConstituent = fund.getTickersWithComposition().get(ADVISE_ID_1);
    assertThat(fundConstituent).isNull();
  }
  
  @SuppressWarnings("unused")
  @After public void 
  deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    Terminated result = terminate.result(Duration.Inf(), new CanAwait() {
    });
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      FileUtils.deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private void waitForViewToPopulate(int seconds) throws Exception {
    Thread.sleep(seconds * 1000);
    viewPopulater.blockingPopulateView(system, materializer);
  }
}
