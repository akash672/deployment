package com.wt.utils;

import static akka.pattern.Patterns.ask;
import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static scala.concurrent.Await.result;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Files;
import org.chiknrice.test.SpringifiedConcordionRunner;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.domain.FundPerformance;
import com.wt.domain.MaxDrawdown;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.repo.FundPerformanceRepository;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.write.commands.CalculateHistoricalFundPerformance;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.GetPricePaf;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateMFInstruments;
import com.wt.domain.write.events.DividendEvent;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.EquityCorporateEvent;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.StockSplitEvent;
import com.wt.utils.akka.ActorResultWaiter;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.stream.ActorMaterializer;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@RunWith(SpringifiedConcordionRunner.class)
//@Extensions(ExcelExtension.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@Ignore
public class WDConcordionIntegration {
  private static final String ADVISER_IDENTITY_ID = "swapnil";
  private static final String TICKER1 = "1214-NSE-EQ";
  private static final String SYMBOL1 = "INFY";
  private static final int INT_TOKEN1 = 1214;
  private static final String TICKER2 = "1234-NSE-EQ";
  private static final String SYMBOL2 = "RIL";
  private static final int INT_TOKEN2 = 1234;
  private static final String TICKER3 = "123-NSE-EQ";
  private static final String SYMBOL3 = "TATA";
  private static final int INT_TOKEN3 = 123;
  @Autowired
  private FundPerformanceRepository fundPerformanceRepository;

  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private FundRepository fundRepository;
  @Autowired
  private ActorSystem system;
  private ActorMaterializer materializer;
  private ActorRef adviserManagerActor;
  private ActorRef universeRootActor;
  private Map<String, Double> pafMap = new HashMap<>();
  
  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
  
  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      FileUtils.deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void setUpAdvisorFundActorSystem(String advisor, String fundName, String startDate, String endDate)
      throws Exception {
    RegisterAdviserFromUserPool adviser = new RegisterAdviserFromUserPool(advisor, advisor + " Joshi", "swapnil@bpwealth.com", 
        "32publicId", "Fort Capital", "");
    Files.delete(new File("build/journal"));
    Files.delete(new File("build/snapshots"));
    materializer = ActorMaterializer.create(system);
    adviserManagerActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"), "adviser-aggregator");
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL1,INT_TOKEN1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL2,INT_TOKEN2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL3,INT_TOKEN3);
    dbTest.setupDB();
    environmentVariables.set("WT_MARKET_OPEN", "true");
    askAndWaitForResult(adviserManagerActor,
        new RegisterAdviserFromUserPool(adviser.getAdviserUsername(), adviser.getName(), adviser.getEmail(),
            adviser.getPublicARN(), adviser.getCompany(), adviser.getPhoneNumber()));
    askAndWaitForResult(adviserManagerActor,
        new FloatFund(ADVISER_IDENTITY_ID, fundName, eodDateFromExcelDate(startDate), eodDateFromExcelDate(endDate),
            fundName + "-Description", "Theme", 6500, 20500, "Medium"));
  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(new UpdateEquityInstrumentSymbol(String.valueOf(token), "", symbol, ticker, "", "NSE", "EQ"), ActorRef.noSender());
  }

  public String updateInstrumentsMapOfMutualFundActor(String token, String schemeCode, String segment, String schemeName ) throws Exception {
     
    Set<MutualFundInstrument> mutualFundInstruments = new HashSet<>();
    mutualFundInstruments.add(new MutualFundInstrument(token,segment, schemeName, schemeCode,"5000", 
                                                "500.0", "ELSS", "1.0", "CAMS" ));
    Done result = (Done) askAndWaitForResult(universeRootActor, new UpdateMFInstruments(mutualFundInstruments));
    return result.getMessage();
  }
  
  public void populateHistoricalPaf(String token, String date, String paf) throws Exception {
    pafMap.put(token + eodDateFromExcelDate(date), Double.parseDouble(paf));
  }

  public String populateDividendEvents(String date, String token, String value) throws Exception {
    DividendEvent dividendEvent = new DividendEvent(token, DateUtils.bodDateFromExcelDate(date, "dd/MM/yyyy", "/"),
        Double.parseDouble(value));
    Done result = (Done) sendCorporateEventUpdate(universeRootActor, dividendEvent);
    return result.getId();
  }

  public String populateStockSplitEvents(String date, String token, String from, String to) throws Exception {
    StockSplitEvent stockSplitEvent = new StockSplitEvent(token,
        DateUtils.bodDateFromExcelDate(date, "dd/MM/yyyy", "/"), Double.parseDouble(from), Double.parseDouble(to));
    Done result = (Done) sendCorporateEventUpdate(universeRootActor, stockSplitEvent);
    return result.getId();
  }

  public String populateEffectivePaf(String token) throws Exception {
    return "1";
  }

  public String populatePrices(String date, String token, String price) throws Exception {
    long timestamp = eodDateFromExcelDate(date);
    InstrumentPrice pxEvt = new InstrumentPrice(Double.parseDouble(price), timestamp, token);
    sendPriceUpdate(universeRootActor, pxEvt);

    double paf = (Double) askAndWaitForResult(universeRootActor, new GetPricePaf(token, timestamp));
    return String.format("%.9f", paf);
  }

  public String issueAdvice(String advisor, String fund, String adviceId, String token, String allocation,
      String issueTime, String entryTime, String entryPrice, String exitTime, String exitPrice) throws Exception {
	  environmentVariables.set("WT_MARKET_OPEN", "true");
    Done result = (Done) askAndWaitForResult(adviserManagerActor,
        new IssueAdvice(ADVISER_IDENTITY_ID, fund, adviceId, token, Double.parseDouble(allocation),
            Double.parseDouble(entryPrice), eodDateFromExcelDate(entryTime), Double.parseDouble(exitPrice),
            eodDateFromExcelDate(exitTime), eodDateFromExcelDate(issueTime)));
    return result.getId();
  }

  public String closeAdvice(String advisor, String fund, String closeAdviceId, String closingToken,
      String closeAllocation, String actualExitTime, String actualExitPrice) throws Exception {
	  environmentVariables.set("WT_MARKET_OPEN", "true");
    Done result = (Done) askAndWaitForResult(adviserManagerActor,
        new CloseAdvise(ADVISER_IDENTITY_ID, fund, closeAdviceId, Double.parseDouble(closeAllocation),
            eodDateFromExcelDate(actualExitTime), Double.parseDouble(actualExitPrice)));

    return result.getId();
  }

  public MaxDrawdown getFundMaxDrawdown(String fund, String period) {
    List<MaxDrawdown> returns = fundRepository.findById(fund).orElse(null).getDrawDowns();
    for (MaxDrawdown maxDrawdown : returns) {
      if (maxDrawdown.getPeriod().equalsIgnoreCase(period)) {
        return maxDrawdown;
      }
    }
    return null;
  }

  public FundPerformanceWithCash getFundPerformance(String fund, String time) throws Exception {
    List<FundPerformance> fundPerformance = new ArrayList<>();
    try {
      fundPerformanceRepository.findByFund(fund).stream().filter(new Predicate<FundPerformance>() {
        @Override
        public boolean test(FundPerformance perf) {
          try {
            return perf.getDate() == eodDateFromExcelDate(time);
          } catch (ParseException e) {
            e.printStackTrace();
          }
          return false;
        }
      }).forEach(fundPerformance::add);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    double performance = (!fundPerformance.isEmpty()) ? fundPerformance.get(0).getPerformance() : 0.;
    double cash = (!fundPerformance.isEmpty()) ? fundPerformance.get(0).getCash() : 0.;
    FundPerformanceWithCash fundPerformanceWithCash = new FundPerformanceWithCash(performance, cash);
    return fundPerformanceWithCash;

  }

  public void calculateFundPerformance(String advisor, String fund) throws Exception {
    askAndWaitForResult(adviserManagerActor, new CalculateHistoricalFundPerformance());
    viewPopulater.blockingPopulateView(system, materializer);
  }

  protected Object sendPriceUpdate(final ActorRef to, PriceWithPaf event) throws Exception {
    Future<Object> maybeRegistered = ask(to, event, 10 * 1000L);
    Done result = (Done) result(maybeRegistered, ActorResultWaiter.TIMEOUT);

    return result;
  }

  protected Object sendPriceUpdate(final ActorRef to, InstrumentPrice event) throws Exception {
    Future<Object> maybeRegistered = ask(to, event, 10 * 1000L);
    Done result = (Done) result(maybeRegistered, ActorResultWaiter.TIMEOUT);

    return result;
  }

  protected Object sendCorporateEventUpdate(final ActorRef to, EquityCorporateEvent event) throws Exception {
    return (Done) askAndWaitForResult(to, event);
  }

  class FundPerformanceWithCash {
    public double performance;
    public double cash;

    public FundPerformanceWithCash(double performance, double cash) {
      super();
      this.performance = performance;
      this.cash = cash;
    }
  }
}
