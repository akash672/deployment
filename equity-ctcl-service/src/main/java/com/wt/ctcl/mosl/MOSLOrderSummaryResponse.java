package com.wt.ctcl.mosl;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.wt.mosl.write.commands.MOSLTradeUpdateStatus;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class MOSLOrderSummaryResponse {
  private List<MOSLTradeUpdateStatus> moslTradeUpdateStatus;
}