package com.wt.ctcl.mosl.util;

import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.HashMap;
import java.util.Map;

import com.wt.domain.EquityInstrument;
import com.wt.domain.OrderSide;
import com.wt.domain.write.commands.BrokerOrderAccepted;
import com.wt.domain.write.commands.BrokerOrderRejected;
import com.wt.domain.write.commands.ExchangeOrderAccepted;
import com.wt.domain.write.commands.GetSymbolToInstrumentMap;
import com.wt.domain.write.commands.OrderPlacementUpdate;
import com.wt.domain.write.commands.SymbolToInstrumentMapSent;
import com.wt.domain.write.commands.TradeOrderConfirmation;
import com.wt.domain.write.events.Failed;
import com.wt.mosl.domain.MOSLOrderStatus;
import com.wt.mosl.write.commands.MOSLTradeUpdateStatus;
import com.wt.utils.CommonConstants;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;

import lombok.extern.log4j.Log4j;

@Log4j
public class MOSLMessageFactory extends UntypedAbstractActor {
  
  private Map<String, EquityInstrument> mapSymbolToEqInstrument = new HashMap<String, EquityInstrument>();
  private WDActorSelections actorSelections;

  public MOSLMessageFactory(WDActorSelections actorSelections) {
    super();
    this.actorSelections = actorSelections;
    updateSymbolToEqInstrumentMap();

  }

  private void updateSymbolToEqInstrumentMap() {
    SymbolToInstrumentMapSent symbolToInstrumentMapSent = null;
    boolean instrumentsMapReceived = false;
    int retryCounter = 1;
    while (!instrumentsMapReceived && retryCounter <= 3) {
      try {
        Object result = askAndWaitForResult(actorSelections.universeRootActor(), new GetSymbolToInstrumentMap());

        if (result instanceof Failed) {
          retryCounter++;
          log.warn("MOSLMessageFactory :Failed to get instruments by symbol from EquityActor. Retrying " + result);
          continue;
        }
        log.info("Received Instrument Map from universe on MOSLMessageFactory");
        symbolToInstrumentMapSent = (SymbolToInstrumentMapSent) result;
        mapSymbolToEqInstrument = symbolToInstrumentMapSent.getSymbolToInstruments();
        instrumentsMapReceived = true;
      } catch (Exception e) {
        log.warn("MOSLMessageFactory: Exception while getting instruments by symbol from EquityActor. Retrying ", e);
        retryCounter++;
      }
    }
    if (retryCounter > 3)
      log.error("MOSLMessageFactory : Failed to get instruments map for universe actor after multiple attempts.");

  }
    
  public void onReceive(Object msg) throws Throwable {
    if (msg instanceof MOSLTradeUpdateStatus) {
      MOSLTradeUpdateStatus moslTradeUpdateStatus = (MOSLTradeUpdateStatus) msg;
      checkOrderStatus(moslTradeUpdateStatus);
    }
    if (msg instanceof OrderPlacementUpdate) {
      OrderPlacementUpdate tradeStatus = (OrderPlacementUpdate) msg;
      updateOrderActorIfOrderPlacementFailed(tradeStatus);
    }
  }

  private void checkOrderStatus(MOSLTradeUpdateStatus moslTradeUpdateStatus) {
    if (isOrderTraded(moslTradeUpdateStatus.getOrderStatus())) {
      generateOrderConfirmations(moslTradeUpdateStatus);
    } else if (isOrderRejected(moslTradeUpdateStatus.getOrderStatus())) {
      sendOrderRejections(moslTradeUpdateStatus);
    }

  }

  private void updateOrderActorIfOrderPlacementFailed(OrderPlacementUpdate tradeStatus) {
    if (!orderPlacedWithBroker(tradeStatus.getOrderResponse())) {
      actorSelections.equityOrderRootActor().tell(tradeStatus, ActorRef.noSender());
    }

  }
  
  private boolean isOrderTraded(String orderStatus) {
    return MOSLOrderStatus.valueOf(orderStatus).equals(MOSLOrderStatus.Traded);
  }

  private boolean isOrderRejected(String orderStatus) {
    return MOSLOrderStatus.valueOf(orderStatus).equals(MOSLOrderStatus.Cancel)
        || MOSLOrderStatus.valueOf(orderStatus).equals(MOSLOrderStatus.Rejected)
        || MOSLOrderStatus.valueOf(orderStatus).equals(MOSLOrderStatus.Error);
  }

  private boolean orderPlacedWithBroker(String orderResponse) {
    return orderResponse.contains("True");
  }

  private void sendOrderRejections(MOSLTradeUpdateStatus moslTradeUpdateStatus) {

    String exchange = getExchangeNameFromCode(moslTradeUpdateStatus.getExchangeCode());
    String token = getTokenForCorrespondingSymbol(moslTradeUpdateStatus.getScripName(), exchange);
    OrderSide currentOrderSide = getOrderSideFrom(moslTradeUpdateStatus.getBatchSequenceId());
    moslTradeUpdateStatus.setToken(token);
    actorSelections.equityOrderRootActor().tell(
        new BrokerOrderRejected(moslTradeUpdateStatus.getBatchSequenceId(), moslTradeUpdateStatus.getBatchSequenceId(),
            moslTradeUpdateStatus.getOrderStatus(), moslTradeUpdateStatus.getClientcode(), token, 0, currentOrderSide),
        ActorRef.noSender());
  }

  private void generateOrderConfirmations(MOSLTradeUpdateStatus moslTradeUpdateStatus) {

    String exchange = getExchangeNameFromCode(moslTradeUpdateStatus.getExchangeCode());
    String token = getTokenForCorrespondingSymbol(moslTradeUpdateStatus.getScripName(), exchange);
    OrderSide currentOrderSide = getOrderSideFrom(moslTradeUpdateStatus.getOrderSide());
    moslTradeUpdateStatus.setToken(token);
    sendBrokerOrderConfirmation(moslTradeUpdateStatus, currentOrderSide);
    sendExchangeOrderConfirmation(moslTradeUpdateStatus, currentOrderSide);
    sendTradeOrderConfirmation(moslTradeUpdateStatus, currentOrderSide);
  }

  private void sendTradeOrderConfirmation(MOSLTradeUpdateStatus moslTradeUpdateStatus, OrderSide currentOrderSide) {
    actorSelections.equityOrderRootActor()
        .tell(new TradeOrderConfirmation(moslTradeUpdateStatus.getUniqueOrderId(),
            moslTradeUpdateStatus.getBatchSequenceId(), moslTradeUpdateStatus.getTradeOrderId(),
            Float.valueOf(moslTradeUpdateStatus.getTradePrice()), Integer.valueOf(moslTradeUpdateStatus.getTradeQty()),
            DateUtils.currentTimeInMillis(), moslTradeUpdateStatus.getClientcode(), moslTradeUpdateStatus.getToken(),
            Double.valueOf(moslTradeUpdateStatus.getTradeQty()), currentOrderSide), ActorRef.noSender());
  }

  private void sendExchangeOrderConfirmation(MOSLTradeUpdateStatus moslTradeUpdateStatus, OrderSide currentOrderSide) {
    actorSelections.equityOrderRootActor()
        .tell(new ExchangeOrderAccepted(moslTradeUpdateStatus.getClientcode(),
            moslTradeUpdateStatus.getBatchSequenceId(), moslTradeUpdateStatus.getUniqueOrderId(),
            DateUtils.currentTimeInMillis(), moslTradeUpdateStatus.getToken(),
            Integer.valueOf(moslTradeUpdateStatus.getTradeQty()), currentOrderSide), ActorRef.noSender());

  }

  private String getTokenForCorrespondingSymbol(String symbol, String exchange) {
    String symbolWithExchange = symbol.concat(CommonConstants.HYPHEN).concat(exchange);
    if(!mapSymbolToEqInstrument.containsKey(symbolWithExchange))
      updateSymbolToEqInstrumentMap();
    EquityInstrument equityInstrument = mapSymbolToEqInstrument
        .get(symbolWithExchange);
    return equityInstrument.getToken().split("-")[0];
  }

  private void sendBrokerOrderConfirmation(MOSLTradeUpdateStatus moslTradeUpdateStatus, OrderSide currentOrderSide) {
    actorSelections.equityOrderRootActor()
        .tell(new BrokerOrderAccepted(moslTradeUpdateStatus.getBatchSequenceId(),
            moslTradeUpdateStatus.getBatchSequenceId(), moslTradeUpdateStatus.getClientcode(),
            moslTradeUpdateStatus.getToken(), Integer.valueOf(moslTradeUpdateStatus.getTradeQty()), currentOrderSide),
            ActorRef.noSender());

  }

  private OrderSide getOrderSideFrom(String orderSideReceived) {
    return orderSideReceived.equals("B") ? OrderSide.BUY : OrderSide.SELL;
  }

  private String getExchangeNameFromCode(String exchangeCode) {
    if (Integer.valueOf(exchangeCode) == 0)
      return "NSE";
    else
      return "BSE";
  }

}
