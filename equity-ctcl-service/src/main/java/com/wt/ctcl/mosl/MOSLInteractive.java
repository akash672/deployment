package com.wt.ctcl.mosl;

import static com.amazonaws.util.json.Jackson.toJsonString;
import static com.wt.domain.BrokerName.MOSL;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.utils.CommonConstants.CommaSeparated;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.mosl.util.MOSLCtclConfig;
import com.wt.ctcl.mosl.util.MOSLMessageFactory;
import com.wt.domain.BrokerType;
import com.wt.domain.CancelOrder;
import com.wt.domain.CtclOrder;
import com.wt.domain.OrderSide;
import com.wt.domain.write.commands.OrderPlacementUpdate;
import com.wt.mosl.write.commands.MOSLOrderRequestDetails;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import lombok.extern.log4j.Log4j;

@Service("MOSLInteractive")
@Log4j
public class MOSLInteractive implements CtclInteractive {

  private ActorRef moslMessageFactory;
  @SuppressWarnings("unused")
  private WDActorSelections actorSelections;
  private MOSLCtclConfig moslConfig;

  public MOSLInteractive(WDActorSelections actorSelections, MOSLCtclConfig moslConfig) {
    super();
    this.actorSelections = actorSelections;
    this.moslConfig = moslConfig;
    ActorSystem system = ActorSystem.create("mosl-interactive");
    moslMessageFactory = system.actorOf(Props.create(MOSLMessageFactory.class, actorSelections));
  }

  @Override
  public synchronized void sendMarketCashOrder(CtclOrder order) throws Exception {
    String orderSide = returnOrderSide(order.getOrderExecutionMetaData().getOrderSide());
    String localOrderId = splitBasketOrderCompositeIdAndReturnLocalId(
        order.getOrderExecutionMetaData().getBasketOrderCompositeId());
    String orderString = generateOrderString(order, orderSide, localOrderId);
    sendOrderToBroker(orderString, localOrderId);

  }

  private String generateOrderString(CtclOrder order, String orderSide, String localOrderId) {
    String exchangeCode = getExchangeCode(order.getExchange());
    String symbol = order.getSymbol();
    String orderType = "M";
    String quantity = String.valueOf(order.getIntegerQuantity());
    String lots = String.valueOf(order.getIntegerQuantity());
    String price = "0";
    String clientCode = order.getClientCode();
    String orderDuration = "D";

    String orderString = orderSide + CommaSeparated + exchangeCode + CommaSeparated + symbol + CommaSeparated
        + orderType + CommaSeparated + quantity + CommaSeparated + lots + CommaSeparated + price + CommaSeparated
        + clientCode + CommaSeparated + orderDuration + CommaSeparated + localOrderId;
    log.info("orderString for localOrderId: " + localOrderId + " and clientCode: " + order.getClientCode() + " is " + orderString);

    return orderString;
  }

  private void sendOrderToBroker(String orderString, String localOrderId) throws IOException {
    Client client = ClientBuilder.newClient();
    WebTarget webTarget = client.target(moslConfig.inserOrderUrl());
    Form form = new Form();
    form.param("input", orderString);
    Response response = webTarget.request().post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED),
        Response.class);
    String responseString = response.readEntity(String.class);
    log.info("MOSL Order Placement Status for " + localOrderId + " is " + responseString);
    moslMessageFactory.tell(new OrderPlacementUpdate(responseString, localOrderId), ActorRef.noSender());
    client.close();
  }

  @Override
  public synchronized void sendOrderBook(String clientCode, String localOrderId) throws Exception {
    String basketOrderId = getBasketIdFromLocalOrderId(localOrderId);
    MOSLOrderRequestDetails moslRequestOrderDetails = new MOSLOrderRequestDetails(clientCode, basketOrderId);
    String jsonString = toJsonString(moslRequestOrderDetails);
    String orderBookJsonResponse = pollForOrderBookAndReturnTradeUpdate(jsonString, clientCode, basketOrderId);
    parseReceivedResponseAndForwardToFactory(orderBookJsonResponse);
  }

  private String getBasketIdFromLocalOrderId(String localOrderId) {
    int splitIndex = localOrderId.lastIndexOf("_");
    return localOrderId.substring(0, splitIndex);
  }

  private void parseReceivedResponseAndForwardToFactory(String jsonResponse)
      throws JsonParseException, JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    MOSLOrderSummaryResponse orderSummaryResponse = mapper.readValue(jsonResponse, MOSLOrderSummaryResponse.class);
    orderSummaryResponse.getMoslTradeUpdateStatus()
        .forEach(tradeStatus -> moslMessageFactory.tell(tradeStatus, ActorRef.noSender()));
  }

  private String pollForOrderBookAndReturnTradeUpdate(String jsonString, String clientCode, String basketId)
      throws IOException {
    Client client = ClientBuilder.newClient();
    WebTarget webTarget = client.target(moslConfig.tradeStatusPollingUrl());
    Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

    Response response = invocationBuilder.post(Entity.entity(jsonString, MediaType.APPLICATION_JSON));
    String responseString = response.readEntity(String.class);
    log.info("MOSL order polling status for clientCode " + clientCode + " and basketId " + basketId + " is  "
        + responseString);
    String parsedJson = cleanTheJson(responseString);
    client.close();
    return parsedJson;
  }

  private String cleanTheJson(String responseString) {
    String backSlashRemovedString = responseString.replace("\\", "");
    String commaTrimmedString = backSlashRemovedString.substring(1, backSlashRemovedString.length() - 1);
    String keyaddedToJson = "{\"moslTradeUpdateStatus\":" + commaTrimmedString + "}";
    return keyaddedToJson;
  }

  private String getExchangeCode(String exchange) {
    if (exchange.equals("NSE"))
      return "0";
    else if (exchange.equals("BSE"))
      return "1";
    return null;
  }

  private String returnOrderSide(OrderSide orderSide) {
    return orderSide.equals(BUY) ? "B" : "S";
  }

  private String splitBasketOrderCompositeIdAndReturnLocalId(String basketOrderCompositeId) {
    String[] parts = basketOrderCompositeId.split("__");
    return parts[0] + "_" + parts[1];
  }

  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(EQUITYSUBSCRIPTION, REAL, MOSL);
  }

  @Override
  public void sendTradeBook(String clientCode, String localOrderId) throws Exception {
    sendOrderBook(clientCode, localOrderId);
  }

  @Override
  public void sendMarginRequest(String clientCode) throws Exception {
  }

  @Override
  public void sendCancelLimitCashOrder(CancelOrder order) throws Exception {
  }

  @Override
  public void sendLimitCashOrder(CtclOrder order) throws Exception {
  }

  @Override
  public void heartBeatCheck() throws Exception {
  }
}
