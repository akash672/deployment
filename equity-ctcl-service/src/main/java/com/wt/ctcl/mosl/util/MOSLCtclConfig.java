package com.wt.ctcl.mosl.util;

import java.io.IOException;
import java.util.Properties;

import org.springframework.stereotype.Component;

@Component
public class MOSLCtclConfig {
  
  public Object property(String key) throws IOException {
    String value = System.getProperty(key);
    if (value != null)
      return value;
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("mosl-ctcl.config"));

    return properties.getProperty(key);
  }
  
  public String inserOrderUrl() throws IOException {
    String key = "mosl.insert.order.url";
    return property(key).toString();
  }
  
  public String tradeStatusPollingUrl() throws IOException {
    String key = "mosl.trade.status.polling.url";
    return property(key).toString();
  }
  
  public String marketOrderType() throws IOException {
    String key = "mosl.market.order.type";
    return property(key).toString();
  }


}
