package com.wt.ctcl.xneutrino.util;

import java.io.IOException;
import java.util.Properties;

import org.springframework.stereotype.Component;

@Component
public class BPWXNeutrinoConfig extends CtclXNeutrinoConfig {

  @Override
  public Object property(String key) throws IOException {
    String value = System.getProperty(key);
    if (value != null)
      return value;
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("bpw-ctcl-xneutrino.config"));

    return properties.getProperty(key);
  }

}
