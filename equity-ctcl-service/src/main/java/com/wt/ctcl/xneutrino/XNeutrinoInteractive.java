package com.wt.ctcl.xneutrino;

import static com.ctcl.wealthdesk.trading.TradeType.DELIVERY;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.ctcl.wealthdesk.trading.Orders;
import com.ctcl.xneutrino.interactive.HeartBeat;
import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.ctcl.xneutrino.interactive.LoginResponse;
import com.ctcl.xneutrino.interactive.LogoffResponse;
import com.ctcl.xneutrino.interactive.MarginRequest;
import com.ctcl.xneutrino.interactive.OrderBookRequest;
import com.ctcl.xneutrino.interactive.TradeBookRequest;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.xneutrino.util.CtclXNeutrinoConfig;
import com.wt.ctcl.xneutrino.util.MessageFactory;
import com.wt.domain.BrokerName;
import com.wt.domain.CancelOrder;
import com.wt.domain.CtclOrder;
import com.wt.domain.OrderSide;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;
import com.wt.utils.email.templates.wealthdesk.CTCLConnectionFailureEmail;
import com.wt.utils.email.templates.wealthdesk.WrongDealerPasswordEmail;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Log4j
@ToString
public abstract class XNeutrinoInteractive implements CtclInteractive, Observer {

  private InteractiveManager interactiveManager;
  private WDActorSelections actorSelections;
  private MessageFactory messageFactory;
  private CtclXNeutrinoConfig ctclXNeutrinoConfig;
  private SESEmailUtil emailerService;
  protected WDProperties properties;
  protected long lastHeartBeatTime;
  protected long timeBenchmark;
  private Orders orderExecutionService;
  private boolean loggedInSuccessfully;

  public XNeutrinoInteractive(InteractiveManager interactiveManager, ActorSystem actorSystem,
      WDActorSelections actorSelections, MessageFactory messageFactory, CtclXNeutrinoConfig ctclXNeutrinoConfig,
      SESEmailUtil emailerService, WDProperties properties) throws IOException {
    this.interactiveManager = interactiveManager;
    this.messageFactory = messageFactory;
    this.actorSelections = actorSelections;
    this.ctclXNeutrinoConfig = ctclXNeutrinoConfig;
    this.properties = properties;
    this.emailerService = emailerService;
    this.timeBenchmark = TimeUnit.NANOSECONDS.convert(60, TimeUnit.SECONDS);
    try {
      if (isBrokerActive())
        startInteractive();
    } catch (Exception e) {
      sendCtclConnectionFailureToDevTeam();
      log.error("NOT ABLE TO CONNECT TO CTCL, EMAIL SENT TO RESPECTIVE AUTHORITIES");
    }
    loggedInSuccessfully = false;
  }

  protected abstract boolean isBrokerActive();
  
  protected abstract BrokerName brokerName();
  
  @Override
  public synchronized void sendMarketCashOrder(CtclOrder order) throws Exception {
    interactiveManager.sendBlankPacket();
    short localOrderID = new Short(order.getLocalOrderId());
    if (connectIfNotConnected() && loggedInSuccessfully) {
      if (order.getExchange().equals("NSE"))
        orderExecutionService.sendMarketCashOrder(interactiveManager, localOrderID, order.getClientCode(), 0,
            (order.getOrderExecutionMetaData().getToken()), getSide(order.getSide()),
            order.getOrderExecutionMetaData().getSymbol(), order.getIntegerQuantity(), DELIVERY.getTradeType());
      else if (order.getExchange().equals("BSE"))
        orderExecutionService.sendMarketCashOrder(interactiveManager, localOrderID, order.getClientCode(), 1,
            (order.getOrderExecutionMetaData().getToken()), getSide(order.getSide()),
            order.getOrderExecutionMetaData().getSymbol(), order.getIntegerQuantity(), DELIVERY.getTradeType());
      else {
        log.error("INVALID EXCHANGE SENT IN ORDER");
        throw new Exception("Invalid exchange sent in order");
      }
    } else if (!loggedInSuccessfully) {
      log.error("DEALER NOT LOGGED IN OR WRONG PASSWORD ENTERED");
      throw new Exception("Wrong Dealer Password");
    }
  }

  @Override
  public synchronized void sendOrderBook(String clientCode, String localOrderId) throws Exception {
    OrderBookRequest orderBookRequest = new OrderBookRequest();
    orderBookRequest.setClientCode(clientCode);
    if (connectIfNotConnected())
      interactiveManager.sendRequest(orderBookRequest.getStruct(), false);
  }

  @Override
  public synchronized void sendTradeBook(String clientCode, String localOrderId) throws Exception {
    TradeBookRequest tradeBookRequest = new TradeBookRequest();
    tradeBookRequest.setClientCode(clientCode);
    if (connectIfNotConnected())
      interactiveManager.sendRequest(tradeBookRequest.getStruct(), false);
  }

  @Override
  public void sendMarginRequest(String clientCode) throws Exception {
    MarginRequest marginRequest = new MarginRequest();
    marginRequest.setClientCode(clientCode);
    if (connectIfNotConnected())
      interactiveManager.sendRequest(marginRequest.getStruct(), false);
  }

  @Override
  public synchronized void sendLimitCashOrder(CtclOrder order) throws Exception {
    interactiveManager.sendBlankPacket();
    short localOrderID = new Short(order.getLocalOrderId());
    if (connectIfNotConnected() && loggedInSuccessfully) {
      if (order.getExchange().equals("NSE"))
        orderExecutionService.sendLimitCashOrder(interactiveManager, localOrderID, order.getClientCode(), 0,
            (order.getOrderExecutionMetaData().getToken()), getSide(order.getSide()),
            order.getOrderExecutionMetaData().getSymbol(),
            (int) (order.getOrderExecutionMetaData().getLimitPrice() * 100), order.getIntegerQuantity(),
            DELIVERY.getTradeType());
      else if (order.getExchange().equals("BSE"))
        orderExecutionService.sendLimitCashOrder(interactiveManager, localOrderID, order.getClientCode(), 1,
            (order.getOrderExecutionMetaData().getToken()), getSide(order.getSide()),
            order.getOrderExecutionMetaData().getSymbol(),
            (int) (order.getOrderExecutionMetaData().getLimitPrice() * 100), order.getIntegerQuantity(),
            DELIVERY.getTradeType());
      else {
        log.error("INVALID EXCHANGE SENT IN ORDER");
        throw new Exception("Invalid exchange sent in order");
      }
    } else if (!loggedInSuccessfully)
      throw new Exception("Wrong Dealer Password");
  }

  @Override
  public synchronized void sendCancelLimitCashOrder(CancelOrder order) throws Exception {
    interactiveManager.sendBlankPacket();
    short localOrderID = new Short(order.getLocalOrderId());
    long exchangeOrderId = Long.parseLong(order.getExchangeOrderId());
    if (connectIfNotConnected() && loggedInSuccessfully) {
      if (order.getExchange().equals("NSE"))
        orderExecutionService.sendCancelLimitCashOrder(interactiveManager, localOrderID, order.getClientCode(), 0,
            order.getOrderExecutionMetaData().getToken(), getSide(order.getOrderExecutionMetaData().getOrderSide()),
            order.getOrderExecutionMetaData().getSymbol(), order.getOrderExecutionMetaData().getIntegerQuantity(),
            exchangeOrderId, DELIVERY.getTradeType());
      else if (order.getExchange().equals("BSE"))
        orderExecutionService.sendCancelLimitCashOrder(interactiveManager, localOrderID, order.getClientCode(), 1,
            order.getOrderExecutionMetaData().getToken(), getSide(order.getOrderExecutionMetaData().getOrderSide()),
            order.getOrderExecutionMetaData().getSymbol(), order.getOrderExecutionMetaData().getIntegerQuantity(),
            exchangeOrderId, DELIVERY.getTradeType());
      else {
        log.error("INVALID EXCHANGE SENT IN ORDER");
        throw new Exception("Invalid exchange sent in order");
      }
    } else if (!loggedInSuccessfully)
      throw new Exception("Wrong Dealer Password");
  }

  private synchronized boolean connectIfNotConnected() throws Exception {
    if (!interactiveManager.isConnected() && isBrokerActive())
      startInteractive();
    return interactiveManager.isConnected();
  }

  public synchronized void startInteractive() throws Exception {
    lastHeartBeatTime = System.nanoTime();
    log.info("Connecting to interactive of " + brokerName());
    if(interactiveManager.isConnected())
      interactiveManager.sendSocketClose();
    interactiveManager.connect();
    interactiveManager.addObserver(this);
    interactiveManager.login(interactiveManager, ctclXNeutrinoConfig.dealerCode(), ctclXNeutrinoConfig.dealerPan(),
        ctclXNeutrinoConfig.dealerPassword(), ctclXNeutrinoConfig.neutrinoVersion());
    orderExecutionService = new Orders(ctclXNeutrinoConfig.nnfFieldNSECash(), ctclXNeutrinoConfig.nnfFieldBSECash(),
        ctclXNeutrinoConfig.nnfFieldFo(), ctclXNeutrinoConfig.dealerCode());
  }

  private short getSide(OrderSide side) {
    if (side == OrderSide.BUY)
      return (short) 0;
    else
      return (short) 1;
  }

  @Override
  public void update(Observable o, Object arg) {
    if (arg instanceof HeartBeat) {
      lastHeartBeatTime = System.nanoTime();
      log.debug("HeartBeat Received time: "+lastHeartBeatTime);
    } else if (arg instanceof LoginResponse) {
      LoginResponse loginResponse = (LoginResponse) arg;
      lastHeartBeatTime = System.nanoTime();
      if (loginResponse.getSuccess() == 1) {
        loggedInSuccessfully = true;
        log.info("Dealer logged in with correct password");
      } else if (loginResponse.getSuccess() == 0) {
        try {
          sendLoginFailureToDevTeam();
          loggedInSuccessfully = false;
          log.info("Wrong Dealer password entered in the config");
        } catch (IOException e) {
          log.error("Could not send email to Dev emails for dealer login failure - Error Details: " + e);
        }
      }
    } else if (arg instanceof LogoffResponse) {
      LogoffResponse logoffResponse = (LogoffResponse) arg;
      lastHeartBeatTime = System.nanoTime();
      if (logoffResponse.getStatus() == -2) {
        loggedInSuccessfully = false;
      }
    } else {
      Object message = getMessage(arg);
      if (message != null) {
        actorSelections.equityOrderRootActor().tell(message, ActorRef.noSender());
      } else
        log.error("Null packet received " + message);
    }
  }

  private Object getMessage(Object arg) {
    return messageFactory.getMessage(arg);
  }

  private void sendLoginFailureToDevTeam() throws IOException {
    WrongDealerPasswordEmail template = new WrongDealerPasswordEmail(properties.DEV_TEAM_EMAILIDS(), properties.environment());
    if (properties.isSendingUserEmailUpdatesAllowed())
      emailerService.sendEmail(template);
  }

  private void sendCtclConnectionFailureToDevTeam() throws IOException {
    CTCLConnectionFailureEmail template = new CTCLConnectionFailureEmail(properties.DEV_TEAM_EMAILIDS());
    if (properties.isSendingUserEmailUpdatesAllowed())
      emailerService.sendEmail(template);
  }
  
  @Async
  @Scheduled(cron = "${ctcl.schedule}", zone = "Asia/Kolkata")
  public void heartBeatCheck() throws Exception {
    if ((System.nanoTime() > (lastHeartBeatTime + timeBenchmark)) && isBrokerActive())
      startInteractive();
    else if (lastHeartBeatTime == 0 && !isBrokerActive())
      log.debug("Checked hearbeat for " + brokerName() + " but not starting interative for broker " + brokerName()
          + " as it is inactive!");
    else if (lastHeartBeatTime != 0 && isBrokerActive()) 
      log.debug("Checked hearbeat for " + brokerName() + " but not starting interative for broker " + brokerName()
      + " as it is already connected!");
  }
}