package com.wt.ctcl.xneutrino;

import static com.wt.domain.BrokerName.SYKES;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;

import java.io.IOException;

import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.xneutrino.util.CtclXNeutrinoConfig;
import com.wt.ctcl.xneutrino.util.MessageFactory;
import com.wt.domain.BrokerName;
import com.wt.domain.BrokerType;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;

import akka.actor.ActorSystem;
import lombok.extern.log4j.Log4j;

@Log4j
public class SYKESInteractive extends XNeutrinoInteractive {

  public SYKESInteractive(InteractiveManager interactiveManager, ActorSystem actorSystem,
      WDActorSelections actorSelections, MessageFactory messageFactory, CtclXNeutrinoConfig ctclXNeutrinoConfig,
      SESEmailUtil emailerService, WDProperties properties) throws IOException {
    super(interactiveManager, actorSystem, actorSelections, messageFactory, ctclXNeutrinoConfig, emailerService,
        properties);
  }

  @Override
  public BrokerType getBrokerType() {
    return new BrokerType(EQUITYSUBSCRIPTION, REAL, SYKES);
  }

  @Override
  protected boolean isBrokerActive() {
    try {
      return properties.ACTIVE_BROKERS().contains(SYKES.getBrokerName());
    } catch (Exception e) {
      log.error("Broker could not be found in the active broker list!" + e);
    }
    return false;
  }

  @Override
  protected BrokerName brokerName() {
    return SYKES;
  }

}
