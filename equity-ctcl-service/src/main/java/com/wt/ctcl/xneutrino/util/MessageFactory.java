package com.wt.ctcl.xneutrino.util;

import org.springframework.stereotype.Component;

import com.ctcl.xneutrino.interactive.ExchangeConfirmation;
import com.ctcl.xneutrino.interactive.MarginResponse;
import com.ctcl.xneutrino.interactive.OrderConfirmation;
import com.ctcl.xneutrino.interactive.OrderRetrieved;
import com.ctcl.xneutrino.interactive.TradeConfirmation;
import com.wt.domain.MarginRequestMessage;
import com.wt.domain.MessageType;
import com.wt.domain.OrderBookMessage;
import com.wt.domain.OrderSide;
import com.wt.domain.write.commands.BrokerOrderAccepted;
import com.wt.domain.write.commands.BrokerOrderRejected;
import com.wt.domain.write.commands.ExchangeOrderAccepted;
import com.wt.domain.write.commands.ExchangeOrderRejected;
import com.wt.domain.write.commands.TradeOrderConfirmation;

import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class MessageFactory {

  public MessageType getMessage(Object receivedMessage) {
    log.debug("Received from broker ctcl: "+receivedMessage);
    if (receivedMessage instanceof OrderConfirmation) {
      OrderConfirmation orderConfirmation = (OrderConfirmation) receivedMessage;
      if (orderConfirmation.getStatus() == 2)
        return new BrokerOrderAccepted(
            String.valueOf(orderConfirmation.getLocalOrderID()), 
            String.valueOf(orderConfirmation.getBrokerOrderID()),
            orderConfirmation.getClientCode(), 
            String.valueOf(orderConfirmation.getToken()), 
            orderConfirmation.getQty(), 
            getOrderSide(orderConfirmation.getOrderType()));
      else if (orderConfirmation.getStatus() == 6)
        return new BrokerOrderRejected(
            String.valueOf(orderConfirmation.getLocalOrderID()), 
            String.valueOf(orderConfirmation.getBrokerOrderID()),
            orderConfirmation.getMessage(),
            orderConfirmation.getClientCode(), 
            String.valueOf(orderConfirmation.getToken()), 
            orderConfirmation.getQty(), 
            getOrderSide(orderConfirmation.getOrderType()));
    } else if (receivedMessage instanceof ExchangeConfirmation) {
      ExchangeConfirmation exchangeConfirmation = (ExchangeConfirmation) receivedMessage;
      if (exchangeConfirmation.getStatus() == 3)
        return new ExchangeOrderAccepted(
            exchangeConfirmation.getClientCode(), 
            String.valueOf(exchangeConfirmation.getBrokerOrderId()), 
            String.valueOf(exchangeConfirmation.getExchOrderID()),
            exchangeConfirmation.getExchOrderTime(),
            String.valueOf(exchangeConfirmation.getToken()), 
            exchangeConfirmation.getQty(), 
            getOrderSide(exchangeConfirmation.getOrderType()));
      else if (exchangeConfirmation.getStatus() == 7 || exchangeConfirmation.getStatus() == 8
          || exchangeConfirmation.getStatus() == 9)
        return new ExchangeOrderRejected(
            exchangeConfirmation.getClientCode(), 
            String.valueOf(exchangeConfirmation.getBrokerOrderId()), 
            String.valueOf(exchangeConfirmation.getExchOrderID()),
            exchangeConfirmation.getExchOrderTime(),
            String.valueOf(exchangeConfirmation.getToken()),
            exchangeConfirmation.getQty(),
            getOrderSide(exchangeConfirmation.getOrderType()),
            exchangeConfirmation.getMessage());
    } else if (receivedMessage instanceof TradeConfirmation) {
      TradeConfirmation tradeConfirmation = (TradeConfirmation) receivedMessage;
      if (tradeConfirmation.getStatus() == 4 || tradeConfirmation.getStatus() == 13)
        return new TradeOrderConfirmation(String.valueOf(tradeConfirmation.getExchangeOrderID()),
            String.valueOf(tradeConfirmation.getBrokerOrderID()), String.valueOf(tradeConfirmation.getExchangeTradeID()), 
            tradeConfirmation.getTradePrice() / 100f, tradeConfirmation.getTradeQty(),
            tradeConfirmation.getExchangeTradeTime(),tradeConfirmation.getClientCode(),
            String.valueOf(tradeConfirmation.getToken()), tradeConfirmation.getOrderQty(), 
            getOrderSide(tradeConfirmation.getOrderType()));
    } else if(receivedMessage instanceof OrderRetrieved){
      OrderRetrieved orderRetrieved = (OrderRetrieved) receivedMessage;
      return new OrderBookMessage(
          orderRetrieved.getClientCode(), 
          orderRetrieved.getLocalOrderID(), 
          orderRetrieved.getBrokerOrderID(), 
          orderRetrieved.getExchangeOrderID(),
          orderRetrieved.getExchangeOrderTime(), 
          String.valueOf(orderRetrieved.getToken()), 
          orderRetrieved.getQty(),
          getOrderSide(orderRetrieved.getOrderType()), 
          orderRetrieved.getMsg().trim());
    } else if(receivedMessage instanceof MarginResponse){
      MarginResponse marginResponse = (MarginResponse) receivedMessage;
      return new MarginRequestMessage(
          marginResponse.getClientCode(),
          (marginResponse.getAvaliableMargin() - marginResponse.getPDHV()) / 100.0);
    }

    return null;
  }
  
  private OrderSide getOrderSide(short side) {
    if (side == 0)
      return OrderSide.BUY;
    else
      return OrderSide.SELL;
  }
}