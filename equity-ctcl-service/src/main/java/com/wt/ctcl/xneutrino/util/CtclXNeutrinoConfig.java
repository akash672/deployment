package com.wt.ctcl.xneutrino.util;

import java.io.IOException;

public abstract class CtclXNeutrinoConfig {

  public long nnfFieldNSECash() throws IOException {
    String key = "nnf.field.nse.cash";
    return Long.parseLong(property(key).toString());
  }

  public long nnfFieldBSECash() throws IOException {
    String key = "nnf.field.bse.cash";
    return Long.parseLong(property(key).toString());
  }

  public long nnfFieldFo() throws IOException {
    String key = "nnf.field.fo";
    return Long.parseLong(property(key).toString());
  }

  public String dealerCode() throws IOException {
    String key = "dealer.code";
    return property(key).toString();
  }

  public String dealerPan() throws IOException {
    String key = "dealer.pan";
    return property(key).toString();
  }

  public String dealerPassword() throws IOException {
    String key = "dealer.password";
    return property(key).toString();
  }

  public String neutrinoVersion() throws IOException {
    String key = "neutrino.version";
    return property(key).toString();
  }

  public abstract Object property(String key) throws IOException;

}