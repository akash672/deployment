package com.wt.universe.util;

import static com.google.common.collect.Lists.newArrayList;
import static com.typesafe.config.ConfigFactory.load;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.typesafe.config.Config;
import com.wt.config.utils.WDProperties;
import com.wt.ctcl.BPWCorporateEventCTCLInteractive;
import com.wt.ctcl.BPWCorporateEventCTCLReal;
import com.wt.ctcl.BPWOnboardingCTCLInteractive;
import com.wt.ctcl.CTCLMock;
import com.wt.ctcl.CorporateEventCTCLMock;
import com.wt.ctcl.MOSLCorporateEventCTCLReal;
import com.wt.ctcl.SYKESCorporateEventCTCLInteractive;
import com.wt.ctcl.SYKESOnboardingCTCLInteractive;
import com.wt.ctcl.VirtualMutualFundInteractive;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.ctcl.mosl.MOSLInteractive;
import com.wt.ctcl.mosl.util.MOSLCtclConfig;
import com.wt.ctcl.xneutrino.BPWInteractive;
import com.wt.ctcl.xneutrino.SYKESInteractive;
import com.wt.ctcl.xneutrino.util.BPWXNeutrinoConfig;
import com.wt.ctcl.xneutrino.util.MessageFactory;
import com.wt.ctcl.xneutrino.util.SYKESXNeutrinoConfig;
import com.wt.domain.BrokerType;
import com.wt.utils.akka.SpringExtension;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;

import akka.actor.ActorSystem;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan(basePackages = "com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:environment.properties")
public class EquityCtclServiceAppConfiguration {
  private ApplicationContext applicationContext;

  @Autowired
  public EquityCtclServiceAppConfiguration(ApplicationContext applicationContext) {
    super();
    this.applicationContext = applicationContext;
  }

  @Autowired
  public MessageFactory messageFactory;

  @Value("${amazon.aws.ses.accesskey}")
  private String amazonSESAccessKey;

  @Value("${amazon.aws.ses.secretkey}")
  private String amazonSESSecretKey;

  @Autowired
  public BPWXNeutrinoConfig bpwCtclXNeutrinoConfig;
  
  @Autowired
  public SYKESXNeutrinoConfig sykesCtclXNeutrinoConfig;
  
  @Autowired
  public MOSLCtclConfig moslCtclConfig;

  @Autowired
  public SESEmailUtil emailerService;

  @Autowired
  @Lazy
  protected WDActorSelections wdActorSelections;

  @Autowired
  protected WDProperties wdProperties;

  public void setSystemProperties() {
    System.setProperty("environment", wdProperties.environment());
  }

  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
    return properties;
  }

  @Bean
  public List<CtclInteractive> listOfCTCLs() throws Exception {
    return newArrayList(mockCTCL(), bpwInteractive(), sykesInteractive(), moslInteractive(), bpwOnboardingCTCLInteractive(), sykesOnboardingCTCLInteractive(),
        corporateEventCtclMock(), bpwCorporateEventCtclInteractive(), sykesCorporateEventCtclInteractive(), bpwCorporateEventCtclReal(),
        moslCorporateEventCtclReal());
  }

  @Bean
  public Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation() throws Exception {
    Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation = new HashMap<BrokerType, CtclInteractive>();
    listOfCTCLs().stream().forEach(ctcl -> brokerTypeWithCTCLImplementation.put(ctcl.getBrokerType(), ctcl));
    return brokerTypeWithCTCLImplementation;
  }

  @Bean
  public Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation() throws Exception {
    return new HashMap<BrokerType, MutualFundInteractive>();
  }

  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() throws IOException {
    setSystemProperties();
    String config = "EquityCTCL-application.conf";
    String actorName = "EquityCTCL";
    if (System.getProperty("appConfig") != null) {
      config = System.getProperty("appConfig");
      actorName = System.getProperty("appConfig").split("-")[0];
    }
    Config conf = load(config);
    ActorSystem system = ActorSystem.create(actorName, conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }

  @Bean(name = "BPWInteractiveManager")
  public InteractiveManager bpwInteractiveManager() throws IOException {
    InteractiveManager interactiveManager = new InteractiveManager(wdProperties.BPW_XNEUTRINO_INTERACTIVE_IP(),
        wdProperties.BPW_XNEUTRINO_INTERACTIVE_PORT());
    return interactiveManager;
  }

  @Bean(name = "BPWInteractive")
  public CtclInteractive bpwInteractive() throws Exception {
    return new BPWInteractive(bpwInteractiveManager(), actorSystem(), wdActorSelections, messageFactory,
        bpwCtclXNeutrinoConfig, emailerService, wdProperties);
  }

  @Bean(name = "SYKESInteractiveManager")
  public InteractiveManager sykesInteractiveManager() throws IOException {
    InteractiveManager interactiveManager = new InteractiveManager(wdProperties.SYKES_XNEUTRINO_INTERACTIVE_IP(),
        wdProperties.SYKES_XNEUTRINO_INTERACTIVE_PORT());
    return interactiveManager;
  }

  @Bean(name = "SYKESInteractive")
  public CtclInteractive sykesInteractive() throws Exception {
    return new SYKESInteractive(sykesInteractiveManager(), actorSystem(), wdActorSelections, messageFactory,
        sykesCtclXNeutrinoConfig, emailerService, wdProperties);
  }

  @Bean(name = "CTCLMock")
  public CtclInteractive mockCTCL() throws IOException {
    return new CTCLMock(actorSystem(), wdActorSelections);
  }

  @Bean(name = "BPWOnboardingCTCLInteractive")
  public CtclInteractive bpwOnboardingCTCLInteractive() {
    return new BPWOnboardingCTCLInteractive(wdActorSelections);
  }

  @Bean(name = "SYKESOnboardingCTCLInteractive")
  public CtclInteractive sykesOnboardingCTCLInteractive() {
    return new SYKESOnboardingCTCLInteractive(wdActorSelections);
  }

  @Bean(name = "CorporateEventCTCLMock")
  public CtclInteractive corporateEventCtclMock() {
    return new CorporateEventCTCLMock(wdActorSelections);
  }
  
  @Bean(name = "BPWCorporateEventCTCLReal")
  public CtclInteractive bpwCorporateEventCtclReal() {
    return new BPWCorporateEventCTCLReal(wdActorSelections);
  }
  
  @Bean(name = "MOSLCorporateEventCTCLReal")
  public CtclInteractive moslCorporateEventCtclReal() {
    return new MOSLCorporateEventCTCLReal(wdActorSelections);
  }

  @Bean(name = "BPWCorporateEventCTCLInteractive")
  public CtclInteractive bpwCorporateEventCtclInteractive() {
    return new BPWCorporateEventCTCLInteractive(wdActorSelections);
  }
  @Bean(name = "MOSLInteractive")
  public CtclInteractive moslInteractive() throws IOException {
    return new MOSLInteractive(wdActorSelections, moslCtclConfig);
  }

  @Bean(name = "SYKESCorporateEventCTCLInteractive")
  public CtclInteractive sykesCorporateEventCtclInteractive() {
    return new SYKESCorporateEventCTCLInteractive(wdActorSelections);
  }
  
  @Bean( name = "VirtualMutualFundInteractive")
  public MutualFundInteractive virtualMutualFundInteractive(){
    return new VirtualMutualFundInteractive(wdActorSelections);
  }
}
