package com.wt.domain;

import static akka.stream.ThrottleMode.shaping;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;
import com.wt.domain.write.ParentActor;
import com.wt.domain.write.commands.BODCompleted;
import com.wt.domain.write.commands.GetActiveTokens;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.GetWdIdToInstrumentMap;
import com.wt.domain.write.commands.SendStreamUpdate;
import com.wt.domain.write.commands.StartRealtimePerformance;
import com.wt.domain.write.commands.StopRealtimePerformance;
import com.wt.domain.write.commands.SubscribeToFollowingTickers;
import com.wt.domain.write.commands.SubscribeToToken;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.commands.UpdateInstrumentsMap;
import com.wt.domain.write.commands.WdIdToInstrumentMapSent;
import com.wt.domain.write.events.Failed;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.UntypedAbstractActor;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import scala.collection.JavaConverters;

@Scope("prototype")
@Service("RealtimeFundPerformanceRoot")
public class RealtimeFundPerformanceRoot extends UntypedAbstractActor implements ParentActor<RealtimeFundPerformance> {

  SendStreamUpdate sendPerformance = new SendStreamUpdate();
  private Set<String> listOfChildrens = new HashSet<String>();
  private Set<String> tokensInLiveMarket = new HashSet<String>();
  private WDActorSelections wdActorSelection;
  private WDProperties wdproperties;
  private Map<String, EquityInstrument> wdIdToEquityInstrumentMap = new HashMap<>();
  
  @Autowired
  public void setWdActorSelection( WDActorSelections wdActorSelection) {
    this.wdActorSelection = wdActorSelection;
  }
  
  @Autowired
  public void setWdProperties( WDProperties wdproperties) {
    this.wdproperties = wdproperties;
  }

  @Override
  public void onReceive(Object msg) {
    if (msg instanceof StopRealtimePerformance) {
      StopRealtimePerformance stopRequest = (StopRealtimePerformance) msg;
      tellChildren(context(), sender(), msg,
          "RealtimePerf-" + stopRequest.getAdviserUsername() + stopRequest.getFundName());
    } else if(msg instanceof StartRealtimePerformance){
       tellChildrenToStartCalculatingPerformance(msg);
    } else if (msg instanceof BODCompleted) {
      for (Iterator<String> iterator = listOfChildrens.iterator(); iterator.hasNext();) {
        String childId = (String) iterator.next();
        tellChildren(context(), self(), msg, childId);
      }
      this.listOfChildrens = new HashSet<String>();
    } else if (msg instanceof SubscribeToFollowingTickers) {
      sendCommandForSubscription(msg);
    } else if(msg instanceof UpdateInstrumentsMap){
      updateWdIdtoInstrumentsMap();
    } else if(msg instanceof GetSymbolForWdId){
      sendSymbolForWdId(msg);
    }
  }
  
  private void sendSymbolForWdId(Object cmd) {
    GetSymbolForWdId getSymbolForWdId = (GetSymbolForWdId) cmd;
    if (!wdIdToEquityInstrumentMap.containsKey(getSymbolForWdId.getWdId())){
      updateWdIdtoInstrumentsMap();
    }
    if (wdIdToEquityInstrumentMap.containsKey(getSymbolForWdId.getWdId())) {
      String symbol = wdIdToEquityInstrumentMap.get(getSymbolForWdId.getWdId()).getSymbol();
      sender().tell(new SymbolSentForWdId(getSymbolForWdId.getWdId(), symbol), self());
      return;
    } 
      
    sender().tell(new SymbolSentForWdId(getSymbolForWdId.getWdId(), getSymbolForWdId.getWdId()), self());
    
  }

  private void tellChildrenToStartCalculatingPerformance(Object msg) {
    requestForActiveTokens();
    StartRealtimePerformance startRequest = (StartRealtimePerformance) msg;
    String adviserFundPerformancePersistenceId = "RealtimePerf-" + startRequest.getAdviserUsername()
        + startRequest.getFundName();
    this.listOfChildrens.add(adviserFundPerformancePersistenceId);
    tellChildren(context(), sender(), msg, adviserFundPerformancePersistenceId);
  }

  private void requestForActiveTokens() {
    if(tokensInLiveMarket.isEmpty())
      wdActorSelection.activeInstrumentTracker().tell(new GetActiveTokens(), self());
  }

  private void sendCommandForSubscription(Object msg) {
    SubscribeToFollowingTickers subscribeToFollowingTickers = (SubscribeToFollowingTickers) msg;
    keepTokensInLiveMarketUpdated(subscribeToFollowingTickers.getTickers());
    sendSubscriptionCallsToUniverseActor();

}

  private void sendSubscriptionCallsToUniverseActor() {
    Source.from(tokensInLiveMarket)
        .throttle(wdproperties.NUMBER_OF_CHILDREN_TO_RECOVER(),
            Duration.ofSeconds(wdproperties.CHILD_RECOVERY_FREQUENCY()),
            wdproperties.CHILD_RECOVERY_MAXIMUM_BURST(), shaping())
        .runForeach(ticker -> {
          wdActorSelection.universeRootActor().tell(new SubscribeToToken(ticker), self());
        }, ActorMaterializer.create(context().system()));
  }
  
  private void updateWdIdtoInstrumentsMap() {

    WdIdToInstrumentMapSent wdIdToInstrumentMapSent = null;
    boolean instrumentsMapReceived = false;
    int retryCounter = 1;
    while (!instrumentsMapReceived && retryCounter <= 5) {
      try {
        Object result = askAndWaitForResult(wdActorSelection.universeRootActor(), new GetWdIdToInstrumentMap());

        if (result instanceof Failed) {
          retryCounter++;
          log.warn("RealtimeFundPerformanceRoot :Failed to get instruments by wdId from EquityActor. Retrying " + result);
          continue;
        }
        wdIdToInstrumentMapSent = (WdIdToInstrumentMapSent) result;
        wdIdToEquityInstrumentMap = wdIdToInstrumentMapSent.getWdIdtoInstruments();
        instrumentsMapReceived = true;
        log.info("RealtimeFundPerformanceRoot : wdIdToInstrument Map updated");
      } catch (Exception e) {
        log.warn("RealtimeFundPerformanceRoot :Exception while getting instruments by wdId from EquityActor. Retrying ", e);
        retryCounter++;
      }
    }
    if(retryCounter >5 )
      log.error("RealtimeFundPerformanceRoot : Failed to get instruments map for universe actor after multiple attempts.");
  }

private void keepTokensInLiveMarketUpdated(Set<String> tickers) {
  tickers.stream().forEach(ticker -> tokensInLiveMarket.add(ticker));
  }

@Scheduled(cron = "${realtime.tick.schedule}", zone = "Asia/Kolkata")
  public void sendFundPerformanceToDestinationActors() {
    JavaConverters.asJavaCollectionConverter(context().children()).asJavaCollection().stream()
        .forEach(actor -> actor.tell(sendPerformance, self()));
  }

  @Override
  public String getName() {
    return "RealtimeFundPerformanceRoot";
  }

  @Override
  public String getChildBeanName() {
    return "RealtimeFundPerformance";
  }

}
