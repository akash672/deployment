package com.wt.domain.write.commands;

import java.io.Serializable;

import akka.actor.ActorRef;
import lombok.Getter;

@Getter
public class StartRealtimePriceReceiving implements Serializable{

  String wdId;
  String connectionId;
  ActorRef destinationRef;
  
  public StartRealtimePriceReceiving(String wdId, String connectionId, ActorRef destinationRef) {
    this.wdId = wdId;
    this.connectionId = connectionId;
    this.destinationRef = destinationRef;
  }

  private static final long serialVersionUID = 1L;
  
}
