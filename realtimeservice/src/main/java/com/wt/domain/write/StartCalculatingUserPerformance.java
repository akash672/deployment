package com.wt.domain.write;

import java.io.Serializable;

import com.wt.domain.UserAdvise;

import akka.actor.ActorRef;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class StartCalculatingUserPerformance implements Serializable {

  private static final long serialVersionUID = 1L;
  private UserAdvise userAdvise;
  private ActorRef sender;

}
