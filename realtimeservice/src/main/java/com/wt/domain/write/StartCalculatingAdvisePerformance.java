package com.wt.domain.write;

import java.io.Serializable;

import com.wt.domain.Advise;

import akka.actor.ActorRef;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class StartCalculatingAdvisePerformance implements Serializable {

  private static final long serialVersionUID = 1L;
  private Advise advise;
  private ActorRef sender;
  
}
