package com.wt.domain.write.commands;

import com.wt.domain.InvestingMode;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class StopInvestorRealtimePerformance  {
  private String investorUsername;
  private String connectionId;
  private InvestingMode investingMode;
}
