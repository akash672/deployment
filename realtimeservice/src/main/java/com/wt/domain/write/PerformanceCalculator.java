package com.wt.domain.write;

import static akka.stream.ThrottleMode.shaping;

import java.time.Duration;

import org.reactivestreams.Publisher;

import com.wt.broadcast.xneutrino.CancellablePriceStream;
import com.wt.domain.Instrument;
import com.wt.domain.PerformanceStream;
import com.wt.domain.PriceWithPaf;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.AsPublisher;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

public interface PerformanceCalculator<T> {

  public abstract PerformanceStream<T> realtime(ActorMaterializer materializer, ActorRef actor);

  public default CancellablePriceStream<PriceWithPaf, NotUsed> cancellablePriceStream(Instrument instrument,
      ActorSystem actorSystem, int streamPriceTickFrequency, long streamPriceTickInterval,
      int streamPriceTickMaximumBurst) {
    Pair<ActorRef, Publisher<Object>> sinkActorForPriceStreamWithPublisher = sinkActorForPriceStreamWithPublisher(
        actorSystem);
    return new CancellablePriceStream<PriceWithPaf, NotUsed>(
        Source.fromPublisher(sinkActorForPriceStreamWithPublisher.second())
            .map(f -> (PriceWithPaf) f)
            .via(new SendLastReceived<PriceWithPaf>())
            .throttle(streamPriceTickFrequency, Duration.ofSeconds(streamPriceTickInterval), streamPriceTickMaximumBurst, shaping()),
        sinkActorForPriceStreamWithPublisher.first(), instrument);
  }

  public default Pair<ActorRef, Publisher<Object>> sinkActorForPriceStreamWithPublisher(ActorSystem actorSystem) {
    Pair<ActorRef, Publisher<Object>> sinkActorForPriceStreamWithPublisher = Source
        .actorRef(1, OverflowStrategy.dropHead()).toMat(Sink.asPublisher(AsPublisher.WITH_FANOUT), Keep.both())
        .run(ActorMaterializer.create(actorSystem));
    return sinkActorForPriceStreamWithPublisher;
  }
}
