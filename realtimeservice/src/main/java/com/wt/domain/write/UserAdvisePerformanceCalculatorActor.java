package com.wt.domain.write;

import static akka.stream.ActorMaterializer.create;
import static java.util.concurrent.TimeUnit.SECONDS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.broadcast.xneutrino.CancellablePriceStream;
import com.wt.config.utils.WDProperties;
import com.wt.domain.EquityInstrument;
import com.wt.domain.Instrument;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.PerformanceStream;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.UserAdvise;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.StopCalculatingPerformance;
import com.wt.domain.write.commands.StopLivePriceStream;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceCalculated;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.PoisonPill;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.KillSwitches;
import akka.stream.UniqueKillSwitch;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.MergeHub;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import scala.concurrent.duration.FiniteDuration;

@Service("UserAdvisePerformanceCalculatorActor")
@Scope("prototype")
public class UserAdvisePerformanceCalculatorActor extends AbstractActor implements PerformanceCalculator<UserAdviseAbsolutePerformanceCalculated> {

  private ActorRef subscriberActor;
  private WDActorSelections wdActorSelections;
  private WDProperties wdProperties;
  private ActorRef sender;
  private PerformanceStream<UserAdviseAbsolutePerformanceCalculated> performanceStream;
  Cancellable cancellablePriceStreamRequest;
  private String wdId;
  private boolean isUserAdvisePerformanceStreamRunning;
  @SuppressWarnings("unused")
  private String identifier;

  public UserAdvisePerformanceCalculatorActor(String identifier) {
    super();
    this.identifier = identifier;
    this.isUserAdvisePerformanceStreamRunning = false;
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(PriceWithPaf.class, priceWithPaf -> {
      if (subscriberActor != null && priceWithPaf != null) {
        subscriberActor.tell(priceWithPaf, self());
        cancellablePriceStreamRequest.cancel();
      }
    }).match(StartCalculatingUserPerformance.class, cmd -> {
      handleStartCalculatingPerformance(cmd);
    }).match(StopCalculatingPerformance.class, cmd -> {
      handleStopCalculatingPerformance();
    }).build();
  }

  private void handleStartCalculatingPerformance(StartCalculatingUserPerformance cmd) {
    StartCalculatingUserPerformance startCalculatingPerformance = (StartCalculatingUserPerformance) cmd;
    initializeTickerAndTheSender(startCalculatingPerformance);
    requestUniverseRootToSendPrices();
    calculatePerformanceFor(startCalculatingPerformance.getUserAdvise());
    isUserAdvisePerformanceStreamRunning = true;
  }

  private void initializeTickerAndTheSender(StartCalculatingUserPerformance startCalculatingPerformance) {
    wdId = startCalculatingPerformance.getUserAdvise().getToken();
    sender = startCalculatingPerformance.getSender();
  }

  private void requestUniverseRootToSendPrices() {
    cancellablePriceStreamRequest = context().system()
    .scheduler()
    .schedule(FiniteDuration.Zero(), FiniteDuration.create(wdProperties.STREAM_PRICE_TICK_INTERVAL(), SECONDS), 
        () -> {
          wdActorSelections.universeRootActor().tell(new GetLivePriceStream(wdId), self());
    }, context().system().dispatcher());
  }
  
  private void calculatePerformanceFor(UserAdvise userAdvise) {
    Instrument instrument = wdId.contains("-MF") ? new MutualFundInstrument(wdId) : new EquityInstrument(wdId, userAdvise.getToken(), userAdvise.getSymbol(),userAdvise.getDescription(),"");
    if (!isUserAdvisePerformanceStreamRunning)
      performanceStream = realtime(create(context()), sender);

    CancellablePriceStream<PriceWithPaf, NotUsed> cancellablePriceStream = cancellablePriceStream(instrument,
        context().system(), wdProperties.STREAM_PRICE_TICK_FREQUENCY(), wdProperties.STREAM_PRICE_TICK_INTERVAL(),
        wdProperties.STREAM_PRICE_TICK_MAXIMUM_BURST());

    performanceStream.include(userAdvise.realtimePerformanceUsing(cancellablePriceStream), create(context()));

    subscriberActor = cancellablePriceStream.getActor();
  }

  private void handleStopCalculatingPerformance() {
    isUserAdvisePerformanceStreamRunning = false;
    performanceStream.cancel();
    context().stop(self());
    subscriberActor.tell(PoisonPill.getInstance(), self());
    wdActorSelections.universeRootActor().tell(new StopLivePriceStream(wdId), self());
  }

  @Override
  public PerformanceStream<UserAdviseAbsolutePerformanceCalculated> realtime(ActorMaterializer materializer,
      ActorRef actor) {
    Sink<UserAdviseAbsolutePerformanceCalculated, NotUsed> consumer = Sink.actorRef(actor, PoisonPill.getInstance());

    RunnableGraph<Pair<UniqueKillSwitch,NotUsed>> runnableGraph = MergeHub
        .of(UserAdviseAbsolutePerformanceCalculated.class)
        .viaMat(KillSwitches.single(), Keep.right())
        .toMat(consumer, Keep.both());

    Pair<UniqueKillSwitch, NotUsed> toConsumer = runnableGraph.run(materializer);

    return new PerformanceStream<UserAdviseAbsolutePerformanceCalculated>(consumer, toConsumer.first());
  }

  @Autowired
  public void setWdActorSelections(WDActorSelections wdActorSelections) {
    this.wdActorSelections = wdActorSelections;
  }

  @Autowired
  public void setWdProperties(WDProperties wdProperties) {
    this.wdProperties = wdProperties;
  }

}
