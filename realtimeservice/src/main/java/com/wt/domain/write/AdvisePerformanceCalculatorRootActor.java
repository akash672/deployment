package com.wt.domain.write;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.write.commands.StopCalculatingPerformance;
import com.wt.domain.write.commands.StopRealtimePerformanceForCompletelyExitedAdvises;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Scope("prototype")
@Service("AdvisePerformanceCalculatorRootActor")
public class AdvisePerformanceCalculatorRootActor extends AbstractActor implements ParentActor<AdvisePerformanceCalculatorActor> {
  private String identifier;
  private Map<String, ActorRef> adviseIdToItsPerformanceCalculator = new ConcurrentHashMap<String, ActorRef>();

  public AdvisePerformanceCalculatorRootActor(String identifier) {
    super();
    this.identifier = identifier;
    this.adviseIdToItsPerformanceCalculator = new ConcurrentHashMap<String, ActorRef>();
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(StartCalculatingAdvisePerformance.class, cmd -> {
      StartCalculatingAdvisePerformance startCalculatingPerformance = (StartCalculatingAdvisePerformance) cmd;
      String childName = startCalculatingPerformance.getAdvise().getId() + CompositeKeySeparator
              + startCalculatingPerformance.getAdvise().getAdviserUsername() + CompositeKeySeparator
              + startCalculatingPerformance.getAdvise().getFund();
      forwardToChildren(context(), startCalculatingPerformance, childName);
      adviseIdToItsPerformanceCalculator.put(startCalculatingPerformance.getAdvise().getId(),
          findChild(context(), childName));
    }).match(StopRealtimePerformanceForCompletelyExitedAdvises.class, cmd -> {
      stopCompletelyExitedAdvisePerformanceCalculations(cmd);
    }).build();
  }

  private void stopCompletelyExitedAdvisePerformanceCalculations(
      StopRealtimePerformanceForCompletelyExitedAdvises stopCalculatingPerformance) {
    adviseIdToItsPerformanceCalculator.forEach((adviseId, performanceCalculator) -> {
      boolean anyMatch = stopCalculatingPerformance.getActiveAdvises().stream().anyMatch(activeAdviseId -> {
        return activeAdviseId.equals(adviseId);
      });
      if (!anyMatch) {
        performanceCalculator.tell(new StopCalculatingPerformance(), self());
      }
    });
  }

  @Override
  public String getName() {
    return identifier;
  }

  @Override
  public String getChildBeanName() {
    return "AdvisePerformanceCalculatorActor";
  }
}
