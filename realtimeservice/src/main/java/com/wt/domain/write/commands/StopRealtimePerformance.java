package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class StopRealtimePerformance {
  @Getter
  private String adviserUsername;
  @Getter
  private String fundName;
  @Getter
  private String connectionId;
}
