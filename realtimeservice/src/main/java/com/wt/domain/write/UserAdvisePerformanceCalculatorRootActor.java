package com.wt.domain.write;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.write.commands.StopCalculatingPerformance;

import akka.actor.AbstractActor;
import lombok.EqualsAndHashCode;
import scala.collection.JavaConverters;

@EqualsAndHashCode(callSuper = true)
@Service("UserAdvisePerformanceCalculatorRootActor")
@Scope("prototype")
public class UserAdvisePerformanceCalculatorRootActor extends AbstractActor implements ParentActor<UserAdvisePerformanceCalculatorActor> {
  private String identifier;

  public UserAdvisePerformanceCalculatorRootActor(String identifier) {
    super();
    this.identifier = identifier;
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(StartCalculatingUserPerformance.class, cmd -> {
      StartCalculatingUserPerformance startCalculatingPerformance = (StartCalculatingUserPerformance) cmd;
      forwardToChildren(context(), startCalculatingPerformance,
                startCalculatingPerformance.getUserAdvise().getAdviseId() 
              + CompositeKeySeparator
              + startCalculatingPerformance.getUserAdvise().getUserAdviseCompositeKey().getUsername()
              + CompositeKeySeparator
              + startCalculatingPerformance.getUserAdvise().getUserAdviseCompositeKey().getFund()
              + CompositeKeySeparator
              + startCalculatingPerformance.getUserAdvise().getUserAdviseCompositeKey().getAdviser()
              + CompositeKeySeparator
              + startCalculatingPerformance.getUserAdvise().getUserAdviseCompositeKey().getInvestingMode());
    }).match(StopCalculatingPerformance.class, cmd -> {
      StopCalculatingPerformance stopCalculatingPerformance = (StopCalculatingPerformance) cmd;
      JavaConverters.asJavaCollectionConverter(context().children()).asJavaCollection().stream()
          .forEach(actor -> actor.tell(stopCalculatingPerformance, self()));
    }).build();
  }

  @Override
  public String getName() {
    return identifier;
  }

  @Override
  public String getChildBeanName() {
    return "UserAdvisePerformanceCalculatorActor";
  }
}
