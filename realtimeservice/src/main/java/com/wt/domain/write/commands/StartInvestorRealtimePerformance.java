package com.wt.domain.write.commands;

import com.wt.domain.InvestingMode;

import akka.actor.ActorRef;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class StartInvestorRealtimePerformance  {
  private String investorUsername;
  private InvestingMode investingMode;
  private String connectionId;
  private ActorRef destinationRef;
  
}
