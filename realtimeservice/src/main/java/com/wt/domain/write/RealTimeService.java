package com.wt.domain.write;

import static akka.http.javadsl.model.ws.TextMessage.create;
import static akka.http.javadsl.server.Directives.handleWebSocketMessages;
import static akka.http.javadsl.server.Directives.parameter;
import static akka.http.javadsl.server.Directives.path;
import static akka.http.javadsl.server.Directives.route;
import static akka.stream.javadsl.Flow.fromSinkAndSource;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.Runtime.getRuntime;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wt.api.write.CommandForwarderRoute;
import com.wt.cloud.monitoring.ServiceStatusReporter;
import com.wt.config.utils.WDProperties;
import com.wt.domain.InvestingMode;
import com.wt.domain.Outgoing;
import com.wt.domain.write.commands.GetActiveTokens;
import com.wt.domain.write.commands.StartInvestorRealtimePerformance;
import com.wt.domain.write.commands.StartRealtimePerformance;
import com.wt.domain.write.commands.StartRealtimePriceReceiving;
import com.wt.domain.write.commands.StopInvestorRealtimePerformance;
import com.wt.domain.write.commands.StopRealtimePerformance;
import com.wt.domain.write.commands.StopRealtimePriceReceiving;
import com.wt.domain.write.commands.SubscribeToToken;
import com.wt.domain.write.commands.UpdateInstrumentsMap;
import com.wt.utils.RealTimeServiceAppConfiguration;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.ConnectionContext;
import akka.http.javadsl.Http;
import akka.http.javadsl.HttpsConnectionContext;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.server.Directives;
import akka.http.javadsl.server.Route;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class RealTimeService {
  
  private static AnnotationConfigApplicationContext ctx;

  private ActorSystem realTimeSystem;
  private ActorRef realtimeFundPerformanceActor;
  private ActorRef realtimeInvestorPerformanceActor;
  private ActorRef marketPriceRealtimeActor;
  private WDProperties properties;
  private WDActorSelections wdActorSelections;
  private CommandForwarderRoute commandForwarderRoute;
  private ServiceStatusReporter serviceStatusReporter;
  
  @Autowired
  public RealTimeService(ActorSystem realTimeSystem, WDProperties properties, WDActorSelections wdActorSelections,
      CommandForwarderRoute commandForwarderRoute, ServiceStatusReporter serviceStatusReporter) {
    super();
    this.realTimeSystem = realTimeSystem;
    this.properties = properties;
    this.wdActorSelections = wdActorSelections;
    this.commandForwarderRoute = commandForwarderRoute;
    this.serviceStatusReporter =serviceStatusReporter;
  }
  
  public static void main(String[] args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(RealTimeServiceAppConfiguration.class);
    RealTimeService service = ctx.getBean(RealTimeService.class);
    service.start();
  }
  
  
  private void start() throws Exception {
    log.info("=======================================");
    log.info("| Starting RealTimeService           |");
    log.info("=======================================");
     createActors();
     Materializer materializer = ActorMaterializer.create(realTimeSystem);
     getRuntime().addShutdownHook(new Thread(() -> {
       ctx.close();
     })); 
     
     final Http http = Http.get(realTimeSystem);

     log.info("Starting on " + properties.realtimeStreamUrl() + ":" + properties.realtimeStreamPort());
     final ConnectHttp host = ConnectHttp.toHost(properties.realtimeStreamUrl(), properties.realtimeStreamPort());

     http.bindAndHandle(appRoute().flow(realTimeSystem, materializer), host, materializer);
     log.info("Started on " + properties.realtimeStreamUrl() + ":" + properties.realtimeStreamPort());

     if (properties.useSSL()) {

       HttpsConnectionContext https = useHttps(realTimeSystem);
       ConnectHttp connect = ConnectHttp.toHostHttps(properties.realtimeStreamUrlSSL(), properties.realtimeStreamPortSSL())
           .withCustomHttpsContext(https);

       http.bindAndHandle(appRoute().flow(realTimeSystem, materializer), connect, materializer);
       log.info("Started on " + properties.realtimeStreamUrlSSL() + ":" + properties.realtimeStreamPortSSL());
     }
  }

  public Route appRoute() {
    return route(
        adviserRealtimePerformance(), 
        investorRealtimePerformance(),
        getBroadcastStreamForStock(),
        subscribeToTokenForStock());
  }
  
  private Route subscribeToTokenForStock() {
    Route parameter =  parameter("wdId", wdId -> handle(new SubscribeToToken(wdId)));
    return path("subscribeToToken", () -> Directives.get(() -> parameter));
  }

  
  private Route getBroadcastStreamForStock() {
    Route parameter = parameter("connectionId", connectionId -> parameter("wdId",
        wdId -> handleWebSocketMessages(marketPrice(connectionId, wdId))));

    return path("marketprice", () -> Directives.get(() -> parameter));
  }
  
  private Flow<Message, Message, NotUsed> marketPrice(String connectionId, String wdId) {
    Source<Message, NotUsed> source = Source.<Outgoing> actorRef(1, OverflowStrategy.dropHead())
        .map((outgoing) -> (Message) create(outgoing.message)).<NotUsed> mapMaterializedValue(destinationRef -> {
          marketPriceRealtimeActor.tell(
              new StartRealtimePriceReceiving(wdId, connectionId, destinationRef), ActorRef.noSender());
          return NotUsed.getInstance();
        });

    Sink<Message, NotUsed> sink = Flow.<Message> create()
        .map((msg) -> new Incoming(msg.asTextMessage().getStrictText())).to(Sink.actorRef(marketPriceRealtimeActor,
            new StopRealtimePriceReceiving(wdId, connectionId)));

    return fromSinkAndSource(sink, source);
  }

  public Route adviserRealtimePerformance() {
    Route parameter = parameter("connectionId", connectionId -> parameter("adviser",
        adviser -> parameter("fund", fund -> handleWebSocketMessages(fundPerformance(connectionId, adviser, fund)))));

    return path("performance", () -> Directives.get(() -> parameter));
  }

  private Route investorRealtimePerformance() {
    Route parameter = parameter("connectionId",
        connectionId -> parameter("email", email -> parameter("investingMode", investingMode -> handleWebSocketMessages(investorPerformance(connectionId, email, InvestingMode.valueOf(investingMode))))));

    return path("investorPerformance", () -> Directives.get(() -> parameter));
  }
  
  private Flow<Message, Message, NotUsed> fundPerformance(String connectionId, String adviserUsername, String fund) {
    Source<Message, NotUsed> source = Source.<Outgoing> actorRef(1, OverflowStrategy.dropHead())
        .map((outgoing) -> (Message) create(outgoing.message)).<NotUsed> mapMaterializedValue(destinationRef -> {
          realtimeFundPerformanceActor.tell(
              new StartRealtimePerformance(adviserUsername, fund, connectionId, destinationRef), ActorRef.noSender());
          return NotUsed.getInstance();
        });

    Sink<Message, NotUsed> sink = Flow.<Message> create()
        .map((msg) -> new Incoming(msg.asTextMessage().getStrictText())).to(Sink.actorRef(realtimeFundPerformanceActor,
            new StopRealtimePerformance(adviserUsername, fund, connectionId)));

    return fromSinkAndSource(sink, source);
  }

  private Flow<Message, Message, NotUsed> investorPerformance(String connectionId, String investor, InvestingMode investingMode) {
    Source<Message, NotUsed> source = Source.<Outgoing> actorRef(1, OverflowStrategy.dropHead())
        .map((outgoing) -> (Message) create(outgoing.message)).<NotUsed> mapMaterializedValue(destinationRef -> {
          realtimeInvestorPerformanceActor
              .tell(new StartInvestorRealtimePerformance(investor, investingMode, connectionId, destinationRef), ActorRef.noSender());
          return NotUsed.getInstance();
        });

    Sink<Message, NotUsed> sink = Flow.<Message> create()
        .map((msg) -> new Incoming(msg.asTextMessage().getStrictText())).to(Sink
            .actorRef(realtimeInvestorPerformanceActor, new StopInvestorRealtimePerformance(investor, connectionId, investingMode)));

    return fromSinkAndSource(sink, source);
  }
  
  private Route handle(Object command) {
    return commandForwarderRoute.handleUniverseCommand(command, wdActorSelections.universeRootActor());
  }
  
  static class Incoming {
    public final String message;

    public Incoming(String message) {
      this.message = message;
    }
  }
  
  public void createActors() {
    realtimeFundPerformanceActor = realTimeSystem.actorOf(SpringExtProvider.get(realTimeSystem).props("RealtimeFundPerformanceRoot"),
        "realtime-fund-performance-root");
    realtimeInvestorPerformanceActor = realTimeSystem.actorOf(
        SpringExtProvider.get(realTimeSystem).props("RealtimeInvestorPerformanceRoot"), "realtime-investor-performance-root");
    marketPriceRealtimeActor = realTimeSystem.actorOf(SpringExtProvider.get(realTimeSystem).props("MarketPriceRealtimeRoot"),
        "realtime-market-price-root");
    wdActorSelections.activeInstrumentTracker().tell(new GetActiveTokens(), ActorRef.noSender());
    realtimeFundPerformanceActor.tell(new UpdateInstrumentsMap(), ActorRef.noSender());
    
  }
  
  public HttpsConnectionContext useHttps(ActorSystem system) {
    HttpsConnectionContext https = null;
    try {
      final char[] password = properties.keystorePassword().toCharArray();

      final KeyStore keyStore = KeyStore.getInstance("PKCS12");
      final InputStream keyStoreStream = RealTimeService.class.getClassLoader()
          .getResourceAsStream(properties.keystoreFileName());
      if (keyStoreStream == null) {
        throw new RuntimeException("Keystore required!");
      }
      keyStore.load(keyStoreStream, password);

      final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
      keyManagerFactory.init(keyStore, password);

      final TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
      tmf.init(keyStore);

      final SSLContext sslContext = SSLContext.getInstance("TLS");
      sslContext.init(keyManagerFactory.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

      https = ConnectionContext.https(sslContext);
    } catch (NoSuchAlgorithmException | KeyManagementException e) {
      log.debug(" while configuring HTTPS." + e.getCause(), e);
    } catch (CertificateException | KeyStoreException | UnrecoverableKeyException | IOException e) {
      log.debug(e.getCause() + " while ", e);
    } catch (Exception e) {
      log.debug(e.getCause() + " Exception", e);
    }

    return https;
  }
  
  @Scheduled(cron = "${service.heartbeat.schedule}", zone = "Asia/Kolkata")
  public void sendServiceStatusHeartBeats() {
    if (properties.shallMonitorService())
      serviceStatusReporter.sendStatusUpdate(this.getClass().getSimpleName());
  }

}
