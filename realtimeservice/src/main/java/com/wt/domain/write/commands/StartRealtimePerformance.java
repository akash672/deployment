package com.wt.domain.write.commands;

import akka.actor.ActorRef;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class StartRealtimePerformance {

  @Getter
  private String adviserUsername;
  @Getter
  private String fundName;
  @Getter
  private String connectionId;
  @Getter
  private ActorRef destinationRef;

}
