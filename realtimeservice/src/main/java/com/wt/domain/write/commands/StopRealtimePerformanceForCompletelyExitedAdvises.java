package com.wt.domain.write.commands;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class StopRealtimePerformanceForCompletelyExitedAdvises {
  @Getter
  private List<String> activeAdvises = new ArrayList<String>(); 
}
