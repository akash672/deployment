package com.wt.domain;

import static akka.actor.ActorRef.noSender;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.broadcast.xneutrino.CancellablePriceStream;
import com.wt.config.utils.WDProperties;
import com.wt.domain.write.PerformanceCalculator;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.SendStreamUpdate;
import com.wt.domain.write.commands.StartRealtimePriceReceiving;
import com.wt.domain.write.commands.StopRealtimePriceReceiving;
import com.wt.utils.akka.CancellableStream;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.PoisonPill;
import akka.actor.UntypedAbstractActor;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.KillSwitches;
import akka.stream.UniqueKillSwitch;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.MergeHub;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import lombok.extern.log4j.Log4j;
import scala.concurrent.duration.FiniteDuration;

@Service("RealtimeEquityPrice")
@Scope("prototype")
@Log4j
public class MarketPriceRealtime extends UntypedAbstractActor implements PerformanceCalculator<PriceWithPaf> {
  private ActorMaterializer materializer;
  private WDProperties wdProperties;
  private WDActorSelections wdActorSelections;
  private ActorRef subscriberActor;
  private final Map<String, ActorRef> websocketActorAddressMappedWithConnectionId = new ConcurrentHashMap<String, ActorRef>();
  private String id;
  private Cancellable cancellablePriceStreamRequest;
  private PerformanceStream<PriceWithPaf> runningMarketPriceStream;
  private boolean isMarketPriceStreamRunning;
  private double currentStockPrice = 0;
  private final Map<String, String> lastMessagesToIndividualWebsockets = new ConcurrentHashMap<String, String>();

  public MarketPriceRealtime(String id) {
    super();
    this.id = id;
    this.isMarketPriceStreamRunning = false;
  }

  @Override
  public void onReceive(Object msg) throws Exception {
    if (msg instanceof StartRealtimePriceReceiving) {
      startRealtimeEquityPrice((StartRealtimePriceReceiving) msg);
    } else if (msg instanceof StopRealtimePriceReceiving) {
      stopRealtimeEquityPrice((StopRealtimePriceReceiving) msg);
    } else if (msg instanceof PriceWithPaf) {
      PriceWithPaf priceWithPaf = (PriceWithPaf) msg;
      if (subscriberActor != null && priceWithPaf != null) {
        subscriberActor.tell(priceWithPaf, self());
        cancellablePriceStreamRequest.cancel();
        currentStockPrice = priceWithPaf.getPrice().getPrice();
      }
    } else if (msg instanceof SendStreamUpdate) {
      sendCurrentPrice();
    } 
  }

  private void sendCurrentPrice() {
    try {
      sendPrice();
    } catch (Exception e) {
      log.error(e);
    }
  }

  private void sendPrice() {
    String json = "{\"ticker\":\"" + id.replaceFirst("RealtimePrice-", "") 
                  + "\",\"price\":" + currentStockPrice + "}";

    Outgoing msg = new Outgoing(json);
    websocketActorAddressMappedWithConnectionId.entrySet().stream().forEach((entry) -> {
      sendIfCurrentMessageIsDifferentFromLastMessage(json, msg, entry);
    });
  }

  private void sendIfCurrentMessageIsDifferentFromLastMessage(String json, Outgoing msg,
      Entry<String, ActorRef> entry) {
    if (!json.equals(lastMessagesToIndividualWebsockets.get(entry.getKey()))) {
      entry.getValue().tell(msg, noSender());
    }
    lastMessagesToIndividualWebsockets.put(entry.getKey(), json);
  }

  public void startRealtimeEquityPrice(StartRealtimePriceReceiving realtimePrice) throws Exception {
    requestUniverseRootToSendPrices(realtimePrice.getWdId());
    
    if (!isMarketPriceStreamRunning) {
      runningMarketPriceStream = realtime(materializer, self());
      keepCurrentStockPriceUpdatedWithLivePrice(realtimePrice);
    }
    
    websocketActorAddressMappedWithConnectionId.put(realtimePrice.getConnectionId(), realtimePrice.getDestinationRef());
  }
  
  private void requestUniverseRootToSendPrices(String wdId) {
    cancellablePriceStreamRequest = context().system()
    .scheduler()
    .schedule(FiniteDuration.Zero(), FiniteDuration.create(wdProperties.STREAM_PRICE_TICK_INTERVAL(), SECONDS), 
        () -> {
          wdActorSelections.universeRootActor().tell(new GetLivePriceStream(wdId), self());
    }, context().system().dispatcher());
  }
  
  private void keepCurrentStockPriceUpdatedWithLivePrice(StartRealtimePriceReceiving realtimePrice) throws Exception {
    include(PriceWithPaf.noPrice(realtimePrice.getWdId()));
  }

  protected void include(PriceWithPaf price) throws Exception {
    runningMarketPriceStream.include(marketPriceStream(price.getPrice().getWdId()), materializer);
  }

  public void stopRealtimeEquityPrice(StopRealtimePriceReceiving c) {
    ActorRef priceStreamReceiverActor = websocketActorAddressMappedWithConnectionId.get(c.getConnectionId());
    if (priceStreamReceiverActor == null)
      return;
    priceStreamReceiverActor.tell(PoisonPill.getInstance(), noSender());
    lastMessagesToIndividualWebsockets.remove(c.getConnectionId());
    websocketActorAddressMappedWithConnectionId.remove(c.getConnectionId());
    isMarketPriceStreamRunning = false;
    runningMarketPriceStream.cancel();
    context().stop(self());
  }

  private CancellableStream<PriceWithPaf, NotUsed> marketPriceStream(String wdId) throws Exception {
    Instrument instrument = wdId.endsWith("-MF") ? new MutualFundInstrument(wdId) : new EquityInstrument(wdId);
    CancellablePriceStream<PriceWithPaf, NotUsed> cancellablePriceStream = cancellablePriceStream(instrument,
        context().system(), wdProperties.STREAM_PRICE_TICK_FREQUENCY(), wdProperties.STREAM_PRICE_TICK_INTERVAL(),
        wdProperties.STREAM_PRICE_TICK_MAXIMUM_BURST());
    subscriberActor = cancellablePriceStream.getActor();
    return cancellablePriceStream;
  }

  @Autowired
  public void setWdActorSelections(WDActorSelections wdActorSelections) {
    this.wdActorSelections = wdActorSelections;
  }
  
  @Autowired
  public void setMaterializer(ActorMaterializer materializer) {
    this.materializer = materializer;
  }

  @Autowired
  public void setWdProperties(WDProperties wdProperties) {
    this.wdProperties = wdProperties;
  }

  @Override
  public PerformanceStream<PriceWithPaf> realtime(ActorMaterializer materializer, ActorRef actor) {
    Sink<PriceWithPaf, NotUsed> consumer = Sink.actorRef(actor, PoisonPill.getInstance());

    RunnableGraph<Pair<UniqueKillSwitch,NotUsed>> runnableGraph = MergeHub
        .of(PriceWithPaf.class)
        .viaMat(KillSwitches.single(), Keep.right())
        .toMat(consumer, Keep.both());

    Pair<UniqueKillSwitch,NotUsed> toConsumer = runnableGraph.run(materializer);

    return new PerformanceStream<PriceWithPaf>(consumer, toConsumer.first());
  }
  
  
}