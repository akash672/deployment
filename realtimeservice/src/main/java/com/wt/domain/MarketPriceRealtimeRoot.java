package com.wt.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wt.domain.write.ParentActor;
import com.wt.domain.write.commands.BODCompleted;
import com.wt.domain.write.commands.SendStreamUpdate;
import com.wt.domain.write.commands.StartRealtimePriceReceiving;
import com.wt.domain.write.commands.StopRealtimePriceReceiving;

import akka.actor.UntypedAbstractActor;
import scala.collection.JavaConverters;

@Scope("prototype")
@Service("MarketPriceRealtimeRoot")
public class MarketPriceRealtimeRoot extends UntypedAbstractActor implements ParentActor<MarketPriceRealtime> {

  SendStreamUpdate sendPerformance = new SendStreamUpdate();

  @Override
  public void onReceive(Object msg) {
    if (msg instanceof StopRealtimePriceReceiving) {
      StopRealtimePriceReceiving stopRequest = (StopRealtimePriceReceiving) msg;
      tellChildren(context(), sender(), msg,
          "RealtimePrice-" + stopRequest.getWdId());
    } else if(msg instanceof StartRealtimePriceReceiving){
      StartRealtimePriceReceiving startRequest = (StartRealtimePriceReceiving) msg;
      String marketPriceRealtimePersistenceId = "RealtimePrice-" + startRequest.getWdId();
      tellChildren(context(), sender(), msg, marketPriceRealtimePersistenceId);
    } else if (msg instanceof BODCompleted) {
    }
  }

  @Scheduled(cron = "${realtime.tick.schedule}", zone = "Asia/Kolkata")
  public void sendFundPerformanceToDestinationActors() {
    JavaConverters.asJavaCollectionConverter(context().children()).asJavaCollection().stream()
        .forEach(actor -> actor.tell(sendPerformance, self()));
  }
  
  @Override
  public String getName() {
    return "MarketPriceRealtimeRoot";
  }

  @Override
  public String getChildBeanName() {
    return "RealtimeEquityPrice";
  }

}
