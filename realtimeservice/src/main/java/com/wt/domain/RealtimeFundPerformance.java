package com.wt.domain;

import static akka.actor.ActorRef.noSender;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wt.domain.repo.FundAdviseRepository;
import com.wt.domain.repo.FundPerformanceSnapshotRepository;
import com.wt.domain.write.StartCalculatingAdvisePerformance;
import com.wt.domain.write.commands.BODCompleted;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.SendStreamUpdate;
import com.wt.domain.write.commands.StartRealtimePerformance;
import com.wt.domain.write.commands.StopRealtimePerformance;
import com.wt.domain.write.commands.StopRealtimePerformanceForCompletelyExitedAdvises;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.events.AdvisePerformanceCalculated;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundEvent;
import com.wt.domain.write.events.FundPerformanceCalculated;
import com.wt.domain.write.events.FundPerformanceCalculated.FundPerformanceCalculatedBuilder;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.UntypedAbstractActor;
import akka.persistence.query.EventEnvelope;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Service("RealtimeFundPerformance")
@Scope("prototype")
@Log4j
public class RealtimeFundPerformance extends UntypedAbstractActor {
  private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
      .excludeFieldsWithModifiers(Modifier.STATIC).create();

  private ActorRef advisePerformanceRootActor;
  private ActorMaterializer materializer;
  private JournalProvider journalProvider;
  private long lastEncounteredSequenceNumber;
  private double fundRealizedPerforanceFromDb;

  private final Map<String, ActorRef> performanceStreamReceiverActors = new ConcurrentHashMap<String, ActorRef>();
  private String id;
  private boolean isRunningFundPerformanceStream;
  private Map<String, AdvisePerformanceCalculated> allPerf = new ConcurrentHashMap<String, AdvisePerformanceCalculated>();
  private final Map<String, String> lastMessagesToIndividualReceivers = new ConcurrentHashMap<String, String>();
  private Map<String, Advise> adviseIdToAdvise = new HashMap<String, Advise>();
  private Map<String,String> wdIdToSymbol = new HashMap<String,String>();
  private FundAdviseRepository adviseRepository;
  private FundPerformanceSnapshotRepository fundPerformanceSnapshotRepository;
  
  @Autowired
  public void setFundPerformanceSnapshotRepo( FundPerformanceSnapshotRepository fundPerformanceSnapshotRepository) {
    this.fundPerformanceSnapshotRepository = fundPerformanceSnapshotRepository;
  }
  
  @Autowired
  public void setAdviseRepo( FundAdviseRepository adviseRepository) {
    this.adviseRepository = adviseRepository;
  }

  public RealtimeFundPerformance(String id) {
    super();
    this.id = id;
    this.isRunningFundPerformanceStream = false;
    this.advisePerformanceRootActor = context().actorOf(SpringExtProvider.get(context().system()).props("AdvisePerformanceCalculatorRootActor", "AdvisePerformanceCalculatorRootActor"),
        "advise-performance-root-actor-" + id);
    this.lastEncounteredSequenceNumber = -1L;
  }

  @Override
  public void onReceive(Object msg) throws InterruptedException, ExecutionException {
    if (msg instanceof StartRealtimePerformance) {
      startRealtimePerformance((StartRealtimePerformance) msg);
    } else if (msg instanceof StopRealtimePerformance) {
      stopRealtimePerformance((StopRealtimePerformance) msg);
    } else if (msg instanceof AdvisePerformanceCalculated) {
      AdvisePerformanceCalculated advisePerformanceCalculated = (AdvisePerformanceCalculated) msg;
      allPerf.put(advisePerformanceCalculated.getAdviseId(), advisePerformanceCalculated);
    } else if (msg instanceof SendStreamUpdate) {
      sendCurrentPerformance();
    } else if (msg instanceof BODCompleted) {
      stopAllProcessAndSelfKill();      
    }
  }

  private void stopAllProcessAndSelfKill() {
    log.info("Stopping Realtime Fund Performance for " + id );
    isRunningFundPerformanceStream = false;
    context().stop(getSelf());
    lastEncounteredSequenceNumber = -1L;
  }

  private void sendCurrentPerformance() {
    try {
      sendPerformance();
    } catch (Exception e) {
      log.error("Error in sending perforamce " + e);
    }
  }

  private void sendPerformance() {
    FundPerformanceCalculated fundPerformanceCalculated = new FundPerformanceCalculatedBuilder("", "",
        DateUtils.currentTimeInMillis()).withAdvisePerformance(allPerf.values()).build();
    fundPerformanceCalculated.addToPerformance(fundRealizedPerforanceFromDb);
    fundPerformanceCalculated.getAllAdvisePerformances().stream().forEach(p -> {
      String symbol = p.getTicker().equals("Cash") ? p.getTicker(): getSymbolFor(p.getTicker());
      p.setSymbol(symbol);
    });
    String json = gson.toJson(fundPerformanceCalculated);
    
    Outgoing msg = new Outgoing(json);
    performanceStreamReceiverActors.entrySet()
                                   .stream()
                                   .forEach((entry) -> 
    {
      sendIfCurrentMessageIsDifferentFromLastMessage(json, msg, entry);
    });
  }

  private void sendIfCurrentMessageIsDifferentFromLastMessage(String json, Outgoing msg, Entry<String, ActorRef> entry) {
    if(!json.equals(lastMessagesToIndividualReceivers.get(entry.getKey()))) {
      entry.getValue().tell(msg, noSender());
    }
    lastMessagesToIndividualReceivers.put(entry.getKey(), json);
  }

  public void startRealtimePerformance(StartRealtimePerformance realtimePerformance)
      throws InterruptedException, ExecutionException {

    if (!isRunningFundPerformanceStream) {
      keepFundsStateUpdatedWithLiveEvents(realtimePerformance);
    }

    performanceStreamReceiverActors.put(realtimePerformance.getConnectionId(), realtimePerformance.getDestinationRef());
  }

  private void keepFundsStateUpdatedWithLiveEvents(StartRealtimePerformance realtimePerformance) {
    String fundNameWithAdviserUsername = realtimePerformance.getFundName() +"__" + realtimePerformance.getAdviserUsername();

    Optional<FundPerformanceSnapshot> latestSnapshot = fundPerformanceSnapshotRepository.findById(fundNameWithAdviserUsername);
    FundPerformanceSnapshot fundSnapshot ;
    if(latestSnapshot.isPresent()){
      fundSnapshot = latestSnapshot.get();
    }else{
      fundSnapshot = new FundPerformanceSnapshot(fundNameWithAdviserUsername);
    }
    fundRealizedPerforanceFromDb = fundSnapshot.getFundRealizedPerformance();
    List<String> lastEodActiveIds = fundSnapshot.getActiveAdviseIdsAsOfLastCalculatedEod();
    List<Advise> activeAdvisesAsOfLastEod = new ArrayList<>();

    if (lastEodActiveIds.size() > 0)
      lastEodActiveIds.stream().forEach(adviseId -> {
        Optional<Advise> activeAdvise = adviseRepository.findById(adviseId);
        if (activeAdvise.isPresent())
          activeAdvisesAsOfLastEod.add(activeAdvise.get());
      });

    activeAdvisesAsOfLastEod.stream().forEach(activeAdvise -> {
      try {
        if (activeAdvise.getRemainingAllocation() > 0) {
          adviseIdToAdvise.put(activeAdvise.getId(), activeAdvise);
          startCalculatingPerformanceOf(activeAdvise);
        }
      } catch (Exception e) {
        log.error("Exception encountered while calculating real-time view " + e);
      }
    });
    
    lastEncounteredSequenceNumber = fundSnapshot.getLastReadFundSequenceNumber();
    Source<EventEnvelope, NotUsed> fundEvents = journalProvider
        .allEventsSourceForPersistenceId(realtimePerformance.getFundName(), lastEncounteredSequenceNumber, Long.MAX_VALUE);
    
    
    fundEvents
    .filter(event -> event.event() instanceof FundEvent)
    .runForeach(event -> {
      if (event.event() instanceof FundAdviceIssued) {
        include(event);
      } else if (event.event() instanceof FundAdviceClosed) {
          FundAdviceClosed adviseClosed = (FundAdviceClosed) event.event();
          adviseIdToAdvise.get(adviseClosed.getAdviseId()).update(adviseClosed);
          if(Double.compare(adviseClosed.getAbsoluteAllocation(), 100.0) == 0)
            adviseIdToAdvise.remove(adviseClosed.getAdviseId());
        }
      lastEncounteredSequenceNumber = event.sequenceNr();
    }, materializer);
 }
  
  protected void include(EventEnvelope event) throws Exception {
    FundAdviceIssued adviseIssued = (FundAdviceIssued) event.event();
    String adviseId = adviseIssued.getAdviseId();
    Advise newAdvise = getAdvise(adviseIssued);
    adviseIdToAdvise.put(adviseId, newAdvise);
    startCalculatingPerformanceOf(newAdvise);
  }

  private Advise getAdvise(FundAdviceIssued adviseIssued) {
	if(!adviseIdToAdvise.containsKey(adviseIssued.getAdviseId())){
		Advise newAdvise = new Advise(adviseIssued.getAdviseId(), adviseIssued.getAdviserUsername(), adviseIssued.getFundName(),
				adviseIssued.getToken(), adviseIssued.getAllocation(), adviseIssued.getEntryTime(), adviseIssued.getEntryTime(),
				adviseIssued.getEntryTime(), adviseIssued.getEntryPrice(), adviseIssued.getEntryPrice(), adviseIssued.getExitTime(),
				adviseIssued.getExitTime(), adviseIssued.getExitPrice(), adviseIssued.getExitPrice());
		newAdvise.update(adviseIssued);
		return newAdvise;
		
	} else{
		Advise originalAdvise = adviseIdToAdvise.get(adviseIssued.getAdviseId());
		originalAdvise.updateExistingAdvise(adviseIssued);
		return originalAdvise;
	}
}

public void stopRealtimePerformance(StopRealtimePerformance c) {
    advisePerformanceRootActor.tell(new StopRealtimePerformanceForCompletelyExitedAdvises(adviseIdToAdvise.keySet().stream()
        .collect(Collectors.toList())), self());
    ActorRef performanceStreamReceiverActor = performanceStreamReceiverActors.get(c.getConnectionId());
    if (performanceStreamReceiverActor == null)
      return;
    performanceStreamReceiverActor.tell(PoisonPill.getInstance(), noSender());
    lastMessagesToIndividualReceivers.remove(c.getConnectionId());
    performanceStreamReceiverActors.remove(c.getConnectionId());
    
  }
  
  private String getSymbolFor(String ticker) {
    if(wdIdToSymbol.containsKey(ticker)){
      return wdIdToSymbol.get(ticker);
    }
    try {
      Object result = askAndWaitForResult(
          context().parent(), new GetSymbolForWdId(ticker));
      if (result instanceof Failed) {
        log.error("Error while getting symbol for wdId " + ticker);
        return ticker;
      }
      SymbolSentForWdId symbolReceived = (SymbolSentForWdId) result;
      wdIdToSymbol.put(ticker, symbolReceived.getSymbol());
      return symbolReceived.getSymbol();
    } catch (Exception e) {
      log.error("UserActor: failed to get symbol from UserRootActor for ticker " + ticker);
      return ticker;
    }
  }

  private void startCalculatingPerformanceOf(Advise activeAdvise)
      throws Exception {
    advisePerformanceRootActor.tell(new StartCalculatingAdvisePerformance(activeAdvise, self()), self());
    isRunningFundPerformanceStream = true;
  }

  @Autowired
  public void setMaterializer(ActorMaterializer materializer) {
    this.materializer = materializer;
  }

  @Autowired
  public void setJournalProvider(JournalProvider journalProvider) {
    this.journalProvider = journalProvider;
  }

}