package com.wt.domain;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wt.domain.write.ParentActor;
import com.wt.domain.write.commands.GetActiveTokens;
import com.wt.domain.write.commands.SendStreamUpdate;
import com.wt.domain.write.commands.StartInvestorRealtimePerformance;
import com.wt.domain.write.commands.StopInvestorRealtimePerformance;
import com.wt.domain.write.commands.SubscribeToFollowingTickers;
import com.wt.domain.write.commands.SubscribeToToken;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.UntypedAbstractActor;
import scala.collection.JavaConverters;

@Scope("prototype")
@Service("RealtimeInvestorPerformanceRoot")
public class RealtimeInvestorPerformanceRoot extends UntypedAbstractActor implements ParentActor<RealtimeInvestorPerformance> {

  private static final SendStreamUpdate sendFundPerformance = new SendStreamUpdate();
  private Set<String> tokensInLiveMarket = new HashSet<String>();
  private WDActorSelections wdActorSelection;

  @Autowired
  public void setWdActorSelection(WDActorSelections wdActorSelection) {
    this.wdActorSelection = wdActorSelection;
  }

  @Override
  public void onReceive(Object msg) {
    if (msg instanceof StartInvestorRealtimePerformance) {
      if(tokensInLiveMarket.isEmpty())
        wdActorSelection.activeInstrumentTracker().tell(new GetActiveTokens(), self());

      StartInvestorRealtimePerformance command = (StartInvestorRealtimePerformance) msg;
      tellChildren(context(), sender(), msg, "RealtimeInvestorPerf-" + command.getInvestorUsername() + "-" + command.getInvestingMode().name() + "-" + command.getConnectionId());
    }
    if (msg instanceof StopInvestorRealtimePerformance) {
      StopInvestorRealtimePerformance stopRequest = (StopInvestorRealtimePerformance) msg;
      tellChildren(context(), sender(), msg,
          "RealtimeInvestorPerf-" + stopRequest.getInvestorUsername() + "-" + stopRequest.getInvestingMode().name() + "-" + stopRequest.getConnectionId());
    }
    else if (msg instanceof SubscribeToFollowingTickers) {
      sendCommandForSubscription(msg);
    }
  }
  
  private void sendCommandForSubscription(Object msg) {
    SubscribeToFollowingTickers subscribeToFollowingTickers = (SubscribeToFollowingTickers) msg;
    wdActorSelection.realtimeAdviserFundPerformanceRootActor().tell(subscribeToFollowingTickers, self());
    tokensInLiveMarket = subscribeToFollowingTickers.getTickers();
    log.info("Received Tokens from Application Server" + tokensInLiveMarket.toString());
    tokensInLiveMarket.stream().forEach(ticker -> {
      wdActorSelection.universeRootActor().tell(new SubscribeToToken(ticker), self());
    });

}

  @Scheduled(cron = "${realtime.tick.schedule}", zone = "Asia/Kolkata")
  public void sendFundPerformanceToDestinationActors() {
    JavaConverters.asJavaCollectionConverter(context().children()).asJavaCollection().stream()
        .forEach(actor -> actor.tell(sendFundPerformance, self()));
  }

  @Override
  public String getName() {
    return "RealtimeInvestorPerformanceRoot";
  }

  @Override
  public String getChildBeanName() {
    return "RealtimeInvestorPerformance";
  }

}