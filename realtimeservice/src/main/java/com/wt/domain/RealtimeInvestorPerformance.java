package com.wt.domain;

import static akka.actor.ActorRef.noSender;
import static com.google.common.collect.Multimaps.index;
import static com.wt.domain.write.events.IssueType.Equity;
import static com.wt.utils.CommonConstants.HYPHEN;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wt.aws.cognito.exception.InternalErrorException;
import com.wt.domain.write.StartCalculatingUserPerformance;
import com.wt.domain.write.commands.SendStreamUpdate;
import com.wt.domain.write.commands.StartInvestorRealtimePerformance;
import com.wt.domain.write.commands.StopCalculatingPerformance;
import com.wt.domain.write.commands.StopInvestorRealtimePerformance;
import com.wt.domain.write.commands.UserSubscribedToThisFund;
import com.wt.domain.write.events.AdviseOrderRejected;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.BrokerNameReceived;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.ExecutedAdviseDetailsUpdated;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.InsufficientCapital;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.UserAdviseAbsolutePerformanceCalculated;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserEvent;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundPerformanceCalculated;
import com.wt.domain.write.events.UserFundPerformanceCalculated.UserFundPerformanceCalculatedBuilder;
import com.wt.domain.write.events.UserPerformanceCalculated;
import com.wt.domain.write.events.UserPerformanceCalculated.UserPerformanceCalculatedBuilder;
import com.wt.domain.write.events.UserSubscribedToFund;
import com.wt.domain.write.events.UserSubscribedToFundResponse;
import com.wt.utils.JournalProvider;
import com.wt.utils.akka.WDActorSelections;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.UntypedAbstractActor;
import akka.persistence.query.EventEnvelope;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Service("RealtimeInvestorPerformance")
@Scope("prototype")
@Log4j
public class RealtimeInvestorPerformance extends UntypedAbstractActor {
  private final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
      .excludeFieldsWithModifiers(Modifier.STATIC).create();
  private ActorRef userAdvisePerformanceRootActor;
  private ActorMaterializer materializer;
  private JournalProvider journalProvider;
  private WDActorSelections wdActorSelection;

  private final Map<String, ActorRef> performanceStreamReceiverActors = new ConcurrentHashMap<String, ActorRef>();
  private User user = new User();
  private Map<UserFundCompositeKey, UserFund> userFunds = new ConcurrentHashMap<UserFundCompositeKey, UserFund>();
  private Map<UserAdviseCompositeKey, UserAdvise> userAdvises = new ConcurrentHashMap<UserAdviseCompositeKey, UserAdvise>();
  private Map<UserAdviseCompositeKey, Boolean> userAdvisesStreamCreated = new ConcurrentHashMap<UserAdviseCompositeKey, Boolean>();
  private String id;
  private boolean isUserPerformanceStreamRunning;
  private Map<String, UserAdviseAbsolutePerformanceCalculated> allPerformance = new ConcurrentHashMap<String, UserAdviseAbsolutePerformanceCalculated>();
  private String lastMessage = "";
  private Map<String, Long> fundNameToLastUnsubscriptionTimeStamp = new HashMap<String, Long>();

  private InvestingMode investingMode;

  public RealtimeInvestorPerformance(String id) {
    super();
    this.id = id;
    this.isUserPerformanceStreamRunning = false;
    this.investingMode = InvestingMode.valueOf(id.split(HYPHEN)[2]);
    this.userAdvisePerformanceRootActor = context().actorOf(SpringExtProvider.get(context().system()).props("UserAdvisePerformanceCalculatorRootActor", "UserAdvisePerformanceCalculatorRootActor"),
        "user-advise-performance-root-" + id);
  }

  @Override
  public void onReceive(Object msg) throws Exception {
    if (msg instanceof StartInvestorRealtimePerformance) {
      startRealtimeUserPerformance((StartInvestorRealtimePerformance) msg);
    } else if (msg instanceof StopInvestorRealtimePerformance) {
      stopRealtimePerformance((StopInvestorRealtimePerformance) msg);
    } else if (msg instanceof UserSubscribedToFundResponse) {
      createUserFund((UserSubscribedToFundResponse) msg);
    } else if (msg instanceof UserAdviseAbsolutePerformanceCalculated) {
      UserAdviseAbsolutePerformanceCalculated advisePerformanceCalculated = (UserAdviseAbsolutePerformanceCalculated) msg;
        allPerformance.put(advisePerformanceCalculated.getAdviseId(), advisePerformanceCalculated);
    } else if (msg instanceof SendStreamUpdate) {
      sendCurrentPerformance();
    }
  }

  private void sendCurrentPerformance() {
    try {
      sendPerformance();
    } catch (Exception e) {
      log.error("Error ", e);
    }
  }

  private void sendPerformance() {
    send(userPerformanceFrom(allFundPerformanceFrom(allAdvisePerformance())));
  }

  private void send(UserPerformanceCalculated userPerformance) {
    String json = gson.toJson(userPerformance);
    if (json.equals(lastMessage)) {
      return;
    }
    lastMessage = json;
    Outgoing msg = new Outgoing(json);
    performanceStreamReceiverActors.values().stream().forEach(actor -> {
      actor.tell(msg, noSender());
    });
  }

  private void startRealtimeUserPerformance(StartInvestorRealtimePerformance realtimePerformance)
      throws InterruptedException, ExecutionException {
    log.info("Starting realtime performance for  " + id);

    if (!isUserPerformanceStreamRunning) {
      keepUserStateUpdatedWithLiveEvents(realtimePerformance);
    }

    performanceStreamReceiverActors.put(realtimePerformance.getConnectionId(), realtimePerformance.getDestinationRef());
  }

  private void keepUserStateUpdatedWithLiveEvents(StartInvestorRealtimePerformance realtimePerformance) {
    Source<EventEnvelope, NotUsed> userEvents = journalProvider
        .allEventsSourceForPersistenceId(realtimePerformance.getInvestorUsername(), 0L, Long.MAX_VALUE);

    userEvents.filter(event -> event.event() instanceof UserEvent).runForeach(event -> {
      updateUser(((UserEvent) event.event()));
    }, materializer);
  }

  private void updateUser(UserEvent userEvent) throws IllegalArgumentException, InternalErrorException, ParseException {
    user.update(userEvent);
    
    if (userEvent instanceof UserSubscribedToFund) {
      startRealtimeFundPerfromance((UserSubscribedToFund) userEvent);
    }
  }

  protected void startRealtimeFundPerfromance(UserSubscribedToFund userSubscribeEvent) {
    UserFundCompositeKey key = new UserFundCompositeKey(userSubscribeEvent.getUsername(), userSubscribeEvent.getFundName(),
        userSubscribeEvent.getAdviserUsername(), userSubscribeEvent.getInvestingMode());

    if (userFunds.get(key) != null) {
      return;
    }

    UserSubscribedToThisFund userSubscribedToThisFund = new UserSubscribedToThisFund(userSubscribeEvent.getUsername(),
        userSubscribeEvent.getAdviserUsername(), userSubscribeEvent.getFundName(), userSubscribeEvent.getInvestingMode());
    wdActorSelection.userRootActor().tell(userSubscribedToThisFund, self());
  }

  private void createUserFund(UserSubscribedToFundResponse userSubscribedToFundResponse) {
    if (!userSubscribedToFundResponse.isSubscribed()) {
      return;
    }

    if (userSubscribedToFundResponse.getInvestingMode() != investingMode)
      return;

    UserFundCompositeKey key = new UserFundCompositeKey(userSubscribedToFundResponse.getUsername(), userSubscribedToFundResponse.getFundName(),
        userSubscribedToFundResponse.getAdviserUsername(), userSubscribedToFundResponse.getInvestingMode());
    userFunds.put(key, new UserFund());
    
    Source<EventEnvelope, NotUsed> allEvents = journalProvider.allEventsSourceForPersistenceId(key.persistenceId());

    allEvents.filter(event -> event.event() instanceof FundSuccessfullyUnsubscribed).runForeach(userFundEvent -> {
      updateFundLastUnsubscriptionTimestamp((UserFundEvent) userFundEvent.event());
    }, materializer);
    
    Source<EventEnvelope, NotUsed> oldEvents = journalProvider.allEventsSourceForPersistenceId(key.persistenceId());

    oldEvents.filter(event -> event.event() instanceof UserFundEvent).runForeach(userFundEvent -> {
      updateUserFund((UserFundEvent) userFundEvent.event());
    }, materializer);
  }

  private void updateFundLastUnsubscriptionTimestamp(UserFundEvent event) {
    FundSuccessfullyUnsubscribed fundSuccessfullyUnsubscribed = (FundSuccessfullyUnsubscribed) event;
    fundNameToLastUnsubscriptionTimeStamp.put(fundSuccessfullyUnsubscribed.getFundName(), fundSuccessfullyUnsubscribed.getTimestamp());
  }

  private void updateUserFund(UserFundEvent userFundEvent) {
    UserFundCompositeKey key = new UserFundCompositeKey(userFundEvent.getUsername(), userFundEvent.getFundName(),
        userFundEvent.getAdviserUsername(), userFundEvent.getInvestingMode());

    if (userFunds.containsKey(key)) {
      UserFund userFund = userFunds.get(key);
      userFund.update((UserFundEvent) userFundEvent);
      if (userFundEvent instanceof ExecutedAdviseDetailsUpdated) {
        ExecutedAdviseDetailsUpdated executedAdviseDetailsUpdated = (ExecutedAdviseDetailsUpdated) userFundEvent;
        UserAdviseCompositeKey adviseKey = new UserAdviseCompositeKey(executedAdviseDetailsUpdated.getAdviseId(),
            executedAdviseDetailsUpdated.getUsername(), executedAdviseDetailsUpdated.getFundName(),
            executedAdviseDetailsUpdated.getAdviserUsername(), executedAdviseDetailsUpdated.getInvestingMode());
        keepAdviseUpdated(adviseKey);
      } else if (userFundEvent instanceof FundAdviceTradeExecuted) {
        FundAdviceTradeExecuted fundAdviceTradeExecuted = (FundAdviceTradeExecuted) userFundEvent;
        UserAdviseCompositeKey adviseKey = new UserAdviseCompositeKey(fundAdviceTradeExecuted.getAdviseId(),
            fundAdviceTradeExecuted.getUsername(), fundAdviceTradeExecuted.getFundName(),
            fundAdviceTradeExecuted.getAdviserUsername(), fundAdviceTradeExecuted.getInvestingMode());
        keepAdviseUpdated(adviseKey);
      }
    }
  }

  private void keepAdviseUpdated(UserAdviseCompositeKey key) {
    if (userAdvises.get(key) != null) {
      return;
    }

    Source<EventEnvelope, NotUsed> oldEvents = journalProvider.allEventsSourceForPersistenceId(key.persistenceId());
    
    oldEvents
    .filter(event -> event.event() instanceof UserAdviseEvent)
    .filter(event -> !(event.event() instanceof BrokerNameReceived))
    .filter(event -> !(event.event() instanceof InsufficientCapital))
    .filter(event -> !(event.event() instanceof AdviseOrderRejected))
    .runForeach(event -> {
      IssueType issueType = (((UserAdviseEvent) event.event()).getIssueType() == null) ? Equity : ((UserAdviseEvent) event.event()).getIssueType();
      userAdvises.putIfAbsent(key, issueType.userAdvise());
      updateUserAdvise((UserAdviseEvent) event.event());
      }, materializer);
  }

  private void updateUserAdvise(UserAdviseEvent event) throws Exception {
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(event.getAdviseId(), event.getUsername(),
        event.getFundName(), event.getAdviserUsername(), event.getInvestingMode());
    UserAdvise userAdvise = userAdvises.get(userAdviseCompositeKey);
    if (event instanceof CurrentUserAdviseFlushedDueToExit) {
      CurrentUserAdviseFlushedDueToExit currentUserAdviseFlushedDueToExit = (CurrentUserAdviseFlushedDueToExit) event;
      if (fundNameToLastUnsubscriptionTimeStamp.containsKey(currentUserAdviseFlushedDueToExit.getFundName())
          && currentUserAdviseFlushedDueToExit.getTimestamp() < fundNameToLastUnsubscriptionTimeStamp
              .get(currentUserAdviseFlushedDueToExit.getFundName())) {
        userAdvise.update(event);
      }
    } else {
      userAdvise.update(event);
    }
    if (event instanceof AdviseOrderTradeExecuted) {
      include(userAdvise.getToken(), userAdvise, userAdviseCompositeKey);
    }
  }

  public void stopRealtimePerformance(StopInvestorRealtimePerformance c) {
    userAdvisePerformanceRootActor.tell(new StopCalculatingPerformance(), self());

    isUserPerformanceStreamRunning = false;
    
    ActorRef performanceStreamReceiverActor = performanceStreamReceiverActors.get(c.getConnectionId());
    if (performanceStreamReceiverActor == null)
      return;
    performanceStreamReceiverActor.tell(PoisonPill.getInstance(), noSender());
    performanceStreamReceiverActors.remove(c.getConnectionId());

    if (performanceStreamReceiverActors.isEmpty()) {
      log.info("No more receiver actors. Stopping " + id);
      context().stop(self());
    }
  }

  private void include(String token, UserAdvise newAdvise, UserAdviseCompositeKey userAdviseCompositeKey)
      throws Exception {
    if ((userAdvisesStreamCreated.get(userAdviseCompositeKey) != null)
        && (userAdvisesStreamCreated.get(userAdviseCompositeKey))) {
      return;
    }
    startCalculatingPerformanceOf(token, newAdvise);
    userAdvisesStreamCreated.put(userAdviseCompositeKey, true);
  }

  private void startCalculatingPerformanceOf(String token, UserAdvise advise) throws Exception {
    userAdvisePerformanceRootActor.tell(new StartCalculatingUserPerformance(advise, self()), self());
    isUserPerformanceStreamRunning = true;
  }

  @Autowired
  public void setMaterializer(ActorMaterializer materializer) {
    this.materializer = materializer;
  }

  @Autowired
  public void setJournalProvider(JournalProvider journalProvider) {
    this.journalProvider = journalProvider;
  }

  @Autowired
  public void setWDActorSelections(WDActorSelections wdActorSelections) {
    this.wdActorSelection = wdActorSelections;
  }

  private UserPerformanceCalculated userPerformanceFrom(List<UserFundPerformanceCalculated> allFundPerformance) {
    return new UserPerformanceCalculatedBuilder(user.getUsername(), investingMode)
        .withUserPerformance(allFundPerformance).build();
  }

  private List<UserFundPerformanceCalculated> allFundPerformanceFrom(
      Multimap<UserFundCompositeKey, UserAdviseAbsolutePerformanceCalculated> allAdvisePerformance) {
    List<UserFundPerformanceCalculated> allFundPerformance = new ArrayList<>();
    for (UserFundCompositeKey key : userFunds.keySet()) {
      UserFund userFund = userFunds.get(key);
      Collection<UserAdviseAbsolutePerformanceCalculated> userAdvisePerformanceList = (allAdvisePerformance
          .containsKey(key)) ? allAdvisePerformance.get(key) : new ArrayList<>();
      UserFundPerformanceCalculated fundPerformanceCalculated = new UserFundPerformanceCalculatedBuilder(
          key.getUsername(), key.getAdviser(), key.getFund(), key.getInvestingMode(),
          userFund.getTotalInvestment(), userFund.getCashComponent(), userFund.getWithdrawal(), 0L)
              .withAdvisePerformance(userAdvisePerformanceList).build();
      if (fundPerformanceCalculated.getCurrentValue() <= 0.)
        continue;
      allFundPerformance.add(fundPerformanceCalculated);
    }
    return allFundPerformance;
  }

  private Multimap<UserFundCompositeKey, UserAdviseAbsolutePerformanceCalculated> allAdvisePerformance() {
    return index(allPerformance.values(),
        perf -> new UserFundCompositeKey(perf.getUsername(), perf.getFundName(), perf.getAdviserUsername(), perf.getInvestingMode()));
  }
  
}