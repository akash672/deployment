package com.wt.utils;

import static com.typesafe.config.ConfigFactory.load;

import java.io.IOException;
import java.util.Properties;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.typesafe.config.Config;
import com.wt.config.utils.WDProperties;
import com.wt.utils.akka.SpringExtension;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@EnableAsync
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:environment.properties")
@Log4j
public class RealTimeServiceAppConfiguration {
  
  @Value("${amazon.aws.accesskey}")
  private String amazonAWSAccessKey;

  @Value("${amazon.aws.secretkey}")
  private String amazonAWSSecretKey;
  
  @Value("${amazon.dynamodb.endpoint}")
  private String amazonDynamoDBEndpoint;

  @Getter
  @Value("${amazon.aws.region}")
  private String awsRegion;
  
  private ApplicationContext applicationContext;
  @Autowired
  protected WDProperties wdProperties;
    
  @Autowired
  protected WDActorSelections wdActorSelections;

  @Autowired
  public RealTimeServiceAppConfiguration(ApplicationContext applicationContext) {
    super();
    this.applicationContext = applicationContext;
  }
  
  @Primary
  @Bean(name = "AWSCredentials")
  public AWSCredentials amazonAWSCredentials() {
    return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
  }
  
  @Primary
  @Bean(name="AWSCredentialsProvider")
  public AWSCredentialsProvider amazonAWSCredentialsProvider() {
    return new AWSStaticCredentialsProvider(amazonAWSCredentials());
  }
  
  
  public void setSystemProperties() throws IOException {
    System.setProperty("environment", getEnvironment());
  }

  @Bean(name = "amazonDynamoDB")
  public AmazonDynamoDB amazonDynamoDB() {
    ClientConfiguration config = new ClientConfiguration();
    config.setMaxConnections(20);
    AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().
        withEndpointConfiguration(new EndpointConfiguration(amazonDynamoDBEndpoint, awsRegion)).
        withCredentials(amazonAWSCredentialsProvider()).build();
    
    log.debug("Creating dynamodb connections");
    return amazonDynamoDB;
  }
  
  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
    return properties;
  }
  
  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() throws IOException {
    setSystemProperties();
    String config = "RealTime-application.conf";
    String actorName = "RealTime";
    if (System.getProperty("appConfig") != null) {
      config = System.getProperty("appConfig");
      actorName = System.getProperty("appConfig").split("-")[0];
    }
    Config conf = load(config);
    ActorSystem system = ActorSystem.create(actorName, conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }
  
  @Bean(name="JournalProvider")
  public JournalProvider journalProvider() throws IOException
  {
    JournalProviderProduction journalProviderProduction = new JournalProviderProduction(actorSystem());
    return journalProviderProduction;
  }

  
  @Bean(destroyMethod = "shutdown")
  public ActorMaterializer actorMaterializer() throws IOException {
    return ActorMaterializer.create(actorSystem());
  }

  private String getEnvironment() throws IOException {
    String value = System.getProperty("environment");
    if (value != null)
      return value;

    return properties().getProperty("environment");
  }
  
}
