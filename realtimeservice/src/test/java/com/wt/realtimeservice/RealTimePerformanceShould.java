package com.wt.realtimeservice;

import static akka.http.javadsl.marshallers.jackson.Jackson.unmarshaller;
import static akka.http.javadsl.model.HttpRequest.GET;
import static akka.http.javadsl.model.HttpRequest.PATCH;
import static akka.http.javadsl.model.HttpRequest.POST;
import static akka.http.javadsl.model.MediaTypes.APPLICATION_JSON;
import static akka.http.javadsl.model.StatusCodes.OK;
import static com.amazonaws.util.json.Jackson.toJsonString;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static com.wt.domain.UserAcceptanceMode.Auto;
import static com.wt.domain.UserAcceptanceMode.Manual;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.wt.api.write.WDService;
import com.wt.config.utils.WDProperties;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.BrokerName;
import com.wt.domain.InvestorApproval;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Wallet;
import com.wt.domain.WealthCode;
import com.wt.domain.write.RealTimeInstrumentPrice;
import com.wt.domain.write.RealTimeService;
import com.wt.domain.write.commands.AddMoreAllocationToAnExistingAdvice;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.ReceivedUserAcceptanceMode;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateMFInstruments;
import com.wt.domain.write.events.AdviserCredentialsWithType;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.MarginRequested;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.CoreDBTest;
import com.wt.utils.OTPUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.http.javadsl.model.Uri;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import akka.http.javadsl.testkit.WSProbe;
import akka.stream.Materializer;
import commons.Retry;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RealTimeTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class RealTimePerformanceShould extends JUnitRouteTest {
  private static final long serialVersionUID = 1L;
  private static final String TICKER = "IN-NSE-EQ";
  private static final String TICKER4 = "IN4-NSE-EQ";
  private static final String SYMBOL = "ACC";
  private static final int INT_TOKEN = 22;
  private static final String TICKER2 = "IN2-NSE-EQ";
  private static final String SYMBOL2 = "SBIN";
  private static final int INT_TOKEN2 = 3045;
  private static final String TICKER3 = "IN3-NSE-EQ";
  private static final String SYMBOL3 = "HDFCBANK";
  private static final String SYMBOL4 = "HDFC";
  private static final int INT_TOKEN3 = 1333;
  private static final int INT_TOKEN4 = 221;
  private static final double LAST_PRICE_OF_TICKER = 101.;
  private static final double LAST_PRICE_OF_TICKER2 = 249.;
  private static final double LAST_PRICE_OF_TICKER3 = 1919.;
  private static final String MF_WDID = "INF515L01841-MF";
  private static final String MF_TOKEN= "INF515L01841";
  private static final String MF_SCHEME_NAME = "INF515L01841";
  private static final String SCHEME_CODE = "120295";
  private static final String SEGMENT = "MF";
  private static final String MIN_INVESTMENT = "5000";
  private static final String SCHEME_TYPE = "ELSS";
  private static final String MIN_MULTIPLIER = "500";
  private static final String RTA = "CAMS";
  private static final String EXIT_LOAD = "1.0";
  private static final double LAST_NAV_OF_MF_WDID = 14.1168;
  private static final String FUND_NAME = "WeeklyPicks";
  private static final String FUND_DESCRIPTION = FUND_NAME + "Description";
  private static final String FUND_THEME = "Theme";
  private static final double FUND_MIN_SIP = 6500;
  private static final double FUND_MIN_LUMPSUM = 22500;
  private static final String FUND_RISK_LEVEL = "MEDIUM";
  private static final String ADVISER_USERNAME = "swapnil";
  private static final String ADVISER_IDENTITY = "swapnil";

  private static final String ADVISER_IDENTITY_ID = ADVISER_USERNAME;
  private static final String USER_NAME = "subscriber";
  private static final double DUMMY_NOTIFICATION_LUMPSUM = 8000;
  private static final double DUMMY_NOTIFICATION_LUMPSUM2 = 800000;
  private static final double DUMMY_NOTIFICATION_SIPAMOUNT = 3500;
  private static final String DUMMY_NOTIFICATION_SIP_DATE = "dummy-notification-sip-date";
  private static final String DUMMY_NOTIFICATION_WEALTHCODE = "dummy-notification-wealthcode";
  private static final String WEALTHCODE = "1235";
  private static final String INVESTOR_NAME = "firstname lastname";

  private static final RegisterAdviserFromUserPool ADVISER = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi",
      "swapnil@bpwealth.com", "32publicId", "Fort Capital", "");
  private static final long STARTDATE = 1388082600000L;
  private static final long ENDDATE = 3471186600000L;
  private final static BrokerName BROKER_COMPANY_NAME = BrokerName.BPWEALTH;
  private static final String USER_EMAIL = "test@gmail.com";
  private static final String USER_PHONENUMBER = "8080621831";
  private static final String PANCARD = "ARNPC9761A";
  private static final String CLIENT_CODE = "EMP919191";
  private static final String DUMMY_STOCK_TICKER = "INDUMMY-NSE-EQ";
  private static final double DUMMY_STOCK_PRICE_IN_MARKET = 304.5;
  private static final double DUMMY_MUTUAL_FUND_PRICE_IN_MARKET = 592;
  private static final String DUMMY_MUTUAL_FUND_TICKER = "2342a-MF";
  private static final double NEW_MARKET_ORDER_PRICE = 55.;
  private static final double ADVISE_NEW_ALLOCATION = 20.;
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;
  
  private static final String[] INVESTOR_EQUITY_PERF_MSGS = new String[] {
      "{\"currentValue\":799154.75,\"returnPct\":0.11,\"pnl\":867.75,\"realizedPnL\":975.0,\"unrealizedPnL\":-107.25,\"allFundPerformance\":[{\"returnPct\":0.11,\"realizedPnL\":975.0,\"unrealizedPnL\":-107.25,\"pnl\":867.75,\"currentValue\":799154.75,\"allAdvisePerformance\":[{\"wdId\":\"IN-NSE-EQ\",\"symbol\":\"IN-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-99.5,\"pnl\":-99.5,\"pctPnL\":-0.01,\"currentValue\":80396.0,\"allocation\":10.06,\"nbShares\":796.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"IN3-NSE-EQ\",\"symbol\":\"IN3-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-7.75,\"pnl\":-7.75,\"pctPnL\":0.0,\"currentValue\":118978.0,\"allocation\":14.89,\"nbShares\":62.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":599780.75,\"allocation\":75.05,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}",
      "{\"currentValue\":799154.75,\"returnPct\":0.11,\"pnl\":867.75,\"realizedPnL\":975.0,\"unrealizedPnL\":-107.25,\"allFundPerformance\":[{\"returnPct\":0.11,\"realizedPnL\":975.0,\"unrealizedPnL\":-107.25,\"pnl\":867.75,\"currentValue\":799154.75,\"allAdvisePerformance\":[{\"wdId\":\"IN3-NSE-EQ\",\"symbol\":\"IN3-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-7.75,\"pnl\":-7.75,\"pctPnL\":0.0,\"currentValue\":118978.0,\"allocation\":14.89,\"nbShares\":62.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"IN-NSE-EQ\",\"symbol\":\"IN-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-99.5,\"pnl\":-99.5,\"pctPnL\":-0.01,\"currentValue\":80396.0,\"allocation\":10.06,\"nbShares\":796.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":599780.75,\"allocation\":75.05,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}" };

  private @Autowired ActorSystem system;
  private @Autowired RealTimeService service;
  private @Autowired WDService wdService;
  private @Autowired @Qualifier("WDActors") WDActorSelections wdActorselections;
  private @Autowired CoreDBTest dbTest;
  private @Autowired OTPUtils otpGeneratorMock;
  private @Autowired @Qualifier("RealWallet") Wallet realWallet;

  private ActorRef universeRootActor;
  @SuppressWarnings("unused")
  private ActorRef equityOrderRootActor;
  @SuppressWarnings("unused")
  private ActorRef mfOrderRootActor;
  @SuppressWarnings("unused")
  private ActorRef adviserRootActor;

  @Override
  public FiniteDuration awaitDuration() {
    return FiniteDuration.apply(1, TimeUnit.MINUTES);
  }

  @Before
  public void setupDB() throws Exception {
    environmentVariables.set("WT_MARKET_OPEN", "true");
    dbTest.createSchema();
    wdService.createActors();
    service.createActors();
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    equityOrderRootActor = system.actorOf(SpringExtProvider.get(system).props("EquityOrderRootActor"), "equity-order-aggregator");
    mfOrderRootActor = system.actorOf(SpringExtProvider.get(system).props("MFOrderRootActor"), "mf-order-aggregator");
    doReturn(WDProperties.DUMMY_HASHCODE_FOR_USER_ACTIVATION).when(otpGeneratorMock).getUniqueHashCode();
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL,INT_TOKEN);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL2,INT_TOKEN2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL3,INT_TOKEN3);
    updateInstrumentsMapOfEquityActor(TICKER4, SYMBOL4,INT_TOKEN4);
    updateInstrumentsMapOfMutualFundActor(MF_TOKEN,SEGMENT, MF_SCHEME_NAME, SCHEME_CODE, MIN_INVESTMENT, MIN_MULTIPLIER,
                                        SCHEME_TYPE, EXIT_LOAD, RTA);
  }


  class Updater implements Runnable {

    @Override
    public void run() {
      wdActorselections.universeRootActor().tell(new RealTimeInstrumentPrice(new InstrumentPrice(101, currentTimeInMillis(), "22-NSE-EQ")), ActorRef.noSender());
      wdActorselections.universeRootActor().tell(new RealTimeInstrumentPrice(new InstrumentPrice(249, currentTimeInMillis(), "3045-NSE-EQ")), ActorRef.noSender());
      wdActorselections.universeRootActor().tell(new RealTimeInstrumentPrice(new InstrumentPrice(1919, currentTimeInMillis(), "1333-NSE-EQ")), ActorRef.noSender());
    }
    
  }
  
  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    try {
      deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  @Rule
  public Retry retry = new Retry(5);
  
  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
  
  @Test
  @Ignore
  public void return_realtime_virtual_investor_performance_for_equity_over_websocket_for_successive_issue_on_a_given_ticker() throws Exception {
    updateLastPriceWithUniverseActor(TICKER4, LAST_PRICE_OF_TICKER, currentTimeInMillis());
   
    registerAdviseAndFloatTheirFund();
    
    String adviseId = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER4, 10.0, 100, 10, 102, 102));
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForProcessToComplete(4);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(4);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, DUMMY_NOTIFICATION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    autoApprove(FUND_NAME);
    
    waitForProcessToComplete(2);
    
    response = patch("/increaseallocation",
        new AddMoreAllocationToAnExistingAdvice(ADVISER_IDENTITY, adviseId, FUND_NAME, TICKER4, ADVISE_NEW_ALLOCATION, NEW_MARKET_ORDER_PRICE, 102, 102));
    response.assertStatusCode(OK).assertMediaType("application/json");
    
    waitForProcessToComplete(4);
    
    manualApprove(FUND_NAME);
    
    waitForProcessToComplete(2);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000); 
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+VIRTUAL.name()+"&connectionId=1"),
        wsClient.flow(), materializer));
    Thread.sleep(4000);
    wsClient.expectMessage(
        "{\"currentValue\":7997.5,\"returnPct\":-0.03,\"pnl\":-2.5,\"realizedPnL\":0.0,\"unrealizedPnL\":-2.5,\"allFundPerformance\":[{\"returnPct\":-0.03,\"realizedPnL\":0.0,\"unrealizedPnL\":-2.5,\"pnl\":-2.5,\"currentValue\":7997.5,\"allAdvisePerformance\":[{\"wdId\":\"221-NSE-EQ\",\"symbol\":\"HDFC\",\"realizedPnL\":0.0,\"unrealizedPnL\":-2.5,\"pnl\":-2.5,\"pctPnL\":-0.03,\"currentValue\":2222.0,\"allocation\":27.78,\"nbShares\":22.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":5775.5,\"allocation\":72.22,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}" );
  }

  @Test
  public void return_realtime_fund_with_equity_performance_over_websocket() throws Exception {
    updateLastPriceWithUniverseActor(TICKER, LAST_PRICE_OF_TICKER, currentTimeInMillis());
    
    registerAdviseAndFloatTheirFund();
    
    String equityAdviseId = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER, 100.0, 100, 10, 102, 102));
    
    closeAdvice(equityAdviseId);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/performance?adviser=" + ADVISER_USERNAME + "&fund=" + FUND_NAME + "&connectionId=2"),
          wsClient.flow(), materializer));
    Thread.sleep(100);
    wsClient.expectMessage(
        "{\"performance\":1.5,\"allAdvisePerformances\":[{\"performance\":1.5,\"remainingAllocation\":50.0,\"ticker\":\"IN-NSE-EQ\",\"symbol\":\"ACC\",\"adviseId\":\""+ equityAdviseId + "\"},{\"performance\":0.0,\"remainingAllocation\":50.0,\"ticker\":\"Cash\",\"symbol\":\"Cash\",\"adviseId\":\"Cash\"}]}");
  }
 
  @Test
  public void return_realtime_fund_with_equity_performance_over_websocket_for_successive_issue_on_the_same_ticker() throws Exception {
    updateLastPriceWithUniverseActor(TICKER, LAST_PRICE_OF_TICKER, currentTimeInMillis());
    
    registerAdviseAndFloatTheirFund();
    
    String equityAdviseId = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER, 10.0, 100, 10, 102, 102));
    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/performance?adviser=" + ADVISER_USERNAME + "&fund=" + FUND_NAME + "&connectionId=3"),
          wsClient.flow(), materializer));
    Thread.sleep(100);
    wsClient.expectMessage(
        "{\"performance\":0.1,\"allAdvisePerformances\":[{\"performance\":0.1,\"remainingAllocation\":10.0,\"ticker\":\"IN-NSE-EQ\",\"symbol\":\"ACC\",\"adviseId\":\"" + equityAdviseId + "\"},{\"performance\":0.0,\"remainingAllocation\":90.0,\"ticker\":\"Cash\",\"symbol\":\"Cash\",\"adviseId\":\"Cash\"}]}");
    
    
    TestRouteResult response = patch("/increaseallocation",
        new AddMoreAllocationToAnExistingAdvice(ADVISER_IDENTITY, equityAdviseId, FUND_NAME, TICKER, ADVISE_NEW_ALLOCATION, NEW_MARKET_ORDER_PRICE, 102, 102));
    response.assertStatusCode(OK).assertMediaType("application/json");
    waitForProcessToComplete(4);
    
    materializer = materializer();
    wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/performance?adviser=" + ADVISER_USERNAME + "&fund=" + FUND_NAME + "&connectionId=4"),
          wsClient.flow(), materializer));
    Thread.sleep(100);
    wsClient.expectMessage(
        "{\"performance\":13.29,\"allAdvisePerformances\":[{\"performance\":13.29,\"remainingAllocation\":30.0,\"ticker\":\"IN-NSE-EQ\",\"symbol\":\"ACC\",\"adviseId\":\"" + equityAdviseId + "\"},{\"performance\":0.0,\"remainingAllocation\":70.0,\"ticker\":\"Cash\",\"symbol\":\"Cash\",\"adviseId\":\"Cash\"}]}");
  }
  
  @Test
  public void return_realtime_fund_with_mutual_fund_performance_over_websocket() throws Exception {
    updateLastPriceWithUniverseActor(MF_WDID, LAST_NAV_OF_MF_WDID, currentTimeInMillis());
    
    registerAdviseAndFloatTheirFund();
    
    String mfAdviseId = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, MF_WDID, 25.0, 100, 10, 102, 102));
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/performance?adviser=" + ADVISER_USERNAME + "&fund=" + FUND_NAME + "&connectionId=5"),
          wsClient.flow(), materializer));
    Thread.sleep(100);
    wsClient.expectMessage(
        "{\"performance\":-21.47,\"allAdvisePerformances\":[{\"performance\":-21.47,\"remainingAllocation\":25.0,\"ticker\":\"INF515L01841-MF\",\"symbol\":\"INF515L01841-MF\",\"adviseId\":\""+ mfAdviseId + "\"},{\"performance\":0.0,\"remainingAllocation\":75.0,\"ticker\":\"Cash\",\"symbol\":\"Cash\",\"adviseId\":\"Cash\"}]}");
  }
  

  @Test
  public void return_realtime_virtual_investor_performance_for_equity_over_websocket() throws Exception {
    updateLastPriceWithUniverseActor(TICKER, LAST_PRICE_OF_TICKER, currentTimeInMillis());
   
    registerAdviseAndFloatTheirFund();
    
    String adviseId = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER, 100.0, 100, 10, 102, 102));
    
    closeAdvice(adviseId);
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForProcessToComplete(4);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(4);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, DUMMY_NOTIFICATION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+VIRTUAL.name()+"&connectionId=6"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
        "{\"currentValue\":7919.0,\"returnPct\":-0.06,\"pnl\":-4.75,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"allFundPerformance\":[{\"returnPct\":-0.06,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"currentValue\":7919.0,\"allAdvisePerformance\":[{\"wdId\":\"IN-NSE-EQ\",\"symbol\":\"IN-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"pctPnL\":-0.06,\"currentValue\":3939.0,\"allocation\":49.74,\"nbShares\":39.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":3980.0,\"allocation\":50.26,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}");
  }
  
  @Test
  public void return_realtime_virtual_investor_performance_for_equity_over_websocket_for_single_issue_on_a_ticker() throws Exception {
    updateLastPriceWithUniverseActor(TICKER, LAST_PRICE_OF_TICKER, currentTimeInMillis());
   
    registerAdviseAndFloatTheirFund();
    
    issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER, 10.0, 100, 10, 102, 102));
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForProcessToComplete(4);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(4);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, DUMMY_NOTIFICATION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+VIRTUAL.name()+"&connectionId=6"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
        "{\"currentValue\":7899.8,\"returnPct\":-0.01,\"pnl\":-0.75,\"realizedPnL\":0.0,\"unrealizedPnL\":-0.75,\"allFundPerformance\":[{\"returnPct\":-0.01,\"realizedPnL\":0.0,\"unrealizedPnL\":-0.75,\"pnl\":-0.75,\"currentValue\":7899.8,\"allAdvisePerformance\":[{\"wdId\":\"IN-NSE-EQ\",\"symbol\":\"IN-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-0.75,\"pnl\":-0.75,\"pctPnL\":-0.01,\"currentValue\":707.0,\"allocation\":8.95,\"nbShares\":7.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":7192.8,\"allocation\":91.05,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}");
  }
  
  @Test
  public void return_realtime_virtual_investor_performance_for_equity_over_websocket_for_fund_with_completely_exited_advises() throws Exception {
    updateLastPriceWithUniverseActor(TICKER, LAST_PRICE_OF_TICKER, currentTimeInMillis());
    updateLastPriceWithUniverseActor(TICKER2, LAST_PRICE_OF_TICKER2, currentTimeInMillis());
    updateLastPriceWithUniverseActor(TICKER3, LAST_PRICE_OF_TICKER3, currentTimeInMillis());
   
    registerAdviseAndFloatTheirFund();
    environmentVariables.set("WT_MARKET_OPEN", "true");
    issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER, 10.0, 100, 10, 102, 102));
    String adviseId2 = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER2, 10.0, 245, 10, 300, 103));
    issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER3, 15.0, 1900, 10, 2300, 104));
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForProcessToComplete(4);
    response = authenticatedGet("/subscription",
        "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + "&sipAmount=" + DUMMY_NOTIFICATION_SIPAMOUNT
            + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM2 + "&sipDate=" + DUMMY_NOTIFICATION_SIP_DATE
            + "&investingMode=" + VIRTUAL.getInvestingMode());

    waitForProcessToComplete(4);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        DUMMY_NOTIFICATION_WEALTHCODE, VIRTUAL,  BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    autoApprove(FUND_NAME);
    closeAdviceCompletely(adviseId2, 252);
    waitForProcessToComplete(1);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+VIRTUAL.name()+"&connectionId=7"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    Message message = wsClient.expectMessage();
    assertThat(INVESTOR_EQUITY_PERF_MSGS).contains(message.asTextMessage().getStrictText());
    }
    
  @Test
  public void return_realtime_virtual_investor_performance_for_mutual_fund_over_websocket() throws Exception {
    updateLastPriceWithUniverseActor(MF_WDID, LAST_NAV_OF_MF_WDID, currentTimeInMillis());
   
    registerAdviseAndFloatTheirFund();
    
    issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, MF_WDID, 100.0, 100, 10, 102, 102));
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, DUMMY_NOTIFICATION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, MUTUALFUNDSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+VIRTUAL.name()+"&connectionId=8"),
        wsClient.flow(), materializer));
    Thread.sleep(100);
    wsClient.expectMessage(
            "{\"currentValue\":8000.0,\"returnPct\":0.0,\"pnl\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"allFundPerformance\":[{\"returnPct\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"currentValue\":8000.0,\"allAdvisePerformance\":[{\"wdId\":\"INF515L01841-MF\",\"symbol\":\"INF515L01841-MF\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":8000.0,\"allocation\":100.0,\"nbShares\":566.7007,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":0.0,\"allocation\":0.0,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}");
  }
  
  
  @Test
  public void return_realtime_real_investor_performance_for_equity_over_websocket() throws Exception {
    Mockito.doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(2000000, USER_NAME, REAL);
      wdActorselections.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));
    
    updateLastPriceWithUniverseActor(TICKER, LAST_PRICE_OF_TICKER, currentTimeInMillis());
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    registerAdviseAndFloatTheirFund();
    
    String adviseId = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER, 100.0, 100, 10, 102, 102));
    
    closeAdvice(adviseId);
    
    Thread.sleep(3*1000);
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForProcessToComplete(4);
    approveKYCAndAddWealthcode();
    waitForProcessToComplete(4);
    subscriptionProcess();

    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+REAL.name()+"&connectionId=9"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
        "{\"currentValue\":7919.0,\"returnPct\":-0.06,\"pnl\":-4.75,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"allFundPerformance\":[{\"returnPct\":-0.06,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"currentValue\":7919.0,\"allAdvisePerformance\":[{\"wdId\":\"IN-NSE-EQ\",\"symbol\":\"IN-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"pctPnL\":-0.06,\"currentValue\":3939.0,\"allocation\":49.74,\"nbShares\":39.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":3980.0,\"allocation\":50.26,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"investingMode\":\"REAL\"}");
  }

  private void subscriptionProcess() throws InterruptedException {
    TestRouteResult response;
    Done responseEntity;
    waitForProcessToComplete(3);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + REAL.getInvestingMode());
    waitForProcessToComplete(3);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, WEALTHCODE,
        REAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Subscription in process. You will receive a notification once its done.");
  }
  
  @Test
  public void return_realtime_real_investor_performance_for_mutual_fund_over_websocket() throws Exception {
    Mockito.doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(2000000, USER_NAME, REAL);
      wdActorselections.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));
    
    updateLastPriceWithUniverseActor(MF_WDID, LAST_NAV_OF_MF_WDID, currentTimeInMillis());
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    registerAdviseAndFloatTheirFund();
    
    issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, MF_WDID, 100.0, 100, 10, 102, 102));
    
    Thread.sleep(3*1000);
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    approveKYCAndAddWealthcode();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + REAL.getInvestingMode());
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, WEALTHCODE,
        REAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+REAL.name()+"&connectionId=10"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
            "{\"currentValue\":8000.0,\"returnPct\":0.0,\"pnl\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"allFundPerformance\":[{\"returnPct\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"currentValue\":8000.0,\"allAdvisePerformance\":[{\"wdId\":\"INF515L01841-MF\",\"symbol\":\"INF515L01841-MF\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":8000.0,\"allocation\":100.0,\"nbShares\":566.7007,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":0.0,\"allocation\":0.0,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"investingMode\":\"REAL\"}");
  }
  
  @Test
  public void return_realtime_real_and_virtual_investor_performance_for_equity_over_websocket() throws Exception {
    Mockito.doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(2000000, USER_NAME, REAL);
      wdActorselections.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));
    
    updateLastPriceWithUniverseActor(TICKER, LAST_PRICE_OF_TICKER, currentTimeInMillis());   
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    registerAdviseAndFloatTheirFund();
    
    String adviseId = issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, TICKER, 100.0, 100, 10, 102, 102));
    
    closeAdvice(adviseId);
    
    Thread.sleep(3*1000);
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    approveKYCAndAddWealthcode();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + REAL.getInvestingMode());
    waitForProcessToComplete(1);
    
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, WEALTHCODE,
        REAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+REAL.name()+"&connectionId=11"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
        "{\"currentValue\":7919.0,\"returnPct\":-0.06,\"pnl\":-4.75,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"allFundPerformance\":[{\"returnPct\":-0.06,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"currentValue\":7919.0,\"allAdvisePerformance\":[{\"wdId\":\"IN-NSE-EQ\",\"symbol\":\"IN-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"pctPnL\":-0.06,\"currentValue\":3939.0,\"allocation\":49.74,\"nbShares\":39.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":3980.0,\"allocation\":50.26,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"investingMode\":\"REAL\"}");
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    materializer = materializer();
    wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+VIRTUAL.name()+"&connectionId=12"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
        "{\"currentValue\":7919.0,\"returnPct\":-0.06,\"pnl\":-4.75,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"allFundPerformance\":[{\"returnPct\":-0.06,\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"currentValue\":7919.0,\"allAdvisePerformance\":[{\"wdId\":\"IN-NSE-EQ\",\"symbol\":\"IN-NSE-EQ\",\"realizedPnL\":0.0,\"unrealizedPnL\":-4.75,\"pnl\":-4.75,\"pctPnL\":-0.06,\"currentValue\":3939.0,\"allocation\":49.74,\"nbShares\":39.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":3980.0,\"allocation\":50.26,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}");

  }
  
  @Test
  public void return_realtime_real_and_virtual_investor_performance_for_mutual_fund_over_websocket() throws Exception {
    Mockito.doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(2000000, USER_NAME, REAL);
      wdActorselections.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));
    
    updateLastPriceWithUniverseActor(MF_WDID, LAST_NAV_OF_MF_WDID, currentTimeInMillis());   
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    registerAdviseAndFloatTheirFund();
    
    issueAdvise(new IssueAdvice(ADVISER_IDENTITY_ID, FUND_NAME, MF_WDID, 100.0, 100, 10, 102, 102));
    
    Thread.sleep(3*1000);
    
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(USER_NAME, INVESTOR_NAME, "", USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    approveKYCAndAddWealthcode();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + REAL.getInvestingMode());
    waitForProcessToComplete(1);
    
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, WEALTHCODE,
        REAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+REAL.name()+"&connectionId=13"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
            "{\"currentValue\":8000.0,\"returnPct\":0.0,\"pnl\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"allFundPerformance\":[{\"returnPct\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"currentValue\":8000.0,\"allAdvisePerformance\":[{\"wdId\":\"INF515L01841-MF\",\"symbol\":\"INF515L01841-MF\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":8000.0,\"allocation\":100.0,\"nbShares\":566.7007,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":0.0,\"allocation\":0.0,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"REAL\"}],\"investingMode\":\"REAL\"}");
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_NOTIFICATION_SIPAMOUNT + "&lumpSumAmount=" + DUMMY_NOTIFICATION_LUMPSUM + "&sipDate=" +
        DUMMY_NOTIFICATION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());

    waitForProcessToComplete(1);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");

    waitForProcessToComplete(4);
    
    materializer = materializer();
    wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/investorPerformance?email=" + USER_NAME + "&investingMode="+VIRTUAL.name()+"&connectionId=14"),
        wsClient.flow(), materializer));
    Thread.sleep(1000);
    wsClient.expectMessage(
            "{\"currentValue\":8000.0,\"returnPct\":0.0,\"pnl\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"allFundPerformance\":[{\"returnPct\":0.0,\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"currentValue\":8000.0,\"allAdvisePerformance\":[{\"wdId\":\"INF515L01841-MF\",\"symbol\":\"INF515L01841-MF\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":8000.0,\"allocation\":100.0,\"nbShares\":566.7007,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"},{\"wdId\":\"Cash\",\"symbol\":\"Cash\",\"realizedPnL\":0.0,\"unrealizedPnL\":0.0,\"pnl\":0.0,\"pctPnL\":0.0,\"currentValue\":0.0,\"allocation\":0.0,\"nbShares\":0.0,\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"fundName\":\"WeeklyPicks\",\"adviserUsername\":\"swapnil\",\"investingMode\":\"VIRTUAL\"}],\"investingMode\":\"VIRTUAL\"}");

  }
  
  @Test
  public void return_realtime_market_price_for_stock_over_websocket() throws Exception {
    updateLastPriceWithUniverseActor(DUMMY_STOCK_TICKER, DUMMY_STOCK_PRICE_IN_MARKET, currentTimeInMillis());   
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
        Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/marketprice?wdId=" + DUMMY_STOCK_TICKER + "&connectionId=15"),
        wsClient.flow(), materializer));
    wsClient.expectMessage("{\"ticker\":\""+DUMMY_STOCK_TICKER+"\",\"price\":"+DUMMY_STOCK_PRICE_IN_MARKET+"}");    
  }
  
  @Test
  public void return_realtime_market_price_for_mutual_fund_over_websocket() throws Exception {
    updateLastPriceWithUniverseActor(DUMMY_MUTUAL_FUND_TICKER, DUMMY_MUTUAL_FUND_PRICE_IN_MARKET, currentTimeInMillis());   
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
        Materializer materializer = materializer();
    WSProbe wsClient = WSProbe.create(system, materializer, 100, 100 * 1000);
    routeRealTime().run(WS(Uri.create("/marketprice?wdId=" + DUMMY_MUTUAL_FUND_TICKER + "&connectionId=16"),
        wsClient.flow(), materializer));
    wsClient.expectMessage("{\"ticker\":\""+DUMMY_MUTUAL_FUND_TICKER+"\",\"price\":"+DUMMY_MUTUAL_FUND_PRICE_IN_MARKET+"}");    
  }
  
  private void approveKYCAndAddWealthcode() {
    TestRouteResult response = userCommandPost("/approvekyc",
        new KycApproveCommand(CLIENT_CODE, PANCARD, USER_NAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = postAuthUserCommand("/wealthcode", new WealthCode(WEALTHCODE));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  private void waitForProcessToComplete(int time) throws InterruptedException {
    Thread.sleep(time * 1000);
  }

  private String issueAdvise(IssueAdvice issuedAdvice) throws Exception {
    TestRouteResult response = post("/issueadvise", issuedAdvice);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    String adviseId = responseEntity.getId();
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    return adviseId;
  }

  private void closeAdvice(String adviseId) {
    TestRouteResult response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY_ID, FUND_NAME, adviseId, 50.0, 12, 102));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }
  
  private void closeAdviceCompletely(String adviseId, double exitPrice) {
    TestRouteResult response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY_ID, FUND_NAME, adviseId, 10.0, 12, exitPrice));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  
  private void registerAdviseAndFloatTheirFund() {
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY_ID, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION,
        FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  private void registerAdviserProcess() {
    TestRouteResult response = post("/registeradviserfromuserpool", ADVISER);
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  private void autoApprove(String fundName) {
    postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(fundName, ADVISER_IDENTITY, VIRTUAL, Auto));
   }
  
  private void manualApprove(String fundName) {
    postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(fundName, ADVISER_IDENTITY, VIRTUAL, Manual));
   }
  
  private void updateLastPriceWithUniverseActor(String ticker, double price, long time) {
    wdActorselections.universeRootActor().tell(
        new PriceWithPaf(new InstrumentPrice(price, time, ticker), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }
  
  private TestRouteResult userCommandPost(String url, UnAuthenticatedUserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult postAuthUserCommand(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(POST(url).addHeader(RawHeader.create("username", USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }
  
  private TestRouteResult authenticatedGet(String url, String queryString) {
    TestRouteResult response;
    if (queryString != "" || queryString != null) {
      response = route().run(GET(url + "?" + queryString)
          .addHeader(RawHeader.create("username", USER_NAME)));
    } else {
      response = route().run(GET(url).addHeader(RawHeader.create("username", USER_NAME)));
    }
    return response;
  }

  protected TestRouteResult patchAuthUserCommand(String url, Object command) {
    return postAuthUserCommand(url, command);
  }
  
  private TestRouteResult post(String url, UnAuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult post(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  protected TestRouteResult patch(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(PATCH(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRoute route() {
    return testRoute(wdService.appRoute());
  }
  private TestRoute routeRealTime() {
    return testRoute(service.appRoute());
  }
  
  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(new UpdateEquityInstrumentSymbol(String.valueOf(token), "", symbol, ticker, "", "NSE", "EQ"), ActorRef.noSender());
  }

  private String updateInstrumentsMapOfMutualFundActor(String token, String segment, String schemeName, String schemeCode, String minInvestment,
      String minMultiplier, String schemeType, String exitLoad, String rta) throws Exception {
    Set<MutualFundInstrument> mutualFundInstruments = new HashSet<>();
    mutualFundInstruments.add(new MutualFundInstrument(token, segment, schemeName, schemeCode, minInvestment, minMultiplier, 
        schemeType, exitLoad, rta));
    Done result = (Done) askAndWaitForResult(universeRootActor, new UpdateMFInstruments(mutualFundInstruments));
    return result.getMessage();
  }
  
}
