package com.wt.realtimeservice;

import static com.wt.domain.BrokerName.BPWEALTH;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.CORPORATEACTION;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.broadcast.xneutrino.XNeutrinoBroadcast;
import com.wt.ctcl.CTCLMock;
import com.wt.ctcl.CorporateEventCTCLMock;
import com.wt.ctcl.VirtualMutualFundInteractive;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.Wallet;
import com.wt.util.BroadcastManagerStub;
import com.wt.utils.akka.WDActorSelections;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan("com.wt")
@PropertySource("classpath:test-environment.properties")
public class RealTimeTestAppConfiguration extends TestAppConfiguration {

  private @Autowired @Qualifier("WDActors") WDActorSelections wdActorSelections;

  @Bean 
  public BroadCastManager broadCastManager() throws NumberFormatException, IOException {
    return new BroadcastManagerStub(properties().getProperty("xneutrino.broadcast.ip"),
        Integer.parseInt(properties().getProperty("xneutrino.broadcast.port")));
  }
  
  @Bean
  public CtclBroadcast ctclBroadcast() throws Exception {
      return new XNeutrinoBroadcast(broadCastManager(), wdActorSelections, wdProperties);
  }
  
  @Bean(name="RealWallet")
  public Wallet realWallet()  {
    return mock(Wallet.class);
  }
  
  
  @Bean
  public Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation() throws IOException {
    Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation = new HashMap<BrokerType, CtclInteractive>();
    brokerTypeWithCTCLImplementation.put(new BrokerType(EQUITYSUBSCRIPTION, REAL, BPWEALTH), new CTCLMock(actorSystem(), wdActorSelections));
    brokerTypeWithCTCLImplementation.put(new BrokerType(EQUITYSUBSCRIPTION, VIRTUAL, MOCK), new CTCLMock(actorSystem(), wdActorSelections));
    brokerTypeWithCTCLImplementation.put(new BrokerType(CORPORATEACTION, VIRTUAL, MOCK), new CorporateEventCTCLMock(wdActorSelections));
    return brokerTypeWithCTCLImplementation;
  }
  
  @Bean
  public Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation() throws IOException {
    Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation = new HashMap<BrokerType, MutualFundInteractive>();
    brokerTypeWithMFCTCLImplementation.put(new BrokerType(MUTUALFUNDSUBSCRIPTION, VIRTUAL, MOCK), new VirtualMutualFundInteractive(wdActorSelections));
    brokerTypeWithMFCTCLImplementation.put(new BrokerType(MUTUALFUNDSUBSCRIPTION, REAL, BPWEALTH), new VirtualMutualFundInteractive(wdActorSelections));
    return brokerTypeWithMFCTCLImplementation;
  }

}