package com.wt.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.wt.domain.write.events.FundToWdIdAdded;
import com.wt.domain.write.events.FundToWdIdRemoved;
import com.wt.domain.write.events.ResetActiveInstruments;
import com.wt.domain.write.events.UpdateWdIdMap;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ActiveInstrumentTracker implements Serializable {

  private static final long serialVersionUID = 1L;
  private Map<String, List<String>> wdIdToListOfFundsMap;
  
  public void update(Object evt) {

    if (evt instanceof FundToWdIdAdded) {
      FundToWdIdAdded addFundToWdId = (FundToWdIdAdded) evt;
      updateWdIdToListOfUserFundsMap(addFundToWdId);
    } else if (evt instanceof FundToWdIdRemoved) {
      FundToWdIdRemoved removeFundToWdId = (FundToWdIdRemoved) evt;
      updateWdIdToListOfUserFundsMap(removeFundToWdId);
    } else if (evt instanceof ResetActiveInstruments) {
      clearWdIdToFundListMap();
    }
  }
  
  private void clearWdIdToFundListMap() {
    wdIdToListOfFundsMap.clear();
  }
  
  private void updateWdIdToListOfUserFundsMap(UpdateWdIdMap mapUpdationCommand) {
    List<String> fundIdsList;
    if(wdIdToListOfFundsMap.containsKey(mapUpdationCommand.getWdId()))
      fundIdsList = wdIdToListOfFundsMap.get(mapUpdationCommand.getWdId());
    else
      fundIdsList = new ArrayList<String>();
    wdIdToListOfFundsMap.put(mapUpdationCommand.getWdId(), fundIdsList);
    
    
    if(mapUpdationCommand instanceof FundToWdIdAdded ){
      FundToWdIdAdded addFundToWdId = (FundToWdIdAdded) mapUpdationCommand;
      if(!fundIdsList.contains(addFundToWdId.getFundNameWithAdviserUserName())){
        fundIdsList.add(addFundToWdId.getFundNameWithAdviserUserName());
      }
    }else{
      FundToWdIdRemoved removeFromFund = (FundToWdIdRemoved) mapUpdationCommand;
      fundIdsList.remove(removeFromFund.getFundNameWithAdviserUserName());
      if(fundIdsList.size() == 0)
        wdIdToListOfFundsMap.remove(removeFromFund.getWdId());
    }
  }

  public ActiveInstrumentTracker() {
    this.wdIdToListOfFundsMap =  new ConcurrentHashMap<String,  List<String>>();
  }

}
