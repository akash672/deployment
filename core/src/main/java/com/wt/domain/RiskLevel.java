package com.wt.domain;
public enum RiskLevel {
  LOW, MEDIUM, HIGH
}