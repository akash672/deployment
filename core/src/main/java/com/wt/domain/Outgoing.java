package com.wt.domain;

import lombok.ToString;

@ToString
public class Outgoing {
  public final String message;

  public Outgoing(String message) {
    this.message = message;
  }
}