package com.wt.domain;

import static com.wt.domain.PriceWithPaf.noPrice;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(of="fundConstituentKey")
@NoArgsConstructor
public class FundConstituent implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Getter
  @Setter
  @DynamoDBIgnore
  private FundConstituentKey fundConstituentKey = new FundConstituentKey() ;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String ticker;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String symbol;

  @Getter
  @DynamoDBAttribute
  private double allocation;

  @Getter
  @Setter
  @DynamoDBIgnore
  private PriceWithPaf entryPrice;

  @Getter
  @Setter
  @DynamoDBIgnore
  private PriceWithPaf exitPrice;


  public void setAllocation(double allocation) {
    if (!isZero(allocation)) {
      this.allocation = round(allocation);
      return;
    }
    this.allocation = 0.;
  }

  public FundConstituent(String adviseId, String fund, String adviserUsername, String ticker, String symbol,
      double allocation, PriceWithPaf entryPrice, PriceWithPaf exitPrice) {
    super();
    this.fundConstituentKey = new FundConstituentKey(adviseId, fund, adviserUsername);
    this.ticker = ticker;
    this.symbol = symbol;
    this.allocation = allocation;
    this.entryPrice = entryPrice;
    this.exitPrice = exitPrice;
  }

  public FundConstituent(String adviseId, String fund, String adviserUsername, String ticker,
      double allocation, PriceWithPaf entryPrice, PriceWithPaf exitPrice) {
    super();
    this.fundConstituentKey = new FundConstituentKey(adviseId, fund, adviserUsername);
    this.ticker = ticker;
    this.symbol = ticker;
    this.allocation = allocation;
    this.entryPrice = entryPrice;
    this.exitPrice = exitPrice;
  }

  public FundConstituent(FundConstituentKey funConstituentKey, String ticker, double allocation,
      PriceWithPaf entryPrice, PriceWithPaf exitPrice) {
    super();
    this.fundConstituentKey = funConstituentKey;
    this.ticker = ticker;
    this.symbol = ticker;
    this.allocation = allocation;
    this.entryPrice = entryPrice;
    this.exitPrice = exitPrice;
  }

  @DynamoDBHashKey(attributeName = "adviseId")
  public String getAdviseId() {
    return fundConstituentKey.getAdviseId();
  }

  public void setAdviseId(String adviseId) {
    fundConstituentKey.setAdviseId(adviseId);
  }

  @DynamoDBRangeKey(attributeName = "fundWithAdviser")
  public String getFundWithAdviser() {
    return fundConstituentKey.getFundWithAdviser();
  }

  public void setFundWithAdviser(String fundWithAdviser) {
    fundConstituentKey.setFundWithAdviser(fundWithAdviser);
  }
  
  public static FundConstituent withActiveAdvise(String adviseId, String fund, String adviserUsername, String ticker,
      String symbol, double allocation, PriceWithPaf entryPrice) {
    return new FundConstituent(adviseId, fund, adviserUsername, ticker, symbol, allocation, entryPrice,
        noPrice(ticker));
  }
  
  public static FundConstituent withJustCash(String adviseId, String fund, String adviserUsername, String ticker,
      String symbol, double allocation) {
    return new FundConstituent(adviseId, fund, adviserUsername, ticker, symbol, allocation, noPrice(ticker),
        noPrice(ticker));
  }
  
  public static FundConstituent withBlankAdviseId(String fund, String adviserUsername, double allocation, String ticker,
      String symbol, PriceWithPaf entryPrice) {
    return new FundConstituent("", fund, adviserUsername, ticker, symbol, allocation, entryPrice,
        noPrice(ticker));
  }

  public static FundConstituent withActiveAdvise(String adviseId, String fund, String adviserUsername, String ticker,
      double allocation, PriceWithPaf entryPrice) {
    return new FundConstituent(adviseId, fund, adviserUsername, ticker, allocation, entryPrice,
        noPrice(ticker));
  }

  public static FundConstituent withJustCash(String adviseId, String fund, String adviserUsername, String ticker,
      double allocation) {
    return new FundConstituent(adviseId, fund, adviserUsername, ticker, allocation, noPrice(ticker),
        noPrice(ticker));
  }
  
  public static FundConstituent withBlankAdviseId(String fund, String adviserUsername, double allocation, String ticker,
      PriceWithPaf entryPrice) {
    return new FundConstituent("", fund, adviserUsername, ticker, allocation, entryPrice,
        noPrice(ticker));
  }
}
