package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class ValidateUnSubscribeOtp {
  private @Getter final String subscriberNumber;
  private @Getter final String fundName;
  private @Getter final String otp;
  
  @JsonCreator
  public ValidateUnSubscribeOtp(@JsonProperty("subscriberNumber") String subscriberNumber, @JsonProperty("fundName") String fundName, @JsonProperty("otp") String otp) {
    this.subscriberNumber = subscriberNumber;
    this.fundName = fundName;
    this.otp = otp;
  }
}