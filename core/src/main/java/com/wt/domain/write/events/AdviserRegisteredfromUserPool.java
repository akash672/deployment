package com.wt.domain.write.events;

import com.wt.aws.cognito.configuration.CredentialsProviderType;

import lombok.Getter;

@Getter
public class AdviserRegisteredfromUserPool extends AdviserRegistered {
  private static final long serialVersionUID = 1L;
  private String phoneNumber;
  private String company;
  private String publicARN;

  public AdviserRegisteredfromUserPool(String adviserUsername, String adviserName, String email, long timestamp,
      CredentialsProviderType type, String phoneNumber, String company, String publicARN) {
    super(adviserUsername, adviserName, email, adviserUsername, timestamp, type);
    this.phoneNumber = phoneNumber;
    this.company = company;
    this.publicARN = publicARN;
  }
}
