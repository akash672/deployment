package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wt.domain.write.commands.IssueAdvice;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
@ToString
public class AdviseIssuedFailed extends FundEvent {

  @NonNull
  @Getter
  private final String token;
  @Getter
  private final double allocation;
  @Getter
  private final double entryPrice;
  @Getter
  private final long entryTime;
  @Getter
  private final double exitPrice;
  @Getter
  private final long exitTime;
  @Getter
  @JsonIgnore
  private final long issueTime;
  @Getter
  private final String adviseId;
  @Getter
  private final String title;
  @Getter
  private final String errorMessage;
  @Getter
  private final String identityId;

  private static final long serialVersionUID = 1L;

  @Deprecated
  public AdviseIssuedFailed(String adviserUsername, String fundName, String token, double allocation, double entryPrice,
      long entryTime, double exitPrice, long exitTime, long issueTime, String adviseId, String title,
      String errorMessage, String identityId) {
    super(adviserUsername, fundName);
    this.token = token;
    this.allocation = allocation;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.issueTime = issueTime;
    this.adviseId = adviseId;
    this.title = title;
    this.errorMessage = errorMessage;
    this.identityId = adviserUsername;
  }
  
  public AdviseIssuedFailed(String adviserUsername, String fundName, String token, double allocation, double entryPrice,
      long entryTime, double exitPrice, long exitTime, long issueTime, String adviseId, String title,
      String errorMessage) {
    super(adviserUsername, fundName);
    this.token = token;
    this.allocation = allocation;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.issueTime = issueTime;
    this.adviseId = adviseId;
    this.title = title;
    this.errorMessage = errorMessage;
    this.identityId = adviserUsername;
  }

  public AdviseIssuedFailed(AdviseReceivedIssuancePending adviseReceived, String title, String errorMessage, String identityId) {
    super(adviseReceived.getAdviserUsername(), adviseReceived.getFundName());
    this.token = adviseReceived.getToken();
    this.allocation = adviseReceived.getAllocation();
    this.entryPrice = adviseReceived.getEntryPrice();
    this.entryTime = adviseReceived.getEntryTime();
    this.exitPrice = adviseReceived.getExitPrice();
    this.exitTime = adviseReceived.getExitTime();
    this.issueTime = adviseReceived.getIssueTime();
    this.adviseId = adviseReceived.getAdviseId();
    this.title = title;
    this.errorMessage = errorMessage;
    this.identityId = getAdviserUsername();
  }
  
  public AdviseIssuedFailed(IssueAdvice adviseReceived, String title, String errorMessage, String identityId) {
    super(adviseReceived.getAdviserUsername(), adviseReceived.getFundName());
    this.token = adviseReceived.getToken();
    this.allocation = adviseReceived.getAllocation();
    this.entryPrice = adviseReceived.getEntryPrice();
    this.entryTime = adviseReceived.getEntryTime();
    this.exitPrice = adviseReceived.getExitPrice();
    this.exitTime = adviseReceived.getExitTime();
    this.issueTime = adviseReceived.getIssueTime();
    this.adviseId = adviseReceived.getAdviseId();
    this.title = title;
    this.errorMessage = errorMessage;
    this.identityId = getAdviserUsername();
  }

}