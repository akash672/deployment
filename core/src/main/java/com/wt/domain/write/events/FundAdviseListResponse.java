package com.wt.domain.write.events;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.Advise;

import lombok.Getter;

@Getter
@JsonIgnoreProperties({ "id" })
public class FundAdviseListResponse extends Done {
  private static final long serialVersionUID = 1L;
  private Collection<Advise> fundAdviseList;

  @JsonCreator
  public FundAdviseListResponse(@JsonProperty("id") String id,
      @JsonProperty("fundAdviseList") Collection<Advise> fundAdviseList) {
    super(id, "");
    this.fundAdviseList = fundAdviseList;
  }

  @java.lang.SuppressWarnings("all")
  public Collection<Advise> getFundAdviseList() {
    return this.fundAdviseList;
  }
}
