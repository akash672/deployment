package com.wt.domain.write.events;

import com.wt.domain.PriceWithPaf;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode(callSuper=false)
@ToString
@Getter
public class FundAdviceClosed extends AdviseEvent {
  private static final long serialVersionUID = 1L;
  private double allocation;
  private double absoluteAllocation;
  private long exitDate;
  private PriceWithPaf exitPrice;
  private boolean isSpecialAdvise;

  public FundAdviceClosed(String adviserUsername, String fundName, String adviseId, double allocation,
      double absoluteAllocation, long exitDate, PriceWithPaf exitPrice, boolean isSpecialAdvise) {
    super(adviserUsername, fundName, adviseId);
    this.allocation = allocation;
    this.exitDate = exitDate;
    this.absoluteAllocation = absoluteAllocation;
    this.exitPrice = exitPrice;
    this.isSpecialAdvise = isSpecialAdvise;
  }
}
