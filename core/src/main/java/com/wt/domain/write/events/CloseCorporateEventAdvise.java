package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
  
public class CloseCorporateEventAdvise extends FundEvent {
  private static final long serialVersionUID = 1L;
  private String token;
  private double price;
  private String adviseId;
  

  public CloseCorporateEventAdvise(String adviserUsername, String fundName, String token, double price,
                                   String adviseId) {
    super(adviserUsername, fundName);
    this.token = token;
    this.price = price;
    this.adviseId = adviseId;
    
  }

}
