package com.wt.domain.write.events;

import lombok.Getter;

public class RecommendedEntryPriceUpdated extends FundEvent {

  private static final long serialVersionUID = 1L;
  @Getter
  private double recoEntryPrice;
  @Getter
  private String adviseId;
  
  public RecommendedEntryPriceUpdated(String adviserUsername, String fundName, double recoEntryPrice, String adviseId) {
    super(adviserUsername, fundName);
    this.recoEntryPrice = recoEntryPrice;
    this.adviseId = adviseId;
  }

}
