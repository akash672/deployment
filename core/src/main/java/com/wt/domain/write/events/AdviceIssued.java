package com.wt.domain.write.events;

import lombok.Getter;

public class AdviceIssued extends AdviseEvent {
	private static final long serialVersionUID = 1L;

	@Getter private String token;
	@Getter private double allocation;
	@Getter private long issueTime;
	@Getter private double entryPrice;
	@Getter private long entryTime;
	@Getter private double exitPrice;
	@Getter private long exitTime;

	public AdviceIssued(String adviserUsername, String fundName, String adviseId, String token, 
			double allocation, double entryPrice, long entryTime,
	    double exitPrice, long exitTime, long issueTime) {
		super(adviserUsername, fundName, adviseId);
		this.token = token;
		this.allocation = allocation;
		this.issueTime = issueTime;
		this.entryPrice = entryPrice;
		this.entryTime = entryTime;
		this.exitPrice = exitPrice;
		this.exitTime = exitTime;
	}

	@Override
	public String toString() {
		return "AdviceIssued [token=" + token + ", allocation=" + allocation + ", issueTime=" + issueTime + ", entryPrice="
		    + entryPrice + ", entryTime=" + entryTime + ", exitPrice=" + exitPrice + ", exitTime=" + exitTime
		    + ", getAdviseId()=" + getAdviseId() + "]";
	}

}
