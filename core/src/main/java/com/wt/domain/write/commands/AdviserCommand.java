package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public abstract class AdviserCommand {
  @NonNull
  private String adviserUsername;

  public AdviserCommand(String name) {
    super();
    this.adviserUsername = name;
    assertStringNotEmpty(adviserUsername, "adviserUsername");
  }

}
