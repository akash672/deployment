package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviserCorporateEventPrimaryBuy extends AdviserCorporateEvents {

  private static final long serialVersionUID = 1L;
  private String ticker;
  private double oldPrice;
  private double newPrice;
  private double investorsSharesRatio;
  private String symbol;
 
  
  public AdviserCorporateEventPrimaryBuy(@JsonProperty("token")String ticker, 
      @JsonProperty("oldPrice")double oldPrice, @JsonProperty("newPrice")double newPrice ,@JsonProperty("investorsSharesRatio")double investorsSharesRatio
      , @JsonProperty("symbol")String symbol) {
    this.ticker = ticker;
    this.oldPrice = oldPrice;
    this.newPrice = newPrice;
    this.investorsSharesRatio = investorsSharesRatio;
    this.symbol = symbol;
  }
  

}

