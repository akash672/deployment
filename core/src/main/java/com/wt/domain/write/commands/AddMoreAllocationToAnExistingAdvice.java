package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.utils.DateUtils;

import lombok.ToString;

@ToString(callSuper=true)
public class AddMoreAllocationToAnExistingAdvice extends IssueAdvice {

  private static final long serialVersionUID = 1L;
  public AddMoreAllocationToAnExistingAdvice(String username, String fundName, String adviseId, String token,
      double allocation, double entryPrice, long entryTime, double exitPrice, long exitTime, long issueTime) {
    super(username, fundName, adviseId, token, allocation, entryPrice, entryTime, exitPrice, exitTime, issueTime);
  }

  @JsonCreator
  public AddMoreAllocationToAnExistingAdvice(@JsonProperty("username") String username,
      @JsonProperty("adviseId") String adviseId, @JsonProperty("fundName") String fundName,
      @JsonProperty("token") String token, @JsonProperty("allocation") double allocation,
      @JsonProperty("entryPrice") double entryPrice, @JsonProperty("exitPrice") double exitPrice,
      @JsonProperty("exitTime") long exitTime) {
    this(username, fundName, adviseId, token, allocation, entryPrice,
        DateUtils.currentTimeInMillis(), exitPrice, exitTime, DateUtils.currentTimeInMillis());

  }

}