package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class FundNamesForWdIdSent implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<String> fundNames;
   
  
}
