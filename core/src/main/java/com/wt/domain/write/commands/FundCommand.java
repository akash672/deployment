package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString(callSuper=true)
public abstract class FundCommand extends AuthenticatedAdviserCommand {
  
  private static final long serialVersionUID = 1L;
  @NonNull
  private String fundName;

  public FundCommand(String adviserUsername, String fundName) {
    super(adviserUsername);
    this.fundName = fundName;
    assertStringNotEmpty(fundName, "fundName");
  }

}
