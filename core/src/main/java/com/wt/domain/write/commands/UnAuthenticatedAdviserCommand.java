package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import com.wt.aws.cognito.configuration.CredentialsProviderType;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString
public class UnAuthenticatedAdviserCommand {
  @NonNull
  private String adviserUsername;
  @NonNull
  private CredentialsProviderType type;

  public UnAuthenticatedAdviserCommand(String adviserUsername, CredentialsProviderType type) {
    super();
    this.adviserUsername = adviserUsername;
    this.type = type;
    assertStringNotEmpty(adviserUsername, "adviserUsername");
  }

}
