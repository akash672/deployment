package com.wt.domain.write;

import static akka.http.javadsl.model.StatusCodes.BAD_REQUEST;
import static akka.http.javadsl.model.StatusCodes.EXPECTATION_FAILED;
import static akka.http.javadsl.model.StatusCodes.NOT_FOUND;
import static com.wt.aws.cognito.configuration.CredentialsProviderType.Cognito;
import static com.wt.domain.write.events.Exists.exists;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.BadRequestException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.wt.domain.Adviser;
import com.wt.domain.Notification;
import com.wt.domain.NotificationCompositeKey;
import com.wt.domain.NotificationTypes;
import com.wt.domain.write.commands.AdviserCorpActionPrimaryBuy;
import com.wt.domain.write.commands.AdviserCorpActionSecondaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventPrimaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSecondaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSell;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.CloseFund;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.FundCommand;
import com.wt.domain.write.commands.GetCurrentFundComposition;
import com.wt.domain.write.commands.GetFundDetails;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.InternalAdviserCommand;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.events.AdviserEvent;
import com.wt.domain.write.events.AdviserFundClosed;
import com.wt.domain.write.events.AdviserFundFloated;
import com.wt.domain.write.events.AdviserRegistered;
import com.wt.domain.write.events.AdviserRegisteredfromUserPool;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundEvent;
import com.wt.utils.DateUtils;
import com.wt.utils.NotificationFacade;

import akka.http.javadsl.model.StatusCodes;
import akka.persistence.AbstractPersistentActor;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
@Service("AdviserActor")
@Scope("prototype")
public class AdviserActor extends AbstractPersistentActor implements ParentActor<FundActor> {
  private String persistenceId;
  private Adviser adviser = new Adviser();
  @Getter
  private String lastSentOTP;
  @Getter
  private long lastSentOTPTimestamp;
  private NotificationFacade notificationFacade;

  public AdviserActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(RegisterAdviserFromUserPool.class, command -> {
      registerAdviserFromUserPool(command);
    }).match(AuthenticatedAdviserCommand.class, command -> {
      handleAuthenticatedAdviser(command);
    }).match(InternalAdviserCommand.class, command -> {
      handleInternalAdviser(command);
    }).match(GetFundDetails.class, command -> {
      log.info("Received command " + command);
      if(adviser.getFunds().contains(command.getFundName()))
        forwardToChildren(context(), command, command.getFundName());
      else
        sender().tell(failed(command.getFundName(), "Fund does not exist for the adviser specified.", StatusCodes.NOT_FOUND), self());
    }).match(FundEvent.class, evt -> {
      handleEventForNotification(evt);
    }).match(AdviserCorporateEventSell.class, command -> {
      forwardCommandToAllChildren(command);
    }).match(AdviserCorporateEventPrimaryBuy.class, command -> {
      AdviserCorporateEventPrimaryBuy buy = (AdviserCorporateEventPrimaryBuy) command;
      AdviserCorpActionPrimaryBuy primaryBuy = new AdviserCorpActionPrimaryBuy(buy.getTicker(), buy.getOldPrice(),
          buy.getNewPrice(), adviser.getAdviserUsername(), buy.getInvestorsSharesRatio(), buy.getSymbol());
      forwardCommandToAllChildren(primaryBuy);
    }).match(AdviserCorporateEventSecondaryBuy.class, command -> {
      AdviserCorporateEventSecondaryBuy buy = (AdviserCorporateEventSecondaryBuy) command;
      AdviserCorpActionSecondaryBuy secondaryBuy = new AdviserCorpActionSecondaryBuy(buy.getOldTicker(),
          buy.getNewTicker(), buy.getCashAllocationRatio(), buy.getSharesRatio(),
          adviser.getAdviserUsername(), buy.getNewSymbol());
      forwardCommandToAllChildren(secondaryBuy);
    }).build();
  }

  private void handleEventForNotification(FundEvent evt) {
    if (evt instanceof FundAdviceIssued) {
      FundAdviceIssued advise = (FundAdviceIssued) evt;
      sendSNSNotification(evt.getFundName(), "Issue Advise Completed",
          "Advise Issued successfully in " + getSymbolFor(advise.getToken()) + " at price: "
              + advise.getEntryPrice().getPrice().getPrice() + " with allocation: " + advise.getAllocation()
              + ", Advise Id:" + advise.getAdviseId() + "");
    } else if (evt instanceof FundAdviceClosed) {
      FundAdviceClosed advise = (FundAdviceClosed) evt;
      sendSNSNotification(evt.getFundName(), "Close Advise Completed",
          "Advise Closed successfully for Advise Id: " + advise.getAdviseId() + " at price: "
              + advise.getExitPrice().getPrice().getPrice() + " with allocation: " + advise.getAllocation() + "");
    }
    log.info("Notification is being sent for Fund: " + evt.getFundName() + ", Adviser: " + evt.getAdviserUsername());
  }
  
  private String getSymbolFor(String ticker) {
    try {
      Object result = askAndWaitForResult(
          context().parent(), new GetSymbolForWdId(ticker));
      if (result instanceof Failed) {
        log.warn("Error while getting symbol for wdId " + ticker);
        return ticker;
      }
      
      SymbolSentForWdId symbolReceived = (SymbolSentForWdId) result;
      return symbolReceived.getSymbol();
    } catch (Exception e) {
      log.warn("AdviserActor: failed to get symbol from AdviserRootActor for ticker " + ticker);
      return ticker;
    }
  }

  private void handleInternalAdviser(InternalAdviserCommand command) {
    if (command instanceof GetCurrentFundComposition) {
      GetCurrentFundComposition getRealtimeFundComposition = (GetCurrentFundComposition) command;
      forwardToChildren(context(), getRealtimeFundComposition, getRealtimeFundComposition.getFundName());
    }
  }

  private void handleAuthenticatedAdviser(AuthenticatedAdviserCommand command) throws Exception {
    if (command instanceof FloatFund) {
      FloatFund cmd = (FloatFund) command;
      if (cmd instanceof FloatFund) {
        try {
          cmd.validateCommand();
        } catch (IllegalArgumentException e) {
          sender().tell(failed(cmd.getFundName() + ", From " + cmd.getAdviserUsername() + " failed to launch",
              e.getMessage(), BAD_REQUEST), self());
          return;
        }
        ((FloatFund) cmd).setAdviserUsername(adviser.getAdviserUsername());
        ((FloatFund) cmd).setAdviserName(adviser.getAdviserName());
      }
      if (!adviser.hasFund(cmd.getFundName())) {
        if (cmd.getFundName().contains(" "))
        {
          sender().tell(failed(cmd.getAdviserUsername(),
              "New Fund could not be floated as it contains white space in its name", BAD_REQUEST),
              self());
          return;
        }
        AdviserFundFloated adviserFundFloated = new AdviserFundFloated(adviser.getAdviserUsername(),
            adviser.getAdviserName(), cmd.getFundName());
        persist(adviserFundFloated, (AdviserFundFloated evt) -> {
          adviser.update(evt);
          forwardToChildren(context(), cmd, cmd.getFundName());
        });
      } else {
        sender().tell(exists(cmd.getFundName()), self());
      }
    } else if (command instanceof CloseFund) {
      CloseFund closeFund = (CloseFund) command;
      try {
        closeFund.validateCommand();
      } catch (IllegalArgumentException e) {
        sender().tell(failed(closeFund.getFundName() + ", From " + closeFund.getUsername() + " failed to close ",
            e.getMessage(), BAD_REQUEST), self());
        return;
      }
      if (!adviser.hasFund(closeFund.getFundName()))
        sender().tell(failed(closeFund.getUsername(),
            closeFund.getFundName() + " Fund could not be closed as it does not exist for " + adviser.getAdviserUsername(),
            BAD_REQUEST), self());
      AdviserFundClosed adviserFundClosed = new AdviserFundClosed(adviser.getAdviserUsername(),
          adviser.getAdviserName(), closeFund.getFundName());
      persist(adviserFundClosed, (AdviserFundClosed evt) -> {
        adviser.update(evt);
        forwardToChildren(context(), closeFund, closeFund.getFundName());
      });
    } else if (command instanceof FundCommand) {
      FundCommand cmd = (FundCommand) command;
      if (adviser.hasFund(cmd.getFundName())) {
        Object result = askChild(context(), cmd, cmd.getFundName());
        sender().tell(result, sender());
      } else {
        sender().tell(failed(cmd.getFundName(), "Fund does not exist", NOT_FOUND), sender());
      }
    }
  }

  private void sendSNSNotification(String fundName, String title, String message) {
    Map<String, String> attributes = setAttributes(adviser.getAdviserUsername(), fundName, title,
        NotificationTypes.Adviser, message);
    notificationFacade.sendNotification(adviser.getDeviceToken(), message, attributes);
  }
  
  private Map<String, String> setAttributes(String adviserUsername, String fundName, String title,
      NotificationTypes type, String message) {
    Map<String, String> additionalAttributes = new HashMap<String, String>();
    additionalAttributes.put("fund_name", fundName);
    additionalAttributes.put("author", adviserUsername);
    additionalAttributes.put("title", title);
    additionalAttributes.put("type", type.name());
    
    Notification notificationDomain = createNotificationDomain(adviserUsername, fundName, title, message, type);
    String recordString = (new Gson().toJson(notificationDomain).toString());
    additionalAttributes.put("recordString", recordString);
    additionalAttributes.put("id", adviserUsername);
    additionalAttributes.put("userType", "Adviser");
    additionalAttributes.put("investingMode", "NONE");
    additionalAttributes.put("tablename", "notification");
    
    return additionalAttributes;
  }
  
  private Notification createNotificationDomain(String adviserUsername, String fundName, String title,
      String message, NotificationTypes type) {
    Map<String, String> notificationMap = Maps.newHashMap();
    notificationMap.put("author", adviserUsername);
    notificationMap.put("fund_name", fundName);
    notificationMap.put("title", title);
    notificationMap.put("type", type.name());
    notificationMap.put("investingMode", "NONE");
    notificationMap.put("message", message);
    
    NotificationCompositeKey notificationCompositeKey = new NotificationCompositeKey();
    notificationCompositeKey.setUsername(adviser.getAdviserUsername());
    notificationCompositeKey.setTimestamp(DateUtils.currentTimeInMillis());
    Notification notification = new Notification(notificationCompositeKey, notificationMap);
    return notification;
  }

  private void registerAdviserFromUserPool(RegisterAdviserFromUserPool command) {
    try {
      command.validate();
    } catch (BadRequestException e) {
      log.error("Exception " + e);
      sender().tell(failed(command.getAdviserUsername(), e.getMessage(), BAD_REQUEST), self());
    }
    try {
      AdviserRegisteredfromUserPool adviserRegistered = new AdviserRegisteredfromUserPool(command.getAdviserUsername(),
          command.getName(), command.getEmail(), DateUtils.currentTimeInMillis(), Cognito, command.getPhoneNumber(),
          command.getCompany(), command.getPublicARN());
      persist(adviserRegistered, (AdviserRegistered evt) -> {
        adviser.update(evt);
        sender().tell(new Done(evt.getAdviserUsername(), "Adviser Registered"), self());
      });
    } catch (Exception e) {
      log.error("Exception " + e);
      sender().tell(failed(command.getAdviserUsername(), "failed to register adviser", EXPECTATION_FAILED), self());
    }
  }

  private void forwardCommandToAllChildren(Object obj) {
    Set<String> adviserUserNames = adviser.getFunds();
    forwardToChildren(context(), obj, adviserUserNames.toArray(new String[0]));
  }

  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(AdviserEvent.class, e -> {
      adviser.update(e);
    }).build();
  }

  @Override
  public String persistenceId() {
    return persistenceId;
  }

  @Override
  public String getName() {
    return persistenceId();
  }

  @Autowired
  public void setNotificationFacade(NotificationFacade notificationFacade) {
    this.notificationFacade = notificationFacade;
  }

  @Override
  public String getChildBeanName() {
    return "FundActor";
  }
}
