package com.wt.domain.write.events;

import lombok.Getter;

@Getter
public class AdviserPasswordChanged extends AdviserEvent {
  private static final long serialVersionUID = 1L;
  private byte[] password;
  private byte[] salt;

  public AdviserPasswordChanged(String adviserUsername, byte[] password, byte[] salt) {
    super(adviserUsername);
    this.password = password;
    this.salt = salt;
  }

}
