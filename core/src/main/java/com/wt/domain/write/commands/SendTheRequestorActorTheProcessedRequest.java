package com.wt.domain.write.commands;

import java.util.List;

import com.wt.domain.FundConstituent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class SendTheRequestorActorTheProcessedRequest {
  @Getter private List<FundConstituent> fundConstituents;
}
