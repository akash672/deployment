package com.wt.domain.write.events;

import java.util.List;

import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.HistoricalPriceEventsList;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PriceEventsList extends FundEvent {

  private String wdId;
  private List<PriceWithPaf> priceList;

  public PriceEventsList(HistoricalPriceEventsList cmd, String adviserUsername, String fundName) {
    super(adviserUsername, fundName);
    this.priceList = cmd.getListOfPriceEvent();
    this.wdId = cmd.getKey();
  }

  private static final long serialVersionUID = 1L;

}
