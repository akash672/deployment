
package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class UpdateAdviseDetailsInCorpActionsMap extends FundEvent{

  
  private static final long serialVersionUID = 1L;
  private String oldTicker;
  private AdviseDetails adviseDetails;
  public UpdateAdviseDetailsInCorpActionsMap(String adviserUsername, String fundName, 
                                      String oldTicker,AdviseDetails adviseDetails) {
    super(adviserUsername, fundName);
    this.oldTicker = oldTicker;
    this.adviseDetails = adviseDetails;
  }

}
