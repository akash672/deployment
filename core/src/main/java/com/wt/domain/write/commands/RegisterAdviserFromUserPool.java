package com.wt.domain.write.commands;

import static com.wt.aws.cognito.configuration.CredentialsProviderType.Cognito;
import static com.wt.utils.CommonUtils.isValidEmail;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterAdviserFromUserPool extends UnAuthenticatedAdviserCommand implements Serializable {

  private static final long serialVersionUID = 1L;
  private String name;
  private String email;
  private String publicARN;
  private String phoneNumber;
  private String company;

  @JsonCreator
  public RegisterAdviserFromUserPool(@NotNull @JsonProperty("adviserUsername") String adviserUsername,
      @NotNull @JsonProperty("name") String name, @JsonProperty("email") String email,
      @JsonProperty("publicARN") String publicARN, @JsonProperty("company") String company,
      @JsonProperty("phoneNumber") String phoneNumber) {
    super(adviserUsername, Cognito);
    this.name = name;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.publicARN = publicARN;
    this.company = company;
  }

  public void validate() throws BadRequestException {
    if (getAdviserUsername() == null || getAdviserUsername().isEmpty())
      throw new BadRequestException("Username is required");
    if (getName() == null || getName().isEmpty())
      throw new BadRequestException("Investor Name is required");
    if (getPublicARN() == null || publicARN.isEmpty())
      throw new BadRequestException("Advisory License number is required");
    if (company == null || company.isEmpty())
      throw new BadRequestException("Company name is required");
    if ((phoneNumber == null || phoneNumber.isEmpty()) && (email == null || email.isEmpty()))
      throw new BadRequestException("Email and Phone number both cannot be empty, either one is required");
    if (email != null && !email.isEmpty()) {
      if (!isValidEmail(email))
        throw new BadRequestException("Email is invalid");
    }
  }
}
