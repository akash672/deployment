package com.wt.domain.write.events;

import com.wt.domain.RiskLevel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class FundFloated extends FundEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private final long startDate;
  @Getter
  private final long endDate;
  @Getter
  private String description;
  @Getter
  @Setter
  private String theme;
  @Getter
  private String adviserName;
  @Getter
  private double minSIP;
  @Getter
  private double minLumpSum;
  @Getter
  private RiskLevel riskLevel;

  public FundFloated(String username, String adviserName, String fundName, long startDate, long endDate,
      String description, String theme, double minSIP, double minLumpSum, RiskLevel riskLevel) {
    super(username, fundName);
    this.adviserName = adviserName;
    this.startDate = startDate;
    this.endDate = endDate;
    this.description = description;
    this.theme = theme;
    this.minSIP = minSIP;
    this.minLumpSum = minLumpSum;
    this.riskLevel = riskLevel;
  }
}
