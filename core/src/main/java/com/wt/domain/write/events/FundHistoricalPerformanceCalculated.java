package com.wt.domain.write.events;

import java.io.Serializable;

import akka.http.javadsl.model.StatusCode;
import akka.http.javadsl.model.StatusCodes;

public class FundHistoricalPerformanceCalculated extends Done implements Serializable{

  private static final long serialVersionUID = 1L;

  public FundHistoricalPerformanceCalculated(String id, String message) {
    super(id, message);
  }

  @Override
  public StatusCode getStatusCode() {
    return StatusCodes.OK;
  }

}
