package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@ToString
@AllArgsConstructor
public abstract class InternalAdviserCommand  {
  
  @NonNull
  @Getter
  protected String adviserUsername;
  
}
