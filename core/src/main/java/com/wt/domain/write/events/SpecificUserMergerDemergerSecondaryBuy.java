package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class SpecificUserMergerDemergerSecondaryBuy extends MergerDemergerEvent{

	private static final long serialVersionUID = 1L;
	private String oldToken;
	private String newToken;
	private double sharesRatio;
	private double cashAllocationRatio;
	private String symbol;
	private String userName;
	private String fundName;
	private String adviserUsername;
	private String adviseId;
	
	
	public SpecificUserMergerDemergerSecondaryBuy(@JsonProperty("oldToken")String oldToken, @JsonProperty("newToken")String newToken, 
			@JsonProperty("sharesRatio")double sharesRatio,
			@JsonProperty("cashAllocationRatio")double cashAllocationRatio, @JsonProperty("symbol")String symbol, @JsonProperty("userName")String userName, @JsonProperty("fundName")String fundName, @JsonProperty("adviserUsername")String adviserUsername,
			@JsonProperty("adviseId")String adviseId) {
		super();
		this.oldToken = oldToken;
		this.newToken = newToken;
		this.sharesRatio = sharesRatio;
		this.cashAllocationRatio = cashAllocationRatio;
		this.symbol = symbol;
		this.userName = userName;
		this.fundName = fundName;
		this.adviserUsername = adviserUsername;
		this.adviseId = adviseId;
	}
	
	
	

}
