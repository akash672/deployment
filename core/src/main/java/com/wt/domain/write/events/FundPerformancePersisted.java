package com.wt.domain.write.events;

import lombok.Getter;
import lombok.ToString;

@ToString
public class FundPerformancePersisted extends FundEvent {

  private static final long serialVersionUID = 1L;
  @Getter 
  private final long date;
  
  public FundPerformancePersisted(String adviserUsername, String fundName, long timestamp) {
    super(adviserUsername, fundName);
    this.date = timestamp;
  }
  
  public static class FundPerformancePersistedBuilder {
    private String adviserUsername;
    private String fundName;
    private long timestamp = 0;

    public FundPerformancePersistedBuilder(String adviserUsername, String fundName, long date) {
      super();
      this.adviserUsername = adviserUsername;
      this.fundName = fundName;
      this.timestamp = date;
    }
    
    public FundPerformancePersisted build() {
      return new FundPerformancePersisted(adviserUsername,fundName,timestamp);
    }
  }
}
