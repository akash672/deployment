package com.wt.domain.write.events;

public class StartHistoricalFundPerformance extends FundEvent {

  public StartHistoricalFundPerformance(String adviserUsername, String fundName) {
    super(adviserUsername, fundName);
  }

  private static final long serialVersionUID = 1L;

}
