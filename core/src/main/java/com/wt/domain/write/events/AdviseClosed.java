package com.wt.domain.write.events;

import lombok.Getter;

public class AdviseClosed extends AdviseEvent {
    private static final long serialVersionUID = 1L;
  @Getter private double allocation;
  @Getter private long actualExitDate;
  @Getter private double actualExitPrice;

  public AdviseClosed(String adviserName, String fundName, String adviseId, double allocation, long actualExitDate,
      double actualExitPrice) {
        super(adviserName, fundName, adviseId);
        this.actualExitDate = actualExitDate;
        this.actualExitPrice = actualExitPrice;
        this.allocation = allocation;
    }

    @Override
    public String toString() {
        return "AdviseClosed [allocation=" + allocation + ", actualExitDate=" + actualExitDate + ", actualExitPrice="
            + actualExitPrice + ", getAdviseId()=" + getAdviseId() + "]";
    }
  
}