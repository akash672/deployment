package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class AdviseDetails implements Serializable {
  
  private static final long serialVersionUID = 1L;
  private String token;
  @Setter private double oldAdviseAllocation;
  @Setter private double recoEntryPrice;
  @Setter private double paf;
  @Setter private double primaryAdviseAvgEntryPrice; 
  @Setter private double primaryAdviseAllocation;
  @Setter private double secondaryAdviseAllocationRatio;
  
  public double allocationPendingPostPrimaryBuy(){
    return oldAdviseAllocation - primaryAdviseAllocation;
  }
  
  public double averagePriceDifferential(){
    return recoEntryPrice - primaryAdviseAvgEntryPrice;
  }

}
