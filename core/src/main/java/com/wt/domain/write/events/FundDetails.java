package com.wt.domain.write.events;

import com.wt.domain.RiskLevel;

import lombok.Getter;
import lombok.ToString;

@ToString
public class FundDetails extends FundEvent {

  private static final long serialVersionUID = 1L;

  @Getter
  private RiskLevel riskLevel;

  public FundDetails(String adviserName, String fundName, RiskLevel riskLevel) {
    super(adviserName, fundName);
    this.riskLevel = riskLevel;
  }
  
}