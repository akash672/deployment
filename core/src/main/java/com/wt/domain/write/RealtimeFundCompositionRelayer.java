package com.wt.domain.write;

import static com.wt.domain.FundConstituent.withActiveAdvise;
import static com.wt.domain.FundConstituent.withJustCash;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.DoublesUtil.validDouble;
import static java.util.Collections.unmodifiableCollection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.wt.domain.FundConstituent;
import com.wt.domain.FundConstituentKey;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.GetCurrentFundComposition;
import com.wt.domain.write.commands.GetFundRebalancingView;
import com.wt.domain.write.commands.GetPriceBasketFromTracker;
import com.wt.domain.write.commands.PriceSnapshotsFromPriceSource;
import com.wt.domain.write.commands.SendTheRequestorActorTheProcessedRequest;
import com.wt.domain.write.events.FundConstituentListResponse;
import com.wt.domain.write.events.ProcessingComplete;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;
import lombok.extern.log4j.Log4j;

@Log4j
public class RealtimeFundCompositionRelayer extends UntypedAbstractActor {
  private ActorRef destinationActor;
  private Collection<FundConstituent> allFundConstituents;
  private Set<FundConstituent> updatedUniqueFundConstituents;
  private List<FundConstituent> fundConstituentsOfAdvisesAboveFreezePrice;
  private double fundCash;
  private String fundName;
  private Map<String, PriceWithPaf> adviseIdToRecommendedEntryPrice;
  private WDActorSelections wdActorSelections;
  private boolean withFreezedAdvises;
  Set<String> uniqueWdIdsInFund;

  public RealtimeFundCompositionRelayer(Collection<FundConstituent> allFundConstituents, double fundCash,
      Map<String, PriceWithPaf> adviseIdToRecommendedEntryPrice, WDActorSelections wdActorSelections) {
    super();
    this.allFundConstituents = allFundConstituents;
    this.fundCash = fundCash;
    this.adviseIdToRecommendedEntryPrice = adviseIdToRecommendedEntryPrice;
    this.wdActorSelections = wdActorSelections;
    this.updatedUniqueFundConstituents = new HashSet<FundConstituent>();
    this.fundConstituentsOfAdvisesAboveFreezePrice = new ArrayList<FundConstituent>();
    this.uniqueWdIdsInFund = new HashSet<String>();
  }

  @Override
  public void onReceive(Object msg) throws Throwable {
    if (msg instanceof GetCurrentFundComposition) {
      GetCurrentFundComposition getFundComposition = (GetCurrentFundComposition) msg;
      this.destinationActor = getFundComposition.getSenderActor();
      this.fundName = getFundComposition.getFundName() ;
      this.withFreezedAdvises = true;
      if (allFundConstituents.size() != 0) {
        allFundConstituents.stream().forEach(fundConstituent -> uniqueWdIdsInFund.add(fundConstituent.getTicker()));
        wdActorSelections.activeInstrumentTracker().tell(new GetPriceBasketFromTracker(uniqueWdIdsInFund), self());
      } else {
        destinationActor.tell(new FundConstituentListResponse("", fundName, allFundConstituents), self());
        self().tell(new ProcessingComplete(), self());
      }
    } else if (msg instanceof PriceSnapshotsFromPriceSource) {

      HashMap<String, PriceWithPaf> tokenToPriceWithPafMap = ((PriceSnapshotsFromPriceSource) msg).getTickerToPriceWithPafMap();
      allFundConstituents.stream().forEach(fundConstituent -> {
        try {
          FundConstituent updatedFundConstituent = updatedFundConstituents(
              tokenToPriceWithPafMap.get(fundConstituent.getTicker()), fundConstituent);
          if (updatedFundConstituent.getAdviseId().equals(""))
            fundConstituentsOfAdvisesAboveFreezePrice.add(updatedFundConstituent);
          else
            updatedUniqueFundConstituents.add(updatedFundConstituent);
        } catch (Exception e) {
          log.error("Unable to update fund constituent " + e);
        }
      });
      if (updatedUniqueFundConstituents.size() + fundConstituentsOfAdvisesAboveFreezePrice.size() == allFundConstituents
          .size())
        returnNormalizedFundConstituents(updatedUniqueFundConstituents, fundConstituentsOfAdvisesAboveFreezePrice);
    } else if (msg instanceof GetFundRebalancingView) {
      GetFundRebalancingView getFundRebalancingView = (GetFundRebalancingView) msg;
      this.destinationActor = sender();
      this.fundName = getFundRebalancingView.getFundName();
      this.withFreezedAdvises = false;
      if (allFundConstituents.size() != 0) {
        allFundConstituents.stream().forEach(fundConstituent -> uniqueWdIdsInFund.add(fundConstituent.getTicker()));
        wdActorSelections.activeInstrumentTracker().tell(new GetPriceBasketFromTracker(uniqueWdIdsInFund), self());
      } else {
        List<FundConstituent> onlyCashConstituent = updateWithCashConstituent(new ArrayList<FundConstituent>());
        destinationActor.tell(new FundConstituentListResponse("", fundName, onlyCashConstituent), self());
        self().tell(new ProcessingComplete(), self());
      }
    } else if (msg instanceof SendTheRequestorActorTheProcessedRequest) {
      SendTheRequestorActorTheProcessedRequest request = (SendTheRequestorActorTheProcessedRequest) msg;
      destinationActor.tell(new FundConstituentListResponse("", fundName, unmodifiableCollection(request.getFundConstituents())),
          self());
      self().tell(new ProcessingComplete(), self());
    } else if (msg instanceof ProcessingComplete) {
      context().stop(self());
    }
  }

  FundConstituent updatedFundConstituents(PriceWithPaf priceWithPaf, FundConstituent fundConstituent) throws Exception {
      return updateRealtimeFundComposition(priceWithPaf, fundConstituent);
  }

  private boolean checkFreezePriceCondition(PriceWithPaf currentMarketPrice, FundConstituent fundConstituent) {
    if (!withFreezedAdvises || adviseIdToRecommendedEntryPrice.isEmpty() || adviseIdToRecommendedEntryPrice == null
        || adviseIdToRecommendedEntryPrice.size() == 0)
      return true;
    
    if(adviseIdToRecommendedEntryPrice.get(fundConstituent.getAdviseId()).getPrice().getPrice() == -1.){
        return true;
    }
    
    return !(adviseIdToRecommendedEntryPrice.get(fundConstituent.getAdviseId()).getPrice()
        .getPrice() <= currentMarketPrice.getPrice().getPrice());
  }

  private FundConstituent updateRealtimeFundComposition(PriceWithPaf currentMarketPrice,
      FundConstituent fundConstituent) throws IOException {
    if (checkFreezePriceCondition(currentMarketPrice, fundConstituent)) {
      double returnAwareAllocation = fundConstituent.getAllocation()
          * (1 + fundConstituent.getEntryPrice().performanceWith(currentMarketPrice));
      return withActiveAdvise(fundConstituent.getAdviseId(), fundConstituent.getFundConstituentKey().getFund(),
          fundConstituent.getFundConstituentKey().getAdviser(), fundConstituent.getTicker(), fundConstituent.getSymbol(),
          round(validDouble(returnAwareAllocation)), currentMarketPrice);
    } else {
      return FundConstituent.withBlankAdviseId(fundConstituent.getFundConstituentKey().getFund(),
          fundConstituent.getFundConstituentKey().getAdviser(), fundConstituent.getAllocation(),
          fundConstituent.getTicker(), fundConstituent.getSymbol(), currentMarketPrice);
    }
  }

  private void returnNormalizedFundConstituents(Set<FundConstituent> realtimeFundConstituentsWithBlankAdvises, 
                                                  List<FundConstituent> fundConstituentsOfAdvisesAboveFreezePrice) {
    double totalReturnAdjustedFundAllocation = realtimeFundConstituentsWithBlankAdvises.stream()
        .mapToDouble(fundConstituent -> fundConstituent.getAllocation()).sum() +
        fundConstituentsOfAdvisesAboveFreezePrice.stream()
        .mapToDouble(fundConstituent -> fundConstituent.getAllocation()).sum() +
        fundCash;
    for (FundConstituent fundConstituent : realtimeFundConstituentsWithBlankAdvises) {
      if (!isZero(totalReturnAdjustedFundAllocation)) {
        double returnAdjustedAllocation = (fundConstituent.getAllocation() / round(totalReturnAdjustedFundAllocation))
            * 100.;
        fundConstituent.setAllocation(returnAdjustedAllocation);
      }
    }
    List<FundConstituent> realtimeFundConstituentsWithoutBlankAdvises = realtimeFundConstituentsWithBlankAdvises
        .stream().filter(fundConstituent -> fundConstituent.getAdviseId() != "").collect(Collectors.toList());
    realtimeFundConstituentsWithoutBlankAdvises = updateWithCashConstituent(realtimeFundConstituentsWithoutBlankAdvises);
    self().tell(new SendTheRequestorActorTheProcessedRequest(realtimeFundConstituentsWithoutBlankAdvises), self());
  }

  private List<FundConstituent> updateWithCashConstituent(List<FundConstituent> realtimeFundConstituentsWithoutBlankAdvises) {
    if (!withFreezedAdvises) {
      FundConstituent cashConstituent = getCashConstituent(realtimeFundConstituentsWithoutBlankAdvises); 
      realtimeFundConstituentsWithoutBlankAdvises.add(cashConstituent);
    }
    return realtimeFundConstituentsWithoutBlankAdvises;
  }

  private FundConstituent getCashConstituent(List<FundConstituent> realtimeFundConstituentsWithoutBlankAdvises) {
    if (realtimeFundConstituentsWithoutBlankAdvises.size() == 0) {
      return withJustCash("Cash", fundName, "", "Cash", "Cash", 100.);
    } else {
      FundConstituentKey fundConstituentKey = realtimeFundConstituentsWithoutBlankAdvises.stream().findAny().get()
          .getFundConstituentKey();
      String fundName = fundConstituentKey.getFund();
      double cashRemaining = (100. - realtimeFundConstituentsWithoutBlankAdvises.stream()
          .mapToDouble(fundConstituent -> fundConstituent.getAllocation()).sum());
      return withJustCash("Cash", fundName, "", "Cash", "Cash", cashRemaining);
    }
  }

}
