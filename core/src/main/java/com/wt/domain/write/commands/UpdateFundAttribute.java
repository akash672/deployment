package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.RiskLevel;

import lombok.Getter;

public class UpdateFundAttribute extends FundCommand {

  private static final long serialVersionUID = 1L;
  private @Getter long startDate;
  private @Getter long endDate;
  private @Getter String description;
  private @Getter String theme;
  private @Getter double minSIP;
  private @Getter double minLumpSum;
  private @Getter String adviserUsername;
  private @Getter RiskLevel riskLevel;

  @JsonCreator
  public UpdateFundAttribute(@JsonProperty("username") String id, @JsonProperty("fundName") String fundName,
      @JsonProperty("startDate") long startDate, @JsonProperty("endDate") long endDate,
      @JsonProperty("description") String description, @JsonProperty("theme") String theme,
      @JsonProperty("minSIP") double minSIP, @JsonProperty("minLumpSum") double minLumSum,
      @JsonProperty("riskLevel") String riskLevel) {
    super(id, fundName);
    this.startDate = startDate;
    this.endDate = endDate;
    this.description = description;
    this.theme = theme;
    this.minSIP = minSIP;
    this.minLumpSum = minLumSum;
    this.riskLevel = RiskLevel.valueOf(riskLevel.toUpperCase());
  }

  public void validateCommand() {
    if (isNegativeMinimumLumpSum() || isNegativeMinimumSIP()) {
      throw new IllegalArgumentException("Minimum LumpSum or Minimum SIP cannot be less than zero");
    } else if (isMinimumLumpSumLessThanMinimumSIP()) {
      throw new IllegalArgumentException("Minimum LumpSum or Minimum SIP cannot be less than minimum SIP");
    }
  }

  private boolean isMinimumLumpSumLessThanMinimumSIP() {
    if (minSIP != 0)
      return minLumpSum < minSIP;
    else
      return false;
  }

  private boolean isNegativeMinimumLumpSum() {
    return minLumpSum < 0;
  }

  private boolean isNegativeMinimumSIP() {
    return minSIP < 0;
  }
}
