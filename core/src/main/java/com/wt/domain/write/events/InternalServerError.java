package com.wt.domain.write.events;

import static akka.http.javadsl.model.StatusCodes.INTERNAL_SERVER_ERROR;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import akka.http.javadsl.model.StatusCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class InternalServerError extends Failed {

  private static final long serialVersionUID = 1L;

  @JsonCreator
  public static InternalServerError internalServerError(String id, @JsonProperty("reason") String reason) {
    return new InternalServerError(id, reason);
  }

  protected InternalServerError(String id, String reason) {
    super(id, reason, INTERNAL_SERVER_ERROR);
  }
  
  @JsonIgnore
  @Override
  public StatusCode getStatusCode() {
    return INTERNAL_SERVER_ERROR;
  }
}
