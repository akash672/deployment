package com.wt.domain.write.events;

import com.wt.domain.RiskLevel;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FundAttributeUpdate extends FundEvent {
  private static final long serialVersionUID = 1L;
  private long startDate;
  private long endDate;
  private String description;
  private String theme;
  private double minSIP;
  private double minLumpSum;
  private RiskLevel riskLevel;

  public FundAttributeUpdate(String username, String fundName, long startDate, long endDate,
      String description, String theme, double minSIP, double minLumpSum, RiskLevel riskLevel) {
    super(username, fundName);
    this.startDate = startDate;
    this.endDate = endDate;
    this.description = description;
    this.theme = theme;
    this.minSIP = minSIP;
    this.minLumpSum = minLumpSum;
    this.riskLevel = riskLevel;
  }

}
