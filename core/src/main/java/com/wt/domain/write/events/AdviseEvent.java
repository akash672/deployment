package com.wt.domain.write.events;

import com.google.gson.annotations.Expose;

import lombok.ToString;

@ToString
public class AdviseEvent extends FundEvent {
  private static final long serialVersionUID = 1L;
  @Expose
  private final String adviseId;

  public AdviseEvent(String adviserUsername, String fundName, String adviseId) {
    super(adviserUsername, fundName);
    this.adviseId = adviseId;
  }

  public String getAdviseId() {
    return adviseId;
  }

}
