package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FundMessage implements Serializable {
  private static final long serialVersionUID = 1L;
  private String fund;
  private String message;
}
