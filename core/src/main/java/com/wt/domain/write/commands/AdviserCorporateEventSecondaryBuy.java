package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviserCorporateEventSecondaryBuy extends AdviserCorporateEvents {

  private static final long serialVersionUID = 1L;
  private String oldTicker;
  private String newTicker;
  private double cashAllocationRatio;
  private double sharesRatio;
  private String newSymbol;
  
  
  public AdviserCorporateEventSecondaryBuy(@JsonProperty("oldToken")String oldTicker, 
      @JsonProperty("newToken")String newTicker,
      @JsonProperty("cashRatio")double cashAllocationRatio, @JsonProperty("sharesRatio")double sharesRatio,
      @JsonProperty("newSymbol")String newSymbol) {
    super();
    this.oldTicker = oldTicker;
    this.newTicker = newTicker;
    this.cashAllocationRatio = cashAllocationRatio;
    this.sharesRatio = sharesRatio;
    this.newSymbol = newSymbol;
  }
  
  
  
}
