package com.wt.domain.write.events;

import lombok.Getter;

@Getter
public class FundEvent extends AdviserEvent {
  private static final long serialVersionUID = 1L;

  private final String fundName;

  public FundEvent(String adviserUsername, String fundName) {
    super(adviserUsername);
    this.fundName = fundName;
  }

}
