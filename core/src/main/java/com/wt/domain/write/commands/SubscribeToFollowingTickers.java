package com.wt.domain.write.commands;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SubscribeToFollowingTickers implements Serializable {

  private static final long serialVersionUID = 1L;
  private Set<String> tickers;
}
