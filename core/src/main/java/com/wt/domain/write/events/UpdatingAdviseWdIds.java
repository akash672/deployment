package com.wt.domain.write.events;

import java.util.Map;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdatingAdviseWdIds extends FundEvent{
  
  private static final long serialVersionUID = 1L;
  private Map<String, String> adivseIdToUpdatedWdIdMap;
  

  public UpdatingAdviseWdIds(String adviserUsername, String fundName, Map<String, String> adivseIdToUpdatedWdIdMap) {
    super(adviserUsername, fundName);
    this.adivseIdToUpdatedWdIdMap = adivseIdToUpdatedWdIdMap;
  }

  


}