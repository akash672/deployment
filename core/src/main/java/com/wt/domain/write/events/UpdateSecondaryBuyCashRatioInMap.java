package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode(callSuper = true)
public class UpdateSecondaryBuyCashRatioInMap extends FundEvent{

  
  private static final long serialVersionUID = 1L;
  private String oldTicker;
  private double cashRatio;
  public UpdateSecondaryBuyCashRatioInMap(String adviserUsername, String fundName, 
                                      String oldTicker, double cashRatio) {
    super(adviserUsername, fundName);
    this.oldTicker = oldTicker;
    this.cashRatio = cashRatio;
  }

}
