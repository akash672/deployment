package com.wt.domain.write.commands;

import static com.wt.utils.DateUtils.isExecutionWindowOpen;

import java.io.IOException;
import java.text.ParseException;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@ToString(callSuper=true)
public class IssueAdvice extends AdviseCommand {
  
  private static final long serialVersionUID = 1L;
  @NonNull
  @Getter
  private final String token;
  @Getter
  private final double allocation;
  @Getter
  private final double entryPrice;
  @Getter
  private final long entryTime;
  @Getter
  private final double exitPrice;
  @Getter
  private final long exitTime;
  @Getter
  @JsonIgnore
  private final long issueTime;
  @Getter
  @Setter
  private String adviserUsername;

  public IssueAdvice(String username, String fundName, String adviseId, String token, double allocation,
      double entryPrice, long entryTime, double exitPrice, long exitTime, long issueTime) {
    super(username, fundName, adviseId);
    this.token = token;
    this.allocation = DoublesUtil.round(allocation);
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
  }

  public IssueAdvice(String username, String fundName, String token, double allocation, double entryPrice,
      long entryTime, double exitPrice, long exitTime) {
    this(username, fundName, UUID.randomUUID().toString(), token, allocation, entryPrice, entryTime, exitPrice,
        exitTime, (token.endsWith("-MF") ? DateUtils.lastDayEOD(DateUtils.currentTimeInMillis()) : DateUtils.currentTimeInMillis()));
  }

  @JsonCreator
  public IssueAdvice(@JsonProperty("username") String username, @JsonProperty("fundName") String fundName,
      @JsonProperty("token") String token, @JsonProperty("allocation") double allocation,
      @JsonProperty("entryPrice") double entryPrice, @JsonProperty("exitPrice") double exitPrice,
      @JsonProperty("exitTime") long exitTime) throws ParseException, IOException {
    this(username, fundName, UUID.randomUUID().toString(), token, allocation, entryPrice, DateUtils.currentTimeInMillis(),
        exitPrice, exitTime, DateUtils.currentTimeInMillis());
    
  }

  @Override
  public void validate(Object adviseSpecificParameter) throws Exception {
    handleAdviseDuringMarketClose();
    validateAvailableFundCash((double) adviseSpecificParameter);
    validatePrices();
    validateAllocation();
    validateEntryTime();
  }
  
  private void handleAdviseDuringMarketClose() throws ParseException, IOException {
    if (!isExecutionWindowOpen()) {
      throw new RuntimeException("Market is closed. Please issue new advise during market hours.");
    }
  }
  
  private void validateAvailableFundCash(double cash) {
    if (cash < DoublesUtil.round(this.getAllocation())) {
      throw new IllegalArgumentException(
          "Fund current available cash " + cash + " is less than Issued Advice allocation");
    }
  }

  public void validateEntryTime() throws IllegalArgumentException {
    if (entryTime > DateUtils.currentTimeInMillis()) {
      throw new IllegalArgumentException("Entry time for the advice is greater than the current time");
    }
  }

  public void validatePrices() throws IllegalArgumentException {
    if (entryPrice < 0.) {
      throw new IllegalArgumentException("Entry Price cannot not be less than zero.");
    }
    if (exitPrice <= 0.) {
      throw new IllegalArgumentException("Exit Price should be non zero only");
    }
  }

  public void validateAllocation() throws IllegalArgumentException {
    if (allocation <= 0.) {
      throw new IllegalArgumentException("Issued Advice Allocation in the portfolio should only be non-zero and positive.");
    }
  }

  public String getAdviseId() {
    return super.getAdviseId();
  }

}
