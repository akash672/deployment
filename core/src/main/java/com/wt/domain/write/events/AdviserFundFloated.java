package com.wt.domain.write.events;

import lombok.Getter;

public class AdviserFundFloated extends FundEvent {
	private static final long serialVersionUID = 1L;

	@Getter
	private String adviserName;
	
	public AdviserFundFloated(String adviserUsername,String adviserName, String fundName) {
		super(adviserUsername, fundName);
		this.adviserName = adviserName;
	}
}
