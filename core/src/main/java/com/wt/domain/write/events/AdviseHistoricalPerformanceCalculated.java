package com.wt.domain.write.events;

import java.util.List;

import akka.http.javadsl.model.StatusCode;
import akka.http.javadsl.model.StatusCodes;
import lombok.Getter;
import lombok.ToString;

@ToString
public class AdviseHistoricalPerformanceCalculated extends Done {
  private static final long serialVersionUID = 1L;
  @Getter
  private List<AdvisePerformanceCalculated> allPerformance;

  public AdviseHistoricalPerformanceCalculated(String id, String message,
      List<AdvisePerformanceCalculated> allPerformance) {
    super(id, message);
    this.allPerformance = allPerformance;
  }

  @Override
  public StatusCode getStatusCode() {
    return StatusCodes.OK;
  }

}
