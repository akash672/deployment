package com.wt.domain.write.events;

import lombok.Getter;

@Getter
public class AdviserPasswordResetted extends AdviserEvent {

  private byte[] newPassword;
  private byte[] salt;
  private String otp;

  public AdviserPasswordResetted(String adviserUsername, byte[] newPassword, byte[] salt, String otp) {
    super(adviserUsername);
    this.newPassword = newPassword;
    this.salt = salt;
    this.otp = otp;
  }

  private static final long serialVersionUID = 1L;

}
