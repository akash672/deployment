package com.wt.domain.write;

import static com.wt.domain.write.events.Failed.failed;
import static com.wt.domain.write.events.FundAdviceIssued.withoutExitParameters;
import static com.wt.domain.write.events.Pafs.withCumulativePaf;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.domain.Advise;
import com.wt.domain.Fund;
import com.wt.domain.FundConstituent;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.AddFundToActiveWdId;
import com.wt.domain.write.commands.AdviserCorpActionPrimaryBuy;
import com.wt.domain.write.commands.AdviserCorpActionSecondaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSell;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.CloseFund;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.GetCurrentFundComposition;
import com.wt.domain.write.commands.GetFundAdvises;
import com.wt.domain.write.commands.GetFundDetails;
import com.wt.domain.write.commands.GetFundRebalancingView;
import com.wt.domain.write.commands.GetPricePaf;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.UpdateAdviseRecommendedEntryPrice;
import com.wt.domain.write.commands.UpdateFundAttribute;
import com.wt.domain.write.events.AdviseDetails;
import com.wt.domain.write.events.CloseCorporateEventAdvise;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundAdviseListResponse;
import com.wt.domain.write.events.FundAttributeUpdate;
import com.wt.domain.write.events.FundClosed;
import com.wt.domain.write.events.FundDetails;
import com.wt.domain.write.events.FundEvent;
import com.wt.domain.write.events.FundFloated;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.MergerDemergerFundAdviseClosed;
import com.wt.domain.write.events.MergerDemergerPrimaryBuyIssued;
import com.wt.domain.write.events.MergerDemergerSecondaryBuyIssued;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.RecommendedEntryPriceUpdated;
import com.wt.domain.write.events.UpdateAdviseDetailsInCorpActionsMap;
import com.wt.domain.write.events.UpdateSecondaryBuyCashRatioInMap;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;
import com.wt.utils.akka.ActorResultWaiter;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.InvalidActorNameException;
import akka.actor.Props;
import akka.http.javadsl.model.StatusCodes;
import akka.persistence.AbstractPersistentActor;
import lombok.extern.log4j.Log4j;

@Scope("prototype")
@Service("FundActor")
@Log4j
public class FundActor extends AbstractPersistentActor {

  private Fund fund = new Fund();
  private String persistenceId;
  private WDActorSelections wdActorSelections;

  public FundActor(String persistenceId) {
    super();
    this.persistenceId = persistenceId;
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(FloatFund.class, cmd -> {
      floatFund(cmd);
    }).match(UpdateFundAttribute.class, cmd -> {
      updateFundAttribute(cmd);
    }).match(GetFundRebalancingView.class, cmd -> {
       getCurrentFundComposition(cmd);
    }).match(UpdateAdviseRecommendedEntryPrice.class, c -> {
      updateRecommendedEntryPrice(c);
    }).match(IssueAdvice.class, issueAdvise -> {
      handleIssueAdvise(issueAdvise);
    }).match(CloseAdvise.class, closeAdvise -> {
      handleCloseAdvise(closeAdvise);
    }).match(GetFundAdvises.class, cmd -> {
      sender().tell(new FundAdviseListResponse(cmd.getFundName(), fund.allAdviseObjects()), self());
    }).match(GetCurrentFundComposition.class, cmd -> {
      getFundRealtimeComposition(cmd, getAdviseIdToRecommmendedEntryPriceMap());
    }).match(GetFundDetails.class, command -> {
      replyFundDetails(command);
    }).match(AdviserCorporateEventSell.class, c -> {
      handleCorporateEventsSell(c);
    }).match(AdviserCorpActionPrimaryBuy.class, c -> {
      handleCorporateEventsPrimaryBuy(c);
    }).match(AdviserCorpActionSecondaryBuy.class, c -> {
      handleCorporateEventsSecondaryBuy(c);
    }).match(CloseFund.class, cmd -> {
      closeFund(cmd);
    }).build();
  }


  private void getCurrentFundComposition(GetFundRebalancingView getFundRebalancingView) {
    ActorRef internalRequestProcessor = null;
    try {
      internalRequestProcessor = context().actorOf(
          Props.create(RealtimeFundCompositionRelayer.class,
              fund.allFundComposition(),
              fund.getCash(),
              new HashMap<String, PriceWithPaf>(),
              wdActorSelections),
          "RealtimeFundCompositionRelayer-" + getFundRebalancingView.getUsername());
    } catch (InvalidActorNameException e) {
      internalRequestProcessor = context().actorOf(
          Props.create(RealtimeFundCompositionRelayer.class, fund.allFundComposition(), fund.getCash(),
              new HashMap<String, PriceWithPaf>(), wdActorSelections),
          "RealtimeFundCompositionRelayer-" + UUID.randomUUID().toString());
    }
    internalRequestProcessor.tell(getFundRebalancingView, sender());
  }

  private void handleCorporateEventsPrimaryBuy(AdviserCorpActionPrimaryBuy command) throws Exception {
    if (fund.getCorpActionTokenToAdviseDetails().containsKey(command.getTicker())) {
      AdviseDetails adviseDetails = fund.getCorpActionTokenToAdviseDetails().get(command.getTicker());
      double paf = round(command.getOldPrice() / command.getNewPrice());
      adviseDetails.setPaf(paf);
      double newAdviseRecoEntryPrice = round(adviseDetails.getRecoEntryPrice() / paf);
      adviseDetails.setPrimaryAdviseAvgEntryPrice(newAdviseRecoEntryPrice);
      double adviseAllocation = round(adviseDetails.getOldAdviseAllocation() / (paf));
      adviseDetails.setPrimaryAdviseAllocation(adviseAllocation);
      long currentTime = DateUtils.currentTimeInMillis();
      String newAdviseId = UUID.randomUUID().toString();
      IssueAdvice issueAdvice = new IssueAdvice(command.getAdviserUsername(), fund.getFundName(), newAdviseId,
          command.getTicker(), adviseAllocation, newAdviseRecoEntryPrice, currentTime, newAdviseRecoEntryPrice + 200.0,
          currentTime, currentTime);

      UpdateAdviseDetailsInCorpActionsMap updateMap = new UpdateAdviseDetailsInCorpActionsMap(fund.getAdviserUsername(),
          fund.getFundName(), command.getTicker(), adviseDetails);
      persist(updateMap, evt -> {
        fund.update(updateMap);

      });

      persistIssuedAdvise(issueAdvice,
          new PriceWithPaf(new InstrumentPrice(newAdviseRecoEntryPrice, currentTime, command.getTicker()),
              getPricePaf(command.getTicker(), currentTime)),
          true);
      
      MergerDemergerPrimaryBuyIssued mergerDemergerPrimaryBuyIssued = new MergerDemergerPrimaryBuyIssued(command.getAdviserUsername(), fund.getFundName(), 
          newAdviseId, command.getTicker(), command.getOldPrice(), command.getNewPrice(), command.getSharesRatio(), command.getSymbol() );
      context().system().eventStream().publish(mergerDemergerPrimaryBuyIssued);
    }
  }
  
  private void handleCorporateEventsSecondaryBuy(AdviserCorpActionSecondaryBuy command) throws Exception {
    if(fund.getCorpActionTokenToAdviseDetails().containsKey(command.getOldTicker())){
      AdviseDetails adviseDetails = fund.getCorpActionTokenToAdviseDetails().get(command.getOldTicker());
      double allocationRemainingPostPrimaryBuy = adviseDetails.allocationPendingPostPrimaryBuy();
      double entryPriceDifferencePostPrimaryBuy = adviseDetails.averagePriceDifferential();
      double newAdviseRecoEntryPrice = calculateEntryPriceForSecondaryBuy(entryPriceDifferencePostPrimaryBuy , command.getCashAllocationRatio(), command.getSharesRatio());
      double adviseAllocation = round(allocationRemainingPostPrimaryBuy * command.getCashAllocationRatio());
      long currentTime = DateUtils.currentTimeInMillis();
      String newAdviseId = UUID.randomUUID().toString();
      IssueAdvice issueAdvice = new IssueAdvice(command.getAdviserUsername(), fund.getFundName(),newAdviseId, 
          command.getNewTicker(), adviseAllocation, newAdviseRecoEntryPrice, currentTime, newAdviseRecoEntryPrice ,currentTime,currentTime);
     
      persistIssuedAdvise(issueAdvice, 
          new PriceWithPaf(new InstrumentPrice(newAdviseRecoEntryPrice, currentTime, command.getNewTicker()), 
              getPricePaf(command.getNewTicker(), currentTime)), true);
      
      UpdateSecondaryBuyCashRatioInMap updateMap = new UpdateSecondaryBuyCashRatioInMap(fund.getAdviserUsername(), fund.getFundName(), command.getOldTicker(),command.getCashAllocationRatio());
      persist(updateMap, evt -> {
        fund.update(updateMap);
      });
      
      MergerDemergerSecondaryBuyIssued mergerDemergerSecondaryBuyIssued = new MergerDemergerSecondaryBuyIssued(command.getAdviserUsername(), fund.getFundName(), 
          newAdviseId, command.getOldTicker(), command.getNewTicker(), command.getSharesRatio(), command.getCashAllocationRatio(), command.getSymbol());
      context().system().eventStream().publish(mergerDemergerSecondaryBuyIssued);
    }
  }

  private void handleCorporateEventsSell(AdviserCorporateEventSell command) {
    List<FundConstituent> fundConstituents = new ArrayList<FundConstituent>(fund.getTickersWithComposition().values());
    for(FundConstituent fundConstituent : fundConstituents){
      if(fundConstituent.getTicker().equals(command.getToken())){
        CloseCorporateEventAdvise event = new CloseCorporateEventAdvise(fund.getAdviserName(), fund.getFundName(),
                                                                        command.getToken(), command.getPrice(),
                                                                        fundConstituent.getFundConstituentKey().getAdviseId());
        
      persist(event, evt -> {
      fund.update(event);
      updateCloseCorpEventAdvise(event);
      });
      }
    }
  }

  private void updateCloseCorpEventAdvise(CloseCorporateEventAdvise event) throws Exception {
    Advise advise = fund.getAdvisesWithIds().get(event.getAdviseId());
    PriceWithPaf exitPrice = price(event.getToken(), event.getPrice(), DateUtils.currentTimeInMillis());
    FundAdviceClosed fundCloseAdviseEvent = new FundAdviceClosed(fund.getAdviserUsername(), fund.getFundName(),
      event.getAdviseId(),advise.getRemainingAllocation(), 100.0, DateUtils.currentTimeInMillis(),
      exitPrice, true);
    
    MergerDemergerFundAdviseClosed mergerDemergerFundAdviseClosed = new MergerDemergerFundAdviseClosed(fund.getAdviserUsername(), fund.getFundName(),
        event.getAdviseId(), event.getToken(), event.getPrice() );
    
    persist(fundCloseAdviseEvent, evt -> {
      fund.update(fundCloseAdviseEvent);
      context().system().eventStream().publish(mergerDemergerFundAdviseClosed);
  });
}

  private void replyFundDetails(GetFundDetails command) {
    if (fund.getFundName() == null || fund.getFundName().isEmpty())
      sender().tell(failed(command.getFundName(), "Fund does not exist.", StatusCodes.NOT_FOUND), self());
    else
      sender().tell(new FundDetails(command.getAdviserUsername(), command.getFundName(), fund.riskLevel), self());
  }

  private Map<String, PriceWithPaf> getAdviseIdToRecommmendedEntryPriceMap() {
    Map<String, PriceWithPaf> adviseIdwithRecoEntryPrice = new HashMap<String, PriceWithPaf>();
    fund.allAdviseObjects().stream().forEach(advise -> {
      adviseIdwithRecoEntryPrice.put(advise.getId(), advise.getRecoEntryPrice());
    });
    return adviseIdwithRecoEntryPrice;
  }

  private void handleIssueAdvise(IssueAdvice issueAdvise) throws Exception {
    try {
      issueAdvise.validate(fund.getCash());
    } catch (Exception e) {
      sender().tell(failed(issueAdvise.getAdviseId(), e.getMessage(), StatusCodes.PRECONDITION_FAILED), self());
      return;
    }
    persistIssuedAdvise(issueAdvise, entryPrice(issueAdvise), false);
  }

  private void persistIssuedAdvise(IssueAdvice issueAdvise, PriceWithPaf entryPrice, boolean isSpecialAdvise) {
    double absoluteAllocation = round((issueAdvise.getAllocation() / fund.getCash()) * 100.);
    FundAdviceIssued event = withoutExitParameters(issueAdvise.getUsername(), issueAdvise.getFundName(),
        issueAdvise.getAdviseId(), issueAdvise.getToken(), issueAdvise.getToken(), issueAdvise.getAllocation(), absoluteAllocation,
        issueAdvise.getIssueTime(), entryPrice, issueAdvise.getEntryTime(), isSpecialAdvise);
    
   
    persist(event, (FundAdviceIssued evt) -> {
      fund.update(evt);
      wdActorSelections.activeInstrumentTracker().tell(new AddFundToActiveWdId(evt.getToken(), evt.getFundName() + CompositeKeySeparator + evt.getAdviserUsername()), self());
      context().parent().tell(evt, self());
      sender().tell(new Done(evt.getAdviseId(), "Your advise has been received, you will be soon notifed when process completes"), self());
      if(!isSpecialAdvise)
        context().system().eventStream().publish(evt);
    });
  }

  private void handleCloseAdvise(CloseAdvise closeAdvise) throws Exception {
    try {
      fund.validateCloseAdvise(closeAdvise);
    } catch (IllegalArgumentException e) {
      sender().tell(failed(closeAdvise.getAdviseId(), e.getMessage(), StatusCodes.BAD_REQUEST), self());
      return;
    }
    persistClosedAdvise(closeAdvise, fund.adviseForId(closeAdvise.getAdviseId()), 
        exitPrice(fund.adviseForId(closeAdvise.getAdviseId()).getTicker(), closeAdvise));
    
  }
  
  private void persistClosedAdvise(CloseAdvise closeAdvise, Advise advise, PriceWithPaf exitPrice) {
    double absoluteAllocation = DoublesUtil
        .round((closeAdvise.getAllocation() / advise.getRemainingAllocation()) * 100.);
    FundAdviceClosed event = new FundAdviceClosed(closeAdvise.getAdviserUsername(), closeAdvise.getFundName(),
        closeAdvise.getAdviseId(), closeAdvise.getAllocation(), absoluteAllocation, closeAdvise.getActualExitDate(),
        exitPrice, false);
    persist(event, (FundAdviceClosed evt) -> {
      fund.update(evt);
      context().parent().tell(evt, self());
      context().system().eventStream().publish(evt);
      if (isZero(absoluteAllocation - 100.0)) {
        sender().tell(new Done(closeAdvise.getAdviseId(),
            "Your exit advise request is being processed, please check the notification for the updates."), self());
      }else
        sender().tell(
            new Done(closeAdvise.getAdviseId(),
                "Your partial exit advise request is being processed, please check the notification for the updates."), self());
    });
  }
  
  private void updateFundAttribute(UpdateFundAttribute command) {
    try {
      command.validateCommand();
    } catch (IllegalArgumentException e) {
      sender().tell(failed(command.getFundName(), e.getMessage(), StatusCodes.BAD_REQUEST), self());
      return;
    }

    FundAttributeUpdate event = new FundAttributeUpdate(command.getAdviserUsername(), command.getFundName(),
        command.getStartDate(), command.getEndDate(), command.getDescription(), command.getTheme(), command.getMinSIP(),
        command.getMinLumpSum(), command.getRiskLevel());

    persist(event, evt -> {
      fund.update(evt);
      sender().tell(new Done(command.getFundName(), "Fund Attribute has been recorded."), self());
    });
  }

  private void updateRecommendedEntryPrice(UpdateAdviseRecommendedEntryPrice command) {
    try {
      command.validate(fund.allAdviseIds());
    } catch (IllegalArgumentException e) {
      sender().tell(failed(command.getAdviseId(), e.getMessage(), StatusCodes.BAD_REQUEST), self());
      return;
    }

    RecommendedEntryPriceUpdated event = new RecommendedEntryPriceUpdated(fund.getAdviserUsername(), fund.getFundName(), command.getRecoEntryPrice(), command.getAdviseId());

    persist(event, evt -> {
      fund.update(evt);
      sender().tell(new Done(command.getAdviseId(),
          "Recommended Entry Price has been updated to " + command.getRecoEntryPrice() + " for your advise"), self());
    });
  }

  private void floatFund(FloatFund floatFund) {
    try {
      floatFund.validateCommand();
    } catch (IllegalArgumentException e) {
      sender().tell(failed(floatFund.getFundName() + ", From " + floatFund.getAdviserUsername() + " failed to launch",
          e.getMessage(), StatusCodes.BAD_REQUEST), self());
      return;
    }
    FundFloated fundFloated = new FundFloated(floatFund.getAdviserUsername(), floatFund.getAdviserName(),
        floatFund.getFundName(), floatFund.getStartDate(), floatFund.getEndDate(), floatFund.getDescription(),

        floatFund.getTheme(), floatFund.getMinSIP(), floatFund.getMinLumpSum(), floatFund.getRiskLevel());
    persist(fundFloated, (FundFloated evt) -> {
      fund.update(evt);
      sender().tell(new Done(floatFund.getFundName(), " fund floated."), self());
    });
  }

  private void closeFund(CloseFund closeFund) {
    FundClosed fundClosed = new FundClosed(closeFund.getUsername(), closeFund.getFundName(),
        closeFund.getEndDate());
    persist(fundClosed, (FundClosed evt) -> {
      fund.update(evt);
      sender().tell(new Done(closeFund.getFundName(), " fund closed."), self());
    });
  }

  private void getFundRealtimeComposition(GetCurrentFundComposition getRealTimeFundComposition, Map<String, PriceWithPaf> adviseIdToRecommendedEntryPrice) {
    tellCurrentFundCompositionRelayerTo(getRealTimeFundComposition,adviseIdToRecommendedEntryPrice);
  }

  private void tellCurrentFundCompositionRelayerTo(GetCurrentFundComposition getRealTimeFundComposition,Map<String, PriceWithPaf> adviseIdToRecommendedEntryPrice) {
    ActorRef internalRequestProcessor = null;
    try {
      internalRequestProcessor = context().actorOf(Props.create(RealtimeFundCompositionRelayer.class,
      fund.allFundComposition(), 
      fund.getCash(),
      adviseIdToRecommendedEntryPrice,
      wdActorSelections), 
      "RealtimeFundCompositionRelayer-" + getRealTimeFundComposition.getUsername());
    } catch (InvalidActorNameException e) {
      internalRequestProcessor = context().actorOf(Props.create(RealtimeFundCompositionRelayer.class,
          fund.allFundComposition(), 
          fund.getCash(),
          adviseIdToRecommendedEntryPrice,
          wdActorSelections), 
          "RealtimeFundCompositionRelayer-" + UUID.randomUUID().toString());
    }
    internalRequestProcessor.tell(getRealTimeFundComposition, self());
  }

  private PriceWithPaf entryPrice(IssueAdvice issueAdvise) throws Exception {
    return price(issueAdvise.getToken(), issueAdvise.getEntryPrice(), issueAdvise.getEntryTime());
  }

  private PriceWithPaf exitPrice(String wdId, CloseAdvise closeAdvise) throws Exception {
    return price(wdId, closeAdvise.getActualExitPrice(), closeAdvise.getActualExitDate());
  }

  private PriceWithPaf price(String token, double price, long time) throws Exception {
    return new PriceWithPaf(new InstrumentPrice(price, time, token), getPricePaf(token, time));
  }

  private Pafs getPricePaf(String token, long time) throws Exception {
    Object result =  ActorResultWaiter.askAndWaitForResult(wdActorSelections.universeRootActor(),
        new GetPricePaf(token, time));
    if (result instanceof Failed) {
      log.error("No price paf returned for " + token + " .Setting default value of 1");
      return withCumulativePaf(1.);
    }
    double paf = (double) result;
    return withCumulativePaf(paf);
  }
  
  
  
  private double calculateEntryPriceForSecondaryBuy(double entryPriceDifferencePostPrimaryBuy,
      double cashAllocationRatio, double sharesRatio) {
    double normalizedAllocation =  cashAllocationRatio / sharesRatio;
    return (entryPriceDifferencePostPrimaryBuy * normalizedAllocation);
  }
  
  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(FundFloated.class, e -> {
      fund = new Fund();
      fund.update(e);
    }).match(FundEvent.class, e -> {
      fund.update(e);
    }).build();
  }

  @Override
  public String persistenceId() {
    return persistenceId;
  }

  @Autowired
  public void setWdActorSelections(WDActorSelections wdActorSelections) {
    this.wdActorSelections = wdActorSelections;
  }

}