package com.wt.domain.write;

import static akka.http.javadsl.model.StatusCodes.FAILED_DEPENDENCY;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;
import com.wt.domain.EquityInstrument;
import com.wt.domain.write.commands.AdviserCorporateEventPrimaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSecondaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSell;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.BODCompleted;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.GetFundDetails;
import com.wt.domain.write.commands.GetIdentityIdByAdviserUsername;
import com.wt.domain.write.commands.GetSymbolForWdId;
import com.wt.domain.write.commands.GetWdIdToInstrumentMap;
import com.wt.domain.write.commands.InternalAdviserCommand;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.SymbolSentForWdId;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.WdIdToInstrumentMapSent;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Exists;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundMaxDrawdownCalculated;
import com.wt.domain.write.events.Result;
import com.wt.domain.write.events.RootAdviserRegistered;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;
import com.wt.utils.email.templates.wealthdesk.BODCompletedEmail;
import com.wt.utils.email.templates.wealthdesk.EODCompletedEmail;

import akka.actor.ActorRef;
import akka.http.javadsl.model.StatusCodes;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.RecoveryCompleted;
import lombok.extern.log4j.Log4j;

@Service("AdviserRootActor")
@Scope("prototype")
@Log4j
public class AdviserRootActor extends AbstractPersistentActor implements ParentActor<AdviserActor> {
  private Map<String, String> adviserIdentitiesWithUsername = new HashMap<String, String>();
  private @Autowired SESEmailUtil emailerService;
  private @Autowired WDProperties properties;
  private @Autowired WDActorSelections wdActorSelections;
  private ChildrenResultsAggregator resultAwaitingActor;
  private Map<String, EquityInstrument> wdIdToEquityInstrumentMap = new HashMap<>();

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(UnAuthenticatedAdviserCommand.class, command -> {
      log.info("Received command " + command);
      UnAuthenticatedAdviserCommand adviserCommand = (UnAuthenticatedAdviserCommand) command;
      try {
        if (isRegistrationCommand(adviserCommand)) {
          try {
            if (adviserCommand instanceof RegisterAdviserFromUserPool)
              registerFromUserPool(adviserCommand);
          } catch (Throwable e) {
            log.error("Exception " + e);
          }
        } else {
          login(adviserCommand);
        }
      } catch (IllegalArgumentException e) {
        sender().tell(failed(adviserCommand.getAdviserUsername(), e.getMessage(), StatusCodes.BAD_REQUEST),
            self());
        return;
      }
    }).match(EODCompleted.class, command -> {
      log.info("Received command " + command);
      wdActorSelections.activeInstrumentTracker().tell(command, self());
      updateWdIdtoInstrumentsMap();
      sendStatusEmailToDevTeam(command);
    }).match(BODCompleted.class, command -> {
      log.info("Received command " + command);
      sendStatusEmailToDevTeam(command);
    }).match(FundMaxDrawdownCalculated.class, fundMaxDrawdownNReturnsCalculated -> {
      log.info("Received command " + fundMaxDrawdownNReturnsCalculated);
      Consumer<Pair<ActorRef, List<Result>>> aggreagatorFunction = pair -> {
        pair.getLeft().tell(new Done(getName(), "All"), self());
      };
      resultAwaitingActor.onResultReceived(fundMaxDrawdownNReturnsCalculated, aggreagatorFunction);
      
      log.info(
          "Sending command to all advisers to calculate EOD fund composition as max drawdown calculation is completed");
    }).match(AuthenticatedAdviserCommand.class, cmd -> {
      String adviserUsername = adviserIdentitiesWithUsername.get(cmd.getUsername());
      if (adviserUsername == null) {
        sender().tell(failed(adviserUsername, "Adviser does not exist.", StatusCodes.NOT_FOUND), self());
        return;
      }
      if (cmd instanceof IssueAdvice) {
        ((IssueAdvice) cmd).setAdviserUsername(adviserUsername);
      }
      if (cmd instanceof CloseAdvise) {
        ((CloseAdvise) cmd).setAdviserUsername(adviserUsername);
      }
      forwardToChildren(context(), cmd, adviserUsername);
    }).match(InternalAdviserCommand.class, cmd -> {
      String adviserUsername = cmd.getAdviserUsername();
      if (adviserUsername == null) {
        sender().tell(failed(adviserUsername, "Adviser does not exist.", StatusCodes.NOT_FOUND), self());
        return;
      }
      forwardToChildren(context(), cmd, adviserUsername);
    }).match(GetFundDetails.class, command -> {
      log.info("Received command " + command);
      if (adviserIdentitiesWithUsername.containsValue(command.getAdviserUsername()))
        forwardToChildren(context(), command, command.getAdviserUsername());
      else
        sender().tell(failed(command.getAdviserUsername(), "This Adviser does not exists", StatusCodes.NOT_FOUND), self());
    }).match(GetIdentityIdByAdviserUsername.class, command -> {
      sender().tell(adviserIdentitiesWithUsername.entrySet().stream()
          .filter(e -> e.getValue().equals(command.getAdviserUserName()))
          .map(Map.Entry::getKey)
          .findFirst()
          .orElse(null), self());
    }).match(AdviserCorporateEventSell.class, command -> {
      forwardCommandToAllChildren(command);
    }).match(AdviserCorporateEventPrimaryBuy.class, command -> {
      forwardCommandToAllChildren(command);
    }).match(AdviserCorporateEventSecondaryBuy.class, command -> {
      forwardCommandToAllChildren(command);
    }).match(GetSymbolForWdId.class, command -> {
      sendSymbolForWdId(command);
    }).build();
  }

  private void sendSymbolForWdId(GetSymbolForWdId command) {
    if (wdIdToEquityInstrumentMap.containsKey(command.getWdId())) {
      String symbol = wdIdToEquityInstrumentMap.get(command.getWdId()).getSymbol();
      sender().tell(new SymbolSentForWdId(command.getWdId(), symbol), self());
    } else{
      sender().tell(new SymbolSentForWdId(command.getWdId(), command.getWdId()), self());
    }
  }

  private void sendStatusEmailToDevTeam(Object command) {
    if(properties.isSendingUserEmailUpdatesAllowed())
    if (command instanceof EODCompleted) {
      EODCompletedEmail template = new EODCompletedEmail(properties.DEV_TEAM_EMAILIDS(), properties.environment());
      emailerService.sendEmail(template);
    } else if (command instanceof BODCompleted) {
      BODCompletedEmail template = new BODCompletedEmail(properties.DEV_TEAM_EMAILIDS(), properties.environment());
      emailerService.sendEmail(template);
    }
  }

  private void login(UnAuthenticatedAdviserCommand adviserCommand) {
    if (adviserRegistered(adviserCommand.getAdviserUsername())) {
      forwardToChildren(context(), adviserCommand, adviserCommand.getAdviserUsername());
    } else {
      sender().tell(
          failed(adviserCommand.getAdviserUsername(), adviserCommand.getAdviserUsername() + " does not exists.",
              StatusCodes.NOT_FOUND),
          self());
    }

  }

  private boolean adviserRegistered(String adviserUsername) {
    return adviserIdentitiesWithUsername.containsValue(adviserUsername);
  }

  private void registerFromUserPool(UnAuthenticatedAdviserCommand adviserCommand) throws Exception {
    RegisterAdviserFromUserPool command = (RegisterAdviserFromUserPool) adviserCommand;
    if (adviserRegistered(command.getAdviserUsername())) {
      sender().tell(Exists.exists(adviserCommand.getAdviserUsername()), self());
      return;
    }
    
    Result registerationResult = (Result) askChild(context(), command, command.getAdviserUsername());

    if (registerationResult instanceof Failed) {
      sender().tell(failed(command.getAdviserUsername(), "Could not create adviser in the system", FAILED_DEPENDENCY),
          self());
      return;
    }

    RootAdviserRegistered rootAdviser = new RootAdviserRegistered(command.getAdviserUsername(),
        adviserCommand.getAdviserUsername());
    persist(rootAdviser, rootAdviserRegistrationEvent -> {
      adviserIdentitiesWithUsername.put(rootAdviserRegistrationEvent.getAdviserUsername(),
          rootAdviserRegistrationEvent.getAdviserUsername());
      sender().tell(registerationResult, self());
    });
    return;
  }

  private boolean isRegistrationCommand(UnAuthenticatedAdviserCommand adviserCommand) {
    return (adviserCommand instanceof RegisterAdviserFromUserPool);
  }
  
  private void forwardCommandToAllChildren(Object obj){
    List<String> adviserUserNames = new ArrayList<String>(adviserIdentitiesWithUsername.values());
    forwardToChildren(context(), obj, adviserUserNames.toArray(new String[0]));
  }

  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(RootAdviserRegistered.class, command -> {
      log.info("Recover " + command);
      adviserIdentitiesWithUsername.put(command.getAdviserUsername(), command.getAdviserUsername());
    }).match(RecoveryCompleted.class, e -> {
      updateWdIdtoInstrumentsMap();
    }).build();
  }
  
  private void updateWdIdtoInstrumentsMap() {

    WdIdToInstrumentMapSent wdIdToInstrumentMapSent = null;
    boolean instrumentsMapReceived = false;
    int retryCounter = 1;
    while (!instrumentsMapReceived && retryCounter <= 5) {
      try {
        Object result = askAndWaitForResult(wdActorSelections.universeRootActor(), new GetWdIdToInstrumentMap());

        if (result instanceof Failed) {
          retryCounter++;
          log.warn("AdviserRootActor :Failed to get instruments by wdId from EquityActor. Retrying " + result);
          continue;
        }
        wdIdToInstrumentMapSent = (WdIdToInstrumentMapSent) result;
        wdIdToEquityInstrumentMap = wdIdToInstrumentMapSent.getWdIdtoInstruments();
        instrumentsMapReceived = true;
        log.info("AdviserRootActor : wdIdToInstrument Map updated");
      } catch (Exception e) {
        log.warn("AdviserRootActor :Exception while getting instruments by wdId from EquityActor. Retrying ", e);
        retryCounter++;
      }
    }
    if(retryCounter >5 )
      log.error("AdviserRootActor : Failed to get instruments map for universe actor after multiple attempts.");
  }
  
  @Override
  public String getChildBeanName() {
    return "AdviserActor";
  }

  @Override
  public String getName() {
    return self().path().name();
  }

  @Override
  public String persistenceId() {
    return "advisers-root";
  }
}