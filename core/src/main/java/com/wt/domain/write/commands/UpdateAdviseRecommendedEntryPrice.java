package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertNotNull;
import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdateAdviseRecommendedEntryPrice extends AdviseCommand {

  private static final long serialVersionUID = 1L;
  @Getter
  private double recoEntryPrice;
  @Getter
  private String adviseId;

  @JsonCreator
  public UpdateAdviseRecommendedEntryPrice(@JsonProperty("username") String username,
      @JsonProperty("recoEntryPrice") double recoEntryPrice, @JsonProperty("adviseId") String adviseId,
      @JsonProperty("fundName") String fundName) {
    super(username, fundName, adviseId);
    assertNotNull(recoEntryPrice, "recoEntryPrice");
    assertStringNotEmpty(adviseId, "adviseId");
    this.recoEntryPrice = recoEntryPrice;
    this.adviseId = adviseId;
  }

  public void validate(Collection<String> advises) {
    if (recoEntryPrice <= 0) {
      throw new IllegalArgumentException(
          "Invalid price " + recoEntryPrice + "! Only non-zero prositive values allowed.");
    }
    if (!advises.contains(this.adviseId)) {
      throw new IllegalArgumentException("This advise does not exists any more.");
    }
  }

  @Override
  public void validate(Object adviseSpecificParameter) throws Exception {
    if (!allAdvises(adviseSpecificParameter).contains(adviseId)) {
      throw new IllegalArgumentException("This advise does not exists any more.");
    }
  }

  @SuppressWarnings("unchecked")
  public static <T extends List<?>> T allAdvises(Object obj) {
      return (T) obj;
  }

}
