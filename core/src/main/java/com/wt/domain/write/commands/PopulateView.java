package com.wt.domain.write.commands;

import akka.stream.Materializer;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PopulateView {
  private String id;
  private long startSequenceNr;
  private Materializer materializer;
}
