package com.wt.domain.write.events;

import lombok.Getter;

public class CloseAdviseFailed extends FundEvent {
  @Getter
  private final double allocation;
  @Getter
  private final double exitPrice;
  @Getter
  private final long exitTime;
  @Getter
  private final String adviseId;
  @Getter
  private final String title;
  @Getter
  private final String errorMessage;


  private static final long serialVersionUID = 1L;


  public CloseAdviseFailed(String adviserUsername, String fundName, double allocation, double exitPrice, long exitTime, String adviseId, String title,
      String errorMessage) {
    super(adviserUsername, fundName);
    this.allocation = allocation;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.adviseId = adviseId;
    this.title = title;
    this.errorMessage = errorMessage;
  }

  @Deprecated
  public CloseAdviseFailed(CloseAdviseIssuancePending closeAdviseReceived, String title, String errorMessage, String identityId) {
    super(closeAdviseReceived.getAdviserUsername(), closeAdviseReceived.getFundName());
    this.allocation = closeAdviseReceived.getAllocation();
    this.exitPrice = closeAdviseReceived.getExitPrice();
    this.exitTime = closeAdviseReceived.getExitTime();
    this.adviseId = closeAdviseReceived.getAdviseId();
    this.title = title;
    this.errorMessage = errorMessage;
  }
  
  
}
