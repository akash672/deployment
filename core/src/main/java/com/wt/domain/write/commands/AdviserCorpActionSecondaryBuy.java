package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@EqualsAndHashCode
public class AdviserCorpActionSecondaryBuy implements Serializable{

  private static final long serialVersionUID = 1L;
  private String oldTicker;
  private String newTicker;
  private double cashAllocationRatio;
  private double sharesRatio;
  private String adviserUsername;
  private String symbol;
  
  
  public AdviserCorpActionSecondaryBuy(String oldTicker, String newTicker,
      double cashAllocationRatio, double sharesRatio, String adviserUsername, String symbol) {
    super();
    this.oldTicker = oldTicker;
    this.newTicker = newTicker;
    this.cashAllocationRatio = cashAllocationRatio;
    this.sharesRatio = sharesRatio;
    this.adviserUsername = adviserUsername;
    this.symbol = symbol;
  }
  
  

  
  
  

}
