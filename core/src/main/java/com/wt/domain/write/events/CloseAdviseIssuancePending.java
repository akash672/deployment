package com.wt.domain.write.events;

import java.io.IOException;
import java.text.ParseException;

import com.wt.domain.write.commands.CloseAdvise;

import lombok.Getter;

@Getter
public class CloseAdviseIssuancePending extends FundEvent {

  private String ticker;
  private final double allocation;
  private final double exitPrice;
  private final long exitTime;
  private final String adviseId;
  private final String identityId;

  private static final long serialVersionUID = 1L;

  public CloseAdviseIssuancePending(String adviserUsername, String fundName, String token, double allocation,
      double entryPrice, long entryTime, double exitPrice, long exitTime, long issueTime, String adviseId,
      String identityId, String ticker) {
    super(adviserUsername, fundName);
    this.allocation = allocation;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.adviseId = adviseId;
    this.identityId = adviserUsername;
    this.ticker = ticker;
  }

  public CloseAdviseIssuancePending(CloseAdvise command, String ticker) {
    super(command.getAdviserUsername(), command.getFundName());
    this.allocation = command.getAllocation();
    this.exitPrice = command.getActualExitPrice();
    this.exitTime = command.getActualExitDate();
    this.adviseId = command.getAdviseId();
    this.identityId = command.getUsername();
    this.ticker = ticker;
  }

  public CloseAdviseIssuancePending(CloseAdvise closedAdvise) {
    super(closedAdvise.getAdviserUsername(), closedAdvise.getFundName());
    this.allocation = closedAdvise.getAllocation();
    this.exitPrice = closedAdvise.getActualExitPrice();
    this.exitTime = closedAdvise.getActualExitDate();
    this.adviseId = closedAdvise.getAdviseId();
    this.identityId = closedAdvise.getUsername();
  }

  public CloseAdvise createCommand() throws ParseException, IOException {
    return new CloseAdvise(getAdviserUsername(), getFundName(), adviseId, allocation, exitPrice);
  }

}
