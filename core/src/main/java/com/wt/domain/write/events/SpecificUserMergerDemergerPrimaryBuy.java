package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SpecificUserMergerDemergerPrimaryBuy extends MergerDemergerEvent{

	private static final long serialVersionUID = 1L;
	private String token;
	private double oldPrice;
	private double newPrice;
	private double sharesRatio;
	private String symbol;
	private String userName;
	private String fundName;
	private String adviserUsername;
	private String adviseId;
	
	
	public SpecificUserMergerDemergerPrimaryBuy(@JsonProperty("token")String token, @JsonProperty("oldPrice")double oldPrice, @JsonProperty("newPrice")double newPrice, 
			@JsonProperty("sharesRatio")double sharesRatio,
			@JsonProperty("symbol")String symbol, @JsonProperty("userName")String userName, @JsonProperty("fundName")String fundName,
			@JsonProperty("adviserUsername")String adviserUsername, @JsonProperty("adviseIds")String adviseId) {
		super();
		this.token = token;
		this.oldPrice = oldPrice;
		this.newPrice = newPrice;
		this.sharesRatio = sharesRatio;
		this.symbol = symbol;
		this.userName = userName;
		this.fundName = fundName;
		this.adviserUsername = adviserUsername;
		this.adviseId = adviseId;
	}
	
	
	
	

}
