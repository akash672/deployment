package com.wt.domain.write.events;

import static com.wt.utils.DoublesUtil.round;

import com.google.gson.annotations.Expose;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class AdvisePerformanceCalculated extends AdviseEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  @Expose
  private final double performance;
  @Getter
  @Expose(serialize = false)
  private final long date;
  @Getter
  @Expose(serialize = false)
  private double realizedM2M;
  @Getter
  @Expose(serialize = false)
  private double unrealizedM2M;
  @Getter
  @Expose
  private double remainingAllocation;
  @Expose
  @Getter
  private String ticker;
  @Expose
  @Setter
  @Getter
  private String symbol;

  public AdvisePerformanceCalculated(String adviserUsername, String fundName, String adviseId, String ticker, String symbol,
      double realizedM2M, double unrealizedM2M, double performance, long timestamp, double remainingAllocation) {
    super(adviserUsername, fundName, adviseId);
    this.ticker = ticker;
    this.symbol = symbol;
    this.realizedM2M = realizedM2M;
    this.unrealizedM2M = unrealizedM2M;
    this.performance = round(performance);
    this.date = timestamp;
    this.remainingAllocation = round(remainingAllocation);
  }

  public AdvisePerformanceCalculated(String adviserUsername, String fundName, String adviseId, String ticker,
      double realizedM2M, double unrealizedM2M, double performance, long timestamp, double remainingAllocation) {
    super(adviserUsername, fundName, adviseId);
    this.ticker = ticker;
    this.symbol = ticker;
    this.realizedM2M = realizedM2M;
    this.unrealizedM2M = unrealizedM2M;
    this.performance = round(performance);
    this.date = timestamp;
    this.remainingAllocation = round(remainingAllocation);
  }
}
