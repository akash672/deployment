package com.wt.domain.write.commands;

import akka.actor.ActorRef;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GetCurrentFundComposition extends InternalFundCommand {

  private ActorRef senderActor;
  private String username;

  public GetCurrentFundComposition(String fundName, String adviserusername, String username, ActorRef senderActor) {
    super(adviserusername, fundName);
    this.senderActor = senderActor;
    this.username = username;
  }

}
