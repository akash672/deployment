package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GetFundRebalancingView extends FundCommand {

  private static final long serialVersionUID = 1L;

  @JsonCreator
  public GetFundRebalancingView(@JsonProperty("username") String adviserUsername, @JsonProperty("fundName") String fundName) {
    super(adviserUsername, fundName);
  }

}