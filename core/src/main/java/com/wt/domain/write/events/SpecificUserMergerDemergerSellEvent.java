package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class SpecificUserMergerDemergerSellEvent extends MergerDemergerEvent {
	
	private static final long serialVersionUID = 1L;
	private String userName;
	private String adviserUsername;
	private String fundName;
	private String adviseId;
	private String token;
	private double price;
	
	public SpecificUserMergerDemergerSellEvent(@JsonProperty("adviserUsername")String adviserUsername, @JsonProperty("fundName")String fundName, 
			@JsonProperty("adviseId")String adviseId, @JsonProperty("token")String token,
			@JsonProperty("price")double price, @JsonProperty("userName")String userName) {
		this.userName = userName;
		this.adviseId = adviseId;
		this.adviserUsername = adviserUsername;
		this.fundName = fundName;
		this.token = token;
		this.price = price;
	}

}
