package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.WDCredentials;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviserCredentialsWithType extends Done {
  private static final long serialVersionUID = 1L;
  private WDCredentials credentials;

  @JsonCreator
  public AdviserCredentialsWithType(@JsonProperty("id") String id,
      @JsonProperty("credentials") WDCredentials credentials) {
    super(id, "Adviser Registered");
    this.credentials = credentials;
  }
  
  
}
