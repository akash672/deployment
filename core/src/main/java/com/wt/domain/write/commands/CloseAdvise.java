package com.wt.domain.write.commands;

import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.isExecutionWindowOpen;
import static com.wt.utils.DateUtils.todayEOD;
import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;
import static java.lang.Double.compare;

import java.io.IOException;
import java.text.ParseException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class CloseAdvise extends AdviseCommand {
  
  private static final long serialVersionUID = 1L;
  @Getter
  private double allocation;
  @Getter
  @Setter
  private long actualExitDate;
  @Getter
  @Setter
  private double actualExitPrice;
  @Getter
  @Setter
  private String adviserUsername;

  public CloseAdvise(@JsonProperty("username") String username, @JsonProperty("fundName") String fundName,
      @JsonProperty("adviseId") String adviseId, @JsonProperty("allocation") double allocation,
      @JsonProperty("actualExitPrice") double actualExitPrice) throws ParseException, IOException {
    super(username, fundName, adviseId);
    this.allocation = DoublesUtil.round(allocation);
    this.actualExitDate = (currentTimeInMillis() > todayEOD()) ? todayEOD() : currentTimeInMillis();
    this.actualExitPrice = actualExitPrice;
  }

  public CloseAdvise(String adviserIdentity, String fundName, String adviseId, double allocation, long actualExitDate,
      double actualExitPrice) {
    super(adviserIdentity, fundName, adviseId);
    this.allocation = round(allocation);
    this.actualExitDate = actualExitDate;
    this.actualExitPrice = actualExitPrice;
  }

  @Override
  public void validate(Object adviseSpecificParameter) throws Exception {
    validateAllocation((double) adviseSpecificParameter);
    handleAdviseDuringMarketClose();
    validateExitPrice();
    validateExitTime();

  }

  private void handleAdviseDuringMarketClose() throws ParseException, IOException {
    if (!isExecutionWindowOpen()) {
      throw new RuntimeException("Market is closed. Please issue new advise during market hours.");
    }
  }

  private void validateExitTime() throws IllegalArgumentException {
    if (actualExitDate > DateUtils.currentTimeInMillis()) {
      throw new IllegalArgumentException("Exit time is in future and is greater than the current time ");
    }

  }

  private void validateExitPrice() throws IllegalArgumentException {
    if (actualExitPrice < 0.) {
      throw new IllegalArgumentException("Price should be non zero only");
    }
  }

  private void validateAllocation(double currentAllocation) throws IllegalArgumentException {
    if (compare(allocation, 0.) <= 0)
      throw new IllegalArgumentException("Closing Advice Allocation in the portfolio should only be non-zero");
    else if (isZero(currentAllocation))
      throw new IllegalArgumentException("This advise is closed already.");
    else if (compare(allocation, currentAllocation) > 0) {
      throw new IllegalArgumentException(
          "Entered allocation " + allocation + "% is greater than the advise remaining allocation, i.e. "
              + currentAllocation + "% is only remaining to be closed.");
    }
  }
}
