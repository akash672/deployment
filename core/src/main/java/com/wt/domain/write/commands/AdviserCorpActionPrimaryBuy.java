package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviserCorpActionPrimaryBuy implements Serializable{

  private static final long serialVersionUID = 1L;
  private String ticker;
  private double oldPrice;
  private double newPrice;
  private String adviserUsername;
  private double sharesRatio;
  private String symbol;
  
  public AdviserCorpActionPrimaryBuy(String ticker, double oldPrice, double newPrice,
      String adviserUsername, double sharesRatio, String symbol) {
    super();
    this.ticker = ticker;
    this.oldPrice = oldPrice;
    this.newPrice = newPrice;
    this.adviserUsername = adviserUsername;
    this.sharesRatio = sharesRatio;
    this.symbol = symbol;
  }
}
