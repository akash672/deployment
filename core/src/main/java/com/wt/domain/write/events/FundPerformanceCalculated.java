package com.wt.domain.write.events;

import static com.wt.utils.DoublesUtil.round;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.wt.utils.DoublesUtil;

import lombok.Getter;
import lombok.ToString;

@ToString
public class FundPerformanceCalculated extends FundEvent {
  private static final long serialVersionUID = 1L;
  @Getter @Expose
  private double performance;
  @Getter 
  private final long date;
  @Getter
  private double realizedM2M;
  @Getter 
  private double unrealizedM2M;
  @Getter 
  private double remainingAllocation;
  @Getter @Expose
  private transient List<AdvisePerformanceCalculated> allAdvisePerformances;
  
  public FundPerformanceCalculated(String adviserUsername, String fundName, double performance, long timestamp,
      double realizedM2M, double unrealizedM2M, double remainingAllocation, List<AdvisePerformanceCalculated> allAdvisePerformances) {
    super(adviserUsername, fundName);
    this.performance = performance;
    this.date = timestamp;
    this.realizedM2M = realizedM2M;
    this.unrealizedM2M = unrealizedM2M;
    this.remainingAllocation = remainingAllocation;
    this.allAdvisePerformances = allAdvisePerformances;
  }
  
  public void addToPerformance(double performanceNumber){
    performance = round(performance + performanceNumber);
  }

  public static class FundPerformanceCalculatedBuilder {
    private List<AdvisePerformanceCalculated> all = new ArrayList<>();
    private String adviserUsername;
    private String fundName;
    private long timestamp = 0;

    public FundPerformanceCalculatedBuilder(String adviserUsername, String fundName, long date) {
      super();
      this.adviserUsername = adviserUsername;
      this.fundName = fundName;
      this.timestamp = date;
    }

    public FundPerformanceCalculatedBuilder withAdvisePerformance(
        AdvisePerformanceCalculated advisePerformanceCalculated) {
      all.add(advisePerformanceCalculated);
      return this;
    }
    
    public FundPerformanceCalculatedBuilder withAdvisePerformance(
        Collection<AdvisePerformanceCalculated> allAdvisePerformanceCalculated) {
      all.addAll(allAdvisePerformanceCalculated);
      return this;
    }

    public FundPerformanceCalculated build() {
      double performance = 0D;
      double realizedM2M = 0D;
      double unrealizedM2M = 0D;
      double remainingAllocation = 100D;
      List<AdvisePerformanceCalculated> allAdvisePerformances = new ArrayList<>();
      for (AdvisePerformanceCalculated advisePerformanceCalculated : all) {
        performance += advisePerformanceCalculated.getPerformance();
        realizedM2M += advisePerformanceCalculated.getRealizedM2M();
        unrealizedM2M += advisePerformanceCalculated.getUnrealizedM2M();
        remainingAllocation -= advisePerformanceCalculated.getRemainingAllocation();
        if (!(DoublesUtil.isZero(advisePerformanceCalculated.getRemainingAllocation())))
          allAdvisePerformances.add(advisePerformanceCalculated);
      }
      AdvisePerformanceCalculated advisePerformanceCalculated = 
          new AdvisePerformanceCalculated(adviserUsername, fundName, "Cash", "Cash", "Cash", 0., 0., 0.,timestamp, round(remainingAllocation));
      allAdvisePerformances.add(advisePerformanceCalculated);
      return new FundPerformanceCalculated(adviserUsername, fundName, round(performance), timestamp,
          round(realizedM2M), round(unrealizedM2M), round(remainingAllocation), allAdvisePerformances);
    }
  }

}
