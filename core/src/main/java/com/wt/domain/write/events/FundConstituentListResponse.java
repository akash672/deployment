package com.wt.domain.write.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.FundConstituent;

import lombok.Getter;
import lombok.ToString;

@Getter
@JsonIgnoreProperties({"id"})
@ToString
public class FundConstituentListResponse extends Done {
  private static final long serialVersionUID = 1L;
  private String fundName;
  private List<FundConstituent> fundConstituentList;

  @JsonCreator
  public FundConstituentListResponse(@JsonProperty("id") String id, @JsonProperty("fundName") String fundName,
      @JsonProperty("fundCompositionList") Collection<FundConstituent> fundConstituentList) {
    super(id, "");
    this.fundName = fundName;
    if (fundConstituentList != null)
      this.fundConstituentList = new ArrayList<>(fundConstituentList);
    else
      this.fundConstituentList = new ArrayList<>();
  }

}
