package com.wt.domain.write.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class GetFundDetails  {

  private String adviserUsername;
  private String fundName;

}
