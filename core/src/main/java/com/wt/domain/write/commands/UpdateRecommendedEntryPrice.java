package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertNotNull;
import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class UpdateRecommendedEntryPrice {

  @Getter
  private double recoEntryPrice;
  @Getter
  private String adviseId;
  @Getter
  private String fundName;
  
  @JsonCreator
  public UpdateRecommendedEntryPrice(@JsonProperty("recoEntryPrice") double recoEntryPrice,
      @JsonProperty("adviseId") String adviseId, @JsonProperty("fundName") String fundName) {
    assertStringNotEmpty(adviseId, "adviseId");
    assertNotNull(recoEntryPrice, "recoEntryPrice");
    this.recoEntryPrice = recoEntryPrice;
    this.adviseId = adviseId;
    this.fundName = fundName;
  }
}
