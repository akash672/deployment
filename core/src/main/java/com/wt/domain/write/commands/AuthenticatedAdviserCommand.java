package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@ToString
@AllArgsConstructor
public abstract class AuthenticatedAdviserCommand implements Serializable {
  
  private static final long serialVersionUID = 1L;
  @NonNull
  @Getter
  protected String username;
  
}
