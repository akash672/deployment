package com.wt.domain.write.events;

import com.wt.domain.BrokerName;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class MergerDemergerPrimaryBuyIssued extends AdviseEvent{
  
  private static final long serialVersionUID = 1L;
  private String token;
  private double oldPrice;
  private double newPrice;
  private double sharesRatio;
  @Setter private BrokerName brokerName;
  @Setter private String clientCode;
  private String symbol;
  
  public MergerDemergerPrimaryBuyIssued(String adviserUsername, String fundName, String adviseId,
      String token, double oldPrice, double newPrice, double sharesRatio, String symbol) {
    super(adviserUsername, fundName, adviseId);
    this.token = token;
    this.oldPrice = oldPrice;
    this.newPrice = newPrice;
    this.sharesRatio = sharesRatio;
    this.symbol = symbol;
  }
}
