package com.wt.domain.write.commands;

import lombok.NonNull;
import lombok.ToString;

@ToString(callSuper=true)
public abstract class AdviseCommand extends FundCommand {
  
  private static final long serialVersionUID = 1L;
	@NonNull
	private String adviseId;
	
	public AdviseCommand(String adviserIdentity, String fundName, String adviseId) {
		super(adviserIdentity, fundName);
		this.adviseId = adviseId;
	}

	public String getAdviseId() {
		return adviseId;
	}
	
	public abstract void validate(Object adviseSpecificParameter) throws Exception;
	
}
