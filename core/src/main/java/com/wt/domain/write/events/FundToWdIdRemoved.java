package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter

public class FundToWdIdRemoved extends UpdateWdIdMap implements Serializable {
 
  private static final long serialVersionUID = 1L;
  private String fundNameWithAdviserUserName;
  
  public FundToWdIdRemoved(String wdId, String fundNameWithAdviserUserName) {
    super(wdId);
    this.fundNameWithAdviserUserName = fundNameWithAdviserUserName;
  }
}
