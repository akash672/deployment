package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;

import lombok.Getter;
@Getter
public class GetFundAdvises extends FundCommand {

  private static final long serialVersionUID = 1L;
  @JsonCreator
  public GetFundAdvises(String adviserName, String fundName) {
    super(adviserName, fundName);
  }
}
