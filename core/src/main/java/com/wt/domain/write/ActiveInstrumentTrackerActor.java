package com.wt.domain.write;

import static akka.stream.ThrottleMode.shaping;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;
import com.wt.domain.ActiveInstrumentTracker;
import com.wt.domain.FundConstituent;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.AddFundToActiveWdId;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.FundNamesForWdIdSent;
import com.wt.domain.write.commands.GetActiveTokens;
import com.wt.domain.write.commands.GetFundNamesWithWdId;
import com.wt.domain.write.commands.GetLivePriceStream;
import com.wt.domain.write.commands.GetPriceBasketFromTracker;
import com.wt.domain.write.commands.GetPriceFromTracker;
import com.wt.domain.write.commands.PriceSnapshotsFromPriceSource;
import com.wt.domain.write.commands.SendPricesToRDSViaSNS;
import com.wt.domain.write.commands.SendSubscriptionForInstruments;
import com.wt.domain.write.commands.SubscribeAndGetLivePriceStream;
import com.wt.domain.write.commands.SubscribeToFollowingTickers;
import com.wt.domain.write.events.ActiveInstrument;
import com.wt.domain.write.events.FundToWdIdAdded;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.ResetActiveInstruments;
import com.wt.utils.akka.WDActorSelections;

import akka.persistence.AbstractPersistentActor;
import akka.persistence.RecoveryCompleted;
import akka.persistence.SnapshotOffer;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Service("ActiveInstrumentTrackerActor")
@Scope("prototype")
@Log4j
public class ActiveInstrumentTrackerActor extends AbstractPersistentActor {

  private WDActorSelections wdActorSelection;
  private WDProperties wdproperties;
  private ActorMaterializer materializer;
  private ActiveInstrumentTracker activetracker = new ActiveInstrumentTracker();
  private ConcurrentHashMap<String, PriceWithPaf> tokenToInstrumentPrice = new ConcurrentHashMap<String, PriceWithPaf>();
  
  @Autowired
  public void setWdActorSelection(@Qualifier("WDActors") WDActorSelections wdActorSelection) {
    this.wdActorSelection = wdActorSelection;
  }
  
  @Autowired
  public void setWDproperties(WDProperties wdProperties){
    this.wdproperties = wdProperties;
  }
  
  @Autowired
  public void setMaterializer(ActorMaterializer materializer) { 
    this.materializer = materializer;
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(SendSubscriptionForInstruments.class, command -> {
      handleSendSubscriptionFor(command);
    }).match(GetActiveTokens.class, command -> {
      tellRealTimeToSubscribeToFollowingTokens();
    }).match(PriceWithPaf.class, command -> {
      updatePriceFromUniverse(command);
    }).match(GetPriceBasketFromTracker.class, command -> {
      returnPriceBasket(command);
    }).match(GetPriceFromTracker.class, command -> {
      returnPriceIndividual(command);
    }).match(EODCompleted.class, command -> {
      saveSnapshot(activetracker);
    }).match(AddFundToActiveWdId.class, command -> {
      addFundToActiveTicker(command);
    }).match(GetFundNamesWithWdId.class, command -> {
      replyWithFundNamesForWdId(command);
    }).match(SendPricesToRDSViaSNS.class, command -> {
      sendPriceBatchToRDSFacadeRootActor();
    }).build();
  }

  private void sendPriceBatchToRDSFacadeRootActor() {
    HashMap<String, PriceWithPaf> activeStocksPriceMap = new HashMap<String, PriceWithPaf>(tokenToInstrumentPrice);
    wdActorSelection.rdsFacadeRootActor().tell(new PriceSnapshotsFromPriceSource(activeStocksPriceMap), self());
  }

  private void addFundToActiveTicker(AddFundToActiveWdId command) {
    FundToWdIdAdded addFundToWdId = new FundToWdIdAdded(command.getWdId(), command.getFundNameWithAdviserUserName());
    persist(addFundToWdId, event -> {
      activetracker.update(event);
      wdActorSelection.universeRootActor().tell(new SubscribeAndGetLivePriceStream(event.getWdId()), self());
    });
  }

  private void replyWithFundNamesForWdId(GetFundNamesWithWdId command) {
    if(activetracker.getWdIdToListOfFundsMap().containsKey(command.getWdId()))
      sender().tell(new FundNamesForWdIdSent(activetracker.getWdIdToListOfFundsMap().get(command.getWdId())), self());
    else
      sender().tell(new FundNamesForWdIdSent(new ArrayList<String>()), self());
  }


  private void returnPriceBasket(GetPriceBasketFromTracker command) {
    HashMap<String, PriceWithPaf> localTokenToPrice = new HashMap<String, PriceWithPaf>();
    for (String wdId : command.getCollectionOfWdId()) {
      if (tokenToInstrumentPrice.get(wdId) != null)
        localTokenToPrice.put(wdId, sendLastPriceWithCurrentTimeStamp(tokenToInstrumentPrice.get(wdId)));
      else {
        try {
          PriceWithPaf priceWithPaf = (PriceWithPaf) askAndWaitForResult(wdActorSelection.universeRootActor(),
              new GetLivePriceStream(wdId));
          localTokenToPrice.put(wdId, sendLastPriceWithCurrentTimeStamp(priceWithPaf));
        } catch (Exception e) {
          log.error("Could not get the price while getting price from Universe Service for " + wdId);
        }
      }
    }
    sender().tell(new PriceSnapshotsFromPriceSource(localTokenToPrice), self());
  }

  private void returnPriceIndividual(GetPriceFromTracker command) {
    String wdId = command.getWdId();
    if (tokenToInstrumentPrice.get(wdId) != null)
      sender().tell(sendLastPriceWithCurrentTimeStamp(tokenToInstrumentPrice.get(command.getWdId())), self());
    else {
      try {
        PriceWithPaf priceWithPaf = (PriceWithPaf) askAndWaitForResult(wdActorSelection.universeRootActor(),
            new GetLivePriceStream(wdId));
        sender().tell(sendLastPriceWithCurrentTimeStamp(priceWithPaf), self());
      } catch (Exception e) {
        log.error("Could not get the price while getting price from Universe Service for " + wdId);
      }
    }
  }

  private void updatePriceFromUniverse(PriceWithPaf priceWithPaf) {
    tokenToInstrumentPrice.put(priceWithPaf.getPrice().getWdId(), priceWithPaf);
  }

  private PriceWithPaf sendLastPriceWithCurrentTimeStamp(PriceWithPaf lastPriceWithPaf) {
    PriceWithPaf currentTimePriceWithPaf = new PriceWithPaf(new InstrumentPrice(lastPriceWithPaf.getPrice().getPrice(),
        currentTimeInMillis(), lastPriceWithPaf.getPrice().getWdId()), lastPriceWithPaf.getPafs());
    return currentTimePriceWithPaf;
  }

  private void tellRealTimeToSubscribeToFollowingTokens() {
    SubscribeToFollowingTickers subscribeToFollowingTickers = new SubscribeToFollowingTickers(
        activetracker.getWdIdToListOfFundsMap().keySet());
    wdActorSelection.realtimeInvestorPerformanceRootActor().tell(subscribeToFollowingTickers, self());
  }

  private void handleSendSubscriptionFor(SendSubscriptionForInstruments command) {
    List<FundConstituent> fundConstituents = command.getFundConstituents();
    fundConstituents.stream().forEach(fundConstituent -> {
      FundToWdIdAdded addFundToWdId = new FundToWdIdAdded(fundConstituent.getTicker(), fundConstituent.getFundConstituentKey().getFundWithAdviser());
      if (!notYetSubscribed(addFundToWdId.getWdId(), fundConstituent.getFundConstituentKey().getFundWithAdviser()))
        persist(addFundToWdId, event -> {
            wdActorSelection.universeRootActor().tell(new SubscribeAndGetLivePriceStream(event.getWdId()), self());
            activetracker.update(event);
      });
    });
  }

  private boolean notYetSubscribed(String ticker, String fundNameWithAdviserUserName) {
    if (!activetracker.getWdIdToListOfFundsMap().keySet().contains(ticker))
      return false;
    else {
      List<String> fundIdsList = activetracker.getWdIdToListOfFundsMap().get(ticker);
      if (fundIdsList.contains(fundNameWithAdviserUserName))
        return true;
      else
        return false;
    }
  }

  @Override
  public Receive createReceiveRecover() {
    return receiveBuilder().match(SnapshotOffer.class, event -> {
      activetracker = (ActiveInstrumentTracker) event.snapshot();
      log.info("Recover " + activetracker);
      intializeDomainAttributeForNoSnapshot();
    }).match(ActiveInstrument.class, event -> {
      log.info("Recover " + event);
      activetracker.update(event);
    }).match(FundToWdIdAdded.class, event -> {
      activetracker.update(event);
    }).match(RecoveryCompleted.class, event -> {
      tellRealTimeToSubscribeToFollowingTokens();
      sendSubscriptionCallsToUniverseActor();
    }).match(ResetActiveInstruments.class, event -> {
      activetracker.update(event);
    }).build();
  }

  private void sendSubscriptionCallsToUniverseActor() {
    if(activetracker.getWdIdToListOfFundsMap().keySet().size() > 0)
      Source.from(activetracker.getWdIdToListOfFundsMap().keySet())
          .throttle(wdproperties.NUMBER_OF_CHILDREN_TO_RECOVER(),
              Duration.ofSeconds(wdproperties.CHILD_RECOVERY_FREQUENCY()),
              wdproperties.CHILD_RECOVERY_MAXIMUM_BURST(), shaping())
          .runForeach(ticker -> {
            wdActorSelection.universeRootActor().tell(new SubscribeAndGetLivePriceStream(ticker), self());
          }, materializer);
  }
  
  private void intializeDomainAttributeForNoSnapshot() {
    if (activetracker.getWdIdToListOfFundsMap() == null) {
      activetracker.setWdIdToListOfFundsMap(new ConcurrentHashMap<>());
    }
  }

  @Override
  public String persistenceId() {
    return "active-instrument-tracker";
  }

}
