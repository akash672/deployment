package com.wt.domain.write.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode(callSuper=false)
@ToString
@Getter
public class MergerDemergerFundAdviseClosed extends AdviseEvent {
  
  private static final long serialVersionUID = 1L;
  private String token;
  private double price;

  public MergerDemergerFundAdviseClosed(String adviserUsername, String fundName, String adviseId,
                                      String token, double price) {
    super(adviserUsername, fundName, adviseId);
    this.token = token;
    this.price = price;
  }
  
  

}
