package com.wt.domain.write.events;

public class AdviserLoggedIn extends AdviserEvent {
  private static final long serialVersionUID = 1L;

  public AdviserLoggedIn(String adviserUsername) {
    super(adviserUsername);
  }

}
