package com.wt.domain.write.commands;

import static com.amazonaws.util.ValidationUtils.assertStringNotEmpty;

import lombok.Getter;
import lombok.NonNull;

@Getter
public abstract class InternalFundCommand extends InternalAdviserCommand {
  @NonNull
  private String fundName;

  public InternalFundCommand(String adviserName, String fundName) {
    super(adviserName);
    this.fundName = fundName;
    assertStringNotEmpty(fundName, "fundName");
  }

}
