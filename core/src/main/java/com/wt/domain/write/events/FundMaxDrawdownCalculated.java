package com.wt.domain.write.events;

public class FundMaxDrawdownCalculated extends Done {
  private static final long serialVersionUID = 1L;

  public FundMaxDrawdownCalculated(String id, String message) {
    super(id, message);
  }

}