package com.wt.domain.write.events;

import com.wt.domain.BrokerName;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class MergerDemergerSecondaryBuyIssued extends AdviseEvent{

  private static final long serialVersionUID = 1L;
  private String oldToken;
  private String newToken;
  private double sharesRatio;
  private double cashAllocationRatio;
  private String symbol;
  @Setter private BrokerName brokerName;
  @Setter private String clientCode;
  

  public MergerDemergerSecondaryBuyIssued(String adviserUsername, String fundName, String adviseId,
      String oldToken, String newToken, double sharesRatio, double cashAllocationRatio, String symbol) {
    super(adviserUsername, fundName, adviseId);
    this.oldToken = oldToken;
    this.newToken = newToken;
    this.sharesRatio = sharesRatio;
    this.cashAllocationRatio = cashAllocationRatio;
    this.symbol = symbol;
  }

}
