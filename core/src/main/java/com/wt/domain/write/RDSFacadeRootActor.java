package com.wt.domain.write;

import java.text.ParseException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.PriceSnapshotsFromPriceSource;
import com.wt.domain.write.commands.SendPricesToRDSViaSNS;
import com.wt.utils.CoreSNSToDDBHandler;
import com.wt.utils.CoreSNSToRDSHandler;
import com.wt.utils.DateUtils;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.Cancellable;
import akka.actor.UntypedAbstractActor;
import lombok.extern.log4j.Log4j;
import scala.concurrent.duration.Duration;

@Service("RDSFacadeRootActor")
@Scope("prototype")
@Log4j
public class RDSFacadeRootActor extends UntypedAbstractActor {

  private WDActorSelections wdActorSelections;
  private WDProperties wdProperties;
  @Autowired
  @Lazy
  private CoreSNSToRDSHandler snsToRDSHandler;
  @Autowired
  @Lazy
  private CoreSNSToDDBHandler snsToDDBHandler;

  @Autowired
  public void setWDActorSelections(@Qualifier("WDActors") WDActorSelections wdActorSelections) {
    this.wdActorSelections = wdActorSelections;
  }

  @Autowired
  public void setWDProperties(WDProperties wdProperties) {
    this.wdProperties = wdProperties;
  }

  @SuppressWarnings("unused")
  @PostConstruct
  public void initialScheduler() {
    Cancellable askForPrices = getContext().system().scheduler().schedule(Duration.create(500, TimeUnit.MILLISECONDS),
        Duration.create(wdProperties.ASK_FOR_PRICES_FROM_TRACKER_INTERVAL(), TimeUnit.MINUTES), getSelf(),
        new SendPricesToRDSViaSNS(), getContext().dispatcher(), null);
  }

  @Override
  public void onReceive(Object cmd) throws ParseException {
    if (cmd instanceof SendPricesToRDSViaSNS) {
      if (DateUtils.isMarketOpen()) {
        log.info("SendPricesToRDS periodic message processed by RDSFacadeRootActor");
        wdActorSelections.activeInstrumentTracker().tell((SendPricesToRDSViaSNS) cmd, self());
      }
    } else if (cmd instanceof PriceSnapshotsFromPriceSource) {
      HashMap<String, PriceWithPaf> tickerToPriceWithPafMap = ((PriceSnapshotsFromPriceSource) cmd)
          .getTickerToPriceWithPafMap();
      snsToRDSHandler.handlePriceData(tickerToPriceWithPafMap);
      snsToDDBHandler.handlePriceData(tickerToPriceWithPafMap);
    }
  }
}
