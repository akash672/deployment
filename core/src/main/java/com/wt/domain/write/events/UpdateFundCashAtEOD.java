package com.wt.domain.write.events;

import lombok.Getter;

public class UpdateFundCashAtEOD extends FundEvent{

  private static final long serialVersionUID = 1L;
  @Getter private double cash;
  public UpdateFundCashAtEOD(String adviserUsername, String fundName, double cash) {
    super(adviserUsername, fundName);
    this.cash = cash;
  }

}
