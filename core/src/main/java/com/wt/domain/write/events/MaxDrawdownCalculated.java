package com.wt.domain.write.events;

import java.util.List;

import com.wt.domain.MaxDrawdown;

import lombok.Getter;
import lombok.ToString;

@ToString
public class MaxDrawdownCalculated extends FundEvent {

  private static final long serialVersionUID = 1L;

  @Getter
  private List<MaxDrawdown> maxDrawDowns;

  public MaxDrawdownCalculated(String adviserName, String fundName, List<MaxDrawdown> maxDrawDowns) {
    super(adviserName, fundName);
    this.maxDrawDowns = maxDrawDowns;
  }
  
}