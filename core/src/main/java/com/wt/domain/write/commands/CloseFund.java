package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class CloseFund extends FundCommand {

  private static final long serialVersionUID = 1L;
  private long endDate;

  @JsonCreator
  public CloseFund(@JsonProperty("username") String username, @JsonProperty("fundName") String fundName,
      @JsonProperty("endDate") long endDate) {
    super(username, fundName);
    this.endDate = endDate;
  }

  public void validateCommand() {
    if (!isValidEndDate()) {
      throw new IllegalArgumentException("End date is not valid");
    }
  }

  private boolean isValidEndDate() {
    if (this.endDate == 0L)
      return false;
    return true;
  }

}