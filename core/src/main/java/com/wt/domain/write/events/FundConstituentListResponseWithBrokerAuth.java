package com.wt.domain.write.events;

import java.io.Serializable;

import com.wt.domain.write.commands.BrokerAuthInformation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FundConstituentListResponseWithBrokerAuth implements Serializable{
  
  private static final long serialVersionUID = 1L;
  private FundConstituentListResponse fundListResponse;
  private BrokerAuthInformation brokerAuthInformation;
  
  
  public FundConstituentListResponseWithBrokerAuth(FundConstituentListResponse fundListResponse, BrokerAuthInformation brokerAuthInformation) {
    super();
    this.fundListResponse = fundListResponse;
    this.brokerAuthInformation = brokerAuthInformation;
  }
  

}
