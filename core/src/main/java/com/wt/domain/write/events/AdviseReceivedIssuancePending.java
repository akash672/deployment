package com.wt.domain.write.events;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wt.domain.write.commands.IssueAdvice;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@EqualsAndHashCode(callSuper=false)
public class AdviseReceivedIssuancePending extends FundEvent {

  @NonNull
  @Getter
  private final String token;
  @Getter
  private final double allocation;
  @Getter
  private final double entryPrice;
  @Getter
  private final long entryTime;
  @Getter
  private final double exitPrice;
  @Getter
  private final long exitTime;
  @Getter
  @JsonIgnore
  private final long issueTime;
  @Getter
  private final String adviseId;
  @Getter
  private final String identityId;
  @Getter
  private final boolean isSpecialAdvise;
  
  private static final long serialVersionUID = 1L;

  @Deprecated
  public AdviseReceivedIssuancePending(String adviserUsername, String fundName, String token, double allocation, double entryPrice,
      long entryTime, double exitPrice, long exitTime, long issueTime, String adviseId, String identityId, boolean isSpecialAdvise) {
    super(adviserUsername, fundName);
    this.token = token;
    this.allocation = allocation;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.issueTime = issueTime;
    this.adviseId = adviseId;
    this.identityId = adviserUsername;
    this.isSpecialAdvise = isSpecialAdvise;
  }
  
  public AdviseReceivedIssuancePending(String adviserUsername, String fundName, String token, double allocation, double entryPrice,
      long entryTime, double exitPrice, long exitTime, long issueTime, String adviseId, boolean isSpecialAdvise) {
    super(adviserUsername, fundName);
    this.token = token;
    this.allocation = allocation;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.issueTime = issueTime;
    this.adviseId = adviseId;
    this.identityId = adviserUsername;
    this.isSpecialAdvise = isSpecialAdvise;
  }

  public AdviseReceivedIssuancePending(IssueAdvice issuedAdvise, boolean isSpecialAdvise) {
    super(issuedAdvise.getAdviserUsername(), issuedAdvise.getFundName());
    this.token = issuedAdvise.getToken();
    this.allocation = issuedAdvise.getAllocation();
    this.entryPrice = issuedAdvise.getEntryPrice();
    this.entryTime = issuedAdvise.getEntryTime();
    this.exitPrice = issuedAdvise.getExitPrice();
    this.exitTime = issuedAdvise.getExitTime();
    this.issueTime = issuedAdvise.getIssueTime();
    this.adviseId = issuedAdvise.getAdviseId();
    this.identityId = issuedAdvise.getUsername();
    this.isSpecialAdvise = isSpecialAdvise;
  }

  public IssueAdvice createCommand() {
    return new IssueAdvice(getAdviserUsername(), getFundName(), adviseId, token, allocation, entryPrice, entryTime, exitPrice, exitTime, issueTime);
  }

}
