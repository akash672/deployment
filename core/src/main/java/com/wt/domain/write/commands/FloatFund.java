package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wt.domain.RiskLevel;

import lombok.Getter;
import lombok.Setter;

public class FloatFund extends FundCommand {

  private static final long serialVersionUID = 1L;
  private @Getter double cash;
  private @Getter long startDate;
  private @Getter long endDate;
  private @Getter String description;
  private @Getter String theme;
  private @Getter double minSIP;
  private @Getter double minLumpSum;
  private @Getter @Setter String adviserUsername;
  private @Getter @Setter String adviserName;
  private @Getter RiskLevel riskLevel;

  @JsonCreator
  public FloatFund(@JsonProperty("username") String id, @JsonProperty("fundName") String fundName,
      @JsonProperty("startDate") long startDate, @JsonProperty("endDate") long endDate,
      @JsonProperty("description") String description, @JsonProperty("theme") String theme,
      @JsonProperty("minSIP") double minSIP, @JsonProperty("minLumpSum") double minLumSum,
      @JsonProperty("riskLevel") String riskLevel) {
    super(id, fundName);
    this.startDate = startDate;
    this.endDate = endDate;
    this.description = description;
    this.theme = theme;
    this.minSIP = minSIP;
    this.minLumpSum = minLumSum;
    this.riskLevel = RiskLevel.valueOf(riskLevel.toUpperCase());
  }
  
  public void validateCommand() {
    if (isNegativeMinimumLumpSum() || isNegativeMinimumSIP()) {
      throw new IllegalArgumentException("Minimum LumpSum or Minimum SIP cannot be less than zero");
    } else if (isMinimumLumpSumLessThanMinimumSIP()) {
      throw new IllegalArgumentException("Minimum LumpSum or Minimum SIP cannot be less than minimum SIP");
    } else if (isThemeBlank()) {
      throw new IllegalArgumentException("Theme cannot be left blank");
    }
  }

  private boolean isThemeBlank() {
    if(theme == null)
      return true;
    return theme.isEmpty();
  }

  private boolean isMinimumLumpSumLessThanMinimumSIP() {
    return minLumpSum < minSIP;
  }

  private boolean isNegativeMinimumLumpSum() {
    return minLumpSum <= 0;
  }

  private boolean isNegativeMinimumSIP() {
    return minSIP <= 0;
  }
}
