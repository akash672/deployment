package com.wt.domain.write.commands;

import java.util.List;

import com.wt.domain.FundConstituent;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SendSubscriptionForInstruments {

  private List<FundConstituent> fundConstituents;

}
