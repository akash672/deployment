package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@ToString
public class RootAdviserRegistered implements Serializable {
  private static final long serialVersionUID = 1L;

  @Getter private String identityID;
  @Getter private String adviserUsername;
  
  public RootAdviserRegistered(String identityID, String adviserUsername) {
    this.identityID = identityID;
    this.adviserUsername = adviserUsername;
  }
  
}

