package com.wt.domain.write.events;

import com.wt.aws.cognito.configuration.CredentialsProviderType;

import lombok.Getter;

@Getter
public class AdviserRegistered extends AdviserEvent {
  private static final long serialVersionUID = 1L;
  private String adviserName;
  private String email;
  private String identityID;
  private String publicId;
  private CredentialsProviderType type;

  public AdviserRegistered(String adviserUsername, String adviserName, String email, String identityID, long timestamp,
      CredentialsProviderType type) {
    super(adviserUsername);
    this.adviserName = adviserName;
    this.email = email;
    this.identityID = identityID;
    this.type = type;
  }
}
