package com.wt.domain.write.events;

import java.nio.ByteBuffer;

import com.wt.aws.cognito.configuration.CredentialsProviderType;

import lombok.Getter;

public class CognitoAdviserRegistered extends AdviserRegistered {
  private static final long serialVersionUID = 8883400090596267790L;
  @Getter
  private byte[] password;
  @Getter
  private byte[] salt;

  public CognitoAdviserRegistered(String adviserUsername,  String adviserName, String email, ByteBuffer password,
      ByteBuffer salt, String identityID, long timestamp) {
    this(adviserUsername, adviserName, email, password, salt, identityID, timestamp, CredentialsProviderType.Cognito);
  }
  
  public CognitoAdviserRegistered(String adviserUsername,  String adviserName,String email, ByteBuffer password,
      ByteBuffer salt, String identityID, long timestamp, CredentialsProviderType type) {
    super(adviserUsername, adviserName, email, identityID, timestamp, type);
    this.password = password.array();
    this.salt = salt.array();
  }
}
