package com.wt.domain.write.events;

import java.io.Serializable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ResetActiveInstruments implements Serializable {

  private static final long serialVersionUID = 1L;

}
