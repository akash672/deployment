package com.wt.domain.write.events;

import com.wt.domain.FundConstituent;

import lombok.Getter;

public class UpdateFundConsituentsAtEOD extends FundEvent {

  private static final long serialVersionUID = 1L;
  @Getter private FundConstituent fundConstituent = new FundConstituent();
  
  public UpdateFundConsituentsAtEOD(String adviserUsername, String fundName, FundConstituent fundConstituent) {
    super(adviserUsername, fundName);
    this.fundConstituent = fundConstituent;
  }

}
