package com.wt.domain.write.events;

import static com.wt.domain.PriceWithPaf.noPrice;
import static com.wt.utils.DateUtils.currentTimeInMillis;

import com.wt.domain.PriceWithPaf;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
public class FundAdviceIssued extends AdviseEvent {
  private static final long serialVersionUID = 1L;
  private String token;
  @Setter private String symbol;
  private double allocation;
  private double absoluteAllocation;
  private long issueTime;
  private PriceWithPaf entryPrice;
  private long entryTime;
  private PriceWithPaf exitPrice;
  private long exitTime;
  @SuppressWarnings("unused")
  private String identityId;
  private boolean isSpecialAdvise;
  
  @Deprecated
  public String getIdentityId() {
    return this.getAdviserUsername();
  }

  @Deprecated
  public FundAdviceIssued(String adviserUsername, String fundName, String adviseId, String token, String symbol,
      double allocation, double absoluteAllocation, long issueTime, PriceWithPaf entryPrice, long entryTime,
      PriceWithPaf exitPrice, long exitTime, String identityId, boolean isSpecialAdvise) {
    super(adviserUsername, fundName, adviseId);
    this.token = token;
    this.symbol = symbol;
    this.allocation = allocation;
    this.absoluteAllocation = absoluteAllocation;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.isSpecialAdvise = isSpecialAdvise;
  }

  @Deprecated
  public FundAdviceIssued(String adviserUsername, String fundName, String adviseId, String token,
      double allocation, double absoluteAllocation, long issueTime, PriceWithPaf entryPrice, long entryTime,
      PriceWithPaf exitPrice, long exitTime, String identityId) {
    super(adviserUsername, fundName, adviseId);
    this.token = token;
    this.symbol = token;
    this.allocation = allocation;
    this.absoluteAllocation = absoluteAllocation;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.isSpecialAdvise = false;
  }
  
  public FundAdviceIssued(String adviserUsername, String fundName, String adviseId, String token, String symbol,
      double allocation, double absoluteAllocation, long issueTime, PriceWithPaf entryPrice, long entryTime,
      PriceWithPaf exitPrice, long exitTime,boolean isSpecialAdvise) {
    super(adviserUsername, fundName, adviseId);
    this.token = token;
    this.symbol = symbol;
    this.allocation = allocation;
    this.absoluteAllocation = absoluteAllocation;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.isSpecialAdvise = isSpecialAdvise;
  }

  public FundAdviceIssued(String adviserUsername, String fundName, String adviseId, String token,
      double allocation, double absoluteAllocation, long issueTime, PriceWithPaf entryPrice, long entryTime,
      PriceWithPaf exitPrice, long exitTime) {
    super(adviserUsername, fundName, adviseId);
    this.token = token;
    this.symbol = token;
    this.allocation = allocation;
    this.absoluteAllocation = absoluteAllocation;
    this.issueTime = issueTime;
    this.entryPrice = entryPrice;
    this.entryTime = entryTime;
    this.exitPrice = exitPrice;
    this.exitTime = exitTime;
    this.isSpecialAdvise = false;
  }
  public static FundAdviceIssued withoutExitParameters(String adviserUsername, String fundName, String adviseId,
      String token, String symbol, double allocation, double absoluteAllocation, long issueTime, PriceWithPaf entryPrice,
      long entryTime, boolean isSpecialAdvise) {
    return new FundAdviceIssued(adviserUsername, fundName, adviseId, token, symbol, allocation, absoluteAllocation, issueTime,
        entryPrice, entryTime, noPrice(token), currentTimeInMillis(), isSpecialAdvise);
  }
  
}
