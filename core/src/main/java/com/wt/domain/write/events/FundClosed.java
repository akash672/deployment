package com.wt.domain.write.events;

import lombok.Getter;
import lombok.ToString;

@ToString
public class FundClosed extends FundEvent {
  private static final long serialVersionUID = 1L;
  @Getter
  private final long endDate;

  public FundClosed(String adviserUsername, String fundName, long endDate) {
    super(adviserUsername, fundName);
    this.endDate = endDate;
  }
}
