package com.wt.domain.write.commands;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class AdviserCorporateEventSell extends AdviserCorporateEvents{

  private static final long serialVersionUID = 1L;
  private String token;
  private double price;
  
  
  public AdviserCorporateEventSell(@JsonProperty("token")String token, 
      @JsonProperty("price")double price) {
    this.token = token;
    this.price = price;
  }

  
}
