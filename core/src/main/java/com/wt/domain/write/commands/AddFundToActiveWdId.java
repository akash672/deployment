package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor
public class AddFundToActiveWdId implements Serializable{
   
  private static final long serialVersionUID = 1L;
  private String wdId;  
  private String fundNameWithAdviserUserName;
}
