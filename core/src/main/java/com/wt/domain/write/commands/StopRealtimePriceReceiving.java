package com.wt.domain.write.commands;

import java.io.Serializable;

import lombok.Getter;

@Getter
public class StopRealtimePriceReceiving implements Serializable {

  String wdId;
  String connectionId;
  
  public StopRealtimePriceReceiving(String wdId, String connectionId) {
    this.wdId = wdId;
    this.connectionId = connectionId;
  }

  private static final long serialVersionUID = 1L;

}
