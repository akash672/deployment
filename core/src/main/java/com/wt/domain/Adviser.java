package com.wt.domain;

import static com.google.common.collect.ImmutableList.copyOf;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.common.collect.ImmutableList;
import com.wt.domain.write.events.AdviserEvent;
import com.wt.domain.write.events.AdviserPasswordChanged;
import com.wt.domain.write.events.AdviserRegistered;
import com.wt.domain.write.events.CognitoAdviserRegistered;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "adviser")
@Entity
@NoArgsConstructor
@ToString(callSuper=true)
public class Adviser extends CurrentAdviserDb implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter
  @Setter
  private byte[] password;
  @Getter
  @Setter
  private byte[] salt;
  @Getter
  private String accessToken;
  @Getter
  private boolean registered;
  @Getter
  @Setter
  private long registeredTimestamp;


  public Adviser(String adviserUsername, String adviserName, String email, String publicId) {
    super(adviserUsername, adviserName, email, publicId);
  }

  public void update(AdviserEvent evt) {
    super.update(evt);
    if (evt instanceof CognitoAdviserRegistered) {
      CognitoAdviserRegistered registerEvent = (CognitoAdviserRegistered) evt;
      password = (registerEvent.getPassword());
      salt = (registerEvent.getSalt());
    } else if (evt instanceof AdviserPasswordChanged) {
      AdviserPasswordChanged event = (AdviserPasswordChanged) evt;
      this.salt = event.getSalt();
      this.password = event.getPassword();
    }
  }

  protected void setParams(AdviserRegistered registerEvent) {
    super.setParams(registerEvent);
    registered = true;
    registeredTimestamp = registerEvent.getTimestamp();
  }

  public boolean hasFund(String fundName) {
    return getFunds().contains(fundName);
  }

  public ImmutableList<String> allFunds() {
    return copyOf(super.getFunds());
  }

  
  public String getDeviceToken() {
    return "NODEVICETOKENIMPLEMENTED";
  }

}
