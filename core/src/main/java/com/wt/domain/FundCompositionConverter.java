package com.wt.domain;

import java.lang.reflect.Type;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class FundCompositionConverter implements DynamoDBTypeConverter<String, Map<String, FundConstituent>> {

  @Override
  public String convert(Map<String, FundConstituent> object) {
    return new Gson().toJson(object);
  }

  @Override
  public Map<String, FundConstituent> unconvert(String object) {
    Type mapType = new TypeToken<Map<String, FundConstituent>>(){}.getType(); 
    return new Gson().fromJson(object, mapType);
  }
}
