package com.wt.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class FundConstituentKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @DynamoDBHashKey
  private String adviseId;

  @DynamoDBRangeKey
  private String fundWithAdviser;

  public FundConstituentKey(String adviseId, String fundName, String adviserUsername) {
    super();
    this.adviseId = adviseId;
    this.fundWithAdviser = fundName + CompositeKeySeparator + adviserUsername;
  }

  @JsonIgnore
  public String getAdviser() {
    return fundWithAdviser.split(CompositeKeySeparator)[1];
  }

  @JsonIgnore
  public String getFund() {
    return fundWithAdviser.split(CompositeKeySeparator)[0];
  }

}