package com.wt.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.wt.domain.write.events.AdviserEvent;
import com.wt.domain.write.events.AdviserFundClosed;
import com.wt.domain.write.events.AdviserFundFloated;
import com.wt.domain.write.events.AdviserRegistered;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Entity
@NoArgsConstructor
@ToString
@DynamoDBTable(tableName = "Adviser")
@Log4j
public class CurrentAdviserDb implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Getter
  @Setter
  @DynamoDBHashKey
  private String adviserUsername;

  @Getter
  @Setter
  @DynamoDBAttribute
  private String email;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String adviserName;
  @Getter
  @Setter
  @DynamoDBAttribute
  private String publicId;


  @Getter
  @Setter
  @DynamoDBTypeConverted(converter = FundNameMarshaller.class)
  private Set<String> funds = new HashSet<>();

  public CurrentAdviserDb(String adviserUsername, String adviserName, String email, String publicId) {
    super();
    this.adviserUsername = adviserUsername;
    this.adviserName = adviserName;
    this.email = email;
    this.publicId = publicId;
  }

  public void update(AdviserEvent evt) {
    if (evt instanceof AdviserRegistered) {
        AdviserRegistered registerEvent = (AdviserRegistered) evt;
        setParams(registerEvent);
    } else if (evt instanceof AdviserFundFloated) {
      if (((AdviserFundFloated) evt).getFundName().contains(" ")) {
        log.error("Skipping " + ((AdviserFundFloated) evt).getFundName() + " as it contains spaces.");
      } else {
        funds.add(((AdviserFundFloated) evt).getFundName());
      }
    } else if (evt instanceof AdviserFundClosed) {
      funds.remove(((AdviserFundClosed) evt).getFundName());
    } 
  }

  protected void setParams(AdviserRegistered registerEvent) {
    adviserName = registerEvent.getAdviserName();
    adviserUsername = registerEvent.getAdviserUsername();
    email = registerEvent.getEmail();
  }
}
