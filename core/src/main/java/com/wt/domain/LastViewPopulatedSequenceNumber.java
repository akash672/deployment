package com.wt.domain;

import java.io.Serializable;

import javax.persistence.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "Sequence_Store")
public class LastViewPopulatedSequenceNumber implements Serializable{
	private static final long serialVersionUID = 1L;
	@DynamoDBHashKey @Id @Getter @Setter
	private String persistenceId;
	@DynamoDBAttribute @Getter @Setter
	private long seqNum;
}
