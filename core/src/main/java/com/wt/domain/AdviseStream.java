package com.wt.domain;

import com.wt.utils.akka.CancellableStream;

import akka.actor.ActorRef;
import akka.stream.javadsl.Source;

public class AdviseStream<Out, Mat> extends CancellableStream<Out, Mat> {

  private CancellableStream<PriceWithPaf, Mat> priceStream;

  public AdviseStream(Source<Out, Mat> source, ActorRef actor, CancellableStream<PriceWithPaf, Mat> priceStream) {
    super(source, actor);
    this.priceStream = priceStream;
  }

  public void cancel() {
    super.cancel();
    priceStream.cancel();
  }
}
