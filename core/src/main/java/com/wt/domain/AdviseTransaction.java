package com.wt.domain;

import javax.persistence.Id;
import javax.persistence.Transient;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class AdviseTransaction {
  @Id
  @DynamoDBHashKey
  @Getter
  @Setter
  private String id;

  @DynamoDBAttribute
  private String adviseId;

  @DynamoDBAttribute
  @Getter
  private double allocation;

  @DynamoDBAttribute
  @Getter
  private long exitTime;

  @DynamoDBAttribute
  @Getter
  @DynamoDBTypeConverted(converter = PriceMarshaller.class)
  private PriceWithPaf exitPrice;

  @DynamoDBAttribute
  private double performance;

  @Transient
  @DynamoDBTypeConverted(converter = PriceMarshaller.class)
  private PriceWithPaf entryPrice;

  public AdviseTransaction() {
    super();
  }

  public AdviseTransaction(String adviseId, double allocation, long exitTime, PriceWithPaf exitPrice,
      PriceWithPaf entryPrice) {
    super();
    this.adviseId = adviseId;
    this.allocation = allocation;
    this.exitTime = exitTime;
    this.exitPrice = exitPrice;
    this.entryPrice = entryPrice;
    this.performance = allocation * entryPrice.performanceWith(exitPrice);
  }

  public boolean onOrBefore(long date) {
    return exitTime <= date;
  }

  public double getPerformance() {
    return allocation * entryPrice.performanceWith(exitPrice);
  }

}
