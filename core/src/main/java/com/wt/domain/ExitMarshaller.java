package com.wt.domain;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class ExitMarshaller implements DynamoDBTypeConverter<String, List<AdviseTransaction>> {

  @Override
  public String convert(List<AdviseTransaction> object) {
    if (object instanceof AdviseTransaction) {
      AdviseTransaction adviseTransaction = (AdviseTransaction) object;
      return new Gson().toJson(adviseTransaction);
    } else if (object instanceof Collection<?>) {
      List<AdviseTransaction> setOfuserResponseParameterss = (List<AdviseTransaction>) object;
      return new Gson().toJson(setOfuserResponseParameterss);
    }
    return new Gson().toJson(object);  }

  @Override
  public List<AdviseTransaction> unconvert(String object) {
    List<AdviseTransaction> fromDb;
    try {

      fromDb = new Gson().fromJson(object, new TypeToken<List<AdviseTransaction>>() {

        private static final long serialVersionUID = 1L;
      }.getType());
      fromDb = new CopyOnWriteArrayList<AdviseTransaction>(fromDb);
    } catch (JsonSyntaxException e) {
      fromDb = new Gson().fromJson(object, new TypeToken<AdviseTransaction>() {

        private static final long serialVersionUID = 1L;
      }.getType());
    }
    return fromDb;
  }
}
