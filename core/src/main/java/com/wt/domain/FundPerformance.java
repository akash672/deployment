
package com.wt.domain;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "fund_performance")
@Entity
@NoArgsConstructor
@DynamoDBTable(tableName = "Fund_Performance")
public class FundPerformance implements Serializable, Performance {

  private static final long serialVersionUID = 1L;

  @org.springframework.data.annotation.Id
  @DynamoDBIgnore
  private FundPerformanceKey fundPerformanceKey = new FundPerformanceKey();

  @Getter
  @Setter
  @DynamoDBAttribute
  private double realizedMTM;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double unrealizedMTM;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double performance;

  @Getter
  @Setter
  @DynamoDBAttribute
  private double cash;

  public FundPerformance(String fund, String adviserUsername,long currentTime, double realizedMTM, double unrealizedMTM,
      double cumulativePerformance, double cash) {
    super();
    fundPerformanceKey.setFund(fund);
    fundPerformanceKey.setDate(currentTime);
    fundPerformanceKey.setAdviserUsername(adviserUsername);
    this.realizedMTM = realizedMTM;
    this.unrealizedMTM = unrealizedMTM;
    this.performance = cumulativePerformance;
    this.cash = cash;
  }

  @DynamoDBHashKey(attributeName = "fund")
  public String getFund() {
    return fundPerformanceKey.getFund();
  }

  public void setFund(String fund) {
    fundPerformanceKey.setFund(fund);
  }

  @DynamoDBRangeKey(attributeName = "date")
  public long getDate() {
    return fundPerformanceKey.getDate();
  }

  public void setDate(long date) {
    fundPerformanceKey.setDate(date);
  }
  
  public String getAdviserUsername() {
    return fundPerformanceKey.getAdviserUsername();
  }
  
  public void setAdviserUsername(String adviserUsername) {
    fundPerformanceKey.setAdviserUsername(adviserUsername);
  }

  @Override
  public String toString() {
    return "FundPerformance(date=" + getDate() + ", fund=" + getFund() + ", adviserUsername=" + getAdviserUsername()
        + ", realizedMTM=" + this.realizedMTM + ", unrealizedMTM=" + this.unrealizedMTM + ", performance="
        + this.performance + ", cash=" + this.cash + ")";
  }

  public static Comparator<FundPerformance> FundPerformanceDatewise = new Comparator<FundPerformance>() {

    public int compare(FundPerformance fundPerformance1, FundPerformance fundPerformance2) {

      long date1 = fundPerformance1.getDate();
      long date2 = fundPerformance2.getDate();
      
      return Long.compare(date1, date2); 
    }
  };
}