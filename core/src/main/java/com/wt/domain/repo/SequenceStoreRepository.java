package com.wt.domain.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.LastViewPopulatedSequenceNumber;

@EnableScan
public interface SequenceStoreRepository extends CrudRepository<LastViewPopulatedSequenceNumber, String>{

}
