package com.wt.domain.repo;

import org.springframework.data.repository.CrudRepository;

import com.wt.domain.FundPerformanceSnapshot;

public interface FundPerformanceSnapshotRepository extends CrudRepository<FundPerformanceSnapshot, String>{

}
