package com.wt.domain.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.Adviser;
@EnableScan
public interface FundAdviserRepository extends CrudRepository<Adviser, String> {}
