package com.wt.domain.repo;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wt.domain.FundPerformance;
import com.wt.domain.FundPerformanceKey;

@EnableScan
public interface FundPerformanceRepository extends CrudRepository<FundPerformance, FundPerformanceKey> {

  public List<FundPerformance> findByFund(String fund);

}