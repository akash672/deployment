package com.wt.domain;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.wt.utils.DoublesUtil;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@EqualsAndHashCode
@ToString
public class MaxDrawdown implements Serializable {
  private static final long serialVersionUID = 1L;
  @Getter
  private double maxDrawdown = 0.0d;
  @Getter
  private double returns = 0.0d;
  @Getter
  private String period;

  public MaxDrawdown() {
    super();
  }

  public MaxDrawdown(String period, double maxDrawdown, double returns) {
    this.period = period;
    this.maxDrawdown = maxDrawdown;
    this.returns = returns;
  }

  private static MaxDrawdown empty() {
    return new MaxDrawdown("", 0.0d, 0.0d);
  }

  public static MaxDrawdown from(String period, long dateFromToday, List<? extends Performance> fullDataSet) {
    if (fullDataSet.isEmpty())
      return empty();

    return forPeriod(period, dateFromToday, fullDataSet);
  }

  private static MaxDrawdown forPeriod(String period, long date, List<? extends Performance> fullDataSet) {
    List<? extends Performance> performanceForPeriod = fullDataSet.stream()
        .filter(performance -> periodStartDate(date, fullDataSet) <= performance.getDate())
        .collect(Collectors.toList());

    return calculate(period, performanceForPeriod);
  }

  private static long periodStartDate(long date, List<? extends Performance> fullDataSet) {
    long seriesStartDate = fullDataSet.get(0).getDate();

    if (date < seriesStartDate)
      return seriesStartDate;

    return date;
  }

  private static MaxDrawdown calculate(String period, List<? extends Performance> fundPerformance) {
    double maxInRange = 0.0d;
    double returns = 0.0d;
    double drawDown = 0.0d;
    double maxDrawdown = 0.0d;
    double lastDayReturn = 0d;
    double firstDayReturn = 0d;
    long inceptionDate = 0L;
    
    for(Performance performance : fundPerformance) {
      if(inceptionDate == 0)
        inceptionDate = performance.getDate();
      if(inceptionDate > performance.getDate()) {
        inceptionDate = performance.getDate();
      }
    }

    Performance lastItem = null;
    for (Performance performance : fundPerformance) {
      lastItem = (lastItem == null) ? performance : lastItem; 
      if (performance.getDate() > lastItem.getDate()) {
        lastDayReturn = performance.getPerformance();
      }
      if (performance.getDate() < lastItem.getDate() || performance.getDate() == inceptionDate) {
        firstDayReturn = performance.getPerformance();
      }
      if (performance.getPerformance() > maxInRange) {
        maxInRange = performance.getPerformance();
      }
      drawDown = maxInRange - performance.getPerformance();
      if (drawDown > maxDrawdown) {
        maxDrawdown = drawDown;
      }
      lastItem = performance;
    }

    maxDrawdown = DoublesUtil.round(maxDrawdown);
    returns = DoublesUtil.round(lastDayReturn - firstDayReturn);
    return new MaxDrawdown(period, maxDrawdown, returns);
  }
}