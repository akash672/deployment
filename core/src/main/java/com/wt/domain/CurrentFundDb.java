package com.wt.domain;

import static com.wt.utils.DoublesUtil.isZero;
import static com.wt.utils.DoublesUtil.round;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundAttributeUpdate;
import com.wt.domain.write.events.FundClosed;
import com.wt.domain.write.events.FundEvent;
import com.wt.domain.write.events.FundFloated;
import com.wt.domain.write.events.UpdateFundCashAtEOD;
import com.wt.domain.write.events.UpdateFundConsituentsAtEOD;
import com.wt.domain.write.events.UpdatingAdviseWdIds;
import com.wt.utils.DoublesUtil;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Table(name = "fund")
@Entity
@DynamoDBTable(tableName = "Fund")
@Log4j
public class CurrentFundDb implements Serializable {
  private static final long serialVersionUID = 1L;
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = FundCompositionConverter.class)
  private Map<String, FundConstituent> tickersWithComposition = new HashMap<>();
  @DynamoDBAttribute
  private String adviserUsername;
  @DynamoDBAttribute
  private String adviserName;
  @DynamoDBHashKey
  @Id
  private String fundName;
  @DynamoDBAttribute
  private long startDate;
  @DynamoDBAttribute
  private long endDate;
  @DynamoDBAttribute
  private double cash;
  @DynamoDBAttribute
  private String description;
  @DynamoDBAttribute
  private String theme;
  @DynamoDBAttribute
  private double minSIP;
  @DynamoDBAttribute
  private double minLumpSum;
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = DrawdownsConverter.class)
  private List<MaxDrawdown> drawDowns;
  @DynamoDBAttribute
  @DynamoDBTypeConvertedEnum
  public RiskLevel riskLevel;
  @Getter
  @Setter
  @DynamoDBAttribute
  @DynamoDBTypeConverted(converter = AdviseConverter.class)
  private Map<String, Advise> advisesWithIds = new HashMap<>();

  public CurrentFundDb(String advisorUsername, String adviserName, String fund, long startDate, long endDate, String description, String theme, double minSIP, double minLumpSum, RiskLevel riskLevel) {
    this.adviserUsername = advisorUsername;
    this.adviserName = adviserName;
    this.fundName = fund;
    this.startDate = startDate;
    this.endDate = endDate;
    this.description = description;
    this.theme = theme;
    this.minSIP = minSIP;
    this.minLumpSum = minLumpSum;
    this.riskLevel = riskLevel;
  }

  public CurrentFundDb(String advisorUsername, String adviserName, String fund, long startDate, long endDate, String description, double minSIP, double minLumpSum, RiskLevel riskLevel) {
    this.adviserUsername = advisorUsername;
    this.adviserName = adviserName;
    this.fundName = fund;
    this.startDate = startDate;
    this.endDate = endDate;
    this.description = description;
    this.theme = "LongTerm";
    this.minSIP = minSIP;
    this.minLumpSum = minLumpSum;
    this.riskLevel = riskLevel;
  }

  public void update(FundEvent evt) {
    if (evt instanceof FundFloated) {
      fundFloated(evt);
    } else if (evt instanceof FundAttributeUpdate) {
      updateFundAttributes(evt);
    } else if (evt instanceof FundAdviceIssued) {
      adviseIssued((FundAdviceIssued) evt);
    } else if (evt instanceof FundAdviceClosed) {
      adviseClosed(evt);
    } else if (evt instanceof UpdateFundConsituentsAtEOD) {
//      updatingFundConstituents((UpdateFundConsituentsAtEOD) evt);
    } else if (evt instanceof UpdateFundCashAtEOD) {
      //updateFundCashAtEOD((UpdateFundCashAtEOD) evt);
    } else if (evt instanceof FundClosed) {
      fundClosed(evt);
    } else if (evt instanceof UpdatingAdviseWdIds) {
      handleWdIdUpdationForAdviseIds((UpdatingAdviseWdIds) evt);
    }
  }

  protected void handleWdIdUpdationForAdviseIds(UpdatingAdviseWdIds evt) {
    UpdatingAdviseWdIds updatingAdviseWdIds = (UpdatingAdviseWdIds) evt;
    Map<String, String> updatedAdviseWdIds = updatingAdviseWdIds.getAdivseIdToUpdatedWdIdMap();
    for (String adviseId : updatedAdviseWdIds.keySet()) {
      if (tickersWithComposition.containsKey(adviseId)) {
        tickersWithComposition.get(adviseId).setTicker(updatedAdviseWdIds.get(adviseId));
      }
    }
    for (String adviseId : updatedAdviseWdIds.keySet()) {
      if (advisesWithIds.containsKey(adviseId)) {
        log.info("WdId updated for advise " + adviseId + " in fund " + getFundName());
        advisesWithIds.get(adviseId).setTicker(updatedAdviseWdIds.get(adviseId));
      }
    }
  }

  protected void adviseIssued(FundEvent evt) {
    FundAdviceIssued issuedAdvice = (FundAdviceIssued) evt;
    Advise updatedAdvise = updatedAdvise(issuedAdvice);
    advisesWithIds.put(updatedAdvise.getId(), updatedAdvise);
    if (!issuedAdvice.isSpecialAdvise())
      cash = round(cash - DoublesUtil.round(issuedAdvice.getAllocation()));
    tickersWithComposition.put(issuedAdvice.getAdviseId(),
        new FundConstituent(issuedAdvice.getAdviseId(), issuedAdvice.getFundName(), adviserUsername,
            issuedAdvice.getToken(), updatedAdvise.getRemainingAllocation(), updatedAdvise.getEntryPrice(),
            updatedAdvise.getRecoExitPrice()));
  }

  private Advise updatedAdvise(FundAdviceIssued issuedAdvice) {
    Advise advise = Advise.withRecommendedSameAsActual(issuedAdvice.getAdviseId(), issuedAdvice.getAdviserUsername(),
        issuedAdvice.getFundName(), issuedAdvice.getToken(), issuedAdvice.getAllocation(), issuedAdvice.getIssueTime(),
        issuedAdvice.getEntryTime(), issuedAdvice.getEntryPrice(), issuedAdvice.getExitTime(),
        issuedAdvice.getExitPrice());
   if(adviseAlreadyExists(issuedAdvice.getAdviseId())) {  
      advise = advisesWithIds.get(issuedAdvice.getAdviseId());
      advise.updateExistingAdvise(issuedAdvice);
   } else {
      advise.update(issuedAdvice);
   }
   return advise;
  }

  private boolean adviseAlreadyExists(String adviseId) {
    return advisesWithIds.containsKey(adviseId);
  }

  protected void updateFundAttributes(FundEvent evt) {
    FundAttributeUpdate fundAttribute = (FundAttributeUpdate) evt;

    if (fundAttribute.getStartDate() != 0)
      setStartDate(fundAttribute.getStartDate());
    if (fundAttribute.getEndDate() != 0)
      setEndDate(fundAttribute.getEndDate());
    if (!fundAttribute.getDescription().isEmpty())
      setDescription(fundAttribute.getDescription());
    if (!fundAttribute.getTheme().isEmpty())
      setTheme(fundAttribute.getTheme());
    if (!isZero(fundAttribute.getMinSIP()))
      setMinSIP(fundAttribute.getMinSIP());
    if (!isZero(fundAttribute.getMinLumpSum()))
      setMinLumpSum(fundAttribute.getMinLumpSum());
    if (fundAttribute.getRiskLevel() != null)
      setRiskLevel(fundAttribute.getRiskLevel());
  }

  protected void fundFloated(FundEvent evt) {
    FundFloated fundFloated = (FundFloated) evt;

    setAdviserUsername(fundFloated.getAdviserUsername());
    setAdviserName(fundFloated.getAdviserName());
    setFundName(fundFloated.getFundName());
    setStartDate(fundFloated.getStartDate());
    setEndDate(fundFloated.getEndDate());
    setDescription(fundFloated.getDescription());
    setTheme(fundFloated.getTheme());
    setMinSIP(fundFloated.getMinSIP());
    setMinLumpSum(fundFloated.getMinLumpSum());
    setRiskLevel(fundFloated.getRiskLevel());
    this.cash = 100.;
  }

  protected void fundClosed(FundEvent evt) {
    FundClosed fundClosed = (FundClosed) evt;
    setEndDate(fundClosed.getEndDate());
  }

  protected void adviseClosed(FundEvent evt) {

    FundAdviceClosed adviceClosed = (FundAdviceClosed) evt;
    if (!adviceClosed.isSpecialAdvise())
      cash = DoublesUtil.round(cash + DoublesUtil.round(adviceClosed.getAllocation()));
    if (advisesWithIds.get(adviceClosed.getAdviseId()) != null) {
      advisesWithIds.get(((FundAdviceClosed) evt).getAdviseId()).update(evt);
    }
    if (adviceClosed.getAbsoluteAllocation() >= 100.) {
      tickersWithComposition.remove(adviceClosed.getAdviseId());
      advisesWithIds.remove(adviceClosed.getAdviseId());
    }
    else {
      FundConstituent oldFundConstituent = tickersWithComposition.get(adviceClosed.getAdviseId());
      Advise advise = advisesWithIds.get(adviceClosed.getAdviseId());
      tickersWithComposition.put(adviceClosed.getAdviseId(),
          new FundConstituent(oldFundConstituent.getFundConstituentKey(), oldFundConstituent.getTicker(),
              (oldFundConstituent.getAllocation() - adviceClosed.getAllocation()), oldFundConstituent.getEntryPrice(),
              advise.getExitPrice()));
    }
    
  }

  protected void updatingFundConstituents(UpdateFundConsituentsAtEOD updateEODFundConstituent) {
    if (updateEODFundConstituent.getFundConstituent().getAllocation() > 0.0) {
      tickersWithComposition.put(updateEODFundConstituent.getFundConstituent().getAdviseId(), updateEODFundConstituent.getFundConstituent());
    } else if (tickersWithComposition.containsKey(updateEODFundConstituent.getFundConstituent().getAdviseId())) {
      tickersWithComposition.remove(updateEODFundConstituent.getFundConstituent().getAdviseId());
    }
  }

  @java.lang.Override
  @java.lang.SuppressWarnings("all")
  public java.lang.String toString() {
    return "CurrentFundDb(tickersWithComposition=" + this.getTickersWithComposition() + ", adviserUsername=" + this.getAdviserUsername() + ", adviserName=" + this.getAdviserName() + ", fundName=" + this.getFundName() + ", startDate=" + this.getStartDate() + ", endDate=" + this.getEndDate() + ", cash=" + this.getCash() + ", description=" + this.getDescription() + ", theme=" + this.getTheme() + ", minSIP=" + this.getMinSIP() + ", minLumpSum=" + this.getMinLumpSum() + ", drawDowns=" + this.getDrawDowns() + ", riskLevel=" + this.getRiskLevel() + ")";
  }

  @java.lang.SuppressWarnings("all")
  public CurrentFundDb() {
  }

  @java.lang.SuppressWarnings("all")
  public Map<String, FundConstituent> getTickersWithComposition() {
    return this.tickersWithComposition;
  }

  @java.lang.SuppressWarnings("all")
  public void setTickersWithComposition(final Map<String, FundConstituent> tickersWithComposition) {
    this.tickersWithComposition = tickersWithComposition;
  }

  @java.lang.SuppressWarnings("all")
  public String getAdviserUsername() {
    return this.adviserUsername;
  }

  @java.lang.SuppressWarnings("all")
  public void setAdviserUsername(final String adviserUsername) {
    this.adviserUsername = adviserUsername;
  }

  @java.lang.SuppressWarnings("all")
  public String getAdviserName() {
    return this.adviserName;
  }

  @java.lang.SuppressWarnings("all")
  public void setAdviserName(final String adviserName) {
    this.adviserName = adviserName;
  }

  @java.lang.SuppressWarnings("all")
  public String getFundName() {
    return this.fundName;
  }

  @java.lang.SuppressWarnings("all")
  public void setFundName(final String fundName) {
    this.fundName = fundName;
  }

  @java.lang.SuppressWarnings("all")
  public long getStartDate() {
    return this.startDate;
  }

  @java.lang.SuppressWarnings("all")
  public void setStartDate(final long startDate) {
    this.startDate = startDate;
  }

  @java.lang.SuppressWarnings("all")
  public long getEndDate() {
    return this.endDate;
  }

  @java.lang.SuppressWarnings("all")
  public void setEndDate(final long endDate) {
    this.endDate = endDate;
  }

  @java.lang.SuppressWarnings("all")
  public double getCash() {
    return this.cash;
  }

  @java.lang.SuppressWarnings("all")
  public void setCash(final double cash) {
    this.cash = cash;
  }

  @java.lang.SuppressWarnings("all")
  public String getDescription() {
    return this.description;
  }

  @java.lang.SuppressWarnings("all")
  public void setDescription(final String description) {
    this.description = description;
  }

  @java.lang.SuppressWarnings("all")
  public String getTheme() {
    return this.theme;
  }

  @java.lang.SuppressWarnings("all")
  public void setTheme(final String theme) {
    this.theme = theme;
  }

  @java.lang.SuppressWarnings("all")
  public double getMinSIP() {
    return this.minSIP;
  }

  @java.lang.SuppressWarnings("all")
  public void setMinSIP(final double minSIP) {
    this.minSIP = minSIP;
  }

  @java.lang.SuppressWarnings("all")
  public double getMinLumpSum() {
    return this.minLumpSum;
  }

  @java.lang.SuppressWarnings("all")
  public void setMinLumpSum(final double minLumpSum) {
    this.minLumpSum = minLumpSum;
  }

  @java.lang.SuppressWarnings("all")
  public List<MaxDrawdown> getDrawDowns() {
    return this.drawDowns;
  }

  @java.lang.SuppressWarnings("all")
  public void setDrawDowns(final List<MaxDrawdown> drawDowns) {
    this.drawDowns = drawDowns;
  }

  @java.lang.SuppressWarnings("all")
  public RiskLevel getRiskLevel() {
    return this.riskLevel;
  }

  @java.lang.SuppressWarnings("all")
  public void setRiskLevel(final RiskLevel riskLevel) {
    this.riskLevel = riskLevel;
  }
}
