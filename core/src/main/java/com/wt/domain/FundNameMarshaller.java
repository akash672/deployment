package com.wt.domain;

import java.util.Set;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;

public class FundNameMarshaller implements DynamoDBTypeConverter<String, Set<String>> {

  private Gson gson = new Gson();

  @Override
  public String convert(Set<String> object) {
    return gson.toJson(object);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Set<String> unconvert(String object) {
    return gson.fromJson(object, Set.class);
  }

}
