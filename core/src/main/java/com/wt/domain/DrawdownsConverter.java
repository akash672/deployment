  package com.wt.domain;

  import static java.util.Arrays.asList;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;

  public class DrawdownsConverter implements DynamoDBTypeConverter<String,List<MaxDrawdown>> {

    @Override
    public String convert(List<MaxDrawdown> object) {
      return new Gson().toJson(object);
    }

    @Override
    public List<MaxDrawdown> unconvert(String object) {
      return asList(new Gson().fromJson(object, MaxDrawdown[].class));
    }
  }
