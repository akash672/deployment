package com.wt.domain;

public interface Performance {

  public static final long serialVersionUID = 1L;
  
  public double getPerformance();
  
  public long getDate();

}
