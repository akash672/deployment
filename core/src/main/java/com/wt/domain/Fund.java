package com.wt.domain;

import static com.wt.domain.FundConstituent.withActiveAdvise;
import static java.util.Collections.unmodifiableCollection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.events.AdviseDetails;
import com.wt.domain.write.events.AdvisePerformanceCalculated;
import com.wt.domain.write.events.CloseCorporateEventAdvise;
import com.wt.domain.write.events.FundEvent;
import com.wt.domain.write.events.FundPerformanceCalculated;
import com.wt.domain.write.events.FundPerformanceCalculated.FundPerformanceCalculatedBuilder;
import com.wt.domain.write.events.FundPerformancePersisted;
import com.wt.domain.write.events.PriceEventsList;
import com.wt.domain.write.events.RecommendedEntryPriceUpdated;
import com.wt.domain.write.events.StartHistoricalFundPerformance;
import com.wt.domain.write.events.UpdateAdviseDetailsInCorpActionsMap;
import com.wt.domain.write.events.UpdateSecondaryBuyCashRatioInMap;
import com.wt.domain.write.events.UpdatingAdviseWdIds;

import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Log4j
@Entity
@ToString(callSuper=true)
@NoArgsConstructor
public class Fund extends CurrentFundDb implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Getter
  @Transient
  private Map<String, AdviseDetails> corpActionTokenToAdviseDetails = new HashMap<>();
  
  @Transient
  @Setter
  @Getter
  private Map<String, List<PriceWithPaf>> priceListForPerformanceCalculation;

  @Getter
  private long performanceCalculatedTill;

  @Getter
  private String topicARN;

  public Fund(String advisorUsername, String adviserName, String fund, long startDate, long endDate, String description, String theme,

      double minSIP, double minLumpSum, RiskLevel riskLevel) {
    super(advisorUsername, adviserName, fund, startDate, endDate, description, theme, minSIP, minLumpSum, riskLevel);
    this.performanceCalculatedTill = startDate;
  }

  public Fund(String advisorUsername, String adviserName, String fund, long startDate, long endDate, String description,
      double minSIP, double minLumpSum, RiskLevel riskLevel) {
    super(advisorUsername, adviserName, fund, startDate, endDate, description, minSIP, minLumpSum, riskLevel);
    this.performanceCalculatedTill = startDate;
  }

  public void update(FundEvent evt) {
    super.update(evt);
    if (evt instanceof FundPerformanceCalculated) {
      historicalPerformanceCalculated((FundPerformanceCalculated) evt);
    } else if (evt instanceof FundPerformancePersisted) {
      historicalPerformanceCalculated((FundPerformancePersisted) evt);
    } else if(evt instanceof RecommendedEntryPriceUpdated) {
      RecommendedEntryPriceUpdated event = (RecommendedEntryPriceUpdated) evt;
      getAdvisesWithIds().get(event.getAdviseId()).update(event);
    } else if (evt instanceof StartHistoricalFundPerformance) {
      priceListForPerformanceCalculation = new HashMap<String, List<PriceWithPaf>>();
    } else if (evt instanceof PriceEventsList) {
      PriceEventsList prices = (PriceEventsList) evt;
      priceListForPerformanceCalculation.put(prices.getWdId(), prices.getPriceList());
    } else if( evt instanceof CloseCorporateEventAdvise){
      handleCloseCorporateEvent((CloseCorporateEventAdvise) evt);
    } else if( evt instanceof UpdateSecondaryBuyCashRatioInMap){
      handleSecondaryBuyCashInMap((UpdateSecondaryBuyCashRatioInMap) evt);
    } else if( evt instanceof UpdateAdviseDetailsInCorpActionsMap){
      updateCorporateActionsMap((UpdateAdviseDetailsInCorpActionsMap) evt);
    } else if( evt instanceof UpdatingAdviseWdIds){
      handleWdIdUpdationForAdviseIds((UpdatingAdviseWdIds) evt);
    }
  }
  
  private void updateCorporateActionsMap(UpdateAdviseDetailsInCorpActionsMap evt) {
    UpdateAdviseDetailsInCorpActionsMap updateMap = (UpdateAdviseDetailsInCorpActionsMap) evt;
    corpActionTokenToAdviseDetails.put(updateMap.getOldTicker(), updateMap.getAdviseDetails());
  }

  private void handleCloseCorporateEvent(CloseCorporateEventAdvise evt) {
    CloseCorporateEventAdvise closeAdvise = (CloseCorporateEventAdvise) evt;
    Advise advise = getAdvisesWithIds().get(closeAdvise.getAdviseId());
    AdviseDetails adviseDetails;
    if (!corpActionTokenToAdviseDetails.containsKey(advise.getTicker())) {
       adviseDetails = new AdviseDetails(advise.getTicker(), advise.getRemainingAllocation(),
          advise.getEntryPrice().getPrice().getPrice(), 1.0, 0.0, 0.0, 0.0);

      corpActionTokenToAdviseDetails.put(advise.getTicker(), adviseDetails);
    }else{
      adviseDetails = corpActionTokenToAdviseDetails.get(advise.getTicker());
      double updatedAdviseAllocation = adviseDetails.getOldAdviseAllocation() + advise.getAllocation();
      adviseDetails.setOldAdviseAllocation(updatedAdviseAllocation);
      double updatedAverageEntryPrice = calaculateUpdateEntryPrice(advise.getAllocation(), advise.getEntryPrice().getPrice().getPrice(), adviseDetails.getOldAdviseAllocation(), adviseDetails.getRecoEntryPrice());
      adviseDetails.setRecoEntryPrice(updatedAverageEntryPrice);
      corpActionTokenToAdviseDetails.put(advise.getTicker(), adviseDetails);
    }
  }
  
  private void handleSecondaryBuyCashInMap(UpdateSecondaryBuyCashRatioInMap evt) {
    AdviseDetails adviseDetails = corpActionTokenToAdviseDetails.get(evt.getOldTicker());
    double cumulativeCashRatio = adviseDetails.getSecondaryAdviseAllocationRatio() + evt.getCashRatio();
    adviseDetails.setSecondaryAdviseAllocationRatio(cumulativeCashRatio);
    if(cumulativeCashRatio == 1.0){
      corpActionTokenToAdviseDetails.remove(evt.getOldTicker());
    }
  }

  private void historicalPerformanceCalculated(FundPerformanceCalculated evt) {
    if(evt.getDate() > performanceCalculatedTill) {
      performanceCalculatedTill = evt.getDate();
    }
  }

  private void historicalPerformanceCalculated(FundPerformancePersisted evt) {
    if (evt.getDate() > performanceCalculatedTill) {
      performanceCalculatedTill = evt.getDate();
    }
  }
  
  public int getDistinctStocksTradedCount(){
    return (int) getAdvisesWithIds().values().stream().map(advise -> advise.getTicker()).distinct().count();
  }
  
  public Collection<Advise> allAdviseObjects() {
    return unmodifiableCollection(getAdvisesWithIds().values());
  }

  public Collection<String> allAdviseIds() {
    return unmodifiableCollection(getAdvisesWithIds().keySet());
  }

  public Collection<FundConstituent> allFundComposition() {
    List<FundConstituent> activeConstituents = new ArrayList<>();
    for (Advise advise : getAdvisesWithIds().values()) {
      if (advise.getRemainingAllocation() > 0.) {
        activeConstituents.add(withActiveAdvise(advise.getId(), getFundName(), getAdviserUsername(), advise.getTicker(),
            advise.getSymbol(), advise.getRemainingAllocation(), advise.getEntryPrice()));
      }
    }

    return unmodifiableCollection(activeConstituents);
  }

  public void validateCloseAdvise(CloseAdvise closeAdvise) throws Exception {
    Advise advise = adviseForId(closeAdvise.getAdviseId());
    if (advise == null) {
      throw new IllegalArgumentException("advise not found, adviseId: " + closeAdvise.getAdviseId());
    }
    closeAdvise.validate(advise.getRemainingAllocation());
  }

  public Advise adviseForId(String id) {
    return getAdvisesWithIds().get(id);
  }

  public List<FundPerformanceCalculated> historical(ActorMaterializer materializer)
      throws Exception, InterruptedException, ExecutionException {
    log.info("Calculating performance for " + getFundName());

    List<AdvisePerformanceCalculated> allAdvisePerformanceStreams = allAdvisePerformanceStreams(materializer);
    
    if (allAdvisePerformanceStreams.isEmpty()) {
      List<FundPerformanceCalculated> fundPerformanceCalculatedList = new ArrayList<>();
      return fundPerformanceCalculatedList;
    }

    List<FundPerformanceCalculated> fundPerfStream = createFundPerformanceList(allAdvisePerformanceStreams);
    priceListForPerformanceCalculation.clear();

    return fundPerfStream;
  }

  private List<FundPerformanceCalculated> createFundPerformanceList(
      List<AdvisePerformanceCalculated> allAdvisePerformanceStreams) {
    Map<Long, List<AdvisePerformanceCalculated>> advisePerformanceWithDate = new HashMap<>();
    for(AdvisePerformanceCalculated advisePerformanceCalculated : allAdvisePerformanceStreams) {
      List<AdvisePerformanceCalculated> advisePerformanceCalculatedList = (advisePerformanceWithDate.get(advisePerformanceCalculated.getDate()) != null)?
          advisePerformanceWithDate.get(advisePerformanceCalculated.getDate()) : new ArrayList<>();
          advisePerformanceCalculatedList.add(advisePerformanceCalculated);
      advisePerformanceWithDate.put(advisePerformanceCalculated.getDate(), advisePerformanceCalculatedList);
    }

    List<FundPerformanceCalculated> fundPerformanceCalculatedList = new ArrayList<>();
    for(Long date : advisePerformanceWithDate.keySet()) {
      List<AdvisePerformanceCalculated> advisePerformanceCalculatedList = advisePerformanceWithDate.get(date);
      FundPerformanceCalculatedBuilder builder = new FundPerformanceCalculatedBuilder(getAdviserUsername(),
          getFundName(), date);
      builder.withAdvisePerformance(advisePerformanceCalculatedList);
      fundPerformanceCalculatedList.add(builder.build());
      
    }
    return fundPerformanceCalculatedList;
  }

  public List<AdvisePerformanceCalculated> allAdvisePerformanceStreams(Materializer materializer)
      throws Exception {
    List<AdvisePerformanceCalculated> allAdvisePerformanceStreams = new ArrayList<>();
    for (Advise child : allAdviseObjects()) {
      List<PriceWithPaf> priceSet = priceListForPerformanceCalculation.get(child.getTicker());
      if ((priceSet == null) || (priceSet.isEmpty()))
        continue;
      List<AdvisePerformanceCalculated> adviseStream = child.historicalPerformance(priceSet, materializer);
      allAdvisePerformanceStreams.addAll(adviseStream);
    }

    return allAdvisePerformanceStreams;
  }
  
  private double calaculateUpdateEntryPrice(double adviseAllocation, double adviseIssuePrice,
      double advisePreviousAllocation, double advisePreviousEntryPrice) {

    return (adviseAllocation * adviseIssuePrice + advisePreviousAllocation * advisePreviousEntryPrice)
        / (advisePreviousAllocation + adviseAllocation);
  }
}