package com.wt.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@DynamoDBTable(tableName = "FundPerformanceSnapshot")
@ToString
@NoArgsConstructor
public class FundPerformanceSnapshot implements Serializable{
  
  private static final long serialVersionUID = 1L;
  
  @Id
  @DynamoDBHashKey
  @Getter
  @Setter
  private String fundNameWithAdviser;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private long lastReadFundSequenceNumber;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private double fundPerformance;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private double fundRealizedPerformance;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private double fundUnRealizedPerformance;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private double fundCash;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private long lastPerformanceCalculatedDate;
  
  @DynamoDBAttribute
  @Getter
  @Setter
  private List<String> activeAdviseIdsAsOfLastCalculatedEod;
  
  
  public FundPerformanceSnapshot(String fundNameWithAdviser, long lastReadFundSequenceNumber, double fundPerformance,
      double fundRealizedPerformance, double fundUnRealizedPerformance, double fundCash,
      long lastPerformanceCalculatedDate, List<String> activeAdviseIdsAsOfLastCalculatedEod) {
    super();
    this.fundNameWithAdviser = fundNameWithAdviser;
    this.lastReadFundSequenceNumber = lastReadFundSequenceNumber;
    this.fundPerformance = fundPerformance;
    this.fundRealizedPerformance = fundRealizedPerformance;
    this.fundUnRealizedPerformance = fundUnRealizedPerformance;
    this.fundCash = fundCash;
    this.lastPerformanceCalculatedDate = lastPerformanceCalculatedDate;
    this.activeAdviseIdsAsOfLastCalculatedEod = activeAdviseIdsAsOfLastCalculatedEod;
  }
  
  public FundPerformanceSnapshot(String fundNameWithAdviser){
    this.fundNameWithAdviser = fundNameWithAdviser;
    this.lastPerformanceCalculatedDate = 0;
    this.activeAdviseIdsAsOfLastCalculatedEod = new ArrayList<>();
    this.fundCash = 100;
    this.fundPerformance = 0.0;
    this.fundRealizedPerformance = 0.0;
    this.fundUnRealizedPerformance = 0.0;
    this.lastReadFundSequenceNumber = 0;
  }
  
  

}
