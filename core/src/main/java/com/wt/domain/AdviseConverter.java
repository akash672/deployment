package com.wt.domain;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AdviseConverter implements DynamoDBTypeConverter<String, Map<String, Advise>> {

  @Override
  public String convert(Map<String, Advise> object) {
    return new Gson().toJson(object);
  }

  @Override
  public Map<String, Advise> unconvert(String object) {
    return new Gson().fromJson(object, new TypeToken<Map<String, Advise>>() {

      private static final long serialVersionUID = 1L;
    }.getType());
  }
}
