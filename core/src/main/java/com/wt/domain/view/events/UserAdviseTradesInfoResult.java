package com.wt.domain.view.events;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class UserAdviseTradesInfoResult {
  
  String investorName;
  String clientCode;
  String stockBought;
  String stockSold;
  String totalBuyValue;
  String totalSellValue;
  String averageEntryPrice;
  String averageExitPrice;
  List<AllTradesForEachUser> allTradesForEachUser;
  List<AllOrderForEachUser> allOrderForEachUser;
  
  @JsonCreator
  public UserAdviseTradesInfoResult(@JsonProperty("investorName") String investorName, @JsonProperty("clientCode") String clientCode, @JsonProperty("stockBought") String stockBought, @JsonProperty("stockSold") String stockSold,
      @JsonProperty("totalBuyValue") String totalBuyValue, @JsonProperty("totalSellValue") String totalSellValue, @JsonProperty("averageEntryPrice") String averageEntryPrice, @JsonProperty("averageExitPrice") String averageExitPrice,
      @JsonProperty("trades") List<AllTradesForEachUser> allTradesForEachUser, @JsonProperty("orderIds") List<AllOrderForEachUser> allOrderForEachUser) {
    this.investorName = investorName;
    this.clientCode = clientCode;
    this.stockBought = stockBought;
    this.stockSold = stockSold;
    this.totalBuyValue = totalBuyValue;
    this.totalSellValue = totalSellValue;
    this.averageEntryPrice = averageEntryPrice;
    this.averageExitPrice = averageExitPrice;
    this.allTradesForEachUser = allTradesForEachUser;
    this.allOrderForEachUser = allOrderForEachUser;
  }

  
  
}
