package com.wt.domain.view.events;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.domain.write.events.Done;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
@Getter
public class AdviseAnalyticsResponse extends Done {
  
  private static final long serialVersionUID = 1L;
  public AdviseAnalyticsResult result;

  public AdviseAnalyticsResponse(String id, String message, String resultString) {
    super(id, message);
    try {
      ObjectMapper mapper = new ObjectMapper();
      this.result = mapper.readValue(resultString, AdviseAnalyticsResult.class);
    } catch (Exception e) {
      log.error(e);
      this.result = new AdviseAnalyticsResult("", "", "", "", "", "", "", "", "", "", "",  "", "", "", "", "", "", "", "", "", new ArrayList<>() );
      // it can be blank but cannot be null else Json will be wrong
    }
  }
  
  @JsonCreator
  public AdviseAnalyticsResponse(@JsonProperty("id") String id, @JsonProperty("message") String message,
      @JsonProperty("result") AdviseAnalyticsResult result) {
    super(id, message);
    this.result = result;
  }


}
