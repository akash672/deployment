package com.wt.domain.view.events;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class AdviseAnalyticsResult {
  
  private String currentRealAmount;
  private String currentRealShares;
  private String  totalRealSharesBought;
  private String totalRealSharesSold;
  private String numberOfRealInvestor;
  private String numberOfRealInvestorLTD;
  private String averageRealEntryPrice;
  private String averageRealExitPrice;
  private String  currentVirtualAmount;
  private String currentVirtualShares;
  private String totalVirtualSharesBought;
  private String totalVirtualSharesSold;
  private String numberOfVirtualInvestor;
  private String numberOfVirtualInvestorLTD;
  private String averageVirtualEntryPrice;
  private String averageVirtualExitPrice;
  private String totalEntryRealAmount;
  private String totalExitRealAmount;
  private String totalEntryVirtualAmount;
  private String totalExitVirtualAmount;
  private List<String> unique_clientCode;
  
  @JsonCreator
  public AdviseAnalyticsResult(@JsonProperty("currentRealAmount") String currentRealAmount, @JsonProperty("currentRealShares") String currentRealShares,  @JsonProperty("totalRealSharesBought") String totalRealSharesBought,
      @JsonProperty("totalRealSharesSold") String totalRealSharesSold, @JsonProperty("numberOfRealInvestor") String numberOfRealInvestor, @JsonProperty("numberOfRealInvestorLTD") String numberOfRealInvestorLTD,
      @JsonProperty("averageRealEntryPrice") String averageRealEntryPrice, @JsonProperty("averageRealExitPrice") String averageRealExitPrice, @JsonProperty("currentVirtualAmount") String currentVirtualAmount,
      @JsonProperty("currentVirtualShares") String currentVirtualShares, @JsonProperty("totalVirtualSharesBought") String totalVirtualSharesBought, @JsonProperty("totalVirtualSharesSold") String totalVirtualSharesSold,
      @JsonProperty("numberOfVirtualInvestor") String numberOfVirtualInvestor, @JsonProperty("numberOfVirtualInvestorLTD") String numberOfVirtualInvestorLTD, @JsonProperty("averageVirtualEntryPrice") String averageVirtualEntryPrice,
      @JsonProperty("averageVirtualExitPrice") String averageVirtualExitPrice, @JsonProperty("totalEntryRealAmount") String totalEntryRealAmount, @JsonProperty("totalExitRealAmount") String totalExitRealAmount,
      @JsonProperty("totalEntryVirtualAmount") String totalEntryVirtualAmount, @JsonProperty("totalExitVirtualAmount") String totalExitVirtualAmount, @JsonProperty("unique_clientCode") List<String> unique_clientCode) {
    this.currentRealAmount = currentRealAmount;
    this.currentRealShares = currentRealShares;
    this.totalRealSharesBought = totalRealSharesBought;
    this.totalRealSharesSold = totalRealSharesSold;
    this.numberOfRealInvestor = numberOfRealInvestor;
    this.numberOfRealInvestorLTD = numberOfRealInvestorLTD;
    this.averageRealEntryPrice = averageRealEntryPrice;
    this.averageRealExitPrice = averageRealExitPrice;
    this.currentVirtualAmount = currentVirtualAmount;
    this.currentVirtualShares = currentVirtualShares;
    this.totalVirtualSharesBought = totalVirtualSharesBought;
    this.totalVirtualSharesSold = totalVirtualSharesSold;
    this.numberOfVirtualInvestor = numberOfVirtualInvestor;
    this.numberOfVirtualInvestorLTD = numberOfVirtualInvestorLTD;
    this.averageVirtualEntryPrice = averageVirtualEntryPrice;
    this.averageVirtualExitPrice = averageVirtualExitPrice;
    this.totalEntryRealAmount = totalEntryRealAmount;
    this.totalExitRealAmount = totalExitRealAmount;
    this.totalEntryVirtualAmount = totalEntryVirtualAmount;
    this.totalExitVirtualAmount = totalExitVirtualAmount;
    this.unique_clientCode = unique_clientCode;
  }

}
