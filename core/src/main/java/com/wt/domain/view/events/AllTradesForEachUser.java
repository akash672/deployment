package com.wt.domain.view.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class AllTradesForEachUser {

  String date;
  String quantity;
  String totalValue;
  String orderSide;
  String exchangeOrderId;
  String tradeId;

  @JsonCreator
  public AllTradesForEachUser(@JsonProperty("date") String date, @JsonProperty("quantity") String quantity,
      @JsonProperty("totalValue") String totalValue, @JsonProperty("orderSide") String orderSide,
      @JsonProperty("exchangeOrderId") String exchangeOrderId, @JsonProperty("tradeId") String tradeId) {
    this.date = date;
    this.quantity = quantity;
    this.totalValue = totalValue;
    this.orderSide = orderSide;
    this.exchangeOrderId = exchangeOrderId;
    this.tradeId = tradeId;
  }

}
