package com.wt.domain.view.events;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.domain.write.events.Done;

import lombok.extern.log4j.Log4j;

@Log4j
public class UserAdviseTradeResponse extends Done{
  
  private static final long serialVersionUID = 1L;
  public UserAdviseTradesInfoResult result;

  public UserAdviseTradeResponse(String id, String message, String resultString) {
    super(id, message);
    try {
      ObjectMapper mapper = new ObjectMapper();
      this.result = mapper.readValue(resultString, UserAdviseTradesInfoResult.class);
    } catch (Exception e) {
      log.error(e);
      this.result = new UserAdviseTradesInfoResult("", "", "", "", "", "", "", "", new ArrayList<>(), new ArrayList<>() );
      // it can be blank but cannot be null else Json will be wrong
    }
  }
  
  @JsonCreator
  public UserAdviseTradeResponse(@JsonProperty("id") String id, @JsonProperty("message") String message,
      @JsonProperty("result") UserAdviseTradesInfoResult result) {
    super(id, message);
    this.result = result;
  }
  

}
