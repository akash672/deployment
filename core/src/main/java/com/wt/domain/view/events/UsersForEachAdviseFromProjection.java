package com.wt.domain.view.events;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.wt.domain.write.events.Done;

import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
@Getter
public class UsersForEachAdviseFromProjection extends Done{
  private static final long serialVersionUID = 1L;
  private Map<String,String[]> result;

  public UsersForEachAdviseFromProjection(String id, String message, String resultString) {
    super(id, message);
    try {
      ObjectMapper mapper = new ObjectMapper();
      this.result = mapper.readValue(resultString, new TypeReference<Map<String, String[]>>() {});
    } catch (Exception e) {
      log.error(e);
      this.result = Maps.newHashMap();
    }
  }
  
  @JsonCreator
  public UsersForEachAdviseFromProjection(@JsonProperty("id") String id, @JsonProperty("message") String message,
      @JsonProperty("result") Map<String, String[]> result) {
    super(id, message);
    this.result = result;
  }

  

}
