package com.wt.domain.view.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class AllOrderForEachUser {
  String useradviseState;
  String date;
  String exchangeorderId;
  String quantity;

  @JsonCreator
  public AllOrderForEachUser(@JsonProperty("useradviseState") String useradviseState, @JsonProperty("date") String date,
      @JsonProperty("exchangeorderId") String exchangeorderId, @JsonProperty("quantity") String quantity) {
    this.useradviseState = useradviseState;
    this.date = date;
    this.exchangeorderId = exchangeorderId;
    this.quantity = quantity;
  }

}
