package com.wt.domain;

import com.wt.utils.akka.CancellableStream;

import akka.NotUsed;
import akka.stream.Materializer;
import akka.stream.UniqueKillSwitch;
import akka.stream.javadsl.Sink;
import lombok.ToString;

@ToString
public class PerformanceStream<T> {
  
  private Sink<T, NotUsed> toConsumer;
  
  private UniqueKillSwitch killSwitch;

  public PerformanceStream(Sink<T, NotUsed> toConsumer, UniqueKillSwitch killSwitch) {
    super();
    this.toConsumer = toConsumer;
    this.killSwitch = killSwitch;
  }

  public void cancel() {
    if (killSwitch != null) {
      killSwitch.shutdown();
    }
  }

  public void include(CancellableStream<T, NotUsed> performanceStream, Materializer materializer) {
    performanceStream.getSource()
    .runWith(toConsumer, materializer);
  }

}