package com.wt.utils.akka;

import com.wt.domain.write.events.Done;

import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;
import lombok.extern.log4j.Log4j;

@Log4j
public class ResultsWaiterActor extends UntypedAbstractActor {
  private int totalAwaited = 0;
  private int received = 0;
  private ActorRef orignalSender;

  @Override
  public void onReceive(Object msg) {
    log.debug("ResultsWaiterActor " + msg);
    if (msg instanceof Integer) {
      totalAwaited = (Integer) msg;
      orignalSender = sender();
      send();
    }
    if (msg instanceof Done) {
      received++;
      send();
    }
  }

  protected void send() {
    if (orignalSender != null && totalAwaited == received) {
      orignalSender.tell(new Done(orignalSender.toString(), "All"), self());
      log.debug("Sending to " + orignalSender);

    }
  }

}