package com.wt.utils.akka;

public interface Cancellable {
  void cancel();
}
