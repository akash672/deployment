package com.wt.utils.akka;

import java.util.function.Consumer;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import akka.actor.UntypedAbstractActor;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@Service("OnEventActionActor")
@Scope("prototype")
@Log4j
public class OnEventActionActor<In> extends UntypedAbstractActor {

  private Class<In> eventClass;
  private Consumer<In> action;

  @SuppressWarnings("unchecked")
  @Override
  public void onReceive(Object event) {
    log.info("OnEventActionActor" + event);
    if (event.getClass().equals(eventClass)) {
      log.info("Received " + eventClass + " object : " + event);
      action.accept((In) event);
    } else {
      throw new IllegalStateException("This actor can  not handle " + event);
    }
  }

}
