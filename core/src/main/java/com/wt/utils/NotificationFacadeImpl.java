package com.wt.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.amazonaws.services.cognitoidp.model.InvalidParameterException;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.wt.config.utils.WDProperties;

import lombok.extern.log4j.Log4j;

@Service("NotificationFacade")
@Log4j
public class NotificationFacadeImpl implements NotificationFacade {

  private AmazonSNS snsClient;
  private AmazonSNS snsClientForSMS;
  private WDProperties properties;

  @Autowired
  public NotificationFacadeImpl(@Qualifier("SNSClientForSMS") AmazonSNS snsClientForSMS,
      @Qualifier("SNSClient") AmazonSNS snsClient, WDProperties wdProperties) {
    this.snsClientForSMS = snsClientForSMS;
    this.snsClient = snsClient;
    this.properties = wdProperties;
  }

  @Override
  public void sendOTP(String message, String phoneNumber) throws IOException {
    Map<String, MessageAttributeValue> smsAttributes = getBasicMessageAttributes(false);

    String messageId = sendSMS(message, phoneNumber, smsAttributes);
    log.debug("SMS sent to " + phoneNumber + " and its message id is " + messageId);
  }

  private Map<String, MessageAttributeValue> getBasicMessageAttributes(boolean isPromotional) {
    Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
    smsAttributes.put("AWS.SNS.SMS.SenderID",
        new MessageAttributeValue().withStringValue(properties.SMS_SENDER_NAME()).withDataType("String"));
    if (isPromotional)
      smsAttributes.put("AWS.SNS.SMS.SMSType",
          new MessageAttributeValue().withStringValue("Promotional").withDataType("String"));
    else
      smsAttributes.put("AWS.SNS.SMS.SMSType",
          new MessageAttributeValue().withStringValue("Transactional").withDataType("String"));
    return smsAttributes;
  }

  @Override
  public void sendNotification(String deviceToken, String message, Map<String, String> additionalAttributes) {
    additionalAttributes.put("message", message);
    sendToPushNotification(deviceToken, additionalAttributes);
    sendToSNSLambda(additionalAttributes);
  }

  private void sendToPushNotification(String deviceToken, Map<String, String> additionalAttributes) {
    try {
      if (deviceToken.equals("NODEVICETOKENIMPLEMENTED"))
        return;
      PublishRequest publishRequest;
      String applicationArn = properties.SNS_APPLICATION_ARN();
      String endPoint = createEndpoint(applicationArn, deviceToken);

      publishRequest = publishRequest(additionalAttributes, endPoint, true);
      snsClient.publish(publishRequest);
    } catch (Exception e) {
      log.warn("Device Token:" + deviceToken
          + ", Additional Attributes:" + additionalAttributes.entrySet().stream()
              .map(entry -> entry.getKey() + " - " + entry.getValue()).collect(Collectors.joining(", "))
          + ",Error:" + e.getMessage());
    }
  }

  private void sendToSNSLambda(Map<String, String> additionalAttributes) {
    PublishRequest publishRequest;
    try {
      publishRequest = publishRequest(additionalAttributes, properties.SNS_TOPIC_ARN_TRIGGERING_FOR_DYNAMODB_DUMP(),
          false);
      log.info("Sending Notification to SNS: " + additionalAttributes.get("message"));
      snsClient.publish(publishRequest);
    } catch (Exception e) {
      log.error(e.getCause(), e);
    }
  }

  private PublishRequest publishRequest(Map<String, String> additionalAttributes, String endPoint,
      boolean isGCMMessage) {
    PublishRequest publishRequest = new PublishRequest();
    publishRequest.setTargetArn(endPoint);
    if (isGCMMessage) {
      prepareGCMMessage(additionalAttributes, publishRequest);
      publishRequest.setMessageStructure("json");
    } else {
      publishRequest.setMessageAttributes(getMessageAttributesMap(additionalAttributes));
      publishRequest.setMessage(additionalAttributes.get("message"));
    }
    return publishRequest;
  }

  private void prepareGCMMessage(Map<String, String> additionalAttributes, PublishRequest publishRequest) {
    Map<String, String> gcmMap = additionalAttributes.entrySet().stream().filter(entry -> {
      return isGCMAttribute(entry);
    }).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
    String jsonMessage = "{\"GCM\": \"{ \\\"data\\\": ";
    jsonMessage += (new Gson().toJson(gcmMap).toString()).replace("\"", "\\\"") + "} }\"}";
    log.debug(jsonMessage);
    publishRequest.setMessage(jsonMessage);
  }

  private boolean isGCMAttribute(Entry<String, String> entry) {
    return entry.getKey() == "fund_name" || entry.getKey() == "author" || entry.getKey() == "title"
        || entry.getKey() == "type" || entry.getKey() == "message";
  }

  private Map<String, MessageAttributeValue> getMessageAttributesMap(Map<String, String> additionalAttributes) {
    Map<String, MessageAttributeValue> map = additionalAttributes.entrySet().stream()
        .filter(entry -> !isGCMAttribute(entry)).collect(Collectors.toMap(p -> p.getKey(), p -> {
          MessageAttributeValue attributeValue = new MessageAttributeValue();
          attributeValue.setDataType("String");
          attributeValue.setStringValue(p.getValue().isEmpty() ? "NA" : p.getValue());
          return attributeValue;
        }));
    return map;
  }

  private String createEndpoint(String applicationArn, String token) {
    try {
      log.debug("Creating platform endpoint with token " + token);
      CreatePlatformEndpointRequest cpeReq = new CreatePlatformEndpointRequest()
          .withPlatformApplicationArn(applicationArn).withToken(token);
      CreatePlatformEndpointResult cpeRes = snsClient.createPlatformEndpoint(cpeReq);
      return cpeRes.getEndpointArn();
    } catch (InvalidParameterException ipe) {
      String message = ipe.getErrorMessage();
      log.error("Exception message: " + message);
      Pattern p = Pattern.compile(".*Endpoint (arn:aws:sns[^ ]+) already exists with the same token.*");
      Matcher m = p.matcher(message);
      if (m.matches()) {
        return m.group(1);
      } else {
        throw ipe;
      }
    }
  }

  @Override
  public void sendSMSMessage(String message, String phoneNumber) {
    Map<String, MessageAttributeValue> smsAttributes = getBasicMessageAttributes(false);
    if(!message.isEmpty() && phoneNumber != null)
      sendSMS(message, phoneNumber, smsAttributes);
    else
      log.debug("Not sending empty message to : "+phoneNumber);
  }

  public void sendSMSMessage(String message, String phoneNumber, boolean isPromotionalSMS) {
    Map<String, MessageAttributeValue> smsAttributes = getBasicMessageAttributes(isPromotionalSMS);

    sendSMS(message, phoneNumber, smsAttributes);
  }

  private String sendSMS(String message, String phoneNumber, Map<String, MessageAttributeValue> smsAttributes) {
    PublishResult result = snsClientForSMS.publish(
        new PublishRequest().withMessage(message).withPhoneNumber(phoneNumber).withMessageAttributes(smsAttributes));
    log.debug("SMS sent to " + phoneNumber + " with its message id is " + result.getMessageId()+" and message: "+message);
    return result.getMessageId();
  }
}