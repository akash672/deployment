package com.wt.utils;

import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.config.utils.WDProperties;

import lombok.NonNull;

@Service("OTPUtils")
public class OTPUtils {
  private WDProperties properties;
  
  @Autowired
  public OTPUtils(@NonNull WDProperties properties) {
    this.properties = properties;
  }

  public String getNewOtp() {
    return "" + (1000 + new Random().nextInt(8999));
  }

  /**
   * Checks whether the same OTP is provided by the User and checks OTP
   * expiration (OTP passed 10 min's is considered as expired OTP)
   * 
   * @param otp
   * @return boolean
   */
  public boolean isOTPTimeValid(String attemptedOtp, String otp, long lastOTPSentTimestamp) {
    return (otp.equalsIgnoreCase(attemptedOtp)) 
        && ((lastOTPSentTimestamp + properties.OTP_EXPIRING_MINUTES()) > DateUtils.currentTimeInMillis());
  }

  public void setProperties(WDProperties properties) {
    this.properties = properties;
  }
  
  public String getUniqueHashCode() {
    return UUID.randomUUID().toString();
  }
}
