package com.wt.utils;

import java.io.IOException;
import java.util.Map;

public interface NotificationFacade {
  public void sendOTP(String otp, String inputPhoneNumber) throws IOException;

  public void sendNotification(String deviceToken, String message, Map<String, String> additionalAttributes);

  public void sendSMSMessage(String message, String inputPhoneNumber);
  
}