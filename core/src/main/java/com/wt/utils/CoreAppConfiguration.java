package com.wt.utils;

import static com.typesafe.config.ConfigFactory.load;
import static com.wt.utils.CommonConstants.COLON;
import static java.lang.Integer.parseInt;
import static scala.collection.JavaConversions.asScalaBuffer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.typesafe.config.Config;
import com.wt.utils.akka.SpringExtension;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import eventstore.HttpSettings;
import eventstore.ProjectionsClient;
import eventstore.Settings;
import eventstore.cluster.ClusterSettings;
import eventstore.j.ClusterSettingsBuilder;
import eventstore.j.SettingsBuilder;

@EnableAsync
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "com.wt")
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:environment.properties")
public class CoreAppConfiguration extends CommonAppConfiguration {
  private ApplicationContext applicationContext;
  
  @Autowired
  public CoreAppConfiguration(ApplicationContext applicationContext) {
    super();
    this.applicationContext = applicationContext;
  }
  
  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() throws IOException {
    setSystemProperties();
    String config = "WD-application.conf";
    String actorName = "WD";
    if (System.getProperty("appConfig") != null) {
      config = System.getProperty("appConfig");
      actorName = System.getProperty("appConfig").split("-")[0];
    }
    Config conf = load(config);
    ActorSystem system = ActorSystem.create(actorName, conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }
  
  @Bean(name="JournalProvider")
  public JournalProvider journalProvider() throws IOException
  {
    JournalProviderProduction journalProviderProduction = new JournalProviderProduction(actorSystem());
    return journalProviderProduction;
  }

  @Bean(name = "SNSClient")
  public AmazonSNS snsClientCreation() {
    return AmazonSNSAsyncClientBuilder.standard().withRegion(getAmazonAWSRegion())
        .withCredentials(amazonAWSCredentialsProvider()).build();
  }

  @Bean(name = "SNSClientForSMS")
  public AmazonSNS snsClientSMSRegionCreation() {
    return AmazonSNSAsyncClientBuilder.standard().withRegion(getAmazonSMSAWSRegion())
        .withCredentials(amazonAWSCredentialsProvider()).build();
  }

  @Bean(destroyMethod = "shutdown")
  public ActorMaterializer actorMaterializer() throws IOException {
    return ActorMaterializer.create(actorSystem());
  }

  @Bean(name="WdEventStoreSettings")
  public Settings settingsToConnect() throws IOException {
    ActorSystem actorSystem = actorSystem();
    String ip = actorSystem.settings().config().getString("eventstore.address.host");
    int port= Integer.parseInt(actorSystem.settings().config().getString("eventstore.http.port"));
    String userName = actorSystem.settings().config().getString("eventstore.credentials.login");
    String password = actorSystem.settings().config().getString("eventstore.credentials.password");
    List<String> clusterGossipIps = actorSystem.settings().config().getStringList("eventstore.cluster.gossip-seeds");
    final Settings settings = getSettings(actorSystem, ip, port, userName, password, clusterGossipIps);
    return settings;
  }

  private Settings getSettings(ActorSystem actorSystem, String ip, int port, String userName, String password, List<String> clusterGossipIps) {
    List<InetSocketAddress> clusterGossipInets = new ArrayList<InetSocketAddress>();
    if (!clusterGossipIps.isEmpty()) {
      clusterGossipIps.stream().forEach(clusterNode -> clusterGossipInets
          .add(new InetSocketAddress(clusterNode.split(COLON)[0], parseInt(clusterNode.split(COLON)[1]))));
      ClusterSettings clusterSettings = new ClusterSettingsBuilder()
          .gossipSeeds(asScalaBuffer(clusterGossipInets))
          .build();
      return new SettingsBuilder()
          .address(new InetSocketAddress(ip, port))
          .http(HttpSettings.apply(actorSystem.settings().config().getConfig("eventstore")))
          .defaultCredentials(userName, password)
          .cluster(clusterSettings)
          .build();
    } else
      return new SettingsBuilder()
          .address(new InetSocketAddress(ip, port))
          .http(HttpSettings.apply(actorSystem.settings().config().getConfig("eventstore")))
          .defaultCredentials(userName, password)
          .build();
  }

  @Bean(name="WDProjections")
  public ProjectionsClient projectionFromWDService() throws IOException{
      return new ProjectionsClient(settingsToConnect(), actorSystem());
  }
  
  @Bean(name = "CognitoUserPoolForInvestor")
  @Lazy
  @Primary
  public AWSCognitoIdentityProvider userPoolSDK() {
      return AWSCognitoIdentityProviderClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider())
          .withRegion(Regions.fromName(getAmazonAWSRegion()).getName()).withClientConfiguration(new ClientConfiguration()).build();
  }
}
