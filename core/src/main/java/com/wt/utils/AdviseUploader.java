package com.wt.utils;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.wt.domain.Instrument;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.GetEquityInstrumentBySymbol;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.Result;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import lombok.extern.log4j.Log4j;

@Log4j
public class AdviseUploader {

  public static void main(String... args) throws Exception {
    AdviseUploader historicalPerformanceUploader = new AdviseUploader();
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(CoreAppConfiguration.class)) {
      final ActorSystem system = (ActorSystem) ctx.getBean("actorSystem");
      final ActorRef adviserManagerActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"),
          "adviser-aggregator");
      final WDActorSelections actorSelection = (WDActorSelections) ctx.getBean(WDActorSelections.class);

      historicalPerformanceUploader.upload(adviserManagerActor, args[0], args[1], actorSelection.universeRootActor());
      system.terminate();
    }
  }

  @SuppressWarnings("unused")
  public void upload(ActorRef adviserManagerActor, String fileName, String adviserIdentityId,
      ActorSelection actorSelection) throws Exception {
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    String adviserUsername = StringUtils.substringBefore(fileName, "_")
        .split("/")[StringUtils.substringBefore(fileName, "_").split("/").length - 1];
    String[] splits = fileName.split("_");
    String adviserName = splits[1].replaceAll("-", " ");
    String email = splits[2];
    String fundName = splits[3];
    String startDate = splits[4].replaceAll("-", "/");
    String endDate = splits[5].replaceAll("-", "/");
    String Description = splits[6];
    String theme = splits[7];
    double minSIP = Double.parseDouble(splits[8]);
    double minLumpSum = Double.parseDouble(splits[9]);
    String riskLevel = splits[10].substring(0, splits[10].lastIndexOf("."));
    File adviseFile = new File(fileName);

    FileInputStream fis = new FileInputStream(adviseFile);
    XSSFWorkbook workBooks = new XSSFWorkbook(fis);
//    adviserManagerActor.tell(new RegisterCognitoAdviser(adviserUsername, adviserName, email, "123456", "publicId", 0),null);
    adviserManagerActor.tell(new FloatFund(adviserIdentityId, fundName, eodDateFromExcelDate(startDate),
        eodDateFromExcelDate(endDate), Description, theme, minSIP, minLumpSum, riskLevel), null);
    sendAdvises(adviserManagerActor, df, workBooks, adviserIdentityId, actorSelection);
  }

  private boolean isRowEmpty(Row row) {
    for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
      Cell cell = row.getCell(c);
      if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
        return false;
    }
    return true;
  }

  private void sendAdvises(final ActorRef adviserManagerActor, DateFormat df, XSSFWorkbook workBooks, String username,
      ActorSelection actorSelection) throws Exception {
    XSSFSheet issueAdviseSheet = workBooks.getSheetAt(0);
    Iterator<Row> rowIterator = issueAdviseSheet.iterator();
    Row row = rowIterator.next();
    while (rowIterator.hasNext()) {
      row = rowIterator.next();
      if (isRowEmpty(row))
        continue;

      nonEmptyRow(adviserManagerActor, df, row, username, actorSelection);
    }
  }

  private void nonEmptyRow(final ActorRef adviserManagerActor, DateFormat df, Row row, String username,
      ActorSelection actorSelection) throws ParseException, Exception {
    if (isNewAdvise(row.getCell(0).getStringCellValue().trim())) {
      newAdvise(adviserManagerActor, df, row, username, actorSelection);
    } else if (isClosedAdvise(row.getCell(0).getStringCellValue().trim())) {
      closeAdvise(adviserManagerActor, df, row, username);

    } else {
      log.info("Neither new nor close");
    }
  }

  private boolean isClosedAdvise(String adviseType) {
    return adviseType.equals("Close");
  }

  private boolean isNewAdvise(String adviseType) {
    return adviseType.equals("New");
  }

  private void newAdvise(final ActorRef adviserManagerActor, DateFormat df, Row row, String username,
      ActorSelection actorSelection) throws ParseException, Exception {
    Date entryTime = row.getCell(10).getDateCellValue();
    String entryDate = df.format(entryTime);
    Date issueTime = row.getCell(9).getDateCellValue();
    String issueDate = df.format(issueTime);
    String ticker = row.getCell(5).getStringCellValue();
    String exchange = row.getCell(6).getStringCellValue();
    String token = getRequiredToken(ticker, exchange, actorSelection);
    if (token == null)
    {
      log.error("Provided Ticker: "+ticker+" is wrong and system is unable to find its token");
      return;
    }
    IssueAdvice issueAdvice = null;
    try {
      issueAdvice = new IssueAdvice(username, row.getCell(4).getStringCellValue(),
          String.valueOf(row.getCell(1).getNumericCellValue()), token, row.getCell(8).getNumericCellValue(),
          row.getCell(11).getNumericCellValue(), eodDateFromExcelDate(entryDate, "dd/MM/yyyy", "/"),
          row.getCell(12).getNumericCellValue(), recoExitTime(df, row),
          eodDateFromExcelDate(issueDate, "dd/MM/yyyy", "/"));
    } catch (IllegalStateException e) {
      e.printStackTrace();
      log.error("Row Number" + row.getRowNum() + " Has issues to be solved");
      System.exit(0);
    }
    askAndWaitForResult(adviserManagerActor, issueAdvice);
  }

  private String getRequiredToken(String ticker, String exchange, ActorSelection actorSelection) throws Exception {
    Object maybeInstrument = askAndWaitForResult(actorSelection, new GetEquityInstrumentBySymbol(ticker, exchange));
    if (maybeInstrument.getClass().equals(Failed.class)) {
      return null;
    }

    Instrument instrument = (Instrument) maybeInstrument;
    return instrument.getWdId();
  }

  private void closeAdvise(final ActorRef adviserManagerActor, DateFormat df, Row row, String username)
      throws ParseException, Exception {
    CloseAdvise closeAdvise = new CloseAdvise(username, row.getCell(4).getStringCellValue(),
        String.valueOf(row.getCell(1).getNumericCellValue()), row.getCell(8).getNumericCellValue(), exitTime(df, row),
        row.getCell(15).getNumericCellValue());
    Result result = (Result) askAndWaitForResult(adviserManagerActor, closeAdvise);

    if (!(result instanceof Done)) {
      throw new RuntimeException("Failed " + result);
    }
  }

  private long recoExitTime(DateFormat df, Row row) throws ParseException {
    try {
      Date exitTime = row.getCell(13).getDateCellValue();
      String exitDate = df.format(exitTime);
      return eodDateFromExcelDate(exitDate, "dd/MM/yyyy", "/");
    } catch (NullPointerException e) {
      return -1L;
    }
  }

  private long exitTime(DateFormat df, Row row) throws ParseException {
    try {
      String actualExitDate = df.format(row.getCell(14).getDateCellValue());
      return eodDateFromExcelDate(actualExitDate, "dd/MM/yyyy", "/");
    } catch (NullPointerException e) {
      return -1L;
    }
  }

}
