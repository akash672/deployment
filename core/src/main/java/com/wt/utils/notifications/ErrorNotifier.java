package com.wt.utils.notifications;

import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;

import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j;
@Service("ErrorNotification")
@Log4j
public class ErrorNotifier {

  public ErrorNotifier() {
    super();
  }

  public void notify(Exception exception) {
    log.error(getStackTrace(exception));
  }

}
