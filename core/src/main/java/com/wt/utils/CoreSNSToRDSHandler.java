package com.wt.utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.google.gson.Gson;
import com.wt.config.utils.WDProperties;
import com.wt.domain.PriceWithPaf;
import com.wt.rds.domain.PriceRecordRDS;
import com.wt.rds.domain.PriceRecordRDSBatch;
import com.wt.rds.domain.PriceRecordRDSKey;

import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class CoreSNSToRDSHandler {

  @Autowired
  private WDProperties wdProperties;
  @Autowired
  @Qualifier("SNSClient")
  private AmazonSNS rdsSNSClient;

  public void handlePriceData(HashMap<String, PriceWithPaf> tickerToPriceWithPafMap) {
    List<Map<String, PriceWithPaf>> listOfMaps = splitMap(tickerToPriceWithPafMap,
        wdProperties.maxPricesToSNSBatchSizeToRDS());
    for (Map<String, PriceWithPaf> map : listOfMaps) {
      Map<String, String> attributes = setPriceTableAttributes(map);
      sendToRDS(attributes);
    }
  }

  private void sendToRDS(Map<String, String> additionalAttributes) {
    PublishRequest publishRequest;
    publishRequest = publishRequest(additionalAttributes, wdProperties.SNS_TOPIC_ARN_TRIGGERING_FOR_WRITE_TO_RDS());
    log.info(additionalAttributes.get("message") + " into DB: " + additionalAttributes.get("dbName") + ", in table: "
        + additionalAttributes.get("tablename"));
    rdsSNSClient.publish(publishRequest);
  }

  private PublishRequest publishRequest(Map<String, String> additionalAttributes, String endPoint) {
    PublishRequest publishRequest = new PublishRequest();
    publishRequest.setTargetArn(endPoint);
    publishRequest.setMessageAttributes(getMessageAttributesMap(additionalAttributes));
    publishRequest.setMessage(additionalAttributes.get("message"));
    return publishRequest;
  }

  private Map<String, MessageAttributeValue> getMessageAttributesMap(Map<String, String> additionalAttributes) {
    Map<String, MessageAttributeValue> map = additionalAttributes.entrySet().stream()
        .collect(Collectors.toMap(p -> p.getKey(), p -> {
          MessageAttributeValue attributeValue = new MessageAttributeValue();
          attributeValue.setDataType("String");
          attributeValue.setStringValue(p.getValue().isEmpty() ? "NA" : p.getValue());
          return attributeValue;
        }));
    return map;
  }

  private Map<String, String> setPriceTableAttributes(Map<String, PriceWithPaf> pwpMap) {
    Map<String, String> additionalAttributes = new HashMap<String, String>();
    PriceRecordRDSBatch priceRecordBatchDomain = createPriceRecordRDSBatchDomain(pwpMap);
    String recordString = (new Gson().toJson(priceRecordBatchDomain).toString());
    additionalAttributes.put("recordString", recordString);
    additionalAttributes.put("dbName", wdProperties.environment() + "_WDTimeSeries");
    additionalAttributes.put("tablename", wdProperties.environment() + "_prices");
    additionalAttributes.put("message", "Sending PriceBatch of size : " + pwpMap.size());
    return additionalAttributes;
  }

  private PriceRecordRDSBatch createPriceRecordRDSBatchDomain(Map<String, PriceWithPaf> pwpMap) {
    List<PriceRecordRDS> priceRecordRDSBatch = new ArrayList<PriceRecordRDS>();
    for (String wdId : pwpMap.keySet()) {
      PriceWithPaf pwp = pwpMap.get(wdId);
      try {
        PriceRecordRDSKey priceRecordRDSKey = new PriceRecordRDSKey(pwp.getPrice().getWdId(), DateUtils.todayEOD());
        PriceRecordRDS priceRecord = new PriceRecordRDS(priceRecordRDSKey, DateUtils.currentTimeInMillis(),
            pwp.getPrice().getPrice(), pwp.getAdjusted(), pwp.getCumulativePaf(), pwp.getDividend(),
            pwp.getHistoricalPaf(), pwp.getNonDividendPaf());
        priceRecordRDSBatch.add(priceRecord);
      } catch (ParseException e) {
        log.error("Error getting today eod timestamp, reason: " + e);
      }
    }
    return new PriceRecordRDSBatch(priceRecordRDSBatch);
  }

  private List<Map<String, PriceWithPaf>> splitMap(Map<String, PriceWithPaf> tickerToPriceWithPafMap, int splitSize) {

    float mapSize = tickerToPriceWithPafMap.size();
    float splitFactorF = splitSize;
    float actualNoOfBatches = (mapSize / splitFactorF);
    double noOfBatches = Math.ceil(actualNoOfBatches);

    List<Map<String, PriceWithPaf>> listOfMaps = new ArrayList<>();
    List<List<String>> listOfListOfKeys = new ArrayList<>();
    int startIndex = 0;
    int endIndex = (int) ((splitSize > mapSize) ? mapSize : splitSize);

    Set<String> keys = tickerToPriceWithPafMap.keySet();
    List<String> keysAsList = new ArrayList<>();
    keysAsList.addAll(keys);

    for (int i = 0; i < noOfBatches; i++) {
      listOfListOfKeys.add(keysAsList.subList(startIndex, endIndex));
      startIndex = endIndex;
      endIndex = (int) (((endIndex + splitSize) > mapSize) ? mapSize : (endIndex + splitSize));
    }

    for (List<String> keyList : listOfListOfKeys) {
      Map<String, PriceWithPaf> subMap = new HashMap<>();
      for (String key : keyList) {
        subMap.put(key, tickerToPriceWithPafMap.get(key));
      }
      listOfMaps.add(subMap);
    }

    return listOfMaps;
  }
}
