package com.not.wt.pkg.to.override.main.config;

import static com.typesafe.config.ConfigFactory.load;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import com.amazonaws.services.dynamodbv2.local.shared.access.AmazonDynamoDBLocal;
import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.sns.AmazonSNS;
import com.ctcl.xneutrino.interactive.InteractiveManager;
import com.typesafe.config.Config;
import com.wt.config.utils.WDProperties;
import com.wt.domain.repo.EquityInstrumentRepository;
import com.wt.utils.CommonAppConfiguration;
import com.wt.utils.JournalProvider;
import com.wt.utils.JournalProviderTest;
import com.wt.utils.NotificationFacade;
import com.wt.utils.OTPUtils;
import com.wt.utils.akka.SpringExtension;

import akka.actor.ActorSystem;
import eventstore.ProjectionsClient;

@Configuration
@EnableDynamoDBRepositories(basePackages = "com.wt.domain.repo")
@PropertySource("classpath:test-environment.properties")
@Import(CommonAppConfiguration.class)
public class TestAppConfiguration {

  @Autowired
  protected ApplicationContext applicationContext;
  @Autowired
  public WDProperties wdProperties;

  @Bean(destroyMethod = "terminate")
  public ActorSystem actorSystem() {
    Config conf = load("test-application.conf");
    ActorSystem system = ActorSystem.create("WD", conf);
    SpringExtension.SpringExtProvider.get(system).initialize(applicationContext);
    return system;
  }

  @Bean
  public AWSCredentials amazonAWSCredentials() {
    return mock(AWSCredentials.class);
  }

  @Bean
  public EquityInstrumentRepository equityInstrumentRepository() {
    EquityInstrumentRepository equityInstrumentRepository = applicationContext
        .getBean(EquityInstrumentRepository.class);
    EquityInstrumentRepository mockEquityInstrumentRepository = spy(equityInstrumentRepository);
    doNothing().when(mockEquityInstrumentRepository).deleteAll();

    return mockEquityInstrumentRepository;
  }

  public AmazonDynamoDBLocal amazonDynamoDBLocal() {
    return DynamoDBEmbedded.create();
  }

  @Bean
  public AmazonDynamoDB amazonDynamoDB() {
    return amazonDynamoDBLocal().amazonDynamoDB();
  }

  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("test-environment.properties"));
    return properties;
  }

  @Bean(name = "NotificationFacade")
  public NotificationFacade notificationFacade() {
    return mock(NotificationFacade.class);
  }

  @Bean(name = "InteractiveManager")
  @Lazy
  public InteractiveManager interactiveManager() throws IOException {
    return mock(InteractiveManager.class);
  }

  @Bean(name = "JournalProvider")
  public JournalProvider journalProvider() {
    JournalProviderTest journalProvidermock = new JournalProviderTest(actorSystem());
    return journalProvidermock;
  }

  @Bean
  public OTPUtils OTPUtils() {
    return mock(OTPUtils.class);
  }

  @Bean(name = "WDProjections")
  public ProjectionsClient projectionFromWDService() throws IOException {
    return mock(ProjectionsClient.class);
  }

  @Bean(name = "CognitoUserPoolForInvestor")
  public AWSCognitoIdentityProvider investorUserPoolMock() {
    return mock(AWSCognitoIdentityProvider.class);
  }

  @Bean(name = "dataSource")
  public DataSource dataSource() throws IOException {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName(wdProperties.rdsDriverClass());
    dataSource.setUsername(wdProperties.rdsUsername());
    dataSource.setPassword(wdProperties.rdsPassword());
    dataSource.setUrl(wdProperties.rdsUrl());
    return dataSource;
  }

  @Bean(name = "SNSClient")
  public AmazonSNS snsClientCreation() {
    return mock(AmazonSNS.class);
  }

  @Bean(name = "SNSClientForSMS")
  public AmazonSNS snsClientSMSRegionCreation() {
    return mock(AmazonSNS.class);
  }

  @Bean("sesClient")
  public AmazonSimpleEmailService sesClient() {
    return mock(AmazonSimpleEmailService.class);
  }
  
  @Bean
  public AmazonCloudWatch amazonCloudWatch() {
    return mock(AmazonCloudWatch.class);
  }
  
  @Bean
  public AWSLogs awsLogs() {
    return mock(AWSLogs.class);
  }
}