package com.wt.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wt.config.utils.ExecuteSqlSchema;
import com.wt.utils.db.DropSchema;

@Component
public class CoreDBTest {
  @Autowired
  private ExecuteSqlSchema executeSqlSchema;
  @Autowired
  private DropSchema dropSchema;
  
  public void createSchema() throws Exception {
    executeSqlSchema.setupDB();
  }

  public void setupDB() throws Exception {
    createSchema();
  }
  
  public void dropSchema() throws Exception {
    dropSchema.dropDB();
  }
}