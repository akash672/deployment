package com.wt.utils;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;

import org.junit.Test;

import com.wt.domain.write.commands.IssueAdvice;

public class CommandValidationShould {
  @Test(expected = Exception.class)
  public void testInvalidAdviceAllocation() throws Exception {
    IssueAdvice advise = new IssueAdvice("Test", "Test", "1", "FSL", 0., 100.0, eodDateFromExcelDate("28/02/2013"), 103.0,
        eodDateFromExcelDate("28/02/2013"), eodDateFromExcelDate("28/02/2013"));
    advise.validate(100.);
  }

  @Test(expected = Exception.class)
  public void testFundCashCapacity() throws Exception {
    IssueAdvice advise = new IssueAdvice("Test", "Test", "1", "FSL", 50., 100.0, eodDateFromExcelDate("28/02/2013"), 103.0,
        eodDateFromExcelDate("28/02/2013"), eodDateFromExcelDate("28/02/2013"));
    advise.validate(20.);
  }
}
