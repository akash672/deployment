package com.wt.utils;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class DateUtilsShould {
  @Test
  public void extract_date_info_in_IST() throws Exception {
    assertThat(eodDateFromExcelDate("27/09/2013")).isEqualTo(1380276000000L);
  }

  @Test public void dateUtils_eod_date_not_from_system_environment()
	{
	  String date = DateUtils.eodDateToday();
	  assertEquals(date, DateUtils.todayPrettyTime());
	} 
  
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void dateUtils_eod_date_from_system_environment() {
      environmentVariables.set("WT_EOD_DATE", "01/01/2016");
      assertEquals("01/01/2016", System.getenv("WT_EOD_DATE"));
    }
  
}


