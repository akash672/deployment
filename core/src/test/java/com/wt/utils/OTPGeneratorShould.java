package com.wt.utils;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TestAppConfiguration;
import com.wt.config.utils.WDProperties;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class OTPGeneratorShould {

  private final String ATTEMPTED_OTP = "8999";
  private final String OTP = "8999";
  private OTPUtils otpUtils;
  @Autowired
  private WDProperties properties;

  @Before
  public void setupOTPUtils() {
    otpUtils = new OTPUtils(properties);
  }

  @Test
  public void test_otp_generated() {
    int otp = Integer.parseInt(otpUtils.getNewOtp());
    assertTrue((otp > 1000) && (otp < 10000));
  }

  @Test
  public void test_otp_comparision() throws IOException {
    assertTrue(otpUtils.isOTPTimeValid(ATTEMPTED_OTP, OTP, DateUtils.currentTimeInMillis() - (2 * 60 * 1000)));
  }

}
