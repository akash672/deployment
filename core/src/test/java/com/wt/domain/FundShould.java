package com.wt.domain;

import static com.wt.utils.DateUtils.eodDateFromExcelDate;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.text.ParseException;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundFloated;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.akka.CancellableStream;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;

@RunWith(MockitoJUnitRunner.class)
public class FundShould {
  @Mock
  private CtclBroadcast broadcast;
  private ActorSystem system;
  private ActorMaterializer materializer;
  private String wdId = "ISIN123-NSE-EQ";
  private String symbol = "ACC";
  PriceWithPaf entry;
  PriceWithPaf price1;
  PriceWithPaf price2;

  @Before
  public void init() throws ParseException, IOException {
    system = ActorSystem.create();
    materializer = ActorMaterializer.create(system);
    entry = new PriceWithPaf(new InstrumentPrice(10, eodDateFromExcelDate("20/01/2015"), wdId),
        Pafs.withCumulativePaf(1.));
    price1 = new PriceWithPaf(new InstrumentPrice(11, eodDateFromExcelDate("20/01/2015"), wdId),
        Pafs.withCumulativePaf(1.));
    price2 = new PriceWithPaf(new InstrumentPrice(12, eodDateFromExcelDate("20/01/2015"), wdId),
        Pafs.withCumulativePaf(1.));

    Source<PriceWithPaf, NotUsed> source = Source.from(Lists.newArrayList(price1, price2));
    CancellableStream<PriceWithPaf, NotUsed> stream = new CancellableStream<>(source, ActorRef.noSender());
    doReturn(stream).when(broadcast).subscribeStream(new EquityInstrument(wdId, "", symbol, "", ""), materializer);
  }

  @Test
  public void calculate_realtime_performance() throws Exception {
    Fund fund = new Fund();
    String adviserUsername = "swapnil";
    String adviserName = "Swapnil Joshi";
    String fundName = "WeeklyPicks";
    fund.update(new FundFloated(adviserUsername, adviserName, fundName, eodDateFromExcelDate("01/01/2015"),
        eodDateFromExcelDate("31/12/2079"), fundName + "-Description", "Theme", 6500, 20500, RiskLevel.MEDIUM));
    fund.update(
        new FundAdviceIssued(adviserUsername, fundName, "1", wdId, symbol,  10., 100.0, eodDateFromExcelDate("01/01/2015"),
            entry, eodDateFromExcelDate("20/01/2015"), entry, eodDateFromExcelDate("01/01/2015"), false));
  }

}
