package com.wt.domain;

import static com.wt.utils.DateUtils.currentTimeInMillis;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;

@RunWith(JUnit4.class)
public class FundConstituentSetShould {
  private static final String ADVISEID = "1asda445sdf";
  private static final String FUNDNAME = "FUND";
  private static final String ADVISERNAME = "ADVISER";
  private static final String TICKER = "TICKER";
  private static final double ALLOCATION_1 = 20;
  private static final PriceWithPaf ENTRYPRICE = new PriceWithPaf(new InstrumentPrice(10, currentTimeInMillis(), TICKER), Pafs.withCumulativePaf(1.));
  private static final PriceWithPaf EXITPRICE = new PriceWithPaf(new InstrumentPrice(20, currentTimeInMillis(), TICKER), Pafs.withCumulativePaf(1.));
  private static final double ALLOCATION_2 = 10;
  private Set<FundConstituent> allFundConstituents = new HashSet<FundConstituent>();
  
  @Test
  public void should_contain_unique_fund_constituents() {
    FundConstituent firstFundConstituent = new FundConstituent(ADVISEID, FUNDNAME, ADVISERNAME, TICKER, ALLOCATION_1, ENTRYPRICE, EXITPRICE);
    FundConstituent secondFundConstituent = new FundConstituent(ADVISEID, FUNDNAME, ADVISERNAME, TICKER, ALLOCATION_2, ENTRYPRICE, EXITPRICE);
    allFundConstituents.add(firstFundConstituent);
    allFundConstituents.add(secondFundConstituent);
    assertThat(allFundConstituents.size()).isEqualTo(1);
  }
}
