akka.persistence.journal.plugin = "eventstore.persistence.journal"
akka.persistence.snapshot-store.plugin = "eventstore.persistence.snapshot-store"

akka.persistence.snapshot-store-plugin-fallback.circuit-breaker {
        max-failures = 100
        call-timeout = 3600s
        reset-timeout = 60s
}

akka.persistence.journal-plugin-fallback.circuit-breaker {
        max-failures = 100
        call-timeout = 3600s
        reset-timeout = 60s
}

queryJournalClass = "akka.persistence.eventstore.query.javadsl.EventStoreReadJournal"
queryIdentifier = "eventstore.persistence.query"

akka {
  actor {
  	provider = remote
    serializers {
      jsonSer = "com.wt.domain.serializers.JsonSerializer"
    }

    serialization-bindings {
      "akka.persistence.PersistentRepr" = jsonSer
    }
    
  	guardian-supervisor-strategy = "com.wt.utils.akka.ResilientSupervisorStrategy"
  }
  remote {
 	
      enabled-transports = ["akka.remote.netty.tcp"]
      netty.tcp {
        hostname = "127.0.0.1"
        port = 2559
      }
 }
}

eventstore {
  # IP & port of Event Store
  address {
    host = "127.0.0.1"
    port = 1114
  }

  http {
    protocol = "http"
    port = 2114
    prefix = ""
  }

  # The desired connection timeout
  connection-timeout = 1s

  # Maximum number of reconnections before backing, -1 to reconnect forever
  max-reconnections = 100

  reconnection-delay {
    # Delay before first reconnection
    min = 250ms
    # Maximum delay on reconnections
    max = 10s
  }

  # The default credentials to use for operations where others are not explicitly supplied.
  credentials {
    login = "admin"
    password = "changeit"
  }

  heartbeat {
    # The interval at which to send heartbeat messages.
    interval = 5000ms
    # The interval after which an unacknowledged heartbeat will cause the connection to be considered faulted and disconnect.
    timeout = 50s
  }

  operation {
    # The maximum number of operation retries
    max-retries = 10
    # The amount of time before an operation is considered to have timed out
    timeout = 30s
  }

  # Whether to resolve LinkTo events automatically
  resolve-linkTos = false

  # Whether or not to require EventStore to refuse serving read or write request if it is not master
  require-master = true

  # Number of events to be retrieved by client as single message
  read-batch-size = 250

  # The size of the buffer in element count
  buffer-size = 100000

  # Strategy that is used when elements cannot fit inside the buffer
  # Possible values DropHead, DropTail, DropBuffer, DropNew, Fail
  buffer-overflow-strategy = "Fail"

  # The number of serialization/deserialization functions to be run in parallel
  serialization-parallelism = 8

  # Serialization done asynchronously and these futures may complete in any order,
  # but results will be used with preserved order if set to true
  serialization-ordered = true

  cluster {
    # Endpoints for seeding gossip
    # For example: ["127.0.0.1:1", "127.0.0.2:2"]
    gossip-seeds = ["127.0.0.2:2113", "127.0.0.3:2113"]

    # The DNS name to use for discovering endpoints
    dns = null

    # The time given to resolve dns
    dns-lookup-timeout = 2s

    # The well-known endpoint on which cluster managers are running
    external-gossip-port = 30778

    # Maximum number of attempts for discovering endpoints
    max-discover-attempts = 10

    # The interval between cluster discovery attempts
    discover-attempt-interval = 500ms

    # The interval at which to keep discovering cluster
    discovery-interval = 1s
  
    # Timeout for cluster gossip
    gossip-timeout = 1s
  }

  persistent-subscription {
    # Whether to resolve LinkTo events automatically
    resolve-linkTos = false

    # Where the subscription should start from (position)
    start-from = last

    # Whether or not in depth latency statistics should be tracked on this subscription.
    extra-statistics = false

    # The amount of time after which a message should be considered to be timedout and retried.
    message-timeout = 30s

    # The maximum number of retries (due to timeout) before a message get considered to be parked
    max-retry-count = 500

    # The size of the buffer listening to live messages as they happen
    live-buffer-size = 500

    # The number of events read at a time when paging in history
    read-batch-size = 10

    # The number of events to cache when paging through history
    history-buffer-size = 20

    # The amount of time to try to checkpoint after
    checkpoint-after = 2s

    # The minimum number of messages to checkpoint
    min-checkpoint-count = 10

    # The maximum number of messages to checkpoint if this number is a reached a checkpoint will be forced.
    max-checkpoint-count = 1000

    # The maximum number of subscribers allowed
    max-subscriber-count = 0

    # The [[ConsumerStrategy]] to use for distributing events to client consumers
    # Known are RoundRobin, DispatchToSingle
    # however you can provide a custom one, just make sure it is supported by server
    consumer-strategy = RoundRobin
  }
}