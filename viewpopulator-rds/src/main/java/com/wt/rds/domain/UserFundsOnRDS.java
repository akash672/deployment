package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "userFund")
@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public class UserFundsOnRDS implements Serializable{
  
  
  private static final long serialVersionUID = 1L;
  
  @EmbeddedId
  private  UserFundsOnRDSIdentity userFundsOnRDSIdentity;
  
  @Column(name = "subscriptionTimeStamp" )
  private long subscriptionTimeStamp;
  
  @Column(name = "unsubscriptionTimeStamp")
  private long unsubscriptionTimeStamp;
  

  public UserFundsOnRDS(UserFundsOnRDSIdentity userFundsOnRDSIdentity) {
    super();
    this.userFundsOnRDSIdentity = userFundsOnRDSIdentity;
  }
  
  

}
