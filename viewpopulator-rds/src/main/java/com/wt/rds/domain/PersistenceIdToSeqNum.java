package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "persistenceIdToSeqNum")
@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersistenceIdToSeqNum implements Serializable{
    
   
  private static final long serialVersionUID = 1L;
  
  @Id
  private @Setter String persistenceId;
  
  @Column(name = "seqNum")
  private long seqNum;

}
