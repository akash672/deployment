package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
@Table(name = "sequenceStore")
@NoArgsConstructor
@AllArgsConstructor
public class SequenceStoreData implements Serializable{
  
  private static final long serialVersionUID = 1L;

  @Id
  private String eventType;
  
  @Column(name = "seqNum")
  private long seqNum;

  public SequenceStoreData(String eventType) {
    super();
    this.eventType = eventType;
  }
  
  
}
