package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "user")
@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public class UsersRDSData implements Serializable{

  private static final long serialVersionUID = 1L;
  
  @EmbeddedId
  private UserRDSIdentity userRDSIdentity;
  
  @Column(name = "onboardedTimeStamp")
  private long onboardedTimeStamp;
  
  @Column(name = "expiryTimeStamp")
  private long expiryTimeStamp;
  
  @Column(name = "clientCode")
  private String clientCode;


  public UsersRDSData(UserRDSIdentity userRDSIdentity) {
    super();
    this.userRDSIdentity = userRDSIdentity;
  }
  

}
