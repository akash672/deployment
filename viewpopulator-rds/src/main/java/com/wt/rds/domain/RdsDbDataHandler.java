package com.wt.rds.domain;

import java.util.LinkedList;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RdsDbDataHandler {
  
  public LinkedList<RDSEventObject> dbObjectsLinkedList();
  
  @SuppressWarnings("rawtypes")
  public JpaRepository repo();

}
