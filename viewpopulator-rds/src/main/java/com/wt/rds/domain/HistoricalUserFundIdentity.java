package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HistoricalUserFundIdentity implements Serializable{

  private static final long serialVersionUID = 1L;
  
  @Column(name = "username")
  private String username;
  
  @Column(name = "fundNameWithAdviserUsername")
  private String fundNameWithAdviserUsername;
  
  @Column(name = "investingMode")
  private String investingMode;
  
  @Column(name = "lastUpdatedTime")
  private long lastUpdatedTime;
}
