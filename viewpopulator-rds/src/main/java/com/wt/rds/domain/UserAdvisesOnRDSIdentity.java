package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserAdvisesOnRDSIdentity implements Serializable{

  private static final long serialVersionUID = 1L;
  
  @Column(name = "username")
  private String username;
  
  @Column(name = "investingMode")
  private String investingMode;
  
  @Column(name = "fundNameWithAdviserUsername")
  private String fundNameWithAdviserUsername;
  
  @Column(name = "adviseId")
  private String adviseId;
  

  public UserAdvisesOnRDSIdentity(String username, String investingMode, String fundNameWithAdviserUsername,
      String adviseId) {
    super();
    this.username = username;
    this.investingMode = investingMode;
    this.fundNameWithAdviserUsername = fundNameWithAdviserUsername;
    this.adviseId = adviseId;
  }

  
}
