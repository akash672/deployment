package com.wt.rds.domain;


import java.util.List;
import java.util.Map.Entry;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RDSEventObject {
  private List<Entry<Object, Object>> data;
  private List<Entry<Object, Object>> seqNumdata;

  public RDSEventObject(List<Entry<Object, Object>> data, List<Entry<Object, Object>> seqNumdata) {
    super();
    this.data = data;
    this.seqNumdata = seqNumdata;
  }

 
}
