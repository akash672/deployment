package com.wt.rds.domain;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Lazy
public interface UserFundsOnRDSRepo extends JpaRepository<UserFundsOnRDS, UserFundsOnRDSIdentity> {
  @Modifying(clearAutomatically = true)
  @Transactional(readOnly = false)
  @Query("update UserFundsOnRDS c set c.unsubscriptionTimeStamp =:unsubscriptionTimeStamp WHERE c.userFundsOnRDSIdentity.username = :username AND c.userFundsOnRDSIdentity.investingMode = :investingMode AND c.userFundsOnRDSIdentity.fundNameWithAdviserUsername = :fundNameWithAdviserUsername")
  public void updateUnsubscriptionTime(@Param("unsubscriptionTimeStamp") Long unsubscriptionTimeStamp,
      @Param("username") String username,  @Param("investingMode") String investingMode, @Param("fundNameWithAdviserUsername") String fundNameWithAdviserUsername);

}
