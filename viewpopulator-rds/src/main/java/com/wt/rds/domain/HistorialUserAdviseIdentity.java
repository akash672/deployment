package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@ToString
public class HistorialUserAdviseIdentity implements Serializable{
  
  
  private static final long serialVersionUID = 1L;
  
  @Column(name = "adviseId")
  private String adviseId;
  
  @Column(name = "fundNameWithAdviserUsername")
  private String fundNameWithAdviserUsername;
  
  @Column(name = "lastUpdatedTime")
  private long lastUpdatedTime;

  @Column(name = "investingMode")
  private String investingMode;
  
  @Column(name = "username")
  private String username;
  
  public HistorialUserAdviseIdentity(String adviseId, String fundNameWithAdviserUsername, long lastUpdatedTime,
      String investingMode, String username) {
    super();
    this.adviseId = adviseId;
    this.fundNameWithAdviserUsername = fundNameWithAdviserUsername;
    this.lastUpdatedTime = lastUpdatedTime;
    this.investingMode = investingMode;
    this.username = username;
  }

}
