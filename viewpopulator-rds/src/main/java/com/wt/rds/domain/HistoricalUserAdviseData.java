package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "userAdviseTransaction")
@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public class HistoricalUserAdviseData implements Serializable{
  
  private static final long serialVersionUID = 1L;
  
  @EmbeddedId
  private HistorialUserAdviseIdentity historialUserAdviseIdentity;
  
  @Column(name = "orderSide")
  private String orderSide ;
  
  @Column(name = "amountBought")
  private double amountBought;
  
  @Column(name = "wdId")
  private String wdId;
  
  @Column(name = "amountSold")
  private double amountSold;
  
  @Column(name = "numberOfSharesTransacted")
  private double numberOfSharesTransacted;
  
  
  public HistoricalUserAdviseData(HistorialUserAdviseIdentity historialUserAdviseIdentity) {
    super();
    this.historialUserAdviseIdentity = historialUserAdviseIdentity;
  }
  
}
