package com.wt.rds.domain;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public class UserFundDividendsRdsDbDataHandler implements RdsDbDataHandler{
  
  @Autowired
  private HistoricalFundDividendRepo historicalUserFundDividendsRepo;
  
  private LinkedList<RDSEventObject> userFundDividendsData = new LinkedList<RDSEventObject>();

  @Override
  public LinkedList<RDSEventObject> dbObjectsLinkedList() {
    return userFundDividendsData;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public JpaRepository repo() {
    return historicalUserFundDividendsRepo;
  }

  public UserFundDividendsRdsDbDataHandler(HistoricalFundDividendRepo historicalUserFundDividendsRepo) {
    super();
    this.historicalUserFundDividendsRepo = historicalUserFundDividendsRepo;
  }

  
}
