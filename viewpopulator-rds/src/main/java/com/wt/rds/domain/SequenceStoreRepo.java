package com.wt.rds.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SequenceStoreRepo  extends JpaRepository<SequenceStoreData, String>{
      
}
