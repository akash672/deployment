package com.wt.rds.domain;

import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wt.domain.write.events.FundEvent;
import com.wt.domain.write.events.FundFloated;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "fund")
@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public class HistoricalFundData implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private @Setter String fundNameWithAdviserUserName;

  @Column(name = "startdate")
  private long startDate;

  @Column(name = "performancecalculatedTill")
  private long performancecalculatedTill;
  
  @Column(name = "endDate")
  private long endDate;

  public HistoricalFundData(String fundNameWithAdviserUserName) {
    super();
    this.fundNameWithAdviserUserName = fundNameWithAdviserUserName;
  }

  public void update(FundEvent evt) {
    if (evt instanceof FundFloated) {
      fundFloated(evt);
    } 
  }

  private void fundFloated(FundEvent evt) {
    FundFloated fundFloated = (FundFloated) evt;
    setFundNameWithAdviserUserName(
        fundFloated.getFundName() + CompositeKeySeparator + fundFloated.getAdviserUsername());
    setStartDate(fundFloated.getStartDate());
    setEndDate(0L);
  }
}
