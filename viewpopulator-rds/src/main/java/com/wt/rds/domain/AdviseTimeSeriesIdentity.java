package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@ToString
public class AdviseTimeSeriesIdentity implements Serializable{


  private static final long serialVersionUID = 1L;
  
  @Column(name = "adviseId")
  private String adviseId;
  
  @Column(name = "fundNameWithAdviserUsername")
  private String fundNameWithAdviserUsername;
  
  @Column(name = "asOfDate")
  private long asOfDate;

  public AdviseTimeSeriesIdentity(String adviseId, String fundNameWithAdviserUsername, long asOfDate) {
    super();
    this.adviseId = adviseId;
    this.fundNameWithAdviserUsername = fundNameWithAdviserUsername;
    this.asOfDate = asOfDate;
  }
  
  
  
}
