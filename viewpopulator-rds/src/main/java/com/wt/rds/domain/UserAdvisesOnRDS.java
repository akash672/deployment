package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "userAdvise")
@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public class UserAdvisesOnRDS implements Serializable {

  private static final long serialVersionUID = 1L;
  @EmbeddedId
  private UserAdvisesOnRDSIdentity userAdvisesOnRDSIdentity;

  @Column(name = "adviseStartTime")
  private long adviseStartTime;

  @Column(name = "adviseEndTime")
  private long adviseEndTime;

  public UserAdvisesOnRDS(UserAdvisesOnRDSIdentity userAdvisesOnRDSIdentity) {
    super();
    this.userAdvisesOnRDSIdentity = userAdvisesOnRDSIdentity;
  }

}
