package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table(name = "userFundDividendTransaction")
@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public class HistoricalFundDividendData implements Serializable {

  private static final long serialVersionUID = 1L;

  @EmbeddedId
  private HistoricalUserFundIdentity historialFundIdentity;

  @Column(name = "dividendValue")
  private double dividendValue;

  public HistoricalFundDividendData(HistoricalUserFundIdentity historialFundIdentity) {
    super();
    this.historialFundIdentity = historialFundIdentity;
  }

}
