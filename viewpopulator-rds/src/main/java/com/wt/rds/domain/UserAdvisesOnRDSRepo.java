package com.wt.rds.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserAdvisesOnRDSRepo extends JpaRepository<UserAdvisesOnRDS, UserAdvisesOnRDSIdentity>{
  public UserAdvisesOnRDS findByUserAdvisesOnRDSIdentity(UserAdvisesOnRDSIdentity userAdvisesOnRDSIdentity);
  
  @Modifying(clearAutomatically = true)
  @Transactional(readOnly = false)
  @Query("update UserAdvisesOnRDS c set c.adviseEndTime =:adviseEndTime WHERE c.userAdvisesOnRDSIdentity.username = :username AND c.userAdvisesOnRDSIdentity.investingMode = :investingMode AND c.userAdvisesOnRDSIdentity.fundNameWithAdviserUsername = :fundNameWithAdviserUsername AND c.userAdvisesOnRDSIdentity.adviseId = :adviseId" )
  public void updateAdviseCloseTime(@Param("adviseEndTime") Long adviseEndTime,
      @Param("username") String username,  @Param("investingMode") String investingMode, @Param("fundNameWithAdviserUsername") String fundNameWithAdviserUsername,
      @Param("adviseId") String adviseId);

}
