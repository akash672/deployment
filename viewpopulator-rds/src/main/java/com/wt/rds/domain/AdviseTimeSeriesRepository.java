package com.wt.rds.domain;


import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdviseTimeSeriesRepository extends JpaRepository<AdviseTimeSeriesData, AdviseTimeSeriesIdentity>{
  public AdviseTimeSeriesData findTopByAdviseIdentityAdviseIdOrderByAdviseIdentityAsOfDateDesc(String AdviseId) throws DataAccessException;
}
