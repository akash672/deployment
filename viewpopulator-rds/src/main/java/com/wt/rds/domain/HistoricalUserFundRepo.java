package com.wt.rds.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoricalUserFundRepo extends JpaRepository<HistoricalUserFundData, HistoricalUserFundIdentity> {
  }
