package com.wt.rds.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wt.domain.OrderSide;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundEvent;
import com.wt.utils.DoublesUtil;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
@Table(name = "adviseTransaction")
@NoArgsConstructor
public class AdviseTimeSeriesData implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @EmbeddedId
  private AdviseTimeSeriesIdentity adviseIdentity;
  
  @Column(name = "wdId")
  private String wdId;
  
  @Column(name = "allocation")
  private double allocation;
  
  @Column(name = "orderSide")
  private String orderSide;
  
  @Column(name = "entryPrice")
  private double entryPrice;

  @Column(name = "exitPrice")
  private double exitPrice;
  
  
  public void update(FundEvent evt) {
    if (evt instanceof FundAdviceIssued) {
      updateAsIssuedAdvise(evt);
    } 
    if (evt instanceof FundAdviceClosed) {
      updateAsClosedAdvise(evt);
    } 
  }

  private void updateAsClosedAdvise(FundEvent evt) {
    FundAdviceClosed closedAdvise = (FundAdviceClosed) evt;
    this.wdId = closedAdvise.getExitPrice().getPrice().getWdId();
    this.allocation = closedAdvise.getAllocation();
    this.orderSide = OrderSide.SELL.toString();
    this.entryPrice = 0d;
    this.exitPrice = DoublesUtil.round(closedAdvise.getExitPrice().getPrice().getPrice() * closedAdvise.getExitPrice().getCumulativePaf());
  }


  private void updateAsIssuedAdvise(FundEvent evt) {
    FundAdviceIssued issuedAdvise = (FundAdviceIssued) evt;
    this.wdId = issuedAdvise.getToken();
    this.allocation = issuedAdvise.getAllocation();
    this.orderSide = OrderSide.BUY.toString();
    this.entryPrice =  DoublesUtil.round(issuedAdvise.getEntryPrice().getPrice().getPrice() * issuedAdvise.getEntryPrice().getCumulativePaf());
    this.exitPrice = 0d;
  }

  public AdviseTimeSeriesData(AdviseTimeSeriesIdentity adviseIdentity) {
    super();
    this.adviseIdentity = adviseIdentity;
  }
  
}
