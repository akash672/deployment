package com.wt.rds.domain;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Component
@Getter
@Setter
@NoArgsConstructor
public class UserAdviseRdsDbDataHandler implements RdsDbDataHandler {
  
  @Autowired
  private HistoricalUserAdviseDataRepo historicalUserAdviseRepo;
  
  private LinkedList<RDSEventObject> userAdviseData = new LinkedList<RDSEventObject>();

  @Override
  public LinkedList<RDSEventObject> dbObjectsLinkedList() {
    return userAdviseData;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public JpaRepository repo() {
    return historicalUserAdviseRepo;
  }

  public UserAdviseRdsDbDataHandler(HistoricalUserAdviseDataRepo historicalUserAdviseRepo) {
    super();
    this.historicalUserAdviseRepo = historicalUserAdviseRepo;
  }
  
  

}
