package com.wt.rds.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoricalFundDividendRepo extends JpaRepository<HistoricalFundDividendData, HistoricalUserFundIdentity>{

}
