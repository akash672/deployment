package com.wt.rds.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UsersOnRDSRepo extends JpaRepository<UsersRDSData, UserRDSIdentity>{
  
  @Modifying(clearAutomatically = true)
  @Transactional(readOnly = false)
  @Query("update UsersRDSData c set c.clientCode =:clientCode WHERE c.userRDSIdentity.username = :username AND c.userRDSIdentity.investingMode = :investingMode")
  public void updateUserClientCode(@Param("clientCode") String clientCode,
      @Param("username") String username, @Param("investingMode") String investingMode);
  
  @Modifying(clearAutomatically = true)
  @Transactional(readOnly = false)
  @Query("update UsersRDSData c set c.expiryTimeStamp =:expiryTimeStamp WHERE c.userRDSIdentity.username = :username AND c.userRDSIdentity.investingMode = :investingMode")
  public void updateUserVirtualExpiryTime(@Param("expiryTimeStamp") long expiryTimeStamp,
      @Param("username") String username, @Param("investingMode") String investingMode);
}
