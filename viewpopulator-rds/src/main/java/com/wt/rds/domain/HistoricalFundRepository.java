package com.wt.rds.domain;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface HistoricalFundRepository extends JpaRepository<HistoricalFundData, String> {
  public HistoricalFundData fundNameWithAdviserUserName(String fundNameWithAdviserUserName) throws DataAccessException;
  
  @Modifying(clearAutomatically = true)
  @Transactional(readOnly = false)
  @Query("update HistoricalFundData c set c.endDate =:endDate WHERE c.fundNameWithAdviserUserName= :fundNameWithAdviserUserName")
  public void updateFundEndDate(@Param("endDate") long endDate,
      @Param("fundNameWithAdviserUserName") String fundNameWithAdviserUserName);
}
