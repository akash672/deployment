package com.wt.rds.domain;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class UserFundRdsDbDataHandler implements RdsDbDataHandler {
  
  @Autowired
  private HistoricalUserFundRepo historicalUserFundRepo;
  
  private LinkedList<RDSEventObject> userfundData = new LinkedList<RDSEventObject>();

  @Override
  public LinkedList<RDSEventObject> dbObjectsLinkedList() {
    return userfundData;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public JpaRepository repo() {
    return historicalUserFundRepo;
  }

  public UserFundRdsDbDataHandler(HistoricalUserFundRepo historicalUserFundRepo) {
    super();
    this.historicalUserFundRepo = historicalUserFundRepo;
  }
  
  
  
}
