package com.wt.utils;

public class RetryingFailedException extends Exception {
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  String persistenceId;
  public RetryingFailedException(String msg)
  {
    super(msg);
    this.persistenceId = msg;
  }
  
  public String getPersistenceId()
  {
    return persistenceId;
  }

}
