package com.wt.utils;

import static com.wt.utils.CommonConstants.CommaSeparated;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Properties;

import org.springframework.stereotype.Component;

@Component
public class RdsEventsConfig {
  
  public Object property(String key) throws IOException {
    String value = System.getProperty(key);
    if (value != null)
      return value;
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("rds-files.config"));

    return properties.getProperty(key);
  }
  
  public String eventStreamPrefix() throws IOException {
    String key = "stream.prefix";
    return property(key).toString();
  }
  
  public LinkedList<String> eventNamesAsLinkedList() throws Exception {
    LinkedList<String> linkedListOfEvents = new LinkedList<>(Arrays.asList(((String) property("event.names")).split(CommaSeparated)));
    return linkedListOfEvents;
  }

}
