package com.wt.utils;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;

@EnableAsync
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "com.wt")
@EnableJpaRepositories(basePackages = "com.wt.rds.domain" , entityManagerFactoryRef = "entityManager" , transactionManagerRef = "transactionManager")
@PropertySource("classpath:environment.properties")
public class ViewPopulatorRdsAppConfiguration extends CoreAppConfiguration {
  
  @Autowired
  public ViewPopulatorRdsAppConfiguration(ApplicationContext applicationContext) {
    super(applicationContext);
  }
  
  @Bean(name = "dataSource")
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    
    dataSource.setDriverClassName(wdProperties.rdsDriverClass());
    dataSource.setUsername(wdProperties.rdsUsername());
    dataSource.setPassword(wdProperties.rdsPassword());
    dataSource.setUrl(wdProperties.rdsUrl().concat("/").concat(wdProperties.environment()).concat("_WDTimeSeries"));

    return dataSource;
  }
  
  @Bean(name = "entityManager")
  public LocalContainerEntityManagerFactoryBean entityManager() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource());
    em.setPackagesToScan(new String[] { "com.wt.rds.domain" });

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    HashMap<String, Object> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", "update");
    properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
    em.setJpaPropertyMap(properties);

    return em;
  }
  
  @Bean(name = "transactionManager")
  public PlatformTransactionManager transactionManager() {

    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManager().getObject());
    return transactionManager;
  }
}
