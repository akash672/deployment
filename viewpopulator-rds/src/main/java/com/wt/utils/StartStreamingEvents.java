package com.wt.utils;

import java.io.Serializable;

import akka.stream.Materializer;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class StartStreamingEvents implements Serializable{

  private static final long serialVersionUID = 1L;
  private Materializer materializer;
  

}
