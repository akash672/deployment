package com.wt.utils;

import static com.wt.utils.CommonConstants.DOT;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wt.rds.domain.SequenceStoreData;
import com.wt.rds.domain.SequenceStoreRepo;

import akka.actor.ActorRef;
import akka.stream.Materializer;
import lombok.extern.log4j.Log4j;

@Service("ViewPopulatorRds")
@Log4j
public class ViewPopulatorRds {
  
  private JournalProvider journalProvider;
  private RdsEventsConfig eventsConfig;
  private SequenceStoreRepo rdsSequenceStoreRepo;
  private Map<String, Long> eventStreamToSeqNum = new HashMap<>();

  
  
  @Autowired
  public ViewPopulatorRds(JournalProvider journalProvider, RdsEventsConfig eventsConfig,
      SequenceStoreRepo rdsSequenceStoreRepo) {
    super();
    this.journalProvider = journalProvider;
    this.eventsConfig = eventsConfig;
    this.rdsSequenceStoreRepo = rdsSequenceStoreRepo;
  }
  
  @PostConstruct
  public void updateTheEventStreamSeqNumbers(){
    List<SequenceStoreData> dataFromRepo = rdsSequenceStoreRepo.findAll();
    dataFromRepo.stream().forEach(repoData -> {
      eventStreamToSeqNum.put(repoData.getEventType(), repoData.getSeqNum());
    });
  }

  public void startPopulatingView(String eventName, ActorRef viewPopulatorRdsRoot, Materializer materializer) throws Exception {
    String streamId = eventsConfig.eventStreamPrefix() + DOT + eventName;
    long seqNum = eventStreamToSeqNum.containsKey(eventName) ? eventStreamToSeqNum.get(eventName) + 1 : 0;
    try{
      journalProvider.getSubscribedEvents(streamId, seqNum, viewPopulatorRdsRoot);
    } catch (Exception e){
      log.error("ViewPopulatorRds exception in getting subscribed events " + e);
    }
  }
}
