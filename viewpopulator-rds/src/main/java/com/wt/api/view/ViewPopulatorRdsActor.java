package com.wt.api.view;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.wt.aws.cognito.exception.InternalErrorException;
import com.wt.domain.write.commands.FinishedProcessingCurrentStream;
import com.wt.utils.RdsEventsConfig;
import com.wt.utils.RetryingFailedException;
import com.wt.utils.StartStreamingEvents;
import com.wt.utils.ViewPopulatorRds;

import akka.actor.UntypedAbstractActor;
import akka.persistence.query.EventEnvelope;
import akka.stream.Materializer;
import lombok.extern.log4j.Log4j;

@Scope("prototype")
@Service("ViewPopulatorRdsActor")
@Log4j
public class ViewPopulatorRdsActor  extends UntypedAbstractActor {
  
  private @Autowired CommonHandlerForRds rdsCommonHandler;
  private @Autowired ViewPopulatorRds viewPopulater;
  private @Autowired RdsEventsConfig config;
  private LinkedList<String> listOfEvents;
  private Materializer materializer;
  
  
  @Override
  public void onReceive(Object cmd) throws Throwable {
    
    if(cmd instanceof EventEnvelope){
      int retryCounter = 1;
      EventEnvelope eventEnvelope = (EventEnvelope) cmd;
      for (int tryCount = 1; tryCount <= 10; tryCount++) {
        try {
          rdsCommonHandler.tryToHandle(eventEnvelope);
          break;
        } catch (IOException | AmazonServiceException | IllegalArgumentException | InternalErrorException
            | ParseException  | SQLException e) {
          log.equals("Error is common handler rds tryToHandle " + e);
          retryCounter++;
        }
      }
      if (retryCounter >= 10) {
        throw new RetryingFailedException(eventEnvelope.persistenceId());
      }
    } else if(cmd instanceof StartStreamingEvents){
      StartStreamingEvents startStreamingEvents = (StartStreamingEvents) cmd;
      this.materializer = startStreamingEvents.getMaterializer();
      listOfEvents = config.eventNamesAsLinkedList();
      getStreamEventsForNextId();
    } else if(cmd instanceof FinishedProcessingCurrentStream){
      getStreamEventsForNextId();
    }
  }

  private void getStreamEventsForNextId() {
    String event = listOfEvents.pollFirst();
    if(event!=null){
      try {
        viewPopulater.startPopulatingView(event, self(), materializer);
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else{
      log.info("ViewPopulatorRdsActor: Finished requesting of event streamss");
    }
  }
  
}
