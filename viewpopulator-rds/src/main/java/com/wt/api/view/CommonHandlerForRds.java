package com.wt.api.view;

import static com.wt.domain.OrderSide.BUY;
import static com.wt.utils.CommonConstants.CompositeKeySeparator;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wt.aws.cognito.exception.InternalErrorException;
import com.wt.config.utils.WDProperties;
import com.wt.domain.InvestingMode;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.CognitoUserRegistered;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundClosed;
import com.wt.domain.write.events.FundFloated;
import com.wt.domain.write.events.FundLumpSumAdded;
import com.wt.domain.write.events.FundSubscribed;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.KycApprovedEvent;
import com.wt.domain.write.events.UpdateKYCDetails;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserFundAdviceIssuedWithUserEmail;
import com.wt.domain.write.events.UserFundDividendReceived;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundWithdrawalDoneSuccessfully;
import com.wt.domain.write.events.UserRegisteredFromUserPool;
import com.wt.rds.domain.AdviseTimeSeriesData;
import com.wt.rds.domain.AdviseTimeSeriesIdentity;
import com.wt.rds.domain.AdviseTimeSeriesRepository;
import com.wt.rds.domain.HistorialUserAdviseIdentity;
import com.wt.rds.domain.HistoricalFundData;
import com.wt.rds.domain.HistoricalFundDividendData;
import com.wt.rds.domain.HistoricalFundDividendRepo;
import com.wt.rds.domain.HistoricalFundRepository;
import com.wt.rds.domain.HistoricalUserAdviseData;
import com.wt.rds.domain.HistoricalUserAdviseDataRepo;
import com.wt.rds.domain.HistoricalUserFundData;
import com.wt.rds.domain.HistoricalUserFundIdentity;
import com.wt.rds.domain.HistoricalUserFundRepo;
import com.wt.rds.domain.RDSEventObject;
import com.wt.rds.domain.RdsDbDataHandler;
import com.wt.rds.domain.SequenceStoreData;
import com.wt.rds.domain.SequenceStoreRepo;
import com.wt.rds.domain.UserAdviseRdsDbDataHandler;
import com.wt.rds.domain.UserAdvisesOnRDS;
import com.wt.rds.domain.UserAdvisesOnRDSIdentity;
import com.wt.rds.domain.UserAdvisesOnRDSRepo;
import com.wt.rds.domain.UserFundDividendsRdsDbDataHandler;
import com.wt.rds.domain.UserFundRdsDbDataHandler;
import com.wt.rds.domain.UserFundsOnRDS;
import com.wt.rds.domain.UserFundsOnRDSIdentity;
import com.wt.rds.domain.UserFundsOnRDSRepo;
import com.wt.rds.domain.UserRDSIdentity;
import com.wt.rds.domain.UsersOnRDSRepo;
import com.wt.rds.domain.UsersRDSData;
import com.wt.utils.DateUtils;

import akka.persistence.query.EventEnvelope;
import lombok.extern.log4j.Log4j;

@Service("CommonHandlerForRds")
@Log4j
public class CommonHandlerForRds {

  private AdviseTimeSeriesRepository adviseTimeSeriesRepo;
  private HistoricalFundRepository historicalFundRepo;
  private HistoricalUserAdviseDataRepo historicalUserAdviseDataRepo;
  private HistoricalUserFundRepo historicalUserFundRepo;
  private UserFundsOnRDSRepo userFundsOnRDSRepo;
  private UserAdvisesOnRDSRepo userAdvisesOnRDSRepo;
  private UsersOnRDSRepo usersOnRDSRepo;
  private HistoricalFundDividendRepo historicalFundDividendRepo;
  private SequenceStoreRepo rdsSequenceStoreRepo;
  private final long milliSecondsInAMonth = 30 * 24 * 3600 * 1000L;
  
  WDProperties properties;
  
  Map<String, RdsDbDataHandler> eventTypeWithDataAndRepoMap = new HashMap<>();
  
  @Autowired
  public CommonHandlerForRds(AdviseTimeSeriesRepository adviseTimeSeriesRepo,
      HistoricalFundRepository historicalFundRepo, HistoricalUserAdviseDataRepo historicalUserAdviseDataRepo,
      HistoricalUserFundRepo historicalUserFundRepo, UserFundsOnRDSRepo userFundsOnRDSRepo,
      UserAdvisesOnRDSRepo userAdvisesOnRDSRepo, UsersOnRDSRepo usersOnRDSRepo,
      HistoricalFundDividendRepo historicalFundDividendRepo, 
      SequenceStoreRepo rdsSequenceStoreRepo, WDProperties properties) {
    super();
    this.adviseTimeSeriesRepo = adviseTimeSeriesRepo;
    this.historicalFundRepo = historicalFundRepo;
    this.historicalUserAdviseDataRepo = historicalUserAdviseDataRepo;
    this.historicalUserFundRepo = historicalUserFundRepo;
    this.userFundsOnRDSRepo = userFundsOnRDSRepo;
    this.userAdvisesOnRDSRepo = userAdvisesOnRDSRepo;
    this.usersOnRDSRepo = usersOnRDSRepo;
    this.historicalFundDividendRepo = historicalFundDividendRepo;
    this.rdsSequenceStoreRepo = rdsSequenceStoreRepo;
    this.properties = properties;
  }
  
  @PostConstruct
  public void initalizeTheDBDataMap(){
    UserFundRdsDbDataHandler userFundRDSDBData = new UserFundRdsDbDataHandler(historicalUserFundRepo);
    eventTypeWithDataAndRepoMap.put("userFundTransactions", userFundRDSDBData);
    
    UserAdviseRdsDbDataHandler userAdviseRDSDBData = new UserAdviseRdsDbDataHandler(historicalUserAdviseDataRepo);
    eventTypeWithDataAndRepoMap.put("userAdviseTransactions", userAdviseRDSDBData);
    
    UserFundDividendsRdsDbDataHandler userFundDividendsRDSDBData = new UserFundDividendsRdsDbDataHandler(historicalFundDividendRepo);
    eventTypeWithDataAndRepoMap.put("userFundDividendTransactions", userFundDividendsRDSDBData);
    
  }
  
  public void tryToHandle(EventEnvelope eventEnvelope)
      throws IOException, IllegalArgumentException, InternalErrorException, ParseException, SQLException  {
    Object event = eventEnvelope.event();
    log.debug("Handling on RDS common hanlder: " + eventEnvelope.persistenceId() + ", event number : "
        + eventEnvelope.sequenceNr() + ", event type: " + event.getClass());

    boolean updateDB = true;
    List<Entry<Object, Object>> userFundDataToBeUpdated = new ArrayList<Entry<Object, Object>>();
    List<Entry<Object, Object>> userAdviseDataToBeUpdated = new ArrayList<Entry<Object, Object>>();
    List<Entry<Object, Object>> userFundDividendToBeAdded = new ArrayList<Entry<Object, Object>>();
    List<Entry<Object, Object>> seqNumDataToAdd = new ArrayList<Entry<Object, Object>>();


    if (event instanceof FundFloated) {
      FundFloated floatedFund = (FundFloated) event;
      String fundNameWithAdviserUsername = floatedFund.getFundName() + CompositeKeySeparator
          + floatedFund.getAdviserUsername();
      HistoricalFundData fundData = new HistoricalFundData(fundNameWithAdviserUsername);
      fundData.update(floatedFund);
      historicalFundRepo.save(fundData);
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
    } else if (event instanceof FundAdviceIssued) {
      FundAdviceIssued fundAdviceIssued = (FundAdviceIssued) event;
      String fundNameWithAdviserUsername = fundAdviceIssued.getFundName() + CompositeKeySeparator
          + fundAdviceIssued.getAdviserUsername();

      AdviseTimeSeriesIdentity adviseTimeSeriesIdentity = new AdviseTimeSeriesIdentity(fundAdviceIssued.getAdviseId(),
          fundNameWithAdviserUsername, fundAdviceIssued.getIssueTime());
      
     AdviseTimeSeriesData adviseTransactionData = new AdviseTimeSeriesData(adviseTimeSeriesIdentity);
      
     adviseTransactionData.update(fundAdviceIssued);
     adviseTimeSeriesRepo.save(adviseTransactionData);
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));

    } else if (event instanceof FundAdviceClosed) {
      FundAdviceClosed closedAdvise = (FundAdviceClosed) event;
      String fundNameWithAdviserUsername = closedAdvise.getFundName() + CompositeKeySeparator
          + closedAdvise.getAdviserUsername();
      
      AdviseTimeSeriesIdentity adviseTimeSeriesIdentity = new AdviseTimeSeriesIdentity(closedAdvise.getAdviseId(),
          fundNameWithAdviserUsername, closedAdvise.getExitDate());
      AdviseTimeSeriesData adviseTransactionData = new AdviseTimeSeriesData(adviseTimeSeriesIdentity);

      adviseTransactionData.update(closedAdvise);
      adviseTimeSeriesRepo.save(adviseTransactionData);
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
    }    
    else if (event instanceof FundClosed) {
      FundClosed fundClosed = (FundClosed) event;
      String fundNameWithAdviserUsername = fundClosed.getFundName() + CompositeKeySeparator
          + fundClosed.getAdviserUsername();
      historicalFundRepo.updateFundEndDate(fundClosed.getEndDate(), fundNameWithAdviserUsername);
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
    } 

    else if (event instanceof UserRegisteredFromUserPool) {
      UserRegisteredFromUserPool userRegistered = (UserRegisteredFromUserPool) event;
      UserRDSIdentity userRDSIdentity = new UserRDSIdentity(getRequiredUsername(userRegistered.getUsername()),
          InvestingMode.VIRTUAL.toString());
      long userExiryTimeStamp = DateUtils.getEODFromDate(userRegistered.getTimestamp() + milliSecondsInAMonth);
      UsersRDSData usersRDSData = new UsersRDSData(userRDSIdentity);
      usersRDSData.setClientCode(userRegistered.getUsername());
      usersRDSData.setOnboardedTimeStamp(userRegistered.getTimestamp());
      usersRDSData.setExpiryTimeStamp(userExiryTimeStamp);
      usersOnRDSRepo.save(usersRDSData);
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
    }

    else if (event instanceof CognitoUserRegistered) {
      CognitoUserRegistered userRegistered = (CognitoUserRegistered) event;
      UserRDSIdentity userRDSIdentity = new UserRDSIdentity(getRequiredUsername(userRegistered.getUsername()),
          InvestingMode.VIRTUAL.toString());
      long userExiryTimeStamp = DateUtils.getEODFromDate(userRegistered.getTimestamp() + milliSecondsInAMonth);
      UsersRDSData usersRDSData = new UsersRDSData(userRDSIdentity);
      usersRDSData.setClientCode(userRegistered.getUsername());
      usersRDSData.setOnboardedTimeStamp(userRegistered.getTimestamp());
      usersRDSData.setExpiryTimeStamp(userExiryTimeStamp);
      usersOnRDSRepo.save(usersRDSData);
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
    }

    else if (event instanceof KycApprovedEvent) {
      KycApprovedEvent userKycCompleted = (KycApprovedEvent) event;
      UserRDSIdentity userRDSIdentity = new UserRDSIdentity(getRequiredUsername(userKycCompleted.getUsername()),
          InvestingMode.REAL.toString());
      UsersRDSData usersRDSData = new UsersRDSData(userRDSIdentity);
      usersRDSData.setClientCode(userKycCompleted.getClientCode());
      usersRDSData.setOnboardedTimeStamp(userKycCompleted.getTimestamp());
      
      usersOnRDSRepo.save(usersRDSData);
      usersOnRDSRepo.updateUserVirtualExpiryTime(0L, getRequiredUsername(userKycCompleted.getUsername()), "VIRTUAL");
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
    }

    else if (event instanceof UpdateKYCDetails) {
      UpdateKYCDetails userKycCompleted = (UpdateKYCDetails) event;
      if (!userKycCompleted.getClientCode().equals("")) {
        usersOnRDSRepo.updateUserClientCode(userKycCompleted.getClientCode(),
            getRequiredUsername(userKycCompleted.getUsername()), InvestingMode.REAL.toString());
        rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
      }
    }

    else if (event instanceof FundSubscribed) {
      
      FundSubscribed fundSubscribed = (FundSubscribed) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(
          getRequiredUsername(fundSubscribed.getUsername()), fundSubscribed.getFundName(),
          fundSubscribed.getAdviserUsername(), fundSubscribed.getInvestingMode());
      String persistenceIdWithTimeStamp = userFundCompositeKey.persistenceId() + CompositeKeySeparator
          + fundSubscribed.getTimestamp();

      HistoricalUserFundIdentity userFundIdentity = new HistoricalUserFundIdentity(
          getRequiredUsername(fundSubscribed.getUsername()),
          fundSubscribed.getFundName() + CompositeKeySeparator + fundSubscribed.getAdviserUsername(),
          fundSubscribed.getInvestingMode().toString(), fundSubscribed.getTimestamp());
      HistoricalUserFundData historicalUserFund = new HistoricalUserFundData(userFundIdentity);
      historicalUserFund.setFundCashFlow(fundSubscribed.getTotalInvestment());

      UserFundsOnRDSIdentity userFundRDSIdentity = new UserFundsOnRDSIdentity(
          getRequiredUsername(fundSubscribed.getUsername()), fundSubscribed.getInvestingMode().toString(),
          fundSubscribed.getFundName() + CompositeKeySeparator + fundSubscribed.getAdviserUsername());
    
        UserFundsOnRDS userFundsOnRDS = new UserFundsOnRDS(userFundRDSIdentity);
        userFundsOnRDS.setSubscriptionTimeStamp(fundSubscribed.getTimestamp());
        userFundsOnRDS.setUnsubscriptionTimeStamp(0);
        userFundsOnRDSRepo.save(userFundsOnRDS);
      addDbEntry(updateDB, userFundDataToBeUpdated, persistenceIdWithTimeStamp, historicalUserFund);
    }

    else if (event instanceof FundSuccessfullyUnsubscribed) {

      FundSuccessfullyUnsubscribed fundUnsubscribed = (FundSuccessfullyUnsubscribed) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(
          getRequiredUsername(fundUnsubscribed.getUsername()), fundUnsubscribed.getFundName(),
          fundUnsubscribed.getAdviserUsername(), fundUnsubscribed.getInvestingMode());
      String persistenceIdWithTimeStamp = userFundCompositeKey.persistenceId() + CompositeKeySeparator
          + fundUnsubscribed.getTimestamp();

      HistoricalUserFundIdentity userFundIdentity = new HistoricalUserFundIdentity(
          getRequiredUsername(fundUnsubscribed.getUsername()),
          fundUnsubscribed.getFundName() + CompositeKeySeparator + fundUnsubscribed.getAdviserUsername(),
          fundUnsubscribed.getInvestingMode().toString(), fundUnsubscribed.getTimestamp());
      HistoricalUserFundData historicalUserFund = new HistoricalUserFundData(userFundIdentity);
      historicalUserFund.setFundCashFlow(-1.0 * fundUnsubscribed.getCurrentCashComponent());

      userFundsOnRDSRepo.updateUnsubscriptionTime(fundUnsubscribed.getTimestamp(),
          getRequiredUsername(fundUnsubscribed.getUsername()), fundUnsubscribed.getInvestingMode().toString(),
          fundUnsubscribed.getFundName() + CompositeKeySeparator + fundUnsubscribed.getAdviserUsername());
      
      addDbEntry(updateDB, userFundDataToBeUpdated, persistenceIdWithTimeStamp, historicalUserFund);
    }

    else if (event instanceof UserFundWithdrawalDoneSuccessfully) {
      UserFundWithdrawalDoneSuccessfully withdrawalDoneSuccessfully = (UserFundWithdrawalDoneSuccessfully) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(
          getRequiredUsername(withdrawalDoneSuccessfully.getUsername()), withdrawalDoneSuccessfully.getFundName(),
          withdrawalDoneSuccessfully.getAdviserUsername(), withdrawalDoneSuccessfully.getInvestingMode());

      HistoricalUserFundIdentity userFundIdentity = new HistoricalUserFundIdentity(
          getRequiredUsername(withdrawalDoneSuccessfully.getUsername()),
          withdrawalDoneSuccessfully.getFundName() + CompositeKeySeparator
              + withdrawalDoneSuccessfully.getAdviserUsername(),
          withdrawalDoneSuccessfully.getInvestingMode().toString(), withdrawalDoneSuccessfully.getTimestamp());
      HistoricalUserFundData historicalUserFund = new HistoricalUserFundData(userFundIdentity);
      historicalUserFund.setFundCashFlow(-1.0 * withdrawalDoneSuccessfully.getTotalWithdrawalAmount());

      String persistenceIdWithTimeStamp = userFundCompositeKey.persistenceId() + CompositeKeySeparator
          + withdrawalDoneSuccessfully.getTimestamp();
      addDbEntry(updateDB, userFundDataToBeUpdated, persistenceIdWithTimeStamp, historicalUserFund);

    }

    else if (event instanceof FundLumpSumAdded) {

      FundLumpSumAdded fundLumpSumAdded = (FundLumpSumAdded) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(
          getRequiredUsername(fundLumpSumAdded.getUsername()), fundLumpSumAdded.getFundName(),
          fundLumpSumAdded.getAdviserUsername(), fundLumpSumAdded.getInvestingMode());

      HistoricalUserFundIdentity userFundIdentity = new HistoricalUserFundIdentity(
          getRequiredUsername(fundLumpSumAdded.getUsername()),
          fundLumpSumAdded.getFundName() + CompositeKeySeparator + fundLumpSumAdded.getAdviserUsername(),
          fundLumpSumAdded.getInvestingMode().toString(), fundLumpSumAdded.getTimestamp());
      HistoricalUserFundData historicalUserFund = new HistoricalUserFundData(userFundIdentity);
      historicalUserFund.setFundCashFlow(fundLumpSumAdded.getLumpSumAdded());

      String persistenceIdWithDate = userFundCompositeKey.persistenceId() + CompositeKeySeparator
          + DateUtils.getEODFromDate(fundLumpSumAdded.getTimestamp());
      addDbEntry(updateDB, userFundDataToBeUpdated, persistenceIdWithDate, historicalUserFund);

    }

    else if (event instanceof UserFundDividendReceived) {
      UserFundDividendReceived fundDividendReceived = (UserFundDividendReceived) event;
      UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(
          getRequiredUsername(fundDividendReceived.getUsername()), fundDividendReceived.getFundName(),
          fundDividendReceived.getAdviserUsername(), fundDividendReceived.getInvestingMode());

      HistoricalUserFundIdentity userFundIdentity = new HistoricalUserFundIdentity(
          getRequiredUsername(fundDividendReceived.getUsername()),
          fundDividendReceived.getFundName() + CompositeKeySeparator + fundDividendReceived.getAdviserUsername(),
          fundDividendReceived.getInvestingMode().toString(), fundDividendReceived.getTimestamp());
      HistoricalFundDividendData historicalFundDividendData = new HistoricalFundDividendData(userFundIdentity);
      historicalFundDividendData.setDividendValue(fundDividendReceived.getDividendAmount());

      String persistenceIdWithDate = userFundCompositeKey.persistenceId() + CompositeKeySeparator
          + DateUtils.getEODFromDate(fundDividendReceived.getTimestamp());
      addDbEntry(updateDB, userFundDividendToBeAdded, persistenceIdWithDate, historicalFundDividendData);

    }

    else if (event instanceof UserFundAdviceIssuedWithUserEmail) {

      UserFundAdviceIssuedWithUserEmail userAdviseIssued = (UserFundAdviceIssuedWithUserEmail) event;
      UserAdvisesOnRDSIdentity userAdvisesOnRDSIdentity = new UserAdvisesOnRDSIdentity(
          getRequiredUsername(userAdviseIssued.getUsername()), userAdviseIssued.getInvestingMode().toString(),
          userAdviseIssued.getFundName() + CompositeKeySeparator + userAdviseIssued.getAdviserUsername(),
          userAdviseIssued.getAdviseId());

      UserAdvisesOnRDS userAdvisesOnRDS = new UserAdvisesOnRDS(userAdvisesOnRDSIdentity);
      userAdvisesOnRDS.setAdviseStartTime(userAdviseIssued.getTimestamp());
      userAdvisesOnRDSRepo.save(userAdvisesOnRDS);
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));

    }
    else if (event instanceof CurrentUserAdviseFlushedDueToExit) {

      CurrentUserAdviseFlushedDueToExit adviseFlushed = (CurrentUserAdviseFlushedDueToExit) event;

      userAdvisesOnRDSRepo.updateAdviseCloseTime(adviseFlushed.getTimestamp(), getRequiredUsername(adviseFlushed.getUsername()),
          adviseFlushed.getInvestingMode().toString(),
          adviseFlushed.getFundName() + CompositeKeySeparator + adviseFlushed.getAdviserUsername(),
          adviseFlushed.getAdviseId());
      rdsSequenceStoreRepo.save(new SequenceStoreData(event.getClass().getSimpleName(), eventEnvelope.sequenceNr()));
    }

    else if (event instanceof AdviseOrderTradeExecuted) {

      AdviseOrderTradeExecuted adviseTradeExecuted = (AdviseOrderTradeExecuted) event;
      UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(adviseTradeExecuted.getAdviseId(),
          getRequiredUsername(adviseTradeExecuted.getUsername()), adviseTradeExecuted.getFundName(),
          adviseTradeExecuted.getAdviserUsername(), adviseTradeExecuted.getInvestingMode());

      HistorialUserAdviseIdentity userAdviseIdentity = new HistorialUserAdviseIdentity(
          adviseTradeExecuted.getAdviseId(),
          adviseTradeExecuted.getFundName() + CompositeKeySeparator + adviseTradeExecuted.getAdviserUsername(),
          adviseTradeExecuted.getTimestamp(), adviseTradeExecuted.getInvestingMode().toString(),
          getRequiredUsername(adviseTradeExecuted.getUsername()));
      HistoricalUserAdviseData historicalUserAdvise = new HistoricalUserAdviseData(userAdviseIdentity);
      double transactedAmount = adviseTradeExecuted.getQuantity()
          * adviseTradeExecuted.getPrice().getPrice().getPrice();
      historicalUserAdvise.setOrderSide(adviseTradeExecuted.getOrderSide().toString());
      if (adviseTradeExecuted.getOrderSide() == BUY) {
        historicalUserAdvise.setAmountBought(transactedAmount);
        historicalUserAdvise.setWdId(adviseTradeExecuted.getWdId());
        historicalUserAdvise.setNumberOfSharesTransacted(adviseTradeExecuted.getQuantity());
      } else {
        historicalUserAdvise.setAmountSold(transactedAmount);
        historicalUserAdvise.setWdId(adviseTradeExecuted.getWdId());
        historicalUserAdvise.setNumberOfSharesTransacted(adviseTradeExecuted.getQuantity());
      }

      String persistenceIdWithTimeStamp = userAdviseCompositeKey.persistenceId() + CompositeKeySeparator
          + adviseTradeExecuted.getTimestamp();
      addDbEntry(updateDB, userAdviseDataToBeUpdated, persistenceIdWithTimeStamp, historicalUserAdvise);

    }

    if (updateDB) {
      
      if (event instanceof UserFundEvent && !userFundDataToBeUpdated.isEmpty()) {
        addToSeqNumMap(event.getClass().getSimpleName(), eventEnvelope.sequenceNr(), seqNumDataToAdd);
        eventTypeWithDataAndRepoMap.get("userFundTransactions").dbObjectsLinkedList().add(new RDSEventObject(userFundDataToBeUpdated, seqNumDataToAdd));
      }
      if (event instanceof UserAdviseEvent && !userAdviseDataToBeUpdated.isEmpty()) {
        addToSeqNumMap(event.getClass().getSimpleName(), eventEnvelope.sequenceNr(), seqNumDataToAdd);
        eventTypeWithDataAndRepoMap.get("userAdviseTransactions").dbObjectsLinkedList().add(new RDSEventObject(userAdviseDataToBeUpdated, seqNumDataToAdd));
      }
      if (event instanceof UserFundEvent && !userFundDividendToBeAdded.isEmpty()) {
        addToSeqNumMap(event.getClass().getSimpleName(), eventEnvelope.sequenceNr(), seqNumDataToAdd);
        eventTypeWithDataAndRepoMap.get("userFundDividendTransactions").dbObjectsLinkedList().add(new RDSEventObject(userFundDividendToBeAdded, seqNumDataToAdd));
      }
    }
  }
  

  private void addToSeqNumMap(String persistenceId, long sequenceNr, List<Entry<Object, Object>> seqNumDataToAdd) {
    seqNumDataToAdd.add(new SimpleEntry<Object, Object>(persistenceId, sequenceNr));
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Scheduled(fixedDelayString =  "${rdsUploadRate.in.milliseconds}")
  public void uploadDataToDB() throws IOException {
    if (!properties.scheduleViewpopWrites()) {
      return;
    }
    eventTypeWithDataAndRepoMap.entrySet().stream().forEach(entry ->{
      Map<Object, Object> dbTransactionsToWrite = new HashMap<>();
      List<SequenceStoreData> seqNumDataToWrite = new ArrayList<>();
      while(entry.getValue().dbObjectsLinkedList().size() >0 && dbTransactionsToWrite.size() < properties.maxNumberOfWritesToRdsInABatch()){
        RDSEventObject ob = entry.getValue().dbObjectsLinkedList().pollFirst();
        if(ob!=null){
          List<Entry<Object, Object>> dataList = ob.getData();
          List<Entry<Object, Object>> dataSeq = ob.getSeqNumdata();
          for (Entry<Object, Object> e : dataList) {
            dbTransactionsToWrite.put(e.getKey(), e.getValue());
          }
          for (Entry<Object, Object> e : dataSeq) {
           seqNumDataToWrite.add(new SequenceStoreData((String)e.getKey(), (Long)e.getValue()));
          }
        }
      }
      if(dbTransactionsToWrite.size() > 0 ){
        try {
          log.debug("Batch writing " + entry.getKey() + " values. Number of entries to write in current batch is  "
              + dbTransactionsToWrite.size());
          entry.getValue().repo().saveAll(new ArrayList(dbTransactionsToWrite.values()));
          log.debug("Current batch write of " + entry.getKey() + " values successfull. Entries pending is "
              + entry.getValue().dbObjectsLinkedList().size());
          rdsSequenceStoreRepo.saveAll(seqNumDataToWrite);
        } catch (Exception e) {
          log.error("SQL batch write exception " + e);
        }
      }
    });
    


  }
  

  private void addDbEntry(boolean updateDB, List<Entry<Object, Object>> dbData, Object key, Object value) {

    if (updateDB) {
      dbData.add(new SimpleEntry<Object, Object>(key, value));
    }
  }
  

  private String getRequiredUsername(String username) {
    if (username.equals("rhunadkat@outlook.com"))
      return "rhunadkat1";
    return username.split("@")[0];
  }
}
