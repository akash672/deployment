package com.wt.api.view;

import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.Runtime.getRuntime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.wt.utils.StartStreamingEvents;
import com.wt.utils.ViewPopulatorRdsAppConfiguration;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class ViewPopulatorRdsService {
  
  private static AnnotationConfigApplicationContext ctx;
  
  private ActorSystem system;
  private ActorRef viewPopulatorRdsActor;
  
  @Autowired
  public ViewPopulatorRdsService(ActorSystem system) {
    super();
    this.system = system;
  }
  
  public static void main(String[] args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(ViewPopulatorRdsAppConfiguration.class);
    ViewPopulatorRdsService service = ctx.getBean(ViewPopulatorRdsService.class);
    service.start();
  }
  private void start() throws Exception {
    log.info("=======================================");
    log.info("| Starting ViewPopulatorRdsService       |");
    log.info("=======================================");

    Materializer materializer = ActorMaterializer.create(system);
    viewPopulatorRdsActor = system.actorOf(SpringExtProvider.get(system).props("ViewPopulatorRdsActor"),
        "view-populator--rds-root");
    viewPopulatorRdsActor.tell(new StartStreamingEvents(materializer), null);

    log.info("View Populator Service for rds started.");

    getRuntime().addShutdownHook(new Thread(() -> {
      ctx.close();
    }));
  }
}
