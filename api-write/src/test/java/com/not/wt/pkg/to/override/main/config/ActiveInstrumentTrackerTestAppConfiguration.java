package com.not.wt.pkg.to.override.main.config;

import java.io.IOException;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.utils.akka.WDActorSelections;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan("com.wt")
@PropertySource("classpath:test-environment.properties")
public class ActiveInstrumentTrackerTestAppConfiguration extends TestAppConfiguration {

  private @Autowired @Qualifier("WDActors") WDActorSelections actorSelections;

  @Bean
  public CtclBroadcast ctclBroadcast() throws NumberFormatException, IOException {
    return Mockito.mock(CtclBroadcast.class);
  }

}