package com.not.wt.pkg.to.override.main.config;

import static com.wt.domain.BrokerName.BPWEALTH;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.CORPORATEACTION;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.MUTUALFUNDSUBSCRIPTION;
import static com.wt.domain.Purpose.ONBOARDING;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ctcl.xneutrino.broadcast.BroadCastManager;
import com.wt.broadcast.xneutrino.XNeutrinoBroadcast;
import com.wt.ctcl.CTCLMock;
import com.wt.ctcl.CorporateEventCTCLMock;
import com.wt.ctcl.VirtualMutualFundInteractive;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.ctcl.facade.MutualFundInteractive;
import com.wt.domain.BrokerType;
import com.wt.domain.Wallet;
import com.wt.util.BroadcastManagerStub;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;


@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan("com.wt")
@PropertySource("classpath:test-environment.properties")
public class APISpecificTestAppConfiguration extends TestAppConfiguration {

  private @Autowired WDActorSelections actorSelections;
  
  @Bean
  public Properties properties() throws IOException {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("test-environment.properties"));
    return properties;
  }
  
  
  @Bean 
  public BroadCastManager broadCastManager() throws NumberFormatException, IOException {
    return new BroadcastManagerStub(properties().getProperty("xneutrino.broadcast.ip"),
        Integer.parseInt(properties().getProperty("xneutrino.broadcast.port")));
  }

  @Bean
  public CtclBroadcast ctclBroadCast() throws NumberFormatException, IOException {
    return new XNeutrinoBroadcast(broadCastManager(), actorSelections,
        wdProperties);
  }
  
  @Bean
  public Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation() throws IOException {
    Map<BrokerType, CtclInteractive> brokerTypeWithCTCLImplementation = new HashMap<BrokerType, CtclInteractive>();
    brokerTypeWithCTCLImplementation.put(new BrokerType(EQUITYSUBSCRIPTION, REAL, MOCK), new CTCLMock(actorSystem(), actorSelections));
    brokerTypeWithCTCLImplementation.put(new BrokerType(ONBOARDING, REAL, BPWEALTH), new CTCLMock(actorSystem(), actorSelections));
    brokerTypeWithCTCLImplementation.put(new BrokerType(EQUITYSUBSCRIPTION, VIRTUAL, MOCK), new CTCLMock(actorSystem(), actorSelections));
    brokerTypeWithCTCLImplementation.put(new BrokerType(EQUITYSUBSCRIPTION, REAL, BPWEALTH), new CTCLMock(actorSystem(), actorSelections));
    brokerTypeWithCTCLImplementation.put(new BrokerType(EQUITYSUBSCRIPTION, VIRTUAL, BPWEALTH), new CTCLMock(actorSystem(), actorSelections));
    brokerTypeWithCTCLImplementation.put(new BrokerType(CORPORATEACTION, VIRTUAL, MOCK), new CorporateEventCTCLMock(actorSelections));
    return brokerTypeWithCTCLImplementation;
  }
  
  @Bean
  public Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation() throws IOException {
    Map<BrokerType, MutualFundInteractive> brokerTypeWithMFCTCLImplementation = new HashMap<BrokerType, MutualFundInteractive>();
    brokerTypeWithMFCTCLImplementation.put(new BrokerType(MUTUALFUNDSUBSCRIPTION, VIRTUAL, MOCK), new VirtualMutualFundInteractive(actorSelections));
    brokerTypeWithMFCTCLImplementation.put(new BrokerType(MUTUALFUNDSUBSCRIPTION, REAL, BPWEALTH), new VirtualMutualFundInteractive(actorSelections));
    return brokerTypeWithMFCTCLImplementation;
  }
  
  @Bean(name="SESEmailUtil")
  public SESEmailUtil sesEmailUtil()  {
    return mock(SESEmailUtil.class);
  }
  
  @Bean(name="RealWallet")
  public Wallet realWallet()  {
    return mock(Wallet.class);
  }
  
}