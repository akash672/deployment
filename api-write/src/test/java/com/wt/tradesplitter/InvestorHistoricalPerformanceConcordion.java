package com.wt.tradesplitter;

import static akka.pattern.Patterns.ask;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.UserAcceptanceMode.Auto;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.mockito.Mockito.doReturn;
import static scala.concurrent.Await.result;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Files;
import org.chiknrice.test.SpringifiedConcordionRunner;
import org.concordion.api.extension.Extensions;
import org.concordion.ext.excel.ExcelExtension;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TradeSplitterSpecificTestAppConfiguration;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.MaxDrawdown;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.UserFundPerformance;
import com.wt.domain.UserFundPerformanceKey;
import com.wt.domain.UserPerformance;
import com.wt.domain.UserPerformanceKey;
import com.wt.domain.repo.UserFundPerformanceRepository;
import com.wt.domain.repo.UserFundRepository;
import com.wt.domain.repo.UserPerformanceRepository;
import com.wt.domain.write.commands.AcceptUserAcceptanceMode;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CalculateUserHistoricalPerformance;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.FundActionApproved;
import com.wt.domain.write.commands.GetPricePaf;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.SubscribeToFund;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.utils.CoreDBTest;
import com.wt.utils.OTPUtils;
import com.wt.utils.ViewPopulater;
import com.wt.utils.akka.ActorResultWaiter;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.stream.ActorMaterializer;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@RunWith(SpringifiedConcordionRunner.class)
@Extensions(ExcelExtension.class)
@ContextConfiguration(classes = TradeSplitterSpecificTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class InvestorHistoricalPerformanceConcordion {
  private static final int INT_TOKEN_1 = 123;
  private static final String SYMBOL_1 = "HINDALCO";
  private static final String TICKER_1 = "123-NSE-EQ";
  private static final int INT_TOKEN_2 = 124;
  private static final String SYMBOL_2 = "LUPIN";
  private static final String TICKER_2 = "124-NSE-EQ";
  private static final int INT_TOKEN_3 = 125;
  private static final String SYMBOL_3 = "ZEEL";
  private static final String TICKER_3 = "125-NSE-EQ";
  private static final String RANDOM_HASHCODE = "sdfjgkjsdegf@123399";
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;
  @Autowired
  private ActorSystem actorSystem;
  @Autowired
  private UserFundPerformanceRepository userFundPerformanceRepository;
  @Autowired
  private UserPerformanceRepository userPerformanceRepository;
  @Autowired
  private UserFundRepository userFundRepository;
  @Autowired
  private @Qualifier("WDActors") WDActorSelections actorSelections;
  private ActorMaterializer materializer;
  private ActorRef userRootActor;
  private ActorRef adviserRootActor;
  private ActorRef universeRootActor;
  @SuppressWarnings("unused")
  private ActorRef equityOrderRootActor;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  @Qualifier("CTCLMock")
  private CtclInteractive ctclInteractive;
  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private OTPUtils otpGeneratorMock;

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Before
  public void setUpUserFundActorSystem() throws Exception {
    Files.delete(new File("build/journal"));
    Files.delete(new File("build/snapshots"));
    materializer = ActorMaterializer.create(actorSystem);
    userRootActor = actorSystem.actorOf(SpringExtProvider.get(actorSystem).props("UserRootActor"), "user-aggregator");
    adviserRootActor = actorSystem.actorOf(SpringExtProvider.get(actorSystem).props("AdviserRootActor"),
        "adviser-aggregator");
    universeRootActor = actorSystem.actorOf(SpringExtProvider.get(actorSystem).props("UniverseRootActor"),
        "universe-root-actor");
    equityOrderRootActor = actorSystem.actorOf(SpringExtProvider.get(actorSystem).props("EquityOrderRootActor"),
        "equity-order-aggregator");
    doReturn(RANDOM_HASHCODE).when(otpGeneratorMock).getUniqueHashCode();
    dbTest.setupDB();
    updateInstrumentsMapOfEquityActor(TICKER_1, SYMBOL_1, INT_TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER_2, SYMBOL_2, INT_TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER_3, SYMBOL_3, INT_TOKEN_3);
    environmentVariables.set("WT_MARKET_OPEN", "true");
  }

  public void registerAdviser(String adviserUsername) throws Exception {
    adviserRootActor.tell(new RegisterAdviserFromUserPool(adviserUsername, adviserUsername,
        adviserUsername + "@test.com", "5345" + adviserUsername, "Company Name", ""), ActorRef.noSender());
    timeoutToEnsurePopulationOfRepositories(2);
  }

  public void floatFund(String adviserUsername, String fundName) throws Exception {
    adviserRootActor.tell(
        new FloatFund(adviserUsername, fundName, 0, 1, "MonsoonFund", "Theme", 20000, 200000, "MEDIUM"),
        ActorRef.noSender());
    timeoutToEnsurePopulationOfRepositories(2);
  }

  public void issueAdvice(String adviserUsername, String fundName, String adviseId, String token, double allocation,
      double entryPrice, long entryTime, double exitPrice, long exitTime, long issueTime) throws Exception {
    adviserRootActor.tell(new IssueAdvice(adviserUsername, fundName, adviseId, token, allocation, entryPrice, entryTime,
        exitPrice, exitTime, issueTime), ActorRef.noSender());
    timeoutToEnsurePopulationOfRepositories(2);
  }

  public void registerUser(String userEmail, String firstName, String lastName, String password, long timestamp,
      String deviceToken) throws Exception {
    doReturn(RANDOM_HASHCODE).when(otpGeneratorMock).getUniqueHashCode();
    userRootActor.tell(new RegisterInvestorFromUserPool(userEmail.split("@")[0], firstName+" " + lastName, "", userEmail), ActorRef.noSender());
    timeoutToEnsurePopulationOfRepositories(2);
  }

  public void subscribeToFund(String adviserUsername, String fundName, String username, double lumpSumAmount,
      double sipAmount, String sipDate, String wealthCode) throws Exception {
    userRootActor.tell(new SubscribeToFund(adviserUsername, fundName, username, lumpSumAmount, sipAmount, sipDate,
        VIRTUAL, EQUITYSUBSCRIPTION), ActorRef.noSender());
    Thread.sleep(2 * 1000);
    userRootActor.tell(new FundActionApproved(username, fundName, adviserUsername, wealthCode, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()), ActorRef.noSender());
    timeoutToEnsurePopulationOfRepositories(5);
    userRootActor.tell(new AcceptUserAcceptanceMode(username, fundName, adviserUsername, VIRTUAL, Auto), ActorRef.noSender());
    timeoutToEnsurePopulationOfRepositories(10);
  }

  public void closeAdvise(String adviserUsername, String fundName, String adviseId, double allocation, long exitTime,
      double exitPrice) throws Exception {
    adviserRootActor.tell(new CloseAdvise(adviserUsername, fundName, adviseId, allocation, exitTime, exitPrice),
        ActorRef.noSender());
    timeoutToEnsurePopulationOfRepositories(10);
  }

  public void calculateUserPerformance(String advisor, String fund) throws Exception {
    userRootActor.tell(new CalculateUserHistoricalPerformance(), ActorRef.noSender());

    timeoutToEnsurePopulationOfRepositories(2);
  }

  public boolean timeoutToEnsurePopulationOfRepositories(int seconds) throws Exception {
    Thread.sleep(seconds * 1000);
    viewPopulater.blockingPopulateView(actorSystem, materializer);
    return true;
  }

  public UserFundPerformance getUserFundPerformance(String email, String fund, String adviser, long time)
      throws Exception {
    timeoutToEnsurePopulationOfRepositories(2);
    UserFundPerformance userFundPerformance = userFundPerformanceRepository
        .findById(new UserFundPerformanceKey(email, fund, VIRTUAL, adviser, time * 1000)).orElse(null);
    return userFundPerformance;
  }

  public UserPerformance getUserPerformance(String emailId, String fundName, String adviserUsername, long time)
      throws Exception {
    timeoutToEnsurePopulationOfRepositories(2);
    UserPerformance userPerformance = userPerformanceRepository
        .findById(new UserPerformanceKey(emailId, VIRTUAL.getInvestingMode(), time * 1000)).orElse(null);
    return userPerformance;
  }

  public MaxDrawdown getFundMaxDrawdown(String emailId, String fundName, String adviserName, String period)
      throws InterruptedException {
    UserFundCompositeKey id = new UserFundCompositeKey(emailId, fundName, adviserName, VIRTUAL);
    List<MaxDrawdown> returns = userFundRepository.findById(id).orElse(null).getDrawDowns();
    for (MaxDrawdown maxDrawdown : returns) {
      if (maxDrawdown.getPeriod().equalsIgnoreCase(period)) {
        return maxDrawdown;
      }
    }
    return null;
  }

  public String populatePrices(long date, String token, String price) throws Exception {
    long timestamp = date * 1000;
    InstrumentPrice pxEvt = new InstrumentPrice(Double.parseDouble(price), timestamp, token);
    sendPriceUpdate(universeRootActor, pxEvt);

    double paf = (Double) askAndWaitForResult(universeRootActor, new GetPricePaf(token, timestamp));
    return String.format("%.9f", paf);
  }

  protected Object sendPriceUpdate(final ActorRef to, InstrumentPrice event) throws Exception {
    Future<Object> maybeRegistered = ask(to, event, 10 * 1000L);
    Done result = (Done) result(maybeRegistered, ActorResultWaiter.TIMEOUT);

    return result;
  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(new UpdateEquityInstrumentSymbol(String.valueOf(token), "", symbol, ticker, "", "NSE", "EQ"),
        ActorRef.noSender());

  }

  @SuppressWarnings("unused")
  @After
  public void cleanUp() throws Exception {
    
    Future<Terminated> terminate = actorSystem.terminate();
    Terminated result = terminate.result(Duration.Inf(), new CanAwait() {
    });
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      FileUtils.deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}