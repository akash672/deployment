
package com.wt.tradesplitter;

import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.OrderSide.BUY;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.todayBOD;
import static com.wt.utils.NumberUtils.getFirstHalf;
import static com.wt.utils.NumberUtils.getSecondHalf;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.mockito.Mockito.doAnswer;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Files;
import org.chiknrice.test.SpringifiedConcordionRunner;
import org.concordion.api.extension.Extensions;
import org.concordion.ext.excel.ExcelExtension;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.TradeSplitterSpecificTestAppConfiguration;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.ctcl.facade.CtclInteractive;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.CtclEquityOrder;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.CurrentUserFundDb;
import com.wt.domain.DestinationAddress;
import com.wt.domain.Instrument;
import com.wt.domain.Order;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Trade;
import com.wt.domain.UserAdvise;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.repo.OrdersRepository;
import com.wt.domain.repo.UserEquityAdviseRepository;
import com.wt.domain.repo.UserFundRepository;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.FundActionApproved;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.SubscribeToFund;
import com.wt.domain.write.commands.UnsubscribeToFund;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.OrderPlacedWithExchange;
import com.wt.domain.write.events.OrderReceivedWithBroker;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.Result;
import com.wt.domain.write.events.TradeExecuted;
import com.wt.utils.CoreDBTest;
import com.wt.utils.DateUtils;
import com.wt.utils.DoublesUtil;
import com.wt.utils.ViewPopulater;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.stream.ActorMaterializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.log4j.Log4j;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

@Ignore
@RunWith(SpringifiedConcordionRunner.class)
@Extensions(ExcelExtension.class)
@ContextConfiguration(classes = TradeSplitterSpecificTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@Log4j
public class TradeSplitterConcordionIntegration {
  private static final double ENTRYPRICE = 1030d;
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;
  @Autowired
  private ActorSystem actorSystem;
  private ActorMaterializer materializer;
  private ActorRef userRootActor;
  private ActorRef adviserRootActor;
  @SuppressWarnings("unused")
  private ActorRef universeRootActor;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private CtclInteractive ctclInteractive;
  @Autowired
  private CtclBroadcast ctclBroadcast;
  @Autowired
  private FundRepository fundRepository;
  @Autowired
  private UserEquityAdviseRepository userEquityAdviseRepository;
  @Autowired
  private OrdersRepository ordersRepository;
  @Autowired
  private UserFundRepository userFundRepository;
  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private WDActorSelections actorSelections;

  @Before
  public void setUpUserFundActorSystem() throws Exception {
    Files.delete(new File("build/journal"));
    Files.delete(new File("build/snapshots"));
    materializer = ActorMaterializer.create(actorSystem);
    userRootActor = actorSystem.actorOf(SpringExtProvider.get(actorSystem).props("UserRootActor"), "user-aggregator");
    adviserRootActor = actorSystem.actorOf(SpringExtProvider.get(actorSystem).props("AdviserRootActor"),
        "adviser-aggregator");
    universeRootActor = actorSystem.actorOf(SpringExtProvider.get(actorSystem).props("UniverseRootActor"), "universe-root-actor");
    dbTest.setupDB();

    doAnswer(invocation -> {
      Instrument instrument = (Instrument) invocation.getArguments()[0];
      ActorRef actor = (ActorRef) invocation.getArguments()[1];
      PriceWithPaf price = new PriceWithPaf(
          new InstrumentPrice(ENTRYPRICE, DateUtils.currentTimeInMillis(), instrument.getWdId()),
          Pafs.withCumulativePaf(1.));
      actor.tell(price, ActorRef.noSender());
      return null;
    }).when(ctclBroadcast).snapshot(Mockito.any(Instrument.class), Mockito.any(ActorRef.class));

    doAnswer(invocation -> {
      Order order = (Order) invocation.getArguments()[0];
      DestinationAddress destinationAddress = (DestinationAddress) invocation.getArguments()[1];
      if (isBuyOrder(order)) {
        handleOrderExecution(order, 1090d, 1090.05d, "wealthdeskUUIDString-brokerOrderId-open", "wealthdeskUUIDString-exchOrderId-open",
            "wealthdeskUUIDString-ctclOrderId-open", "wealthdeskUUIDString-tradeOrderId-1-open",
            "wealthdeskUUIDString-tradeOrderId-1-open", destinationAddress);
      } else {
        handleOrderExecution(order, 1090.2d, 1090.25d, "wealthdeskUUIDString-brokerOrderId-close", "wealthdeskUUIDString-exchOrderId-close",
            "wealthdeskUUIDString-ctclOrderId-close", "wealthdeskUUIDString-tradeOrderId-1-close",
            "wealthdeskUUIDString-tradeOrderId-2-close", destinationAddress);
      }
      return null;
    }).when(ctclInteractive).sendMarketCashOrder(Mockito.any(CtclEquityOrder.class));

  }

  private boolean isBuyOrder(Order order) {
    return order.getSide().equals(BUY);
  }

  private void handleOrderExecution(Order order, double firstPrice, double nextPrice, String brokerOrderId, String ctclOrderId,
      String exchangeOrderId, String trade1Id, String trade2Id, DestinationAddress destinationAddress) throws Exception {
	  actorSelections.userRootActor().tell(new OrderReceivedWithBroker(destinationAddress,order.getOrderId(), brokerOrderId), ActorRef.noSender());
	  actorSelections.userRootActor().tell(new OrderPlacedWithExchange(destinationAddress,brokerOrderId, exchangeOrderId, ctclOrderId, order.getSide(), order.getIntegerQuantity()), ActorRef.noSender());
    PriceWithPaf price1 = new PriceWithPaf(
        new InstrumentPrice(firstPrice, DateUtils.currentTimeInMillis(), order.getTicker()), Pafs.withCumulativePaf(1.));
    PriceWithPaf price2 = new PriceWithPaf(
        new InstrumentPrice(nextPrice, DateUtils.currentTimeInMillis(), order.getTicker()), Pafs.withCumulativePaf(1.));
    actorSelections.userRootActor().tell(new TradeExecuted(destinationAddress,exchangeOrderId, trade1Id, order.getSide(), price1, getFirstHalf(order.getIntegerQuantity())),
        ActorRef.noSender());
    actorSelections.userRootActor().tell(
        new TradeExecuted(destinationAddress,exchangeOrderId, trade2Id, order.getSide(), price2, getSecondHalf(order.getIntegerQuantity())),
        ActorRef.noSender());

  }

  @SuppressWarnings("unused")
  @After
  public void cleanUp() throws Exception {
    Future<Terminated> terminate = actorSystem.terminate();
    Terminated result = terminate.result(Duration.Inf(), new CanAwait() {
    });
    try {
      FileUtils.deleteDirectory(new File("build/journal"));
      FileUtils.deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String registerUser(String userEmail, String firstName, String lastName, String password, long timestamp,
      String deviceToken) throws Exception {
    Result result = (Result) askAndWaitForResult(userRootActor, new RegisterInvestorFromUserPool(userEmail.split("@")[0],
        firstName + " " + lastName, "" , userEmail));

    return result.getId() + " registered.";
  }

  public AmountDetails subscribeToFund(String advisorUsername, String fundName, String username, double lumpSumAmount,
      double sipAmount, String sipDate, String wealthCode) throws Exception {
    userRootActor.tell(new SubscribeToFund(advisorUsername, fundName, username, lumpSumAmount, sipAmount, sipDate, VIRTUAL, EQUITYSUBSCRIPTION), ActorRef.noSender());
    Thread.sleep(2 * 1000);
    Result result = (Result) askAndWaitForResult(userRootActor,
        new FundActionApproved(username, advisorUsername, fundName, wealthCode, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));

    return new AmountDetails(0.0, result.getMessage());
  }
  
  public String unSubscribeFromFund(String advisorUsername, String fundName, String username) throws Exception {
    Result result = (Result) askAndWaitForResult(userRootActor,
        new UnsubscribeToFund(advisorUsername, fundName, username, VIRTUAL));

    
    return result.getMessage();
  }

  public String registerAdviser(String adviserUsername) throws Exception {
    Result result = (Result) askAndWaitForResult(adviserRootActor, new RegisterAdviserFromUserPool(adviserUsername,
        adviserUsername, adviserUsername + "@test.com", "5345publicId", "Company Name", ""));

    
    return result.getMessage();
  }

  public String floatFund(String advisorIdentity, String fundName, long startDate, long endDate, String description,
      String theme, double minSIP, double minLumSum, String riskLevel) throws Exception {
    Result result = (Result) askAndWaitForResult(adviserRootActor,
        new FloatFund(advisorIdentity, fundName, startDate, endDate, description, theme, minSIP, minLumSum, riskLevel));

    
    return result.getMessage();
  }

  public String issueAdvice(String adviserIdentity, String fundName, String adviseId, String token,
      double allocation, double entryPrice, long entryTime, double exitPrice, long exitTime, long issueTime)
      throws Exception {
    Result adviseStatus = (Result) askAndWaitForResult(adviserRootActor, new IssueAdvice(adviserIdentity, fundName,
        adviseId, token, allocation, entryPrice, entryTime, exitPrice, exitTime, issueTime));
    
    
    return adviseStatus.getMessage();
  }

  public String closeAdvise(String adviserIdentity, String fundName, String adviseId, double allocation,
      long actualExitDate, double actualExitPrice) throws Exception {
    Result adviseStatus = (Result) askAndWaitForResult(adviserRootActor,
        new CloseAdvise(adviserIdentity, fundName, adviseId, allocation, actualExitDate, actualExitPrice));
    
    
    return adviseStatus.getMessage();
  }
  
  public double verifyIssueAdviseResults(String adviserIdentity, String fundName) throws Exception {
    CurrentFundDb fund = fundRepository.findById(fundName).orElse(null);
    return fund.getCash();
  }

  public double verifyCloseAdviseResults(String adviserIdentity, String fundName) throws Exception {
    CurrentFundDb fund = fundRepository.findById(fundName).orElse(null);
    return fund.getCash();
  }

  public SimplifiedPosition verifyPosition(String emailId, String fund, String adviserUsername, String token)
      throws Exception {
    UserAdvise userAdvise = userEquityAdviseRepository.findByToken(token);
    int totalExitedQuantity;
    try {
      Order latestExitedOrder = userAdvise.getLatestSellOrder().get();
      Stream<Trade> exitTrades = latestExitedOrder.getTrades().stream();
      totalExitedQuantity = exitTrades.mapToInt(trade -> trade.getIntegerQuantity()).sum();
      log.debug("Token: " + latestExitedOrder.getTicker() + " " + totalExitedQuantity);
    } catch(ArrayIndexOutOfBoundsException exception) {
      totalExitedQuantity = 0;
    }
    return new SimplifiedPosition(userAdvise.getIssueAdviseAllocationValue(),
        DoublesUtil.round(totalExitedQuantity * userAdvise.getAverageExitPrice().getPrice().getPrice()),
        userAdvise.getAverageEntryPrice().getPrice().getPrice(),
        userAdvise.getAverageExitPrice().getPrice().getPrice());
  }

  public String verifyUserFundUpdate(String fundName, String emailId, String adviserUsername) throws Exception {
    CurrentUserFundDb userFund = userFundRepository.findById(new UserFundCompositeKey(emailId, fundName, adviserUsername, VIRTUAL)).orElse(null);
    return userFund.getUsername() + " has subscribed to " + userFund.getFundWithAdviserUsername();
  }

  public OrderDetails verifyOrders(String userEmail, String ticker, int side) throws Exception {
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(ticker, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == side).flatMap(order -> order.getTrades().stream());
    int totalOrderQuantity = orders.stream().filter(o -> o.getSide().getValue() == side).mapToInt(order -> order.getIntegerQuantity()).sum();
    int totalTradedQuantity = trades.mapToInt(trade -> trade.getIntegerQuantity()).sum();
    log.debug(" Order of total Quantity" + totalOrderQuantity + " total Executed Quantity " + totalTradedQuantity);
    
    return new OrderDetails(
        (totalOrderQuantity - totalTradedQuantity),
         totalTradedQuantity, 
         totalOrderQuantity);
  }

  public double availableCash(String emaildId, String fundName, String adviserUserName) throws Exception {
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(emaildId, fundName, adviserUserName, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    return DoublesUtil.round(userFund.getCashComponent());
  }

  public AdvisePosition advisePosition(String userEmail, String token) throws Exception {
    UserAdvise userAdvise = userEquityAdviseRepository.findByToken(token);
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(token, timeFromMidnight());
    Order buyOrder = orders.stream().filter(o -> o.getSide().getValue() == 1).findAny().get();
    Stream<Order> allSellOrders = orders.stream().filter(o -> o.getSide().getValue() == -1);
    int totalExitedQuantity = allSellOrders.mapToInt(o -> o.getIntegertotalTradedQuantity()).sum();
    int openQuantity = buyOrder.getTrades().stream().mapToInt(trade -> trade.getIntegerQuantity()).sum() - totalExitedQuantity;
    log.debug("Advise Position Open Quantity: " + openQuantity + " Total Exited Quantity: " + totalExitedQuantity);
    return new AdvisePosition(
        openQuantity, 
        totalExitedQuantity, 
        userAdvise.getAverageEntryPrice().getPrice().getPrice(),
        userAdvise.getAverageExitPrice().getPrice().getPrice());
  }
  
  private long timeFromMidnight() throws ParseException {
    return todayBOD() - (9 * 60 * 60 * 1000L);
  }

  public boolean timeoutToEnsurePopulationOfRepositories(int seconds) throws Exception  {
    viewPopulater.blockingPopulateView(actorSystem, materializer);
    Thread.sleep(seconds * 1000);

    return true;
  }
  
  @ToString
  public class AdvisePosition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Getter
    private int openQuantity;
    @Getter
    private int exitedQuantity;
    @Getter
    private double averageEntryPrice;
    @Getter
    private double averageExitPrice;

    public AdvisePosition(int openQuantity, int exitedQuantity, double averageEntryPrice, double averageExitPrice) {
      super();
      this.openQuantity = openQuantity;
      this.exitedQuantity = exitedQuantity;
      this.averageEntryPrice = averageEntryPrice;
      this.averageExitPrice = averageExitPrice;
    }

  }

  @ToString
  public class OrderDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Getter
    private int openOrderQuantity;
    @Getter
    private int executedOrderQuantity;
    @Getter
    private int totalOrderQuantity;

    public OrderDetails(int openOrderQuantity, int executedOrderQuantity, int totalOrderQuantity) {
      super();
      this.openOrderQuantity = openOrderQuantity;
      this.executedOrderQuantity = executedOrderQuantity;
      this.totalOrderQuantity = totalOrderQuantity;
    }

  }

  @AllArgsConstructor
  @Getter
  @ToString
  public class AmountDetails implements Serializable {
    private static final long serialVersionUID = 3321251584358327181L;
    private double amount;
    private String message;

  }

  @Getter
  @AllArgsConstructor
  @ToString
  public class SimplifiedPosition {
    private double issueAdviseAllocationValue;
    private double closeAdviseAllocationValue;
    private double averageEntryPrice;
    private double averageExitPrice;

  }

}