package com.wt.tradesplitter;

import static akka.http.javadsl.marshallers.jackson.Jackson.unmarshaller;
import static akka.http.javadsl.model.HttpRequest.DELETE;
import static akka.http.javadsl.model.HttpRequest.GET;
import static akka.http.javadsl.model.HttpRequest.PATCH;
import static akka.http.javadsl.model.HttpRequest.POST;
import static akka.http.javadsl.model.MediaTypes.APPLICATION_JSON;
import static akka.http.javadsl.model.StatusCodes.BAD_REQUEST;
import static akka.http.javadsl.model.StatusCodes.FORBIDDEN;
import static akka.http.javadsl.model.StatusCodes.NOT_FOUND;
import static akka.http.javadsl.model.StatusCodes.OK;
import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;
import static com.amazonaws.util.json.Jackson.toJsonString;
import static com.wt.domain.AdviseType.Close;
import static com.wt.domain.AdviseType.Issue;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.CorporateEventType.BONUS;
import static com.wt.domain.CorporateEventType.SPLIT;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.Purpose.PARTIALWITHDRAWAL;
import static com.wt.domain.UserAcceptanceMode.Auto;
import static com.wt.domain.UserAcceptanceMode.Manual;
import static com.wt.domain.UserFundState.ARCHIVED;
import static com.wt.domain.UserResponseType.Approve;
import static com.wt.domain.UserResponseType.Archive;
import static com.wt.domain.UserResponseType.ExitFund;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.lastDayEOD;
import static com.wt.utils.DateUtils.todayBOD;
import static com.wt.utils.DoublesUtil.round;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.not.wt.pkg.to.override.main.config.TradeSplitterSpecificTestAppConfiguration;
import com.wt.api.write.WDService;
import com.wt.config.utils.WDProperties;
import com.wt.domain.AddLumpSum;
import com.wt.domain.Advise;
import com.wt.domain.AdviseType;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.BrokerName;
import com.wt.domain.CorporateEventType;
import com.wt.domain.CurrentUserFundDb;
import com.wt.domain.Fund;
import com.wt.domain.FundConstituent;
import com.wt.domain.FundSubscription;
import com.wt.domain.FundWithAdviserAndInvestingMode;
import com.wt.domain.InvestingMode;
import com.wt.domain.InvestorApproval;
import com.wt.domain.MutualFundInstrument;
import com.wt.domain.Order;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.Purpose;
import com.wt.domain.ReceivedUserApprovalResponse;
import com.wt.domain.Trade;
import com.wt.domain.UserAcceptanceMode;
import com.wt.domain.UserAdvise;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserEquityAdvise;
import com.wt.domain.UserFund;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.UserMFAdvise;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.repo.FundAdviseRepository;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.repo.OrdersRepository;
import com.wt.domain.repo.UserEquityAdviseRepository;
import com.wt.domain.repo.UserFundRepository;
import com.wt.domain.repo.UserMFAdviseRepository;
import com.wt.domain.repo.UserRepository;
import com.wt.domain.write.ConsolidatedPendingRebalancingUserResponse;
import com.wt.domain.write.ConsolidatedRejectedUserAdviseResponses;
import com.wt.domain.write.RealTimeInstrumentPrice;
import com.wt.domain.write.commands.AddMoreAllocationToAnExistingAdvice;
import com.wt.domain.write.commands.AdviserCorporateEventPrimaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSecondaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSell;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CheckForCorporateEvent;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.CloseFund;
import com.wt.domain.write.commands.ExecutePostMarketUserCommands;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.GetFundRebalancingView;
import com.wt.domain.write.commands.GetWalletMoney;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.ReceivedUserAcceptanceMode;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateMFInstruments;
import com.wt.domain.write.commands.UpdateRecommendedEntryPrice;
import com.wt.domain.write.commands.UpdateSplitTypeEvent;
import com.wt.domain.write.events.AdviseOrderTradeExecuted;
import com.wt.domain.write.events.AdviserCredentialsWithType;
import com.wt.domain.write.events.AllRejectedUserFundAdvises;
import com.wt.domain.write.events.BonusEvent;
import com.wt.domain.write.events.CurrentUserAdviseFlushedDueToExit;
import com.wt.domain.write.events.DividendEvent;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.FundConstituentListResponse;
import com.wt.domain.write.events.FundEvent;
import com.wt.domain.write.events.FundSuccessfullyUnsubscribed;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.InvestorCredentialsWithType;
import com.wt.domain.write.events.IssueType;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.Result;
import com.wt.domain.write.events.RetryRejectedAdvise;
import com.wt.domain.write.events.SpecialCircumstancesBuy;
import com.wt.domain.write.events.SpecialCircumstancesSell;
import com.wt.domain.write.events.UpdateCorporateEvent;
import com.wt.domain.write.events.UserAdviseEvent;
import com.wt.domain.write.events.UserAdviseTransactionReceived;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserFundSubscriptionDetails;
import com.wt.domain.write.events.UserResponseReceived;
import com.wt.utils.CoreDBTest;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;
import com.wt.utils.OTPUtils;
import com.wt.utils.ViewPopulater;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import akka.persistence.fsm.PersistentFSM.StateChangeEvent;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import commons.Retry;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TradeSplitterSpecificTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(properties="dbUploadRate.in.milliseconds=1")
public class WDTradeSplitterShould extends JUnitRouteTest {
  private static final double ENTRYPRICE = 1030d;
  private static final double ENTRYPRICE_LOW = 250d;
  private static final double ENTRYPRICE_2 = 515d;
  private static final double ENTRYPRICE_3 = 1040d;
  private static final double ENTRYPRICE_4 = 101d;

  private static final RegisterAdviserFromUserPool ADVISER = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi",
      "swapnil@bpwealth.com", "123publicId", "Wealth Tech", "");
  private static final RegisterAdviserFromUserPool ADVISER2 = new RegisterAdviserFromUserPool("quanttech", "QC",
      "qc@qc.com", "123publicId", "Quant Tech", "");
  private static final double INITIAL_AMOUNT = 5000000;
  private static final String ADVISER_USERNAME = "swapnil";
  private static final String TICKER1 = "IN1-NSE-EQ";
  private static final String TICKER_EOD = "INEOD-NSE-EQ";
  private static final double ALLOCATION1 = 20.0;
  private static final double ALLOCATION_25 = 25.0;
  private static final double ALLOCATION1_NEW = 95.0;
  private static final double ALLOCATION_LESS = 1.0;
  private static final int TOKEN_1 = 133;
  private static final String SYMBOL_1 = "ACC";
  private static final String TICKER2 = "IN2-NSE-EQ";
  private static final int TOKEN_2 = 134;
  private static final int TOKEN_30 = 1350;
  private static final String SYMBOL_2 = "SBIN";
  private static final double ALLOCATION2 = 15.0;
  private static final double ALLOCATION_HIGH = 80.0;
  private static final double ALLOCATION2_NEW = 5.0;
  private static final String TICKER3 = "IN3-NSE-EQ";
  private static final String TICKER30 = "IN30-NSE-EQ";
  private static final int TOKEN_3 = 135;
  private static final String SYMBOL_3 = "ICICIBANK";
  private static final double ALLOCATION3 = 23.0;
  private static final double ALLOCATION3_1 = 23.75;
  private static final String TICKER4 = "IN4-NSE-EQ";
  private static final int INT_TOKEN_4 = 126;
  private static final String TICKER5 = "IN5-NSE-EQ";
  private static final int INT_TOKEN_5 = 127;
  private static final String SYMBOL_5 = "VEDL";
  private static final String TICKER6 = "IN6-NSE-EQ";
  private static final String SYMBOL_6 = "XYZ";
  private static final int INT_TOKEN_6 = 147;
  private static final String TICKER7 = "IN7-NSE-EQ";
  private static final int INT_TOKEN_7 = 157;
  private static final String SYMBOL_7 = "ABC";
  private static final String TICKER8 = "IN7-NSE-EQ";
  private static final int INT_TOKEN_8 = 167;
  private static final String SYMBOL_8 = "PQR";
  private static final String TICKER9 = "IN9-NSE-EQ";
  private static final int INT_TOKEN_9 = 177;
  private static final String SYMBOL_9 = "UVW";
  private static final String TICKER10 = "IN10-NSE-EQ";
  private static final int INT_TOKEN_10 = 187;
  private static final String SYMBOL_10 = "DEF";
  private static final String TICKER11 = "IN11-NSE-EQ";
  private static final int INT_TOKEN_11 = 197;
  private static final String SYMBOL_11 = "LMN";
  private static final String TICKER12 = "IN12-NSE-EQ";
  private static final int INT_TOKEN_12 = 199;
  private static final String SYMBOL_12 = "HIJ";
  private static final String TICKER13 = "IN13-NSE-EQ";
  private static final String SYMBOL_13 = "KLM";
  private static final int TOKEN_13 = 198;
  private static final int TOKEN_13a = 208;
  private static final String TICKER_FOR_REISSUE = "INREISSUE-NSE-EQ";
  private static final int TOKEN_FOR_REISSUE = 130;
  private static final String SYMBOL_FOR_REISSUE = "NOP";
  private static final double EXPECTED_AVERAGE_ENTRY_PRICE_FOR_INITIAL_ISSUE = 1030.12;
  private static final double EXPECTED_AVERAGE_ENTRY_PRICE_FOR_RE_ISSUE_POST_BONUS_EVENT = 721.12;
  private static final double EXPECTED_INITIAL_ISSUED_ADVICE_ON_A_GIVEN_TOKEN_ALLOCATION_VALUE = 60000.0;
  private static final double EXPECTED_NEXT_ISSUED_ADVICE_ON_A_GIVEN_TOKEN_ALLOCATION_VALUE = 44998.91;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_RE_ISSUE = 203;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_RE_ISSUE = 203;
  private static final double USER_FUND_AVAILABLE_CASH_POST_TWO_ISSUED_ADVICES_ON_A_GIVEN_TOKEN = 195437.0;
  private static final String MF_TICKER1 = "INF515L01841-MF";
  private static final String MF_FOLIO_TICKER1 = "INF789F01VF3-MF";
  private static final String MF_FOLIO_TICKER2 = "INF789F01VH9-MF";
  private static final String SCHEME_CODE1 = "AXIS-DR";
  private static final String SCHEME_CODE2 = "AXIS-GR";
  private static final String SCHEME_CODE3 = "AXIS-DP";
  private static final String MIN_INVESTMENT = "5000";
  private static final String MIN_MULTIPLIER = "100.0";
  private static final String SCHEME_TYPE = "ELSS";
  private static final String RTA = "KARVY";
  private static final String EXIT_LOAD = "1.0";
  private static final String MF_TICKER2 = "INF200K01QS4-MF";
  private static final String MF_SCHEME3 = "204";
  private static final String MF_TICKER3 = "INF204KA1UU2-MF";
  private static final String MF_SCHEME1 = "515";
  private static final String MF_SCHEME2 = "200";
  private static final String FUND_NAME = "WeeklyPicks";
  private static final String OTHER_FUND_NAME = "MonthlyPicks";
  private static final String FUND_DESCRIPTION = FUND_NAME + "Description";
  private static final String FUND_THEME = "Theme";
  private static final double FUND_MIN_SIP = 20000.;
  private static final double FUND_MIN_LUMPSUM = 200000.;
  private static final String FUND_RISK_LEVEL = "MEDIUM";
  private static final long STARTDATE = 1388082600000L;
  private static final long ENDDATE = 3471186600000L;
  private static final long ENTRYTIME = 0;
  private static final long ENTRYTIME_2 = 1;
  private static final long ENTRYTIME_3 = 2;
  private static final double EXITPRICE = 1090.;
  private static final long EXITTIME = 3;
  private static final double SUBSCRIPTION_LUMPSUM = 300000.;
  private static final double SUBSCRIPTION_POST_REPLICATION = 298230.0;
  private static final double SUBSCRIPTION_POST_REPLICATION_1 = 60000.0;
  private static final double SUBSCRIPTION_LUMPSUM_IN_ORDER_TO_GET_A_SINGLE_QUANTITY = 5200.;
  private static final double SUBSCRIPTION_LUMPSUM_LESS = 30000.;
  private static final double ADD_LUMPSUM = 200000.;
  private static final double SUBSCRIPTION_SIPAMOUNT = 0.;
  private static final String SUBSCRIPTION_SIP_DATE = "";
  private static final String SUBSCRIPTION_WEALTHCODE = "2108";
  private static final String SUBSCRIPTION_USER_NAME = "subscriber";
  private static final String SUBSCRIPTION_USER_EMAIL = "subscriber@yopmail.com";
  private static final String INVESTOR_NAME = "FirstName LastName";
  private static final double CASH_COMPONENT_POST_ISSUE_OF_TWO_ADVISES_WHEN_AWAITING_A_RESPONSE = 195000.0;
  private static final double ISSUE_ADVISE_ALLOCATION_VALUE_FOR_PENDING_ADVISE = 67678.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_1 = 60360.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_2 = 45300.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_2_LESS = 303.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_3 = 6045.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_LUMPSUM_1 = 41720.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_LUMPSUM_2 = 31300.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_PARTIALCLOSE_POST_LUMPSUM_1 = 31400.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_REJECTION_POST_LUMPSUM_1 = 42040.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_REJECTION_POST_LUMPSUM_2 = 22824.5;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_PARTIALCLOSE_POST_LUMPSUM_2 = 31400.0;
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_REJECTION_AND_FREEZED_ADVICE_POST_LUMPSUM_2 = 2100.0;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1 = 20.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_MULTIPLE_1 = 35.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2_NEW = 100.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_3_NEW = 50.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1 = 60000.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_ADVISES_1 = 105000.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_FOR_SINGLE_CLOSE_ADVISE = 25.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_1 = 15.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_1 = 45000.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1 = 11.72;
  private static final double EXPECTED_CLOSED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1 = 19.64;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1 = 29999.28;
  private static final int EXPECTED_CLOSE_ADVICE_PENDING_CLOSE_QUANTITY_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1 = 11;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_2 = 10.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_2 = 30000.;
  private static final int EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_SINGLE_CLOSE_ADVISE = 14;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_CLOSE_ADVISES = 50.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_ISSUE_CLOSE_ADVISES = 50.;
  private static final int EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_CLOSE_ADVISES = 28;
  private static final int EXPECTED_REJECTED_ADVICE_NUMBER_OF_SHARES_FOR_THE_ADVISE = 289;
  private static final double EXPECTED_REJECTED_BUY_ALLOCATION_VALUE_FOR_THE_ADVISE = 298230.0;
  private static final double EXPECTED_CLOSE_ALLOCATION_VALUE_FOR_MULTIPLE_CLOSE_ADVISES = 29120.0;
  private static final int EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_ISSUE_CLOSE_ADVISES = 144;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_FOR_MULTIPLE_ADVISES_1 = 35.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2_NEW = 15000.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_3_NEW = 70500.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_FOR_QUANTITY_LESS_THAN_ONE = 1040.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2 = 18.75;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2 = 45000.;
  private static final double EXPECTEDAVERAGEENTRYPRICE1 = 1040.12;
  private static final double EXPECTEDAVERAGEENTRYPRICE2 = 1040.12;
  private static final double EXPECTEDAVERAGEENTRYPRICE_3 = 1040.15;
  private static final double EXPECTEDAVERAGEENTRYPRICE_2_NOT_TRADED = 0.;
  private static final double EXPECTEDAVERAGEENTRYPRICE_POST_LUMPSUM_1 = 1063.49;
  private static final double EXPECTEDAVERAGEENTRYPRICE_POST_PARTIALCLOSE_POST_LUMPSUM_1 = 1059.66;
  private static final double EXPECTEDAVERAGEENTRYPRICE_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_1 = 1100.12;
  private static final double EXPECTEDAVERAGEENTRYPRICE_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_3 = 101.13;
  private static final double EXPECTEDAVERAGEENTRYPRICE_POST_PARTIALCLOSE_POST_LUMPSUM_2 = 532.0;
  private static final double EXPECTEDAVERAGEENTRYPRICE_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_2 = 550.08;
  private static final double EXPECTEDAVERAGEENTRYPRICE_POST_LUMPSUM_2 = 531.87;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_LUMPSUM_1 = 95;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_1 = 100;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM_1 = 95;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM1_2nd_Time = 127;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM2_2nd_Time = 191;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_1 = 86;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_PARTIALCLOSE_WITHDRAWAL = 18;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_1 = 38;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_REJECTION_AND_FREZZED_ADVISE_POST_LUMPSUM_1 = 38;
  private static final int EXPECTED_TOTAL_SELL_ORDER_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_1 = 14;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_1 = 58;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_1 = 58;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 = 57;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 = 57;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_MULTIPLE_APPROVAL_ISSUE_CLOSE_1 = 28;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_MULTIPLE_APPROVAL_ISSUE_CLOSE_1 = 28;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_APPROVAL = 100;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_PRE_APPROVAL = 100;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_APPROVAL = 100;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_APPROVAL_1 = 85;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_APPROVAL_1 = 85;
  private static final int EXPECTED_BUY_ORDER_QUANTITY_POST_APPROVAL_2 = 57;
  private static final int EXPECTED_SELL_ORDER_QUANTITY_POST_APPROVAL_2 = 11;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_2 = 274;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM_2 = 143;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_LUMPSUM_2 = 143;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_2 = 144;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_2 = 144;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_2 = 3;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_2 = 3;
  private static final double POST_HIGHTRADE_SUB_FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM = 60000.0;
  private static final double POST_HIGHTRADE_SUB_TOTALINVESTMENT_PRELUMPSUM = 348000.0;
  private static final double POST_HIGHTRADE_SUB_FUND_AVAILABLE_CASH_POST_MIRRORING_POSTLUMPSUM = 100000.0;
  private static final double POST_HIGHTRADE_SUB_TOTALINVESTMENT_POSTLUMPSUM = 580000.0;
  private static final double FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM = 194340.0;
  private static final double FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_ISSUE = 195000;
  private static final double FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_CLOSE = 270000;
  private static final double FUND_AVAILABLE_CASH_AFTER_REMOVAL_POST_MERGING_ISSUE_CLOSE = 300000;
  private static final double FUND_AVAILABLE_CASH_POST_MULTIPLE_ISSUE_CLOSE = 0.;
  private static final double FUND_HYPOTHETICAL_CASH_POST_MULTIPLE_ISSUE_CLOSE = 156993.12;
  private static final double USERFUND_CASH_POST_MULTIPLE_ISSUE_CLOSE = 157011.12;
  private static final double FUND_AVAILABLE_CASH_POST_MERGING_CLOSE_CLOSE = 240712.75;
  private static final double FUND_AVAILABLE_CASH_POST_REMOVAL_MERGING_CLOSE_ISSUE = 240712.75;
  private static final double FUND_AVAILABLE_HYPO_CASH_POST_REMOVAL_MERGING_CLOSE_ISSUE = 252705.28;
  private static final double FUND_AVAILABLE_CASH_POST_MERGING_CLOSE_ISSUE = 212501.22;
  private static final double FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM_1 = 209604.97;
  private static final double FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM_POST_REJECTION_1 = 23955.0;
  private static final double FUND_AVAILABLE_CASH_POST_LUMPSUMADDITION_2nd_time = 429191.63;
  private static final double FUND_AVAILABLE_CASH_POST_PARTIALCLOSE_POST_WITHDRAWAL = 189045.72;
  private static final double FUND_AVAILABLE_CASH_POST_LUMPSUMADDITION = 321320.0;
  private static final double FUND_AVAILABLE_CASH_POST_PARTIALCLOSE_POST_LUMPSUMADDITION = 346804.97;
  private static final double FUND_AVAILABLE_CASH_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUMADDITION = 140075.0;
  private static final double EXPECTED_MF_TOTAL_TRADED_QUANTITY_1 = 4241.7545;
  private static final double EXPECTED_MF_TOTAL_TRADED_QUANTITY_2 = 817.1617;
  private static final double FUND_AVAILABLE_CASH_POST_MIRRORING = 194340.0;
  private static final double PORTFOLIO_VALUE_POST_SUBSCRIPTION = 299380.0;
  private static final String BROKER_NAME_MOCK = "MOCK";
  private static final double EXPECTED_ISSUED_ADVICE_ALLOCATION_3 = 68757.49;
  private static final double EXPECTED_AVERAGE_ENTRY_PRICE_3 = 1040.12;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_3 = 66;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_3 = 66;
  private static final double FUND_AVAILABLE_CASH_POST_MIRRORING_ADVISE_ISSUE = 125691.75;
  private static final String ADVISEID1 = "1a";
  private static final long ISSUETIME1 = currentTimeInMillis();
  private static final long ISSUETIME2 = currentTimeInMillis();
  private static final String ADVISEID2 = "1b";
  private static final String ADVISEID3 = "1c";
  private static final String ADVISEID4 = "1d";
  private static final String MFADVISEID1 = "MF1";
  private static final String MFADVISEID2 = "MF2";
  private static final String MFADVISEID3 = "MF3";
  private static final long ISSUETIME3 = currentTimeInMillis();
  private static final double PARTIAL_CLOSING_ALLOCATION = 5.;
  private static final double FULL_CLOSING_ALLOCATION = 20.;
  private static final double PARTIAL_CLOSING_ALLOCATION_TO_GET_QUANTITY_LESS_THAN_ONE = 10.;
  private static final long ACTUAL_EXITDATE = currentTimeInMillis();
  private static final double PARTIAL_CLOSING_ALLOCATION1 = 47.5;
  private static final double ACTUAL_EXITPRICE = 1090.23;
  private static final double EXPECTED_VALUE_AFTER_WITHDRAWAL_WITH_LOW_ALLOCATION = 287910.0;
  private static final double EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1 = 25.;
  private static final double EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_2 = 50.;
  private static final double EXPECTED_ABSOLUTE_FULL_ISSUE_ADVICE_ALLOCATION = 100.;
  private static final double EXPECTED_ABSOLUTE_FULL_ISSUE_ADVICE_ALLOCATION_VALUE = 298230.;
  private static final double EXPECTED_AVERAGE_PARTIAL_EXIT_PRICE_1 = 1090.36;
  private static final double EXPECTED_AVERAGE_ENTRY_POST_BONUS = 520.06;
  private static final double EXPECTED_CUMULATIVE_DIVIDEND = 150.5;
  private static final double EXPECTED_AVERAGE_ENTRY_POST_CORPORATE_EVENTS = 150.0;
  private static final double EXPECTED_AVERAGE_EXIT_POST_SPLIT = 165.09;
  private static final int EXPECTED_TOTAL_PARTIAL_EXIT_ORDER_QUANTITY_1 = 14;
  private static final int EXPECTED_TOTAL_PARTIAL_ORDER_TRADED_QUANTITY_1 = 14;
  private static final double FUND_AVAILABLE_CASH_POST_PARTIAL_CLOSE_ADVISE = 140956.72;
  private static final double COMPLETE_CLOSING_ALLOCATION = 15.;
  private static final double EXPECTED_ABSOLUTE_COMPLETE_CLOSE_ADVICE_ALLOCATION_2 = 0.;
  private static final double EXPECTED_AVERAGE_COMPLETE_EXIT_PRICE_1 = 0.;
  private static final double EXPECTED_AVERAGE_ENTRY_PRICE_ON_COMPLETE_EXIT = 0.;
  private static final int EXPECTED_TOTAL_COMPLETE_EXIT_ORDER_QUANTITY_2 = 43;
  private static final int EXPECTED_TOTAL_COMPLETE_ORDER_TRADED_QUANTITY_2 = 43;
  private static final int EXPECTED_TOTAL_PRIMARY_BUY_QUANTITY_POST_DEMERGER = 43;
  private static final int EXPECTED_TOTAL_SECONDARY_BUY_QUANTITY_POST_DEMERGER = 43;
  private static final double FUND_AVAILABLE_CASH_POST_COMPLETE_CLOSE_ADVISE = 187841.86;
  private static final double EXPECTED_EXIT_ALLOCATION_1 = 0.;
  private static final double EXPECTED_EXIT_PRICE_1 = 0.;
  private static final double EXPECTED_EXIT_ALLOCATION_2 = 0.;
  private static final double EXPECTED_EXIT_PRICE_2 = 0.;
  private static final double EXPECTED_EXIT_ALLOCATION_3 = 0.;
  private static final double EXPECTED_EXIT_PRICE_3 = 0.;
  private static final int EXPECTED_TOTAL_EXIT_ORDER_QUANTITY_1 = 58;
  private static final int EXPECTED_TOTAL_ENTRY_ORDER_QUANTITY_1 = 289;
  private static final int EXPECTED_TOTAL_ENTRY_TRADED_QUANTITY_1 = 289;
  private static final int EXPECTED_TOTAL_ENTRY_ORDER_QUANTITY_2 = 1;
  private static final int EXPECTED_TOTAL_ENTRY_TRADED_QUANTITY_2 = 1;
  private static final int EXPECTED_TOTAL_EXIT_TRADED_QUANTITY_1 = 58;
  private static final int EXPECTED_TOTAL_EXIT_ORDER_QUANTITY_2 = 43;
  private static final int EXPECTED_TOTAL_EXIT_TRADED_QUANTITY_2 = 43;
  private static final int EXPECTED_TOTAL_EXIT_ORDER_QUANTITY_3 = 66;
  private static final int EXPECTED_TOTAL_EXIT_TRADED_QUANTITY_3 = 66;
  private static final double EXPECTED_WALLET_MONEY_POST_EXIT_FUND_WITH_NO_ADVICES = 5000000.0;
  private static final double FUND_AVAILABLE_CASH_POST_ADD_LUMPSUM_FUND = 500000.;
  private static final double CASH_COMPONENT_AFTER_PARTIAL_WITHDRAWAL = 174865.75;
  private static final double CASH_COMPONENT_AFTER_PARTIAL_WITHDRAWAL_WITHOUT_ADVISES = 270000.0;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES = 65.;
  private static final double EXPECTED_ADVISE_REMAINING_ALLOCATION_ADDITION_TO_ISSUED_ADVICE = 35.;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_POSTLUMPSUM = 65.;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_AND_PARTIALCLOSE_POSTLUMPSUM = 70.;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES_AND_FREEZED_ADVICE_POSTLUMPSUM = 59.;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES = 42.;
  private static final double EXPECTED_USER_FUNDCASH_POST_UNSUBSCRIPTION_WITH_PENDING_ADVICES = 299877.25;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES_AND_A_PARTIAL_EXIT = 47.;
  private static final double EXPECTED_ADVISER_FUNDCASH_PRE_SPECIAL_BUY_FOR_ADVISE2 = 239670.0;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_SPECIAL_BUY_FOR_ADVISE2 = 234470.0;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_SPECIAL_BUY_FOR_ADVISE1 = 225110.0;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES_AND_A_PARTIAL_AND_A_COMPLETE_EXIT = 62.;
  private static final double EXPECTED_ADVISER_FUNDCASH_POST_EXIT_FUND = 62.;
  private static final double CUMULATIVE_PAF_POST_CORPORATE_EVENT = 2.0;
  private static final double UNREALIZED_PNL_POST_CORPORATE_EVENT_1 = 870.0;
  private static final double UNREALIZED_PNL_POST_CORPORATE_EVENT_2 = 609.0;
  private static final double REALIZED_PNL_POST_CORPORATE_EVENT_1 = 280.0;
  private static final double REALIZED_PNL_POST_CORPORATE_EVENT_2 = 860.0;
  private static final int TOTAL_QUANTITY_REMAINING_POST_CORPORATE_EVENTS = 29;
  private static final int TOTAL_QUANTITY_REMAINING_POST_DEMERGER_SELL = 0;
  private static final double EXPECTED_SELL_PRICE_FOR_DEMERGER_EVENT = 1092.0;
  private static final double EXPECTED_AVERAGE_PRICE_FOR_PRIMARY_BUY_DEMERGER_EVENT = 346.71;
  private static final double EXPECTED_AVERAGE_PRICE_FOR_SECONDARY_BUY_DEMERGER_EVENT = 693.4121;
  private static final double EXPECTED_AVERAGE_PRICE_FOR_FIRST_SECONDARY_BUY_DEMERGER_EVENT = 208.02;
  private static final double EXPECTED_AVERAGE_PRICE_FOR_SECOND_SECONDARY_BUY_DEMERGER_EVENT = 485.38846;
  private static final double EXPECTED_AVERAGE_PRICE_FOR_FIRST_SECONDARY_BUY_DEMERGER_EVENT_WITHOUT_PRIMARY_BUY = 312.04;
  private static final double EXPECTED_AVERAGE_PRICE_FOR_SECOND_SECONDARY_BUY_DEMERGER_EVENT_WITHOUT_PRIMARY_BUY = 728.08545;
  private static final double CURRENT_MARKET_PRICE = 1040.;
  private static final double CURRENT_MARKET_PRICE_1 = 1100.;
  private static final double FREEZE_CURRENT_MARKET_PRICE_1 = 110.;
  private static final double CURRENT_MARKET_PRICE_2 = 570.;
  private static final double CURRENT_MARKET_PRICE_TICKER2 = 520.;
  private static final double CURRENT_MARKET_PRICE_3 = 1100.;
  private static final double CURRENT_MARKET_PRICE_TICKER2_2 = 550.;
  private static final double LAST_NAV1 = 14.1168;
  private static final double LAST_NAV2 = 54.9677;
  private static final InvestingMode INVESTING_MODE = VIRTUAL;
  private static final double VERY_HIGH_STOCK_PRICE = 700000.;
  private static final String EXCHANGE = "NSE";
  private static final double RECOMMENDED_ENTRY_PRICE = 25.50;
  private static final double RECOMMENDED_ENTRY_PRICE_1 = 2000.50;
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;
  private static final String BASKETORDERID_FOR_TEST_Close = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;
  private static final double CASH_COMPONENT_AFTER_PARTIAL_WITHDRAWAL_with_rejection = 175000.51;
  private static final double CASH_COMPONENT_1 = 100.;
  private static final double CASH_COMPONENT_2 = 78.93;
  private static final double CASH_COMPONENT_3 = 63.13;
  private static final double FUND_ALLOCATION_TICKER1_1 = 21.07;
  private static final double FUND_ALLOCATION_TICKER1_2 = 20.75;
  private static final double FUND_ALLOCATION_TICKER2_1 = 16.12;
  private static final int QUANTITY_WITHDRAWN_TILL_DATE = 1;
  private static final Object EXPECTED_TOTAL_ORDER_QUANTITY_IN_MANUAL_MODE = 43;
  private static final Object EXPECTED_TOTAL_TRADED_QUANTITY_MANUAL_MODE = 43;
  private static final Object EXPECTED_TOTAL_ORDER_QUANTITY_IN_REPLICATION = 43;
  private static final Object EXPECTED_TOTAL_TRADED_QUANTITY_IN_REPLICATION = 43;
  private static final Object EXPECTED_TOTAL_ORDER_QUANTITY_IN_MIXED_FUND = 43;
  private static final Object EXPECTED_TOTAL_TRADED_QUANTITY_IN_MIXED_FUND = 43;
  private static final Object EXPECTEDAVERAGEENTRYPRICE_2_WHEN_ADDING_LUMPSUM = 520.12;
  private static final Object EXPECTEDAVERAGEENTRYPRICE_2_WHEN_ADDING_LUMPSUM_POST_PARTIAL_CLOSE = 520.12;
  private static final Object EXPECTEDAVERAGEENTRYPRICE_2_IN_MULTIPLE_ADD_LUMPSUM = 520.12;
  private static final double ACTUAL_INVESTMENT = 299877.25;
  private static final double ACTUAL_LUMPSUM_ADDED = 199224.75;
  private static final double  VIRTUALCASH_POST_SUBSCRIPTION = 4701114.5;
  private static final double  VIRTUALCASH_POST_LUMPSUM = 4502204.0;
  private static final double  VIRTUALCASH_POST_WITHDRAW = 4601391.43;
  private static final double  VIRTUALCASH_POST_EXIT = 5000000.0;
  private static final double FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_ISSUE_TRADE_EXECUTION = 195987.5;
  private static final double FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_CLOSE_POST_TRADE_EXECUTION = 270876.5;
  
  @Autowired
  private ActorSystem system;
  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private FundRepository fundRepository;
  @Autowired
  private FundAdviseRepository adviseRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private OrdersRepository ordersRepository;
  @Autowired
  private UserFundRepository userFundRepository;
  @Autowired
  private UserEquityAdviseRepository userEquityAdviseRepository;
  @Autowired
  private UserMFAdviseRepository userMFAdviseRepository;
  @Autowired
  private WDService service;
  @Autowired
  private CoreDBTest dbTest;
  private @Autowired OTPUtils otpGeneratorMock;
  private @Autowired JournalProvider journalProvider;
  @Autowired
  private ActorMaterializer materializer;
  @Autowired
  private WDActorSelections actorSelections;
  private ActorRef universeRootActor;
  @SuppressWarnings("unused")
  private ActorRef equityOrderRootActor;
  @SuppressWarnings("unused")
  private ActorRef mfOrderRootActor;
  @Autowired
  private AmazonSimpleEmailService sesClient;
  @Autowired
  @Qualifier("CognitoUserPoolForInvestor")
  private AWSCognitoIdentityProvider investorUserPoolMock;

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Rule
  public Retry retry = new Retry(5);

  @Override
  public FiniteDuration awaitDuration() {
    return FiniteDuration.apply(1, TimeUnit.MINUTES);
  }

  @Before
  public void setupDB() throws Exception {
    dbTest.createSchema();
    service.createActors();
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    equityOrderRootActor = system.actorOf(SpringExtProvider.get(system).props("EquityOrderRootActor"), "equity-order-aggregator");
    mfOrderRootActor = system.actorOf(SpringExtProvider.get(system).props("MFOrderRootActor"), "mf-order-aggregator");
    doReturn(WDProperties.DUMMY_HASHCODE_FOR_USER_ACTIVATION).when(otpGeneratorMock).getUniqueHashCode();
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    Mockito.when(investorUserPoolMock.adminGetUser(Mockito.any(AdminGetUserRequest.class)))
    .thenThrow(new UserNotFoundException("This user is not registered."));
    
    Mockito.when(investorUserPoolMock.adminCreateUser(Mockito.any(AdminCreateUserRequest.class)))
    .thenAnswer((args) -> {
      AdminCreateUserRequest request = (AdminCreateUserRequest) args.getArguments()[0];
      AdminCreateUserResult result = new AdminCreateUserResult()
          .withUser(new UserType()
                      .withAttributes(request.getUserAttributes())
                      .withEnabled(true)
                      .withUsername(request.getUsername())
                      .withUserCreateDate(new Date(DateUtils.todayBOD()))
                      .withUserLastModifiedDate(new Date(DateUtils.todayBOD())));
      return result;
    });
  }

  @After
  public void shutDown() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    try {
      deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());
  }

  private void sendEODPriceToEquityActor(double currentMarketPrice, String token, long time) {
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(currentMarketPrice, time, token), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }

  private void sendEODPriceWithPafToEquityActor(PriceWithPaf priceWithPaf) {
    universeRootActor.tell(new PriceWithPaf(new InstrumentPrice(priceWithPaf.getPrice().getPrice(),
        priceWithPaf.getPrice().getTime(), priceWithPaf.getPrice().getWdId()), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }

  private void sendLivePriceToEquityActor(double currentMarketPrice, String token) {
    universeRootActor.tell(new RealTimeInstrumentPrice(new InstrumentPrice(currentMarketPrice, currentTimeInMillis(), token)), ActorRef.noSender());
  }

  @Test public void 
  return_200_when_adviser_is_successfully_registered() throws InterruptedException {
    registerAdviserProcess();
  }

  @Test public void 
  return_200_when_adviser_floats_a_fund_and_then_closes_the_fund() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    Failed failedResponseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    waitForViewToPopulate(1);
    
    response = delete("/funds", new CloseFund(ADVISER_USERNAME, FUND_NAME, currentTimeInMillis()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund closed.");
    
    waitForViewToPopulate(1);
    
    assertThat(fundRepository.findById(FUND_NAME).orElse(null)).isEqualTo(null);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);

    waitForViewToPopulate(1);

    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(NOT_FOUND).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage()).isEqualTo("Fund does not exist");
  }

  @Test public void 
  return_200_when_adviser_issues_advices() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_1, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_2, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    waitForProcessToComplete(1);

    double currentCash = getCurrentFundCash(ADVISER_USERNAME, FUND_NAME);
    List<FundConstituent> currentConstituents = getFundRebalancingView(ADVISER_USERNAME, FUND_NAME);
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    assertThat(currentCash).isEqualTo(CASH_COMPONENT_1);
    assertThat(currentConstituents.size()).isEqualTo(1);
    responseEntity = response.entity(unmarshaller(Done.class));
    String firstAdviseId = responseEntity.getId();
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    currentCash = getCurrentFundCash(ADVISER_USERNAME, FUND_NAME);
    currentConstituents = getFundRebalancingView(ADVISER_USERNAME, FUND_NAME);
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE_2,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    assertThat(currentCash).isEqualTo(CASH_COMPONENT_2);
    assertThat(getAllocationFromFundConstituents(currentConstituents, TICKER1)).isEqualTo(FUND_ALLOCATION_TICKER1_1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    waitForProcessToComplete(1);
    
    response = authenticatedAdviserPost("/recoentryprice",
        new UpdateRecommendedEntryPrice(FREEZE_CURRENT_MARKET_PRICE_1, firstAdviseId, FUND_NAME));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForViewToPopulate(1);

    currentCash = getCurrentFundCash(ADVISER_USERNAME, FUND_NAME);
    currentConstituents = getFundRebalancingView(ADVISER_USERNAME, FUND_NAME);
    assertThat(currentCash).isEqualTo(CASH_COMPONENT_3);
    assertThat(getAllocationFromFundConstituents(currentConstituents, TICKER1)).isEqualTo(FUND_ALLOCATION_TICKER1_2);
    assertThat(getAllocationFromFundConstituents(currentConstituents, TICKER2)).isEqualTo(FUND_ALLOCATION_TICKER2_1);
  }

  @Test public void 
  unsubscribing_from_a_fund_that_has_a_rejected_advise() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER7, SYMBOL_7, INT_TOKEN_7);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER7, currentTimeInMillis());    
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER7, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");


    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, TICKER2);

    subscriptionProcess();
    waitForProcessToComplete(1);
    waitForViewToPopulate(1);
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
         "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("UnSubscribe in process. You will receive a notification once its done.");
    waitForProcessToComplete(1);
    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    Optional<CurrentUserFundDb> potentialUserFund = userFundRepository.findById(userFundCompositeKey);
    assertThat(potentialUserFund.isPresent()).isFalse();
  }

  @Test public void 
  closing_an_advise_that_has_been_rejected_during_subscription() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER7, SYMBOL_7, INT_TOKEN_7);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER7, currentTimeInMillis());    
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());

    TestRouteResult response;
    Done responseEntity;
    Failed failedResponseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER7, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");


    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, TICKER2);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(BAD_REQUEST).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage())
          .isEqualTo("Your request to Subscribe/AddLumpSum cannot be processed.");

    subscriptionProcess();
    waitForProcessToComplete(1);
    waitForViewToPopulate(1);
    
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, ADVISEID1, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);

    List<UserFundEvent> allUserFundPendingResponseEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
    .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
    .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
    .filter(userFundEvent -> userFundEvent instanceof UserResponseReceived)
    .runWith(Sink.seq(), materializer)
    .toCompletableFuture().get();

    waitForProcessToComplete(1);
    assertThat(allUserFundPendingResponseEvents.size()).isEqualTo(0);
    
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(BAD_REQUEST).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage())
          .isEqualTo("Your request to Subscribe/AddLumpSum cannot be processed.");

    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, PARTIALWITHDRAWAL, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage())
          .isEqualTo("You cannot send this request before getting the list of advises to be exited.");
  }

  @Test public void 
  return_200_when_an_investor_is_successfully_registered() throws ParseException {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(SUBSCRIPTION_USER_NAME, INVESTOR_NAME, "", SUBSCRIPTION_USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(InvestorCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(InvestorCredentialsWithType.class);
  }

  @Test public void 
  return_200_when_investor_successfully_subscribes_to_the_MF_fund() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);
    
    autoApprove(FUND_NAME);

    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL.getInvestingMode())))
            .isTrue();
    updateInstrumentsMapOfMFActor(MF_TICKER1, SYMBOL_1, MF_SCHEME1, SCHEME_CODE1, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA);
    sendBODPriceToMFActor(LAST_NAV1, MF_TICKER1);
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID1, MF_TICKER1, ALLOCATION1,
        20, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);
    waitForViewToPopulate(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    String issuedAdviseId = responseEntity.getId();
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    double quantity = (ALLOCATION1/100) * SUBSCRIPTION_LUMPSUM / LAST_NAV1;
    quantity = round(quantity, 4);
    waitForViewToPopulate(1);
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(issuedAdviseId, SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    UserMFAdvise userMFAdvise = userMFAdviseRepository.findById(userAdviseCompositeKey).orElse(null);
    assertThat(userMFAdvise.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity()).isEqualTo(quantity);
  }

  @Test public void 
  return_200_when_investor_successfully_subscribes_to_the_fund() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);

    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL.getInvestingMode()))).isTrue();
  }

  @Test public void
  advise_token_time_changes_even_if_price_is_eod() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER_EOD, "APOLLOTYRE", 163);
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION,
        FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    long lastDayEOD = lastDayEOD(currentTimeInMillis());
    sendEODPriceWithPafToEquityActor(
        new PriceWithPaf(new InstrumentPrice(1000.0, lastDayEOD, TICKER_EOD),
            new Pafs(1., 0., 0., 1.)));
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER_EOD, ALLOCATION1,
        ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);

    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL.getInvestingMode())))
            .isTrue();
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER_EOD, timeFromMidnight());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToLong(order -> order.getOrderTimestamp()).sum())
            .isNotEqualTo(lastDayEOD);
  }

  @Test public void 
  subscription_add_lumpsum_at_higher_traded_prices() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER13, SYMBOL_13, TOKEN_13a);
    sendEODPriceToEquityActor(ENTRYPRICE_LOW, TICKER13, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    Failed failedResponseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER13, ALLOCATION_HIGH, ENTRYPRICE_LOW,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(ENTRYPRICE_LOW, valueOf(TOKEN_13a));
    
    subscriptionProcess();
    waitForProcessToComplete(1);
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(POST_HIGHTRADE_SUB_FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM);
    assertThat(userFund.getTotalInvestment()).isEqualTo(POST_HIGHTRADE_SUB_TOTALINVESTMENT_PRELUMPSUM);
    waitForViewToPopulate(1);

    sendLivePriceToEquityActor(ENTRYPRICE_LOW, valueOf(TOKEN_13a));

    waitForProcessToComplete(1);
    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME,
        ADD_LUMPSUM, VIRTUAL));
    
    waitForProcessToComplete(3);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");
    

    waitForViewToPopulate(1);
    
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(POST_HIGHTRADE_SUB_FUND_AVAILABLE_CASH_POST_MIRRORING_POSTLUMPSUM);
    assertThat(userFund1.getTotalInvestment()).isEqualTo(POST_HIGHTRADE_SUB_TOTALINVESTMENT_POSTLUMPSUM);
    
    response = authenticatedGet("/allUserFundPendingApprovals",
        "adviser=" + ADVISER_USERNAME + "&fund=" + FUND_NAME + "&investingMode=" + VIRTUAL);
    response.assertStatusCode(OK).assertMediaType("application/json");

    response = authenticatedGet("/allUserFundPendingApprovals",
        "adviser=" + ADVISER_USERNAME + "&fund=" + OTHER_FUND_NAME + "&investingMode=" + VIRTUAL);
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(FORBIDDEN).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage()).isEqualTo("You are not subscribed to this fund!");

    response = authenticatedGet("/userApprovalMode",
        "adviser=" + ADVISER_USERNAME + "&fund=" + FUND_NAME + "&investingMode=" + VIRTUAL);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");

    response = authenticatedGet("/userApprovalMode",
        "adviser=" + ADVISER_USERNAME + "&fund=" + OTHER_FUND_NAME + "&investingMode=" + VIRTUAL);
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(FORBIDDEN).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage()).isEqualTo("You are not subscribed to this fund!");

    response = postAuthUserCommand("/userApprovalMode",
        new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");

    response = postAuthUserCommand("/userApprovalMode",
        new ReceivedUserAcceptanceMode(OTHER_FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(FORBIDDEN).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage()).isEqualTo("You are not subscribed to this fund!");
  }
  
  @Test public void 
  return_failed_when_wrong_adviser_is_specified_when_subscribing() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER13, SYMBOL_13, TOKEN_13a);
    sendEODPriceToEquityActor(ENTRYPRICE_LOW, TICKER13, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    Failed failedResponseEntity;
    registerAdviserProcess();
    registerSecondAdviser();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER13, ALLOCATION_HIGH, ENTRYPRICE_LOW,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(ENTRYPRICE_LOW, valueOf(TOKEN_13a));
    
     response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER2.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
     
     failedResponseEntity = response.entity(unmarshaller(Failed.class));
     
     assertThat(failedResponseEntity.getMessage())
       .isEqualTo("Subscription to fund WeeklyPicks failed, fund not found");
  }

  @Test public void 
  replicate_issued_advices_in_investors_account_when_adding_lumpsum() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(ENTRYPRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE_2,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, valueOf(TOKEN_2));
    
    subscriptionProcess();
    waitForViewToPopulate(1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_2_WHEN_ADDING_LUMPSUM);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash()).isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_POSTLUMPSUM);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM);
    waitForViewToPopulate(1);

    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_3, TICKER1,currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_3, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2 , valueOf(TOKEN_2));

    waitForProcessToComplete(1);
    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME,
        ADD_LUMPSUM, VIRTUAL));
    
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");
    

    waitForViewToPopulate(1);
    
    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_LUMPSUM_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_LUMPSUM_1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_LUMPSUM_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_LUMPSUM_2);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash()).isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_POSTLUMPSUM);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_LUMPSUM_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_LUMPSUM_2);

    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_LUMPSUMADDITION);
  }

  @Test public void 
  replicate_issued_advices_in_investors_account_when_adding_lumpsum_post_a_partial_close() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(ENTRYPRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE_2,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, valueOf(TOKEN_2));
    
    subscriptionProcess();
    waitForViewToPopulate(1);

    toggleAutoApproval(Auto);

    waitForProcessToComplete(1);

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_2_WHEN_ADDING_LUMPSUM_POST_PARTIAL_CLOSE);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_AND_PARTIALCLOSE_POSTLUMPSUM);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM_1);
    waitForViewToPopulate(1);

    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_3, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_3, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2 , valueOf(TOKEN_2));

    waitForProcessToComplete(1);

    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME,
        ADD_LUMPSUM, VIRTUAL));
    
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");

       

    waitForViewToPopulate(1);
    
    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_PARTIALCLOSE_POST_LUMPSUM_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_PARTIALCLOSE_POST_LUMPSUM_1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_PARTIALCLOSE_POST_LUMPSUM_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_PARTIALCLOSE_POST_LUMPSUM_2);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_AND_PARTIALCLOSE_POSTLUMPSUM);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() != 0)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_1);
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
    .isEqualTo(EXPECTED_TOTAL_SELL_ORDER_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_PARTIALCLOSE_POST_LUMPSUM_2);

    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_PARTIALCLOSE_POST_LUMPSUMADDITION);
  }

  private void registerAdviserProcess() throws InterruptedException {
    TestRouteResult response = post("/registeradviserfromuserpool", ADVISER);
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Adviser Registered");
    Thread.sleep(5*1000);
  }
  
  private void registerSecondAdviser() throws InterruptedException {
    TestRouteResult response = post("/registeradviserfromuserpool", ADVISER2);
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Adviser Registered");
    Thread.sleep(5*1000);
  }

  private void toggleAutoApproval(UserAcceptanceMode changeTo) {
    TestRouteResult response;
    Done responseEntity;
    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, changeTo));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    if(changeTo.equals(Auto))
      assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);
    if(changeTo.equals(Manual))
      assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Manual for fund " + FUND_NAME);
  }

  @Test public void 
  replicate_issued_advices_in_investors_account_when_adding_lumpsum_post_a_freezed_advise() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    updateInstrumentsMapOfEquityActor(TICKER5, SYMBOL_5, INT_TOKEN_5);
    sendEODPriceToEquityActor(ENTRYPRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_4, TICKER3, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER5, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();

    response = authenticatedAdviserPost("/recoentryprice",
        new UpdateRecommendedEntryPrice(RECOMMENDED_ENTRY_PRICE, firstAdviseId, FUND_NAME));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION_LESS, ENTRYPRICE_2,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice anAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID4, TICKER5, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", anAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, valueOf(TOKEN_2));
    
    waitForProcessToComplete(1);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM_LESS + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    toggleAutoApproval(Auto);

    waitForProcessToComplete(1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER1)).isNull();

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_2_LESS);
    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_2_NOT_TRADED);

    assertThat(userEquityAdviseRepository.findByToken(TICKER5).getIssueAdviseAllocationValue())
    .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_3);
    assertThat(userEquityAdviseRepository.findByToken(TICKER5).getAverageEntryPrice().getPrice().getPrice())
    .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_3);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES_AND_FREEZED_ADVICE_POSTLUMPSUM);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM_POST_REJECTION_1);
    waitForViewToPopulate(1);

    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_3, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_3, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2 , valueOf(TOKEN_2));
    
    response = authenticatedAdviserPost("/recoentryprice",
        new UpdateRecommendedEntryPrice(RECOMMENDED_ENTRY_PRICE_1, firstAdviseId, FUND_NAME));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForViewToPopulate(1);

    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME,
        ADD_LUMPSUM, VIRTUAL));
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
            .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    
    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_REJECTION_POST_LUMPSUM_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_REJECTION_AND_FREEZED_ADVICE_POST_LUMPSUM_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_2);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() != 0)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_REJECTION_AND_FREZZED_ADVISE_POST_LUMPSUM_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_2);

    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUMADDITION);
    
    sendLivePriceToEquityActor(ENTRYPRICE_4 , TICKER3);
    
    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, 10., ENTRYPRICE_4,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", thirdAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    waitForViewToPopulate(1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER3).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_REJECTION_POST_LUMPSUM_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER3).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_REJECTION_AND_FREEZED_ADVISE_POST_LUMPSUM_3);
    
  }

  @Test public void 
  unsubcribe_user_post_subscription_with_all_rejections_when_subscribing() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(ENTRYPRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_4, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION_LESS, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION_LESS, ENTRYPRICE_2,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");


    waitForProcessToComplete(1);

    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, TICKER2);
    
    waitForProcessToComplete(1);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM_LESS + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    Optional<CurrentUserFundDb> potentialUserFund = userFundRepository.findById(userFundCompositeKey);
    assertThat(potentialUserFund.isPresent()).isFalse();
  }

  private TestRouteResult authenticatedAdviserPost(String url, Object command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).addHeader(RawHeader.create("username", ADVISER_USERNAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  @Test public void 
  allow_new_open_close_advise_only_post_user_approval_lest_auto_approved() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String secondAdviseId = responseEntity.getId();

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER2, SYMBOL_2, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2),
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1, 0, ENTRYTIME))
        .collect(Collectors.toList());


    toggleAutoApproval(Manual);
    
    waitForProcessToComplete(1);

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();

    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
    .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
    .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
    .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
    .runWith(Sink.seq(), materializer)
    .toCompletableFuture().get();

    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));

    assertThat(userFund.getCashComponent()).isEqualTo(CASH_COMPONENT_POST_ISSUE_OF_TWO_ADVISES_WHEN_AWAITING_A_RESPONSE);

    waitForViewToPopulate(1);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_SUBSCRIPTION_ISSUE_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_IN_MANUAL_MODE);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_MANUAL_MODE);

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    allUserResponseParameter = Stream.of(
    new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
        SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
        EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1, 0, (int) ((EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 * EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1) / 100.), EXITTIME))
    .collect(Collectors.toList());
    
    userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);

    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST_Close,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");

    waitForViewToPopulate(1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_PARTIAL_EXIT_ORDER_QUANTITY_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_PARTIAL_ORDER_TRADED_QUANTITY_1);
    UserEquityAdvise userEquityAdvise = new UserEquityAdvise();
    String userAdviseCompositeKey = "" + new UserAdviseCompositeKey(firstAdviseId, SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider.allEventsSourceForPersistenceId(userAdviseCompositeKey)
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
        .map(userAdviseEvent -> ((UserAdviseEvent) userAdviseEvent.event()))
        .runForeach(event -> userEquityAdvise.update(event), materializer);

    waitForProcessToComplete(1);

    assertThat(userEquityAdvise.getBasketOrderCompositeId()).isEqualTo(BASKETORDERID_FOR_TEST_Close + "__0");
  }

  @Test public void 
  validating_rejections_cases_for_pending_approvals() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    Failed failedResponseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    String firstAdviseId = responseEntity.getId();

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForViewToPopulate(1);

    String secondAdviseId = responseEntity.getId();

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER2, SYMBOL_2, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2),
        new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER2, SYMBOL_2, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage()).isEqualTo("Pending approvals are not unique and are not valid for this user. Your request to approve pending approvals cannot be processed.");

    List<UserResponseParameters> singleUserResponseParameter = Stream.of(
        new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER2, SYMBOL_2, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2))
        .collect(Collectors.toList());

    objectMapper = new ObjectMapper();
    userResponseParametersAsJsonString = objectMapper.writeValueAsString(singleUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage()).isEqualTo("Pending approvals received are not valid for this user. Your request to approve pending approvals cannot be processed.");

    String randomAdviseId = firstAdviseId+secondAdviseId;
    allUserResponseParameter = Stream.of(
    new UserResponseParameters(randomAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
        SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
        EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1, 0, (int) ((EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 * EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1) / 100.), EXITTIME))
    .collect(Collectors.toList());
    
    userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);

    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST_Close,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Pending approvals received are not valid for this user. Your request to approve pending approvals cannot be processed.");
  }

  @Test public void 
  merge_approvals_in_same_advise_issue_issue_type() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    Failed failedResponseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Manual));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Manual for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();

    enhanceAllocationOfTheAboveIssuedAdvice( firstAdviseId, ADVISER_USERNAME, FUND_NAME, 
        TICKER1, ALLOCATION2, CURRENT_MARKET_PRICE, EXITPRICE, EXITTIME);
    
    waitForProcessToComplete(1);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    
    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    
    assertThat(userFund.getPendingApprovalRequests().get(0).getAllocationValue()).isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_ADVISES_1);
    assertThat(userFund.getPendingApprovalRequests().get(0).getAbsoluteAllocation()).isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_FOR_MULTIPLE_ADVISES_1);

    waitForViewToPopulate(1);
    
    TestRouteResult pendingApprovalsResponse = authenticatedGet("/userAllFundPendingApprovals",
        "investingMode=" + VIRTUAL.getInvestingMode());
    ConsolidatedPendingRebalancingUserResponse blah = pendingApprovalsResponse
        .entity(unmarshaller(ConsolidatedPendingRebalancingUserResponse.class));
    pendingApprovalsResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(blah.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getNumberOfShares())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_PRE_APPROVAL);
    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0],
        FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_ISSUE);

    List<UserResponseParameters> randomUserResponseParameter = Stream.of(
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_MULTIPLE_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_ADVISES_1, 0, ENTRYTIME),
        new UserResponseParameters(firstAdviseId, OTHER_FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_MULTIPLE_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_ADVISES_1, 0, ENTRYTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String randomUserResponseParametersAsJsonString = objectMapper.writeValueAsString(randomUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(randomUserResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(failedResponseEntity.getMessage()).isEqualTo("Could not process approvals as multiple fund approvals received for user: "+SUBSCRIPTION_USER_NAME);

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_MULTIPLE_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_ADVISES_1, 0, ENTRYTIME))
        .collect(Collectors.toList());

    objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");
    
    waitForViewToPopulate(1);

    userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_ISSUE_TRADE_EXECUTION);
    
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_APPROVAL);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_APPROVAL);

  }

  @Test public void 
  merge_approvals_in_same_advise_close_close_type() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();
    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Manual));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Manual for fund " + FUND_NAME);
    
    waitForViewToPopulate(1);
    
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    
    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    waitForProcessToComplete(1);
    
    assertThat(userFund.getPendingApprovalRequests().get(0).getAbsoluteAllocation()).isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_FOR_SINGLE_CLOSE_ADVISE);
    assertThat(userFund.getPendingApprovalRequests().get(0).getNumberOfShares()).isEqualTo(EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_SINGLE_CLOSE_ADVISE);

    closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    List<UserFundEvent> allUserFundEvents1 = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents1.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    
    waitForProcessToComplete(1);
    
    assertThat(userFund.getPendingApprovalRequests().get(0).getAbsoluteAllocation()).isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_CLOSE_ADVISES);
    assertThat(userFund.getPendingApprovalRequests().get(0).getNumberOfShares()).isEqualTo(EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_CLOSE_ADVISES);

    waitForViewToPopulate(1);

    TestRouteResult pendingApprovalsResponse = authenticatedGet("/userAllFundPendingApprovals",
        "investingMode=" + VIRTUAL.getInvestingMode());
    ConsolidatedPendingRebalancingUserResponse blah = pendingApprovalsResponse
        .entity(unmarshaller(ConsolidatedPendingRebalancingUserResponse.class));
    pendingApprovalsResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(blah.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getNumberOfShares())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_CLOSE_ADVISES);
    assertThat(blah.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getAllocationValue())
    .isEqualTo(EXPECTED_CLOSE_ALLOCATION_VALUE_FOR_MULTIPLE_CLOSE_ADVISES);

    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MERGING_CLOSE_CLOSE);

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
    new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
        SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
        EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_CLOSE_ADVISES, 0, EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_CLOSE_ADVISES, EXITTIME))
    .collect(Collectors.toList());
    
    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);

    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");

    waitForViewToPopulate(1);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    
    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_SUBSCRIPTION_ISSUE_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_CLOSE_ADVISES);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_CLOSE_ADVISES);
  }

  @Test
  public void merge_approvals_in_same_advise_issue_close_type() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION,
        FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();
    waitForProcessToComplete(1);
    response = authenticatedGet("/subscription",
        "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + "&sipAmount=" + SUBSCRIPTION_SIPAMOUNT
            + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" + SUBSCRIPTION_SIP_DATE + "&investingMode="
            + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION,
        ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(
        "Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();

    List<UserFundEvent> allUserFundEvents = journalProvider
        .currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event())).runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();

    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    waitForProcessToComplete(1);

    assertThat(userFund.getPendingApprovalRequests().get(0).getAbsoluteAllocation())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_1);
    assertThat(userFund.getPendingApprovalRequests().get(0).getAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_1);

    closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION,
        ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(
        "Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    CurrentUserFundDb userFund1 = new CurrentUserFundDb();
    List<UserFundEvent> allUserFundEvents1 = journalProvider
        .currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event())).runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();

    allUserFundEvents1.stream().forEach(userFundEvent -> userFund1.update(userFundEvent));
    waitForProcessToComplete(1);
    assertThat(userFund1.getPendingApprovalRequests().get(0).getAbsoluteAllocation())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_2);
    assertThat(userFund1.getPendingApprovalRequests().get(0).getAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_2);

    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0],
        FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund2 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund2.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_CLOSE);

    List<UserResponseParameters> allUserResponseParameter = Stream
        .of(new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, SUBSCRIPTION_USER_NAME,
            MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve,
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_2,
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_2, 0, EXITTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);

    response = postAuthUserCommand("/userApprovalResponse",
        new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");

    waitForViewToPopulate(1);

    userFund2 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund2.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MERGING_ISSUE_CLOSE_POST_TRADE_EXECUTION);
    
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_MULTIPLE_APPROVAL_ISSUE_CLOSE_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_MULTIPLE_APPROVAL_ISSUE_CLOSE_1);

  }

  @Test public void 
  hypothetical_cash_check_post_many_issue_close_advises() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER30, SYMBOL_3, TOKEN_30);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_3, TICKER30, currentTimeInMillis());

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();
    
    IssueAdvice zeroAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER30, 100.0, ENTRYPRICE_3,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", zeroAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String zeroAdviseId = responseEntity.getId();
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());    
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");
    
    waitForProcessToComplete(1);

    CloseAdvise zeroCloseAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, zeroAdviseId, 50.0, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);

    response = post("/closeadvise", zeroCloseAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION_25, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();
    
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER2, ALLOCATION_25, ENTRYPRICE_2,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String secondAdviseId = responseEntity.getId();

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, ALLOCATION_25, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);

    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    
    CloseAdvise secondCloseAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, secondAdviseId, ALLOCATION_25, ACTUAL_EXITDATE, ENTRYPRICE_2);

    response = post("/closeadvise", secondCloseAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    
    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    
    waitForProcessToComplete(1);
    
    assertThat(userFund.getHypotheticalFundCashWhenApprovalsArePending()).isEqualTo(FUND_HYPOTHETICAL_CASH_POST_MULTIPLE_ISSUE_CLOSE);

    assertThat(userFund.getPendingApprovalRequests()).isNotEqualTo(new CopyOnWriteArrayList<UserResponseParameters>());
    
    waitForViewToPopulate(1);
    
    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MULTIPLE_ISSUE_CLOSE);
    
    List<UserResponseParameters> allUserResponseParameter = Stream
        .of(new UserResponseParameters(zeroAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close,
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER30, SYMBOL_3, VIRTUAL,
            Approve, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_MULTIPLE_ISSUE_CLOSE_ADVISES, 0,
            EXPECTED_ISSUED_ADVICE_NUMBER_OF_SHARES_FOR_MULTIPLE_ISSUE_CLOSE_ADVISES, EXITTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);

    response = postAuthUserCommand("/userApprovalResponse",
        new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");

    waitForViewToPopulate(1);
    
    CurrentUserFundDb userFund2 = new CurrentUserFundDb();
    
    List<UserFundEvent> allUserFundEvents1 = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents1.stream().forEach(userFundEvent -> userFund2.update(userFundEvent));
    waitForProcessToComplete(1);
    assertThat(userFund2.getHypotheticalFundCashWhenApprovalsArePending()).isEqualTo(0.);

    assertThat(userFund2.getPendingApprovalRequests()).isEqualTo(new CopyOnWriteArrayList<UserResponseParameters>());
    
    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKeyRepo1 = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund3 = userFundRepository.findById(userFundCompositeKeyRepo1).orElse(null);
    assertThat(userFund3.getCashComponent()).isEqualTo(USERFUND_CASH_POST_MULTIPLE_ISSUE_CLOSE);
    
    CloseAdvise zeroFullCloseAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, zeroAdviseId, 50.0, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);

    response = post("/closeadvise", zeroFullCloseAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);
    
    CurrentUserFundDb userFundBefore = new CurrentUserFundDb();
    List<UserFundEvent> allOfUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allOfUserFundEvents.stream().forEach(userFundEvent -> userFundBefore.update(userFundEvent));
    
    waitForProcessToComplete(1);
    assertThat(userFundBefore.getPendingApprovalRequests().size()).isEqualTo(1);
    
    actorSelections.userRootActor().tell(new SpecialCircumstancesSell(TICKER30, TICKER30, ACTUAL_EXITPRICE, 144, SUBSCRIPTION_USER_NAME, 
        ADVISER_USERNAME, FUND_NAME, zeroAdviseId, SUBSCRIPTION_USER_NAME, BrokerName.MOCK , InvestingMode.VIRTUAL), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    CurrentUserFundDb userFundAfter = new CurrentUserFundDb();
    List<UserFundEvent> allOfUserFundEventsAfter = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allOfUserFundEventsAfter.stream().forEach(userFundEvent -> userFundAfter.update(userFundEvent));
    
    waitForProcessToComplete(1);
    assertThat(userFundAfter.getPendingApprovalRequests().size()).isEqualTo(0);
  }

  @Test public void 
  remove_approvals_in_same_advise_issue_close_type_when_allocation_is_same() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();
    waitForProcessToComplete(1);

    subscriptionProcess();
    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();
    
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, FULL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    
    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    waitForProcessToComplete(1);
    assertThat(userFund.getPendingApprovalRequests()).isEqualTo(new CopyOnWriteArrayList<UserResponseParameters>());

    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_AFTER_REMOVAL_POST_MERGING_ISSUE_CLOSE);
    assertThat(userFund1.getActiveAdvises().size()).isEqualTo(0);
  }

  @Test public void 
  merge_approvals_in_same_advise_close_issue_type() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();
    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Manual));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Manual for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);


    enhanceAllocationOfTheAboveIssuedAdvice(firstAdviseId, ADVISER_USERNAME, FUND_NAME, 
        TICKER1, ALLOCATION2, CURRENT_MARKET_PRICE, EXITPRICE, EXITTIME);
    
    waitForProcessToComplete(1);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    
    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    assertThat(userFund.getPendingApprovalRequests().get(0).getAllocationValue()).isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1);
    assertThat(userFund.getPendingApprovalRequests().get(0).getAbsoluteAllocation()).isEqualTo(EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1);

    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MERGING_CLOSE_ISSUE);

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1, 0, ENTRYTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");
    
    waitForViewToPopulate(1);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_APPROVAL_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_APPROVAL_1);

  }

  @Test public void 
  merge_approvals_in_same_advise_close_issue_type_is_less() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();
    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Manual));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Manual for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);


    enhanceAllocationOfTheAboveIssuedAdvice(firstAdviseId, ADVISER_USERNAME, FUND_NAME, 
        TICKER1, ALLOCATION_LESS, CURRENT_MARKET_PRICE, EXITPRICE, EXITTIME);
    
    waitForViewToPopulate(1);
    
    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    
    assertThat(userFund.getPendingApprovalRequests().get(0).getAdviseType()).isEqualTo(AdviseType.Close);
    assertThat(userFund.getPendingApprovalRequests().get(0).getNumberOfShares()).isEqualTo(EXPECTED_CLOSE_ADVICE_PENDING_CLOSE_QUANTITY_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1);
    assertThat(userFund.getPendingApprovalRequests().get(0).getAbsoluteAllocation()).isEqualTo(EXPECTED_CLOSED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1);

    waitForViewToPopulate(1);
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_REMOVAL_MERGING_CLOSE_ISSUE);
    assertThat(userFund1.getHypotheticalFundCashWhenApprovalsArePending()).isEqualTo(FUND_AVAILABLE_HYPO_CASH_POST_REMOVAL_MERGING_CLOSE_ISSUE);

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_CLOSED_ADVICE_ABSOLUTE_ALLOCATION_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1, 0, EXPECTED_CLOSE_ADVICE_PENDING_CLOSE_QUANTITY_AFTER_PARTIAL_CLOSE_ADVISE_ADD_ALLOCATION_1, ENTRYTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");
    
    waitForViewToPopulate(1);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> buy_trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_BUY_ORDER_QUANTITY_POST_APPROVAL_2);
    assertThat(buy_trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_BUY_ORDER_QUANTITY_POST_APPROVAL_2);

    Stream<Trade> sell_trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_SELL_ORDER_QUANTITY_POST_APPROVAL_2);
    assertThat(sell_trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_SELL_ORDER_QUANTITY_POST_APPROVAL_2);
  }

  @Test public void 
  remove_approvals_in_same_advise_close_issue_with_same_allocations() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, CURRENT_MARKET_PRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();
    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Manual));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Manual for fund " + FUND_NAME);
    
    waitForProcessToComplete(1);

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);


    enhanceAllocationOfTheAboveIssuedAdvice(firstAdviseId, ADVISER_USERNAME, FUND_NAME, 
        TICKER1, PARTIAL_CLOSING_ALLOCATION, CURRENT_MARKET_PRICE, EXITPRICE, EXITTIME);
    
    waitForProcessToComplete(1);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    
    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));
    assertThat(userFund.getPendingApprovalRequests()).isEqualTo(new CopyOnWriteArrayList<UserResponseParameters>());

    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKeyRepo = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKeyRepo).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_REMOVAL_MERGING_CLOSE_ISSUE);
  }

  @Test public void 
  not_honor_close_advise_where_the_resulting_number_of_shares_are_less_than_one_in_auto_approved_mode() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();
    waitForProcessToComplete(1);
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM_IN_ORDER_TO_GET_A_SINGLE_QUANTITY + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
    VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForProcessToComplete(1);
    
    autoApprove(FUND_NAME);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);
    
    String firstAdviseId = responseEntity.getId();
    
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION_TO_GET_QUANTITY_LESS_THAN_ONE, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);
    
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(firstAdviseId, SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);

    List<UserAdviseEvent> allUserAdviseEvents = journalProvider.currentEventsSourceForPersistenceId(userAdviseCompositeKey.persistenceId())
    .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
    .map(userAdviseEvent -> ((UserAdviseEvent) userAdviseEvent.event()))
    .runWith(Sink.seq(), materializer)
    .toCompletableFuture().get();

    boolean hasCloseAdviseBeenProcessed = allUserAdviseEvents.stream()
    .anyMatch(userAdviseEvent -> userAdviseEvent instanceof UserAdviseTransactionReceived);
    
    assertThat(hasCloseAdviseBeenProcessed).isFalse();
  }
  
  @Test public void 
  acknowledge_close_advise_where_the_resulting_number_of_shares_are_less_than_one_in_manual_mode_but_does_not_process_it_any_further() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);
    
    String firstAdviseId = responseEntity.getId();
    
    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_FOR_QUANTITY_LESS_THAN_ONE, 0, ENTRYTIME))
        .collect(Collectors.toList());
    
    waitForProcessToComplete(1);

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!"); 
    
    waitForProcessToComplete(1);
    
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION_TO_GET_QUANTITY_LESS_THAN_ONE, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);
    
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(firstAdviseId, SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);

    List<UserAdviseEvent> allUserAdviseEvents = journalProvider.currentEventsSourceForPersistenceId(userAdviseCompositeKey.persistenceId())
    .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
    .map(userAdviseEvent -> ((UserAdviseEvent) userAdviseEvent.event()))
    .runWith(Sink.seq(), materializer)
    .toCompletableFuture().get();

    boolean hasCloseAdviseBeenAcknowledged = allUserAdviseEvents.stream()
    .anyMatch(userAdviseEvent -> userAdviseEvent instanceof UserAdviseTransactionReceived);
    
    assertThat(hasCloseAdviseBeenAcknowledged).isTrue();
  }
  
  @Test public void 
  exit_fund_on_user_approval_response() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    toggleAutoApproval(Manual);

    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String issuedAdviseId = responseEntity.getId();

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(issuedAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, ExitFund, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1, 0, ENTRYTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Approvals in progress. Scoot over to your investments to see the status!");

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    Optional<CurrentUserFundDb> potentialUserFund = userFundRepository.findById(userFundCompositeKey);
    assertThat(potentialUserFund.isPresent()).isFalse();
  }

  @Test public void 
  archive_fund_on_user_approval_response() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    toggleAutoApproval(Manual);

    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String issuedAdviseId = responseEntity.getId();

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(issuedAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Archive, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1, 0, ENTRYTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Archiving fund WeeklyPicks. You will not be receiving any updates pertaining to this fund now!");

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);

    List<String> userFundStateIdentifiers = new ArrayList<String>();

    journalProvider.allEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filter(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(stateChangeEvent -> ((StateChangeEvent) stateChangeEvent.event()).stateIdentifier())
        .runForeach(stateIdentifier -> userFundStateIdentifiers.add(stateIdentifier), materializer);

    waitForProcessToComplete(1);

    assertThat(userFundStateIdentifiers.contains(ARCHIVED.getStateIdentifier())).isTrue();

    CurrentUserFundDb userFund = new CurrentUserFundDb();
    journalProvider.allEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runForeach(userResponse -> userFund.update(userResponse), materializer);

    waitForProcessToComplete(1);

    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_LUMPSUM);
  }

  @Test public void 
  exit_fund_post_archival_on_user_approval_response() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);

    toggleAutoApproval(Manual);

    waitForProcessToComplete(1);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String issuedAdviseId = responseEntity.getId();

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(issuedAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Archive, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1, 0, ENTRYTIME))
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Archiving fund WeeklyPicks. You will not be receiving any updates pertaining to this fund now!");

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);

    List<String> userFundStateIdentifiers = new ArrayList<String>();

    journalProvider.allEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filter(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .map(stateChangeEvent -> ((StateChangeEvent) stateChangeEvent.event()).stateIdentifier())
        .runForeach(stateIdentifier -> userFundStateIdentifiers.add(stateIdentifier), materializer);

    waitForProcessToComplete(1);

    assertThat(userFundStateIdentifiers.contains(ARCHIVED.getStateIdentifier())).isTrue();

    CurrentUserFundDb userFund = new CurrentUserFundDb();
    journalProvider.allEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
        .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
        .runForeach(userResponse -> userFund.update(userResponse), materializer);

    waitForProcessToComplete(1);

    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_LUMPSUM);
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("UnSubscribe in process. You will receive a notification once its done.");
  }

  @Test public void 
  replicate_issued_advices_in_investors_account() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);

    subscriptionProcess();
    waitForViewToPopulate(1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE2);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash()).isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_IN_REPLICATION);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_IN_REPLICATION);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING);
    
    IssueAdvice thirdAdvise = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", thirdAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);
    
    UserFundCompositeKey userFundCompositeKey1 = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund1 = new CurrentUserFundDb();
    journalProvider
    .currentEventsSourceForPersistenceId(userFundCompositeKey1.persistenceId())
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
    .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
    .runForeach(userFundEvent -> userFund1.update(userFundEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    assertThat(userFund1.getCurrentFundValue()).isEqualTo(PORTFOLIO_VALUE_POST_SUBSCRIPTION);
    assertThat(userFund1.getActiveAdvises().stream().anyMatch(advise -> advise.getBrokerName().getBrokerName().equals(BROKER_NAME_MOCK)));
  }

  @Test public void 
  check_for_quantity_adjustment_post_bonus() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    String secondAdviseId = responseEntity.getId();

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);

    subscriptionProcess();
    long currentdate = todayBOD();
    universeRootActor.tell(new BonusEvent(TICKER2, currentdate, 1, 1), ActorRef.noSender());
    waitForProcessToComplete(1);
    universeRootActor.tell(new CheckForCorporateEvent(), ActorRef.noSender());
    waitForProcessToComplete(1);

    UserAdvise userAdvise = new UserEquityAdvise();
    String persistenceId = new UserAdviseCompositeKey(secondAdviseId, SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();

    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> userAdvise.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();

    waitForProcessToComplete(3);

    assertThat(userAdvise.getCumulativePaf())
    .isEqualTo(CUMULATIVE_PAF_POST_CORPORATE_EVENT);

    assertThat(round(userAdvise.getAverageEntryPrice().getPrice().getPrice()))
    .isEqualTo(EXPECTED_AVERAGE_ENTRY_POST_BONUS);

  }
  
  @Test public void 
  check_for_dividend_update_on_investor_domains() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);

    subscriptionProcess();
    long currentdate = todayBOD();
    universeRootActor.tell(new DividendEvent(TICKER2, currentdate, 3.5), ActorRef.noSender());
    waitForProcessToComplete(1);
    universeRootActor.tell(new CheckForCorporateEvent(), ActorRef.noSender());
    waitForProcessToComplete(1);
    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getFundDividendsReceived())
        .isEqualTo(EXPECTED_CUMULATIVE_DIVIDEND);
    
    UserAdvise userAdvise = userEquityAdviseRepository.findByToken(TICKER2);
    assertThat(userAdvise.getCumulativeDividendsReceived())
    .isEqualTo(EXPECTED_CUMULATIVE_DIVIDEND);

  }

  
  
  @Test public void 
  check_for_quantity_adjustment_post_corporate_events() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER13, SYMBOL_13, TOKEN_13);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER13, currentTimeInMillis());

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);
    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER13, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    String secondAdviseId = secondAdviseResponseEntity.getId();
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE,  valueOf(TOKEN_13));

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_3));

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, secondAdviseResponseEntity.getId(),
        PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    long currentdate = todayBOD();

    waitForProcessToComplete(1);
    long corporateEventTime1 = currentTimeInMillis();
    actorSelections.userRootActor().tell(new UpdateSplitTypeEvent(TICKER13, 2, CorporateEventType.BONUS, currentdate), ActorRef.noSender());

    waitForProcessToComplete(1);

    long time = currentTimeInMillis();

    closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, secondAdviseResponseEntity.getId(),
        PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");


    long time2 = currentTimeInMillis();
    waitForProcessToComplete(1);
    actorSelections.userRootActor().tell(new UpdateSplitTypeEvent(TICKER13, 2, CorporateEventType.BONUS, currentdate), ActorRef.noSender());
    waitForProcessToComplete(1);
    long corporateEventTime2 = currentTimeInMillis();
    actorSelections.userRootActor().tell(new UpdateSplitTypeEvent(TICKER13, 2, CorporateEventType.SPLIT, currentdate), ActorRef.noSender());
    waitForProcessToComplete(1);
    actorSelections.userRootActor().tell(new UpdateSplitTypeEvent(TICKER13, 2, CorporateEventType.SPLIT, currentdate), ActorRef.noSender());
    waitForProcessToComplete(1);
    long corporateEventTime3 = currentTimeInMillis();
    long nextday = currentdate + 86400000;
    actorSelections.userRootActor().tell(new UpdateSplitTypeEvent(TICKER13, 0.5, CorporateEventType.SPLIT, nextday), ActorRef.noSender());
    waitForProcessToComplete(1);

    UserEquityAdvise userAdvise = new UserEquityAdvise();
    String persistenceId = new UserAdviseCompositeKey(secondAdviseId, SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider.currentEventsSourceForPersistenceId(persistenceId)
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> {
          if (userAdviseEvent instanceof UpdateCorporateEvent) {
            long currentTime;
            UpdateCorporateEvent updateCorporateEvent = (UpdateCorporateEvent) userAdviseEvent;
            if (updateCorporateEvent.getEventType() == BONUS) {
              currentTime = corporateEventTime1;
            } else if (updateCorporateEvent.getEventType() == SPLIT && updateCorporateEvent.getAdjustPaf() == 2.0) {
              currentTime = corporateEventTime2;
            } else {
              currentTime = corporateEventTime3;
            }
            UpdateCorporateEvent updatedCorporateEvent = new UpdateCorporateEvent(updateCorporateEvent.getUsername(),
                updateCorporateEvent.getAdviserUsername(), updateCorporateEvent.getFundName(),
                updateCorporateEvent.getInvestingMode(), updateCorporateEvent.getAdviseId(),
                updateCorporateEvent.getUserAdviseState(), updateCorporateEvent.getAdjustPaf(),
                updateCorporateEvent.getEventType(), updateCorporateEvent.getLocalOrderId(), currentTime,
                updateCorporateEvent.getToken(), IssueType.Equity, Purpose.CORPORATEACTION ,0.0);

            userAdvise.update(updatedCorporateEvent);
          } else {
            userAdvise.update(userAdviseEvent);
          }
    } , materializer)
    .toCompletableFuture().get();

    waitForProcessToComplete(1);
    InstrumentPrice instrumentPrice1 = new InstrumentPrice(165.0, time, TICKER13);
    InstrumentPrice instrumentPrice2 = new InstrumentPrice(171, time2, TICKER13);

    assertThat(userAdvise.integerQuantityYetToBeExited())
    .isEqualTo(TOTAL_QUANTITY_REMAINING_POST_CORPORATE_EVENTS);
    assertThat(userAdvise.getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_POST_CORPORATE_EVENTS);
    assertThat(round(userAdvise.getAverageExitPrice().getPrice().getPrice()))
    .isEqualTo(EXPECTED_AVERAGE_EXIT_POST_SPLIT );
    assertThat(userAdvise.performanceForHistorical(new PriceWithPaf(instrumentPrice1, new Pafs()), time).getRealizedPnL())
            .isEqualTo(REALIZED_PNL_POST_CORPORATE_EVENT_1);
    assertThat(userAdvise.performanceForHistorical(new PriceWithPaf(instrumentPrice1, new Pafs()), time).getUnrealizedPnL())
            .isEqualTo(UNREALIZED_PNL_POST_CORPORATE_EVENT_1);
    assertThat(userAdvise.performanceForHistorical(new PriceWithPaf(instrumentPrice2, new Pafs()), time2).getRealizedPnL())
            .isEqualTo(REALIZED_PNL_POST_CORPORATE_EVENT_2);
    assertThat(userAdvise.performanceForHistorical(new PriceWithPaf(instrumentPrice2, new Pafs()), time2).getUnrealizedPnL())
            .isEqualTo(UNREALIZED_PNL_POST_CORPORATE_EVENT_2);
  }

  @Test public void 
  notify_on_resubscription_when_current_subscription_is_in_process() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER12, SYMBOL_12, INT_TOKEN_12);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER12, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER12, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER12);

    subscriptionProcess();
    Thread.sleep(2 * 1000);

    subscriptionProcess();
  }
  
  @Test public void 
  throw_error_on_state_change_commands_when_user_fund_trade_confirmations_pending() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER12, SYMBOL_12, INT_TOKEN_12);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER12, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    subscriptionProcess();
   
    Thread.sleep(2 * 1000);
  
    autoApprove(FUND_NAME);
    
    Thread.sleep(2 * 1000);
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER12, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER12);

    Thread.sleep(2 * 1000);
    
    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME,
        ADD_LUMPSUM, VIRTUAL));

    Failed responseFailedEntity = response.entity(unmarshaller(Failed.class));
    assertThat(responseFailedEntity.getMessage()).isEqualTo(
        "Adding lumpsum to fund WeeklyPicks failed. Kindly approve the pending approvals in the fund, then proceed with adding LumpSum. If approved, please wait till the trade confirmations are received from the exchange");
    
    Thread.sleep(2 * 1000);
    
    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 20000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    responseFailedEntity = response.entity(unmarshaller(Failed.class));
    
    assertThat(responseFailedEntity.getMessage()).isEqualTo(
        "Withdrawal fromWeeklyPicks failed. Kindly approve the pending approvals in the fund, then proceed with Withdrawal.If approved, please wait till please wait till previous trade confirmations are received from the exchange");
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    
    responseFailedEntity = response.entity(unmarshaller(Failed.class));
    assertThat(responseFailedEntity.getMessage()).isEqualTo(
        "Cannot unsubscribe as previously approved trade confirmations are expected from the exchange. Please wait a while");
    
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseFailedEntity = response.entity(unmarshaller(Failed.class));
    assertThat(responseFailedEntity.getMessage()).isEqualTo(
        "Cannot unsubscribe as previously approved trade confirmations are expected from the exchange. Please wait a while");

  }
  
  @Test public void 
  check_for_fund_cash_adjustment_after_unsubscribing_when_advise_approvals_are_pending() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3);

    subscriptionProcess();
    waitForProcessToComplete(1);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);
    
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("UnSubscribe in process. You will receive a notification once its done.");
    
    waitForProcessToComplete(1);
    
    UserFund fundPostUnsubscription = new UserFund();
    journalProvider
    .currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
    .filter(eventEnvelope -> (eventEnvelope.event() instanceof UserFundEvent && !(eventEnvelope.event() instanceof FundSuccessfullyUnsubscribed)))
    .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
    .runForeach(fundEvent ->  fundPostUnsubscription.update(fundEvent), materializer)
    .toCompletableFuture().get();
    
    assertThat(fundPostUnsubscription.getCashComponentUpdatedWithCashBlockedForPendingAdvises())
        .isEqualTo(EXPECTED_USER_FUNDCASH_POST_UNSUBSCRIPTION_WITH_PENDING_ADVICES);
  }
  
  @Test public void 
  honor_new_advises_post_fund_mirroring_in_investors_account() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3);

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);

    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    waitForViewToPopulate(1);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES);

    assertThat(userEquityAdviseRepository.findByToken(TICKER3).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_3);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER3).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_3);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER3, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_3);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_3);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING_ADVISE_ISSUE);
  }
  
  @Test public void 
  send_an_email_when_new_advises_issued_in_investors_account() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3);

    subscriptionProcess();
    waitForViewToPopulate(1);

    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    waitForViewToPopulate(1);
    
    Mockito.verify(sesClient, times(2)).sendEmail(Mockito.any(SendEmailRequest.class));
  }

  @Test public void 
  honor_changes_in_the_current_advise_in_investors_account() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER_FOR_REISSUE, SYMBOL_FOR_REISSUE, TOKEN_FOR_REISSUE);
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    registerUserAndActivate();
    
    subscriptionProcess();
    waitForProcessToComplete(1);
    
    autoApprove(FUND_NAME);
    waitForViewToPopulate(1);
    sendEODPriceToEquityActor(ENTRYPRICE, TICKER_FOR_REISSUE, currentTimeInMillis());
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER_FOR_REISSUE, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    String firstAdviseId = responseEntity.getId();
    waitForProcessToComplete(1);
    
    postCorporateEventOfBonusType();
    
    waitForViewToPopulate(1);
    
    updateRealtimePriceWithCurrentPaf();
    
    UserEquityAdvise findByToken = userEquityAdviseRepository.findByToken(TICKER_FOR_REISSUE);
    assertThat(findByToken.getIssueAdviseAllocationValue())
    .isEqualTo(EXPECTED_INITIAL_ISSUED_ADVICE_ON_A_GIVEN_TOKEN_ALLOCATION_VALUE);
    
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER_FOR_REISSUE).getAverageEntryPrice().getPrice().getPrice()))
    .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_FOR_INITIAL_ISSUE);

    universeRootActor.tell(new CheckForCorporateEvent(), ActorRef.noSender());
    
    waitForProcessToComplete(1);

    response = enhanceAllocationOfTheAboveIssuedAdvice( responseEntity.getId(), ADVISER_USERNAME, FUND_NAME, 
        TICKER_FOR_REISSUE, ALLOCATION2, ENTRYPRICE_2, EXITPRICE, EXITTIME);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForViewToPopulate(1);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES);
    Advise advise = adviseRepository.findById(firstAdviseId).orElse(null);
    assertThat(advise.getRemainingAllocation()).isEqualTo(EXPECTED_ADVISE_REMAINING_ALLOCATION_ADDITION_TO_ISSUED_ADVICE);
    assertThat(userEquityAdviseRepository.findByToken(TICKER_FOR_REISSUE).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_NEXT_ISSUED_ADVICE_ON_A_GIVEN_TOKEN_ALLOCATION_VALUE);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER_FOR_REISSUE).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_FOR_RE_ISSUE_POST_BONUS_EVENT);

    List<Order> orders = StreamSupport.stream(ordersRepository.findAll().spliterator(), false)
                                      .collect(Collectors.toList());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_RE_ISSUE);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_RE_ISSUE);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(USER_FUND_AVAILABLE_CASH_POST_TWO_ISSUED_ADVICES_ON_A_GIVEN_TOKEN);
  }

  @Test public void 
  honor_new_advises_post_fund_mirroring_in_investors_account_with_ctcl_faults() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER8, SYMBOL_8, INT_TOKEN_8);
    updateInstrumentsMapOfEquityActor(TICKER9, SYMBOL_9, INT_TOKEN_9);
    updateInstrumentsMapOfEquityActor(TICKER10, SYMBOL_10, INT_TOKEN_10);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER8, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER9, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER10, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER8, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER9, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER8);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER9);

    subscriptionProcess();
    waitForViewToPopulate(1);
    
    autoApprove(FUND_NAME);

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER10);

    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER10, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    waitForProcessToComplete(1);

    waitForViewToPopulate(1);
    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES);
    assertThat(userEquityAdviseRepository.findByToken(TICKER10).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_3);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER10).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_3);
    waitForViewToPopulate(1);
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER10, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_3);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_3);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, INVESTING_MODE);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING_ADVISE_ISSUE);
  }

  @Test public void 
  honor_partial_close_advises_post_fund_mirroring_in_investors_account() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3);

    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForViewToPopulate(1);
    
    sendEODPriceToEquityActor(ACTUAL_EXITPRICE, TICKER1, currentTimeInMillis());
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseResponseEntity.getId(),
        PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES_AND_A_PARTIAL_EXIT);

    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getCloseAdviseAbsoluteAllocation())
        .isEqualTo(EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageExitPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTED_AVERAGE_PARTIAL_EXIT_PRICE_1);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_PARTIAL_EXIT_ORDER_QUANTITY_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_PARTIAL_ORDER_TRADED_QUANTITY_1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_PARTIAL_CLOSE_ADVISE);
  }
  
  @Test public void 
  check_special_buy_changes_in_user_fund() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER6, SYMBOL_6, INT_TOKEN_6);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER6, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER6, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(INT_TOKEN_6));

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    
    String userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    
    CurrentUserFundDb fundBeforeCorpActions = new CurrentUserFundDb();
    journalProvider
    .currentEventsSourceForPersistenceId(userFundCompositeKey)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
    .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
    .runForeach(fundEvent ->  fundBeforeCorpActions.update(fundEvent), materializer)
    .toCompletableFuture().get();
    
    assertThat(fundBeforeCorpActions.getCashComponent())
    .isEqualTo(EXPECTED_ADVISER_FUNDCASH_PRE_SPECIAL_BUY_FOR_ADVISE2);
    actorSelections.userRootActor().tell(new SpecialCircumstancesBuy(TICKER6, TICKER6, 1040, 5, SUBSCRIPTION_USER_NAME, 
        ADVISER_USERNAME, FUND_NAME, ADVISEID2, SUBSCRIPTION_USER_NAME, BrokerName.MOCK , InvestingMode.VIRTUAL), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    journalProvider
    .currentEventsSourceForPersistenceId(userFundCompositeKey)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
    .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
    .runForeach(fundEvent ->  fundBeforeCorpActions.update(fundEvent), materializer)
    .toCompletableFuture().get();
    
    assertThat(fundBeforeCorpActions.getCashComponent())
    .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_SPECIAL_BUY_FOR_ADVISE2);
    
    actorSelections.userRootActor().tell(new SpecialCircumstancesBuy(TICKER1, TICKER1, 1040, 9, SUBSCRIPTION_USER_NAME, 
        ADVISER_USERNAME, FUND_NAME, ADVISEID1, SUBSCRIPTION_USER_NAME, BrokerName.MOCK , InvestingMode.VIRTUAL), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    journalProvider
    .currentEventsSourceForPersistenceId(userFundCompositeKey)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
    .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
    .runForeach(fundEvent ->  fundBeforeCorpActions.update(fundEvent), materializer)
    .toCompletableFuture().get();
    
    assertThat(fundBeforeCorpActions.getCashComponent())
    .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_SPECIAL_BUY_FOR_ADVISE1);
    
  }
  
  @Test public void
  check_for_advise_closure_after_demerger_event() throws Exception{
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);
    
    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    String secondAdviseId = secondAdviseResponseEntity.getId();
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_2));
    
    subscriptionProcess();
    waitForViewToPopulate(1);
    
    waitForProcessToComplete(1);
    autoApprove(FUND_NAME);
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSell(TICKER2, EXPECTED_SELL_PRICE_FOR_DEMERGER_EVENT), ActorRef.noSender());

    waitForProcessToComplete(1);
    UserAdvise userAdvise = new UserEquityAdvise();
    String persistenceId = new UserAdviseCompositeKey(secondAdviseId, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  {
      if (!(userAdviseEvent instanceof CurrentUserAdviseFlushedDueToExit))
        userAdvise.update(userAdviseEvent);
        }, materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    
    assertThat(userAdvise.integerQuantityYetToBeExited())
    .isEqualTo(TOTAL_QUANTITY_REMAINING_POST_DEMERGER_SELL);
    Set<Order> closeOrders = userAdvise.getCloseAdviseOrders();
    double averageExitPrice =0; 
    for(Order order : closeOrders){
      averageExitPrice += order.calculateEquityAverageTradedPrice();
    }
    assertThat(averageExitPrice)
    .isEqualTo(EXPECTED_SELL_PRICE_FOR_DEMERGER_EVENT);
  }

  @Test public void
  check_for_changes_post_merger_demerger_on_adviser_side() throws Exception{
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);
    
    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    Fund fundBeforeCorpActions = new Fund();
    journalProvider
    .currentEventsSourceForPersistenceId(FUND_NAME)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof FundEvent)
    .map(eventEnvelope -> (FundEvent) eventEnvelope.event())
    .runForeach(fundEvent ->  fundBeforeCorpActions.update(fundEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSell(TICKER1, 1500.0), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    Fund fundAfterCorpActionSell = new Fund();
    journalProvider
    .currentEventsSourceForPersistenceId(FUND_NAME)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof FundEvent)
    .map(eventEnvelope -> (FundEvent) eventEnvelope.event())
    .runForeach(fundEvent ->  fundAfterCorpActionSell.update(fundEvent), materializer)
    .toCompletableFuture().get();
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventPrimaryBuy(TICKER1, 1500.0, 500.0,1, SYMBOL_1), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSecondaryBuy(TICKER1, TICKER3, 1,1, SYMBOL_3), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    Fund fundAfterCorpActions = new Fund();
    journalProvider
    .currentEventsSourceForPersistenceId(FUND_NAME)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof FundEvent)
    .map(eventEnvelope -> (FundEvent) eventEnvelope.event())
    .runForeach(fundEvent ->  fundAfterCorpActions.update(fundEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    String adviseIdOfPrimaryBuy = fundAfterCorpActions.getTickersWithComposition().entrySet().stream().filter(map -> map.getValue().getTicker().equals(TICKER1)).map(map -> map.getKey()).collect(Collectors.joining());
    String adviseIdOfSecondaryBuy = fundAfterCorpActions.getTickersWithComposition().entrySet().stream().filter(map -> map.getValue().getTicker().equals(TICKER3)).map(map -> map.getKey()).collect(Collectors.joining());
 
    Advise advisePrimary = fundAfterCorpActions.adviseForId(adviseIdOfPrimaryBuy);
    Advise adviseSecondary = fundAfterCorpActions.adviseForId(adviseIdOfSecondaryBuy);
    assertThat(fundBeforeCorpActions.getCash())
    .isEqualTo(fundAfterCorpActions.getCash());
    assertThat(adviseSecondary.getAllocation())
    .isEqualTo(13.33);
    assertThat(advisePrimary.getAllocation())
    .isEqualTo(6.67);
  }
  
  @Test public void
  check_for_merger_demerger_case_A_to_A_plus_B() throws Exception{
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);
    
    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    
    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    waitForProcessToComplete(1);

    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSell(TICKER2, EXPECTED_SELL_PRICE_FOR_DEMERGER_EVENT), ActorRef.noSender());

    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventPrimaryBuy(TICKER2, 1500.0, 500.0,1, SYMBOL_2), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSecondaryBuy(TICKER2, TICKER3, 1,1, SYMBOL_3), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    String userFundPersistenceId = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();

    journalProvider
      .currentEventsSourceForPersistenceId(userFundPersistenceId)
      .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
      .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
      .runForeach(userFundEvent ->  userFund.update(userFundEvent), materializer)
      .toCompletableFuture().get();
    waitForProcessToComplete(1);
    
    String adviseIdOfPrimaryBuy = userFund.getActiveAdvises().stream().filter(userAdvise -> userAdvise.getToken().equals(TICKER2)).findAny().orElse(null).getAdviseId();
    String adviseIdOfSecondaryBuy = userFund.getActiveAdvises().stream().filter(userAdvise -> userAdvise.getToken().equals(TICKER3)).findAny().orElse(null).getAdviseId();
    waitForProcessToComplete(1);
    
    UserAdvise userAdvise1 = new UserEquityAdvise();
    String persistenceId1 = new UserAdviseCompositeKey(adviseIdOfPrimaryBuy, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();

    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId1)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  
    userAdvise1.update(userAdviseEvent) ,materializer)
    .toCompletableFuture().get();
    waitForProcessToComplete(1);
    
    waitForProcessToComplete(1);
    assertThat(userAdvise1.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity())
    .isEqualTo(EXPECTED_TOTAL_PRIMARY_BUY_QUANTITY_POST_DEMERGER);

    assertThat(userAdvise1.getLatestIssuedAdviseOrder().get().getAverageTradedPrice())
    .isEqualTo(EXPECTED_AVERAGE_PRICE_FOR_PRIMARY_BUY_DEMERGER_EVENT);
    
    UserAdvise userAdvise2 = new UserEquityAdvise();
    String persistenceId2 = new UserAdviseCompositeKey(adviseIdOfSecondaryBuy, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId2)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  userAdvise2.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    assertThat(userAdvise2.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity())
    .isEqualTo(EXPECTED_TOTAL_SECONDARY_BUY_QUANTITY_POST_DEMERGER);
    assertThat(userAdvise2.getSymbol())
    .isEqualTo(TICKER3);
    assertThat(userAdvise2.getLatestIssuedAdviseOrder().get().getAverageTradedPrice())
    .isEqualTo(EXPECTED_AVERAGE_PRICE_FOR_SECONDARY_BUY_DEMERGER_EVENT);
  }
  
  @Test public void
  check_for_merger_demerger_case_A_to_A_plus_B_plus_C() throws Exception{
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    updateInstrumentsMapOfEquityActor(TICKER5, SYMBOL_5, INT_TOKEN_5);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER5, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);
    
    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    
    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSell(TICKER2, EXPECTED_SELL_PRICE_FOR_DEMERGER_EVENT), ActorRef.noSender());
    waitForProcessToComplete(1);

    actorSelections.advisersRootActor().tell(new AdviserCorporateEventPrimaryBuy(TICKER2, EXPECTED_SELL_PRICE_FOR_DEMERGER_EVENT, 364.0,1, SYMBOL_2), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSecondaryBuy(TICKER2, TICKER3, .3,1, SYMBOL_3), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSecondaryBuy(TICKER2, TICKER5, .7,1, SYMBOL_5), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    String userFundPersistenceId = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();

    journalProvider
      .currentEventsSourceForPersistenceId(userFundPersistenceId)
      .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
      .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
      .runForeach(userFundEvent ->  userFund.update(userFundEvent), materializer)
      .toCompletableFuture().get();
    waitForProcessToComplete(1);
    
    String adviseIdOfPrimaryBuy = userFund.getActiveAdvises().stream().filter(userAdvise -> userAdvise.getToken().equals(TICKER2)).findAny().orElse(null).getAdviseId();
    String adviseIdOfSecondaryBuy1 = userFund.getActiveAdvises().stream().filter(userAdvise -> userAdvise.getToken().equals(TICKER3)).findAny().orElse(null).getAdviseId();
    String adviseIdOfSecondaryBuy2 = userFund.getActiveAdvises().stream().filter(userAdvise -> userAdvise.getToken().equals(TICKER5)).findAny().orElse(null).getAdviseId();
    waitForProcessToComplete(1);
    
    UserAdvise userAdvise1 = new UserEquityAdvise();
    String persistenceId1 = new UserAdviseCompositeKey(adviseIdOfPrimaryBuy, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();
    
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId1)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  userAdvise1.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    assertThat(userAdvise1.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity())
    .isEqualTo(EXPECTED_TOTAL_PRIMARY_BUY_QUANTITY_POST_DEMERGER);
    
    assertThat(userAdvise1.getLatestIssuedAdviseOrder().get().getAverageTradedPrice())
    .isEqualTo(EXPECTED_AVERAGE_PRICE_FOR_PRIMARY_BUY_DEMERGER_EVENT);
    
    UserAdvise userAdvise2 = new UserEquityAdvise();
    String persistenceId2 = new UserAdviseCompositeKey(adviseIdOfSecondaryBuy1, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId2)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  userAdvise2.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    assertThat(userAdvise2.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity())
    .isEqualTo(EXPECTED_TOTAL_SECONDARY_BUY_QUANTITY_POST_DEMERGER);
    assertThat(userAdvise2.getSymbol())
    .isEqualTo(TICKER3);
    assertThat(round(userAdvise2.getLatestIssuedAdviseOrder().get().getAverageTradedPrice()))
    .isEqualTo(EXPECTED_AVERAGE_PRICE_FOR_FIRST_SECONDARY_BUY_DEMERGER_EVENT);
    waitForProcessToComplete(1);
    
    UserAdvise userAdvise3 = new UserEquityAdvise();
    String persistenceId3 = new UserAdviseCompositeKey(adviseIdOfSecondaryBuy2, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId3)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  userAdvise3.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();

    waitForProcessToComplete(1);
    assertThat(userAdvise3.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity())
    .isEqualTo(EXPECTED_TOTAL_SECONDARY_BUY_QUANTITY_POST_DEMERGER);
    assertThat(userAdvise3.getSymbol())
    .isEqualTo(TICKER5);
    assertThat(userAdvise3.getLatestIssuedAdviseOrder().get().getAverageTradedPrice())
    .isEqualTo(EXPECTED_AVERAGE_PRICE_FOR_SECOND_SECONDARY_BUY_DEMERGER_EVENT);
    
  }
  
  @Test public void
  check_for_merger_demerger_case_A_to_B_plus_C() throws Exception{
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    updateInstrumentsMapOfEquityActor(TICKER5, SYMBOL_5, INT_TOKEN_5);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER5, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);
    
    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    
    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSell(TICKER2, EXPECTED_SELL_PRICE_FOR_DEMERGER_EVENT), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSecondaryBuy(TICKER2, TICKER3, .3,1, SYMBOL_3), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    actorSelections.advisersRootActor().tell(new AdviserCorporateEventSecondaryBuy(TICKER2, TICKER5, .7,1, SYMBOL_5), ActorRef.noSender());
    waitForProcessToComplete(1);
    
    CurrentUserFundDb userFund = new CurrentUserFundDb();
    String userFundPersistenceId = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();

    journalProvider
      .currentEventsSourceForPersistenceId(userFundPersistenceId)
      .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
      .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
      .runForeach(userFundEvent ->  userFund.update(userFundEvent), materializer)
      .toCompletableFuture().get();
    waitForProcessToComplete(1);
    
    String adviseIdOfSecondaryBuy1 = userFund.getActiveAdvises().stream().filter(userAdvise -> userAdvise.getToken().equals(TICKER3)).findAny().orElse(null).getAdviseId();
    String adviseIdOfSecondaryBuy2 = userFund.getActiveAdvises().stream().filter(userAdvise -> userAdvise.getToken().equals(TICKER5)).findAny().orElse(null).getAdviseId();
    waitForProcessToComplete(1);
    
    UserAdvise userAdvise2 = new UserEquityAdvise();
    String persistenceId2 = new UserAdviseCompositeKey(adviseIdOfSecondaryBuy1, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId2)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  userAdvise2.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    assertThat(userAdvise2.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity())
    .isEqualTo(EXPECTED_TOTAL_SECONDARY_BUY_QUANTITY_POST_DEMERGER);
    assertThat(userAdvise2.getSymbol())
    .isEqualTo(TICKER3);
    assertThat(round(userAdvise2.getLatestIssuedAdviseOrder().get().getAverageTradedPrice()))
    .isEqualTo(EXPECTED_AVERAGE_PRICE_FOR_FIRST_SECONDARY_BUY_DEMERGER_EVENT_WITHOUT_PRIMARY_BUY);
    waitForProcessToComplete(1);
    
    UserAdvise userAdvise3 = new UserEquityAdvise();
    String persistenceId3 = new UserAdviseCompositeKey(adviseIdOfSecondaryBuy2, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL).persistenceId();
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId3)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent ->  userAdvise3.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();

    waitForProcessToComplete(1);
    assertThat(userAdvise3.getLatestIssuedAdviseOrder().get().getTotalTradedQuantity())
    .isEqualTo(EXPECTED_TOTAL_SECONDARY_BUY_QUANTITY_POST_DEMERGER);
    assertThat(userAdvise3.getSymbol())
    .isEqualTo(TICKER5);
    assertThat(userAdvise3.getLatestIssuedAdviseOrder().get().getAverageTradedPrice())
    .isEqualTo(EXPECTED_AVERAGE_PRICE_FOR_SECOND_SECONDARY_BUY_DEMERGER_EVENT_WITHOUT_PRIMARY_BUY);
    
  }
  
  @Test public void
  return_200_when_user_fund_subscription_status_is_subscribed() throws Exception{
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);

    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL.getInvestingMode()))).isTrue();

    waitForViewToPopulate(1);

    response = authenticatedGet("/userfundsubscriptiondetails", "adviser=" + ADVISER.getAdviserUsername() + "&fund="
        + FUND_NAME + "&investingMode=" + VIRTUAL.getInvestingMode());
    UserFundSubscriptionDetails responseEntitySubscriptionDetails = response.entity(unmarshaller(UserFundSubscriptionDetails.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntitySubscriptionDetails).isInstanceOf(UserFundSubscriptionDetails.class);
    assertThat(responseEntitySubscriptionDetails.getSubscriptionAmount()).isEqualTo(SUBSCRIPTION_LUMPSUM);
    assertThat(responseEntitySubscriptionDetails.getWithdrawal()).isEqualTo(0.);
  }

  @Test public void 
  return_200_when_user_fund_subscription_status_is_subscribed_then_unsubscribed_get_withdrawal_Amt() throws Exception{
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);

    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL.getInvestingMode()))).isTrue();

    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    response = authenticatedGet("/userfundsubscriptiondetails", "adviser=" + ADVISER.getAdviserUsername() + "&fund="
        + FUND_NAME + "&investingMode=" + VIRTUAL.getInvestingMode());
    UserFundSubscriptionDetails responseEntitySubscriptionDetails = response.entity(unmarshaller(UserFundSubscriptionDetails.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntitySubscriptionDetails).isInstanceOf(UserFundSubscriptionDetails.class);
    assertThat(responseEntitySubscriptionDetails.getSubscriptionAmount()).isEqualTo(SUBSCRIPTION_LUMPSUM);
    assertThat(responseEntitySubscriptionDetails.getWithdrawal()).isEqualTo(0.);
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("UnSubscribe in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    response = authenticatedGet("/userfundsubscriptiondetails", "adviser=" + ADVISER.getAdviserUsername() + "&fund="
        + FUND_NAME + "&investingMode=" + VIRTUAL.getInvestingMode());
    responseEntitySubscriptionDetails = response.entity(unmarshaller(UserFundSubscriptionDetails.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntitySubscriptionDetails).isInstanceOf(UserFundSubscriptionDetails.class);
    assertThat(responseEntitySubscriptionDetails.getSubscriptionAmount()).isEqualTo(0.);
    assertThat(responseEntitySubscriptionDetails.getWithdrawal()).isEqualTo(SUBSCRIPTION_LUMPSUM);
  }

  @Test public void 
  honor_complete_close_advises_post_fund_mirroring_in_investors_account() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);
    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(10);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    registerUserAndActivate();
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_2));

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_3));

    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);
    autoApprove(FUND_NAME);
    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForViewToPopulate(1);
    sendEODPriceToEquityActor(ACTUAL_EXITPRICE, TICKER1, currentTimeInMillis());
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseResponseEntity.getId(),
        PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);
    sendEODPriceToEquityActor(ACTUAL_EXITPRICE, TICKER2, currentTimeInMillis());
    closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, secondAdviseResponseEntity.getId(),
        COMPLETE_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_THREE_ISSUED_ADVICES_AND_A_PARTIAL_AND_A_COMPLETE_EXIT);
    UserAdvise userAdvise = new UserEquityAdvise();
    String persistenceId = new UserAdviseCompositeKey(secondAdviseResponseEntity.getId(), SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();

    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> userAdvise.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);
    
    assertThat(userAdvise.getCloseAdviseAbsoluteAllocation())
        .isEqualTo(EXPECTED_ABSOLUTE_COMPLETE_CLOSE_ADVICE_ALLOCATION_2);
    assertThat(userAdvise.getAverageExitPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_COMPLETE_EXIT_PRICE_1);

    assertThat(userAdvise.getIssueAdviseOrder()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));
    assertThat(userAdvise.getCloseAdviseOrders()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));

    assertThat(userAdvise.getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_ON_COMPLETE_EXIT);
    assertThat(userAdvise.getAverageExitPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_COMPLETE_EXIT_PRICE_1);
    
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, 0);

    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_COMPLETE_EXIT_ORDER_QUANTITY_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_COMPLETE_ORDER_TRADED_QUANTITY_2);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_COMPLETE_CLOSE_ADVISE);
  }
  
  @Test public void
  check_for_cashComponent_adjustments_with_multiple_pending_approvals() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1_NEW, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    String firstAdviseId = responseEntity.getId();
    
    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);
    
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2_NEW, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String secondAdviseId = responseEntity.getId();
    
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION1, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForProcessToComplete(1);
    
    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3_1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", thirdAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String thirdAdviceId = responseEntity.getId();
    
    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(thirdAdviceId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER3, SYMBOL_3, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_3_NEW,  EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_3_NEW, 0, ENTRYTIME_3),
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_2, 0, (int) ((EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_2 * EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_2) / 100.), ENTRYTIME_2),
        new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
            SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER2, SYMBOL_2, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2_NEW, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2_NEW, 0, ENTRYTIME)
        )
        .collect(Collectors.toList());
    
    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = postAuthUserCommand("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    List<UserResponseReceived> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserResponseReceived)
        .map(userFundEvent -> ((UserResponseReceived) userFundEvent.event()))
        .runWith(Sink.seq(), materializer)
        .toCompletableFuture().get();
    
    
    UserResponseReceived userResponse = allUserFundEvents.stream().filter((resp) -> TICKER3.equals(resp.getToken())).findAny().orElse(null);
    assertThat(userResponse.getAllocationValue()).isEqualTo(ISSUE_ADVISE_ALLOCATION_VALUE_FOR_PENDING_ADVISE);
    
    
  }
  
  @Test public void 
  honor_complete_exit_in_investors_account() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER3, ALLOCATION3, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME3);
    response = post("/issueadvise", thirdAdvice);

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3);

    waitForProcessToComplete(1);

    Done thirdResponseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(thirdResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForViewToPopulate(1);
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseResponseEntity.getId(),
        PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);
    

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);

    closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, secondAdviseResponseEntity.getId(),
        COMPLETE_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("UnSubscribe in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash()).isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_EXIT_FUND);
    autoApprove(FUND_NAME);
    UserAdvise userAdvise = new UserEquityAdvise();
    String persistenceId = new UserAdviseCompositeKey(firstAdviseResponseEntity.getId(), SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> userAdvise.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();

    assertThat(userAdvise.getCloseAdviseAbsoluteAllocation())
        .isEqualTo(EXPECTED_EXIT_ALLOCATION_1);
    assertThat(userAdvise.getAverageExitPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_EXIT_PRICE_1);

    assertThat(userAdvise.getIssueAdviseOrder()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));
    assertThat(userAdvise.getCloseAdviseOrders()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));

    assertThat(userAdvise.getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_ON_COMPLETE_EXIT);
    
    UserAdvise userAdvise2 = new UserEquityAdvise();
    String persistenceId2 = new UserAdviseCompositeKey(secondAdviseResponseEntity.getId(), SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId2)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> userAdvise2.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();

    assertThat(userAdvise2.getCloseAdviseAbsoluteAllocation())
        .isEqualTo(EXPECTED_EXIT_ALLOCATION_2);
    assertThat(userAdvise2.getAverageExitPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_EXIT_PRICE_2);
    
    assertThat(userAdvise.getIssueAdviseOrder()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));
    assertThat(userAdvise.getCloseAdviseOrders())
        .isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));

    assertThat(userAdvise2.getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_ON_COMPLETE_EXIT);
    
    UserAdvise userAdvise3 = new UserEquityAdvise();
    String persistenceId3 = new UserAdviseCompositeKey(thirdResponseEntity.getId(), SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId3)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> userAdvise3.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();

    assertThat(userAdvise3.getCloseAdviseAbsoluteAllocation())
        .isEqualTo(EXPECTED_EXIT_ALLOCATION_3);
    assertThat(userAdvise3.getAverageExitPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_EXIT_PRICE_3);

    assertThat(userAdvise3.getIssueAdviseOrder()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));
    assertThat(userAdvise3.getCloseAdviseOrders()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));

    assertThat(userAdvise3.getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_ON_COMPLETE_EXIT);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());

    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_EXIT_ORDER_QUANTITY_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_EXIT_TRADED_QUANTITY_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == -1).flatMap(order -> order.getTrades().stream());

    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_EXIT_ORDER_QUANTITY_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_EXIT_TRADED_QUANTITY_2);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER3, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == -1).flatMap(order -> order.getTrades().stream());

    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_EXIT_ORDER_QUANTITY_3);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_EXIT_TRADED_QUANTITY_3);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    Optional<CurrentUserFundDb> potentialUserFund = userFundRepository.findById(userFundCompositeKey);
    assertThat(potentialUserFund.isPresent()).isFalse();
  }

  @Test public void 
  honor_complete_exit_in_fund_with_no_advises() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("UnSubscribe in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    response = authenticatedGet("/walletmoney", "investingMode=" + VIRTUAL.getInvestingMode());

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    Optional<CurrentUserFundDb> potentialUserFund = userFundRepository.findById(userFundCompositeKey);
    assertThat(potentialUserFund.isPresent()).isFalse();
    assertThat(Double.parseDouble(response.entity(unmarshaller(Done.class)).getMessage()))
        .isEqualTo(EXPECTED_WALLET_MONEY_POST_EXIT_FUND_WITH_NO_ADVICES);
  }

  @Test public void 
  honor_add_lumpsum_in_fund_with_no_advises() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);
    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME,
        ADD_LUMPSUM, VIRTUAL));

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    Optional<CurrentUserFundDb> possibleUserFund = userFundRepository.findById(userFundCompositeKey);
    CurrentUserFundDb userFund = possibleUserFund.orElseGet(() -> {
      return new CurrentUserFundDb();
    });
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_ADD_LUMPSUM_FUND);
  }

  @Test @Ignore public void 
  send_notification_to_user_when_user_advise_cannot_be_honored_after_ten_attempts() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER5, SYMBOL_5, INT_TOKEN_5);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER5, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER5, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    registerUserAndActivate();

    response = postAuthUserCommand("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME,
        SUBSCRIPTION_LUMPSUM, SUBSCRIPTION_SIPAMOUNT, SUBSCRIPTION_SIP_DATE, SUBSCRIPTION_WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

  }

  @Test public void 
  add_the_cash_back_to_fund_in_case_of_insufficient_capital_to_replicate() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER4, SYMBOL_5, INT_TOKEN_4);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER4, currentTimeInMillis());
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER4, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    String firstAdviseId = firstAdviseResponseEntity.getId();
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION_HIGH, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    String secondAdviseId = secondAdviseResponseEntity.getId();

    registerUserAndActivate();

    sendLivePriceToEquityActor(VERY_HIGH_STOCK_PRICE, valueOf(INT_TOKEN_4));

    subscriptionProcess();

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_POST_REPLICATION);
    
    sendLivePriceToEquityActor(ENTRYPRICE, valueOf(INT_TOKEN_4));
    waitForProcessToComplete(1);

    TestRouteResult rejectedUserAdvises = authenticatedGet("/userAllFundRejectedAdvises",
        "investingMode=" + VIRTUAL.getInvestingMode());
    ConsolidatedRejectedUserAdviseResponses consolidatedRejectedAdvises = rejectedUserAdvises
        .entity(unmarshaller(ConsolidatedRejectedUserAdviseResponses.class));
    rejectedUserAdvises.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(consolidatedRejectedAdvises.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getNumberOfShares())
        .isEqualTo(EXPECTED_REJECTED_ADVICE_NUMBER_OF_SHARES_FOR_THE_ADVISE);
    assertThat(consolidatedRejectedAdvises.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getAllocationValue())
    .isEqualTo(EXPECTED_REJECTED_BUY_ALLOCATION_VALUE_FOR_THE_ADVISE);
    assertThat(consolidatedRejectedAdvises.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getClientCode())
    .isEqualTo(SUBSCRIPTION_USER_NAME);

    waitForProcessToComplete(1);

    TestRouteResult rejectedUserFundAdvises = authenticatedGet("/userFundRejectedAdvises",
        "investingMode=" + VIRTUAL.getInvestingMode()+"&adviserName="+ADVISER_USERNAME+"&fundName="+FUND_NAME);
    AllRejectedUserFundAdvises allRejectedUserFundAdvises = rejectedUserFundAdvises
        .entity(unmarshaller(AllRejectedUserFundAdvises.class));
    rejectedUserFundAdvises.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(allRejectedUserFundAdvises.getUserResponseParameters().get(0).getNumberOfShares())
        .isEqualTo(EXPECTED_REJECTED_ADVICE_NUMBER_OF_SHARES_FOR_THE_ADVISE);
    assertThat(allRejectedUserFundAdvises.getUserResponseParameters().get(0).getAllocationValue())
    .isEqualTo(EXPECTED_REJECTED_BUY_ALLOCATION_VALUE_FOR_THE_ADVISE);
    assertThat(allRejectedUserFundAdvises.getUserResponseParameters().get(0).getClientCode())
    .isEqualTo(SUBSCRIPTION_USER_NAME);

    List<UserResponseParameters> randomUserRetries = Stream.of(new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
        SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
        EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_2, 0, (int) ((EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_2 * EXPECTED_ABSOLUTE_PARTIAL_CLOSE_ADVICE_ALLOCATION_2) / 100.), ENTRYTIME_2),
    new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
        SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER2, SYMBOL_2, VIRTUAL, Approve, 
        EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2_NEW, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2_NEW, 0, ENTRYTIME)).collect(Collectors.toList());
    ObjectMapper objectMapper = new ObjectMapper();
    String adviseIdsAsJsonString = objectMapper.writeValueAsString(randomUserRetries);
    RetryRejectedAdvise retryRejectedAdvise = new RetryRejectedAdvise(adviseIdsAsJsonString, "1ABC",BrokerAuthInformation.withNoBrokerAuth());
    TestRouteResult retryAdviseResponse = postAuthUserCommand("/retryrejectedadvise", retryRejectedAdvise);
    waitForViewToPopulate(1);

    Failed retryAdviseResponseEntity = retryAdviseResponse.entity(unmarshaller(Failed.class));
    retryAdviseResponse.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(retryAdviseResponseEntity.getMessage()).isEqualTo("Advise not found in active rejected advises list");
    
    waitForProcessToComplete(1);
    
    List<UserResponseParameters> adviseIds1 = Stream.of(new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Close, 
        SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER4, SYMBOL_1, VIRTUAL, Approve, 
        EXPECTED_ABSOLUTE_FULL_ISSUE_ADVICE_ALLOCATION, EXPECTED_ABSOLUTE_FULL_ISSUE_ADVICE_ALLOCATION_VALUE, EXPECTED_REJECTED_ADVICE_NUMBER_OF_SHARES_FOR_THE_ADVISE, ENTRYTIME_2)).collect(Collectors.toList());
    ObjectMapper objectMapper1 = new ObjectMapper();
    String adviseIdsAsJsonString1 = objectMapper1.writeValueAsString(adviseIds1);
    RetryRejectedAdvise retryRejectedAdvise1 = new RetryRejectedAdvise(adviseIdsAsJsonString1, "1ABC",BrokerAuthInformation.withNoBrokerAuth());
    TestRouteResult retryAdviseResponse1 = postAuthUserCommand("/retryrejectedadvise", retryRejectedAdvise1);
    waitForViewToPopulate(1);

    Done retryAdviseResponseEntity1 = retryAdviseResponse1.entity(unmarshaller(Done.class));
    retryAdviseResponse1.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(retryAdviseResponseEntity1.getMessage()).isEqualTo("Retrials in progress. Scoot over to your investments to see the status!");
    
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER4, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());

    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ENTRY_ORDER_QUANTITY_1);

    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ENTRY_TRADED_QUANTITY_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ENTRY_ORDER_QUANTITY_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ENTRY_TRADED_QUANTITY_2);
    
    CurrentUserFundDb userFund1 = new CurrentUserFundDb();
    journalProvider
    .currentEventsSourceForPersistenceId(FUND_NAME)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
    .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
    .runForeach(userFundEvent ->  userFund1.update(userFundEvent), materializer)
    .toCompletableFuture().get();

    assertThat(userFund1.getActiveRejectedBuySideAdvises().size()).isEqualTo(0);
    
  }

  @Test
  public void schedule_retry_rejections_post_market_hours_and_execute_when_market_is_open() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER4, SYMBOL_5, INT_TOKEN_4);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER4, currentTimeInMillis());
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION,
        FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER4, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    String firstAdviseId = firstAdviseResponseEntity.getId();
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION_HIGH,
        ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    registerUserAndActivate();

    sendLivePriceToEquityActor(VERY_HIGH_STOCK_PRICE, valueOf(INT_TOKEN_4));

    subscriptionProcess();

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0],
        FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_POST_REPLICATION);

    sendLivePriceToEquityActor(ENTRYPRICE, valueOf(INT_TOKEN_4));
    waitForProcessToComplete(1);

    TestRouteResult rejectedUserAdvises = authenticatedGet("/userAllFundRejectedAdvises",
        "investingMode=" + VIRTUAL.getInvestingMode());
    ConsolidatedRejectedUserAdviseResponses consolidatedRejectedAdvises = rejectedUserAdvises
        .entity(unmarshaller(ConsolidatedRejectedUserAdviseResponses.class));
    rejectedUserAdvises.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(consolidatedRejectedAdvises.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getNumberOfShares())
        .isEqualTo(EXPECTED_REJECTED_ADVICE_NUMBER_OF_SHARES_FOR_THE_ADVISE);
    assertThat(consolidatedRejectedAdvises.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getAllocationValue())
    .isEqualTo(EXPECTED_REJECTED_BUY_ALLOCATION_VALUE_FOR_THE_ADVISE);
    assertThat(consolidatedRejectedAdvises.getUserResponseParametersFromFunds().get(0).getUserResponseParameters().get(0).getClientCode())
    .isEqualTo(SUBSCRIPTION_USER_NAME);

    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    List<UserResponseParameters> adviseIds1 = Stream.of(new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME, Issue, 
        SUBSCRIPTION_USER_NAME, MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER4, SYMBOL_5, VIRTUAL, Approve, 
        EXPECTED_ABSOLUTE_FULL_ISSUE_ADVICE_ALLOCATION, EXPECTED_REJECTED_BUY_ALLOCATION_VALUE_FOR_THE_ADVISE,
        EXPECTED_REJECTED_ADVICE_NUMBER_OF_SHARES_FOR_THE_ADVISE, ENTRYTIME_2)).collect(Collectors.toList());
    ObjectMapper objectMapper1 = new ObjectMapper();
    String adviseIdsAsJsonString1 = objectMapper1.writeValueAsString(adviseIds1);
    RetryRejectedAdvise retryRejectedAdvise1 = new RetryRejectedAdvise(adviseIdsAsJsonString1, "1ABC",BrokerAuthInformation.withNoBrokerAuth());
    TestRouteResult retryAdviseResponse1 = postAuthUserCommand("/retryrejectedadvise", retryRejectedAdvise1);
    waitForViewToPopulate(1);

    Done retryAdviseResponseEntity1 = retryAdviseResponse1.entity(unmarshaller(Done.class));
    retryAdviseResponse1.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(retryAdviseResponseEntity1.getMessage())
        .isEqualTo("Your retrying request has been received and will be executed on next trading day.");

    environmentVariables.set("WT_MARKET_OPEN", "true");
    sendLivePriceToEquityActor(ENTRYPRICE, valueOf(INT_TOKEN_4));
    actorSelections.userRootActor().tell(new ExecutePostMarketUserCommands(), ActorRef.noSender());
    
    waitForViewToPopulate(1);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER4, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());

    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ENTRY_ORDER_QUANTITY_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ENTRY_TRADED_QUANTITY_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());

    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ENTRY_ORDER_QUANTITY_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_ENTRY_TRADED_QUANTITY_2);

    CurrentUserFundDb userFund1 = new CurrentUserFundDb();
    journalProvider.currentEventsSourceForPersistenceId(FUND_NAME)
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundEvent)
        .map(eventEnvelope -> (UserFundEvent) eventEnvelope.event())
        .runForeach(userFundEvent -> userFund1.update(userFundEvent), materializer).toCompletableFuture().get();

    assertThat(userFund1.getActiveRejectedBuySideAdvises().size()).isEqualTo(0);
  }

  @Test public void 
  add_the_cash_back_to_fund_in_case_of_rejection_by_ctcl() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER6, SYMBOL_6, INT_TOKEN_6);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER6, currentTimeInMillis());
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER6, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION_HIGH, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(INT_TOKEN_6));

    subscriptionProcess();
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_POST_REPLICATION_1);
  }

  @Test public void 
  add_the_cash_back_to_fund_in_case_of_rejection_by_exchange() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER7, SYMBOL_7, INT_TOKEN_7);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER7, currentTimeInMillis());
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER7, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION_HIGH, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(INT_TOKEN_7));

    subscriptionProcess();
    waitForProcessToComplete(1);
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_POST_REPLICATION_1);
    assertThat(userFund.isSubscribed()).isTrue();

  }

  @Test public void 
  add_the_cash_back_to_fund_in_case_of_rejection_by_exchange_with_faults() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER11, SYMBOL_11, INT_TOKEN_11);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER11, currentTimeInMillis());
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER11, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION_HIGH, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    registerUserAndActivate();

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(INT_TOKEN_11));

    subscriptionProcess();
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, INVESTING_MODE);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_POST_REPLICATION_1);

  }

  @Test public void 
  return_200_when_adviser_issues_MutualFundAndEquity_advices() throws InterruptedException {
    updateInstrumentsMapOfMFActor(MF_TICKER1, SYMBOL_1, MF_SCHEME1, SCHEME_CODE1, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA);
    sendBODPriceToMFActor(LAST_NAV1, MF_TICKER1);
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice MFAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID1, MF_TICKER1, ALLOCATION1,
        0, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", MFAdvice);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    IssueAdvice equityAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", equityAdvice);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
  }
  
  @Test public void 
  return_200_when_investor_successfully_subscribes_and_then_unsubscribe_to_the_Mixed_fund() throws Exception {
    updateInstrumentsMapOfMFActor(MF_TICKER1, SYMBOL_1, MF_SCHEME1, SCHEME_CODE1, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
    sendBODPriceToMFActor(LAST_NAV1, MF_TICKER1);
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    IssueAdvice firstMFAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID1, MF_TICKER1, ALLOCATION1,
        LAST_NAV1, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstMFAdvice);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    IssueAdvice firstEquityAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstEquityAdvice);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    registerUserAndActivate();

    subscriptionProcess();
    waitForProcessToComplete(1);
    autoApprove(FUND_NAME);
    waitForViewToPopulate(1);
    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL.getInvestingMode())))
            .isTrue();
    
    
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_1);
    
    updateInstrumentsMapOfMFActor(MF_TICKER2, SYMBOL_2, MF_SCHEME2, SCHEME_CODE2, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
    sendBODPriceToMFActor(LAST_NAV2, MF_TICKER2);
    IssueAdvice secondMFAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID2, MF_TICKER2, ALLOCATION2,
        LAST_NAV2, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondMFAdvice);

    waitForProcessToComplete(1);
    waitForViewToPopulate(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    IssueAdvice secondEquityAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondEquityAdvice);

    waitForProcessToComplete(1);
    waitForViewToPopulate(3);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    List<Order> secondEquityAdviseorders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    Stream<Trade> secondEquityTrades = secondEquityAdviseorders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        secondEquityAdviseorders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_IN_MIXED_FUND);
    assertThat(secondEquityTrades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_IN_MIXED_FUND);
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("UnSubscribe in process. You will receive a notification once its done.");
    waitForProcessToComplete(1);
    waitForViewToPopulate(1);
    List<Order> mfOrders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(MF_TICKER1, timeFromMidnight());
    assertThat(mfOrders.stream().filter(o -> o.getSide().getValue() == -1)
        .findFirst().get().getQuantity()).isEqualTo(EXPECTED_MF_TOTAL_TRADED_QUANTITY_1);
    
    List<Order> secondmfOrders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(MF_TICKER2, timeFromMidnight());
    assertThat(secondmfOrders.stream().filter(o -> o.getSide().getValue() == -1)
        .findFirst().get().getQuantity()).isEqualTo(EXPECTED_MF_TOTAL_TRADED_QUANTITY_2);
    waitForViewToPopulate(1);
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    Optional<CurrentUserFundDb> potentialUserFund = userFundRepository.findById(userFundCompositeKey);
    assertThat(potentialUserFund.isPresent()).isFalse();
  }

  @Test
  public void honor_partial_exit_in_investors_account_for_MF() throws Exception {
  updateInstrumentsMapOfMFActor(MF_TICKER1, SYMBOL_1, MF_SCHEME1, SCHEME_CODE1, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
  sendBODPriceToMFActor(LAST_NAV1, MF_TICKER1);

  TestRouteResult response;
  Done responseEntity;
  registerAdviserProcess();

  response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
      FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

  responseEntity = response.entity(unmarshaller(Done.class));
  response.assertStatusCode(OK).assertMediaType("application/json");
  assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
  
  IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID1, MF_TICKER1, ALLOCATION1,
      LAST_NAV1, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
  TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

  waitForProcessToComplete(1);

  Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
  firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
  assertThat(firstAdviseResponseEntity.getMessage())
      .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
  
  registerUserAndActivate();

  subscriptionProcess();
  waitForViewToPopulate(1);
  CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseResponseEntity.getId(),
      PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
  response = post("/closeadvise", closeAdvise);
  responseEntity = response.entity(unmarshaller(Done.class));
  response.assertStatusCode(OK).assertMediaType("application/json");
  assertThat(responseEntity.getMessage()).isEqualTo(
      "Your partial exit advise request is being processed, please check the notification for the updates.");

  waitForViewToPopulate(1);
  
  double quantity = (PARTIAL_CLOSING_ALLOCATION/100) * SUBSCRIPTION_LUMPSUM / LAST_NAV1;
  quantity = round(quantity , 4);
  UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(firstAdviseResponseEntity.getId(), SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME, ADVISER_USERNAME, VIRTUAL);
  UserMFAdvise userMFAdvise = userMFAdviseRepository.findById(userAdviseCompositeKey).orElse(null);
  assertThat(userMFAdvise.getCloseAdviseAbsoluteAllocation()).isEqualTo(PARTIAL_CLOSING_ALLOCATION*100/ALLOCATION1);
  }
  
  @Test
  public void honor_complete_exit_in_investors_account_for_MF() throws Exception {
    updateInstrumentsMapOfMFActor(MF_TICKER1, SYMBOL_1, MF_SCHEME1, SCHEME_CODE1, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
    sendBODPriceToMFActor(LAST_NAV1, MF_TICKER1);
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID1, MF_TICKER1, ALLOCATION1,
        LAST_NAV1, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);

    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(firstAdviseResponseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    updateInstrumentsMapOfMFActor(MF_TICKER2, SYMBOL_2, MF_SCHEME2, SCHEME_CODE2, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
    sendBODPriceToMFActor(LAST_NAV2, MF_TICKER2);
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID2, MF_TICKER2, ALLOCATION2,
        LAST_NAV2, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseResponseEntity.getId(),
        ALLOCATION1, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);
    

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(
        "Your exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);

    closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, secondAdviseResponseEntity.getId(),
        ALLOCATION2, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);

    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("UnSubscribe in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    UserAdvise userAdvise = new UserMFAdvise();
    String persistenceId = new UserAdviseCompositeKey(firstAdviseResponseEntity.getId(), SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> userAdvise.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();
    
    waitForProcessToComplete(1);

    assertThat(userAdvise.getCloseAdviseAbsoluteAllocation())
        .isEqualTo(EXPECTED_EXIT_ALLOCATION_1);
    assertThat(userAdvise.getAverageExitPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_EXIT_PRICE_1);

    assertThat(userAdvise.getIssueAdviseOrder()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));
    assertThat(userAdvise.getCloseAdviseOrders()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));

    assertThat(userAdvise.getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_ON_COMPLETE_EXIT);
    
    UserAdvise userAdvise2 = new UserMFAdvise();
    String persistenceId2 = new UserAdviseCompositeKey(secondAdviseResponseEntity.getId(), SUBSCRIPTION_USER_NAME, FUND_NAME,
        ADVISER_USERNAME, VIRTUAL).persistenceId();
    
    journalProvider
    .currentEventsSourceForPersistenceId(persistenceId2)
    .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> userAdvise2.update(userAdviseEvent), materializer)
    .toCompletableFuture().get();

    assertThat(userAdvise2.getCloseAdviseAbsoluteAllocation())
        .isEqualTo(EXPECTED_EXIT_ALLOCATION_2);
    assertThat(userAdvise2.getAverageExitPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_EXIT_PRICE_2);
    
    assertThat(userAdvise2.getIssueAdviseOrder()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));
    assertThat(userAdvise2.getCloseAdviseOrders()).isEqualTo(new TreeSet<Order>(Comparator.comparingLong(Order::getOrderTimestamp)));

    assertThat(userAdvise2.getAverageEntryPrice().getPrice().getPrice())
        .isEqualTo(EXPECTED_AVERAGE_ENTRY_PRICE_ON_COMPLETE_EXIT);
  }
 
  @Test  public void 
  add_the_cash_back_to_MF_fund_in_case_of_rejection() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    registerUserAndActivate();

    subscriptionProcess();
    waitForViewToPopulate(1);
    autoApprove(FUND_NAME);
    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL.getInvestingMode())))
            .isTrue();
    updateInstrumentsMapOfMFActor(MF_TICKER3, SYMBOL_1, MF_SCHEME3, SCHEME_CODE3, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
    sendBODPriceToMFActor(LAST_NAV1, MF_TICKER3);
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID3, MF_TICKER3, ALLOCATION1,
        0, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);
    waitForViewToPopulate(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(SUBSCRIPTION_LUMPSUM);
  }
  
  @Test public void
  check_for_correct_folioNumber_in_MutualFund() throws Exception {
    updateInstrumentsMapOfMFActor(MF_FOLIO_TICKER1, SYMBOL_1, MF_SCHEME1, SCHEME_CODE1, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
    sendBODPriceToMFActor(LAST_NAV1, MF_FOLIO_TICKER1);
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    registerUserAndActivate();

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID1, MF_FOLIO_TICKER1, ALLOCATION1,
        LAST_NAV1, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    TestRouteResult firstAdviseResponse = post("/issueadvise", firstAdvice);

    waitForProcessToComplete(1);
    Done firstAdviseResponseEntity = firstAdviseResponse.entity(unmarshaller(Done.class));
    firstAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    String firstAdviseID = firstAdviseResponseEntity.getId();
    assertThat(firstAdviseResponseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    
    subscriptionProcess();
    waitForProcessToComplete(1);
    autoApprove(FUND_NAME);
    
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(firstAdviseID, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, InvestingMode.VIRTUAL);
    UserMFAdvise usermfAdviseFor1stFolioNumber = new UserMFAdvise();
    String persistenceId = userAdviseCompositeKey.persistenceId();
    journalProvider.currentEventsSourceForPersistenceId(persistenceId)
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> {
      if(userAdviseEvent instanceof AdviseOrderTradeExecuted) {
        AdviseOrderTradeExecuted adviseOrderTradeExecuted = (AdviseOrderTradeExecuted)userAdviseEvent;
        usermfAdviseFor1stFolioNumber.update(adviseOrderTradeExecuted); 
      }
    }, materializer);
    
    waitForProcessToComplete(1);

    updateInstrumentsMapOfMFActor(MF_FOLIO_TICKER2, SYMBOL_2, MF_SCHEME2, SCHEME_CODE2, MIN_INVESTMENT, MIN_MULTIPLIER, SCHEME_TYPE, EXIT_LOAD, RTA );
    sendBODPriceToMFActor(LAST_NAV2, MF_FOLIO_TICKER2);
    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, MFADVISEID2, MF_FOLIO_TICKER2, ALLOCATION2,
        LAST_NAV2, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    TestRouteResult secondAdviseResponse = post("/issueadvise", secondAdvice);

    waitForProcessToComplete(1);

    Done secondAdviseResponseEntity = secondAdviseResponse.entity(unmarshaller(Done.class));
    secondAdviseResponse.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(secondAdviseResponseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
    String secondAdviseId = secondAdviseResponseEntity.getId();
    UserAdviseCompositeKey secondUserAdviseCompositeKey = new UserAdviseCompositeKey(secondAdviseId, SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_USERNAME, InvestingMode.VIRTUAL);
    
    UserMFAdvise usermfAdviseFor2ndFolioNumber = new UserMFAdvise();
    persistenceId = secondUserAdviseCompositeKey.persistenceId();
    journalProvider.currentEventsSourceForPersistenceId(persistenceId)
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserAdviseEvent)
    .map(eventEnvelope -> (UserAdviseEvent) eventEnvelope.event())
    .runForeach(userAdviseEvent -> {
      if(userAdviseEvent instanceof AdviseOrderTradeExecuted) {
        AdviseOrderTradeExecuted adviseOrderTradeExecuted = (AdviseOrderTradeExecuted)userAdviseEvent;
        usermfAdviseFor2ndFolioNumber.update(adviseOrderTradeExecuted);
      }
    }, materializer);
    
    waitForProcessToComplete(1);
    
    assert(usermfAdviseFor1stFolioNumber.getFolioNumber()).equals(usermfAdviseFor2ndFolioNumber.getFolioNumber());
    
  }
  
  @Test public void 
  honor_partial_withdraw_from_fund() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    
    TestRouteResult response; 
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(3);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 30000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(2);
    
    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(CASH_COMPONENT_AFTER_PARTIAL_WITHDRAWAL);
  }
  
  @Test public void 
  honor_partial_withdraw_from_fund_with_no_advises() throws Exception {
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    registerUserAndActivate();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(2);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    
    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 30000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(2);

    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(CASH_COMPONENT_AFTER_PARTIAL_WITHDRAWAL_WITHOUT_ADVISES);
  }
  
  @Test public void
  honor_multiple_withdrawal_along_with_multiple_add_Lumpsum() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(ENTRYPRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());
    TestRouteResult response; 
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION,
        FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2,
        ENTRYPRICE_2, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");

    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, valueOf(TOKEN_2));

    response = authenticatedGet("/subscription",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&sipAmount="
            + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" + SUBSCRIPTION_SIP_DATE
            + "&investingMode=" + VIRTUAL.getInvestingMode());

    waitForProcessToComplete(2);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_2_IN_MULTIPLE_ADD_LUMPSUM);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_POSTLUMPSUM);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0],
        FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM);
    waitForViewToPopulate(1);

    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_3, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_3, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2, valueOf(TOKEN_2));

    waitForProcessToComplete(1);

    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME, ADD_LUMPSUM, VIRTUAL));
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    waitForProcessToComplete(1);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER1).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_LUMPSUM_1);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER1).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_LUMPSUM_1);

    assertThat(userEquityAdviseRepository.findByToken(TICKER2).getIssueAdviseAllocationValue())
        .isEqualTo(EXPECTED_ISSUED_ADVICE_ALLOCATION_VALUE_POST_LUMPSUM_2);
    assertThat(round(userEquityAdviseRepository.findByToken(TICKER2).getAverageEntryPrice().getPrice().getPrice()))
        .isEqualTo(EXPECTEDAVERAGEENTRYPRICE_POST_LUMPSUM_2);

    assertThat(fundRepository.findById(FUND_NAME).orElse(null).getCash())
        .isEqualTo(EXPECTED_ADVISER_FUNDCASH_POST_TWO_ISSUED_ADVICES_POSTLUMPSUM);

    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_LUMPSUM_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(
        orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_LUMPSUM_2);

    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund1.getCashComponent()).isEqualTo(FUND_AVAILABLE_CASH_POST_LUMPSUMADDITION);

    waitForViewToPopulate(1);

    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 30000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);

    List<Order> after_withdrawal_orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1,
        timeFromMidnight());
    Stream<Trade> after_withdrawal_trades = after_withdrawal_orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(after_withdrawal_orders.stream().filter(o -> o.getSide().getValue() == -1)
        .mapToInt(order -> order.getIntegerQuantity()).sum()).isEqualTo(5);
    assertThat(after_withdrawal_trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(5);

    after_withdrawal_orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = after_withdrawal_orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(after_withdrawal_orders.stream().filter(o -> o.getSide().getValue() == -1)
        .mapToInt(order -> order.getIntegerQuantity()).sum()).isEqualTo(8);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(8);

    UserFundCompositeKey userFundCompositeKey1 = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0],
        FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund2 = userFundRepository.findById(userFundCompositeKey1).orElse(null);
    assertThat(round(userFund2.getCashComponent())).isEqualTo(302211.63);

    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME, ADD_LUMPSUM, VIRTUAL));
    waitForProcessToComplete(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");
    waitForViewToPopulate(1);
    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> Buy_trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    Stream<Trade> Sell_trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().mapToInt(order -> {
      if (order.getSide().getValue() == 1)
        return order.getIntegerQuantity();
      else
        return (-1 * order.getIntegerQuantity());
    }).sum()).isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM1_2nd_Time);
    assertThat(Buy_trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()
        - Sell_trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM1_2nd_Time);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    Buy_trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    Sell_trades = orders.stream().filter(o -> o.getSide().getValue() == -1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().mapToInt(order -> {
      if (order.getSide().getValue() == 1)
        return order.getIntegerQuantity();
      else
        return (-1 * order.getIntegerQuantity());
    }).sum()).isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM2_2nd_Time);
    assertThat(Buy_trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()
        - Sell_trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_LUMPSUM2_2nd_Time);

    userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(round(userFund.getCashComponent())).isEqualTo(FUND_AVAILABLE_CASH_POST_LUMPSUMADDITION_2nd_time);
  }

  @Test public void 
  replicate_issued_advices_in_investors_account_when_withdrawal_post_a_partial_close() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(ENTRYPRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(ENTRYPRICE_2, TICKER2, currentTimeInMillis());
    TestRouteResult response; 
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    String firstAdviseId = responseEntity.getId();

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE_2,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2, valueOf(TOKEN_2));
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(2);
    
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    response = postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);

    waitForProcessToComplete(1);

    CloseAdvise closeAdvise = new CloseAdvise(ADVISER_USERNAME, FUND_NAME, firstAdviseId, PARTIAL_CLOSING_ALLOCATION, ACTUAL_EXITDATE, ACTUAL_EXITPRICE);
    response = post("/closeadvise", closeAdvise);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your partial exit advise request is being processed, please check the notification for the updates.");

    waitForViewToPopulate(1);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(round(userFund.getCashComponent())).isEqualTo(FUND_AVAILABLE_CASH_POST_MIRRORING_PRELUMPSUM_1);
    waitForViewToPopulate(1);

    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_3, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2, TICKER2, currentTimeInMillis());
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_3, valueOf(TOKEN_1));
    sendLivePriceToEquityActor(CURRENT_MARKET_PRICE_TICKER2_2 , valueOf(TOKEN_2));

    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 30000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);
    
     List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
     Stream<Trade>trades = orders.stream().filter(o -> o.getSide().getValue() != 0)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_PARTIALCLOSE_WITHDRAWAL);
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
    .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_PARTIALCLOSE_WITHDRAWAL);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == -1).mapToInt(order -> order.getIntegerQuantity()).sum())
        .isEqualTo(8);
    assertThat(trades.mapToInt(trade -> {
      if(trade.getOrderSide().getValue() == 1)
        return trade.getIntegerQuantity();
      else 
        return -1*trade.getIntegerQuantity();
      }).sum()).isEqualTo(79);

    CurrentUserFundDb userFund1 = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(round(userFund1.getCashComponent())).isEqualTo(FUND_AVAILABLE_CASH_POST_PARTIALCLOSE_POST_WITHDRAWAL);
  }
  
  @Test public void 
  honor_partial_withdrawal_along_with_rejection_by_ctcl() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    updateInstrumentsMapOfEquityActor(TICKER6, SYMBOL_6, INT_TOKEN_6);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER6, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    
    TestRouteResult response; 
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);
    
    IssueAdvice thirdAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID3, TICKER6, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", thirdAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());

    waitForProcessToComplete(2);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 30000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(CASH_COMPONENT_AFTER_PARTIAL_WITHDRAWAL_with_rejection);
  }

  @Test
  public void return_updated_amount_after_successful_subscription_and_after_lumpsum() throws Exception {
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    TestRouteResult response;
    Done responseEntity;
    registerUserAndActivate();

    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(2);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    
    Result getAmountResult = (Result) askAndWaitForResult(actorSelections.userRootActor(), new GetWalletMoney(SUBSCRIPTION_USER_EMAIL, VIRTUAL));
    assertThat(Double.parseDouble(getAmountResult.getMessage())).isEqualTo((INITIAL_AMOUNT - ACTUAL_INVESTMENT));
    
    Thread.sleep(10 * 1000);
    
    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME,
        ADD_LUMPSUM, VIRTUAL));

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    
    Thread.sleep(5 * 1000);

    getAmountResult = (Result) askAndWaitForResult(actorSelections.userRootActor(),
        new GetWalletMoney(SUBSCRIPTION_USER_EMAIL, VIRTUAL));
    assertThat(Double.parseDouble(getAmountResult.getMessage())).isEqualTo((INITIAL_AMOUNT - ACTUAL_INVESTMENT - ACTUAL_LUMPSUM_ADDED));
  }

  @Test public void 
  honor_partial_withdraw_from_fund_with_advises_with_low_allocation() throws Exception {
  
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, 1, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, 2, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);
    
    registerUserAndActivate();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(2);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    
    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 3000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(EXPECTED_VALUE_AFTER_WITHDRAWAL_WITH_LOW_ALLOCATION);
  }
  
  @Test public void 
  adjust_quantity_remaining_and_exited_quantity_in_case_of_partial_withdrawal() throws Exception {
  
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, 1, 1030,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    String issuedAdviseId = responseEntity.getId();
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(2);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    
    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 250000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);
    
    UserAdviseCompositeKey userAdviseCompositeKey = new UserAdviseCompositeKey(issuedAdviseId, SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME, 
        ADVISER_USERNAME, VIRTUAL);
    
    Optional<UserEquityAdvise> possibleUserEquityAdvise = userEquityAdviseRepository.findById(userAdviseCompositeKey);
    UserEquityAdvise userEquityAdvise = possibleUserEquityAdvise.orElseGet(() -> {
      return new UserEquityAdvise();
    });
    assertThat(userEquityAdvise.getTotalQuantityWithdrawn()).isEqualTo(QUANTITY_WITHDRAWN_TILL_DATE);
    assertThat(userEquityAdvise.integerQuantityYetToBeExited()).isEqualTo(userEquityAdvise.tradedEntryQuantity() - userEquityAdvise.getTotalQuantityWithdrawn());
    assertThat(userEquityAdvise.exitedQuantity()).isEqualTo(userEquityAdvise.soldTillDate() - userEquityAdvise.getTotalQuantityWithdrawn());
  }
  
  @Test public void
   check_virtual_cash_after_rebalancing() throws Exception {

    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    updateInstrumentsMapOfEquityActor(TICKER3, SYMBOL_3, TOKEN_3);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2, currentTimeInMillis());
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER3, currentTimeInMillis());

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION,
        FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME1);
    response = post("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2,
        ENTRYPRICE_2, ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = post("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(1);

    registerUserAndActivate();
    response = authenticatedGet("/subscription",
        "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + "&sipAmount="
            + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" + SUBSCRIPTION_SIP_DATE
            + "&investingMode=" + VIRTUAL.getInvestingMode());

    waitForProcessToComplete(2);

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);

    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getVirtualWalletAmount())
        .isEqualTo(VIRTUALCASH_POST_SUBSCRIPTION);
    response = patchAuthUserCommand("/subscription", new AddLumpSum(ADVISER_USERNAME, FUND_NAME, ADD_LUMPSUM, VIRTUAL));

    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
        .isEqualTo("Add Lumpsum in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getVirtualWalletAmount())
        .isEqualTo(VIRTUALCASH_POST_LUMPSUM);
    response = authenticatedGet("/withdraw", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName="
        + FUND_NAME + "&withdrawalAmount=" + 100000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());

    response = postAuthUserCommand("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Withdrawal in progress. You will receive a notification once its done.");
    waitForViewToPopulate(1);
    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getVirtualWalletAmount())
        .isEqualTo(VIRTUALCASH_POST_WITHDRAW);
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME
        + "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("UnSubscribe in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    response = authenticatedGet("/walletmoney", "investingMode=" + VIRTUAL.getInvestingMode());

    waitForViewToPopulate(1);
    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getVirtualWalletAmount())
        .isEqualTo(VIRTUALCASH_POST_EXIT);
  }
  
  private double getCurrentFundCash(String adviserIdentityId, String fundName) throws Exception {
    FundConstituentListResponse rebalancingView = (FundConstituentListResponse) askAndWaitForResult(
        actorSelections.advisersRootActor(), new GetFundRebalancingView(adviserIdentityId, fundName));
    return round(( rebalancingView.getFundConstituentList().stream()
        .filter(fundConstituent -> fundConstituent.getTicker().equals("Cash"))).findFirst().get().getAllocation());
  }

  private List<FundConstituent> getFundRebalancingView(String adviserUsername, String fundName) {
    String url = "/fundrebalancingview";
    String queryString = "fundName=" + fundName;
    
    TestRouteResult response = authenticatedAdviserGet(url, queryString);

    FundConstituentListResponse responseEntity = response.entity(unmarshaller(FundConstituentListResponse.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    
    return responseEntity.getFundConstituentList();
  }

  private double getAllocationFromFundConstituents(List<FundConstituent> listOfFundConstituents, String ticker) {
    FundConstituent fundConstituent = listOfFundConstituents.stream()
                                        .filter(constituent -> constituent.getTicker().equals(ticker))
                                        .findFirst()
                                        .get();
    return fundConstituent.getAllocation();
  }

  private long timeFromMidnight() throws ParseException {
    return todayBOD() - (9 * 60 * 60 * 1000L);
  }

  private void subscriptionProcess() throws InterruptedException {
    waitForProcessToComplete(1);
    TestRouteResult response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(2);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
          .isEqualTo("Subscription in process. You will receive a notification once its done.");
  }

  private void registerUserAndActivate() throws InterruptedException {
    TestRouteResult response;
    Done responseEntity;
    response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(SUBSCRIPTION_USER_NAME, INVESTOR_NAME, "", SUBSCRIPTION_USER_EMAIL));
    responseEntity = response.entity(unmarshaller(InvestorCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(InvestorCredentialsWithType.class);
    Thread.sleep(5*1000);
  }

  private void autoApprove(String fundName) {
   postAuthUserCommand("/userApprovalMode", new ReceivedUserAcceptanceMode(fundName, ADVISER_USERNAME, VIRTUAL, Auto));
  }
  
  private void waitForProcessToComplete(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000);
  }

  private void waitForViewToPopulate(int seconds) throws Exception {
    viewPopulater.blockingPopulateView(system, materializer);
    Thread.sleep(seconds * 1000);
  }

  private TestRouteResult enhanceAllocationOfTheAboveIssuedAdvice(String adviseId, String adviserIdentity, String fundName, String ticker,
      double extraAllocation, double entryPrice, double exitPrice, long exitTime) {
    return patch("/increaseallocation", new AddMoreAllocationToAnExistingAdvice(adviserIdentity, adviseId, fundName, 
        ticker, extraAllocation, entryPrice, exitPrice, exitTime));
  }

  private void updateRealtimePriceWithCurrentPaf() {
    universeRootActor.tell(new InstrumentPrice(ENTRYPRICE_2, currentTimeInMillis(), TICKER_FOR_REISSUE), ActorRef.noSender());
    universeRootActor.tell(new RealTimeInstrumentPrice(new InstrumentPrice(ENTRYPRICE_2, currentTimeInMillis(), valueOf(TOKEN_FOR_REISSUE))), ActorRef.noSender());
  }

  private void postCorporateEventOfBonusType() throws ParseException {
    universeRootActor.tell(new BonusEvent(TICKER_FOR_REISSUE, todayBOD(), 1, 1), ActorRef.noSender());
  }
  
  private TestRouteResult userCommandPost(String url, UnAuthenticatedUserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult postAuthUserCommand(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(POST(url).addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }

  protected TestRouteResult patchAuthUserCommand(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(PATCH(url).addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }

  private TestRouteResult authenticatedAdviserGet(String url, String queryString) {
    TestRouteResult response;
    if (queryString != "" || queryString != null) {
      response = route().run(GET(url + "?" + queryString)
          .addHeader(RawHeader.create("username", ADVISER_USERNAME)));
    } else {
      response = route().run(GET(url).addHeader(RawHeader.create("username", ADVISER_USERNAME)));
    }
    return response;
  }

  private TestRouteResult authenticatedGet(String url, String queryString) {
    TestRouteResult response;
    if (queryString != "" || queryString != null) {
      response = route().run(GET(url + "?" + queryString)
          .addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME)));
    } else {
      response = route().run(GET(url).addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME)));
    }
    return response;
  }

  private TestRouteResult authenticatedDelete(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(DELETE(url).addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }

  protected TestRouteResult post(String url, UnAuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  protected TestRouteResult post(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  protected TestRouteResult delete(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(DELETE(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult patch(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(PATCH(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRoute route() {
    return testRoute(service.appRoute());
  }

  private void sendBODPriceToMFActor(double nav, String mfticker) {
        universeRootActor.tell(new PriceWithPaf(new InstrumentPrice(nav, currentTimeInMillis(), mfticker),
            Pafs.withCumulativePaf(1.)), ActorRef.noSender());
      }

  private void updateInstrumentsMapOfMFActor(String mf_ticker, String symbol1, String mfScheme , String schemeCode, 
                                  String minInvestment, String minMultiplier, String schemeType, String exitLoad, String rta) {
    Set<MutualFundInstrument> instruments = new HashSet<MutualFundInstrument>();
    instruments.add(new MutualFundInstrument(mf_ticker, mfScheme, schemeCode, minInvestment, minMultiplier, schemeType, 
                                             exitLoad, rta));
    universeRootActor.tell(new UpdateMFInstruments(instruments), ActorRef.noSender());
  }
  
}