package com.wt.api.write;

import static akka.actor.ActorRef.noSender;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.not.wt.pkg.to.override.main.config.ActiveInstrumentTrackerTestAppConfiguration;
import com.wt.ctcl.facade.CtclBroadcast;
import com.wt.domain.FundConstituent;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.SendSubscriptionForInstruments;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import commons.Retry;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ActiveInstrumentTrackerTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class ActiveInstrumentTrackerShould {

  private @Autowired CtclBroadcast broadcast;
  private @Autowired ActorSystem system;
  private ActorRef activeInstrumentTrackerActor;
  private ActorRef universeRootActor;
  private Map<String, Boolean> tokenToSubscriptionStatus = new ConcurrentHashMap<String, Boolean>();

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Rule
  public Retry retry = new Retry(5);

  @Before
  public void init() throws ParseException, IOException {
    environmentVariables.set("WT_BROADCAST_OPEN", "true");
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    updateInstrumentsMapOfEquityActor("INHDFC-NSE-EQ","HDFC",1333);
    updateInstrumentsMapOfEquityActor("INSBI-NSE-EQ","SBIN",22);
    activeInstrumentTrackerActor = system.actorOf(SpringExtProvider.get(system).props("ActiveInstrumentTrackerActor"),
        "active-instrument-tracker");
    Mockito.doAnswer(invocation -> {
      String token = (String) invocation.getArguments()[0];
      tokenToSubscriptionStatus.put(token, true);
      return null;
    }).when(broadcast).addSubscription(Mockito.anyString(), Mockito.anyString());

    Mockito.doAnswer(invocation -> {
      String token = (String) invocation.getArguments()[0];
      tokenToSubscriptionStatus.remove(token);
      return null;
    }).when(broadcast).removeSubscription(Mockito.anyString(), Mockito.anyString());

  }

  @Test
  public void check_subscribe_to_token_before_and_after_shutting_down_service() throws Exception {
    List<FundConstituent> fundConstituents = new ArrayList<>();
    fundConstituents.add(new FundConstituent("adviseId", "fundName", "adviserUserName", "INSBI-NSE-EQ", "SBIN", 10.0,
        PriceWithPaf.noPrice("INSBI-NSE-EQ"), PriceWithPaf.noPrice("INSBI-NSE-EQ")));
    fundConstituents.add(new FundConstituent("adviseId", "fundName", "adviserUserName", "INHDFC-NSE-EQ", "HDFC", 10.0,
        PriceWithPaf.noPrice("INHDFC-NSE-EQ"), PriceWithPaf.noPrice("INHDFC-NSE-EQ")));
    
    activeInstrumentTrackerActor.tell(new SendSubscriptionForInstruments(fundConstituents), ActorRef.noSender());
    waitForProcessToComplete(10);
    assertThat(tokenToSubscriptionStatus.get("1333")).isEqualTo(true);
    assertThat(tokenToSubscriptionStatus.get("22")).isEqualTo(true);
    activeInstrumentTrackerActor.tell(new EODCompleted(), ActorRef.noSender());
    broadcast.removeSubscription("1333", "NSE");
    broadcast.removeSubscription("22", "NSE");
    activeInstrumentTrackerActor.tell(PoisonPill.getInstance(), noSender());
    activeInstrumentTrackerActor = system.actorOf(SpringExtProvider.get(system).props("ActiveInstrumentTrackerActor"),
        "active-instrument-tracker1");
    waitForProcessToComplete(8);
    assertThat(tokenToSubscriptionStatus.get("1333")).isEqualTo(true);
    assertThat(tokenToSubscriptionStatus.get("22")).isEqualTo(true);

  }

  private void waitForProcessToComplete(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000);
  }
  
  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());
  }

}