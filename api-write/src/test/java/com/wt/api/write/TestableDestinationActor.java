package com.wt.api.write;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import akka.actor.AbstractActor;
import scala.Option;

@Component("TestableDestinationActor")
@Scope("prototype")
public class TestableDestinationActor extends AbstractActor {
  @Override
  public Receive createReceive() {
    return receiveBuilder().match(Option.class, cmd -> {
      sender().tell(Option.apply(cmd.get()+""), self());
    }).build();
  }
}
