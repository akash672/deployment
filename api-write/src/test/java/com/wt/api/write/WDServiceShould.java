package com.wt.api.write;

import static akka.http.javadsl.marshallers.jackson.Jackson.unmarshaller;
import static akka.http.javadsl.model.HttpRequest.DELETE;
import static akka.http.javadsl.model.HttpRequest.GET;
import static akka.http.javadsl.model.HttpRequest.PATCH;
import static akka.http.javadsl.model.HttpRequest.POST;
import static akka.http.javadsl.model.MediaTypes.APPLICATION_JSON;
import static akka.http.javadsl.model.StatusCodes.BAD_REQUEST;
import static akka.http.javadsl.model.StatusCodes.NOT_ACCEPTABLE;
import static akka.http.javadsl.model.StatusCodes.OK;
import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;
import static com.amazonaws.util.json.Jackson.toJsonString;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.RiskLevel.HIGH;
import static com.wt.domain.write.events.Exists.exists;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.isTradingHoliday;
import static com.wt.utils.DateUtils.isWeekend;
import static com.wt.utils.DateUtils.lastDayEOD;
import static com.wt.utils.DateUtils.todayBOD;
import static com.wt.utils.DateUtils.todayExecutionBOD;
import static com.wt.utils.DateUtils.todayExecutionEOD;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.not.wt.pkg.to.override.main.config.APISpecificTestAppConfiguration;
import com.wt.config.utils.WDProperties;
import com.wt.domain.Advise;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.CurrentFundDb;
import com.wt.domain.Fund;
import com.wt.domain.InvestingMode;
import com.wt.domain.InvestorApproval;
import com.wt.domain.Notification;
import com.wt.domain.NotificationCompositeKey;
import com.wt.domain.NotificationTypes;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.repo.FundAdviseRepository;
import com.wt.domain.repo.FundRepository;
import com.wt.domain.write.commands.AddMoreAllocationToAnExistingAdvice;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.AuthenticatedUserCommand;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateFundAttribute;
import com.wt.domain.write.events.AdviserCredentialsWithType;
import com.wt.domain.write.events.BonusEvent;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Exists;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceClosed;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.utils.CoreDBTest;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;
import com.wt.utils.NotificationFacade;
import com.wt.utils.OTPUtils;
import com.wt.utils.ViewPopulater;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import akka.stream.ActorMaterializer;
import commons.Retry;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = APISpecificTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class WDServiceShould extends JUnitRouteTest {

  private static final double CURRENT_MARKET_PRICE = 1040d;
  private static final long serialVersionUID = 1L;
  private static final TimeZone INDIA_TIME_ZONE = TimeZone.getTimeZone("Asia/Kolkata");
  private static final String TICKER = "INA-NSE-EQ";
  private static final String TICKER_1 = "IN1-NSE-EQ";
  private static final String FUND_NAME = "WeeklyPicks";
  private static final String FUND_DESCRIPTION = FUND_NAME + "Description";
  private static final String FUND_THEME = "Theme";
  private static final double FUND_MIN_SIP = 6500;
  private static final double FUND_MIN_LUMPSUM = 22500;
  private static final String FUND_RISK_LEVEL = "MEDIUM";
  private static final String FUND_DESCRIPTION_NEW = FUND_NAME + "NewDescription";
  private static final String FUND_THEME_NEW = "";
  private static final double FUND_MIN_SIP_NEW = 0.0;
  private static final double FUND_MIN_LUMPSUM_NEW = 225000;
  private static final String FUND_RISK_LEVEL_NEW = "HIGH";
  private static final String ADVISER_IDENTITY = "swapnil";
  private static final RegisterAdviserFromUserPool ADVISER = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi",
      "swapnil@bpwealth.com", "123publicId", "Fort Capital", "");
  private static final long STARTDATE = 1388082600000L;
  private static final long ENDDATE = 3471186600000L;
  private static final long STARTDATE_NEW = 1388082600000L;
  private static final long ENDDATE_NEW = 3471186600000L;
  private static final String DUMMY_DEVICE_TOKEN = "dummy-device-token";
  private static final String DUMMY_NOTIFICATION_TYPE = "Investment";
  private static final String DUMMY_NOTIFICATION_TITLE = "Fund Subscription";
  private static final String DUMMY_NOTIFICATION_MESSAGE = "Successfully subscribed to WeeklyPicks";
  private static final String DUMMY_USER_NAME = "test";
  private static final String DUMMY_USER_EMAIL = "test@gmail.com";
  private static final double DUMMY_LUMPSUM = 8000;
  private static final double DUMMY_SIPAMOUNT = 3500;
  private static final String DUMMY_SIP_DATE = "dummy-notification-sip-date";
  private static final String DUMMY_WEALTHCODE = "dummy-notification-wealthcode";
  private static final String INVESTOR_NAME = "firstname lastname";
  private static final double NEW_MARKET_ORDER_PRICE = 550.;
  private static final double NEW_AVERAGE_PRICE = 540.;
  private static final double ADVISE_NEW_ALLOCATION = 20.;
  private static final double ADVISE_INITIAL_ALLOCATION = 10.;
  private static final double PAF_POST_BONUS_EVENT = 2.;
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;

  private @Autowired ActorSystem system;
  private @Autowired ViewPopulater viewPopulater;
  private @Autowired FundAdviseRepository adviseRepository;
  private @Autowired FundRepository fundRepository;
  private @Autowired WDService service;
  private @Autowired CoreDBTest dbTest;
  private @Autowired ActorMaterializer materializer;
  private @Autowired NotificationFacade snsFacade;
  private @Autowired OTPUtils otpGeneratorMock;
  private @Autowired @Qualifier("JournalProvider") JournalProvider journalProvider;
  private @Autowired @Qualifier("CognitoUserPoolForInvestor") AWSCognitoIdentityProvider investorUserPoolMock;
  private ActorRef universeRootActor;

  @Override
  public FiniteDuration awaitDuration() {
    return FiniteDuration.apply(1, TimeUnit.MINUTES);
  }

  @Before
  public void setupDB() throws Exception {
    dbTest.createSchema();
    service.createActors();
    environmentVariables.set("WT_MARKET_OPEN", "true");
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER, lastDayEOD(currentTimeInMillis()));
    updateInstrumentsMapOfEquityActor(TICKER, "ABC", 130);
    updateInstrumentsMapOfEquityActor(TICKER_1, "EFG", 122);
    doReturn(WDProperties.DUMMY_HASHCODE_FOR_USER_ACTIVATION).when(otpGeneratorMock).getUniqueHashCode();
    HashMap<String, String> additionalAttributes = new HashMap<String, String>();
    additionalAttributes.put("author", ADVISER_IDENTITY);
    additionalAttributes.put("fund_name", FUND_NAME);
    additionalAttributes.put("title", DUMMY_NOTIFICATION_TITLE);
    additionalAttributes.put("type", DUMMY_NOTIFICATION_TYPE);
    doNothing().when(snsFacade).sendNotification(DUMMY_DEVICE_TOKEN, DUMMY_NOTIFICATION_MESSAGE, additionalAttributes);
    
    Mockito.when(investorUserPoolMock.adminGetUser(Mockito.any(AdminGetUserRequest.class)))
    .thenThrow(new UserNotFoundException("This user is not registered."));
    
    Mockito.when(investorUserPoolMock.adminCreateUser(Mockito.any(AdminCreateUserRequest.class)))
    .thenAnswer((args) -> {
      AdminCreateUserRequest request = (AdminCreateUserRequest) args.getArguments()[0];
      AdminCreateUserResult result = new AdminCreateUserResult()
          .withUser(new UserType()
                      .withAttributes(request.getUserAttributes())
                      .withEnabled(true)
                      .withUsername(request.getUsername())
                      .withUserCreateDate(new Date(DateUtils.todayBOD()))
                      .withUserLastModifiedDate(new Date(DateUtils.todayBOD())));
      return result;
    });
  }

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
  
  @Rule
  public Retry retry = new Retry(5);

  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    try {
      deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private void sendEODPriceToEquityActor(double currentMarketPrice, String token, long time) {
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(currentMarketPrice, time, token), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }
  
  @Test
  public void issue_advise_from_UI_and_populate_view() throws Exception {

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    registerUserAndActivate();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    IssueAdvice command = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 100.0, CURRENT_MARKET_PRICE, 102, 102);

    response = post("/issueadvise", command);
    
    waitForProcessToComplete(2);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response.assertStatusCode(OK).assertMediaType("application/json");
    responseEntity = response.entity(unmarshaller(Done.class));
    
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 50.0, 12, 102.0));
    
    waitForProcessToComplete(2);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForViewToPopulate(1);
    Advise advise = adviseRepository.findById(responseEntity.getId()).orElse(null);
    assertThat(advise).isNotNull();

  }

  private void registerAdviserProcess() {
    TestRouteResult response = post("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  @Test
  public void successfully_enhance_the_allocation_of_an_existing_advice() throws Exception {

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    IssueAdvice command = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, ADVISE_INITIAL_ALLOCATION, CURRENT_MARKET_PRICE, 102, 102);

    response = post("/issueadvise", command);
    
    waitForProcessToComplete(2);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    waitForViewToPopulate(1);
    
    Advise advise = adviseRepository.findById(responseEntity.getId()).orElse(new Advise());
    CurrentFundDb fund = fundRepository.findById(FUND_NAME).orElse(new CurrentFundDb());
    
    assertThat(advise.getAllocation()).isEqualTo(ADVISE_INITIAL_ALLOCATION);
    assertThat(fund.getCash()).isEqualTo(100. - ADVISE_INITIAL_ALLOCATION);
    
    postACorporateEventOfBonusType();
    
    waitForProcessToComplete(2);
    
    getRealtimePriceWithUpdatePaf();
    
    response = increaseAllocationOfTheAboveIssuedAdvise(responseEntity);
    
    waitForProcessToComplete(2);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForViewToPopulate(1);
    
    advise = adviseRepository.findById(responseEntity.getId()).orElse(new Advise());
    fund = fundRepository.findById(FUND_NAME).orElse(new Fund());
    
    assertThat(advise.getAllocation()).isEqualTo(ADVISE_INITIAL_ALLOCATION + ADVISE_NEW_ALLOCATION);
    assertThat(fund.getCash()).isEqualTo(100. - (ADVISE_INITIAL_ALLOCATION + ADVISE_NEW_ALLOCATION));
    double cumulativeEntryPrice = 0;
    double cumulativeEntryPaf = 0;
    if (!isWeekend(Calendar.getInstance(INDIA_TIME_ZONE)) && !isTradingHoliday()) {
      cumulativeEntryPrice = advise.getEntryPrice().getPrice().getPrice();
      cumulativeEntryPaf = advise.getEntryPrice().getCumulativePaf();
    } else {
      cumulativeEntryPrice = (advise.getEntryPrice().getPrice().getPrice() * 3 / 4) + 5;
      cumulativeEntryPaf = (advise.getEntryPrice().getCumulativePaf() + 1);
    }
    assertThat(cumulativeEntryPrice).isEqualTo(NEW_AVERAGE_PRICE);
    assertThat(cumulativeEntryPaf).isEqualTo(PAF_POST_BONUS_EVENT);
  }

  @Test
  public void floatFundAndUpdateAttributesViewTest() throws Exception {

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    UpdateFundAttribute updateFundAttibute = new UpdateFundAttribute(ADVISER_IDENTITY, FUND_NAME, STARTDATE_NEW, ENDDATE_NEW,
        FUND_DESCRIPTION_NEW, FUND_THEME_NEW, FUND_MIN_SIP_NEW, FUND_MIN_LUMPSUM_NEW, FUND_RISK_LEVEL_NEW);
    response = post("/updateFundAttribute", updateFundAttibute);
    
    waitForViewToPopulate(3);
    
    Iterable<CurrentFundDb> funds = fundRepository.findAll();
    assertThat(funds).hasSize(1);
    assertThat(funds.iterator().next().getTheme()).isEqualTo("Theme");
    assertThat(funds.iterator().next().getStartDate()).isEqualTo(1388082600000L);
    assertThat(funds.iterator().next().getEndDate()).isEqualTo(3471186600000L);
    assertThat(funds.iterator().next().getDescription()).isEqualTo("WeeklyPicksNewDescription");
    assertThat(funds.iterator().next().getMinSIP()).isEqualTo(6500.0);
    assertThat(funds.iterator().next().getMinLumpSum()).isEqualTo(225000.0);
    assertThat(funds.iterator().next().getRiskLevel()).isEqualTo(HIGH);

  }

  @Test
  public void issue_advise_from_UI_and_populate_view_and_check_Success_Notification() throws Exception {

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();

    registerUserAndActivate();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    IssueAdvice command = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 100.0, CURRENT_MARKET_PRICE, 102, 102);
    response = post("/issueadvise", command);
    
    waitForProcessToComplete(2);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response.assertStatusCode(OK).assertMediaType("application/json");
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 50.0, 12, 102.0));
    
    waitForProcessToComplete(2);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForViewToPopulate(1);
    
    Advise advise = adviseRepository.findById(responseEntity.getId()).orElse(null);
    assertThat(advise).isNotNull();

    Mockito.verify(snsFacade, times(2)).sendNotification(Mockito.anyString(), Mockito.anyString(), Mockito.any());
  }

  @Test
  public void close_advise_from_UI_and_successfully_executed() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    
    waitForViewToPopulate(1);
    registerUserAndActivate();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForViewToPopulate(1);
    
    IssueAdvice command = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 100.0, 100.0, 102, 102);

    response = post("/issueadvise", command);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    waitForProcessToComplete(2);
    
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 50.0, 12, CURRENT_MARKET_PRICE));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForViewToPopulate(1);
    assertThat(journalProvider.fetchEventsByPersistenceId(FUND_NAME)
        .stream().filter(evt -> evt instanceof FundAdviceClosed)
        .map(evt -> (FundAdviceClosed) evt)
        .findFirst().isPresent()).isTrue();
  }

  private void registerUserAndActivate() throws InterruptedException {
    userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(DUMMY_USER_NAME.split("@")[0], INVESTOR_NAME, "", DUMMY_USER_EMAIL));
  }
  
  @Test
  public void issue_advise_from_UI_and_fail_if_advise_issued_in_none_market_hours() throws Exception {
    TestRouteResult response;
    registerAdviserProcess();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    response.assertStatusCode(OK).assertMediaType("application/json");
    environmentVariables.set("WT_MARKET_OPEN", "false");
    assertEquals("false", System.getenv("WT_MARKET_OPEN"));
    IssueAdvice command = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 100.0, CURRENT_MARKET_PRICE, 102, 102);
    
    response = post("/issueadvise", command);
    Failed responseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(StatusCodes.PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Failed.class);
  }

  @Test
  public void issue_advise_and_populate_view() throws Exception {
    IssueAdvice command = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 100.0, 100, 10, 102, 102);

    TestRouteResult response;
    Done responseEntity;
    registerAdviserProcess();
    waitForProcessToComplete(4);
    registerUserAndActivate();
    waitForProcessToComplete(4);
    
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_LUMPSUM + "&lumpSumAmount=" + DUMMY_SIPAMOUNT + "&sipDate=" +
        DUMMY_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForViewToPopulate(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, DUMMY_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION,  ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));

    waitForViewToPopulate(1);

    responseEntity = response.entity(unmarshaller(Done.class));

    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    response = post("/issueadvise", command);
    
    waitForProcessToComplete(2);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertNotificationisReceived(1, NotificationTypes.Investment);

    response.assertStatusCode(OK).assertMediaType("application/json");
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 50.0, 12, 102.0));
    
    waitForProcessToComplete(3);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    waitForViewToPopulate(0);
    assertThat(adviseRepository.findById(responseEntity.getId()).orElse(null)).isNotNull();
  }

  @Test
  public void send_notification_when_advise_is_issued() throws Exception {
    IssueAdvice command = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 100.0, 100, 10, 102, 102);

    TestRouteResult response;
    registerAdviserProcess();

    registerUserAndActivate();

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    response.assertStatusCode(OK).assertMediaType("application/json");

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_LUMPSUM + "&lumpSumAmount=" + DUMMY_SIPAMOUNT + "&sipDate=" +
        DUMMY_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForViewToPopulate(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, DUMMY_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    response.assertStatusCode(OK).assertMediaType("application/json");
    Done responseEntity = response.entity(unmarshaller(Done.class));
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    response = post("/issueadvise", command);
    
    waitForProcessToComplete(5);
    
    waitForViewToPopulate(1);
    
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertNotificationisReceived(1, NotificationTypes.Investment);
  }
  
  public void assertNotificationisReceived(int noOfTimes, NotificationTypes type) {
    HashMap<String, String> additionalAttributes = new HashMap<String, String>();
    additionalAttributes.put("fund_name", FUND_NAME);
    additionalAttributes.put("author", ADVISER_IDENTITY);
    additionalAttributes.put("title", DUMMY_NOTIFICATION_TITLE);
    additionalAttributes.put("type", type.name());

    Notification notificationDomain = createNotificationDomain(ADVISER_IDENTITY, FUND_NAME, DUMMY_USER_NAME,
        DUMMY_NOTIFICATION_TITLE, DUMMY_NOTIFICATION_MESSAGE, type, VIRTUAL);
    String recordString = (new Gson().toJson(notificationDomain).toString());
    additionalAttributes.put("recordString", recordString);
    additionalAttributes.put("id", DUMMY_USER_NAME);
    additionalAttributes.put("userType", "User");
    additionalAttributes.put("investingMode", VIRTUAL.name());
    additionalAttributes.put("tablename", "notification");
    
    //TODO: fix this test for checking timestamp values in the map attributes 
//    Mockito.verify(snsFacade, Mockito.times(noOfTimes)).sendNotification(DUMMY_DEVICE_TOKEN, DUMMY_NOTIFICATION_MESSAGE,
//        additionalAttributes);
  }
  
  private Notification createNotificationDomain(String adviserUsername, String fundName, String username, String title,
      String message, NotificationTypes type, InvestingMode investingMode) {
    Map<String, String> notificationMap = Maps.newHashMap();
    notificationMap.put("author", adviserUsername);
    notificationMap.put("fund_name", fundName);
    notificationMap.put("title", title);
    notificationMap.put("type", type.name());
    if(investingMode != null)
      notificationMap.put("investingMode", investingMode.getInvestingMode());
    else
      notificationMap.put("investingMode", "NONE");
    notificationMap.put("message", message);
    
    NotificationCompositeKey notificationCompositeKey = new NotificationCompositeKey();
    notificationCompositeKey.setUsername(username);
    notificationCompositeKey.setTimestamp(currentTimeInMillis() - 9603);
    Notification notification = new Notification(notificationCompositeKey, notificationMap);
    return notification;
  }
  

  private RegisterAdviserFromUserPool registerAdviser() {
    return ADVISER;
  }

  @Test
  public void return_200_if_execution_bod_is_calculated_correctly() throws ParseException {
    long todayExecutionBOD = todayExecutionBOD();
    Calendar instance = Calendar.getInstance(INDIA_TIME_ZONE);
    instance.set(instance.get(Calendar.YEAR), instance.get(Calendar.MONTH), instance.get(Calendar.DATE), 9, 15, 0);
    assertThat(todayExecutionBOD).isEqualTo((instance.getTimeInMillis()/1000)*1000);
  }
  
  @Test
  public void return_200_if_execution_eod_is_calculated_correctly() throws ParseException {
    long todayExecutionEOD = todayExecutionEOD();
    Calendar instance = Calendar.getInstance(INDIA_TIME_ZONE);
    instance.set(instance.get(Calendar.YEAR), instance.get(Calendar.MONTH), instance.get(Calendar.DATE), 15, 24, 36);
    assertThat(todayExecutionEOD).isEqualTo((instance.getTimeInMillis()/1000)*1000);
  }
  
  @Test
  public void return_400_bad_request_for_duplicate_adviser() throws InterruptedException {
    TestRouteResult response = post("/registeradviserfromuserpool", registerAdviser());

    response = post("/registeradviserfromuserpool", registerAdviser());

    assertThat(response.status()).isEqualTo(NOT_ACCEPTABLE);
    assertThat(response.entity(unmarshaller(Exists.class))).isEqualTo(exists(ADVISER.getAdviserUsername()));
  }

  @Test
  public void return_400_bad_request_for_duplicate_fund() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    assertThat(response.status()).isEqualTo(NOT_ACCEPTABLE);
    assertThat(response.entity(unmarshaller(Exists.class))).isEqualTo(exists(FUND_NAME));
  }

  @Test
  public void return_400_bad_request_for_allocation_more_than_100_pct() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 101.0, 100, 10, 102, 102));
    
    waitForProcessToComplete(2);
    
    assertThat(response.status()).isEqualTo(PRECONDITION_FAILED);
    Failed issueAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(issueAdviseResponse).isInstanceOf(Failed.class);
  }

  @Test
  public void return_400_bad_request_for_negative_issue_allocation() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, -10.0, 100, 10, 102, 102));
    assertThat(response.status()).isEqualTo(PRECONDITION_FAILED);
    
    waitForProcessToComplete(2);
    
    Failed issueAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(issueAdviseResponse).isInstanceOf(Failed.class);
  }

  @Test
  public void return_400_bad_request_for_negative_close_allocation() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 100.0, 100, 10, 102, 102));
    
    waitForProcessToComplete(2);
    
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), -59.70, 102, 102));
    
    waitForProcessToComplete(2);
    
    assertThat(response.status()).isEqualTo(BAD_REQUEST);
    Failed closeAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(closeAdviseResponse).isInstanceOf(Failed.class);
  }

  @Test
  public void return_400_bad_request_for_negative_entry_price() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 10.0, -100, 10, 102, 102));
    
    waitForProcessToComplete(2);
    
    assertThat(response.status()).isEqualTo(PRECONDITION_FAILED);
    Failed issueAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(issueAdviseResponse).isInstanceOf(Failed.class);
  }

  @Test
  public void return_400_bad_request_for_negative_exit_price() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 10.0, 100, 10, 102, 102));
    
    waitForProcessToComplete(2);
    
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 8.0, 102, -102));
    
    waitForProcessToComplete(2);
    
    assertThat(response.status()).isEqualTo(BAD_REQUEST);
    Failed closeAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(closeAdviseResponse).isInstanceOf(Failed.class);
  }

  @Test
  public void return_400_bad_request_for_total_allocation_more_than_100_pct() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 59.69, 100, 10, 102, 102));
    
    waitForProcessToComplete(2);
    
    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 40.32, 100, 10, 102, 102));
    
    waitForProcessToComplete(2);
    
    assertThat(response.status()).isEqualTo(PRECONDITION_FAILED);
    Failed issueAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(issueAdviseResponse).isInstanceOf(Failed.class);
  }

  @Test
  public void return_400_bad_request_for_close_allocation_more_than_open_allocation() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 59.69, 100, 10, 102, 102));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    waitForProcessToComplete(2);
    
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 59.70, 102, 102));
    
    waitForProcessToComplete(2);
    
    assertThat(response.status()).isEqualTo(BAD_REQUEST);
    Failed closeAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(closeAdviseResponse).isInstanceOf(Failed.class);
  }

  @Test
  public void return_400_bad_request_for_subsequent_close_allocation_more_than_open_allocation()
      throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 59.69, 100, 10, 102, 102));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    waitForProcessToComplete(2);
    
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 59.69, 102, 102));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForProcessToComplete(2);
    
    response = post("/closeadvise",
        new CloseAdvise(ADVISER_IDENTITY, FUND_NAME, responseEntity.getId(), 59.69, 102, 102));
    assertThat(response.status()).isEqualTo(BAD_REQUEST);

    Failed closeAdviseResponse = response.entity(unmarshaller(Failed.class));
    assertThat(closeAdviseResponse).isInstanceOf(Failed.class);
    
  }

  @Test
  public void return_400_when_unsubscribing_post_market_hours() throws Exception {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + DUMMY_LUMPSUM + "&lumpSumAmount=" + DUMMY_SIPAMOUNT + "&sipDate=" +
        DUMMY_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForViewToPopulate(1);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, DUMMY_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION,  ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));

    waitForViewToPopulate(1);
    
    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_IDENTITY + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, DUMMY_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));

    Failed unsubscribeResponse = response.entity(unmarshaller(Failed.class));
    assertThat(unsubscribeResponse).isInstanceOf(Failed.class);

  }

  @Test
  public void return_200_ok_request_for_total_allocation_equal_to_100_pct() throws InterruptedException {
    TestRouteResult response;
    registerAdviserProcess();
    response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 59.69, 100, 10, 102, 102));
    
    waitForProcessToComplete(2);
    
    response = post("/issueadvise", new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, TICKER, 40.31, 100, 10, 102, 102));
    
    waitForProcessToComplete(2);

    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }
    
  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());

  }
  
  private void waitForProcessToComplete(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000);
  }

  private void postACorporateEventOfBonusType() throws ParseException, InterruptedException {
    universeRootActor.tell(new BonusEvent(TICKER, todayBOD(), 1, 1), ActorRef.noSender());
  }
  
  private void getRealtimePriceWithUpdatePaf() {
    universeRootActor.tell(new InstrumentPrice(NEW_MARKET_ORDER_PRICE, currentTimeInMillis(), TICKER), ActorRef.noSender());
  }
  
  private TestRouteResult increaseAllocationOfTheAboveIssuedAdvise(Done responseEntity)
      throws ParseException, IOException {
    return patch("/increaseallocation",
        new AddMoreAllocationToAnExistingAdvice(ADVISER_IDENTITY, responseEntity.getId(), FUND_NAME, TICKER, ADVISE_NEW_ALLOCATION, NEW_MARKET_ORDER_PRICE, 102, 102));
  }

  protected TestRouteResult post(String url, UnAuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult post(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  protected TestRouteResult patch(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(PATCH(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  protected TestRouteResult userCommandPost(String url, AuthenticatedUserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRouteResult userCommandPost(String url, UnAuthenticatedUserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult postAuthUserCommand(String url, InvestorApproval command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(POST(url).addHeader(RawHeader.create("username", DUMMY_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }
  
  private TestRouteResult authenticatedGet(String url, String queryString) {
    TestRouteResult response;
    if (queryString != "" || queryString != null) {
      response = route().run(GET(url + "?" + queryString)
          .addHeader(RawHeader.create("username", DUMMY_USER_NAME)));
    } else {
      response = route().run(GET(url).addHeader(RawHeader.create("username", DUMMY_USER_NAME)));
    }
    return response;
  }
  
  private TestRouteResult authenticatedDelete(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(DELETE(url).addHeader(RawHeader.create("username", DUMMY_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }

  protected void waitForViewToPopulate(int seconds) throws Exception {
    Thread.sleep(seconds * 1000);
    viewPopulater.blockingPopulateView(system, materializer);
  }

  private TestRoute route() {
    return testRoute(service.appRoute());
  }

}
