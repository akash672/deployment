package com.wt.api.write;

import static akka.http.javadsl.marshallers.jackson.Jackson.unmarshaller;
import static akka.http.javadsl.model.HttpRequest.DELETE;
import static akka.http.javadsl.model.HttpRequest.GET;
import static akka.http.javadsl.model.HttpRequest.POST;
import static akka.http.javadsl.model.MediaTypes.APPLICATION_JSON;
import static akka.http.javadsl.model.StatusCodes.BAD_REQUEST;
import static akka.http.javadsl.model.StatusCodes.FORBIDDEN;
import static akka.http.javadsl.model.StatusCodes.NOT_ACCEPTABLE;
import static akka.http.javadsl.model.StatusCodes.NOT_FOUND;
import static akka.http.javadsl.model.StatusCodes.OK;
import static akka.http.javadsl.model.StatusCodes.PRECONDITION_FAILED;
import static akka.http.javadsl.model.StatusCodes.UNAUTHORIZED;
import static akka.http.scaladsl.model.StatusCodes.Forbidden;
import static akka.pattern.Patterns.ask;
import static com.amazonaws.util.json.Jackson.toJsonString;
import static com.wt.domain.AdviseType.Issue;
import static com.wt.domain.BrokerName.MOCK;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.domain.UserAcceptanceMode.Auto;
import static com.wt.domain.UserAcceptanceMode.Manual;
import static com.wt.domain.UserResponseType.Approve;
import static com.wt.domain.write.events.Exists.exists;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.todayBOD;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.util.Files;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.not.wt.pkg.to.override.main.config.APISpecificTestAppConfiguration;
import com.wt.config.utils.WDProperties;
import com.wt.domain.ApprovalResponse;
import com.wt.domain.BrokerName;
import com.wt.domain.CurrentUserDb;
import com.wt.domain.CurrentUserFundDb;
import com.wt.domain.Fund;
import com.wt.domain.FundSubscription;
import com.wt.domain.InvestorApproval;
import com.wt.domain.Order;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.ReceivedUserApprovalResponse;
import com.wt.domain.RiskLevel;
import com.wt.domain.Trade;
import com.wt.domain.UserAdviseCompositeKey;
import com.wt.domain.UserAdviseCompositeKeyWithDate;
import com.wt.domain.UserEquityAdvise;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.UserFundCompositeKeyWithDate;
import com.wt.domain.UserResponseParameters;
import com.wt.domain.Wallet;
import com.wt.domain.WealthCode;
import com.wt.domain.repo.OrdersRepository;
import com.wt.domain.repo.UserEquityAdviseRepository;
import com.wt.domain.repo.UserFundRepository;
import com.wt.domain.repo.UserRepository;
import com.wt.domain.write.RealTimeInstrumentPrice;
import com.wt.domain.write.UpdateKycCommand;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.DeviceToken;
import com.wt.domain.write.commands.ExecutePostMarketUserCommands;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.GetUserRoleStatus;
import com.wt.domain.write.commands.GetWalletMoney;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.ReceivedUserAcceptanceMode;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.SubmitSecurityAnswers;
import com.wt.domain.write.commands.SubmitUserAnswers;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.commands.UpdateRecommendedEntryPrice;
import com.wt.domain.write.events.AdviserCredentialsWithType;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.Exists;
import com.wt.domain.write.events.Failed;
import com.wt.domain.write.events.FundAdviceIssued;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.FundAdviseListResponse;
import com.wt.domain.write.events.FundFloated;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.InvestorCredentialsWithType;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.MarginRequested;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.RecommendedEntryPriceUpdated;
import com.wt.domain.write.events.UserCurrentRoleStatus;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.domain.write.events.UserInvestmentStatus;
import com.wt.domain.write.events.UserSecurityQuestionsList;
import com.wt.utils.CoreDBTest;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;
import com.wt.utils.NotificationFacade;
import com.wt.utils.OTPUtils;
import com.wt.utils.ViewPopulater;
import com.wt.utils.akka.WDActorSelections;
import com.wt.utils.aws.SESEmailUtil;
import com.wt.utils.email.templates.wealthdesk.OTPEmail;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import akka.persistence.fsm.PersistentFSM.StateChangeEvent;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import commons.Retry;
import eventstore.ProjectionsClient;
import scala.Option;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = APISpecificTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class WDAuthenticationShould extends JUnitRouteTest {
  private static final String COMPANY_NAME = "COMPANY NAME";
  private static final long serialVersionUID = 1L;
  private static final String INVESTOR_USERNAME = "test@gmail.com";
  private static final String INVESTOR_EMAIL = "emailID@gmail.com";
  private static final String INVESTOR_EMAIL2 = "emailID2@gmail.com";
  private static final String USER_EMAIL_DIFFERENT_DOMAIN = "test@gmail.tech";
  private static final String USER_PHONENUMBER = "8080621831";
  private static final String USER_PHONENUMBER1 = "8080621832";
  private static final String USER_PASSWORD = "test@123";
  private static final String TICKER = "INTICKER-NSE-EQ";
  private static final String DEVICE_TOKEN = "DEVICETOKEN";
  private static final String FUND_NAME = "WeeklyPicks";
  private static final String FUND2_NAME = "WeeklyPicks2";
  private static final String INVALID_FUND_NAME = "skciPylkeeW";
  private static final String FUND_DESCRIPTION = FUND_NAME + "Description";
  private static final String FUND_THEME = "Theme";
  private static final double FUND_MIN_SIP = 6500;
  private static final double FUND_MIN_LUMPSUM = 22500;
  private static final double FUND_EXCESS_LUMPSUM = 457000;
  private static final String FUND_RISK_LEVEL = "MEDIUM";
  private static final String ADVISER_USERNAME = "swapnil";
  private static final String ADVISER_NAME = "Swapnil Joshi";
  private static final String ADVISER_EMAIL = "Swapnil@bpwealth.com";
  private static final String ADVISER_PUBLIC_ID = "SomePublicId";
  private static final long STARTDATE = 1388082600000L;
  private static final long ENDDATE = 3471186600000L;
  private static final String INVALIDUSEREMAIL = "abcd";
  private static final double ALLOCATION = 100.;
  private static final long ENTRYTIME = 1388082600000L;
  private static final double ENTRYPRICE = 50.;
  private static final double EXITPRICE = 90.;
  private static final long EXITTIME = 1388082601000L;
  private static final String DUMMY_SUBSCRIBER_NUMBER = "9876543210";
  private static final String DUMMY_INCORRECT_NUMBER = "12345678";
  private static final String OTP_DUMMY = "4321";
  private static final long TIMESTAMP = DateUtils.currentTimeInMillis();
  private static final String INCORRECT_OTP = "4444";
  private static final String INVESTOR_NAME = "FirstName";
  private static final String LASTNAME = "LastName";
  private static final String INVALID_WEALTH_CODE = "abcd";
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;

  @SuppressWarnings("unused")
  private static final int OLD_TIMESTAMP_TO_MAKE_USER_NTU = 123456789;
  private static final String DUMMY_WEALTH_CODE = "1234";
  private static final String SIP_DATE = "21/08/2016";
  private static final double SIP_AMOUNT = 2000.;
  private static final String WEALTHCODE = "1234";
  private static final double LUMPSUM_AMOUNT = 40000.;
  private static final Map<String, String> USER_QUESTION_ANSWERS = new HashMap<String, String>();
  private static final Map<String, String> USER_QUESTION_ANSWERS_CORRECT = new HashMap<String, String>();

  private static final Map<String, String> USER_SECURITY_ANSWERS_ONE = new HashMap<String, String>();
  private static final Map<String, String> USER_SECURITY_ANSWERS_TWO = new HashMap<String, String>();
  private static final Map<String, String> USER_SECURITY_ANSWERS_THREE = new HashMap<String, String>();
  private static final String ADVISE_ID = "advise_id";
  private static final double RECOMMENDED_ENTRY_PRICE = 25.50;
  public static final String CLIENT_CODE = "EMP919191";
  public static final String CLIENT_CODE1 = "EMP929292";
  private static final String PANCARD = "ARNPC9761A";
  private static final String PANCARD1 = "ARNPC9761B";
  private static final double EXPECTED_REAL_WALLET_MONEY = 2000000d;

  private @Autowired WDService service;
  private @Autowired ActorSystem system;
  private @Autowired CoreDBTest dbTest;
  private @Autowired NotificationFacade snsFacadeMock;
  private @Autowired OTPUtils otpGeneratorMock;
  private @Autowired UserRepository userRepository;
  private @Autowired UserFundRepository userFundRepository;
  private @Autowired OrdersRepository ordersRepository;
  private @Autowired ActorMaterializer materializer;
  private @Autowired ViewPopulater viewPopulater;
  private @Autowired JournalProvider journalProvider;  
  private @Autowired SESEmailUtil sesEmailUtil;
  private @Autowired WDActorSelections actorSelection;
  private @Autowired @Qualifier("WDProjections") ProjectionsClient wdProjections;
  private ActorRef universeRootActor;
  private ActorRef testableDestinationActor;
  private @Autowired @Qualifier("RealWallet") Wallet realWallet;
  private @Autowired  @Qualifier("CognitoUserPoolForInvestor") AWSCognitoIdentityProvider investorUserPoolMock;
  private final static BrokerName BROKER_COMPANY_NAME = BrokerName.BPWEALTH;
  private final static BrokerName BROKER_COMPANY_NAME_1 = BrokerName.SYKES;
  private static final int TOKEN = 22;
  private static final String SYMBOL = "ACC";
  private static final String EXCHANGE = "NSE";
  private static final String TICKER1 = "IN1-NSE-EQ";
  private static final double ALLOCATION1 = 20.0;
  private static final double ALLOCATION2 = 15.0;
  private static final String TICKER2 = "IN2-NSE-EQ";
  private static final String ADVISEID2 = "1b";
  private static final long ISSUETIME2 = DateUtils.currentTimeInMillis();
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2 = 18.75;
  private static final String SYMBOL_2 = "SBIN";
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2 = 45000.;
  private static final String SYMBOL_1 = "ACC";
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1 = 20.;
  private static final double EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1 = 60000.;
  private static final long ENTRYTIME_2 = 1;
  private static final double CASH_COMPONENT_POST_ISSUE_OF_TWO_ADVISES_WHEN_AWAITING_A_RESPONSE = 195000.0;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 = 57;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_POST_SUBSCRIPTION_ISSUE_1 = 57;
  private static final int EXPECTED_TOTAL_ORDER_QUANTITY_2 = 43;
  private static final int EXPECTED_TOTAL_TRADED_QUANTITY_2 = 43;
  private static final double SUBSCRIPTION_LUMPSUM = 300000.;
  private static final double SUBSCRIPTION_SIPAMOUNT = 0.;
  private static final String SUBSCRIPTION_SIP_DATE = "";
  private static final String SUBSCRIPTION_WEALTHCODE = "2108";
  private static final String SUBSCRIPTION_USER_NAME = INVESTOR_USERNAME;
  private static final int TOKEN_1 = 133;
  private static final int TOKEN_2 = 134;
  private static final double CURRENT_MARKET_PRICE = 1040.;
  private static final String PHONENUMBER = "+918080621834";
  private static final String PUBLIC_ARN = "s5d7v657";
  private static final String COMPANY = "Wealth Technology";
  private static String USER_FIRSTNAME = "Ankit";
  @SuppressWarnings("unused")
  private ActorRef equityOrderRootActor;

  @Rule
  public Retry retry = new Retry(5);
  
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
  private @Autowired UserEquityAdviseRepository userEquityAdviseRepository;

  @Before
  public void setupDB() throws Exception {

    Files.delete(new File("build/journal"));
    Files.delete(new File("build/snapshots"));
    dbTest.createSchema();
    service.createActors();
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    testableDestinationActor = system.actorOf(SpringExtProvider.get(system).props("TestableDestinationActor"), "testable-destination-actor");
    equityOrderRootActor = system.actorOf(SpringExtProvider.get(system).props("EquityOrderRootActor"), "equity-order-aggregator");
    doReturn(OTP_DUMMY).when(otpGeneratorMock).getNewOtp();
    doReturn(WDProperties.DUMMY_HASHCODE_FOR_USER_ACTIVATION).when(otpGeneratorMock).getUniqueHashCode();
    doNothing().when(snsFacadeMock).sendOTP(OTP_DUMMY, DUMMY_SUBSCRIBER_NUMBER);
    doReturn(new SendEmailResult()).when(sesEmailUtil).sendEmail(anyString(), anyString(), anyString(), anyString());
    doReturn(new SendEmailResult()).when(sesEmailUtil).sendEmail(anyObject());

    USER_QUESTION_ANSWERS.put("001", "Sample Answer 001");
    USER_QUESTION_ANSWERS.put("002", "Sample Answer 002");
    USER_QUESTION_ANSWERS.put("003", "Sample Answer 003");

    USER_QUESTION_ANSWERS_CORRECT.put("Have you invested before?", "Yes");
    USER_QUESTION_ANSWERS_CORRECT.put("Do you have any large expenses coming up in the future?", "Yes");
    USER_QUESTION_ANSWERS_CORRECT.put("How do you want your money to grow?",
        "Slow and steady wins the race, I prefer safe investments like Fixed Deposits.");

    USER_SECURITY_ANSWERS_ONE.put("What is Sample Answer1", "001");
    USER_SECURITY_ANSWERS_TWO.put("What is Sample Answer1", "001");
    USER_SECURITY_ANSWERS_TWO.put("What is Sample Answer2", "002");

    USER_SECURITY_ANSWERS_THREE.put("What is Sample Answer1", "001");
    USER_SECURITY_ANSWERS_THREE.put("What is Sample Answer2", "002");
    USER_SECURITY_ANSWERS_THREE.put("What is Sample Answer3", "003");

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
  }

  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    deleteDirectory(new File("build/journal"));
    deleteDirectory(new File("build/snapshots"));

  }

  @Override
  public FiniteDuration awaitDuration() {
    return FiniteDuration.apply(500, TimeUnit.SECONDS);
  }

  private void sendEODPriceToEquityActor(double currentMarketPrice, String token) {
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(currentMarketPrice, DateUtils.currentTimeInMillis(), token), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }
  
  private void sendLivePriceToEquityActor(double currentMarketPrice, String token) {
    universeRootActor.tell(new RealTimeInstrumentPrice(new InstrumentPrice(currentMarketPrice, DateUtils.currentTimeInMillis(), token)), ActorRef.noSender());
  }
  
  @Test
  public void return_200_ok_request_for_add_wealthCode() throws InterruptedException, ParseException {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    response = userCommandPost("/approvekyc",
        new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = authenticatedPost("/wealthcode", new WealthCode("1244"));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  @Test
  public void return_200_when_device_token_is_successfully_updated() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME));
    InvestorCredentialsWithType responseEntity = response.entity(unmarshaller(InvestorCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(InvestorCredentialsWithType.class);

    response = authenticatedPost("/devicetoken",
        new DeviceToken(DEVICE_TOKEN));
    response.assertStatusCode(OK).assertMediaType("application/json");
  }

  @Test
  public void return_200_ok_request_for_kyc_approval() throws InterruptedException {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  @Test
  public void return_412_for_multiple_same_client_code_request_for_kyc_approval() throws InterruptedException {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = userCommandPost("/registerinvestorfromuserpool", registerUserThree());
    kycApproveCommand = new KycApproveCommand(CLIENT_CODE1, PANCARD, INVESTOR_EMAIL2, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = userCommandPost("/registerinvestorfromuserpool", registerUserTwo());
    kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_EMAIL, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Failed failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(412).assertMediaType("application/json");
    assertThat(failedResponseEntity).isInstanceOf(Failed.class);
  }

  @Test
  public void return_412_for_multiple_same_client_code_request_for_kyc_update() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_USERNAME);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = userCommandPost("/registerinvestorfromuserpool", registerUserTwo());
    kycApproveCommand = new KycApproveCommand(CLIENT_CODE1, PANCARD, INVESTOR_EMAIL, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    UpdateKycCommand updatekycCommand = new UpdateKycCommand(INVESTOR_USERNAME, CLIENT_CODE, "", "", USER_PHONENUMBER, INVESTOR_USERNAME);
    response = userCommandPost("/updatekyc",updatekycCommand);
    Failed failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(412).assertMediaType("application/json");
    assertThat(failedResponseEntity).isInstanceOf(Failed.class);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);
    assertThat(userRepository.findByEmail(INVESTOR_USERNAME).orElse(null).getClientCode()).isEqualTo(CLIENT_CODE);
  }

  @Test
  public void return_200_ok_request_for_kyc_phone_number_update() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    UpdateKycCommand updatekycCommand = new UpdateKycCommand(INVESTOR_USERNAME.split("@")[0], "", "", "", USER_PHONENUMBER1, INVESTOR_USERNAME);
    response = userCommandPost("/updatekyc",updatekycCommand);
    Done responseEntity1 = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity1).isInstanceOf(Done.class);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);
    assertThat(userRepository.findByEmail(INVESTOR_USERNAME).orElse(null).getPhoneNumber()).isEqualTo(USER_PHONENUMBER1);
  }

  @Test
  public void return_200_ok_request_for_kyc_broker_name_update() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    UpdateKycCommand updatekycCommand = new UpdateKycCommand(INVESTOR_USERNAME.split("@")[0], "", "", BROKER_COMPANY_NAME_1.getBrokerName(), "", INVESTOR_USERNAME);
    response = userCommandPost("/updatekyc",updatekycCommand);
    Done responseEntity1 = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity1).isInstanceOf(Done.class);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);
    assertThat(userRepository.findByEmail(INVESTOR_USERNAME).orElse(null).getBrokerCompany()).isEqualTo(BROKER_COMPANY_NAME_1.getBrokerName());
  }

  @Test
  public void return_200_ok_request_for_kyc_email_update() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    UpdateKycCommand updatekycCommand = new UpdateKycCommand(INVESTOR_USERNAME.split("@")[0], "", "", "", "", INVESTOR_EMAIL);
    response = userCommandPost("/updatekyc",updatekycCommand);
    Done responseEntity1 = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity1).isInstanceOf(Done.class);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);
    assertThat(userRepository.findById(INVESTOR_USERNAME.split("@")[0]).orElse(null).getEmail()).isEqualTo(INVESTOR_EMAIL);
  }

  @Test
  public void return_200_ok_request_for_kyc_pan_number_update() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    UpdateKycCommand updatekycCommand = new UpdateKycCommand(INVESTOR_USERNAME.split("@")[0], "", PANCARD1, "", "", INVESTOR_USERNAME);
    response = userCommandPost("/updatekyc",updatekycCommand);
    Done responseEntity1 = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity1).isInstanceOf(Done.class);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);
    assertThat(userRepository.findByEmail(INVESTOR_USERNAME).orElse(null).getPanCard()).isEqualTo(PANCARD1);
  }

  @Test
  public void return_200_ok_request_for_kyc_update() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    KycApproveCommand kycApproveCommand = new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL);
    response = userCommandPost("/approvekyc",kycApproveCommand);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    UpdateKycCommand updatekycCommand = new UpdateKycCommand(INVESTOR_USERNAME.split("@")[0], CLIENT_CODE1, "", "", USER_PHONENUMBER, INVESTOR_USERNAME);
    response = userCommandPost("/updatekyc",updatekycCommand);
    Done responseEntity1 = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity1).isInstanceOf(Done.class);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);
    assertThat(userRepository.findByEmail(INVESTOR_USERNAME).orElse(null).getClientCode()).isEqualTo(CLIENT_CODE1);
  }

  @Test
  public void return_400_error_request_for_add_wealthCode() throws InterruptedException {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    response = authenticatedPost("/wealthcode", new WealthCode("1244"));
    Failed responseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(403).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Failed.class);
  }

  @Test
  public void return_200_when_otp_is_sent_to_new_subscriber_for_ntu_na_user() throws Exception {
    doReturn(true).when(otpGeneratorMock).isOTPTimeValid(Mockito.anyString(), Mockito.anyString(), Mockito.anyLong());
    registerUserAndActivate();
    authenticatedPost("/phone?phonenumber=" + DUMMY_SUBSCRIBER_NUMBER, "");
    assertOTPisReceived(1);

    authenticatedPost("/otp?otp=" + DUMMY_SUBSCRIBER_NUMBER, "");
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    CurrentUserDb user = userRepository.findByEmail(INVESTOR_USERNAME).orElse(null);
    Assert.assertEquals(DUMMY_SUBSCRIBER_NUMBER, user.getPhoneNumber());
  }

  @Test
  public void return_200_when_sending_a_sms_to_a_user() throws Exception {
    System.setProperty("sendUserEmailUpdates", "true");
    return_200_when_otp_is_sent_to_new_subscriber_for_ntu_na_user();
    approveKYCAndAddWealthcode();
    assertSMSisReceived();
  }
  
  private void registerUserAndActivate() {
    TestRouteResult response;
    response = userCommandPost("/registerinvestorfromuserpool", registerUser());
    response.assertStatusCode(OK).assertContentType("application/json");
    assertThat(response.entity(unmarshaller(InvestorCredentialsWithType.class)))
        .isInstanceOf(InvestorCredentialsWithType.class);
  }

  @Test
  public void return_200_user_answers_are_added_or_updated() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    response = authenticatedPost("/useranswers", new SubmitUserAnswers(INVESTOR_USERNAME, "testEvent", USER_QUESTION_ANSWERS));
    assertThat(response.status()).isEqualTo(OK);
  }

  @Test
  public void return_200_user_role_status_is_replied() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    response = authenticatedPost("/userrolestatus",
        new GetUserRoleStatus(INVESTOR_USERNAME, PHONENUMBER, INVESTOR_EMAIL, USER_FIRSTNAME));
    assertThat(response.status()).isEqualTo(OK);
    assertThat(response.entity(unmarshaller(UserCurrentRoleStatus.class)))
    .isInstanceOf(UserCurrentRoleStatus.class);
  }

  @Test
  public void return_200_user_answers_make_riskProfile_Low() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    response = authenticatedPost("/useranswers",
        new SubmitUserAnswers(INVESTOR_USERNAME, "testEvent", USER_QUESTION_ANSWERS_CORRECT));
    assertThat(response.status()).isEqualTo(OK);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    CurrentUserDb user = userRepository.findByEmail(INVESTOR_USERNAME).orElse(null);
    Assert.assertEquals(RiskLevel.LOW, user.getRiskProfile());
  }

  private RegisterInvestorFromUserPool registerUser() {
    return new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME);
  }

  private RegisterInvestorFromUserPool registerUserTwo() {
    return new RegisterInvestorFromUserPool(INVESTOR_EMAIL.split("@")[0], INVESTOR_NAME, "", INVESTOR_EMAIL);
  }

  private RegisterInvestorFromUserPool registerUserThree() {
    return new RegisterInvestorFromUserPool(INVESTOR_EMAIL2.split("@")[0], INVESTOR_NAME, "", INVESTOR_EMAIL2);
  }

  @Test
  public void return_200_two_user_security_answers_are_added() throws Exception {
    registerUserAndActivate();
        
    TestRouteResult response = userCommandPost("/approvekyc",
        new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = authenticatedPost("/securityanswers", new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_TWO));
    response.assertStatusCode(OK);
  }

  @Test
  public void return_400_when_less_than_two_user_security_answers_are_added() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    response = authenticatedPost("/securityanswers", new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_ONE));
    response.assertStatusCode(BAD_REQUEST);
  }

  @Test
  public void return_400_when_more_than_two_user_security_answers_are_added() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    response = authenticatedPost("/securityanswers",
        new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_THREE));
    response.assertStatusCode(BAD_REQUEST);
  }

  @Test
  public void return_400_user_security_answers_are_added_again() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    
    response = userCommandPost("/approvekyc",
        new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = authenticatedPost("/securityanswers", new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_TWO));
    response.assertStatusCode(OK);
    
    response = authenticatedPost("/securityanswers", new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_TWO));
    response.assertStatusCode(BAD_REQUEST);

    response = authenticatedPost("/securityanswers", new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_TWO));
    response.assertStatusCode(BAD_REQUEST);
  }
  
  @Test
  public void return_400_user_security_answers_are_added_when_kyc_is_not_done() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    
    response = authenticatedPost("/securityanswers", new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_TWO));
    response.assertStatusCode(BAD_REQUEST);    
  }

  @Test
  public void return_400_when_invalid_wealthcode_is_updated() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    response = authenticatedPost("/wealthcode", new Gson().toJson(new WealthCode(INVALID_WEALTH_CODE)));
    response.assertStatusCode(BAD_REQUEST);
  }

  @Test
  public void return_403_when_wealthcode_is_updated_by_ntu_user() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();    
    response = authenticatedPost("/wealthcode", new WealthCode(DUMMY_WEALTH_CODE));
    response.assertStatusCode(Forbidden());
  }

  @Test
  public void populate_profile_view_on_register_custom_user() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME));
    response.assertStatusCode(OK).assertContentType("application/json");
    assertThat(response.entity(unmarshaller(InvestorCredentialsWithType.class)))
        .isInstanceOf(InvestorCredentialsWithType.class);

    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    CurrentUserDb persistedUser = userRepository.findByEmail(INVESTOR_USERNAME).orElse(null);

    assertThat(persistedUser.getFirstName()).isEqualTo(INVESTOR_NAME);
  }

  @Test
  public void return_200_when_otp_is_sent_to_new_subscriber_for_ntu() throws Exception {
    registerUserAndActivate();
    authenticatedPost("/phone?phonenumber=" + DUMMY_SUBSCRIBER_NUMBER, "");
    assertOTPisReceived(1);
    assertOTPEmailisReceived(1);
  }

  private void assertOTPEmailisReceived(int i) {
    verify(sesEmailUtil, Mockito.atLeast(i)).prepareAndSendEmail(OTPEmail.class, OTP_DUMMY, INVESTOR_USERNAME);
  }

  @Test
  public void return_400_for_incorrect_number() throws ParseException {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME));
    InvestorCredentialsWithType responseEntity = response.entity(unmarshaller(InvestorCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(InvestorCredentialsWithType.class);
    
    response = authenticatedPost("/phone?phonenumber=" + DUMMY_INCORRECT_NUMBER, "");
    assertThat(response.status()).isEqualTo(BAD_REQUEST);
  }

  @Test
  public void return_200_for_correct_otp_entered_ntu() throws Exception {
    doReturn(true).when(otpGeneratorMock).isOTPTimeValid(Mockito.anyString(), Mockito.anyString(), Mockito.anyLong());
    return_200_when_otp_is_sent_to_new_subscriber_for_ntu();

    TestRouteResult response = authenticatedPost("/otp?otp=" + OTP_DUMMY, "");
    assertThat(response.status()).isEqualTo(OK);
  }

  @Test
  public void return_200_for_correct_otp_entered_ntu_na() throws Exception {
    return_200_when_otp_is_sent_to_new_subscriber_for_ntu_na_user();
    TestRouteResult response = authenticatedPost("/otp?otp=" + OTP_DUMMY, "");
    assertThat(response.status()).isEqualTo(OK);
  }

  @Test
  public void return_401_for_incorrect_otp_entered_ntu() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME));
    InvestorCredentialsWithType responseEntity = response.entity(unmarshaller(InvestorCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(InvestorCredentialsWithType.class);
    
    authenticatedPost("/phone?phonenumber=" + DUMMY_SUBSCRIBER_NUMBER, "");
    assertOTPisReceived(1);

    response = authenticatedPost("/otp?otp=" + INCORRECT_OTP, "");
    assertThat(response.status()).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void return_401_for_incorrect_otp_entered_ntu_na() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME));
    InvestorCredentialsWithType responseEntity = response.entity(unmarshaller(InvestorCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(InvestorCredentialsWithType.class);
    
    authenticatedPost("/phone?phonenumber=" + DUMMY_SUBSCRIBER_NUMBER, "");
    assertOTPisReceived(1);
    
    response = authenticatedPost("/otp?otp=" + INCORRECT_OTP, "");
    assertThat(response.status()).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void return_400_when_register_request_is_missing_password() throws Exception {
    TestRouteResult response = jsonStringPostToValidateEmptyInputParameterFlow("/registerinvestorfromuserpool",
        "{\"firstName\":\"" + INVESTOR_NAME + "\",\"lastName\":\"" + LASTNAME + "\",\"userEmail\":\"" + INVESTOR_USERNAME
            + "\",\"password\":\"" + "\",\"deviceToken\":\"" + DEVICE_TOKEN + "\"}");
    waitForProcessToComplete(5);
    response.assertStatusCode(BAD_REQUEST);
  }
    
  @Test
  public void return_200_when_register_request_is_having_different_domain_name() throws Exception {
    TestRouteResult response = jsonStringPostToValidateEmptyInputParameterFlow("/registerinvestorfromuserpool",
        "{\"name\":\"" + INVESTOR_NAME + "\",\"username\":\"" + USER_EMAIL_DIFFERENT_DOMAIN
            + "\",\"email\":\"" + INVESTOR_EMAIL+ "\", \"phoneNumber\" : \"\"}");
    waitForProcessToComplete(5);
    response.assertStatusCode(OK);
  }

  @Test
  public void return_400_when_register_request_is_missing_email() throws Exception {
    TestRouteResult response = jsonStringPostToValidateEmptyInputParameterFlow("/registerinvestorfromuserpool",
        "{\"firstName\":\"" + INVESTOR_NAME + "\",\"lastName\":\"" + LASTNAME + "\",\"userEmail\":\"" + "\",\"password\":\""
            + USER_PASSWORD + "\",\"deviceToken\":\"" + DEVICE_TOKEN + "\"}");
    waitForProcessToComplete(5);
    response.assertStatusCode(BAD_REQUEST);
  }

  @Test
  public void return_400_when_register_request_is_missing_firstName() throws Exception {
    TestRouteResult response = jsonStringPostToValidateEmptyInputParameterFlow("/registerinvestorfromuserpool",
        "{\"firstName\":\"" + "\",\"lastName\":\"" + LASTNAME + "\",\"userEmail\":\"" + INVESTOR_USERNAME
            + "\",\"password\":\"" + USER_PASSWORD + "\",\"deviceToken\":\"" + DEVICE_TOKEN + "\"}");
    waitForProcessToComplete(5);
    response.assertStatusCode(BAD_REQUEST);
  }

  @Test
  public void return_200_when_custom_user_successfully_registers() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME));
    InvestorCredentialsWithType responseEntity = response.entity(unmarshaller(InvestorCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    waitForProcessToComplete(5);
    assertThat(responseEntity).isInstanceOf(InvestorCredentialsWithType.class);
  }

  @Test
  public void return_400_when_user_keys_in_incorrect_emailID() throws Exception {
    TestRouteResult response = jsonStringPostToValidateEmptyInputParameterFlow("/registerinvestorfromuserpool",
        "{\"firstName\":\"" + "\",\"lastName\":\"" + LASTNAME + "\",\"userEmail\":\"" + INVALIDUSEREMAIL
            + "\",\"password\":\"" + USER_PASSWORD + "\"}");
    waitForProcessToComplete(5);
    response.assertStatusCode(BAD_REQUEST);
  }

  @Test
  public void returns_400_when_user_is_registered_more_than_once() throws ParseException {
    TestRouteResult response;
    registerUserAndActivate();
    response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME, "", INVESTOR_USERNAME));
    response.assertStatusCode(NOT_ACCEPTABLE).assertMediaType("application/json");
    assertThat(response.entity(unmarshaller(Exists.class))).isEqualTo(exists(INVESTOR_USERNAME.split("@")[0]));
  }

  @Test
  public void returns_200_when_fund_is_successfully_subscribed() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();

    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    environmentVariables.set("WT_MARKET_OPEN", "true");

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(2);
    
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    
    waitForProcessToComplete(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  private void registerAdviserProcess() {
    TestRouteResult response;
    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    try {
      waitForProcessToComplete(5);
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);
  }

  @Test
  public void returns_400_when_fund_is_already_subscribed() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    environmentVariables.set("WT_MARKET_OPEN", "true");

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT+ "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(1);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    waitForProcessToComplete(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo("Subscription in process. You will receive a notification once its done.");
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + FUND_EXCESS_LUMPSUM+ "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(1);

    Failed failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(BAD_REQUEST).assertMediaType("application/json");
    assertThat(failedResponseEntity).isInstanceOf(Failed.class);
  }

  @Test
  public void returns_404_when_a_non_existing_fund_is_subscribed() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    registerAdviserProcess();

    environmentVariables.set("WT_MARKET_OPEN", "true");

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + INVALID_FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT+ "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(1);

    Failed failedResponseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(NOT_FOUND).assertMediaType("application/json");
    assertThat(failedResponseEntity).isInstanceOf(Failed.class);
  }

  @Test
  public void returns_200_when_a_user_tries_to_subscribe_after_market_hours() {
    TestRouteResult response;
    registerUserAndActivate();
    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    environmentVariables.set("WT_MARKET_OPEN", "false");
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Your subscription request has been received.You will be subscribed to fund when the markets open on the next trading day.");
  }
  
  @Test
  public void returns_200_when_a_user_tries_to_unsubscribe_after_market_hours() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    environmentVariables.set("WT_MARKET_OPEN", "true");

    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    waitForProcessToComplete(1);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));    
    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo("Subscription in process. You will receive a notification once its done.");

    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Your request to unsubscribe has been received. You will be unsubscribed from WeeklyPicks on the next trading day.");
  }

  @Test
  public void returns_200_when_fund_is_successfully_unsubscribed() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME+ "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME,  ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));

    waitForAdviseToGetProcessedCompletely(2);
    waitForProcessToComplete(1);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo("Subscription in process. You will receive a notification once its done.");
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    waitForAdviseToGetProcessedCompletely(2);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }

  @Test
  public void check_if_current_market_price_is_greater_than_freeze_price() throws Exception {

    TestRouteResult response;
    registerUserAndActivate();
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISE_ID, TICKER,
        ALLOCATION, ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, DateUtils.currentTimeInMillis()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    Thread.sleep(2 *1000);
    sendLivePriceToEquityActor(101.F, valueOf(TOKEN));

    String adviseId = responseEntity.getId();
    response = authenticatedAdviserPost("/recoentryprice",
        new UpdateRecommendedEntryPrice(RECOMMENDED_ENTRY_PRICE, adviseId, FUND_NAME));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    Thread.sleep(2 *1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForViewToPopulate(1);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(INVESTOR_USERNAME.split("@")[0], FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(LUMPSUM_AMOUNT);

  }

  @Test
  public void returns_200_when_reco_price_is_successfully_updated() throws Exception {
    Fund fund = new Fund();
    FundFloated event = new FundFloated(ADVISER_USERNAME, ADVISER_NAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, RiskLevel.LOW);
    fund.update(event);
    waitForViewToPopulate(1);
    FundAdviceIssued issueAdviseEvent = new FundAdviceIssued(ADVISER_USERNAME, FUND_NAME, ADVISE_ID, TICKER, SYMBOL, ALLOCATION, ALLOCATION, TIMESTAMP, new PriceWithPaf(new InstrumentPrice(ENTRYPRICE, ENTRYTIME, TICKER), new Pafs(1., 1., 0., 1.)),
        ENTRYTIME, new PriceWithPaf(new InstrumentPrice(EXITPRICE, EXITTIME, TICKER), new Pafs(1., 1., 0., 1.)), EXITTIME, false);
    fund.update(issueAdviseEvent);
    waitForViewToPopulate(1);
    RecommendedEntryPriceUpdated recoPriceUpdated = new RecommendedEntryPriceUpdated(ADVISER_NAME, FUND_NAME, RECOMMENDED_ENTRY_PRICE, ADVISE_ID);
    fund.update(recoPriceUpdated);
    waitForViewToPopulate(1);
    assertThat(fund.adviseForId(ADVISE_ID).getRecoEntryPrice().getPrice().getPrice()).isEqualTo(RECOMMENDED_ENTRY_PRICE);
  }

  @Test
  public void returns_200_when_advises_are_successfully_received() throws InterruptedException {
    TestRouteResult response;
    registerUserAndActivate();

    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);

    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER, ALLOCATION,
        ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME));

    waitForAdviseToGetProcessedCompletely(2);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = getMethodForAuthenticatedUser("/advises", "adviser=" + ADVISER_USERNAME + "&fund=" + FUND_NAME+"&investingMode="+VIRTUAL);
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(response.entity(unmarshaller(FundAdviseListResponse.class))).isInstanceOf(FundAdviseListResponse.class);
  }

  private RegisterAdviserFromUserPool registerAdviser() {
    return new RegisterAdviserFromUserPool(ADVISER_USERNAME, ADVISER_NAME, ADVISER_EMAIL, ADVISER_PUBLIC_ID, COMPANY_NAME, "");
  }

  @Test
  public void returns_200_when_adviser_are_successfully_register() {
    registerAdviserProcess();
  }
  
  @Test
  public void check_each_composite_key_for_virtual_and_real()
  {
    UserAdviseCompositeKey userAdvise = new UserAdviseCompositeKey(ADVISE_ID, INVESTOR_USERNAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL );
    assertNotNull(userAdvise);
    UserAdviseCompositeKeyWithDate userAdviseWithDate = new UserAdviseCompositeKeyWithDate(ADVISE_ID, INVESTOR_USERNAME, FUND_NAME, ADVISER_USERNAME, DateUtils.currentTimeInMillis(), VIRTUAL );
    assertNotNull(userAdviseWithDate);
    UserFundCompositeKey userFund = new UserFundCompositeKey(INVESTOR_USERNAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL);
    assertNotNull(userFund);
    UserFundCompositeKey userFundWithDate = new UserFundCompositeKeyWithDate(INVESTOR_USERNAME, FUND_NAME, ADVISER_USERNAME, VIRTUAL, DateUtils.currentTimeInMillis() );
    assertNotNull(userFundWithDate);
  }

  @Test
  public void return_200_for_virtual_and_real_advises_and_wallet_amount() throws Exception
  {
    doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(EXPECTED_REAL_WALLET_MONEY, INVESTOR_USERNAME, REAL);
      actorSelection.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));

    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
    
    TestRouteResult response;
    registerUserAndActivate();

    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    environmentVariables.set("WT_MARKET_OPEN", "true");
    waitForViewToPopulate(2);
    
    sendLivePriceToEquityActor(101.F, "22");
    
    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  
        "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    Thread.sleep(2 * 1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForViewToPopulate(2);
    response = authenticatedPost("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);

    approveKYCAndAddWealthcode();
    waitForViewToPopulate(2);
    response = authenticatedGet("/walletmoney", new GetWalletMoney(INVESTOR_USERNAME, REAL),
        "investingMode=" + REAL.getInvestingMode());
    waitForViewToPopulate(1);
    assertThat(Double.parseDouble(response.entity(unmarshaller(Done.class)).getMessage()))
        .isEqualTo(EXPECTED_REAL_WALLET_MONEY);

    waitForViewToPopulate(2);
    
    sendLivePriceToEquityActor(101.F, "22");
    
    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  
        "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + REAL.getInvestingMode());
    Thread.sleep(2 * 1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        REAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForAdviseToGetProcessedCompletely(2);
    
    response = authenticatedPost("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, REAL, Auto));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Auto for fund " + FUND_NAME);

    sendEODPriceToEquityActor(101., "INTICKER-NSE-EQ");
    
    waitForAdviseToGetProcessedCompletely(2);
    
    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER, ALLOCATION,
        ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME));

    waitForAdviseToGetProcessedCompletely(2);

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    waitForViewToPopulate(4);
    
    Iterable<UserEquityAdvise> equityAdvises = userEquityAdviseRepository.findAll();
    ArrayList<UserEquityAdvise> allEquityAdvises = new ArrayList<UserEquityAdvise>();
    equityAdvises.forEach(advise -> allEquityAdvises.add(advise));
    assertEquals(2l, allEquityAdvises.size());

    StringBuilder result = new StringBuilder();
    equityAdvises.forEach(advise -> result.append(advise));
    assertEquals(true, result.toString().contains("usernameWithFundWithAdviser=test__WeeklyPicks__REAL__swapnil"));
    assertEquals(true, result.toString().contains("usernameWithFundWithAdviser=test__WeeklyPicks__VIRTUAL__swapnil"));
    assertEquals(true, result.toString().contains("description=null, clientCode=null, brokerName=null, token=INTICKER-NSE-EQ, symbol=INTICKER-NSE-EQ, issueAdviceAbsoluteAllocation=100.0, issueAdviseAllocationValue=40000.0, closeAdviseAbsoluteAllocation=0.0, cumulativeDividendsReceived=0.0, issueAdviseOrder=[Order(orderKey=OrderKey(usernameWithOrderId=test__REAL__"));
    assertEquals(true, result.toString().contains("description=null, clientCode=null, brokerName=null, token=INTICKER-NSE-EQ, symbol=INTICKER-NSE-EQ, issueAdviceAbsoluteAllocation=100.0, issueAdviseAllocationValue=40000.0, closeAdviseAbsoluteAllocation=0.0, cumulativeDividendsReceived=0.0, issueAdviseOrder=[Order(orderKey=OrderKey(usernameWithOrderId=test__VIRTUAL__"));
  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());     
  }
  
  private TestRouteResult authenticatedGet(String url, Object command, String queryString) {
    TestRouteResult response;
    String jsonString = toJsonString(command);
    if (queryString != "" || queryString != null) {
      response = route().run(GET(url + "?" + queryString).addHeader(RawHeader.create("username", INVESTOR_USERNAME))
          .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    } else {
      response = route().run(GET(url).addHeader(RawHeader.create("username", INVESTOR_USERNAME))
          .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    }
    return response;
  }

  private void approveKYCAndAddWealthcode() {
    TestRouteResult response = userCommandPost("/approvekyc",
        new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = authenticatedPost("/wealthcode", new WealthCode(WEALTHCODE));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
  }
  
  @Test
  public void return_200_first_name_when_email_send()
  {
    registerUserAndActivate();
    
    TestRouteResult response = getMethodForAuthenticatedUser("/investorname", "email="+INVESTOR_USERNAME);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertThat(responseEntity.getMessage()).isEqualTo(INVESTOR_NAME);
  }
  
  @Test
  public void return_400_first_name_when_wrong_email_is_send()
  {
    TestRouteResult response = userUnAuthenticatedCommandGet("/investorname", "email=someWrongEmail@test.com");
    Failed responseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(NOT_FOUND).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Failed.class);
  }
  
  @SuppressWarnings("unchecked")
  @Test
  public void return_200_when_adviser_wants_user_execution_report_projection() throws Exception
  {    
    registerUserAndActivate();
    Mockito.doAnswer(invocation -> {
    String value = "{\"totalInvested\":[553876],\"totalWithdraw\":[0],\"isSubscribed\":[true],\"fundName\":[\""+FUND_NAME+"\"],\"adviserUsername\":[\""+ADVISER_USERNAME+"\"]}";
      return ask(testableDestinationActor, Option.apply(value), 1 * 1000);
    }).when(wdProjections).fetchProjectionState(Mockito.anyString(), Mockito.any(Option.class));
    
    TestRouteResult response = getMethodForAuthenticatedUser("/usersubscriptiondetails", "investingMode="+REAL);
    
    UserInvestmentStatus responseEntityForProjection = response.entity(unmarshaller(UserInvestmentStatus.class)); //new Gson().fromJson(response.entityString(), ProjectionResults.class);//
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntityForProjection).isInstanceOf(Done.class);
    assertThat(responseEntityForProjection.getId()).isEqualTo(INVESTOR_USERNAME.split("@")[0]);
    assertThat(responseEntityForProjection.getMessage()).isEqualTo("UserAllInvestmentReport");
    assertThat(responseEntityForProjection.getResult().get(0).getAdviserUsername()).isEqualTo(ADVISER_USERNAME);
    assertThat(responseEntityForProjection.getResult().get(0).getFundName()).isEqualTo(FUND_NAME);
    assertThat(responseEntityForProjection.getResult().get(0).getInvestingMode()).isEqualTo(REAL);
    assertThat(responseEntityForProjection.getResult().get(0).getIsFundSubscribed()).isEqualTo("true");
    assertThat(responseEntityForProjection.getResult().get(0).getSubscriptionAmount()).isEqualTo(553876d);
    assertThat(responseEntityForProjection.getResult().get(0).getWithdrawal()).isEqualTo(0d);
    
  }
  
  @Test
  public void get_user_security_questions_after_they_are_set() throws InterruptedException {
    registerUserAndActivate();
    
    TestRouteResult response = userCommandPost("/approvekyc",
        new KycApproveCommand(CLIENT_CODE, PANCARD, INVESTOR_USERNAME, BROKER_COMPANY_NAME.getBrokerName(), USER_PHONENUMBER, INVESTOR_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    response = authenticatedPost("/securityanswers", new SubmitSecurityAnswers(INVESTOR_USERNAME, USER_SECURITY_ANSWERS_TWO));
    response.assertStatusCode(OK);
    
    response = getMethodForAuthenticatedUser("/securityquestions", null);
    UserSecurityQuestionsList entity = response.entity(unmarshaller(UserSecurityQuestionsList.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(entity).isInstanceOf(UserSecurityQuestionsList.class);
  }
  
  @Test
  public void schedule_subscription_post_market_hours_and_execute_when_market_is_open() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
    
    doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(EXPECTED_REAL_WALLET_MONEY, INVESTOR_USERNAME, REAL);
      actorSelection.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));

    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISE_ID, TICKER,
        ALLOCATION, ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, DateUtils.currentTimeInMillis()));
    responseEntity = response.entity(unmarshaller(Done.class));

    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    Thread.sleep(3 * 1000);
    
    sendLivePriceToEquityActor(101.F, valueOf(TOKEN));
    approveKYCAndAddWealthcode();
    waitForViewToPopulate(2);
    response = authenticatedGet("/walletmoney", new GetWalletMoney(INVESTOR_USERNAME, REAL),
        "investingMode=" + REAL.getInvestingMode());
    waitForViewToPopulate(1);
    assertThat(Double.parseDouble(response.entity(unmarshaller(Done.class)).getMessage()))
        .isEqualTo(EXPECTED_REAL_WALLET_MONEY);

    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    
    waitForProcessToComplete(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Your subscription request has been received.You will be subscribed to fund when the markets open on the next trading day.");    
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    actorSelection.userRootActor().tell(new ExecutePostMarketUserCommands(), ActorRef.noSender());
    waitForAdviseToGetProcessedCompletely(5);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    Failed responseFailedEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(BAD_REQUEST).assertMediaType("application/json");
    assertEquals(responseFailedEntity.getMessage(),"Already subscribed");
    
  }
  
  @Test
  public void return_400_subscription_post_market_hours_when_duplicated_request_is_send() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
    sendLivePriceToEquityActor(101.F, TICKER);
    
    doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(EXPECTED_REAL_WALLET_MONEY, INVESTOR_USERNAME, REAL);
      actorSelection.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));

    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISE_ID, TICKER,
        ALLOCATION, ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, DateUtils.currentTimeInMillis()));
    responseEntity = response.entity(unmarshaller(Done.class));

    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    approveKYCAndAddWealthcode();
    waitForViewToPopulate(2);
    response = authenticatedGet("/walletmoney", new GetWalletMoney(INVESTOR_USERNAME, REAL),
        "investingMode=" + REAL.getInvestingMode());
    waitForViewToPopulate(1);
    assertThat(Double.parseDouble(response.entity(unmarshaller(Done.class)).getMessage()))
        .isEqualTo(EXPECTED_REAL_WALLET_MONEY);

    environmentVariables.set("WT_MARKET_OPEN", "false");

    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    Thread.sleep(2 *1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
        
    
    waitForProcessToComplete(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Your subscription request has been received.You will be subscribed to fund when the markets open on the next trading day.");    
    
    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    Thread.sleep(2 *1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    
    Failed responseFailedEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(responseFailedEntity).isInstanceOf(Failed.class);
    assertEquals(responseFailedEntity.getMessage(), "You cannot send more than one request in the same fund during non-market hours.");
    
  }
  
  @Test
  public void return_200_post_market_hours_when_subscription_request_is_send_in_different_funds() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
    sendLivePriceToEquityActor(101.F, TICKER);
    
    doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(EXPECTED_REAL_WALLET_MONEY, INVESTOR_USERNAME, REAL);
      actorSelection.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));

    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND2_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND2_NAME, ADVISE_ID, TICKER,
        ALLOCATION, ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, DateUtils.currentTimeInMillis()));
    responseEntity = response.entity(unmarshaller(Done.class));

    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    approveKYCAndAddWealthcode();
    waitForViewToPopulate(2);
    response = authenticatedGet("/walletmoney", new GetWalletMoney(INVESTOR_USERNAME, REAL),
        "investingMode=" + REAL.getInvestingMode());
    waitForViewToPopulate(1);
    assertThat(Double.parseDouble(response.entity(unmarshaller(Done.class)).getMessage()))
        .isEqualTo(EXPECTED_REAL_WALLET_MONEY);

    environmentVariables.set("WT_MARKET_OPEN", "false");

    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    Thread.sleep(2 *1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    waitForProcessToComplete(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Your subscription request has been received.You will be subscribed to fund when the markets open on the next trading day.");    
    
    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND2_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND2_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + REAL.getInvestingMode());
    Thread.sleep(2 *1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND2_NAME, ADVISER_USERNAME, WEALTHCODE,
        REAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Your subscription request has been received.You will be subscribed to fund when the markets open on the next trading day.");
    
  }
  
  @Test
  public void schedule_UnSubscription_request_after_market_hours() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
        
    environmentVariables.set("WT_MARKET_OPEN", "true");
    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);
    waitForProcessToComplete(1);
    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISE_ID, TICKER,
        ALLOCATION, ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, DateUtils.currentTimeInMillis()));
    responseEntity = response.entity(unmarshaller(Done.class));

    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    waitForProcessToComplete(3);
    sendLivePriceToEquityActor(101.F, valueOf(TOKEN));
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());

    waitForProcessToComplete(2);

    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    waitForProcessToComplete(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Subscription in process. You will receive a notification once its done.");
    waitForProcessToComplete(3);
    waitForViewToPopulate(3);
    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(),
        "Your request to unsubscribe has been received. You will be unsubscribed from " + FUND_NAME
            + " on the next trading day.");
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    actorSelection.userRootActor().tell(new ExecutePostMarketUserCommands(), ActorRef.noSender());
    waitForAdviseToGetProcessedCompletely(5);
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    Failed responseFailedEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(FORBIDDEN).assertMediaType("application/json");
    assertEquals(responseFailedEntity.getMessage(),"Not subscribed");
  }
  
  @Test
  public void return_400_if_user_sends_multiple_requests_after_market_hours_in_same_fund_for_unsubscribe() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
        
    environmentVariables.set("WT_MARKET_OPEN", "true");
    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);
    waitForProcessToComplete(1);
    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    response = adviserCommandPost("/issueadvise", new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISE_ID, TICKER,
        ALLOCATION, ENTRYPRICE, ENTRYTIME, EXITPRICE, EXITTIME, DateUtils.currentTimeInMillis()));
    responseEntity = response.entity(unmarshaller(Done.class));
    
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForProcessToComplete(3);
    
    sendLivePriceToEquityActor(101.F, valueOf(TOKEN));
    waitForProcessToComplete(3);
    response = authenticatedGet("/subscription", new FundSubscription(ADVISER_USERNAME, FUND_NAME, LUMPSUM_AMOUNT, SIP_AMOUNT, SIP_DATE, WEALTHCODE, VIRTUAL, EQUITYSUBSCRIPTION),  "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SIP_AMOUNT + "&lumpSumAmount=" + LUMPSUM_AMOUNT + "&sipDate=" +
        SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    Thread.sleep(2 *1000);
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    
    waitForProcessToComplete(1);
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(), "Subscription in process. You will receive a notification once its done.");
    waitForProcessToComplete(3);
    waitForViewToPopulate(3);
    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    assertEquals(responseEntity.getMessage(),
        "Your request to unsubscribe has been received. You will be unsubscribed from " + FUND_NAME
            + " on the next trading day.");
    
    response = authenticatedGet("/unsubscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&investingMode=" + VIRTUAL.getInvestingMode());
    response = authenticatedDelete("/unsubscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));       
    Failed responseFailedEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(responseFailedEntity).isInstanceOf(Failed.class);
    assertEquals(responseFailedEntity.getMessage(),
        "You cannot send more than one request in the same fund during non-market hours.");    
  }
    
  @Test
  public void schedule_post_market_user_approval_request_in_queue() throws Exception {
    TestRouteResult response;
    registerUserAndActivate();
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    updateInstrumentsMapOfEquityActor(TICKER2, SYMBOL_2, TOKEN_2);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER2);
    updateInstrumentsMapOfEquityActor(TICKER, SYMBOL, TOKEN);
    sendLivePriceToEquityActor(101.F, TICKER);
    
    doAnswer(invocation -> {
      MarginReceived marginReceived = new MarginReceived(EXPECTED_REAL_WALLET_MONEY, SUBSCRIPTION_USER_NAME.split("@")[0], REAL);
      actorSelection.userRootActor().tell(marginReceived, ActorRef.noSender());
      return null;
    }).when(realWallet).requestWalletMoney(Mockito.any(MarginRequested.class));

    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    response = adviserCommandPost("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(AdviserCredentialsWithType.class);
    waitForProcessToComplete(1);
    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    approveKYCAndAddWealthcode();
    waitForViewToPopulate(2);
    response = authenticatedGet("/walletmoney", new GetWalletMoney(SUBSCRIPTION_USER_NAME.split("@")[0], REAL),
        "investingMode=" + REAL.getInvestingMode());
    waitForViewToPopulate(1);
    assertThat(Double.parseDouble(response.entity(unmarshaller(Done.class)).getMessage()))
        .isEqualTo(EXPECTED_REAL_WALLET_MONEY);
    
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForProcessToComplete(3);

    response = authenticatedPost("/userApprovalMode", new ReceivedUserAcceptanceMode(FUND_NAME, ADVISER_USERNAME, VIRTUAL, Manual));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Acceptance mode updated to Manual for fund " + FUND_NAME);

    waitForProcessToComplete(3);

    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME);

    response = authenticatedPost("/issueadvise", firstAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(3);

    String firstAdviseId = responseEntity.getId();

    IssueAdvice secondAdvice = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, ADVISEID2, TICKER2, ALLOCATION2, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME2);
    response = authenticatedPost("/issueadvise", secondAdvice);
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your advise has been received, you will be soon notifed when process completes");

    waitForProcessToComplete(3);

    String secondAdviseId = responseEntity.getId();

    List<UserResponseParameters> allUserResponseParameter = Stream.of(
        new UserResponseParameters(secondAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME.split("@")[0], Issue, 
            SUBSCRIPTION_USER_NAME.split("@")[0], MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER2, SYMBOL_2, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_2, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_2, 0, ENTRYTIME_2),
        new UserResponseParameters(firstAdviseId, FUND_NAME, SUBSCRIPTION_USER_NAME.split("@")[0], Issue, 
            SUBSCRIPTION_USER_NAME.split("@")[0], MOCK.getBrokerName(), EXCHANGE, ADVISER_USERNAME, TICKER1, SYMBOL_1, VIRTUAL, Approve, 
            EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_1, EXPECTED_ISSUED_ADVICE_ABSOLUTE_ALLOCATION_VALUE_FOR_FRESH_ADVICE_AFTER_SUBSCRIPTION_1, 0, ENTRYTIME))
        .collect(Collectors.toList());

    environmentVariables.set("WT_MARKET_OPEN", "false");
    
    ObjectMapper objectMapper = new ObjectMapper();
    String userResponseParametersAsJsonString = objectMapper.writeValueAsString(allUserResponseParameter);
    response = authenticatedPost("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Your approval request has been received and will be executed on next trading day.");
    
    response = authenticatedPost("/userApprovalResponse", new ReceivedUserApprovalResponse(userResponseParametersAsJsonString, BASKETORDERID_FOR_TEST,BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(PRECONDITION_FAILED).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("You cannot send more than one request in the same fund during non-market hours.");
    
    environmentVariables.set("WT_MARKET_OPEN", "true");
    actorSelection.userRootActor().tell(new ExecutePostMarketUserCommands(), ActorRef.noSender());
    waitForProcessToComplete(3);
    
    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = new CurrentUserFundDb();

    List<UserFundEvent> allUserFundEvents = journalProvider.currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
    .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
    .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
    .map(userFundEvent -> ((UserFundEvent) userFundEvent.event()))
    .runWith(Sink.seq(), materializer)
    .toCompletableFuture().get();

    allUserFundEvents.stream().forEach(userFundEvent -> userFund.update(userFundEvent));

    assertThat(userFund.getCashComponent()).isEqualTo(CASH_COMPONENT_POST_ISSUE_OF_TWO_ADVISES_WHEN_AWAITING_A_RESPONSE);

    waitForViewToPopulate(2);

    ordersRepository.findAll().forEach(order -> {
      assertTrue(order.getFundName() != null && order.getFundName().length() > 0);
    });  
    List<Order> orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER1, timeFromMidnight());
    Stream<Trade> trades = orders.stream().filter(o -> o.getSide().getValue() == 1)
        .flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_POST_SUBSCRIPTION_ISSUE_1);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum())
        .isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_POST_SUBSCRIPTION_ISSUE_1);

    orders = ordersRepository.findByTickerAndOrderTimestampGreaterThan(TICKER2, timeFromMidnight());
    trades = orders.stream().filter(o -> o.getSide().getValue() == 1).flatMap(order -> order.getTrades().stream());
    assertThat(orders.stream().filter(o -> o.getSide().getValue() == 1).mapToInt(order -> order.getIntegerQuantity()).sum())
            .isEqualTo(EXPECTED_TOTAL_ORDER_QUANTITY_2);
    assertThat(trades.mapToInt(trade -> trade.getIntegerQuantity()).sum()).isEqualTo(EXPECTED_TOTAL_TRADED_QUANTITY_2);
  }

  private long timeFromMidnight() throws ParseException {
    return todayBOD() - (9 * 60 * 60 * 1000L);
  }
  
  @Test public void 
  honor_partial_withdraw_from_fund_post_market() throws Exception {
   
    TestRouteResult response; 
    Done responseEntity;
    registerAdviserProcess();

    response = adviserCommandPost("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));

    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
    
    registerUserAndActivate();
    
    waitForProcessToComplete(2);
    response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
            SUBSCRIPTION_SIP_DATE + "&investingMode=" + VIRTUAL.getInvestingMode());
    waitForProcessToComplete(2);

    response = authenticatedPost("/subscription", new InvestorApproval(FUND_NAME, ADVISER_USERNAME, SUBSCRIPTION_WEALTHCODE,
        VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve, BrokerAuthInformation.withNoBrokerAuth()));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(1);
    
    environmentVariables.set("WT_MARKET_OPEN", "false");

    response = authenticatedGet("/withdraw",
        "adviserUsername=" + ADVISER_USERNAME + "&fundName=" + FUND_NAME + "&withdrawalAmount="
            + 30000.0 + "&investingMode=" + VIRTUAL.getInvestingMode());
    
    response = authenticatedPost("/withdraw", new InvestorApproval(FUND_NAME, ADVISER_USERNAME,
        SUBSCRIPTION_WEALTHCODE, VIRTUAL, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, ApprovalResponse.Approve,BrokerAuthInformation.withNoBrokerAuth()));
    
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your withdrawal request has been received.You will get your withdrawl amount when"
                  + " the markets open on the next trading day.");
    environmentVariables.set("WT_MARKET_OPEN", "true");
    actorSelection.userRootActor().tell(new ExecutePostMarketUserCommands(), ActorRef.noSender());
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
    waitForProcessToComplete(3);

    UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey(SUBSCRIPTION_USER_NAME.split("@")[0], FUND_NAME,
        ADVISER_USERNAME, VIRTUAL);
    CurrentUserFundDb userFund = userFundRepository.findById(userFundCompositeKey).orElse(null);
    assertThat(userFund.getCashComponent()).isEqualTo(270000.0);
    }

  @Test
  public void test_create_investor_actor_when_user_pool_confirms() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", registerInvestorFromUserPool());
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    waitForProcessToComplete(5);
    assertThat(responseEntity).isInstanceOf(Done.class);    
  }
  
  @Test
  public void test_create_investor_actor_when_user_pool_confirms_using_a_json_example() throws Exception {
    TestRouteResult response = userCommandPost("/registerinvestorfromuserpool", "{\"phoneNumber\":\"+918080621831\",\"name\":\"Wealth Tech Test\",\"email\":\"wdtest@yopmail.com\",\"username\":\"wdtest200\"}");
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    waitForProcessToComplete(5);
    assertThat(responseEntity).isInstanceOf(Done.class);    
  }
  
  @Test
  public void test_create_adviser_actor_when_user_pool_confirms() throws Exception {
    TestRouteResult response = adviserCommandPost("/registeradviserfromuserpool", registerAdviserFromUserPool());
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    waitForProcessToComplete(5);
    assertThat(responseEntity).isInstanceOf(Done.class);    
  }

  @Test
  public void test_create_adviser_actor_when_user_pool_confirms_with_some_missing_attribute() throws Exception {
    TestRouteResult response = adviserCommandPost("/registeradviserfromuserpool", 
        new RegisterAdviserFromUserPool(ADVISER_USERNAME, INVESTOR_NAME + " " + LASTNAME, ADVISER_EMAIL,PUBLIC_ARN, COMPANY, ""));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    waitForProcessToComplete(5);
    assertThat(responseEntity).isInstanceOf(Done.class);    
  }
  
  @Test
  public void test_create_adviser_actor_when_user_pool_confirms_with_some_missing_attribute2() throws Exception {
    TestRouteResult response = commandPost("/registeradviserfromuserpool", 
        "{\"sub\":\"ecb42435-c0e8-4f46-b908-18b220f1b466\",\"phoneNumber\":\"\",\"name\":\"Akash Agarwal\",\"publicARN\":\"XAIUgi546\",\"company\":\"Wealth Technology & Services Pvt Ltd\",\"adviserUsername\":\"AkashAgarwal\",\"email\":\"akash@yopmail.com\"}");
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    waitForProcessToComplete(5);
    assertThat(responseEntity).isInstanceOf(Done.class);    
  }
  
  @Test
  public void create_investor_actor_when_user_pool_confirms_with_invalid_attributes() {
    TestRouteResult response = commandPost("/registerinvestorfromuserpool", 
        "{\"sub\":\"05d76fea-5392-4a0d-b2ab-1151c70f6bff\",\"name\":\"Akash Agarwal\",\"username\":\"AkashAgarwal\"}");
    Failed responseEntity = response.entity(unmarshaller(Failed.class));
    response.assertStatusCode(BAD_REQUEST).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Failed.class);    
  }
  
  private UnAuthenticatedAdviserCommand registerAdviserFromUserPool() {
    return new RegisterAdviserFromUserPool(ADVISER_USERNAME, INVESTOR_NAME + " " + LASTNAME, ADVISER_EMAIL,PUBLIC_ARN, COMPANY, PHONENUMBER);
  }

  private UnAuthenticatedUserCommand registerInvestorFromUserPool() throws ParseException {
    return new RegisterInvestorFromUserPool(INVESTOR_USERNAME.split("@")[0], INVESTOR_NAME + " " + LASTNAME, PHONENUMBER, INVESTOR_USERNAME);
  }

  private void waitForAdviseToGetProcessedCompletely(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000);
  }

  private TestRouteResult jsonStringPostToValidateEmptyInputParameterFlow(String url, String command) {
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), command));
  }

  private void assertOTPisReceived(int times) throws Exception {
    waitForProcessToComplete(1);
    verify(snsFacadeMock, times(times)).sendOTP(OTP_DUMMY + " is your One Time Password for WealthDesk", "91"+DUMMY_SUBSCRIBER_NUMBER);
  }
  
  private void assertSMSisReceived() throws IOException {
    verify(snsFacadeMock).sendSMSMessage("Hi " + INVESTOR_NAME + ", your KYC is updated with Client Code: " + CLIENT_CODE + " @ "
        + BROKER_COMPANY_NAME, "91"+DUMMY_SUBSCRIBER_NUMBER);
  }

  private TestRouteResult commandPost(String url, String jsonString) {    
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(),jsonString));
  }
  
  private TestRouteResult adviserCommandPost(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRouteResult authenticatedAdviserPost(String url, Object command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).addHeader(RawHeader.create("username", ADVISER_USERNAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRouteResult adviserCommandPost(String url, UnAuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  private TestRouteResult userUnAuthenticatedCommandGet(String url, String queryParameters) {
    if(queryParameters != null)
    return route().run(GET(url + (queryParameters.contains("?") ? queryParameters : "?" + queryParameters)));
    
    return route().run(GET(url));
  }

  private TestRouteResult userCommandPost(String url, UnAuthenticatedUserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRouteResult userCommandPost(String url, String jsonString) {
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRouteResult authenticatedPost(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(POST(url).addHeader(RawHeader.create("username", INVESTOR_USERNAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }
  
  private TestRouteResult authenticatedGet(String url, String queryString) {
    TestRouteResult response;
    if (queryString != "" || queryString != null) {
      response = route().run(GET(url + "?" + queryString)
          .addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME)));
    } else {
      response = route().run(GET(url).addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME)));
    }
    return response;
  }
  
  private TestRouteResult authenticatedDelete(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(DELETE(url).addHeader(RawHeader.create("username", INVESTOR_USERNAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }

  protected TestRouteResult getMethodForAuthenticatedAdviser(String url, String queryParameter) {
    if (queryParameter == "" || queryParameter == null)
      return route().run(GET(url).addHeader(RawHeader.create("username", ADVISER_USERNAME)));
    else
      return route().run(GET(url+"?"+queryParameter).addHeader(RawHeader.create("username", ADVISER_USERNAME)));
  }
  
  protected TestRouteResult getMethodForAuthenticatedUser(String url, String queryParameter) {
    if (queryParameter == "" || queryParameter == null)
      return route().run(GET(url).addHeader(RawHeader.create("username", INVESTOR_USERNAME)));
    else
      return route().run(GET(url+"?"+queryParameter).addHeader(RawHeader.create("username", INVESTOR_USERNAME)));
  }

  private TestRoute route() {
    return testRoute(service.appRoute());
  }

  protected void waitForViewToPopulate(int seconds) throws Exception {
    Thread.sleep(seconds * 1000);
    viewPopulater.blockingPopulateView(system, ActorMaterializer.create(system));
  }

  protected void waitForProcessToComplete(int seconds) throws Exception {
    Thread.sleep(seconds * 1000);
  }
}
