package com.wt.api.write;

import static akka.http.javadsl.marshallers.jackson.Jackson.unmarshaller;
import static akka.http.javadsl.model.HttpRequest.DELETE;
import static akka.http.javadsl.model.HttpRequest.PATCH;
import static akka.http.javadsl.model.HttpRequest.POST;
import static akka.http.javadsl.model.MediaTypes.APPLICATION_JSON;
import static akka.http.javadsl.model.StatusCodes.OK;
import static com.amazonaws.util.json.Jackson.toJsonString;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.DateUtils.lastDayEOD;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.not.wt.pkg.to.override.main.config.APISpecificTestAppConfiguration;
import com.wt.config.utils.WDProperties;
import com.wt.domain.InvestorApproval;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.UserFund;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.AuthenticatedUserCommand;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.events.AdviserCredentialsWithType;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.UserFundEvent;
import com.wt.onboarding.utils.CustomerOnboardingService;
import com.wt.utils.CoreDBTest;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;
import com.wt.utils.NotificationFacade;
import com.wt.utils.OTPUtils;
import com.wt.utils.ViewPopulater;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import akka.stream.ActorMaterializer;
import commons.Retry;
import lombok.extern.log4j.Log4j;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

@Log4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = APISpecificTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class CustomerOnboardingServiceShould extends JUnitRouteTest {

  private static final String TICKER7 = "6762-NSE-EQ";
  private static final String TICKER6 = "1342-NSE-EQ";
  private static final String TICKER5 = "12-NSE-EQ";
  private static final String TICKER4 = "3442-NSE-EQ";
  private static final String TICKER3 = "13432-NSE-EQ";
  private static final String TICKER2 = "1442-NSE-EQ";
  private static final double CURRENT_MARKET_PRICE = 1040d;
  private static final double CURRENT_MARKET_PRICE2 = 104d;
  private static final long serialVersionUID = 1L;
  private static final String TICKER = "130-NSE-EQ";
  private static final String TICKER_1 = "122-NSE-EQ";
  private static final String FUND_NAME = "FortOpportunityFund";
  private static final String FUND_DESCRIPTION = FUND_NAME + "Description";
  private static final String FUND_THEME = "Theme";
  private static final double FUND_MIN_SIP = 6500;
  private static final double FUND_MIN_LUMPSUM = 22500;
  private static final String FUND_RISK_LEVEL = "MEDIUM";
  private static final String ADVISER_USERNAME = "swapnil";
  private static final RegisterAdviserFromUserPool ADVISER = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi",
      "swapnil@bpwealth.com", "32publicId", "Fort Capital", "");
  private static final long STARTDATE = 1388082600000L;
  private static final long ENDDATE = 3471186600000L;
  private static final String DUMMY_DEVICE_TOKEN = "dummy-device-token";
  private static final String DUMMY_NOTIFICATION_TYPE = "Investment";
  private static final String DUMMY_NOTIFICATION_TITLE = "Fund Subscription";
  private static final String DUMMY_NOTIFICATION_MESSAGE = "Successfully subscribed to WeeklyPicks";
  private static final String DUMMY_USER_NAME = "test@gmail.com";

  private @Autowired ActorSystem system;
  private @Autowired ViewPopulater viewPopulater;
  private @Autowired WDService service;
  private @Autowired CoreDBTest dbTest;
  private @Autowired ActorMaterializer materializer;
  private @Autowired NotificationFacade snsFacade;
  private @Autowired OTPUtils otpGeneratorMock;
  private @Autowired @Qualifier("JournalProvider") JournalProvider journalProvider;
  private @Autowired CustomerOnboardingService customerOnboardingService;
  private @Autowired  @Qualifier("CognitoUserPoolForInvestor") AWSCognitoIdentityProvider investorUserPoolMock;
  private ActorRef universeRootActor;
  @SuppressWarnings("unused")
  private ActorRef equityOrderRootActor;
  private @Autowired WDActorSelections actorSelection;

  @Override
  public FiniteDuration awaitDuration() {
    return FiniteDuration.apply(1, TimeUnit.MINUTES);
  }

  @Before
  public void setupDB() throws Exception {
    dbTest.createSchema();
    service.createActors();
    environmentVariables.set("WT_MARKET_OPEN", "true");
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    equityOrderRootActor = system.actorOf(SpringExtProvider.get(system).props("EquityOrderRootActor"),
        "equity-order-aggregator");
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER, lastDayEOD(currentTimeInMillis()));
    updateInstrumentsMapOfEquityActor(TICKER, "ABC", 130);
    updateInstrumentsMapOfEquityActor(TICKER_1, "EFG", 122);
    updateInstrumentsMapOfEquityActor(TICKER2, "AEGISCHEM", 1442);
    updateInstrumentsMapOfEquityActor(TICKER3, "BEML", 13432);
    updateInstrumentsMapOfEquityActor(TICKER4, "GSFC", 3442);
    updateInstrumentsMapOfEquityActor(TICKER5, "HINDPETRO", 12);
    updateInstrumentsMapOfEquityActor(TICKER6, "ICICIBANK", 1342);
    updateInstrumentsMapOfEquityActor(TICKER7, "JKTYRE", 6762);
    doReturn(WDProperties.DUMMY_HASHCODE_FOR_USER_ACTIVATION).when(otpGeneratorMock).getUniqueHashCode();
    HashMap<String, String> additionalAttributes = new HashMap<String, String>();
    additionalAttributes.put("author", ADVISER_USERNAME);
    additionalAttributes.put("fund_name", FUND_NAME);
    additionalAttributes.put("title", DUMMY_NOTIFICATION_TITLE);
    additionalAttributes.put("type", DUMMY_NOTIFICATION_TYPE);
    doNothing().when(snsFacade).sendNotification(DUMMY_DEVICE_TOKEN, DUMMY_NOTIFICATION_MESSAGE, additionalAttributes);
    
    Mockito.when(investorUserPoolMock.adminGetUser(Mockito.any(AdminGetUserRequest.class)))
    .thenThrow(new UserNotFoundException("This user is not registered."));
    
    Mockito.when(investorUserPoolMock.adminCreateUser(Mockito.any(AdminCreateUserRequest.class)))
    .thenAnswer((args) -> {
      AdminCreateUserRequest request = (AdminCreateUserRequest) args.getArguments()[0];
      AdminCreateUserResult result = new AdminCreateUserResult()
          .withUser(new UserType()
                      .withAttributes(request.getUserAttributes())
                      .withEnabled(true)
                      .withUsername(request.getUsername())
                      .withUserCreateDate(new Date(DateUtils.todayBOD()))
                      .withUserLastModifiedDate(new Date(DateUtils.todayBOD())));
      return result;
    });
  }

  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
  
  @Rule
  public Retry retry = new Retry(5);

  @After
  public void deleteJournal() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    try {
      deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private void sendEODPriceToEquityActor(double currentMarketPrice, String token, long time) {
    universeRootActor.tell(
        new PriceWithPaf(new InstrumentPrice(currentMarketPrice, time, token), Pafs.withCumulativePaf(1.)),
        ActorRef.noSender());
  }
  
  @Test
  public void customer_onboarding_test_end_2_end() throws InterruptedException, ParseException, IOException, URISyntaxException {

    TestRouteResult response = post("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);

    response = post("/funds", new FloatFund(ADVISER_USERNAME, FUND_NAME, STARTDATE, ENDDATE, FUND_DESCRIPTION, FUND_THEME,
        FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    
    updateInstrumentsMapOfEquityActor(TICKER2, "AEGISCHEM", 1442);
    updateInstrumentsMapOfEquityActor(TICKER3, "BEML", 13432);
    updateInstrumentsMapOfEquityActor(TICKER4, "GSFC", 3442);
    updateInstrumentsMapOfEquityActor(TICKER5, "HINDPETRO", 12);
    updateInstrumentsMapOfEquityActor(TICKER6, "ICICIBANK", 1342);
    updateInstrumentsMapOfEquityActor(TICKER7, "JKTYRE", 6762);
    IssueAdvice command = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, "100001.0", TICKER2, 10.0, CURRENT_MARKET_PRICE2, 102, 102,432423l , 5234524523523l);
    executeAdviseCommand(command);
    command = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, "100002.0", TICKER3, 10.0, CURRENT_MARKET_PRICE2, 102, 102,432423l , 5234524523523l);
    executeAdviseCommand(command);
    command = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, "100004.0", TICKER4, 10.0, CURRENT_MARKET_PRICE2, 102, 102,432423l , 5234524523523l);
    executeAdviseCommand(command);
    command = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, "100005.0", TICKER5, 10.0, CURRENT_MARKET_PRICE2, 102, 102,432423l , 5234524523523l);
    executeAdviseCommand(command);
    command = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, "100006.0", TICKER6, 10.0, CURRENT_MARKET_PRICE2, 102, 102,432423l , 5234524523523l);
    executeAdviseCommand(command);
    command = new IssueAdvice(ADVISER_USERNAME, FUND_NAME, "100010.0", TICKER7, 10.0, CURRENT_MARKET_PRICE2, 102, 102,432423l , 5234524523523l);
    executeAdviseCommand(command);
    
    
    String filenamePath = new File(getClass().getClassLoader().getResource("com//wt//customer//onboarding//").getFile()).getPath();
    Files.newDirectoryStream(Paths.get(filenamePath), path -> path.toFile().isFile()).forEach(fileName -> {
      try {
        File file = new File(fileName.toAbsolutePath().toString()); //fileName.toFile();
        customerOnboardingService.processAdviserClientFile(file);
        customerOnboardingService.computeFundCompositions();
        if (customerOnboardingService.validateFundAndUserAdvises(fileName.toFile().getName())) {
          customerOnboardingService.populateClientCodeWithSymbolToPriceMap();
          customerOnboardingService.startOnBoardingProcess();
        }
        
        Thread.sleep(10*1000*2);
        
        UserFundCompositeKey userFundCompositeKey = new UserFundCompositeKey("jatin.kadakia", "FortOpportunityFund", "swapnil", REAL);
        List<UserFundEvent> UFEvents = journalProvider
        .fetchEventsByPersistenceId(
            userFundCompositeKey.persistenceId())
        .stream().filter(evt -> evt instanceof UserFundEvent).map(evt -> (UserFundEvent) evt)
        .collect(Collectors.toList());
        
        UserFund userFund = new UserFund();
        UFEvents.stream().forEach(userFundEvent -> userFund .update(userFundEvent));
        
        Map<String, Double> nbShares = userFund.getAdviseIdToExecutedShares();
        assertTrue(nbShares.get("100010.0").equals(345.0));
        assertTrue(nbShares.get("100001.0").equals(190.0));
        assertTrue(nbShares.get("100002.0").equals(40.0));
        assertTrue(nbShares.get("100004.0").equals(400.0));
        assertTrue(nbShares.get("100005.0").equals(130.0));
        assertTrue(nbShares.get("100006.0").equals(167.0));
        
        Thread.sleep(10*1000);
        fileName.toFile().delete();
      } catch (Exception e) {
        log.error("Unable to read " + fileName.toFile().getName() + " due to " + e.getMessage() + " Exception type: " + e.getStackTrace());
      }
    });

  }

  private void executeAdviseCommand(IssueAdvice command) throws InterruptedException {
    actorSelection.advisersRootActor().tell(command, ActorRef.noSender());
    waitForProcessToComplete(5);
  }

  private RegisterAdviserFromUserPool registerAdviser() {
    return new RegisterAdviserFromUserPool(ADVISER.getAdviserUsername(), ADVISER.getName(), ADVISER.getEmail(),
        ADVISER.getPublicARN(), ADVISER.getCompany(), ADVISER.getPhoneNumber());
  }
  
  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(
        new UpdateEquityInstrumentSymbol(String.valueOf(token),"",symbol,ticker,"","NSE","EQ"),ActorRef.noSender());

  }
  
  private void waitForProcessToComplete(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000);
  }

  protected TestRouteResult post(String url, UnAuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult post(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  protected TestRouteResult patch(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(PATCH(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }
  
  protected TestRouteResult userCommandPost(String url, AuthenticatedUserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  protected TestRouteResult postAuthUserCommand(String url, InvestorApproval command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(POST(url).addHeader(RawHeader.create("username", DUMMY_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }
  
  protected TestRouteResult deleteAuthUserCommand(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(DELETE(url).addHeader(RawHeader.create("username", DUMMY_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }

  protected void waitForViewToPopulate(int seconds) throws Exception {
    Thread.sleep(seconds * 1000);
    viewPopulater.blockingPopulateView(system, materializer);
  }

  private TestRoute route() {
    return testRoute(service.appRoute());
  }

}
