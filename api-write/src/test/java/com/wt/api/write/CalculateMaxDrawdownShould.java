package com.wt.api.write;

import static akka.http.javadsl.marshallers.jackson.Jackson.unmarshaller;
import static akka.http.javadsl.model.HttpRequest.GET;
import static akka.http.javadsl.model.HttpRequest.POST;
import static akka.http.javadsl.model.MediaTypes.APPLICATION_JSON;
import static akka.http.javadsl.model.StatusCodes.OK;
import static com.amazonaws.util.json.Jackson.toJsonString;
import static com.wt.domain.ApprovalResponse.Approve;
import static com.wt.domain.InvestingMode.REAL;
import static com.wt.domain.InvestingMode.VIRTUAL;
import static com.wt.domain.Purpose.EQUITYSUBSCRIPTION;
import static com.wt.utils.DateUtils.currentTimeInMillis;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;
import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.not.wt.pkg.to.override.main.config.UserSpecificTestAppConfiguration;
import com.wt.config.utils.WDProperties;
import com.wt.domain.FundWithAdviserAndInvestingMode;
import com.wt.domain.InvestingMode;
import com.wt.domain.InvestorApproval;
import com.wt.domain.PriceWithPaf;
import com.wt.domain.UserFundCompositeKey;
import com.wt.domain.Wallet;
import com.wt.domain.WealthCode;
import com.wt.domain.repo.UserRepository;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.BrokerAuthInformation;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;
import com.wt.domain.write.commands.UpdateEquityInstrumentSymbol;
import com.wt.domain.write.events.AdviserCredentialsWithType;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.FundAdviceTradeExecuted;
import com.wt.domain.write.events.InstrumentPrice;
import com.wt.domain.write.events.MarginReceived;
import com.wt.domain.write.events.Pafs;
import com.wt.domain.write.events.UserFundMaxDrawdownCalculated;
import com.wt.utils.CoreDBTest;
import com.wt.utils.DateUtils;
import com.wt.utils.JournalProvider;
import com.wt.utils.OTPUtils;
import com.wt.utils.ViewPopulater;
import com.wt.utils.WalletFactory;
import com.wt.utils.akka.WDActorSelections;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Terminated;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import akka.http.javadsl.testkit.TestRouteResult;
import akka.persistence.fsm.PersistentFSM.StateChangeEvent;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import commons.Retry;
import scala.concurrent.CanAwait;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UserSpecificTestAppConfiguration.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class CalculateMaxDrawdownShould extends JUnitRouteTest {
  private static final long serialVersionUID = 1L;
  private static final RegisterAdviserFromUserPool ADVISER = new RegisterAdviserFromUserPool("swapnil", "Swapnil Joshi",
      "swapnil@bpwealth.com", "123publicId", "Fort Capital", "");
  private static final double ENTRYPRICE = 1030d;
  private static final String TICKER1 = "IN1-NSE-EQ";
  private static final double ALLOCATION1 = 20.0;
  private static final int TOKEN_1 = 133;
  private static final String SYMBOL_1 = "ACC";
  private static final double CURRENT_MARKET_PRICE = 1040.;
  private static final String ADVISER_IDENTITY = "swapnil";
  private static final String FUND_NAME = "FortOpportunity";
  private static final long STARTDATE = 1388082600000L;
  private static final long ENDDATE = 3471186600000L;
  private static final String FUND_DESCRIPTION = FUND_NAME + "Description";
  private static final String FUND_THEME = "Theme";
  private static final double FUND_MIN_SIP = 20000.;
  private static final double FUND_MIN_LUMPSUM = 200000.;
  private static final String FUND_RISK_LEVEL = "MEDIUM";
  private static final String ADVISEID1 = "1a";
  private static final long ISSUETIME = 0;
  private static final long ENTRYTIME = 1;
  private static final double EXITPRICE = 1090.;
  private static final long EXITTIME = 3;
  private static final String SUBSCRIPTION_USER_NAME = "subscriber";
  private static final String SUBSCRIPTION_USER_EMAIL = "subscriber@yopmail.com";
  private static final String INVESTOR_NAME = "FirstName LastName";
  private static final double SUBSCRIPTION_LUMPSUM = 300000.;
  private static final double SUBSCRIPTION_SIPAMOUNT = 0.;
  private static final String SUBSCRIPTION_SIP_DATE = "";
  private static final String SUBSCRIPTION_WEALTHCODE = "2108";
  private static final String CLIENT_CODE = "2035";
  private static final String PAN_CARD = "AQRTY2343E";
  private static final String BROKER_NAME = "BPWEALTH";
  private static final String PHONE_NUMBER = "8347234044";
  private static final double MARGIN_AMOUNT = 5000000.;
  private static final String BASKETORDERID_FOR_TEST = valueOf(randomUUID().toString() + valueOf(currentTimeInMillis())) ;

  @Autowired
  private ActorSystem system;
  @Autowired
  private WDService service;
  @Autowired
  private ViewPopulater viewPopulater;
  @Autowired
  private CoreDBTest dbTest;
  @Autowired
  private OTPUtils otpGeneratorMock;
  @Autowired
  public Wallet moneyWallet;
  @Autowired
  public WalletFactory walletFactory;
  @Autowired
  private JournalProvider journalProvider;
  @Autowired
  private WDActorSelections actorSelections;
  @Autowired
  private ActorMaterializer materializer;
  @Autowired
  private UserRepository userRepository;
  private ActorRef universeRootActor;
  @SuppressWarnings("unused")
  private ActorRef equityOrderRootActor;
  @Autowired
  @Qualifier("CognitoUserPoolForInvestor")
  private AWSCognitoIdentityProvider investorUserPoolMock;
  @Rule
  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

  @Rule
  public Retry retry = new Retry(5);
  
  @Override
  public FiniteDuration awaitDuration() {
    return FiniteDuration.apply(1, TimeUnit.MINUTES);
  }

  @Before
  public void setupDB() throws Exception {
    dbTest.createSchema();
    service.createActors();
    universeRootActor = system.actorOf(SpringExtProvider.get(system).props("UniverseRootActor"), "universe-root-actor");
    equityOrderRootActor = system.actorOf(SpringExtProvider.get(system).props("EquityOrderRootActor"),
        "equity-order-aggregator");

    doReturn(WDProperties.DUMMY_HASHCODE_FOR_USER_ACTIVATION).when(otpGeneratorMock).getUniqueHashCode();
    
    doReturn(moneyWallet).when(walletFactory).getWallet(Mockito.anyString(), Mockito.any());
    
    updateInstrumentsMapOfEquityActor(TICKER1, SYMBOL_1, TOKEN_1);
    sendEODPriceToEquityActor(CURRENT_MARKET_PRICE, TICKER1);
    environmentVariables.set("WT_MARKET_OPEN", "true");
    
    Mockito.when(investorUserPoolMock.adminGetUser(Mockito.any(AdminGetUserRequest.class)))
    .thenThrow(new UserNotFoundException("This user is not registered."));
    
    Mockito.when(investorUserPoolMock.adminCreateUser(Mockito.any(AdminCreateUserRequest.class)))
    .thenAnswer((args) -> {
      AdminCreateUserRequest request = (AdminCreateUserRequest) args.getArguments()[0];
      AdminCreateUserResult result = new AdminCreateUserResult()
          .withUser(new UserType()
                      .withAttributes(request.getUserAttributes())
                      .withEnabled(true)
                      .withUsername(request.getUsername())
                      .withUserCreateDate(new Date(DateUtils.todayBOD()))
                      .withUserLastModifiedDate(new Date(DateUtils.todayBOD())));
      return result;
    });
  }

  @After
  public void shutDown() throws Exception {
    Future<Terminated> terminate = system.terminate();
    terminate.result(Duration.Inf(), new CanAwait() {
    });

    try {
      deleteDirectory(new File("build/journal"));
      deleteDirectory(new File("build/snapshots"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void updateInstrumentsMapOfEquityActor(String ticker, String symbol, int token) {
    universeRootActor.tell(new UpdateEquityInstrumentSymbol(String.valueOf(token), "", symbol, ticker, "", "NSE", "EQ"),
        ActorRef.noSender());
  }

  private void sendEODPriceToEquityActor(double currentMarketPrice, String token) {
    universeRootActor
        .tell(new PriceWithPaf(new InstrumentPrice(currentMarketPrice, DateUtils.currentTimeInMillis(), token),
            Pafs.withCumulativePaf(1.)), ActorRef.noSender());
  }

  @Test
  public void persist_user_fund_max_drawdown_with_correct_investing_mode() throws Exception {
    
    initiateAdviserRegistration();

    initiateAdviserFundLaunch();

    initiateIssueAdvise();

    registerUserAndActivate();

    subscribeRealUserToTheFundWith(VIRTUAL);

    approveKycToAllowRealMoneySubscription();

    updateWealthCodeOfTheKycedUser();

    updateMarginOfTheKycedUser();

    subscribeRealUserToTheFundWith(REAL);

    initiateEODProcess();

    assertUserFundMaxDrawdownHasConsistentInvestingMode(
        new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_IDENTITY, REAL));
    
    assertUserFundMaxDrawdownHasConsistentInvestingMode(
        new UserFundCompositeKey(SUBSCRIPTION_USER_NAME, FUND_NAME, ADVISER_IDENTITY, VIRTUAL));
  }

  private void assertUserFundMaxDrawdownHasConsistentInvestingMode(UserFundCompositeKey userFundCompositeKey)
      throws InterruptedException, ExecutionException, TimeoutException {
    CompletionStage<UserFundMaxDrawdownCalculated> futureUserFundMaxDrawdown = journalProvider
        .currentEventsSourceForPersistenceId(userFundCompositeKey.persistenceId())
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof FundAdviceTradeExecuted)
        .filterNot(eventEnvelope -> eventEnvelope.event() instanceof StateChangeEvent)
        .filter(eventEnvelope -> eventEnvelope.event() instanceof UserFundMaxDrawdownCalculated)
        .map(userFundEvent -> ((UserFundMaxDrawdownCalculated) userFundEvent.event()))
        .runWith(Sink.head(), materializer);

    assertThat(futureUserFundMaxDrawdown.toCompletableFuture().get(2, TimeUnit.SECONDS).getInvestingMode())
        .isEqualTo(userFundCompositeKey.getInvestingMode());
  }

  private void initiateAdviserRegistration() {
    TestRouteResult response = post("/registeradviserfromuserpool", registerAdviser());
    Done responseEntity = response.entity(unmarshaller(AdviserCredentialsWithType.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Adviser Registered");
  }

  private void initiateAdviserFundLaunch() {
    TestRouteResult response = post("/funds", new FloatFund(ADVISER_IDENTITY, FUND_NAME, STARTDATE, ENDDATE,
        FUND_DESCRIPTION, FUND_THEME, FUND_MIN_SIP, FUND_MIN_LUMPSUM, FUND_RISK_LEVEL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo(" fund floated.");
  }

  private void initiateIssueAdvise() throws InterruptedException {
    IssueAdvice firstAdvice = new IssueAdvice(ADVISER_IDENTITY, FUND_NAME, ADVISEID1, TICKER1, ALLOCATION1, ENTRYPRICE,
        ENTRYTIME, EXITPRICE, EXITTIME, ISSUETIME);
    TestRouteResult response = post("/issueadvise", firstAdvice);
    waitForProcessToComplete(3);
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Your advise has been received, you will be soon notifed when process completes");
  }

  private void approveKycToAllowRealMoneySubscription() {
    TestRouteResult response = userCommandPost("/approvekyc",
        new KycApproveCommand(CLIENT_CODE, PAN_CARD, SUBSCRIPTION_USER_NAME, BROKER_NAME, PHONE_NUMBER, SUBSCRIPTION_USER_EMAIL));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("KYC Process Completed");
  }

  private void updateWealthCodeOfTheKycedUser() {
    TestRouteResult response = postAuthUserCommand("/wealthcode", new WealthCode(SUBSCRIPTION_WEALTHCODE));
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage()).isEqualTo("Wealth Code updated");
  }

  private void updateMarginOfTheKycedUser() {
    actorSelections.userRootActor().tell(new MarginReceived(MARGIN_AMOUNT, SUBSCRIPTION_USER_NAME, REAL),
        ActorRef.noSender());
  }

  private void subscribeRealUserToTheFundWith(InvestingMode investingMode) throws Exception {
    
    TestRouteResult response = authenticatedGet("/subscription", "adviserUsername=" + ADVISER.getAdviserUsername() + "&fundName=" + FUND_NAME + 
        "&sipAmount="  + SUBSCRIPTION_SIPAMOUNT + "&lumpSumAmount=" + SUBSCRIPTION_LUMPSUM + "&sipDate=" +
        SUBSCRIPTION_SIP_DATE + "&investingMode=" + investingMode);
    
    waitForProcessToComplete(2);
    response = postAuthUserCommand("/subscription", new InvestorApproval(FUND_NAME, ADVISER_IDENTITY, SUBSCRIPTION_WEALTHCODE,
        investingMode, BASKETORDERID_FOR_TEST, EQUITYSUBSCRIPTION, Approve, BrokerAuthInformation.withNoBrokerAuth()));
    
    Done responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity.getMessage())
        .isEqualTo("Subscription in process. You will receive a notification once its done.");

    waitForViewToPopulate(2);

    assertThat(userRepository.findByEmail(SUBSCRIPTION_USER_EMAIL).orElse(null).getAdvisorToFunds()
        .contains(new FundWithAdviserAndInvestingMode(FUND_NAME, ADVISER_IDENTITY, investingMode.getInvestingMode()))).isTrue();
  }

  private void initiateEODProcess() throws Exception {
    actorSelections.userRootActor().tell(new EODCompleted(), ActorRef.noSender());
    waitForProcessToComplete(1);
  }

  private RegisterAdviserFromUserPool registerAdviser() {
    return ADVISER;
  }

  private void registerUserAndActivate() throws Exception {
    TestRouteResult response;
    Done responseEntity;
    response = userCommandPost("/registerinvestorfromuserpool",
        new RegisterInvestorFromUserPool(SUBSCRIPTION_USER_NAME, INVESTOR_NAME, "", SUBSCRIPTION_USER_EMAIL));
    responseEntity = response.entity(unmarshaller(Done.class));
    response.assertStatusCode(OK).assertMediaType("application/json");
    assertThat(responseEntity).isInstanceOf(Done.class);
    waitForViewToPopulate(1);
  }
  
  private TestRouteResult authenticatedGet(String url, String queryString) {
    TestRouteResult response;
    if (queryString != "" || queryString != null) {
      response = route().run(GET(url + "?" + queryString)
          .addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME)));
    } else {
      response = route().run(GET(url).addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME)));
    }
    return response;
  }
  
  private TestRouteResult postAuthUserCommand(String url, Object command) {
    String jsonString = toJsonString(command);
    TestRouteResult run = route().run(POST(url).addHeader(RawHeader.create("username", SUBSCRIPTION_USER_NAME))
        .withEntity(APPLICATION_JSON.toContentType(), jsonString));
    return run;
  }

  private TestRouteResult post(String url, UnAuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRouteResult post(String url, AuthenticatedAdviserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRouteResult userCommandPost(String url, UnAuthenticatedUserCommand command) {
    String jsonString = toJsonString(command);
    return route().run(POST(url).withEntity(APPLICATION_JSON.toContentType(), jsonString));
  }

  private TestRoute route() {
    return testRoute(service.appRoute());
  }

  private void waitForViewToPopulate(int seconds) throws Exception {
    Thread.sleep(seconds * 1000);
    viewPopulater.blockingPopulateView(system, materializer);
  }

  private void waitForProcessToComplete(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000);
  }
}
