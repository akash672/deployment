#!/bin/bash
nohup java -cp ".:./lib/equity-ctcl-service/*" -Dlog4j.configuration=EquityCTCLlog4j.properties -DappConfig=EquityCTCL-application.conf com.wt.domain.write.EquityCtclService >> LOGS/equity_ctcl_service_console.log 2>&1 &
echo $! > equity_ctcl_pid