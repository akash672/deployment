#!/bin/bash
nohup ./run-node.sh --run-projections=all --start-standard-projections --int-ip=127.0.0.1 --ext-ip=127.0.0.1 --ext-http-port=8114 --ext-tcp-port=7114 --ext-http-prefixes="http://*:8114/" --add-interface-prefixes=false --db ./mfExecutiondb 2>&1 &
echo $! > pid