#!/bin/bash
nohup java -cp ".:./lib/viewpopulator-rds/*" -Dlog4j.configuration=viewpopulatorrdslog4j.properties -DscheduleViewpopWrites=true -DappConfig=ViewpopulatorRds-application.conf com.wt.api.view.ViewPopulatorRdsService >> LOGS/viewpopulator_rds_service_console.log 2>&1 &
echo $! > viewpopulator-rds_pid
