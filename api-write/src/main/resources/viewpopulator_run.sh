#!/bin/bash
nohup java -cp ".:./lib/viewpopulator/*" -Dlog4j.configuration=viewpopulatorlog4j.properties -DscheduleViewpopWrites=true -DappConfig=Viewpopulator-application.conf com.wt.api.write.ViewPopulatorService >> LOGS/viewpopulator_service_console.log 2>&1 &
echo $! > viewpopulator_pid
