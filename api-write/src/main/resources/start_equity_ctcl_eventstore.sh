#!/bin/bash
nohup ./run-node.sh --run-projections=all --start-standard-projections --int-ip=127.0.0.1 --ext-ip=127.0.0.1 --ext-http-port=6114 --ext-tcp-port=5114 --ext-http-prefixes="http://*:6114/" --add-interface-prefixes=false --db ./equityCtcldb 2>&1 &
echo $! > pid