#!/bin/bash
nohup java -cp ".:./lib/realtimeservice/*" -Dlog4j.configuration=realtimelog4j.properties -DappConfig=RealTime-application.conf com.wt.domain.write.RealTimeService >> LOGS/realtime_service_console.log 2>&1 &
echo $! > realtime_pid