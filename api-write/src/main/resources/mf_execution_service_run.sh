#!/bin/bash
nohup java -cp ".:./lib/mf-execution-service/*" -Dlog4j.configuration=mfExecutionlog4j.properties -DappConfig=MFExecution-application.conf com.wt.domain.write.MFExecutionService >> LOGS/mf_execution_service_console.log 2>&1 &
echo $! > mf_execution_service