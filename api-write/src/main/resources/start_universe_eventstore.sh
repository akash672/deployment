#!/bin/bash
nohup ./run-node.sh --run-projections=all --start-standard-projections --int-ip=127.0.0.1 --ext-ip=127.0.0.1 --ext-http-port=4114 --ext-tcp-port=3114 --ext-http-prefixes="http://*:4114/" --add-interface-prefixes=false --db ./universedb 2>&1 &
echo $! > pid