#!/bin/bash
nohup java -cp ".:./lib/*" -Dlog4j.configuration=universelog4j.properties -DscheduleEODJobs=true -DscheduleBODJobs=true -DscheduleTicks=true -DappConfig=Universe-application.conf com.wt.domain.write.UniverseService >> LOGS/universe_service_console.log 2>&1 &
echo $! > universe_pid