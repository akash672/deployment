package com.wt.api.write;

import static akka.http.javadsl.model.StatusCodes.INTERNAL_SERVER_ERROR;
import static akka.http.javadsl.server.Directives.completeWithFuture;
import static com.wt.domain.write.events.Failed.failed;
import static com.wt.utils.akka.ActorResultWaiter.askAndWaitForResult;

import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wt.domain.write.commands.AdviserCommand;
import com.wt.domain.write.commands.AdviserCorporateEvents;
import com.wt.domain.write.commands.AssetClassCommand;
import com.wt.domain.write.commands.AuthenticatedAdviserCommand;
import com.wt.domain.write.commands.AuthenticatedUserCommand;
import com.wt.domain.write.commands.BODCompleted;
import com.wt.domain.write.commands.CalculateHistoricalFundPerformance;
import com.wt.domain.write.commands.CalculateUserHistoricalPerformance;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.ForceSaveSnapshot;
import com.wt.domain.write.commands.SendFollowingCommandToUserFund;
import com.wt.domain.write.commands.UnAuthenticatedAdviserCommand;
import com.wt.domain.write.commands.UnAuthenticatedUserCommand;
import com.wt.domain.write.events.Done;
import com.wt.domain.write.events.MergerDemergerEvent;
import com.wt.domain.write.events.Result;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.Route;

@Service("CommandHandlerRoute")
public class CommandForwarderRoute {
  private ObjectMapper mapper = new ObjectMapper();

  public Route handle(Object command, ActorRef adviserRootActor, ActorRef userRootActor) {
    CompletableFuture<HttpResponse> futureResult = CompletableFuture.supplyAsync(() -> {
      try {
        Result status = handleCommand(command, adviserRootActor, userRootActor);
        return processResult(status);
      } catch (Exception e) {
        return HttpResponse.create().withStatus(INTERNAL_SERVER_ERROR).withEntity(e.getMessage());
      }
    });

    return completeWithFuture(futureResult);
  }

  private Result handleCommand(Object command, ActorRef adviserRootActor, ActorRef userRootActor)
      throws Exception, JsonProcessingException {
    if (isAdviserCommand(command))
      return (Result) askAndWaitForResult(adviserRootActor, command);
    else if (isUserCommand(command))
      return (Result) askAndWaitForResult(userRootActor, command);
    else if (isValidCommand(command))
      return (Result) sendToRequiredActor(command, adviserRootActor, userRootActor);
    else
      return failed(command.toString(), "Invalid Command Type", StatusCodes.BAD_REQUEST);
  }

  private Result sendToRequiredActor(Object command, ActorRef adviserRoot, ActorRef userRoot) {
    if (command instanceof EODCompleted || command instanceof BODCompleted ) {
      adviserRoot.tell(command, ActorRef.noSender());
      userRoot.tell(command, ActorRef.noSender());
      return new Done("Admin", command + " has been sent to respective actors");
    } 
    else if (command instanceof ForceSaveSnapshot) {
      userRoot.tell(command, ActorRef.noSender());
      return new Done("Admin", command + " has been sent to respective actors");
    } 
    return null;
  }

  private boolean isValidCommand(Object command) {
    if (command instanceof EODCompleted || command instanceof BODCompleted || command instanceof ForceSaveSnapshot) {
      return true;
    }
    return false;
  }

  private boolean isAdviserCommand(Object command) {
    return command instanceof AdviserCommand || command instanceof CalculateHistoricalFundPerformance
        || command instanceof UnAuthenticatedAdviserCommand || command instanceof AuthenticatedAdviserCommand
        || command instanceof AdviserCorporateEvents ;
  }

  private boolean isUserCommand(Object command) {
    return command instanceof UnAuthenticatedUserCommand || command instanceof AuthenticatedUserCommand
        || command instanceof CalculateUserHistoricalPerformance || command instanceof MergerDemergerEvent
        || command instanceof SendFollowingCommandToUserFund;
  }
  
  private boolean isUniverseCommand(Object command) {
     return command instanceof AssetClassCommand;
  }

  private HttpResponse processResult(Result status) throws JsonProcessingException {
    return HttpResponse.create().withStatus(status.getStatusCode()).withEntity(ContentTypes.APPLICATION_JSON,
        mapper.writeValueAsBytes(status));
  }

  public Route handleUniverseCommand(Object command, ActorSelection universeRootActor) {
    CompletableFuture<HttpResponse> futureResult = CompletableFuture.supplyAsync(() -> {
      try {
        Result status = handleCommand(command, universeRootActor);
        return processResult(status);
      } catch (Exception e) {
        return HttpResponse.create().withStatus(INTERNAL_SERVER_ERROR).withEntity(e.getMessage());
      }
    });

    return completeWithFuture(futureResult);
  }

  private Result handleCommand(Object command, ActorSelection universeRootActor) {
    if (isUniverseCommand(command))
      return (Result) sendToUniverseRootActor(command, universeRootActor);
    else
      return failed(command.toString(), "Invalid Command Type", StatusCodes.BAD_REQUEST);
  }

  private Result sendToUniverseRootActor(Object command, ActorSelection universeRootActor) {
    universeRootActor.tell(command, ActorRef.noSender());
    return new Done("subscribed", "Command has been sent to universe Actor");
  }
}
