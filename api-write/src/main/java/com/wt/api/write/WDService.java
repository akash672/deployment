package com.wt.api.write;

import static akka.actor.ActorRef.noSender;
import static akka.http.javadsl.marshallers.jackson.Jackson.unmarshaller;
import static akka.http.javadsl.model.ws.TextMessage.create;
import static akka.http.javadsl.server.Directives.delete;
import static akka.http.javadsl.server.Directives.entity;
import static akka.http.javadsl.server.Directives.get;
import static akka.http.javadsl.server.Directives.handleWebSocketMessages;
import static akka.http.javadsl.server.Directives.headerValueByName;
import static akka.http.javadsl.server.Directives.parameter;
import static akka.http.javadsl.server.Directives.patch;
import static akka.http.javadsl.server.Directives.path;
import static akka.http.javadsl.server.Directives.post;
import static akka.http.javadsl.server.Directives.route;
import static akka.stream.javadsl.Flow.fromSinkAndSource;
import static com.wt.domain.Purpose.PARTIALWITHDRAWAL;
import static com.wt.domain.Purpose.UNSUBSCRIPTION;
import static com.wt.utils.akka.SpringExtension.SpringExtProvider;
import static java.lang.Double.parseDouble;
import static java.lang.Runtime.getRuntime;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wt.cloud.monitoring.ServiceStatusReporter;
import com.wt.config.utils.WDProperties;
import com.wt.domain.AddCashToUserFund;
import com.wt.domain.AddLumpSum;
import com.wt.domain.InvestingMode;
import com.wt.domain.InvestorApproval;
import com.wt.domain.Outgoing;
import com.wt.domain.Purpose;
import com.wt.domain.ReceivedUserApprovalResponse;
import com.wt.domain.WealthCode;
import com.wt.domain.write.UpdateKycCommand;
import com.wt.domain.write.commands.AcceptUserAcceptanceMode;
import com.wt.domain.write.commands.AcceptUserApprovalResponse;
import com.wt.domain.write.commands.AddLumpsumToFund;
import com.wt.domain.write.commands.AddMoreAllocationToAnExistingAdvice;
import com.wt.domain.write.commands.AdviserCorporateEventPrimaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSecondaryBuy;
import com.wt.domain.write.commands.AdviserCorporateEventSell;
import com.wt.domain.write.commands.BODCompleted;
import com.wt.domain.write.commands.CalculateHistoricalFundPerformance;
import com.wt.domain.write.commands.CalculateUserHistoricalPerformance;
import com.wt.domain.write.commands.CloseAdvise;
import com.wt.domain.write.commands.CloseFund;
import com.wt.domain.write.commands.DeviceToken;
import com.wt.domain.write.commands.EODCompleted;
import com.wt.domain.write.commands.ExecutePostMarketUserCommands;
import com.wt.domain.write.commands.FetchAllUsersPendingFundRebalancingApprovals;
import com.wt.domain.write.commands.FloatFund;
import com.wt.domain.write.commands.ForceSaveSnapshot;
import com.wt.domain.write.commands.FundActionApproved;
import com.wt.domain.write.commands.GetAdvises;
import com.wt.domain.write.commands.GetFundRebalancingView;
import com.wt.domain.write.commands.GetInvestorName;
import com.wt.domain.write.commands.GetPendingUserFundApprovals;
import com.wt.domain.write.commands.GetUserAllInvestmentStatus;
import com.wt.domain.write.commands.GetUserCurrentApprovalStatus;
import com.wt.domain.write.commands.GetUserFundRejectedAdvisesList;
import com.wt.domain.write.commands.GetUserFundStatus;
import com.wt.domain.write.commands.GetUserPendingRebalancingList;
import com.wt.domain.write.commands.GetUserRejectedAdvisesList;
import com.wt.domain.write.commands.GetUserRoleStatus;
import com.wt.domain.write.commands.GetUserSecurityQuestions;
import com.wt.domain.write.commands.GetWalletMoney;
import com.wt.domain.write.commands.IssueAdvice;
import com.wt.domain.write.commands.KycApproveCommand;
import com.wt.domain.write.commands.PostDeviceToken;
import com.wt.domain.write.commands.ReceivedUserAcceptanceMode;
import com.wt.domain.write.commands.RegisterAdviserFromUserPool;
import com.wt.domain.write.commands.RegisterInvestorFromUserPool;
import com.wt.domain.write.commands.RetryRejectedUserAdvise;
import com.wt.domain.write.commands.SendFollowingCommandToUserFund;
import com.wt.domain.write.commands.SendOTP;
import com.wt.domain.write.commands.StopFetchingAllUsersPendingFundRebalancingApprovals;
import com.wt.domain.write.commands.SubmitSecurityAnswers;
import com.wt.domain.write.commands.SubmitUserAnswers;
import com.wt.domain.write.commands.SubscribeToFund;
import com.wt.domain.write.commands.UnsubscribeToFund;
import com.wt.domain.write.commands.UpdateAdviseRecommendedEntryPrice;
import com.wt.domain.write.commands.UpdateFundAttribute;
import com.wt.domain.write.commands.UpdateRecommendedEntryPrice;
import com.wt.domain.write.commands.UpdateUserFundCash;
import com.wt.domain.write.commands.UpdateWealthCode;
import com.wt.domain.write.commands.ValidateOTP;
import com.wt.domain.write.commands.WithdrawalFromFund;
import com.wt.domain.write.events.RetryRejectedAdvise;
import com.wt.domain.write.events.SpecialCircumstancesBuy;
import com.wt.domain.write.events.SpecialCircumstancesSell;
import com.wt.domain.write.events.SpecificUserMergerDemergerPrimaryBuy;
import com.wt.domain.write.events.SpecificUserMergerDemergerSecondaryBuy;
import com.wt.domain.write.events.SpecificUserMergerDemergerSellEvent;
import com.wt.utils.CoreAppConfiguration;
import com.wt.utils.DateUtils;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.ConnectionContext;
import akka.http.javadsl.Http;
import akka.http.javadsl.HttpsConnectionContext;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.server.Directives;
import akka.http.javadsl.server.Route;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class WDService {
  private static AnnotationConfigApplicationContext ctx;

  private ActorSystem system;
  private ActorRef adviserRootActor;
  private ActorRef userRootActor;
  @SuppressWarnings("unused")
  private ActorRef rdsFacadeRootActor;
  @SuppressWarnings("unused")
  private ActorRef activeInstrumentTrackerActor;
  private ActorRef analyticsRootActor; 
  private CommandForwarderRoute commandForwarderRoute;
  private WDProperties properties;
  private ServiceStatusReporter serviceStatusReporter;

  @Autowired
  public WDService(ActorSystem system, CommandForwarderRoute commandForwarderRoute,
      WDProperties properties, ServiceStatusReporter serviceStatusReporter) throws Exception {
    super();
    this.system = system;
    this.commandForwarderRoute = commandForwarderRoute;
    this.properties = properties;
    this.serviceStatusReporter = serviceStatusReporter;
  }
  
  public static void main (String[] args) throws Exception {
    ctx = new AnnotationConfigApplicationContext(CoreAppConfiguration.class);
    WDService service = ctx.getBean(WDService.class);
    service.start();
  }

  public Route appRoute() {
    return route(
        registerAdviserFromUserPool(), 
        registerInvestorFromUserPool(),
        approvekyc(),
        issueAdvise(),
        addMoreAllocationToAnExistingAdvice(),
        closeAdvise(), 
        floatFund(),
        closeFund(),
        updateFundAttribute(),
        getFundRebalancingView(),
        calculateFundPerformance(),
        calculateUserPerformance(),
        getAdvises(),
        subscriptionApproval(),
        updateUserFundCash(),
        subscribe(), 
        addLumpsum(),
        withdraw(),
        withdrawalResponse(),
        unsubscribe(), 
        unsubscriptionApproval(),
        retryRejectedAdvise(),
        sendOTP(),
        sendForceSaveSnapshot(),
        updateNumber(), 
        updateWealthCode(), 
        userAnswers(), 
        securityAnswers(),
        securityQuestions(),
        getUserRole(),
        acceptUserApprovalResponse(),
        toggleAutoApproval(),
        getUserApprovalCurrentStatus(),
        getWalletMoney(),
        sendEODCommand(),
        sendBODCommand(),
        postDeviceToken(),
        getInvestorName(),
        getUserFundPendingApprovals(),
        getUserFundStatus(),
        getUserAllInvestmentStatus(),
        postRecoEntryPrice(),
        sendAdviserMergerDemergerSell(),
        sendAdviserMergerDemergerPrimaryBuy(),
        sendAdviserMergerDemergerSecondaryBuy(),
        sendUserMergerDemergerSell(),
        sendUserMergerDemergerPrimaryBuy(),
        sendUserMergerDemergerSecondaryBuy(),
        sendSpecialCircumstancesBuy(),
        sendSpecialCircumstancesSell(),
        getPendingRebalancingListForUserAcrossFunds(),
        getRejectedListForUserFund(),
        getRejectedListForUserAcrossFunds(),
        getPendingFundRebalancingApprovalAcrossAllUsers(),
        updateKyc(),
        sendMasterCommandToChildren());
  }

  private Route updateUserFundCash() {
    Route route = headerValueByName("username",
        id -> post(() -> entity(unmarshaller(AddCashToUserFund.class),
            updateCashCommand -> handle(new UpdateUserFundCash(id, updateCashCommand.getFundName(),updateCashCommand.getAdviserUsername(), 
                updateCashCommand.getInvestingMode(), updateCashCommand.getCashToAddToUserFund())))));
    return path("updateUserFundCash", () -> Directives.post(() -> route));
  }
  

  private Route getFundRebalancingView() {
    Route parameter = headerValueByName("username",
        username -> parameter("fundName", fundName -> handle(new GetFundRebalancingView(username, fundName))));
    return path("fundrebalancingview", () -> Directives.get(() -> parameter));
  }

  private Route sendMasterCommandToChildren() {
    return path("mastercommand",
        () -> post(() -> entity(unmarshaller(SendFollowingCommandToUserFund.class), command -> handle(command))));
  }

  private Route updateKyc() {
      return path("updatekyc",
          () -> post(() -> entity(unmarshaller(UpdateKycCommand.class), command -> handle(command))));
  }

  private Route getPendingFundRebalancingApprovalAcrossAllUsers() {
    Route parameter = parameter("connectionId",
        connectionId ->  parameter("investingMode", investingMode -> handleWebSocketMessages(pendingFundRebalancingApprovals(connectionId, investingMode))));
    return path("pendingFundRebalancingApprovals", () -> Directives.get(() -> parameter));
  }
  
  private Route getPendingRebalancingListForUserAcrossFunds() {
    
    Route parameter = headerValueByName("username",
        id -> parameter("investingMode", investingMode -> handle(new GetUserPendingRebalancingList(id, investingMode))));
    return path("userAllFundPendingApprovals", () -> Directives.get(() -> parameter));
  }

  private Route getRejectedListForUserFund() {
    Route parameter = headerValueByName("username",
        username -> parameter("fundName", fundName -> parameter("investingMode",
            investingMode -> parameter("adviserName",
                adviserName ->handle(new GetUserFundRejectedAdvisesList(username, adviserName, fundName, investingMode))))));
    return path("userFundRejectedAdvises", () -> Directives.get(() -> parameter));
  }

  private Route getRejectedListForUserAcrossFunds() {
    Route parameter = headerValueByName("username",
        username -> parameter("investingMode", investingMode -> handle(new GetUserRejectedAdvisesList(username, investingMode))));
    return path("userAllFundRejectedAdvises", () -> Directives.get(() -> parameter));
  }

  private Route securityQuestions() {
    Route parameter = headerValueByName("username", username -> handle(new GetUserSecurityQuestions(username)));
    return path("securityquestions", () -> Directives.get(() -> parameter));
  }

  private Route sendAdviserMergerDemergerSecondaryBuy() {
    return path("advisercorpactionsecondarybuy",
        () -> post(() -> entity(unmarshaller(AdviserCorporateEventSecondaryBuy.class), command -> handle(command))));
  }

  private Route sendAdviserMergerDemergerPrimaryBuy() {
    return path("advisercorpactionprimarybuy",
        () -> post(() -> entity(unmarshaller(AdviserCorporateEventPrimaryBuy.class), command -> handle(command))));
  }

  private Route sendAdviserMergerDemergerSell() {
    return path("advisercorpactionsell",
        () -> post(() -> entity(unmarshaller(AdviserCorporateEventSell.class), command -> handle(command))));
  }
  
  private Route sendUserMergerDemergerSell() {
	    return path("usercorpactionsell",
	        () -> post(() -> entity(unmarshaller(SpecificUserMergerDemergerSellEvent.class), command -> handle(command))));
 }
  
  private Route sendUserMergerDemergerPrimaryBuy() {
	    return path("usercorpactionprimarybuy",
	        () -> post(() -> entity(unmarshaller(SpecificUserMergerDemergerPrimaryBuy.class), command -> handle(command))));
 }
  
  private Route sendUserMergerDemergerSecondaryBuy() {
	    return path("usercorpactionsecondarybuy",
	        () -> post(() -> entity(unmarshaller(SpecificUserMergerDemergerSecondaryBuy.class), command -> handle(command))));
 }

  private Route sendSpecialCircumstancesSell() {
    return path("specialsell",
        () -> post(() -> entity(unmarshaller(SpecialCircumstancesSell.class), command -> handle(command))));
  }

  private Route sendSpecialCircumstancesBuy() {
    return path("specialbuy",
        () -> post(() -> entity(unmarshaller(SpecialCircumstancesBuy.class), command -> handle(command))));
  }

  private Route getInvestorName() {
    Route parameter = parameter("email",
        email -> handle(new GetInvestorName(email)));
    return path("investorname", () -> Directives.get(() -> parameter));
  }

  private Route sendEODCommand() {
    return path("eodcommand",
        () -> post(() -> entity(unmarshaller(EODCompleted.class), command -> handle(command))));
  }
  
  private Route sendForceSaveSnapshot() {
    return path("forceSaveSnapshot",
        () -> post(() -> entity(unmarshaller(ForceSaveSnapshot.class), command -> handle(command))));
  }
  
  private Route sendBODCommand() {
    return path("bodcommand",
        () -> post(() -> entity(unmarshaller(BODCompleted.class), command -> handle(command))));
  }

  private Route acceptUserApprovalResponse() {
    Route route = headerValueByName("username",
        id -> post(() -> entity(unmarshaller(ReceivedUserApprovalResponse.class),
            userApprovalResponse -> handle(new AcceptUserApprovalResponse(id, userApprovalResponse.getAllUserResponses(),
                userApprovalResponse.getBasketOrderId(), userApprovalResponse.getBrokerAuthInformation())))));
    return path("userApprovalResponse", () -> Directives.post(() -> route));
  }
  
  private Route toggleAutoApproval() {
    Route route = headerValueByName("username",
        id -> post(() -> entity(unmarshaller(ReceivedUserAcceptanceMode.class),
            userApprovalResponse -> handle(new AcceptUserAcceptanceMode(id, 
                userApprovalResponse.getFundName(), 
                userApprovalResponse.getAdviserName(), 
                userApprovalResponse.getInvestingMode(), 
                userApprovalResponse.getUserAcceptanceMode())))));
    return path("userApprovalMode", () -> Directives.post(() -> route));
  }
  
  private Route approvekyc() {
    return path("approvekyc",
        () -> post(() -> entity(unmarshaller(KycApproveCommand.class), command -> handle(command))));
  }

  private Route getUserRole() {
    return path("userrolestatus",
        () -> post(() -> entity(unmarshaller(GetUserRoleStatus.class), command -> handle(command))));
  }

  private Route securityAnswers() {
    return path("securityanswers",
        () -> post(() -> entity(unmarshaller(SubmitSecurityAnswers.class), command -> handle(command))));
  }

  private Route userAnswers() {
    return path("useranswers",
        () -> post(() -> entity(unmarshaller(SubmitUserAnswers.class), command -> handle(command))));
  }

  private Route issueAdvise() {
    return path("issueadvise", () -> post(() -> entity(unmarshaller(IssueAdvice.class), command -> handle(command))));
  }
  
  private Route addMoreAllocationToAnExistingAdvice() {
    return path("increaseallocation", () -> patch(() -> entity(unmarshaller(AddMoreAllocationToAnExistingAdvice.class), 
        command -> handle(command))));
  }

  private Route closeAdvise() {
    return path("closeadvise", () -> post(() -> entity(unmarshaller(CloseAdvise.class), command -> handle(command))));
  }
  
  private Route registerAdviserFromUserPool() {
    return path("registeradviserfromuserpool",
        () -> post(() -> entity(unmarshaller(RegisterAdviserFromUserPool.class), command -> handle(command))));
  }

  private Route registerInvestorFromUserPool() {
    return path("registerinvestorfromuserpool",
        () -> post(() -> entity(unmarshaller(RegisterInvestorFromUserPool.class), command -> handle(command))));
  }

  private Route floatFund() {
    return path("funds", () -> post(() -> entity(unmarshaller(FloatFund.class), command -> handle(command))));
  }

  private Route closeFund() {
    return path("funds", () -> delete(() -> entity(unmarshaller(CloseFund.class), command -> handle(command))));
  }

  private Route updateFundAttribute() {
    return path("updateFundAttribute", () -> post(() -> entity(unmarshaller(UpdateFundAttribute.class), command -> handle(command))));
  }

  private Route calculateFundPerformance() {
    return path("calculatefundperformance",
        () -> post(() -> entity(unmarshaller(CalculateHistoricalFundPerformance.class), command -> handle(command))));
  }

  private Route calculateUserPerformance() {
    return path("calculateuserperformance",
        () -> post(() -> entity(unmarshaller(CalculateUserHistoricalPerformance.class), command -> handle(command))));
  }

  private Route sendOTP() {
    Route parameter = headerValueByName("username",
        id -> parameter("phonenumber", phonenumber -> handle(new SendOTP(phonenumber, id))));
    return path("phone", () -> Directives.post(() -> parameter));
  }

  private Route updateWealthCode() {
    Route route = headerValueByName("username", id -> post(() -> entity(unmarshaller(WealthCode.class),
        wealthCode -> handle(new UpdateWealthCode(wealthCode.getWealthCode(), id)))));
    return path("wealthcode", () -> Directives.post(() -> route));
  }

  private Route updateNumber() {
    Route parameter = headerValueByName("username", id -> parameter("otp", otp -> handle(new ValidateOTP(otp, id))));
    return path("otp", () -> Directives.post(() -> parameter));
  }

  private Route retryRejectedAdvise() {
    Route route = headerValueByName("username", id -> post(() -> entity(unmarshaller(RetryRejectedAdvise.class),
        retryRejectedAdvise -> handle(new RetryRejectedUserAdvise(id, retryRejectedAdvise.getRejectedAdviseDetails(), retryRejectedAdvise.getBasketOrderId(), retryRejectedAdvise.getBrokerAuthInformation())))));
    return path("retryrejectedadvise", () -> Directives.post(() -> route));
  }

  private Route subscribe() {
    Route parameter = headerValueByName("username",
        username -> parameter("adviserUsername",
            adviserUsername -> parameter("fundName",
                fundName -> parameter("sipAmount",
                    sipAmount -> parameter("lumpSumAmount",
                        lumpSumAmount -> parameter("sipDate",
                            sipDate -> parameter("investingMode",
                                investingMode -> handle(new SubscribeToFund(adviserUsername, fundName, username, Double.parseDouble(lumpSumAmount), Double.parseDouble(sipAmount),
                                    sipDate, InvestingMode.valueOf(investingMode), Purpose.EQUITYSUBSCRIPTION)))))))));
    return path("subscription", () -> Directives.get(() -> parameter));
  }
  
  private Route subscriptionApproval() {
    Route route = headerValueByName("username",
        id -> post(() -> entity(unmarshaller(InvestorApproval.class),
            subscription -> handle(new FundActionApproved(id, subscription.getFundName(),subscription.getAdviserUsername(), subscription.getWealthCode(),
                subscription.getInvestingMode(), subscription.getBasketOrderId(), Purpose.EQUITYSUBSCRIPTION, subscription.getApprovalResponse(),subscription.getBrokerAuthInformation())))));
    return path("subscription", () -> Directives.post(() -> route));
  }
  
  

  private Route addLumpsum() {
    Route route = headerValueByName("username",
        id -> patch(() -> entity(unmarshaller(AddLumpSum.class),
            addLumpsum -> handle(new AddLumpsumToFund(addLumpsum.getAdviserUsername(), addLumpsum.getFundName(), id,
                addLumpsum.getLumpSumAmount(), addLumpsum.getInvestingMode())))));
    return path("subscription", () -> Directives.patch(() -> route));
  }

  private Route withdraw() {
    Route parameter = headerValueByName("username",
        username -> parameter("adviserUsername",
            adviserUsername -> parameter("fundName",
                fundName -> parameter("withdrawalAmount",
                    withdrawalAmount -> parameter("investingMode",
                                investingMode -> handle(new WithdrawalFromFund(username, adviserUsername,
                                    fundName, parseDouble(withdrawalAmount),
                                    InvestingMode.valueOf(investingMode))))))));
    return path("withdraw", () -> Directives.get(() -> parameter));
  }
  
  private Route withdrawalResponse() {
    Route route = headerValueByName("username",
        id -> post(() -> entity(unmarshaller(InvestorApproval.class),
            withdraw -> handle(new FundActionApproved(id, withdraw.getFundName(),withdraw.getAdviserUsername(), withdraw.getWealthCode(),
                withdraw.getInvestingMode(), withdraw.getBasketOrderId(), PARTIALWITHDRAWAL, withdraw.getApprovalResponse(), withdraw.getBrokerAuthInformation())))));
    return path("withdraw", () -> Directives.post(() -> route));
  }

  private Route postDeviceToken() {
    Route route = headerValueByName("username", 
        id -> post(() -> entity(unmarshaller(DeviceToken.class),
        detok -> handle(new PostDeviceToken(id, detok.getDeviceToken())))));
    return path("devicetoken", () -> Directives.post(() -> route));
  }

  private Route postRecoEntryPrice() {
    Route route = headerValueByName("username", id -> post(() -> entity(unmarshaller(UpdateRecommendedEntryPrice.class),
        rep -> handle(new UpdateAdviseRecommendedEntryPrice(id, rep.getRecoEntryPrice(), rep.getAdviseId(), rep.getFundName())))));
    return path("recoentryprice", () -> Directives.post(() -> route));
  }
  
  private Route unsubscribe() {
    Route parameter = headerValueByName("username",
        username -> parameter("adviserUsername",
            adviserUsername -> parameter("fundName", fundName -> parameter("investingMode", investingMode -> handle(
                new UnsubscribeToFund(adviserUsername, fundName, username, InvestingMode.valueOf(investingMode)))))));
    return path("unsubscription", () -> get(() -> parameter));
  }

  private Route unsubscriptionApproval() {
    Route route = headerValueByName("username",
        id -> delete(() -> entity(unmarshaller(InvestorApproval.class),
            unsubscription -> handle(new FundActionApproved(id, unsubscription.getFundName(),
                unsubscription.getAdviserUsername(), unsubscription.getWealthCode(), unsubscription.getInvestingMode(),
                unsubscription.getBasketOrderId(), UNSUBSCRIPTION, unsubscription.getApprovalResponse(), unsubscription.getBrokerAuthInformation())))));
    return path("unsubscription", () -> delete(() -> route));
  }
  
  public Route getAdvises() {
    Route parameter = headerValueByName("username",
        id -> parameter("adviser", adviser -> parameter("fund", fund -> parameter("investingMode",
            investingMode -> handle(new GetAdvises(adviser, fund, InvestingMode.valueOf(investingMode), id))))));
    return path("advises", () -> Directives.get(() -> parameter));
  }

  public Route getWalletMoney() {
    Route parameter = headerValueByName("username",
        id -> parameter("investingMode", investingMode -> handle(new GetWalletMoney(id, InvestingMode.valueOf(investingMode)))));
    return path("walletmoney", () -> Directives.get(() -> parameter));
  }
  
  private Route getUserFundStatus() {
    Route parameter = headerValueByName("username",
        id -> parameter("adviser", adviser -> parameter("fund", fund -> parameter("investingMode",
            investingMode -> handle(new GetUserFundStatus(id, adviser, fund, InvestingMode.valueOf(investingMode)))))));
    return path("userfundsubscriptiondetails", () -> Directives.get(() -> parameter));
  }

  private Route getUserFundPendingApprovals() {
    Route parameter = headerValueByName("username",
        id -> parameter("adviser", adviser -> parameter("fund", fund -> parameter("investingMode",
            investingMode -> handle(new GetPendingUserFundApprovals(id, adviser, fund, InvestingMode.valueOf(investingMode)))))));
    return path("allUserFundPendingApprovals", () -> Directives.get(() -> parameter));
  }
  
  private Route getUserApprovalCurrentStatus() {
    Route parameter = headerValueByName("username",
        id -> parameter("adviser", adviser -> parameter("fund", fund -> parameter("investingMode",
            investingMode -> handle(new GetUserCurrentApprovalStatus(id, adviser, fund, InvestingMode.valueOf(investingMode)))))));
    return path("userApprovalMode", () -> Directives.get(() -> parameter));
  }
  
  private Route getUserAllInvestmentStatus() {
    Route parameter = headerValueByName("username",
        id -> parameter("investingMode",
            investingMode -> handle(new GetUserAllInvestmentStatus(id, InvestingMode.valueOf(investingMode)))));
    return path("usersubscriptiondetails", () -> Directives.get(() -> parameter));
  }
  

  static class Incoming {
    public final String message;

    public Incoming(String message) {
      this.message = message;
    }
  }

  protected Route handle(Object command) {
    return commandForwarderRoute.handle(command, adviserRootActor, userRootActor);
  }

  private void start() throws Exception {
    log.info("=======================================");
    log.info("| Starting WealthDesk - " + properties.version() + " |");
    log.info("=======================================");
    createActors();
    Materializer materializer = ActorMaterializer.create(system);

    getRuntime().addShutdownHook(new Thread(() -> {
      ctx.close();
    }));

    final Http http = Http.get(system);

    log.info("Starting on " + properties.url() + ":" + properties.port());
    final ConnectHttp host = ConnectHttp.toHost(properties.url(), properties.port());

    http.bindAndHandle(appRoute().flow(system, materializer), host, materializer);
    log.info("Started on " + properties.url() + ":" + properties.port());

    if (properties.useSSL()) {

      HttpsConnectionContext https = useHttps(system);
      ConnectHttp connect = ConnectHttp.toHostHttps(properties.urlSSL(), properties.portSSL())
          .withCustomHttpsContext(https);

      http.bindAndHandle(appRoute().flow(system, materializer), connect, materializer);
      log.info("Started on " + properties.urlSSL() + ":" + properties.portSSL());
    }
  }

  private Flow<Message, Message, NotUsed> pendingFundRebalancingApprovals(String connectionId, String investingMode) {
    Source<Message, NotUsed> source = Source.<Outgoing> actorRef(1, OverflowStrategy.dropHead())
        .map((outgoing) -> (Message) create(outgoing.message)).<NotUsed> mapMaterializedValue(destinationRef -> {
          analyticsRootActor
              .tell(new FetchAllUsersPendingFundRebalancingApprovals(investingMode, connectionId, destinationRef), noSender());
          return NotUsed.getInstance();
        });

    Sink<Message, NotUsed> sink = Flow.<Message> create()
        .map((msg) -> new Incoming(msg.asTextMessage().getStrictText())).to(Sink
            .actorRef(analyticsRootActor, new StopFetchingAllUsersPendingFundRebalancingApprovals(connectionId, noSender())));

    return fromSinkAndSource(sink, source);
  }

  public void createActors() {
    adviserRootActor = system.actorOf(SpringExtProvider.get(system).props("AdviserRootActor"), "adviser-aggregator");
    userRootActor = system.actorOf(SpringExtProvider.get(system).props("UserRootActor"), "user-aggregator");
    activeInstrumentTrackerActor = system.actorOf(SpringExtProvider.get(system).props("ActiveInstrumentTrackerActor"), "active-instrument-tracker");
    analyticsRootActor =  system.actorOf(SpringExtProvider.get(system).props("UserAnalyticsRootActor"), "analytics-aggregator");
    rdsFacadeRootActor = system.actorOf(SpringExtProvider.get(system).props("RDSFacadeRootActor"), "rds-facade-root-aggregator");
  }

  public HttpsConnectionContext useHttps(ActorSystem system) {
    HttpsConnectionContext https = null;
    try {
      final char[] password = properties.keystorePassword().toCharArray();

      final KeyStore keyStore = KeyStore.getInstance("PKCS12");
      final InputStream keyStoreStream = WDService.class.getClassLoader()
          .getResourceAsStream(properties.keystoreFileName());
      if (keyStoreStream == null) {
        throw new RuntimeException("Keystore required!");
      }
      keyStore.load(keyStoreStream, password);

      final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
      keyManagerFactory.init(keyStore, password);

      final TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
      tmf.init(keyStore);

      final SSLContext sslContext = SSLContext.getInstance("TLS");
      sslContext.init(keyManagerFactory.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

      https = ConnectionContext.https(sslContext);
    } catch (NoSuchAlgorithmException | KeyManagementException e) {
      log.debug(" while configuring HTTPS." + e.getCause(), e);
    } catch (CertificateException | KeyStoreException | UnrecoverableKeyException | IOException e) {
      log.debug(e.getCause() + " while ", e);
    } catch (Exception e) {
      log.debug(e.getCause() + " Exception", e);
    }

    return https;
  }

  @Scheduled(cron = "${market.offline.queue.schedule}", zone = "Asia/Kolkata")
  public void sendPostMarketOrders() throws Exception {
    if (!DateUtils.isTradingHoliday())
      this.userRootActor.tell(new ExecutePostMarketUserCommands(), ActorRef.noSender());
  }
  
  @Scheduled(cron = "${service.heartbeat.schedule}", zone = "Asia/Kolkata")
  public void sendServiceStatusHeartBeats() {
    if (properties.shallMonitorService())
      serviceStatusReporter.sendStatusUpdate(this.getClass().getSimpleName());
  }

}