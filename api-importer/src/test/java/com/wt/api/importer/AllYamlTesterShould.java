package com.wt.api.importer;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.Map;

import org.junit.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.yaml.snakeyaml.Yaml;

public class AllYamlTesterShould {

  private Resource[] getYAMLResources() throws IOException {
    ClassLoader classLoader = MethodHandles.lookup().getClass().getClassLoader();
    PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);

    return resolver.getResources("classpath:*.yaml");
  }

  @Test
  public void test_All_Yaml_Files_In_Project() throws IOException {
    Yaml yaml = new Yaml();
    Arrays.asList(getYAMLResources()).stream().map(resource -> resource.getFilename()).forEach(filename -> {
      System.out.println("testing YAML " + filename);
      InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(filename);

      @SuppressWarnings("unchecked")
      Map<String, Object> load = (Map<String, Object>) yaml.load(resourceAsStream);
      assertNotNull(load);
    });
  }
}
