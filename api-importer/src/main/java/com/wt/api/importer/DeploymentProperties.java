package com.wt.api.importer;

import java.util.Properties;

public interface DeploymentProperties {

  public default Properties properties() throws Exception {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream(
        "importer".concat("-").concat(System.getProperty("appropriate_environment")).concat(".properties")));
    return properties;
  };

  public default String version() throws Exception {
    Properties properties = new Properties();
    properties.load(this.getClass().getClassLoader().getResourceAsStream("version.properties"));
    return properties.getProperty("version");
  }

  public default String property(String key) throws Exception {
    String value = System.getProperty(key);
    if (value != null)
      return value;

    return properties().getProperty(key);
  }

  public default String awsRegion() throws Exception {
    return property("amazon.aws.region");
  }

  public default String environment() throws Exception {
    return property("environment");
  }

  public default String CUSTOM_DOMAIN_NAME() throws Exception {
    return property("custom.domain.name");
  }

  public default boolean useSSL() throws Exception {
    return property("useSSL").equals("true") ? true : false;
  }

  public default String importerUrlSSL() throws Exception {
    return property("importer.url.ssl");
  }

  public default Integer importerPortSSL() throws Exception {
    return new Integer(property("importer.port.ssl"));
  }

  public default boolean isCustomDomainRequired() throws Exception {
    return property("custom.domain.required").equals("true") ? true : false;
  }

  public default String importerUrl() throws Exception {
    return property("importer.url");
  }

  public default Integer importerPort() throws Exception {
    return new Integer(property("importer.port"));
  }

  public default String CLIENT_INVESTMENT_DETAILS_LAMBDA_ARN() throws Exception {
    return property("amazon.aws.clientInvestmentDetailsLambda");
  }

  public default String ADVISER_FUND_INFO_LAMBDA_ARN() throws Exception {
    return property("amazon.aws.adviserFundInfoLambda");
  }

  public default String ADVISER_FUND_CLIENT_INFO_LAMBDA_ARN() throws Exception {
    return property("amazon.aws.adviserFundClientInfo");
  }

  public default String USER_FUND_CLIENT_TRANSACTIONS_LAMBDA_ARN() throws Exception {
    return property("amazon.aws.userFundClientTransactions");
  }

  public default String CURRENT_FUND_COMPOSITION_LAMBDA_ARN() throws Exception {
    return property("amazon.aws.currentFundCompositionLambda");
  }

  public default String AURORA_ADVISER_CHART_LAMBDA_ARN() throws Exception {
    return property("amazon.aws.topicArnTriggeringReadFromAuroraLambda");
  }
  
  public default String AURORA_INVESTOR_CHART_LAMBDA_ARN() throws Exception {
    return property("amazon.aws.topicArnTriggeringReadInvestorFromAuroraLambda");
  }

  public default String apiInvestorRole() throws Exception {
    return property("iam_role.investor");
  }

  public default String apiAdviserRole() throws Exception {
    return property("iam_role.adviser");
  }

  public default String elasticSearchEndpoint() throws Exception {
    return property("amazon.aws.elasticsearch.endpoint");
  }

  public default String adviserUserPoolARN() throws Exception {
    return property("cognito.arn.adviser");
  }

  public default String apiAuthLambdaARN() throws Exception {
    return property("lambda.apiauthentication.apigateway");
  }

  public default String userAdviseTransactionLambdaARN() throws Exception {
    return property("lambda.userAdviseTransaction.apigateway");
  }

  public default String dynamoDBPerformanceProcessor() throws Exception {
    return property("lambda.dynamoDBPerformanceProcessor.apigateway");
  }

  public default String adviserAnalyticsLambdaARN() throws Exception {
    return property("lambda.adviser.analtyics");
  }

  public default String usagePlanId() throws Exception {
    return property("usagePlanId");
  }

  public String investorUserPooLARN() throws Exception;

  public default String investorFundPerformanceSnapshotLambda() throws Exception {
    return property("lambda.investor.fund.performance.snapshot");
  };
  
  public default String INVESTORS_HOLDINGS_AT_EOD_BROKER_LAMBDA_ARN() throws Exception{
    return property("lambda.investor.eod.holdings");
  }


}
