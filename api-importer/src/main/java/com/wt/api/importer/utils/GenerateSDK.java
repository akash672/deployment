package com.wt.api.importer.utils;

import static com.amazonaws.regions.Regions.fromName;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.apigateway.AmazonApiGateway;
import com.amazonaws.services.apigateway.AmazonApiGatewayClientBuilder;
import com.amazonaws.services.apigateway.model.GetRestApisRequest;
import com.amazonaws.services.apigateway.model.GetRestApisResult;
import com.amazonaws.services.apigateway.model.GetSdkRequest;
import com.amazonaws.services.apigateway.model.GetSdkResult;
import com.amazonaws.services.apigateway.model.RestApi;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;

@Lazy
@Service
public class GenerateSDK {
  private static final String VERSION_10_NOT_TO_CLASH = "10.0.0";
  private AmazonApiGateway client;
  private FileUtils fileUtils;
  private WDImporterProperties properties;

  @Autowired
  public GenerateSDK(AWSCredentialsProvider credentials, @Value("${amazon.aws.region}") String amazonAWSRegion,
      FileUtils fileUtils, WDImporterProperties properties) {
    this.properties = properties;
    client = AmazonApiGatewayClientBuilder.standard().withCredentials(credentials).withRegion(fromName(amazonAWSRegion))
        .build();
    this.fileUtils = fileUtils;
  }

  public static void main(String[] args) throws IOException {
    try (
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(APIImporterConfiguration.class)) {
      GenerateSDK importer = (GenerateSDK) ctx.getBean(GenerateSDK.class);

      String name = "Investor_API-" + importer.properties.environment();
      importer.generateSDK(name);
    }
  }

  private void generateSDK(String name) throws IOException {
    download(name);
    unzip();
  }

  private void download(String name) throws IOException {
    GetSdkRequest request = new GetSdkRequest();
    request.setStageName(properties.environment());
    request.setRestApiId(getId(name));
    request.setSdkType("android");
    ImmutableMap<String, String> params = new ImmutableMap.Builder<String, String>().put("groupId", "com.wt")
        .put("artifactId", "Wealthtech").put("artifactVersion", VERSION_10_NOT_TO_CLASH)
        .put("invokerPackage", "com.wt.client").build();

    request.withParameters(params);
    System.out.println("Environment " + properties.environment());

    GetSdkResult sdk = client.getSdk(request);

    Files.write(sdk.getBody().array(), new File(System.getProperty("user.dir") + File.separator + "sdk.zip"));
  }

  private String getId(String name) {
    GetRestApisResult restApis = client.getRestApis(new GetRestApisRequest());
    for (RestApi api : restApis.getItems()) {
      if (api.getName().equals(name))
        return api.getId();
    }
    return "";
  }

  private void unzip() throws FileNotFoundException, IOException {
    fileUtils.unZipIt(System.getProperty("user.dir") + File.separator + "sdk.zip",
        System.getProperty("user.dir") + File.separator + "sdk");
  }

}
