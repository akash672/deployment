package com.wt.api.importer.utils;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;

public class WDImporterProperties {

  @Autowired
  private Properties properties;

  public String environment() {
    return property("environment");
  }

  private String property(String key) {
    String value = System.getProperty(key);
    if (value != null)
      return value;

    return properties.getProperty(key);
  }

}
