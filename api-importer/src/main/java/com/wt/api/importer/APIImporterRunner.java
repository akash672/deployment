package com.wt.api.importer;

import java.io.IOException;
import java.util.Map;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.wt.api.importer.utils.APIImporterConfiguration;

public class APIImporterRunner {

  public static void main(String... args) throws IOException, InterruptedException {
    try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
        APIImporterConfiguration.class)) {
      @SuppressWarnings("unchecked")
      Map<String, APIImporter> environmentToItsImporter = (Map<String, APIImporter>) ctx
          .getBean("environmentToItsImporter");
      String environment = System.getProperty("appropriate_environment");
      if (args == null)
        throw new RuntimeException("Arguments are absent!");
      else if (args.length == 0)
        throw new RuntimeException("Missing Yaml file");
      APIImporter importer = environmentToItsImporter.get(environment);
      importer.importAPI(args);
    }
  }
}
