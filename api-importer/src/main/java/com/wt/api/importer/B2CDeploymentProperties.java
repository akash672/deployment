package com.wt.api.importer;

import org.springframework.stereotype.Service;

@Service("B2CDeploymentProperties")
public class B2CDeploymentProperties implements DeploymentProperties {

  public String investorUserPooLARN() throws Exception {
    return property("cognito.arn.investor");
  }
  
  public String investorFundPerformanceSnapshotLambda() throws Exception {
    return property("lambda.investor.fund.performance.snapshot");
  }

}

