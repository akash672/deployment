package com.wt.api.importer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.amazonaws.services.apigateway.AmazonApiGateway;
import com.amazonaws.services.apigateway.model.BadRequestException;
import com.amazonaws.services.apigateway.model.CreateBasePathMappingRequest;
import com.amazonaws.services.apigateway.model.CreateDeploymentRequest;
import com.amazonaws.services.apigateway.model.DeleteBasePathMappingRequest;
import com.amazonaws.services.apigateway.model.DeleteRestApiRequest;
import com.amazonaws.services.apigateway.model.GetBasePathMappingRequest;
import com.amazonaws.services.apigateway.model.GetRestApisRequest;
import com.amazonaws.services.apigateway.model.GetRestApisResult;
import com.amazonaws.services.apigateway.model.NotFoundException;
import com.amazonaws.services.apigateway.model.Op;
import com.amazonaws.services.apigateway.model.PatchOperation;
import com.amazonaws.services.apigateway.model.RestApi;
import com.amazonaws.services.apigateway.model.TooManyRequestsException;
import com.amazonaws.services.apigateway.model.UpdateStageRequest;
import com.amazonaws.services.apigateway.model.UpdateUsagePlanRequest;

public interface APIImporter {
  public static final String ADD = "add";
  public static final String API_STAGES = "/apiStages";
  public static final String REMOVE = "remove";
  
  public AmazonApiGateway getClient();
  public DeploymentProperties getProperties();
  public HashMap<String,String> getApiNameToIdMap();
  
  public default void importAPI(String... args) {
    try {
      String fileName = args[0];
      String name = name(fileName);
      System.out.println("Name : " + name);
      removeAPIfromUsagePlan(name);
      removeAPIfromCustomDomain(name, getCustomDomainPathName(fileName));
      deleteAPI(name);
      String apiID = importAPI(name, fileName);
      deployAPI(name, apiID);
      addAPItoUsagePlan(name);
      addAPItoCustomDomain(name, getCustomDomainPathName(fileName));
    } catch (Exception e) {
      System.err.println("FATAL ERROR FOR PROCESSING API TEMPLATE" + e.getMessage());
      e.printStackTrace();
    }
  }
  public default String getCustomDomainPathName(String fileName) {
    String pathName = fileName.split("API")[0].toLowerCase();
    if(pathName.contains("noauth"))
      return pathName.split("noauth")[0];
    return pathName;
  };

  public String importAPI(String name, String fileName) throws Exception;
  
  public String getEnvironment() throws Exception;
  
  public default String name(String fileName) throws Exception {
    return fileName.split("\\.")[0].concat("-").concat(getEnvironment());
  }
  
  public default void removeAPIfromUsagePlan(String name) {
    for (int i = 0; i < 2; i++) {
      try {
        UpdateUsagePlanRequest updateUsagePlanRequest = createUsagePlanRequest(name, REMOVE);
        getClient().updateUsagePlan(updateUsagePlanRequest);
        break;
      } catch (TooManyRequestsException e) {
        System.out.println("TooManyRequestsException - Wait for a while");
        sleepToAvoidAWSTooManyRequestException();
      } catch (NotFoundException e) {
        System.err.println("No Mapping found for API and Usage Plans, going to next step");
        break;
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    System.out.println("Removed API from UsagePlan  : " + name);
  }
  
  public default void removeAPIfromCustomDomain(String name, String path) throws Exception {
    if (!isCustomDomainPresent()) {
      System.out.println("Not removing api from domain");
      return;
    }
    try {
      GetBasePathMappingRequest getBasePathMappingRequest = new GetBasePathMappingRequest().withBasePath(path)
          .withDomainName(getProperties().CUSTOM_DOMAIN_NAME());
      getClient().getBasePathMapping(getBasePathMappingRequest);
    } catch (NotFoundException e) {
      System.err.println("base path does not exist");
      return;
    }
    DeleteBasePathMappingRequest deleteBasePathMappingRequest = new DeleteBasePathMappingRequest()
        .withDomainName(getProperties().CUSTOM_DOMAIN_NAME()).withBasePath(path);
    getClient().deleteBasePathMapping(deleteBasePathMappingRequest);
    System.out.println("Removed API " + name + " from custom domain  : " + getProperties().CUSTOM_DOMAIN_NAME());
  }
  
  public default void deleteAPI(String name) {

    String id = getId(name);
    DeleteRestApiRequest request = new DeleteRestApiRequest();
    request.setRestApiId(id);

    for (int i = 0; i < 2; i++) {
      try {
        getClient().deleteRestApi(request);
        System.out.println("Deleted " + name);
        break;
      } catch (TooManyRequestsException e) {
        System.err.println("TooManyRequestsException - Wait for a while");
        sleepToAvoidAWSTooManyRequestException();
      } catch (Exception e) {
        System.err.println(e);
      }
    }
  }
  
  public default void addAPItoUsagePlan(String name) throws Exception {
    UpdateUsagePlanRequest createUsagePlanRequest = createUsagePlanRequest(name, ADD);
    for (int i = 0; i < 2; i++) {
      try {
        getClient().updateUsagePlan(createUsagePlanRequest);
        break;
      } catch (TooManyRequestsException e) {
        System.err.println("TooManyRequestsException - Wait for a while");
        sleepToAvoidAWSTooManyRequestException();
      } catch (BadRequestException e) {
        System.err.println("BadRequestException - Wait for a while");
        sleepToAvoidAWSTooManyRequestException();
      } catch (Exception e) {
        System.err.println("Exception Reported");
        e.printStackTrace();
        sleepToAvoidAWSTooManyRequestException();
      }
    }
    System.out.println("addAPItoUsagePlan  : " + name);
  }
  
  public default void addAPItoCustomDomain(String name, String path) throws Exception {
    if (!isCustomDomainPresent()) {
      System.out.println("Not adding api to custom domain");
      return;
    }
    
    for (int i = 0; i < 2; i++) {
      try {
        CreateBasePathMappingRequest createBasePathMappingRequest = new CreateBasePathMappingRequest()
            .withDomainName(getProperties().CUSTOM_DOMAIN_NAME()).withBasePath(path).withRestApiId(getId(name))
            .withStage(getEnvironment());
        getClient().createBasePathMapping(createBasePathMappingRequest);
        System.out.println("Added API " + name + " to custom domain  : " + getProperties().CUSTOM_DOMAIN_NAME());
        break;
      } catch (Exception e) {
        e.printStackTrace();
        System.out.println(e.getMessage() + " Retrying again after 1 minute");
        sleepToAvoidAWSTooManyRequestException();
      }
    }
  }
  
  public default boolean isCustomDomainPresent() throws Exception {
    return getProperties().isCustomDomainRequired();
  }
  
  public default void deployAPI(String name, String apiID) throws Exception {
    CreateDeploymentRequest createDeploymentRequest = new CreateDeploymentRequest();
    createDeploymentRequest.setRestApiId(apiID);
    createDeploymentRequest.setStageName(getEnvironment());
    for (int i = 0; i < 2; i++) {
      try {
        getClient().createDeployment(createDeploymentRequest);
        break;
      } catch (TooManyRequestsException e) {
        System.err.println("TooManyRequestsException - Wait for a while");
        sleepToAvoidAWSTooManyRequestException();
      }
    }

    setId(name, apiID);
    System.out.println("API deployed  : " + apiID);
    if (isNotDevDemoProd(name))
      getClient().updateStage(new UpdateStageRequest().withRestApiId(apiID).withStageName(getEnvironment())
          .withPatchOperations(getpatchOperations()));
  }

  public default List<PatchOperation> getpatchOperations() {
    List<PatchOperation> list = new ArrayList<PatchOperation>();
    list.add(new PatchOperation().withOp(Op.Replace).withPath("/*/*/metrics/enabled").withValue("true"));
    list.add(new PatchOperation().withOp(Op.Replace).withPath("/*/*/logging/dataTrace").withValue("true"));
    list.add(new PatchOperation().withOp(Op.Replace).withPath("/*/*/logging/loglevel").withValue("ERROR"));
    return list;
  }
  
  public default UpdateUsagePlanRequest createUsagePlanRequest(String name, String operation) throws Exception {
    String apiID = getId(name);
    UpdateUsagePlanRequest updateUsagePlanRequest = new UpdateUsagePlanRequest();
    updateUsagePlanRequest.setUsagePlanId(getProperties().usagePlanId());
    PatchOperation patchOperation = createPatchOperations(apiID, operation);
    return updateUsagePlanRequest.withPatchOperations(Arrays.asList(patchOperation));
  }
  
  public default String getId(String name) {
    String apiId = getApiNameToIdMap().get(name);
    if (apiId == null)
      return getIdFromAWS(name);
    return apiId;
  }
  
  public default String getIdFromAWS(String name) {
    GetRestApisResult restApis = getClient().getRestApis(new GetRestApisRequest());
    for (RestApi api : restApis.getItems()) {
      if (api.getName().equals(name))
        return api.getId();
    }
    System.err.println("Could get correct Api ID from AWS ***");
    return "";
  }
  
  public default PatchOperation createPatchOperations(String apiID, String operation) throws Exception {
    PatchOperation patchOperation = new PatchOperation();
    patchOperation.setPath(API_STAGES);
    patchOperation.setValue(apiID + ":" + getEnvironment());
    patchOperation.setOp(operation);
    return patchOperation;
  }
  
  public default void setId(String name, String id) {
    getApiNameToIdMap().put(name, id);
  }
  
  public default void sleepToAvoidAWSTooManyRequestException() {
    try {
      Thread.sleep((long) (1.2 * 60 * 1000));
    } catch (InterruptedException e) {
      System.err.println("Thread InterruptedException Occured");
    }
  }
  
  public default boolean isNotDevDemoProd(String name) {
    return !(name.toLowerCase().contains("uat")) || !(name.toLowerCase().contains("dev")) || !(name.toLowerCase().contains("demo"))
        || !(name.toLowerCase().contains("prod"));
  }
}
