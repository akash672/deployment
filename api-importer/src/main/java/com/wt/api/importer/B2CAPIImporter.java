package com.wt.api.importer;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.apigateway.AmazonApiGateway;
import com.amazonaws.services.apigateway.AmazonApiGatewayClientBuilder;
import com.amazonaws.services.apigateway.model.ImportRestApiRequest;
import com.amazonaws.services.apigateway.model.ImportRestApiResult;
import com.amazonaws.services.apigateway.model.TooManyRequestsException;

import lombok.Getter;

public class B2CAPIImporter implements APIImporter {
  @Getter
  private AmazonApiGateway client;
  @Getter
  private B2CDeploymentProperties properties;
  @Getter
  private HashMap<String, String> apiNameToIdMap = new HashMap<String, String>();

  public B2CAPIImporter(AWSCredentialsProvider credentials, B2CDeploymentProperties properties) throws Exception {
    super();
    this.properties = properties;
    client = AmazonApiGatewayClientBuilder.standard().withCredentials(credentials)
        .withRegion(Regions.fromName(properties.awsRegion())).build();
  }

  @Override
  public String getEnvironment() throws Exception {
    if (properties.environment() == null)
      return "b2c";
    return properties.environment();
  }

  @Override
  public String importAPI(String name, String fileName) throws Exception {
    ImportRestApiRequest request = new ImportRestApiRequest();

    try (InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName)) {
      String contents = IOUtils.toString(in);
      contents = contents.replaceAll("_TITLE_", name);
      contents = contents.replaceAll("_ENVIRONMENT_", getEnvironment());
      if (properties.useSSL()) {
        contents = contents.replaceAll("_HOST_", "https://" + properties.importerUrlSSL());
        contents = contents.replaceAll("_PORT_", "" + properties.importerPortSSL());
      } else {
        contents = contents.replaceAll("_HOST_", "http://" + properties.importerUrl());
        contents = contents.replaceAll("_PORT_", "" + properties.importerPort());
      }
      contents = contents.replaceAll("_CLIENTINVESTMENTDETAILS_", properties.CLIENT_INVESTMENT_DETAILS_LAMBDA_ARN());
      contents = contents.replaceAll("_ADVISERFUNDINFO_", properties.ADVISER_FUND_INFO_LAMBDA_ARN());
      contents = contents.replaceAll("_ADVISERFUNDCLIENTINFO_", properties.ADVISER_FUND_CLIENT_INFO_LAMBDA_ARN());
      contents = contents.replaceAll("_CLIENTTRANSACTIONS_", properties.USER_FUND_CLIENT_TRANSACTIONS_LAMBDA_ARN());
      contents = contents.replaceAll("_CURRENTFUNDCOMPOSITION_", properties.CURRENT_FUND_COMPOSITION_LAMBDA_ARN());
      contents = contents.replaceAll("_ADVISERCHARTLAMBDA_", properties.AURORA_ADVISER_CHART_LAMBDA_ARN());
      contents = contents.replaceAll("_INVESTORCHARTLAMBDA_", properties.AURORA_INVESTOR_CHART_LAMBDA_ARN());
      contents = contents.replaceAll("_INVESTOREODHOLDINGSFORBROKER_", properties.INVESTORS_HOLDINGS_AT_EOD_BROKER_LAMBDA_ARN());
      String role = properties.apiInvestorRole();
      if (name.contains("Adviser_API")) {
        role = properties.apiAdviserRole();
      }

      contents = contents.replaceAll("_ES_ENDPOINT_", properties.elasticSearchEndpoint());
      contents = contents.replaceAll("_IAMROLE_", role);
      contents = contents.replaceAll("_REGION_", "" + properties.awsRegion());
      contents = contents.replaceAll("_ADVISERUSERPOOLARN_", "" + properties.adviserUserPoolARN());
      contents = contents.replaceAll("_AUTHLAMBDAARN_", "" + properties.apiAuthLambdaARN());
      contents = contents.replace("_INVESTORUSERPOOLARN_", "" + properties.investorUserPooLARN());
      contents = contents.replace("_ADVISE_TRANSACTION_LAMBDA_ARN_",
          "".concat(properties.userAdviseTransactionLambdaARN()));
      contents = contents.replace("_DYNAMODBPERFORMANCEPROCESSOR_",
          "".concat(properties.dynamoDBPerformanceProcessor()));
      contents = contents.replace("_ANALYTICSLAMBDA_", "".concat(properties.adviserAnalyticsLambdaARN()));
      contents = contents.replace("_INVESTOR_FUND_PERFORMANCE_SNAPSHOT_LAMBDA_ARN_",
          "".concat(properties.investorFundPerformanceSnapshotLambda()));
      request.setBody(ByteBuffer.wrap(contents.getBytes()));
      for (int i = 0; i < 2; i++) {
        try {
          ImportRestApiResult result = client.importRestApi(request);
          System.out.println("Imported " + name);

          return result.getId();
        } catch (TooManyRequestsException e) {
          System.err.println("TooManyRequestsException - Wait for a while");
          sleepToAvoidAWSTooManyRequestException();
        }
      }
      throw new RuntimeException("Could not deploy API " + name);
    } catch (Exception e) {
      System.err.println("Error " + e.getMessage());
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

}