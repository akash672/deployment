package com.wt.api.importer.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.apigateway.AmazonApiGateway;
import com.amazonaws.services.apigateway.AmazonApiGatewayClientBuilder;
import com.wt.api.importer.APIImporter;
import com.wt.api.importer.B2BAPIImporter;
import com.wt.api.importer.B2BDeploymentProperties;
import com.wt.api.importer.B2CAPIImporter;
import com.wt.api.importer.B2CDeploymentProperties;

@EnableAsync
@EnableScheduling
@Configuration
@ComponentScan(basePackages = { "com.wt.api.importer" })
@PropertySource("classpath:importer-${appropriate_environment}.properties")
public class APIImporterConfiguration {

  @Autowired
  protected B2CDeploymentProperties b2cProperties;

  @Autowired
  protected B2BDeploymentProperties b2bProperties;

  @Value("${amazon.aws.accesskey}")
  private String amazonAWSAccessKey;

  @Value("${amazon.aws.secretkey}")
  private String amazonAWSSecretKey;

  @Value("${amazon.aws.region}")
  private String amazonAWSRegion;

  @Primary
  @Bean(name = "AWSCredentials")
  public AWSCredentials amazonAWSCredentials() {
    return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
  }

  @Primary
  @Bean(name = "AWSCredentialsProvider")
  public AWSCredentialsProvider amazonAWSCredentialsProvider() {
    return new AWSStaticCredentialsProvider(amazonAWSCredentials());
  }

  @Bean(name = "B2CAPIImporter")
  public B2CAPIImporter getB2CApiImporter() throws Exception {
    return new B2CAPIImporter(amazonAWSCredentialsProvider(), b2cProperties);
  }

  @Bean(name = "B2BAPIImporter")
  public B2BAPIImporter getB2BApiImporter() throws Exception {
    return new B2BAPIImporter(amazonAWSCredentialsProvider(), b2bProperties);
  }

  @Bean(name = "environmentToItsImporter")
  public Map<String, APIImporter> environmentToItsImporter() throws Exception {
    Map<String, APIImporter> environmentToItsImporter = new HashMap<String, APIImporter>();
      environmentToItsImporter.put("b2c", getB2CApiImporter());
      environmentToItsImporter.put("b2b", getB2BApiImporter());

    return environmentToItsImporter;
  }

  public String getAmazonAWSRegion() {
    return this.amazonAWSRegion;
  }

  public AmazonApiGateway apiGatewayClient() {
    return AmazonApiGatewayClientBuilder.standard().withRegion(getAmazonAWSRegion())
        .withCredentials(amazonAWSCredentialsProvider()).build();
  }
}
