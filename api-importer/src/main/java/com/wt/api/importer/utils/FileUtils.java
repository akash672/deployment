package com.wt.api.importer.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.springframework.stereotype.Service;

@Service
public class FileUtils {
  public void unZipIt(String zipFile, String outputFolder) throws FileNotFoundException, IOException {
    byte[] buffer = new byte[1024];
    File folder = new File(outputFolder);
    if (!folder.exists()) {
      folder.mkdir();
    }
    try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {

      ZipEntry ze = zis.getNextEntry();

      while (ze != null) {

        String fileName = ze.getName();
        File newFile = new File(outputFolder + File.separator + fileName);

        new File(newFile.getParent()).mkdirs();

        FileOutputStream fos = new FileOutputStream(newFile);

        int len;
        while ((len = zis.read(buffer)) > 0) {
          fos.write(buffer, 0, len);
        }

        fos.close();
        ze = zis.getNextEntry();
      }

      zis.closeEntry();
    }

  }

}
